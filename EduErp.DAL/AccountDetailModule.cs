﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using EduErp.BO;
using EduErp.DAL.Utilities;
using EduErp.Model;
using AccountDetail = EduErp.Model.AccountDetail;

namespace EduErp.DAL
{
   public class AccountDetailModule
    {

       public static bool AddAccountDetail(AccountDetailBo account)
       {

           using (var context = new EduErpEntities())
           {
               var accountDetail = context.AccountDetails.SingleOrDefault(a => a.SchoolId == account.SchoolId);
               if (accountDetail != null) return true;
               var model = new AccountDetail
               {
                   SchoolId = account.SchoolId,
                   AppStartDate = account.AppStartDate,
                   NextPaymentDate = account.NextPaymentDate,
                   LeftStudentCount = account.LeftStudentCount,
                   LeftMessageCount = account.LeftMessageCount,
                   LeftUserCount=account.LeftUserCount,
                   PlanId = account.PlanId,
                   StatusId = account.StatusId,
                   DueDate = account.DueDate,
                   CreatedOn = account.CreatedOn,
                   CreatedBy = account.CreatedBy,
                   WeeklyHolidays = account.WeeklyHolidays
               };
               context.AccountDetails.Add(model);
               context.SaveChanges();
           }
           return true;
       }

       public static List<AccountDetailBo> GetAcountDetails()
       {
           List<AccountDetailBo> details = new List<AccountDetailBo>();
           using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
           {
              // spdb.ClearParameters();
              // spdb.AddParameter("@SchoolId", schoolId);
               using (var dr = spdb.ExecuteReader("GetAccountDetail", CommandType.StoredProcedure))
               {
                   var drgetter = new DataReaderObjectLoader<AccountDetailBo>(dr, false);
                   while (dr.Read())
                   {
                       var obj = drgetter.GetObjectFromDataReader(dr, true);//use tool to load object from dr.row
                       details.Add(obj);
                   }
               }

           }
           return details;
       }

       public static AccountDetailBo GetAccountDetailById(int id)
       {
           AccountDetailBo account;
           using (var context = new EduErpEntities())
           {
               account = (from ac in context.AccountDetails
                         where ac.Id == id
                          select new AccountDetailBo
                          {
                              Id = ac.Id,
                              SchoolId = ac.SchoolId,
                              AppStartDate = ac.AppStartDate,
                              NextPaymentDate = ac.NextPaymentDate,
                              DueDate = ac.DueDate,
                              LeftUserCount = ac.LeftUserCount,
                              LeftStudentCount=ac.LeftStudentCount,
                              LeftMessageCount = ac.LeftMessageCount,
                              StatusId = ac.StatusId,
                              PlanId = ac.PlanId
                            }).FirstOrDefault();
           }
           return account;
       }

       public static bool UpdateAccountDetail(AccountDetailBo model)
       {
           using (var context = new EduErpEntities())
           {
               var account = context.AccountDetails.FirstOrDefault(a => a.Id == model.Id);
               if (account != null)
               {
                   account.SchoolId = model.SchoolId;
                   account.AppStartDate = model.AppStartDate;
                   account.NextPaymentDate = model.NextPaymentDate;
                   account.DueDate = model.DueDate;
                  account.LeftStudentCount = model.LeftStudentCount;
                  account.LeftUserCount = model.LeftUserCount;
                   account.LeftMessageCount = model.LeftMessageCount;
                   account.UpdatedBy = model.UpdatedBy;
                   account.UpdatedOn = model.UpdatedOn;
                   account.PlanId = model.PlanId;
                   context.SaveChanges();
                   return true;
               }
              return false;
           }
       }

       public static List<Common> GetPaymentMode()
       {
           List<Common> paymentmodeList;
           using (var context = new EduErpEntities())
           {
               paymentmodeList = (from p in context.KeyWordMasters
                          where p.Name == "PaymentMode"
                          select new Common
                          {
                              Id = p.KeyWordId,
                              Name = p.KeyWordName
                          }).ToList();
           }
           return paymentmodeList;
       }

       public static List<Common> GetPlan()
       {
           List<Common> planList;
           using (var context = new EduErpEntities())
           {
               planList = (from p in context.PlanMasters
                                  where p.StatusId==(int)UserStatus.Active
                                  select new Common
                                  {
                                      Id = p.Id,
                                      Name = p.Name
                                  }).ToList();
           }
           return planList;
       }

       public static bool UpdateStatus(int id, int statusid, int userId)
       {
           using (var context = new EduErpEntities())
           {
               var account = context.AccountDetails.FirstOrDefault(a => a.Id == id);
               if (account == null) return false;
               account.StatusId = statusid;
               account.UpdatedOn = DateTime.Now;
               account.UpdatedBy = userId;
               context.SaveChanges();
               return true;
           }
       }

       public static List<DayMenu> GetDayList()
       {
           List<DayMenu> day;
           using (var context = new EduErpEntities())
           {
               day = (from d in context.KeyWordMasters
                               where d.Name == "Day"
                                select new DayMenu
                               {
                                   DayId = d.KeyWordId,
                                   DayName = d.KeyWordName
                               }).ToList();
           }
           return day;
       }

    }
}
