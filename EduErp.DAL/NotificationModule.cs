﻿using System;
using System.Collections.Generic;
using System.Linq;
using EduErp.BO;
using EduErp.Model;
using NoticeMaster = EduErp.Model.NoticeMaster;

namespace EduErp.DAL
{
    public class NotificationModule
    {
        public static List<Common> GetNotificationTypeList()
        {
            List<Common> notificationList;
            using (var context = new EduErpEntities())
            {
                notificationList = (from k in context.KeyWordMasters
                                    where k.Name == "Notification"
                                    select new Common
                                    {
                                        Name = k.KeyWordName,
                                        Id = k.KeyWordId
                                    }).ToList();
            }
            return notificationList;
        }

        public static NotificationBo GetNotificationById(int id)
        {
            NotificationBo notification;
            using (var context = new EduErpEntities())
            {
                notification = (from nm in context.NoticeMasters
                                where nm.StatusId != (int)UserStatus.Deleted && nm.Id == id
                                select new NotificationBo
                                {
                                    Subject = nm.Subject,
                                    Description = nm.Description,
                                    NoticeTypeId = nm.NoticeTypeId,
                                    Id = nm.Id,
                                }).FirstOrDefault();
            }
            return notification;
        }

        public static bool UpdateNotification(NotificationBo model)
        {
            var context = new EduErpEntities();
            var notification = context.NoticeMasters.SingleOrDefault(a => a.Id == model.Id);
            if (notification != null)
            {
                notification.Subject = model.Subject;
                notification.NoticeTypeId = model.NoticeTypeId;
                notification.Description = model.Description;
                notification.UpdatedBy = model.UpdatedBy;
                notification.UpdatedOn = model.UpdatedOn;
            }
            context.SaveChanges();
            return true;
        }

        public static bool UpdateStatus(int id, int statusId, int userId)
        {
            using (var context = new EduErpEntities())
            {
                var notice = context.NoticeMasters.FirstOrDefault(a => a.Id == id);
                if (notice == null) return false;
                notice.StatusId = statusId;
                notice.UpdatedBy = userId;
                notice.UpdatedOn = DateTime.Now;
                context.SaveChanges();
                return true;
            }
        }

        public static bool AddNotification(NotificationBo model)
        {

            using (var context = new EduErpEntities())
            {
                var notification = new NoticeMaster
                 {
                     Subject = model.Subject,
                     NoticeTypeId = model.NoticeTypeId,
                     Description = model.Description,
                     SchoolId = model.SchoolId,
                     CreatedBy = model.CreatedBy,
                     CreatedOn = model.CreatedOn,
                     StatusId = model.StatusId
                 };

                context.NoticeMasters.Add(notification);
                context.SaveChanges();
            }
            return true;
        }

        public static List<NotificationBo> GetNotifications()
        {
            List<NotificationBo> notificationsList;
            using (var context = new EduErpEntities())
            {
                notificationsList = (from nm in context.NoticeMasters
                                     join k in context.KeyWordMasters on nm.NoticeTypeId equals k.KeyWordId
                                     where nm.StatusId != (int)UserStatus.Deleted && k.Name == "Notification"
                                     select new NotificationBo
                                     {
                                         Subject = nm.Subject,
                                         Description = nm.Description,
                                         StatusId = nm.StatusId,
                                         NoticeTypeId = nm.NoticeTypeId,
                                         Id = nm.Id,
                                         SchoolId = nm.SchoolId,
                                         NotificationTypeName = k.KeyWordName
                                     }).ToList();
            }
            return notificationsList;
        }

    }
}
