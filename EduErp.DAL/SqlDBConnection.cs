﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace EduErp.DAL
{
   public  class SqlDBConnection
    {
        public static string GetConnectionString()
        {
            string str = ConfigurationManager.ConnectionStrings["EduErpDBConn"].ConnectionString;
            //SqlConnection con = new SqlConnection(str);
            //con.Open();
            return str;
        }

        //-- code to make sure to close connection and dispose the object
        public static void DisposeConnection(SqlConnection con)
        {
            if (con.State == ConnectionState.Open)
                con.Close();
            con.Dispose();
        }
    }
}
