﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using EduErp.BO;
using EduErp.DAL.Utilities;
using EduErp.Model;
using FeeTransaction = EduErp.Model.FeeTransaction;
using FeeTransactionDetail = EduErp.Model.FeeTransactionDetail;

namespace EduErp.DAL
{
    public class FeeTransactionModule
    {

        public static List<Common> GetSections(int classId)
        {
            List<Common> sections;
            using (var context = new EduErpEntities())
            {
                sections = (from c in context.SectionMasters
                            where c.Status == (int)UserStatus.Active && c.ClassId == classId
                            select new Common
                            {
                                Id = c.Id,
                                Name = c.Name
                            }).ToList();
            }
            return sections;
        }

        public static List<Common> GetClasses(int schoolId)
        {
            List<Common> classes;
            using (var context = new EduErpEntities())
            {
                classes = (from c in context.ClassMasters
                           where c.SchoolId == schoolId && c.Status == (int)UserStatus.Active
                           select new Common
                           {
                               Id = c.Id,
                               Name = c.Name

                           }).ToList();
            }
            return classes;
        }

        public static List<Common> GetStudentBySectionId(int sectionId, int sessionId)
        {
            List<Common> students;
            using (var context = new EduErpEntities())
            {

                students = (from s in context.Students
                            join sm in context.StudentClassMappings on s.Id equals sm.StudentId
                            where sm.SectionId == sectionId && s.StatusId == (int)UserStatus.Active && sm.SessionId == sessionId
                            select new Common
                            {
                                Id = s.Id,
                                Name = s.FirstName
                            }).ToList();
            }
            return students;
        }

        public static int AddFeeTransaction(FeeTransactionBo model, int schoolId)
        {
            using (var context = new EduErpEntities())
            {


                var tomonthId = (from f in context.FeeTransactions where f.StudentId == model.StudentId select f.ToMonthId).Max();
                if (tomonthId != null)
                {
                    //Boolean resul = context.FeeTransactions.Any(p => p.ToMonthId < model.FromMonthId && p.StudentId == model.StudentId);
                    if (tomonthId >= model.FromMonthId)
                    {
                        return -1;
                    }
                }
                var availableMessage = MessageModule.LeftMessage(schoolId);
                if (availableMessage >= 1)
                {
                    var updatedLeftMessage = availableMessage - 1;
                    var accountDetail = context.AccountDetails.FirstOrDefault(a => a.SchoolId == schoolId);
                    if (accountDetail != null)
                    {
                        accountDetail.LeftMessageCount = updatedLeftMessage;
                        context.SaveChanges();
                    }
                }
                var transaction = new FeeTransaction
                {
                    IsSettled = model.IsBalancedSettled,
                    StudentId = model.StudentId,
                    TransactionDate = model.TransactionDate,
                    Remark = model.Remark,
                    TotalAmountDeposit = model.TotalAmountDeposit,
                    BalanceAmount = model.BalanceAmount,
                    CreatedOn = model.CreatedOn,
                    CreatedBy = model.CreatedBy,
                    StatusId = model.StatusId,
                    FromMonthId = model.FromMonthId,
                    ToMonthId = model.ToMonthId,
                    SessionId = model.SessionId,
                    PaymentModeId = model.PaymentModeId,
                    TotalAmount = model.TotalClassGeneralAmount,
                    TotalPayableAmount = model.TotalAmount,
                    PreviousBalance = model.PreviousBalance
                };

                context.FeeTransactions.Add(transaction);
                context.SaveChanges();
                if (model.FeeTransactionTemplateBo.ClassByComponentLists != null)
                {
                    foreach (var feedetail in model.FeeTransactionTemplateBo.ClassByComponentLists)
                    {
                        if (!feedetail.Selected)
                            continue;
                        var feetransactionDetail = new FeeTransactionDetail
                        {
                            FeeTransactionId = transaction.Id,
                            FeeHeadMappingId = feedetail.FeeHeadMappingId

                        };
                        context.FeeTransactionDetails.Add(feetransactionDetail);
                        context.SaveChanges();
                    }
                }
                if (model.FeeTransactionTemplateBo.GeneralByComponentLists != null)
                {
                    foreach (var generalFeeTransactionDetail in from generalFeeDetail in model.FeeTransactionTemplateBo.GeneralByComponentLists
                                                                where generalFeeDetail.Selected
                                                                select new FeeTransactionDetail
                                                                {
                                                                    FeeTransactionId = transaction.Id,
                                                                    FeeHeadMappingId = generalFeeDetail.FeeHeadMappingId

                                                                })

                        context.FeeTransactionDetails.Add(generalFeeTransactionDetail);
                    context.SaveChanges();
                }

                if (model.FeeTransactionTemplateBo.DiscountDetailList != null)
                {
                    foreach (var discount in model.FeeTransactionTemplateBo.DiscountDetailList)
                    {
                        var discounttransDetail = new Model.FineDiscountTransactionDetail
                        {
                            TransactionId = transaction.Id,
                            FeeHeadId = discount.DiscountFeeHeadId,
                            FeeHeadAmount = discount.DiscountAmount
                        };
                        context.FineDiscountTransactionDetails.Add(discounttransDetail);
                        context.SaveChanges();
                    }
                }
                if (model.FeeTransactionTemplateBo.FineDetailList != null)
                {
                    foreach (var discount in model.FeeTransactionTemplateBo.FineDetailList)
                    {
                        var finetransDetail = new Model.FineDiscountTransactionDetail
                        {
                            TransactionId = transaction.Id,
                            FeeHeadId = discount.FineFeeHeadId,
                            FeeHeadAmount = discount.Amount
                        };
                        context.FineDiscountTransactionDetails.Add(finetransDetail);
                        context.SaveChanges();
                    }
                }
                return transaction.Id;
            }
        }

        // public int GetTransactionId()

        public static FeeTransactionTemplateBo GetFeeTransactionDetailByClassSection(int sectionId, int classId, int schoolId, int sessionId)
        {   //var context= new EduErpEntities();
            // var previuosAmount = (from ft in context.FeeTransactions select ft).ToList().Select(a=>a.Reverse().Skip(1).Take(1).DefaultIfEmpty());
            var transactionDetail = new List<FeetransactionDetailList>();
            var model = new FeeTransactionTemplateBo();
            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                spdb.AddParameter("@SessionId", sessionId);
                spdb.AddParameter("@SectionId", sectionId);
                spdb.AddParameter("@ClassId", classId);
                using (var dr = spdb.ExecuteReader("GetFeeTransactionByClassSection", CommandType.StoredProcedure))
                {
                    var drgetter = new DataReaderObjectLoader<FeetransactionDetailList>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true);//use tool to load object from dr.row
                        transactionDetail.Add(obj);
                    }
                }

                //spdb.ClearParameters();
                //spdb.AddParameter("@StudentId", 19);
                //using (var dr = spdb.ExecuteReader("GetPreviousBalance", CommandType.StoredProcedure))
                //{
                //    //var drgetter = new DataReaderObjectLoader<FeeTransactionTemplateBo>(dr, false);
                //    while (dr.Read())
                //    {
                //        model.Prevbal = Convert.ToDecimal((dr["BalanceAmount"] as decimal?) ?? 0);
                //    }
                //}
                var result = new FeeTransactionTemplateBo
                {
                    FeetransactionDetailList = transactionDetail
                    // Prevbal=model.Prevbal

                };
                return result;
            }

        }
        public static FeeTransactionBo GetFeeTransactionByTransactionId(int transactionId)
        {
            var model = new FeeTransactionBo();
            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@FeetransactionId", transactionId);
                //spdb.AddParameter("@SessionId", sessionId);
                //spdb.AddParameter("@SectionId", sectionId);
                //spdb.AddParameter("@ClassId", classId);

                using (var dr = spdb.ExecuteReader("GetFeeTransactionByTransactionId", CommandType.StoredProcedure))
                {
                    // var drgetter = new DataReaderObjectLoader<FeeTransactionBo>(dr, false);

                    while (dr.Read())
                    {
                        //var obj = drgetter.GetObjectFromDataReader(dr, true);//use tool to load object from dr.row
                        //transactionDetailByTransactionId=obj;
                        //transactionDetailByTransactionId.Add(obj);
                        model.Id = Convert.ToInt32(dr["Id"]);
                        model.StudentId = Convert.ToInt32(dr["StudentId"]);
                        model.TotalAmountDeposit = Convert.ToDecimal((dr["TotalAmountDeposit"] as decimal?) ?? 0);
                        model.ToMonthId = Convert.ToInt32((dr["ToMonthId"] as int?) ?? 0);
                        model.FromMonthId = Convert.ToInt32((dr["FromMonthId"] as int?) ?? 0);
                        model.SessionId = Convert.ToInt32(dr["SessionId"]);
                        model.SessionName = dr["SessionName"].ToString();
                        model.ClassId = Convert.ToInt32(dr["ClassId"]);
                        model.SectionId = Convert.ToInt32(dr["SectionId"]);
                        model.TotalAmount = Convert.ToDecimal((dr["TotalAmount"] as decimal?) ?? 0);
                        model.StatusId = Convert.ToInt32(dr["StatusId"]);
                        model.IsBalancedSettled = Convert.ToBoolean((dr["IsSettled"] as bool?) ?? false);
                        model.Remark = dr["Remark"].ToString();
                        model.BalanceAmount = Convert.ToDecimal((dr["BalanceAmount"] as decimal?) ?? 0);
                        model.TotalPayableAmount = Convert.ToDecimal((dr["TotalPayableAmount"] as decimal?) ?? 0);
                        model.PreviousBalance = Convert.ToDecimal((dr["PreviousBalance"] as decimal?) ?? 0);
                        model.TotalClassGeneralAmount = Convert.ToDecimal((dr["TotalClassGeneralAmount"] as decimal?) ?? 0);
                        model.ClassName = dr["ClassName"].ToString();
                        model.SectionName = dr["SectionName"].ToString();
                        model.SessionName = dr["SessionName"].ToString();
                        model.FromMonthName = dr["FromMonthName"].ToString();
                        model.ToMonthName = dr["ToMonthName"].ToString();
                        model.StudentName = dr["StudentName"].ToString();
                       
                        //model.FirstName = dr["FirstName"].ToString();
                        //model.LastName = dr["LastName"].ToString();
                        model.PaymentModeId = Convert.ToInt32((dr["PaymentModeId"] as int?) ?? 0);
                        model.PaymentModeName = dr["PaymentModeName"].ToString();
                        model.S3key = new Guid(Convert.ToString(dr["S3key"]));
                        model.SessionId = Convert.ToInt32((dr["SessionId"] as int?) ?? 0);

                    }
                }

                return model;
            }
        }

        public static FeeTransactionTemplateBo GetFeeTransactionDetailByStudentId(int studentId, int sessionId)
        {
            var transactionDetailByStudentId = new List<FeetransactionDetailList>();
            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@StudentId", studentId);
                spdb.AddParameter("@SessionId", sessionId);
                //spdb.AddParameter("@SectionId", sectionId);
                //spdb.AddParameter("@ClassId", classId);
                using (var dr = spdb.ExecuteReader("GetFeeTransactionByStudentId", CommandType.StoredProcedure))
                {
                    var drgetter = new DataReaderObjectLoader<FeetransactionDetailList>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true);//use tool to load object from dr.row
                        transactionDetailByStudentId.Add(obj);
                    }
                }
                var result = new FeeTransactionTemplateBo
                {
                    FeetransactionDetailList = transactionDetailByStudentId

                };
                return result;
            }

        }

        public static List<FineDetail> GetFineList(int transId)
        {
            List<FineDetail> fineList;
            using (var context = new EduErpEntities())
            {
                fineList = (from fm in context.FeeHeadMasters
                            join fdtd in context.FineDiscountTransactionDetails on fm.Id equals fdtd.FeeHeadId
                            where fdtd.TransactionId == transId && fm.FeeHeadTypeId == (int)FeeHeadType.Fine
                            select new FineDetail
                            {
                                Id = fdtd.TransactionId,
                                Amount = fdtd.FeeHeadAmount,
                                FineName = fm.Name,
                                FineFeeHeadId = fdtd.FeeHeadId

                            }).ToList();
            }
            return fineList;
        }


        public static List<DiscountDetail> GetDiscountList(int transId)
        {
            List<DiscountDetail> discountList;
            using (var context = new EduErpEntities())
            {
                discountList = (from fm in context.FeeHeadMasters
                                join fdtd in context.FineDiscountTransactionDetails on fm.Id equals fdtd.FeeHeadId
                                where fdtd.TransactionId == transId && fm.FeeHeadTypeId == (int)FeeHeadType.Discount
                                select new DiscountDetail
                                {
                                    Id = fdtd.TransactionId,
                                    DiscountAmount = fdtd.FeeHeadAmount,
                                    DiscountTypeName = fm.Name,
                                    DiscountFeeHeadId = fdtd.FeeHeadId

                                }).ToList();
            }
            return discountList;
        }
        public static List<BO.MessageAutomateMappingBo> GetAutomateMessageId(int schoolId)
        {
            List<BO.MessageAutomateMappingBo> automateMessageDetail;
            using (var context = new EduErpEntities())
            {
                automateMessageDetail = (from amm in context.MessageAutomateMappings
                                         join tm in context.TemplateMasters on amm.TemplateId equals tm.Id
                                         where amm.SchoolId == schoolId
                                         select new BO.MessageAutomateMappingBo
                                         {
                                             TemplateId = amm.TemplateId,
                                             TemplateMessage = tm.Text,
                                             MessageAutomateId = amm.MessageAutomateId
                                         }).ToList();
            }
            return automateMessageDetail;
        }
        public static List<int> GetFeeTransactionMappingId(int id)
        {
            List<int> transactionDetail;
            using (var context = new EduErpEntities())
            {
                transactionDetail = (from ftd in context.FeeTransactionDetails
                                     where ftd.FeeTransactionId == id
                                     select ftd.FeeHeadMappingId).ToList();
            }

            return transactionDetail;
        }

        //public static FeeTransactionBo GetFeeTransactionDetailById(int studentId)
        //{

        //    var feeTransactiondetail = new FeeTransactionBo();

        //    using (var context=new EduErpEntities())
        //    {
        //         feeTransactiondetail = (from ft in context.FeeTransactions join s in context.Students on ft.StudentId equals s.Id
        //                                 where ft.StudentId == studentId
        //                                 select new FeeTransactionBo()
        //                                  {
        //                                    Id = ft.Id,
        //                                    Name = s.FirstName+" "+s.MiddileName+" "+s.LastName,
        //                                    TotalAmountDeposit=ft.TotalAmountDeposit,
        //                                    BalanceAmount=ft.BalanceAmount,
        //                                    IsSettled=ft.IsSettled,
        //                                    TransactionDate=ft.TransactionDate

        //                                  }).FirstOrDefault();

        //        }
        //    return feeTransactiondetail;


        // }

        public static FeeTransactionTemplateBo GetReceiptData(int transactionId, int studentId)
        {
            var Components = new List<ComponentList>();
            var model = new FeeTransactionTemplateBo();
            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@FeeTranId", transactionId);
                using (var dr = spdb.ExecuteReader("FeeTranPrintDetail", CommandType.StoredProcedure))
                {
                    var drgetter = new DataReaderObjectLoader<ComponentList>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true);//use tool to load object from dr.row
                        Components.Add(obj);
                    }
                }

                var result = new FeeTransactionTemplateBo
                {
                    ComponentList = Components
                };
                return result;
            }
        }

        //public static decimal 
        public static FeeTransactionTemplateBo GetComponentForEdit(int sessionId, int schoolId, int fromMonthId, int toMonthId, int classId, int studentId)
        {
            var classByComponents = new List<ClassByComponentList>();
            var genaralByComponents = new List<GeneralByComponentList>();
            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                spdb.AddParameter("@SessionId", sessionId);
                spdb.AddParameter("@FromMonth", fromMonthId);
                spdb.AddParameter("@ToMonth", toMonthId);
                spdb.AddParameter("@ClassId", classId);
                spdb.AddParameter("@StudentId", studentId);
                spdb.AddParameter("@HeadTypeId", (int)FeeHeadType.ClassBy);

                using (var dr = spdb.ExecuteReader("GetFeeHeadDetailForEdit", CommandType.StoredProcedure))
                {
                    var drgetter = new DataReaderObjectLoader<ClassByComponentList>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true);//use tool to load object from dr.row
                        classByComponents.Add(obj);
                    }
                }
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                spdb.AddParameter("@SessionId", sessionId);
                spdb.AddParameter("@FromMonth", fromMonthId);
                spdb.AddParameter("@ToMonth", toMonthId);

                spdb.AddParameter("@HeadTypeId", (int)FeeHeadType.General);

                using (var dr = spdb.ExecuteReader("GetGeneralwiseFeeHeadDetialForEdit", CommandType.StoredProcedure))
                {
                    var drgetter = new DataReaderObjectLoader<GeneralByComponentList>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true);//use tool to load object from dr.row
                        genaralByComponents.Add(obj);
                    }
                }
                var result = new FeeTransactionTemplateBo
                {
                    ClassByComponentLists = classByComponents,
                    GeneralByComponentLists = genaralByComponents
                };
                return result;
            }
        }
        public static decimal GetPreviousAmount(int studentId)
        {
            using (var context = new EduErpEntities())
            {
                if (studentId != 0)
                {
                    var result = context.FeeTransactions.Where(b => b.StudentId == studentId).OrderByDescending(a => a.Id).FirstOrDefault();
                    if (result != null)
                        return result.IsSettled == true ? 0 : result.BalanceAmount ?? 0;
                }
                return 0;
            }
        }

        public static FeeTransactionTemplateBo GetComponent(int sessionId, int schoolId, int fromMonthId, int toMonthId, int classId, int studentId)
        {
            var classByComponents = new List<ClassByComponentList>();
            var genaralByComponents = new List<GeneralByComponentList>();
            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                spdb.AddParameter("@SessionId", sessionId);
                spdb.AddParameter("@FromMonth", fromMonthId);
                spdb.AddParameter("@ToMonth", toMonthId);
                spdb.AddParameter("@ClassId", classId);
                spdb.AddParameter("@StudentId", studentId);
                spdb.AddParameter("@HeadTypeId", (int)FeeHeadType.ClassBy);
                using (var dr = spdb.ExecuteReader("GetFeeHeadDetial", CommandType.StoredProcedure))
                {
                    var drgetter = new DataReaderObjectLoader<ClassByComponentList>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true);//use tool to load object from dr.row
                        classByComponents.Add(obj);
                    }
                }
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                spdb.AddParameter("@SessionId", sessionId);
                spdb.AddParameter("@FromMonth", fromMonthId);
                spdb.AddParameter("@ToMonth", toMonthId);
                spdb.AddParameter("@StudentId", studentId);
                spdb.AddParameter("@HeadTypeId", (int)FeeHeadType.General);

                using (var dr = spdb.ExecuteReader("GetGeneralwiseFeeHeadDetial", CommandType.StoredProcedure))
                {
                    var drgetter = new DataReaderObjectLoader<GeneralByComponentList>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true);//use tool to load object from dr.row
                        genaralByComponents.Add(obj);
                    }
                }
                var result = new FeeTransactionTemplateBo
                {
                    ClassByComponentLists = classByComponents,
                    GeneralByComponentLists = genaralByComponents,

                };
                return result;
            }
        }


        public static bool UpdateComponent(FeeTransactionBo model)
        {

            var context = new EduErpEntities();
            var feeTransaction = context.FeeTransactions.FirstOrDefault(a => a.Id == model.Id);
            if (feeTransaction == null) return false;
            feeTransaction.FromMonthId = model.FromMonthId;
            feeTransaction.ToMonthId = model.ToMonthId;
            feeTransaction.TotalAmountDeposit = model.TotalAmountDeposit;
            feeTransaction.BalanceAmount = model.BalanceAmount;
            feeTransaction.IsSettled = model.IsBalancedSettled;
            feeTransaction.Remark = model.Remark;
            feeTransaction.LastUpdatedBy = model.LastUpdatedBy;
            feeTransaction.LastUpdatedOn = model.LastUpdatedOn;
            feeTransaction.PaymentModeId = model.PaymentModeId;
            feeTransaction.SessionId = model.SessionId;
            feeTransaction.TotalAmount = model.TotalClassGeneralAmount;
            feeTransaction.TotalPayableAmount = model.TotalPayableAmount;
            feeTransaction.PreviousBalance = model.PreviousBalance;
            if (model.FeeTransactionTemplateBo.ClassByComponentLists != null)
            {
                foreach (var item in model.FeeTransactionTemplateBo.ClassByComponentLists)
                {

                    var oldTransactionDetail = context.FeeTransactionDetails.FirstOrDefault(a => a.Id == model.Id && a.FeeHeadMappingId == item.FeeHeadMappingId);
                    if (oldTransactionDetail != null)
                    {
                        context.FeeTransactionDetails.Remove(oldTransactionDetail);
                        context.SaveChanges();
                    }
                    if (!item.Selected) continue;

                    var newTransactionDetail = new FeeTransactionDetail
                    {
                        FeeTransactionId = model.Id,
                        FeeHeadMappingId = item.FeeHeadMappingId

                    };
                    context.FeeTransactionDetails.Add(newTransactionDetail);
                    context.SaveChanges();

                }
            }
            if (model.FeeTransactionTemplateBo.GeneralByComponentLists != null)
            {
                foreach (var item in model.FeeTransactionTemplateBo.GeneralByComponentLists)
                {
                    var oldTransactionDetailByGeneral = context.FeeTransactionDetails.FirstOrDefault(a => a.FeeTransactionId == model.Id && a.FeeHeadMappingId == item.FeeHeadMappingId);
                    if (oldTransactionDetailByGeneral != null)
                    {
                        context.FeeTransactionDetails.Remove(oldTransactionDetailByGeneral);
                        context.SaveChanges();
                    }

                    if (!item.Selected)
                        continue;
                    var newTransactionDetailByGeneral = new FeeTransactionDetail
                    {
                        FeeTransactionId = model.Id,
                        FeeHeadMappingId = item.FeeHeadMappingId

                    };
                    context.FeeTransactionDetails.Add(newTransactionDetailByGeneral);
                    context.SaveChanges();


                }
            }

            if (model.FeeTransactionTemplateBo.DiscountDetailList != null)
            {
                foreach (var item in model.FeeTransactionTemplateBo.DiscountDetailList)
                {
                    context.FineDiscountTransactionDetails.Where(a => a.FeeHeadId == item.DiscountFeeHeadId && a.TransactionId == item.Id).ToList().ForEach(a => context.FineDiscountTransactionDetails.Remove(a));
                    context.SaveChanges();
                }

                foreach (var discount in model.FeeTransactionTemplateBo.DiscountDetailList)
                {
                    //var oldTransactionDetail = context.FineDiscountTransactionDetails.FirstOrDefault(a => a.Id ==  discount.Id);
                    //if (oldTransactionDetail != null)
                    //{
                    //    context.FineDiscountTransactionDetails.Remove(oldTransactionDetail);
                    //    context.SaveChanges();
                    //}
                    var discounttransDetail = new Model.FineDiscountTransactionDetail
                    {
                        TransactionId = model.Id,
                        FeeHeadId = discount.DiscountFeeHeadId,
                        FeeHeadAmount = discount.DiscountAmount
                    };
                    context.FineDiscountTransactionDetails.Add(discounttransDetail);
                    context.SaveChanges();
                }
            }
            if (model.FeeTransactionTemplateBo.FineDetailList != null)
            {
                foreach (var item in model.FeeTransactionTemplateBo.FineDetailList)
                {
                    context.FineDiscountTransactionDetails.Where(a => a.FeeHeadId == item.FineFeeHeadId && a.TransactionId == item.Id).ToList().ForEach(a => context.FineDiscountTransactionDetails.Remove(a));
                    context.SaveChanges();
                }
                foreach (var fine in model.FeeTransactionTemplateBo.FineDetailList)
                {
                    //var oldTransactionDetail = context.FineDiscountTransactionDetails.FirstOrDefault(a => a.Id ==fine.Id );
                    //if (oldTransactionDetail != null)
                    //{
                    //    context.FineDiscountTransactionDetails.Remove(oldTransactionDetail);
                    //    context.SaveChanges();
                    //}
                    var finetransDetail = new Model.FineDiscountTransactionDetail
                    {
                        TransactionId = model.Id,
                        FeeHeadId = fine.FineFeeHeadId,
                        FeeHeadAmount = fine.Amount
                    };
                    context.FineDiscountTransactionDetails.Add(finetransDetail);
                    context.SaveChanges();
                }
            }
            context.SaveChanges();
            return true;

        }

        public static bool UpdateStatus(int id, int statusid, int userId)
        {
            using (var context = new EduErpEntities())
            {
                var transaction = context.FeeTransactions.FirstOrDefault(a => a.Id == id);
                if (transaction == null) return false;
                transaction.StatusId = statusid;
                transaction.LastUpdatedOn = DateTime.Now;
                transaction.LastUpdatedBy = userId;
                context.SaveChanges();
                return true;
            }
        }

        public static List<Common> GetMonthList()
        {
            List<Common> monthList;
            using (var context = new EduErpEntities())
            {
                monthList = (from s in context.KeyWordMasters
                             where s.Name == "Month"
                             select new Common
                             {
                                 Id = s.KeyWordId,
                                 Name = s.KeyWordName
                             }).ToList();
            }
            return monthList;
        }

        public static List<Common> GetFineTypeFeeHeads(int schoolId)
        {
            List<Common> fineFeeheadsList;
            using (var context = new EduErpEntities())
            {
                fineFeeheadsList = (from s in context.FeeHeadMasters
                                    where s.SchoolId == schoolId && s.FeeHeadTypeId == 4 && s.StatusId == (int)UserStatus.Active
                                    select new Common
                                    {
                                        Id = s.Id,
                                        Name = s.Name
                                    }).ToList();
            }
            return fineFeeheadsList;
        }
        public static List<Common> GetDiscountTypeFeeHeads(int schoolId)
        {
            List<Common> discountFeeheadsList;
            using (var context = new EduErpEntities())
            {
                discountFeeheadsList = (from s in context.FeeHeadMasters
                                        where s.SchoolId == schoolId && s.FeeHeadTypeId == 3 && s.StatusId == (int)UserStatus.Active
                                        select new Common
                                        {
                                            Id = s.Id,
                                            Name = s.Name
                                        }).ToList();
            }
            return discountFeeheadsList;
        }
        public static List<Common> GetPaymentModeList()
        {
            List<Common> paymentList;
            using (var context = new EduErpEntities())
            {
                paymentList = (from s in context.KeyWordMasters
                               where s.Name == "PaymentMode"
                               select new Common
                               {
                                   Id = s.KeyWordId,
                                   Name = s.KeyWordName
                               }).ToList();
            }
            return paymentList;
        }
    }
}
