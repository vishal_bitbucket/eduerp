﻿using System;
using System.Collections.Generic;
using System.Linq;
using EduErp.DAL.Utilities;
using EduErp.Model;
using EduErp.BO;
using System.Data;


namespace EduErp.DAL
{
    public class ExpensesModule
    {
        public static bool AddExpenses(ExpensesBo model)
        {
            using (var context = new EduErpEntities())
            {
                //var result = context.Inventories.FirstOrDefault(a => a.StatusId != (int)UserStatus.Deleted && a.Name.ToLower().Equals(model..ToLower()) && a.SchoolId == model.SchoolId);
                //if (result != null)
                //{
                //    return -1;
                //}
                var expenses = new Model.Inventory
                {

                    ItemId = model.ItemId,
                    InvetoryTypeId = model.InvetoryTypeId,
                    ItemDescription = model.ItemDescription,
                    EndDate = model.EndDate,
                    VenderId = model.VenderId,
                    StartDate = model.StartDate,
                    StatusId = model.StatusId,
                    SchoolId = model.SchoolId,
                    CreatedBy = model.CreatedBy,
                    CreatedOn = model.CreatedOn,
                    SessionId = model.SessionId,
                    Amount = model.Amount,
                    PaymentModeTypeId=model.PaymentModeTypeId
                    // VendorName = model.VendorName
                };
                context.Inventories.Add(expenses);
                context.SaveChanges();
                return true;
            }
        }

        public static bool UpdateStatus(int id, int statusid, int userId)
        {
            using (var context = new EduErpEntities())
            {
                var expenses = context.Inventories.FirstOrDefault(a => a.Id == id);
                if (expenses == null) return false;
                expenses.StatusId = statusid;
                expenses.LastUpdatedOn = DateTime.Now;
                expenses.LastUpdatedBy = userId;
                context.SaveChanges();
                return true;
            }
        }

        public static List<ExpensesBo> GetInventories(int schoolId)
        {
            List<ExpensesBo> inventories = new List<ExpensesBo>();

            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                using (var dr = spdb.ExecuteReader("GetInventory", CommandType.StoredProcedure))
                {
                    var drExpenses = new DataReaderObjectLoader<ExpensesBo>(dr, false);
                    while (dr.Read())
                    {

                        var objec = drExpenses.GetObjectFromDataReader(dr, true);
                        inventories.Add(objec);
                    }
                }
            }
            return inventories;
        }

        public static bool UpdateExpenses(ExpensesBo model)
        {

            using (var context = new EduErpEntities())
            {
                var expenses = context.Inventories.FirstOrDefault(a => a.Id == model.Id);
                if (expenses != null)
                {
                    expenses.EndDate = model.EndDate;
                    expenses.StartDate = model.StartDate;
                    expenses.ItemId = model.ItemId;
                    expenses.InvetoryTypeId = model.InvetoryTypeId;
                    expenses.VenderId = model.VenderId;
                    expenses.SessionId = model.SessionId;
                    expenses.ItemDescription = model.ItemDescription;
                    expenses.Amount = model.Amount;
                    expenses.LastUpdatedBy = model.LastUpdatedBy;
                    expenses.LastUpdatedOn = model.LastUpdatedOn;
                    expenses.PaymentModeTypeId = model.PaymentModeTypeId;
                    // Expenses.VendorName = model.VendorName;
                    expenses.SchoolId = model.SchoolId;
                    context.SaveChanges();
                    return true;

                }
                return false;
            }

        }
        public static ExpensesBo GetExpensesByExpensesId(int expensesId)
        {
            ExpensesBo expenses = new ExpensesBo();
            using (var spdp = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdp.ClearParameters();
                spdp.AddParameter("@Id", expensesId);
                using (var dr = spdp.ExecuteReader("GetInventoryByInventoryId", CommandType.StoredProcedure))
                {
                    var expensesById = new DataReaderObjectLoader<ExpensesBo>(dr, false);
                    while (dr.Read())
                    {
                        expenses = expensesById.GetObjectFromDataReader(dr, true);

                    }
                }


            }
            return expenses;
        }

        public static List<Common> GetEmployeeNameList(int schoolId)
        {
            List<Common> empList;
            using (var context = new EduErpEntities())
            {
                empList = (from emp in context.TeacherMasters
                           where emp.StatusId == (int)UserStatus.Active && emp.SchoolId == schoolId
                           select new Common
                           {
                               Id = emp.Id,
                               Name = emp.FirstName
                           }).ToList();
            }
            return empList;
        }

        public static List<Common> GetExpensesType()
        {
            List<Common> expenses;
            using (var context = new EduErpEntities())
            {
                expenses = (from invent in context.KeyWordMasters
                            where invent.Name == "InventoryType"
                            select new Common
                            {
                                Id = invent.KeyWordId,
                                Name = invent.KeyWordName
                            }).ToList();
            }
            return expenses;
        }


        public static List<Common> GetItemsList(int expenseTypeId, int schoolId)
        {
            List<Common> items;
            using (var context = new EduErpEntities())
            {
                items = (from item in context.ItemMasters
                         where item.StatusId == (int)UserStatus.Active && item.SchoolId == schoolId && item.ExpensesTypeId == expenseTypeId
                         select new Common
                         {
                             Id = item.Id,
                             Name = item.Name
                         }).ToList();
            }
            return items;
        }
        public static List<Common> GetVendorList(int expenseTypeId, int schoolId)
        {
            List<Common> venders;
            using (var context = new EduErpEntities())
            {
                venders = (from vender in context.VendorMasters
                           where vender.StatusId == (int)UserStatus.Active && vender.SchoolId == schoolId && vender.TypeId == expenseTypeId
                           select new Common
                           {
                               Id = vender.Id,
                               Name = vender.Name
                           }).ToList();
            }
            return venders;
        }
    }
}
