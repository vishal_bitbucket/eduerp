﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EduErp.BO;
using EduErp.Model;
using ClassMaster = EduErp.Model.ClassMaster;
namespace EduErp.DAL
{
   public class OnlineTestModule
    {
       public static int AddOnlineTest(OnlineExamTestBo OnlineUser)
        {
            using (var context = new EduErpEntities())
            {
                var result = context.OnlineTestMasters.FirstOrDefault(a => a.StatusId != (int)UserStatus.Deleted && a.Name.ToLower().Equals(OnlineUser.Name.ToLower()) && a.SchoolId == OnlineUser.SchoolId);
                if (result != null)
                {
                    return -1;
                }
                var model = new Model.OnlineTestMaster()
                {
                    Name = OnlineUser.Name,
                    CreatedOn = OnlineUser.CreatedOn,
                    CreatedBy = OnlineUser.CreatedBy,
                    SchoolId = OnlineUser.SchoolId,
                    StatusId = OnlineUser.Status,
                    //LastUpdatedBy  = OnlineUser.LastUpdatedBy,
                   // LastUpdatedOn = OnlineUser.LastUpdatedOn,
                    TimeInMinutes = OnlineUser.TimeInMinutes,
                    //NumberOfQuestions=OnlineUser.NumberOfQuestion,
                    StartDate = OnlineUser.StartDate,
                    StartTime = OnlineUser.StartTime,
                };
                context.OnlineTestMasters.Add(model);
                context.SaveChanges();
                return model.Id;
            }            
        }
       public static List<OnlineExamTestBo> GetOnlineTest(int schoolId)
       {


           List<OnlineExamTestBo> onlin;
           using (var context = new EduErpEntities())
           {
               onlin = (from s in context.OnlineTestMasters
                       where s.StatusId != (int)UserStatus.Deleted && (s.SchoolId == schoolId || s.SchoolId == 0)

                        select new OnlineExamTestBo
                       {
                           Id = s.Id,
                           SchoolId = s.SchoolId,
                           Name = s.Name,
                           TimeInMinutes = s.TimeInMinutes ?? 0,
                           //NumberOfQuestion = s.NumberOfQuestions ?? 0,
                           StartTime = s.StartTime,
                           StartDate = s.StartDate??DateTime.MinValue,
                           Status = s.StatusId
                       }).ToList();
           }
           return onlin;
       }
       public static bool UpdateStatus(int id, int status, int userId)
       {
           using (var context = new EduErpEntities())
           {
               var onlin = context.OnlineTestMasters.FirstOrDefault(a => a.Id == id);
               if (onlin == null) return false;
               onlin.StatusId = status;
               onlin.LastUpdatedOn = DateTime.Now;
               onlin.LastUpdatedBy = userId;
               context.SaveChanges();
               return true;

           }


       }

       public static OnlineExamTestBo GetOnlineTestById(int id)
       {
           OnlineExamTestBo onlin;
           using (var context = new EduErpEntities())
           {
               onlin = (from o in context.OnlineTestMasters
                       where id == o.Id

                       select new OnlineExamTestBo()
                       {

                           Name = o.Name,
                          
                           //NumberOfQuestion= o.NumberOfQuestions ?? 0,
                           TimeInMinutes= o.TimeInMinutes ?? 0,
                           StartDate = o.StartDate??DateTime.MinValue,
                           StartTime = o.StartTime,
                           SchoolId = o.SchoolId,
                           Status = o.StatusId
                       }).FirstOrDefault();
           }
           return onlin;
       }
       public static bool UpdateClass(OnlineExamTestBo model)
       {

           using (var context = new EduErpEntities())
           {
               var onlin = context.OnlineTestMasters.SingleOrDefault(a => a.Id == model.Id);

               if (onlin != null)
               {
                   onlin.Id = model.Id;
                   onlin.Name = model.Name;
                   //onlin.NumberOfQuestions = model.NumberOfQuestion;
                   onlin.TimeInMinutes = model.TimeInMinutes;
                   onlin.StartDate = model.StartDate;
                   onlin.StartTime = model.StartTime;
                   onlin.LastUpdatedBy = model.LastUpdatedBy;
                   onlin.LastUpdatedOn = model.LastUpdatedOn;
                   onlin.StatusId = model.Status;
                   onlin.SchoolId = model.SchoolId;
                   context.SaveChanges();
                   return true;
               }
               return false;
           }
       }
    }
}
