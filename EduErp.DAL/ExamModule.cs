﻿using System;
using System.Collections.Generic;
using System.Linq;
using EduErp.BO;
using EduErp.Model;
using ExamMaster = EduErp.Model.ExamMaster;
using ExamSubjectMapping = EduErp.Model.ExamSubjectMapping;

namespace EduErp.DAL
{
    public class ExamModule
    {
        public static int AddExam(ExamBo examDetail)
        {
            using (var context = new EduErpEntities())
            {
                
                var result = context.ExamMasters.FirstOrDefault(a => a.StatusId != (int)UserStatus.Deleted && a.Name.ToLower().Equals(examDetail.Name.ToLower()) && a.SchoolId == examDetail.SchoolId);
                if (result != null)
                {
                    return -1;
                }
                var model = new ExamMaster
                {

                    Name = examDetail.Name,
                    SessionId=examDetail.SessionId,
                    CreatedOn = examDetail.CreatedOn,
                    CreatedBy = examDetail.CreatedBy,
                    SchoolId = examDetail.SchoolId,
                    StatusId = examDetail.StatusId
                    };
                context.ExamMasters.Add(model);
                context.SaveChanges();
                if (examDetail.MappingList.Count > 0)
                {
                    foreach (var item in examDetail.MappingList)
                    {
                        var examMappingDetail = new ExamSubjectMapping
                        {
                            ExamId = model.Id,
                            TotalMarks = item.TotalMarks,
                            Date = item.Date,
                            SubjectId = item.SubjectId,
                           };
                        context.ExamSubjectMappings.Add(examMappingDetail);
                        context.SaveChanges();
                    }
                    
                }
                return 1;
                }
        }



        public static List<ExamMapping> GetMappingList(int id)
        {
            List<ExamMapping> mappings;
            using (var context = new EduErpEntities())
            {
                mappings = (from mp in context.ExamSubjectMappings
                            join sub in context.SubjectMasters on mp.SubjectId equals sub.Id
                           where mp.ExamId == id && sub.Status==(int)UserStatus.Active
                            select new ExamMapping
                            {
                               SubjectId = mp.SubjectId,
                                SubjectName = sub.Name,
                                TotalMarks = mp.TotalMarks,
                                Date = mp.Date??DateTime.Now
                            }).ToList();
            }
            return mappings;
        }

        public static ExamBo GetExamById(int id,int schoolId)
        {
            ExamBo exam;
            using (var context = new EduErpEntities())
            {
                exam = (from c in context.ExamMasters
                        where  c.Id==id && c.SchoolId==schoolId && c.StatusId==(int)UserStatus.Active
                        select new ExamBo
                        {
                            Name = c.Name,
                            SessionId=c.SessionId??0,
                            SchoolId = c.SchoolId,
                            StatusId = c.StatusId??0
                        }).FirstOrDefault();
            }
            return exam;
        }
        public static List<ExamBo> GetExames(int schoolId)
        {
            List<ExamBo> exams;
            using (var context = new EduErpEntities())
            {
                exams = (from s in context.ExamMasters
                        where s.StatusId != (int)UserStatus.Deleted && (s.SchoolId == schoolId || s.SchoolId == 0)

                        select new ExamBo
                        {
                            Id = s.Id,
                            SchoolId = s.SchoolId,
                            Name = s.Name,
                            StatusId = s.StatusId??0
                        }).ToList();
            }
            return exams;
        }

        public static bool UpdateExam(ExamBo model)
        {
            using (var context= new EduErpEntities())
            {
                var exam = context.ExamMasters.SingleOrDefault(a => a.Id == model.Id);
                if (exam == null)
                    return true;
                    exam.Name = model.Name;
                    exam.SessionId = model.SessionId;
                    exam.SchoolId = model.SchoolId;
                    exam.LastUpdatedBy = model.LastUpdatedBy;
                    exam.LastUpdatedOn = model.LastUpdatedOn;
                     context.SaveChanges();
                if (model.MappingList == null) return true;
                  var examSubjectMappingId = (from s in context.ExamSubjectMappings
                      where s.ExamId == exam.Id
                      select s.Id).ToList();
                foreach (var mapId in examSubjectMappingId)
                {
                    context.StudentExamMappings.Where(a => a.ExamSubjectMappingId == mapId).ToList().ForEach(a => context.StudentExamMappings.Remove(a));
                    context.SaveChanges(); 
                }
                context.ExamSubjectMappings.Where(a => a.ExamId == exam.Id).ToList().ForEach(a => context.ExamSubjectMappings.Remove(a));
                context.SaveChanges();
                foreach (var item in model.MappingList)
                {
                    var newExamDetail = new ExamSubjectMapping
                    {
                        ExamId = exam.Id,
                        TotalMarks = item.TotalMarks,
                        Date = item.Date,
                        SubjectId = item.SubjectId
                    };
                    context.ExamSubjectMappings.Add(newExamDetail);
                    context.SaveChanges();
                }
             }
            return true;
        }
        

        public static bool UpdateStatus(int id, int statusId, int userId)
        {
            using (var context = new EduErpEntities())
            {
                var exams = context.ExamMasters.FirstOrDefault(a => a.Id == id);
                if (exams == null) return false;
                exams.StatusId = statusId;
                exams.LastUpdatedOn = DateTime.Now;
                exams.LastUpdatedBy = userId;
                context.SaveChanges();
                return true;
            }
        }
    }
}