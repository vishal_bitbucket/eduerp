﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using EduErp.BO;
using EduErp.DAL.Utilities;
using EduErp.Model;
using EmployeeMaster = EduErp.Model.TeacherMaster;
using TeacherSubjectMapping = EduErp.Model.TeacherSubjectMapping;

namespace EduErp.DAL
{
    public class EmployeeModule
    {
        public static int AddEmployee(EmployeeBo model)
        {
            if (model.DateOfBirth == DateTime.Parse("1/1/0001"))
            {
                model.DateOfBirth = null;
            }
            if (model.DOJ == DateTime.Parse("1/1/0001"))
            {
                model.DOJ = null;
            }
            if (model.DateOfExit == DateTime.Parse("1/1/0001"))
            {
                model.DateOfExit = null;
            }
            using (var context = new EduErpEntities())
            {
                var result = context.TeacherMasters.Where(p => p.EmployeeCode == model.EmployeeCode && p.SchoolId==model.SchoolId).FirstOrDefault();
                if (result != null)
                {
                    return -1;
                }
                var Employee = new EmployeeMaster
                {
                    EmployeeCode = model.EmployeeCode,
                    PermanentStateId = model.PermanentStateId,
                    CurrentStateId = model.CurrentStateId,
                    MotherName = model.MotherName,
                    FatherName = model.FatherName,
                    PermanentPinCode = model.PermanentPinCode,
                    PermanentAddress = model.PermanentAddress,
                    CurrentAddress= model.CurrentAddress,
                    FirstName = model.FirstName,
                    MiddleName = model.MiddleName,
                    LastName = model.LastName,
                    MobileNumber = model.MobileNumber,
                    Email = model.Email,
                    GenderId = model.GenderId,
                    DateOfBirth = model.DateOfBirth,
                    TitleId = model.TitleId,
                    RelegionId = model.RelegionId,
                    CategoryId = model.CategoryId,
                    NationalityId = model.NationalityId,
                    DOJ = model.DOJ,
                    DateOfExit = model.DateOfExit,
                    CurrentPinCode = model.CurrentPinCode,
                    ImagePath = model.ImagePath,
                    CreatedOn = model.CreatedOn,
                    CreatedBy = model.CreatedBy,
                    PermanentCityId = model.PermanentCityId,
                    CurrentCityId = model.CurrentCityId,
                    StatusId = model.StatusId,
                    ReasonForExit = model.ReasonForExit,
                    SchoolId = model.SchoolId,
                    UID = model.UID,
                    Graduation = model.Graduation,
                    GraduationMarks = model.GraduationMarks,
                    GraduationPassingYear = model.GraduationPassingYear,
                    HighSchoolBoardName = model.HighSchoolBoardName,
                    HighSchoolMarks = model.HighSchoolMarks,
                    HighSchoolMediumId = model.HighSchoolMediumId,
                    HighschoolPassingYear = model.HighschoolPassingYear,
                    InterPassingYear = model.InterPassingYear,
                    InterSchoolBoardName = model.InterSchoolBoardName,
                    InterSchoolMarks = model.InterSchoolMarks,
                    InterSchoolMediumId = model.InterSchoolMediumId,
                    PanNumber = model.PanNumber,
                    UniversityName = model.UniversityName,
                    S3key = model.S3key,
                    EmployeeTypeId = model.EmployeeTypeId
                    
                };
                context.TeacherMasters.Add(Employee);
                context.SaveChanges();
                if (Employee.EmployeeTypeId != 1) return 1;
                if (model.MappingList == null) return 1;
                foreach (var mapdetail in model.MappingList)
                {
                    var employeemapping = new TeacherSubjectMapping()
                    {
                        TeacherId = Employee.Id,
                        SectionId = mapdetail.SectionId,
                        ClassId = mapdetail.ClassId,
                        SubjectId = mapdetail.SubjectId
                    };
                    context.TeacherSubjectMappings.Add(employeemapping);
                    context.SaveChanges();
                }
            }
            return 1;
        }
        public static List<Common> GetMedium()
        {
            List<Common> medium;
            using (var context = new EduErpEntities())
            {
                medium = (from g in context.KeyWordMasters
                          where g.Name == "Medium"
                          select new Common
                          {
                              Id = g.KeyWordId,
                              Name = g.KeyWordName
                          }).ToList();

            }
            return medium;
        }

        public static List<EmployeeBo> GetEmployees(int schoolId)
        {
            var Employees = new List<EmployeeBo>();

            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                using (var dr = spdb.ExecuteReader("GetTeacherByID", CommandType.StoredProcedure))
                {
                    var drgetter = new DataReaderObjectLoader<EmployeeBo>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true);//use tool to load object from dr.row
                        Employees.Add(obj);
                    }
                }

            }
            return Employees;
        }

        public static List<Common> GetSubjects(int schoolId)
        {
            List<Common> subjects;
            using (var context = new EduErpEntities())
            {
                subjects = (from s in context.SubjectMasters
                    where s.Status == (int)UserStatus.Active && s.SchoolId == schoolId
                    select new Common
                    {
                        Id = s.Id,
                        Name=s.Name
                    }).ToList();
            }
            return subjects;
        }

        public static List<Common> GetEmployeeType(int schoolId)
        {
            var context = new EduErpEntities();
          var emplist=  (from emp in context.EmployeeTypeMasters
                where emp.SchoolId == schoolId && emp.StatusId==(int)UserStatus.Active
                select new Common
                {
                    Id = emp.Id,
                    Name = emp.EmployeeName
                }).ToList();
            return emplist;
        }

        public static bool UpdateEmployee(EmployeeBo model)
        {
            if (model.DateOfBirth == DateTime.Parse("1/1/0001"))
            {
                model.DateOfBirth = null;
            }
            if (model.DOJ == DateTime.Parse("1/1/0001"))
            {
                model.DOJ = null;
            }
            if (model.DateOfExit == DateTime.Parse("1/1/0001"))
            {
                model.DateOfExit = null;
            }
            using (var context = new EduErpEntities())
            {
                var Employee = context.TeacherMasters.FirstOrDefault(a => a.Id == model.Id);
                if (Employee == null) return false;
                Employee.PermanentStateId = model.PermanentStateId;
                Employee.CurrentStateId = model.CurrentStateId;
                Employee.MotherName = model.MotherName;
                Employee.FatherName = model.FatherName;
                Employee.PermanentPinCode = model.PermanentPinCode;
                Employee.PermanentAddress = model.PermanentAddress;
                Employee.CurrentAddress = model.CurrentAddress;
                Employee.FirstName = model.FirstName;
                Employee.MiddleName = model.MiddleName;
                Employee.LastName = model.LastName;
                Employee.MobileNumber = model.MobileNumber;
                Employee.Email = model.Email;
                Employee.GenderId = model.GenderId;
                Employee.DateOfBirth = model.DateOfBirth;
                Employee.TitleId = model.TitleId;
                Employee.RelegionId = model.RelegionId;
                Employee.CategoryId = model.CategoryId;
                Employee.NationalityId = model.NationalityId;
                Employee.DOJ = model.DOJ;
                Employee.DateOfExit = model.DateOfExit;
                Employee.CurrentPinCode = model.CurrentPinCode;
                Employee.ImagePath = model.ImagePath;
                Employee.LastUpdatedOn = model.LastUpdatedOn;
                Employee.LastUpdatedBy = model.LastUpdatedBy;
                Employee.PermanentCityId = model.PermanentCityId;
                Employee.CurrentCityId = model.CurrentCityId;
                Employee.StatusId = model.StatusId;
                Employee.ReasonForExit = model.ReasonForExit;
                Employee.SchoolId = model.SchoolId;
                Employee.UID = model.UID;
                Employee.Graduation = model.Graduation;
                Employee.GraduationMarks = model.GraduationMarks;
                Employee.GraduationPassingYear = model.GraduationPassingYear;
                Employee.HighSchoolBoardName = model.HighSchoolBoardName;
                Employee.HighSchoolMarks = model.HighSchoolMarks;
                Employee.HighSchoolMediumId = model.HighSchoolMediumId;
                Employee.HighschoolPassingYear = model.HighschoolPassingYear;
                Employee.InterPassingYear = model.InterPassingYear;
                Employee.InterSchoolBoardName = model.InterSchoolBoardName;
                Employee.InterSchoolMarks = model.InterSchoolMarks;
                Employee.InterSchoolMediumId = model.InterSchoolMediumId;
                Employee.PanNumber = model.PanNumber;
                Employee.UniversityName = model.UniversityName;
                Employee.S3key = model.S3key;
                Employee.EmployeeTypeId = model.EmployeeTypeId;
                Employee.EmployeeCode = model.EmployeeCode;
                context.SaveChanges();
                if (Employee.EmployeeTypeId != 1) return true;
                if (model.MappingList == null) return true;
                context.TeacherSubjectMappings.Where(a => a.TeacherId == Employee.Id)
                    .ToList()
                    .ForEach(a => context.TeacherSubjectMappings.Remove(a));
                context.SaveChanges();
                foreach (var mapdetail in model.MappingList)
                {
                    var employeemapping = new TeacherSubjectMapping
                    {
                        TeacherId = Employee.Id,
                        SectionId = mapdetail.SectionId,
                        ClassId = mapdetail.ClassId,
                        SubjectId = mapdetail.SubjectId
                    };
                    context.TeacherSubjectMappings.Add(employeemapping);
                    context.SaveChanges();
                }
                return true;
            }
        }
        public static EmployeeBo GetEmployee(int schoolId,int id)
        {
            var Employees = new List<EmployeeBo>();

            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                spdb.AddParameter("@Id", id);
                using (var dr = spdb.ExecuteReader("GetTeacher", CommandType.StoredProcedure))
                {
                    var drgetter = new DataReaderObjectLoader<EmployeeBo>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true);//use tool to load object from dr.row
                        Employees.Add(obj);
                    }
                }

            }
            return Employees.FirstOrDefault();
        }

        public static List<Mapping> GetMappingList(int id)
        {
            List<Mapping> mappings;
            using (var context=new EduErpEntities())
            {
                mappings = (from mp in context.TeacherSubjectMappings
                    join sub in context.SubjectMasters on mp.SubjectId equals sub.Id
                    join clas in context.ClassMasters on mp.ClassId equals clas.Id
                    join sec in context.SectionMasters on mp.SectionId equals sec.Id
                    where mp.TeacherId == id 
                    select new Mapping
                    {
                        ClassId = mp.ClassId,
                        SubjectId = mp.SubjectId,
                        SectionId = mp.SectionId,
                        SectionName = sec.Name,
                        ClassName = clas.Name,
                        SubjectName = sub.Name
                    }).ToList();
            }
            return mappings;
        }

        public static bool UpdateStatus(int id, int statusId, int userId)
        {
            using (var context = new EduErpEntities())
            {
                var Employee = context.TeacherMasters.SingleOrDefault(a => a.Id == id);
                if (Employee == null) return false;
                Employee.StatusId = statusId;
                Employee.LastUpdatedOn = DateTime.Now;
                Employee.LastUpdatedBy = userId;
                context.SaveChanges();
                return true;
            }

        }


        public static bool UpdateProfilePic(int id, Guid? s3Key)
        {
            using (var context = new EduErpEntities())
            {
                var modelUser = context.TeacherMasters.FirstOrDefault(a => a.Id == id);
                if (modelUser == null) return false;
                modelUser.S3key = s3Key;
                context.SaveChanges();
                return true;
            }
        }
    }
}
