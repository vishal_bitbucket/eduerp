﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using EduErp.BO;
using EduErp.DAL.Utilities;
using EduErp.Model;
using EduErp.Mvc.Utilities;
using CityMaster = EduErp.BO.CityMaster;
using Student = EduErp.Model.Student;
using StudentClassMapping = EduErp.Model.StudentClassMapping;

namespace EduErp.DAL
{
    public class StudentModule
    {

        public static int AddStudent(StudentBo model)
        {
            using (var context = new EduErpEntities())
            {
                var result = context.StudentClassMappings.Where(p => p.StudentCode == model.StudentCode).FirstOrDefault();
                if (result != null)
                {
                    return -2;
                }
                var usedStudent = MaxStudentCount(model.SchoolId);
                var leftStudent = LeftStudent(model.SchoolId);
                if (leftStudent ==0)
                {
                    return -1;
                }
                if (leftStudent >= 1)
                {
                    var updatedLeftStudent = leftStudent - 1;
                    var accountDetail = context.AccountDetails.FirstOrDefault(a => a.SchoolId == model.SchoolId);
                    if (accountDetail != null)
                    {
                        accountDetail.LeftStudentCount = updatedLeftStudent;
                        context.SaveChanges();
                    }
                }
                var student = new Student
                {
                    // StudentId = model.StudentId,
                    PermanentStateId = model.PermanentStateId,
                    CurrentStateId = model.CurrentStateId,
                    MotherName = model.MotherName,
                    FatherName = model.FatherName,
                    PermanentPinCode = model.PermanentPinCode,
                    PermanentAddress = model.PermanentAddress,
                    CorrAddress = model.CuurentAddress,
                    FirstName = model.FirstName,
                    RegistrationNumber=model.RegistrationNumber,
                    MiddileName = model.MiddileName,
                    LastName = model.LastName,
                    StudentMobile = model.StudentMobile,
                    ParentMobileNo = model.ParentMobileNumber,
                    ParentContactNo = model.ParentContactNo,
                    Email = model.Email,
                    GenderId = model.GenderId,
                    DOB = model.Dob,
                    TitleId = model.TitleId,
                    RelegionId = model.RelegionId,
                    CategoryId = model.CategoryId,
                    NationlityId = model.NationlityId,
                    DOJ = model.Doj,
                    DOE = model.Doe,
                    CorrPinCode = model.CurrentPinCode,
                    ImagePath = model.ImagePath,
                    CreatedOn = model.CreatedOn,
                    CreatedBy = model.CreatedBy,
                    PermanentCityId = model.PermanentCityId,
                    CorrCityId = model.CurrentCityId,
                    StatusId = model.StatusId,
                    ReasonForExit = model.ReasonForExit,
                    SchoolId = model.SchoolId,
                    Uid = model.Uid,
                    GuardianAddress = model.GuardianAddress,
                    GuardianOccupation = model.GuardianOccupation,
                    GuardianMobileNo = model.GuardianMobile,
                    GuardianName = model.GuardianName,
                    GuardianRelationWithStudent = model.GuardianRelation,
                    S3key = model.S3key
                };

                var studentMapping = new StudentClassMapping
                {
                    SessionId = model.SessionId,
                    SectionId = model.SectionId,
                    ClassId = model.ClassId,
                    StudentCode = model.StudentCode
                };
                context.Students.Add(student);
                context.StudentClassMappings.Add(studentMapping);
                context.SaveChanges();


            }
            return 1;
        }
        public static int MaxStudentCount(int schoolId)
        {
            using (var context = new EduErpEntities())
            {
                var maxStudentCount = (from spm in context.SchoolPlanMappings
                                    join pm in context.PlanMasters on spm.PlanId equals pm.Id
                                    where spm.SchoolId == schoolId
                                    select pm.MaxStudentLimit ?? 0).FirstOrDefault();
                return maxStudentCount;
            }
        }

        public static int LeftStudent(int schoolId)
        {
            using (var context = new EduErpEntities())
            {
                var leftStudentCount = (from ad in context.AccountDetails where ad.SchoolId == schoolId select ad.LeftStudentCount).FirstOrDefault();
                return leftStudentCount ?? 0;
            }

        }

        public static List<StudentFilterList> GetStudentList(int classId, int sectionId, int statusId, int sessionId, int schoolId)
        {
            var students = new List<StudentFilterList>();

            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@SessionId", sessionId);
                spdb.AddParameter("@ClassId", classId);
                spdb.AddParameter("@SectionId", sectionId);
                spdb.AddParameter("@SchoolId", schoolId);
                spdb.AddParameter("@StatusId", statusId);
                using (var dr = spdb.ExecuteReader("GetStudentsForAllSchool", CommandType.StoredProcedure))
                {
                    var drgetter = new DataReaderObjectLoader<StudentFilterList>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true);//use tool to load object from dr.row
                        students.Add(obj);
                    }
                }
                                
            }
            return students;
        }

        public static List<StudentFilterList> GetStudentListForReport(int classId, int sectionId, int statusId, int admsnstatusId, int sessionId, int schoolId)
        {
            var students = new List<StudentFilterList>();

            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@SessionId", sessionId);
                spdb.AddParameter("@ClassId", classId);
                spdb.AddParameter("@SectionId", sectionId);
                spdb.AddParameter("@SchoolId", schoolId);
                spdb.AddParameter("@StatusId", statusId);
                spdb.AddParameter("@AddmissionStatus", admsnstatusId);
                using (var dr = spdb.ExecuteReader("GetStudentsForAllSchoolForReport", CommandType.StoredProcedure))
                {
                    var drgetter = new DataReaderObjectLoader<StudentFilterList>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true);//use tool to load object from dr.row
                        students.Add(obj);
                    }
                }

            }
            return students;
        }
        public static List<StudentBo> GetStudents(int schoolId)
        {
            var students = new List<StudentBo>();

            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();

                spdb.AddParameter("@SchoolId", schoolId);
                using (var dr = spdb.ExecuteReader("GetStudentsList", CommandType.StoredProcedure))
                {
                    var drgetter = new DataReaderObjectLoader<StudentBo>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true);//use tool to load object from dr.row
                        students.Add(obj);
                    }
                }

            }
            return students;
        }

        public static StudentProfileBo GetStudentFeetransctionForProfile( int fromMonth,int toMonth,int studentId,int sessionId)
        {
            var transactions = new List<FeetransactionDetailList>();
            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@FromMonth", fromMonth);
                spdb.AddParameter("@ToMonth", toMonth);
                spdb.AddParameter("@StudentId", studentId);
                spdb.AddParameter("@SessionId", sessionId);
                using (var dr = spdb.ExecuteReader("GetFeeTransactionByStudentIdForProfile", CommandType.StoredProcedure))
                {
                    var drgetter = new DataReaderObjectLoader<FeetransactionDetailList>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true);//use tool to load object from dr.row
                        transactions.Add(obj);
                    }
                }
                var result = new StudentProfileBo
                {
                    FeetransactionDetail= transactions

                };
                return result;
            }
        
        }
        public static StudentProfileBo GetStudentAttendanceForProfile(int fromMonth, int toMonth, int studentId, int SessionId)
        {
            var attendance = new List<AttendanceDetailList>();
            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@FromMonth", fromMonth);
                spdb.AddParameter("@ToMonth", toMonth);
                spdb.AddParameter("@StudentId", studentId);
                spdb.AddParameter("@SessionId", SessionId);
                using (var dr = spdb.ExecuteReader("GetAttendanceByStudentIdForProfile", CommandType.StoredProcedure))
                {
                    var drgetter = new DataReaderObjectLoader<AttendanceDetailList>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true);//use tool to load object from dr.row
                        attendance.Add(obj);
                    }
                }
                var result = new StudentProfileBo
                {
                    AttendanceDetail = attendance

                };
                return result;
            }

        }
        public static StudentProfileBo GetStudentsDetail(int studentId)
        {
            var students = new List<StudentProfileBo>();

            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@StudentId", studentId);
                using (var dr = spdb.ExecuteReader("GetStudentDetailForProfile", CommandType.StoredProcedure))
                {
                    var drgetter = new DataReaderObjectLoader<StudentProfileBo>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true);//use tool to load object from dr.row
                        students.Add(obj);
                    }
                }

            }
            return students.FirstOrDefault();
        }
        public static bool UpdateStatus(int id, int status, int userId)
        {
            using (var context = new EduErpEntities())
            {
                var student = context.Students.FirstOrDefault(a => a.Id == id);
                if (student == null) return false;
                student.StatusId = status;
                student.DOE = null;
                student.ReasonForExit = null;
                student.LastUpdatedOn = DateTime.Now;
                student.LastUpdatedBy = userId;
                context.SaveChanges();
                return true;
            }
        }
        public static bool UpdateStudentStatus(StudentBo model,int userId)
        {
            using (var context = new EduErpEntities())
            {
                var subject = context.Students.FirstOrDefault(a => a.Id == model.Id);
                if (subject == null) return false;
                subject.StatusId = model.StatusId;
                subject.DOE = model.Doe;
                subject.ReasonForExit = model.ReasonForExit;
                subject.LastUpdatedOn = DateTime.Now;
                subject.LastUpdatedBy =userId;
                context.SaveChanges();
                return true;
            }
        }


        public static StudentBo GetStudentById(int id, int sessionid)
        {
            StudentBo student;
            using (var context = new EduErpEntities())
            {
                student = (from s in context.Students
                           join spm in context.StudentClassMappings on s.Id equals spm.StudentId
                           where s.Id == id && spm.SessionId == SessionItems.SessionId
                           select new StudentBo
                           {
                               Id = s.Id,
                               PermanentStateId = s.PermanentStateId ?? 0,
                               CurrentStateId = s.CurrentStateId ?? 0,
                               // StudentId = s.StudentId,
                               MotherName = s.MotherName,
                               FatherName = s.FatherName,
                               RegistrationNumber = s.RegistrationNumber??0,
                               PermanentPinCode = s.PermanentPinCode,
                               CurrentPinCode = s.CorrPinCode,
                               PermanentAddress = s.PermanentAddress,
                               CuurentAddress = s.CorrAddress,
                               FirstName = s.FirstName,
                               MiddileName = s.MiddileName,
                               LastName = s.LastName,
                               ClassId = spm.ClassId,
                               SessionId = spm.SessionId,
                               SectionId = spm.SectionId,
                               StudentCode = spm.StudentCode,
                               MappingId = spm.Id,
                              StudentMobile = s.StudentMobile,
                               ParentMobileNumber = s.ParentMobileNo,
                               ParentContactNo = s.ParentContactNo,
                               Email = s.Email,
                               GenderId = s.GenderId,
                               // Gender=s.GenderId.ToString(),
                               Dob = s.DOB,
                               Uid = s.Uid,
                               TitleId = s.TitleId,
                               RelegionId = s.RelegionId,
                               CategoryId = s.CategoryId,
                               NationlityId = s.NationlityId,
                               Doj = s.DOJ,
                               Doe = s.DOE,
                               ImagePath = s.ImagePath,
                               CreatedOn = s.CreatedOn,
                               CreatedBy = s.CreatedBy,
                               CurrentCityId = s.CorrCityId ?? 0,
                               PermanentCityId = s.PermanentCityId ?? 0,
                               StatusId = s.StatusId,
                               ReasonForExit = s.ReasonForExit,
                               SchoolId = s.SchoolId,
                               GuardianAddress = s.GuardianAddress,
                               GuardianOccupation = s.GuardianOccupation,
                               GuardianMobile = s.GuardianMobileNo,
                               GuardianName = s.GuardianName,
                               GuardianRelation = s.GuardianRelationWithStudent,
                               S3key = s.S3key ?? Guid.Empty
                           }).FirstOrDefault();

                student.StudentAcademicRecordList = (from s in context.Students
                                                     join spm in context.StudentClassMappings on s.Id equals spm.StudentId
                                                     join session in context.SessionMasters on spm.SessionId equals session.Id
                                                     join c in context.ClassMasters on spm.ClassId equals c.Id
                                                     join section in context.SectionMasters on spm.SectionId equals section.Id
                                                     where s.Id == id
                                                     select new StudentAcademicRecord
                                                     {
                                                         StudentClassMappingId = spm.Id,
                                                         ClassId = spm.ClassId,
                                                         SessionId = spm.SessionId,
                                                         SectionId = spm.SectionId,
                                                         SectionName = section.Name,
                                                         ClassName = c.Name,
                                                         SessionName = session.Name
                                                     }).OrderByDescending(a => a.StudentClassMappingId).ToList();
            }

           
            return student;
        }

        public static bool UpdateStudent(StudentBo model)
        {
            using (var context = new EduErpEntities())
            {
                var student = context.Students.FirstOrDefault(a => a.Id == model.Id);
                if (student != null)
                {
                    student.GenderId = model.GenderId;
                    student.FirstName = model.FirstName;
                    student.MiddileName = model.MiddileName;
                    student.RegistrationNumber = model.RegistrationNumber;
                    student.LastName = model.LastName;
                    student.FatherName = model.FatherName;
                    student.MotherName = model.MotherName;
                    student.StudentMobile = model.StudentMobile;
                   // student.Mobile2 = model.Mobile2;
                    student.NationlityId = model.NationlityId;
                   // student.PinCode = model.PinCode;
                    student.ReasonForExit = model.ReasonForExit;
                    student.RelegionId = model.RelegionId;
                    // student.StudentId = model.StudentId;
                    student.TitleId = model.TitleId;
                    student.StatusId = model.StatusId;
                    student.Email = model.Email;
                    student.DOB = model.Dob;
                    student.DOE = model.Doe;
                    student.DOJ = model.Doj;
                    student.CurrentStateId = model.CurrentStateId;
                    student.PermanentStateId = model.PermanentStateId;
                    student.LastUpdatedBy = model.LastUpdatedBy;
                    student.LastUpdatedOn = model.LastUpdatedOn;
                    student.SchoolId = model.SchoolId;
                   // student.Address1 = model.Address1;
                   // student.Address2 = model.Address2;
                    student.CategoryId = model.CategoryId;
                    //student.ContactNo = model.ContactNo;
                    student.ImagePath = model.ImagePath;
                    student.Uid = model.Uid;
                    student.CorrPinCode = model.CurrentPinCode;
                    student.PermanentPinCode = model.PermanentPinCode;
                    student.CorrCityId = model.CurrentCityId;
                    student.PermanentCityId = model.PermanentCityId;
                    student.GuardianAddress = model.GuardianAddress;
                    student.GuardianOccupation = model.GuardianOccupation;
                    student.GuardianMobileNo = model.GuardianMobile;
                    student.GuardianName = model.GuardianName;
                    student.GuardianRelationWithStudent = model.GuardianRelation;
                    context.SaveChanges();
                    
                }
                var studentmap = context.StudentClassMappings.FirstOrDefault(a => a.Id == model.MappingId);
                if (studentmap != null)
                {
                    studentmap.SectionId = model.SectionId;
                    studentmap.SessionId = model.SessionId;
                    studentmap.ClassId = model.ClassId;
                    studentmap.StudentCode = model.StudentCode;
                    context.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public static List<CityMaster> GetCities(int stateid)
        {
            List<CityMaster> cities;
            using (var context = new EduErpEntities())
            {

                cities = (from c in context.CityMasters
                          where c.StateId == stateid
                          select new CityMaster
                          {
                              Id = c.Id,
                              Name = c.Name
                          }).ToList();
            }
            return cities;
        }


        public static List<Common> GetStatus()
        {
            List<Common> gender;
            using (var context = new EduErpEntities())
            {
                gender = (from g in context.KeyWordMasters
                          where g.Name == "Status" where g.KeyWordId !=(int)UserStatus.Deleted
                          select new Common
                          {
                              Id = g.KeyWordId,
                              Name = g.KeyWordName
                          }).ToList();

            }
            return gender;
        }
        public static List<Common> GetGender()
        {
            List<Common> gender;
            using (var context = new EduErpEntities())
            {
                gender = (from g in context.KeyWordMasters
                          where g.Name == "Gender"
                          select new Common
                          {
                              Id = g.KeyWordId,
                              Name = g.KeyWordName
                          }).ToList();

            }
            return gender;
        }

        public static List<Common> GetTitle()
        {
            List<Common> title;
            using (var context = new EduErpEntities())
            {
                title = (from g in context.KeyWordMasters
                         where g.Name == "Title"
                         select new Common
                         {
                             Id = g.KeyWordId,
                             Name = g.KeyWordName
                         }).ToList();

            }
            return title;
        }

       
        public static List<Common> GetCategory()
        {
            List<Common> category;
            using (var context = new EduErpEntities())
            {
                category = (from g in context.KeyWordMasters
                            where g.Name == "Category"
                            select new Common
                            {
                                Id = g.KeyWordId,
                                Name = g.KeyWordName
                            }).ToList();

            }
            return category;
        }

        public static List<Common> GetRelegion()
        {
            List<Common> relegion;
            using (var context = new EduErpEntities())
            {
                relegion = (from g in context.KeyWordMasters
                            where g.Name == "Relegion"
                            select new Common
                            {
                                Id = g.KeyWordId,
                                Name = g.KeyWordName
                            }).ToList();

            }
            return relegion;
        }

        public static List<Common> GetNationality()
        {
            List<Common> nationality;
            using (var context = new EduErpEntities())
            {
                nationality = (from g in context.KeyWordMasters
                               where g.Name == "Nationlity"
                               select new Common
                               {
                                   Id = g.KeyWordId,
                                   Name = g.KeyWordName
                               }).ToList();

            }
            return nationality;
        }

        public static List<Common> GetClasses(int schoolId)
        {
            List<Common> classes;
            using (var context = new EduErpEntities())
            {
                classes = (from c in context.ClassMasters
                           where c.SchoolId == schoolId && c.Status == (int)UserStatus.Active
                           select new Common
                           {
                               Id = c.Id,
                               Name = c.Name
                        }).ToList();
            }
            return classes;
        }

        public static List<Common> GetSessions(int schoolId)
        {
            List<Common> sessions;
            using (var context = new EduErpEntities())
            {
                sessions = (from c in context.SessionMasters
                            where c.SchoolId == schoolId && c.StatusId == (int)UserStatus.Active
                            select new Common
                            {
                                Id = c.Id,
                                Name = c.Name
                            }).ToList();
            }
            return sessions;
        }

        public static List<Common> GetSections(int classId)
        {
            List<Common> sections;
            using (var context = new EduErpEntities())
            {
                sections = (from c in context.SectionMasters
                            where c.Status == (int)UserStatus.Active && c.ClassId == classId
                            select new Common
                            {
                                Id = c.Id,
                                Name = c.Name
                            }).ToList();
            }
            return sections;
        }

        public static bool UpdateProfilePic(int id, Guid? s3Key)
        {
            using (var context = new EduErpEntities())
            {
                var modelUser = context.Students.FirstOrDefault(a => a.Id == id);
                if (modelUser == null) return false;
                modelUser.S3key = s3Key;                
                context.SaveChanges();
                return true;
            }
        }
    }
}
