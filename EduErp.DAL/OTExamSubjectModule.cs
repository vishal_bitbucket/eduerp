﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EduErp.BO;
using EduErp.Model;
namespace EduErp.DAL
{
   public class OTExamSubjectModule
    {
       public static List<Common> GetExams(int schoolId)
       {
           List<Common> exams;
           using (var context = new EduErpEntities())
           {
               exams = (from s in context.OnlineTestMasters
                        where s.StatusId == (int)UserStatus.Active && s.SchoolId == schoolId
                        select new Common
                        {
                            Id = s.Id,
                            Name = s.Name
                        }).ToList();
           }
           return exams;
       }
       public static List<Common> GetSubjects(int schoolId)
       {
           List<Common> subjects;
           using (var context = new EduErpEntities())
           {
               subjects = (from s in context.OnlineTestSubjectMasters
                           where s.StatusId == (int)UserStatus.Active && s.SchoolId == schoolId
                           select new Common
                           {
                               Id = s.Id,
                               Name = s.Name
                           }).ToList();
           }
           return subjects;
       }
       public static int AddExamSubject(BO.OTExamSubjectMapping ExamSubject)
       {
           using (var context = new EduErpEntities())
           {
               foreach (var item in ExamSubject.ExamSubjectMappingLists)
               {
                   var model = new Model.OTExamSubjectMapping()
                   {

                       ExamId = ExamSubject.ExamId,

                       SubjectId = item.SubjectId,
                       TimeInMinutes = item.TimeInMinutes,
                       Sequence = item.Sequence,
                       CreatedOn = ExamSubject.CreatedOn,
                       CreatedBy = ExamSubject.CreatedBy,
                       SchoolId = ExamSubject.SchoolId,
                       StatusId = ExamSubject.StatusId,
                       LastUpdatedBy = ExamSubject.LastUpdatedBy,
                       LastUpdatedOn = ExamSubject.LastUpdatedOn,


                   };

                   context.OTExamSubjectMappings.Add(model);
                   context.SaveChanges();

               }

               return 1;
           }
       }
       public static List<BO.OTExamSubjectMapping> GetExamSubject(int schoolId)
       {


           List<BO.OTExamSubjectMapping> onlin;
           using (var context = new EduErpEntities())
           {
               onlin = (from s in context.OTExamSubjectMappings
                        where s.StatusId != (int)UserStatus.Deleted && (s.SchoolId == schoolId || s.SchoolId == 0)
                        join sm in context.OnlineTestSubjectMasters on s.SubjectId equals sm.Id 
                        join otm in context.OnlineTestMasters on s.ExamId equals otm.Id
                        select new BO.OTExamSubjectMapping
                        {
                            Id = s.Id,
                            SchoolId = s.SchoolId,
                            Sequence = s.Sequence,
                            TimeInMinutes = s.TimeInMinutes,
                           SubjectName = sm.Name,
                           ExamName = otm.Name,
                            StatusId = s.StatusId
                        }).ToList();
           }
           return onlin;
       }
       public static bool UpdateStatus(int id, int statusId, int userId)
       {
           using (var context = new EduErpEntities())
           {
               var onlin = context.OTExamSubjectMappings.FirstOrDefault(a => a.Id == id);
               if (onlin == null) return false;
               onlin.StatusId = statusId;
               onlin.LastUpdatedOn = DateTime.Now;
               onlin.LastUpdatedBy = userId;
               context.SaveChanges();
               return true;

           }


       }
       public static BO.OTExamSubjectMapping GetOTExamSubjectById(int id)
       {
          BO.OTExamSubjectMapping onlin;
           using (var context = new EduErpEntities())
           {
               onlin = (from o in context.OTExamSubjectMappings
                        where id == o.Id
                        join sm in context.OnlineTestSubjectMasters on o.SubjectId equals sm.Id
                        join otm in context.OnlineTestMasters on o.ExamId equals otm.Id
                        select new BO.OTExamSubjectMapping()
                        {
                            SubjectId = o.SubjectId,
                            SubjectName = sm.Name,
                            ExamId = o.ExamId,
                            ExamName = otm.Name,
                            TimeInMinutes = o.TimeInMinutes,
                            Sequence = o.Sequence,
                            SchoolId = o.SchoolId,
                            StatusId = o.StatusId
                        }).FirstOrDefault();
           }
           return onlin;
       }
       public static bool UpdateExamSubject(BO.OTExamSubjectMapping model)
       {

           using (var context = new EduErpEntities())
           {
               var onlin = context.OTExamSubjectMappings.SingleOrDefault(a => a.Id == model.Id);

               if (onlin != null)
               {
                   onlin.Id = model.Id;
                   onlin.SubjectId = model.SubjectId;
                  onlin.Sequence = model.Sequence;
                   onlin.TimeInMinutes = model.TimeInMinutes;
                   onlin.ExamId = model.ExamId;
                   onlin.LastUpdatedBy = model.LastUpdatedBy;
                   onlin.LastUpdatedOn = model.LastUpdatedOn;
                   onlin.StatusId = model.StatusId;
                   onlin.SchoolId = model.SchoolId;
                   context.SaveChanges();
                   return true;
               }
               return false;
           }
       }
       public static List<ExamSubjectList> GetExamSubjectMappingDetail(int examId,int schoolId)
       {
           List<BO.ExamSubjectList> onlin;
           using (var context = new EduErpEntities())
           {
               onlin = (from o in context.OTExamSubjectMappings
                        where examId == o.ExamId && o.StatusId == (int)UserStatus.Active
                        join sm in context.OnlineTestSubjectMasters on o.SubjectId equals sm.Id
                       // join otm in context.OnlineTestMasters on o.ExamId equals otm.Id
                        select new BO.ExamSubjectList()
                        {
                            //SubjectId = o.SubjectId,
                            SubjectName = sm.Name,
                           // ExamId = o.ExamId,
                            //ExamName = otm.Name,
                            TimeInMinutes = o.TimeInMinutes,
                            Sequence = o.Sequence,
                           // SchoolId = o.SchoolId,
                            //StatusId = o.StatusId
                        }).ToList();
           }
           return onlin;
       }
    }
}
