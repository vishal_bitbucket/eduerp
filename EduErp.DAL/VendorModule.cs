﻿using System;
using System.Collections.Generic;
using System.Linq;
using EduErp.BO;
using EduErp.Model;
using VendorMaster = EduErp.Model.VendorMaster;

namespace EduErp.DAL
{
    public class VendorModule
    {
        public static bool AddVendor(VendorBo model)
        {
            using (var context = new EduErpEntities())
            {
                var item = new VendorMaster
                {
                    Name = model.Name,
                    StatusId = model.StatusId,
                    StateId = model.StateId,
                    CityId = model.CityId,
                    Address = model.Address,
                    PinCode = model.PinCode,
                    ContactNumber = model.ContactNumber,
                    TypeId = model.TypeId,
                    SchoolId = model.SchoolId,
                    CreatedBy = model.CreatedBy,
                    CreatedOn = model.CreatedOn
                };
                context.VendorMasters.Add(item);
                context.SaveChanges();
            }
            return true;
        }

        public static List<VendorBo> GetVendors(int schoolId)
        {
            var context = new EduErpEntities();
            var vendorList = (from item in context.VendorMasters
                              join sch in context.SchoolMasters on item.SchoolId equals sch.Id
                              where item.SchoolId == schoolId && item.StatusId != (int)UserStatus.Deleted
                              select new VendorBo
                              {
                                  Id = item.Id,
                                  Name = item.Name,
                                  TypeId = item.TypeId,
                                  ContactNumber = item.ContactNumber,
                                  StatusId = item.StatusId,
                                  SchoolName = sch.Name,
                                  SchoolId = item.SchoolId

                              }).ToList();
            return vendorList;
        }

        public static VendorBo GetVendor(int id)
        {
            var context = new EduErpEntities();
            var vendor = (from itemms in context.VendorMasters
                          where itemms.Id == id && itemms.StatusId != (int)UserStatus.Deleted
                          select new VendorBo
                          {
                              Id = itemms.Id,
                              Name = itemms.Name,
                              TypeId = itemms.TypeId,
                              StateId = itemms.StateId,
                              CityId = itemms.CityId,
                              PinCode = itemms.PinCode,
                              Address = itemms.Address,
                              ContactNumber = itemms.ContactNumber,
                              StatusId = itemms.StatusId,
                              SchoolId = itemms.SchoolId

                          }).FirstOrDefault();
            return vendor;
        }
        public static bool UpdateVendor(VendorBo model)
        {
            using (var context = new EduErpEntities())
            {
                var vendor = context.VendorMasters.FirstOrDefault(a => a.Id == model.Id);
                if (vendor == null) return true;
                vendor.Name = model.Name;
                vendor.TypeId = model.TypeId;
                vendor.SchoolId = model.SchoolId;
                vendor.StateId = model.StateId;
                vendor.CityId = model.CityId;
                vendor.Address = model.Address;
                vendor.PinCode = model.PinCode;
                vendor.ContactNumber = model.ContactNumber;
                vendor.LastUpdatedBy = model.LastUpdatedBy;
                vendor.LastUpdatedOn = model.LastUpdatedOn;
                context.SaveChanges();
            }
            return true;
        }
        public static bool UpdateStatus(int id, int statusId, int userId)
        {
            using (var context = new EduErpEntities())
            {
                var vender = context.VendorMasters.SingleOrDefault(a => a.Id == id);
                if (vender == null) return false;
                vender.StatusId = statusId;
                vender.LastUpdatedOn = DateTime.Now;
                vender.LastUpdatedBy = userId;
                context.SaveChanges();
                return true;
            }

        }
    }
}
