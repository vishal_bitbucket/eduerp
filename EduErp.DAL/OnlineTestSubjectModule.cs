﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EduErp.BO;
using EduErp.Model;

namespace EduErp.DAL
{
   public class OnlineTestSubjectModule
    {
       public static List<OnlineExamTestSubjectBo> GetOnlineTestSubject(int SchoolId)
       {


           List<OnlineExamTestSubjectBo> onlin;
           using (var context = new EduErpEntities())
           {
               onlin = (from s in context.OnlineTestSubjectMasters 
                        where s.StatusId != (int)UserStatus.Deleted && (s.SchoolId == SchoolId || s.SchoolId == 0)

                        select new OnlineExamTestSubjectBo
                        {
                            Id = s.Id,
                            SchoolId = s.SchoolId ?? 0,
                           SubjectName = s.Name,
                            Status = s.StatusId
                        }).ToList();
           }
           return onlin;
       }
       public static int AddOnlineTestSubject(OnlineExamTestSubjectBo OnlineUser)
       {
           using (var context = new EduErpEntities())
           {
               var result = context.OnlineTestSubjectMasters.FirstOrDefault(a => a.StatusId != (int)UserStatus.Deleted && a.Name.ToLower().Equals(OnlineUser.SubjectName.ToLower()) && a.SchoolId == OnlineUser.SchoolId);
               if (result != null)
               {
                   return -1;
               }
               var model = new Model.OnlineTestSubjectMaster()
               {
                   Name = OnlineUser.SubjectName,
                   CreatedOn = OnlineUser.CreatedOn,
                   CreatedBy = OnlineUser.CreatedBy,
                   SchoolId = OnlineUser.SchoolId,
                   StatusId = OnlineUser.Status,
                  
           
               };
               context.OnlineTestSubjectMasters.Add(model);
               context.SaveChanges();
               return model.Id;
           }
       }
       public static bool UpdateStatus(int id, int status, int userId)
       {
           using (var context = new EduErpEntities())
           {
               var onlin = context.OnlineTestSubjectMasters.FirstOrDefault(a => a.Id == id);
               if (onlin == null) return false;
               onlin.StatusId = status;
               onlin.LastUpdatedOn = DateTime.Now;
               onlin.LastUpdatedBy = userId;
               context.SaveChanges();
               return true;

           }


       }
       public static OnlineExamTestSubjectBo GetOnlineTestSubjectById(int id)
       {
           OnlineExamTestSubjectBo onlin;
           using (var context = new EduErpEntities())
           {
               onlin = (from o in context.OnlineTestSubjectMasters
                        where id == o.Id

                        select new OnlineExamTestSubjectBo()
                        {

                            SubjectName = o.Name,
                           
                            SchoolId = o.SchoolId ?? 0,
                            Status = o.StatusId
                        }).FirstOrDefault();
           }
           return onlin;
       }
       public static bool UpdateOnlineTestSubject(OnlineExamTestSubjectBo model)
       {

           using (var context = new EduErpEntities())
           {
               var onlin = context.OnlineTestSubjectMasters.SingleOrDefault(a => a.Id == model.Id);

               if (onlin != null)
               {
                   onlin.Id = model.Id;
                   onlin.Name = model.SubjectName;
                   
                   onlin.LastUpdatedBy = model.LastUpdatedBy;
                   onlin.LastUpdatedOn = model.LastUpdatedOn;
                   onlin.StatusId = model.Status;
                   onlin.SchoolId = model.SchoolId;
                   context.SaveChanges();
                   return true;
               }
               return false;
           }
       }
    }
}
