﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;

namespace EduErp.DAL.Utilities
{
    public class SpdbHelper : IDisposable
    {
       

        #region # Public Members #

        public string StrConnectionString { get; set; }
        public bool HandleErrors { get; set; }
        public string LastError { get; private set; }
        public bool LogErrors { get; set; }
        //public string LogFile { get; set; }

        #endregion

        #region # Private Menbers #

        private readonly DbConnection _objConnection;
        private readonly DbCommand _objCommand;
        private readonly DbProviderFactory _objFactory;

        #endregion

        #region # Constructer #

        public SpdbHelper(string connectionstring, Providers provider)
        {
            StrConnectionString = connectionstring;
            switch (provider)
            {
                case Providers.SqlServer:
                    _objFactory = SqlClientFactory.Instance;
                    break;
            }
            _objConnection = _objFactory.CreateConnection();
            _objCommand = _objFactory.CreateCommand();

            if (_objConnection == null) return;
            _objConnection.ConnectionString = StrConnectionString;
            if (_objCommand != null) _objCommand.Connection = _objConnection;
        }

        public SpdbHelper(string connectionstring)
            : this(connectionstring, Providers.SqlServer)
        {
        }

        #endregion


        public int AddParameter(string name, object value)
        {
            var p = _objFactory.CreateParameter();
            if (p == null) return 0;
            p.ParameterName = name;
            p.Value = value;
            return _objCommand.Parameters.Add(p);
        }

        public void ClearParameters()
        {
            _objCommand.Parameters.Clear();
        }

        public void BeginTransaction()
        {
            if (_objConnection.State == System.Data.ConnectionState.Closed)
            {
                _objConnection.Open();
            }
            _objCommand.Transaction = _objConnection.BeginTransaction();
        }

        public void CommitTransaction()
        {
            _objCommand.Transaction.Commit();
            _objConnection.Close();
        }

        public void RollbackTransaction()
        {
            _objCommand.Transaction.Rollback();
            _objConnection.Close();
        }

        #region # EXECUTENONQUERY #

        public int ExecuteNonQuery(string query)
        {
            return ExecuteNonQuery(query, CommandType.Text, ConnectionState.CloseOnExit);
        }

        public int ExecuteNonQuery(string query, CommandType commandtype)
        {
            return ExecuteNonQuery(query, commandtype, ConnectionState.CloseOnExit);
        }

        public int ExecuteNonQuery(string query, ConnectionState connectionstate)
        {
            return ExecuteNonQuery(query, CommandType.Text, connectionstate);
        }

        public int ExecuteNonQuery(string query, CommandType commandtype, ConnectionState connectionstate)
        {
            _objCommand.CommandText = query;
            _objCommand.CommandType = commandtype;
            int i = -1;
            try
            {
                if (_objConnection.State == System.Data.ConnectionState.Closed)
                {
                    _objConnection.Open();
                }
                i = _objCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                HandleExceptions(ex);
            }
            finally
            {
                _objCommand.Parameters.Clear();
                if (connectionstate == ConnectionState.CloseOnExit)
                {
                    _objConnection.Close();
                }
            }

            return i;
        }

        #endregion

        #region # EXECUTESCALAR #

        public object ExecuteScalar(string query)
        {
            return ExecuteScalar(query, CommandType.Text, ConnectionState.CloseOnExit);
        }

        public object ExecuteScalar(string query, CommandType commandtype)
        {
            return ExecuteScalar(query, commandtype, ConnectionState.CloseOnExit);
        }

        public object ExecuteScalar(string query, ConnectionState connectionstate)
        {
            return ExecuteScalar(query, CommandType.Text, connectionstate);
        }

        public object ExecuteScalar(string query, CommandType commandtype, ConnectionState connectionstate)
        {
            _objCommand.CommandText = query;
            _objCommand.CommandType = commandtype;
            object o = null;
            try
            {
                if (_objConnection.State == System.Data.ConnectionState.Closed)
                {
                    _objConnection.Open();
                }
                o = _objCommand.ExecuteScalar();
            }
            catch (Exception ex)
            {
                HandleExceptions(ex);
            }
            finally
            {
                _objCommand.Parameters.Clear();
                if (connectionstate == ConnectionState.CloseOnExit)
                {
                    _objConnection.Close();
                }
            }

            return o;
        }

        #endregion

        #region # EXECUTEREADER #

        public DbDataReader ExecuteReader(string query)
        {
            return ExecuteReader(query, CommandType.Text, ConnectionState.CloseOnExit);
        }

        public DbDataReader ExecuteReader(string query, CommandType commandtype)
        {
            return ExecuteReader(query, commandtype, ConnectionState.CloseOnExit);
        }

        public DbDataReader ExecuteReader(string query, ConnectionState connectionstate)
        {
            return ExecuteReader(query, CommandType.Text, connectionstate);
        }

        public DbDataReader ExecuteReader(string query, CommandType commandtype, ConnectionState connectionstate)
        {
            _objCommand.CommandText = query;
            _objCommand.CommandType = commandtype;
            DbDataReader reader = null;
            try
            {
                if (_objConnection.State == System.Data.ConnectionState.Closed)
                {
                    _objConnection.Open();
                }
                reader = connectionstate == ConnectionState.CloseOnExit ? _objCommand.ExecuteReader(CommandBehavior.CloseConnection) : _objCommand.ExecuteReader();
            }
            catch (Exception ex)
            {
                HandleExceptions(ex);
            }
            finally
            {
                _objCommand.Parameters.Clear();
            }

            return reader;
        }

        #endregion

        #region # DATASET #

        public DataSet ExecuteDataSet(string query)
        {
            return ExecuteDataSet(query, CommandType.Text, ConnectionState.CloseOnExit);
        }

        public DataSet ExecuteDataSet(string query, CommandType commandtype)
        {
            return ExecuteDataSet(query, commandtype, ConnectionState.CloseOnExit);
        }

        public DataSet ExecuteDataSet(string query, ConnectionState connectionstate)
        {
            return ExecuteDataSet(query, CommandType.Text, connectionstate);
        }

        public DataSet ExecuteDataSet(string query, CommandType commandtype, ConnectionState connectionstate)
        {
            var adapter = _objFactory.CreateDataAdapter();
            _objCommand.CommandText = query;
            _objCommand.CommandType = commandtype;
            if (adapter != null)
            {
                adapter.SelectCommand = _objCommand;
                var ds = new DataSet();
                try
                {
                    adapter.Fill(ds);
                }
                catch (Exception ex)
                {
                    HandleExceptions(ex);
                }
                finally
                {
                    _objCommand.Parameters.Clear();
                    if (connectionstate == ConnectionState.CloseOnExit)
                    {
                        if (_objConnection.State == System.Data.ConnectionState.Open)
                        {
                            _objConnection.Close();
                        }
                    }
                }
                return ds;
            }
            return null;
        }

        #endregion

        #region # DATATABLE #

        public DataTable ExecuteDataTable(string query)
        {
            return ExecuteDataTable(query, CommandType.Text, ConnectionState.CloseOnExit);
        }

        public DataTable ExecuteDataTable(string query, CommandType commandtype)
        {
            return ExecuteDataTable(query, commandtype, ConnectionState.CloseOnExit);
        }

        public DataTable ExecuteDataTable(string query, ConnectionState connectionstate)
        {
            return ExecuteDataTable(query, CommandType.Text, connectionstate);
        }

        public DataTable ExecuteDataTable(string query, CommandType commandtype, ConnectionState connectionstate)
        {
            var adapter = _objFactory.CreateDataAdapter();
            _objCommand.CommandText = query;
            _objCommand.CommandType = commandtype;
            if (adapter != null)
            {
                adapter.SelectCommand = _objCommand;
                var dt = new DataTable();
                try
                {
                    adapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    HandleExceptions(ex);
                }
                finally
                {
                    _objCommand.Parameters.Clear();
                    if (connectionstate == ConnectionState.CloseOnExit)
                    {
                        if (_objConnection.State == System.Data.ConnectionState.Open)
                        {
                            _objConnection.Close();
                        }
                    }
                }
                return dt;
            }
            return null;
        }

        #endregion

        private void HandleExceptions(Exception ex)
        {
            if (LogErrors)
            {
                WriteToLog(ex.Message);
            }
            if (HandleErrors)
            {
                LastError = ex.Message;
            }
            else
            {
                throw ex;
            }
        }

        private static void WriteToLog(string msg)
        {
            var writer = File.AppendText("C:/SpLog.txt");
            writer.WriteLine(DateTime.Now + " - " + msg);
            writer.Close();
        }

        public void Dispose()
        {
            _objConnection.Close();
            _objConnection.Dispose();
            _objCommand.Dispose();
        }
    }

    public enum Providers
    {
        SqlServer
    }

    public  enum ConnectionState
    {
        KeepOpen, CloseOnExit
    }
}
