﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Reflection;

namespace EduErp.DAL.Utilities
{
    /// <summary>
    /// jlt - this class lets you easily create a class from a data reader. 
    /// note- the class property names and the sproc output fields/cols from the DR must match.
    /// And the types must match correctly also. 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class DataReaderObjectLoader<T> where T : new()
    {

        private readonly Dictionary<string, int> _ordinalDict; //dic stores  propertyName-dr.ordinal

        /// <summary>
        /// constructor. 
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="throwExceptionOnMissingFields">if true, will throw exception if obj.properties are missing from dr.</param>
        public DataReaderObjectLoader(DbDataReader dr, bool throwExceptionOnMissingFields)
        {
            _ordinalDict = ordinalDictGet(typeof(T), dr, throwExceptionOnMissingFields);
        }

        /// <summary>
        /// forms a dict of  dr.FieldNames/ objPropertyNames and there corresponding dr- ordinal values.
        /// for each property of the class. see if it has a matching FieldName in the dr row.
        /// if yes. then store the ordinal value.
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="classToPopulateType"></param>
        /// <param name="throwExceptionOnMissingFields">if true, will throw exception if obj.properties are missing from dr.</param>
        /// <returns></returns>
        private Dictionary<string, int> ordinalDictGet(Type classToPopulateType, IDataRecord dr, bool throwExceptionOnMissingFields)
        {
            var ordinals = new Dictionary<string, int>();
            var type = classToPopulateType; // typeof(Model.ServiceUserTreatment);


            foreach (var prop in type.GetProperties())
            {
                int o = -1;
                try
                {
                    o = dr.GetOrdinal(prop.Name);
                }
                catch (IndexOutOfRangeException)
                {
                    if (throwExceptionOnMissingFields) throw new Exception(string.Format("Error:  property name '{0}' not found in DataReader fields.", prop.Name));
                }
                if (o > -1)
                {
                    // var dbtype = dr.GetFieldType(o);
                    //if (dbtype.Equals(prop.PropertyType))
                    //{
                    ordinals.Add(prop.Name, o);
                    //}else
                    //{                       
                    //throw new Exception(string.Format("Error:  property name '{0}'is [{1}]   dataReader type is [{2}]", prop.Name, prop.PropertyType, dbtype ));                        
                    //}
                }
            }
            return ordinals;
        }//meth

        /// <summary>
        /// jlt- this method actually creats the Obj required and populates its values from the current dr.row.
        /// note-  obj.propertyNames and dr.fields must match. 
        /// and an implicit conversion must exist for the matching types. 
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="throwExceptionOnConversionErrors">if true then throws exception when cant make conversions. </param>
        public T GetObjectFromDataReader(DbDataReader dr, bool throwExceptionOnConversionErrors)
        {
            var obj = new T();
            foreach (var prop in obj.GetType().GetProperties()) //loop through properties in this Object 
            {
                if (_ordinalDict.ContainsKey(prop.Name))  //if the property has a valid field in the DR. get the ordinal exists
                {
                    var ordinal = _ordinalDict[prop.Name]; //get dr.ordinal value for this property.
                    if (!dr.IsDBNull(ordinal)) //check its not a db null.
                    {
                        var dbvalue = dr.GetValue(ordinal); //get the value.
                        //use setValue to set the value. i think this uses implicit conversion so need to check this.
                        try
                        {
                            if (dbvalue != null)
                            {
                                var dbvalueType = dbvalue.GetType();
                                if (prop.PropertyType.Equals(dbvalueType))
                                {
                                    prop.SetValue(obj, dbvalue, null); //null means no indexes}                                    
                                }
                                else
                                {
                                    if (IsNullableType(dbvalueType) || IsNullableType(prop.PropertyType)) //if any are nullable try nullable converter
                                    {
                                        var conversionType = prop.PropertyType; // typeof(C);
                                        if (conversionType.IsGenericType && conversionType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                                        {
                                            var nullableConverter = new NullableConverter(conversionType);
                                            conversionType = nullableConverter.UnderlyingType;
                                        }
                                        var o = Convert.ChangeType(dbvalue, conversionType);
                                        prop.SetValue(obj, o, null);
                                    }
                                    else
                                    {
                                        var o = Convert.ChangeType(dbvalue, prop.PropertyType);
                                        prop.SetValue(obj, o, null);
                                    }

                                }
                            }
                        }
                        catch (ArgumentException)
                        {
                            //ArgumentException The index array does not contain the type of arguments needed. The property's Get method is not found.
                            if (throwExceptionOnConversionErrors) throw new Exception(string.Format("Error: setting property value '{0}' to '{1}'   check this property has a Get Method .", prop.Name, dbvalue));
                        }
                        catch (TargetException)
                        {
                            //TargetException : The object does not match the target type, or a property is an instance property but obj is a null reference . 
                            if (throwExceptionOnConversionErrors) throw new Exception(string.Format("Error: setting property value '{0}' type[{1}] to '{2}'   check this property Type is correct .", prop.Name, prop.PropertyType, dbvalue));
                        }
                        catch (Exception)
                        {
                            if (throwExceptionOnConversionErrors) throw new Exception(string.Format("Error: converting dr.field type [{0}] to  property  {1} type[{2}]    check this property Type is correct .", dbvalue.GetType(), prop.Name, prop.PropertyType));

                        }
                    }
                }
            }
            return obj;
        }

        /// <summary>
        /// test for nullable types.
        /// </summary>
        /// <param name="theType"></param>
        /// <returns></returns>
        public bool IsNullableType(Type theType)
        {
            return (theType.IsGenericType && theType.
              GetGenericTypeDefinition().Equals
              (typeof(Nullable<>)));
        }

        public static C ChangeType<C>(object value)
        {
            Type conversionType = typeof(C);

            if (conversionType.IsGenericType && conversionType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                if (value == null)
                {
                    return default(C);
                }
                var nullableConverter = new NullableConverter(conversionType);
                conversionType = nullableConverter.UnderlyingType;
            }

            return (C)Convert.ChangeType(value, conversionType);
        }



    }//class
}
