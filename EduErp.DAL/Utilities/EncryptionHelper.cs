﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace EduErp.DAL.Utilities
{
    internal class EncryptionHelper
    {
        internal static string EncryptPassword(string salt, string password)
        {
            var saltHasher = new Rfc2898DeriveBytes(salt.ToLower(), Encoding.Default.GetBytes(salt), 10000);
            var eSalt = Convert.ToBase64String(saltHasher.GetBytes(25));

            var passwordHasher = new Rfc2898DeriveBytes(password, Encoding.Default.GetBytes(eSalt), 10000);
            return Convert.ToBase64String(passwordHasher.GetBytes(25));
        }
    }
}
