﻿using System.Collections.Generic;
using System.Linq;

namespace EduErp.DAL.Utilities
{
    internal static class Convertor
    {
        internal static TOut Convert<TOut, TIn>(this TIn fromRecord) where TOut : new()
        {
            var toRecord = new TOut();

            if (fromRecord == null) return toRecord;

            var fromFields = typeof(TIn).GetProperties();
            var toFields = typeof(TOut).GetProperties();

            foreach (var fromField in fromFields)
            {
                foreach (var toField in toFields)
                {
                    if (fromField.Name != toField.Name) continue;
                    toField.SetValue(toRecord, fromField.GetValue(fromRecord, null), null);
                    break;
                }

            }
            return toRecord;
        }

        public static List<TOut> Convert<TOut, TIn>(this List<TIn> fromRecordList) where TOut : new()
        {
            if (fromRecordList == null) return new List<TOut>();
            return fromRecordList.Count == 0 ? new List<TOut>() : fromRecordList.Select(Convert<TOut, TIn>).ToList();
        }
    }
}
