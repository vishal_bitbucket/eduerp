﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EduErp.BO;
using EduErp.Model;

namespace EduErp.DAL
{
   public class ExamQuestionModule
    {
       public static List<Common> GetExams(int schoolId)
       {
           List<Common> exams;
           using (var context = new EduErpEntities())
           {
               exams = (from s in context.OnlineTestMasters
                           where s.StatusId == (int)UserStatus.Active && s.SchoolId == schoolId
                           select new Common
                           {
                               Id = s.Id,
                               Name = s.Name
                           }).ToList();
           }
           return exams;
       }
       public static List<QuestionList> GetQuestionsForQuestionMapping(int subjectId)
       {

           List<QuestionList> questions;
           using (var context = new EduErpEntities())
           {
               if ( subjectId == 0)
               {
                   questions = (from s in context.OnlineTestQuestionMasters
                               where  s.StatusId == (int)UserStatus.Active
                               select new QuestionList
                               {
                                   QuestionIdForQuestionMapping = s.Id,
                                   Question = s.Questions
                               }).ToList();
               }
               else if ((subjectId != 0 && subjectId == 0) || subjectId == 0)
               {
                   questions = (from s in context.OnlineTestQuestionMasters
                                join sm in context.OTExamQuestionMappings on s.Id equals sm.ExamId
                                where sm.QuestionId == subjectId && s.StatusId == (int)UserStatus.Active
                                select new QuestionList
                               {
                                   QuestionIdForQuestionMapping = s.Id,
                                   Question = s.Questions
                               }).ToList();
               }
               else
               {
                   questions = (from s in context.OnlineTestQuestionMasters
                                join sm in context.OTExamQuestionMappings on s.Id equals sm.ExamId
                                where sm.QuestionId == subjectId && sm.QuestionId == subjectId && s.StatusId == (int)UserStatus.Active
                                select new QuestionList
                               {
                                   QuestionIdForQuestionMapping = s.Id,
                                   Question = s.Questions
                               }).ToList();
               }
               
           }
           return questions;

       }
      
    }
}
