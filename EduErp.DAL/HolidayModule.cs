﻿using System;
using System.Collections.Generic;
using System.Linq;
using EduErp.BO;
using EduErp.Model;
using EduErp.DAL.Utilities;

namespace EduErp.DAL
{
    public class HolidayModule
    {
        public static int AddHoliday(HolidayBo model)
        {
            
            using (var context = new EduErpEntities())
            {
                var result = context.HolidayMasters.FirstOrDefault(a => a.StatusId != (int)UserStatus.Deleted && a.Name.ToLower().Equals(model.Name.ToLower()) && a.SchoolId == model.SchoolId);
                if (result != null)
                {
                    return -1;
                }

                var holiday = model.Convert<Model.HolidayMaster, HolidayBo>();
                context.HolidayMasters.Add(holiday);
                context.SaveChanges();
                //var attendances = context.AttendanceMasters.Where(a => a.SchoolId == model.SchoolId && a.SessionId == model.SessionId && a.Date >= model.FromDate && a.Date <= model.ToDate);
                //foreach (var attendance in attendances)
                //{
                //    context.AttendanceMasters.Remove(attendance);
                //    context.SaveChanges();
                //}
                return 1;
            }
        }

        public static List<HolidayBo> GetHolidays(int schoolId)
        {
            using (var context = new EduErpEntities())
            {
                return (from s in context.HolidayMasters
                        join c in context.SessionMasters on s.SessionId equals c.Id
                        where s.StatusId != (int)UserStatus.Deleted && (schoolId == 0 || s.SchoolId == schoolId)
                        select new HolidayBo
                        {
                            Id = s.Id,
                            Name = s.Name,
                            SessionName = c.Name,
                            FromDate = s.FromDate,
                            ToDate = s.ToDate,
                            SchoolId = s.SchoolId,
                            StatusId = s.StatusId,
                            CreatedOn = s.CreatedOn
                        }).ToList();
            }
        }

        public static bool UpdateHoliday(HolidayBo model)
        {
            using (var context = new EduErpEntities())
            {
                var holiday = context.HolidayMasters.FirstOrDefault(a => a.Id == model.Id);
                if (holiday == null) return false;
                holiday.Name = model.Name;
                holiday.FromDate = model.FromDate;
                holiday.ToDate = model.ToDate;
                holiday.SessionId = model.SessionId;
                holiday.Description = model.Description;
                holiday.LastUpdatedBy = model.LastUpdatedBy;
                holiday.LastUpdatedOn = model.LastUpdatedOn;
                context.SaveChanges();
                return true;
            }
        }

        public static bool UpdateStatus(int id, int statusId, int userId)
        {
            using (var context = new EduErpEntities())
            {
                var holiday = context.HolidayMasters.FirstOrDefault(a => a.Id == id);
                if (holiday == null) return false;
                holiday.StatusId = statusId;
                holiday.LastUpdatedBy = userId;
                holiday.LastUpdatedOn = DateTime.Now;
                context.SaveChanges();
                return true;
            }
        }
        public static HolidayBo GetHolidayById(int id)
        {
            using (var context = new EduErpEntities())
            {
                var holiday = context.HolidayMasters.FirstOrDefault(a => a.Id == id);
                return holiday.Convert<HolidayBo, Model.HolidayMaster>();
            }
        }

        public static List<Common> GetSessionList(int schoolId)
        {
            List<Common> sessions;
            using (var context = new EduErpEntities())
            {
                sessions = (from s in context.SessionMasters
                            where s.SchoolId == schoolId && s.StatusId == (int)UserStatus.Active
                            select new Common
                            {
                                Id = s.Id,
                                Name = s.Name
                            }).ToList();

            }
            return sessions;
        }
    }
}
