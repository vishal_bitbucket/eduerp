﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EduErp.BO;
using EduErp.Model;

namespace EduErp.DAL
{
   public  class ItemModule
    {
       public static int AddItem(ItemBo model)
       {
           using (var context=new EduErpEntities())
           {
               var result = context.ItemMasters.FirstOrDefault(a => a.StatusId != (int)UserStatus.Deleted && a.Name.ToLower().Equals(model.Name.ToLower()) && a.SchoolId == model.SchoolId);
               if (result != null)
               {
                   return -1;
               }
               var item = new Model.ItemMaster
               {
                  Name = model.Name,
                  StatusId = model.StatusId,
                  ExpensesTypeId = model.ExpensesTypeId,
                  SchoolId = model.SchoolId,
                  CreatedBy = model.CreatedBy,
                  CreatedOn = model.CreatedOn
               };
               context.ItemMasters.Add(item);
               context.SaveChanges();
           }
           return 1;
       }

       public static List<ItemBo> GetItemes(int schoolId)
       {
           var context = new EduErpEntities();
           var employeeTypeList = (from item in context.ItemMasters
                                   join sch in context.SchoolMasters on item.SchoolId equals sch.Id
                                   where item.SchoolId == schoolId && item.StatusId != (int)UserStatus.Deleted
                                   select new ItemBo
                                   {
                                       Id = item.Id,
                                       Name = item.Name,
                                       ExpensesTypeId = item.ExpensesTypeId,
                                       StatusId = item.StatusId,
                                       SchoolName = sch.Name,
                                       SchoolId = item.SchoolId

                                   }).ToList();
           return employeeTypeList;
       }

       public static ItemBo GetItem(int id)
       {
           var context = new EduErpEntities();
           var item = (from itemms in context.ItemMasters
                       where itemms.Id == id && itemms.StatusId != (int)UserStatus.Deleted
                                   select new ItemBo
                                   {
                                       Id = itemms.Id,
                                       Name = itemms.Name,
                                       ExpensesTypeId = itemms.ExpensesTypeId,
                                       StatusId = itemms.StatusId,
                                       SchoolId = itemms.SchoolId

                                   }).FirstOrDefault();
           return item;
       }
       public static bool UpdateItem(ItemBo model)
       {
           using (var context = new EduErpEntities())
           {
               var item = context.ItemMasters.FirstOrDefault(a => a.Id == model.Id);
               if (item == null) return true;
               item.Name = model.Name;
               item.ExpensesTypeId = model.ExpensesTypeId;
               item.SchoolId = model.SchoolId;
               item.LastUpdatedBy = model.LastUpdatedBy;
               item.LastUpdatedOn = model.LastUpdatedOn;
               context.SaveChanges();
           }
           return true;
       }
       public static bool UpdateStatus(int id, int statusId, int userId)
       {
           using (var context = new EduErpEntities())
           {
               var item = context.ItemMasters.SingleOrDefault(a => a.Id == id);
               if (item == null) return false;
               item.StatusId = statusId;
               item.LastUpdatedOn = DateTime.Now;
               item.LastUpdatedBy = userId;
               context.SaveChanges();
               return true;
           }

       }
    }
}
