﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using EduErp.BO;
using EduErp.DAL.Utilities;
using EduErp.Model;
using PlanMaster = EduErp.BO.PlanMaster;

namespace EduErp.DAL
{
    public class MyAccountModule
    {
        public static SchoolBo GetSchoolById(int schoolId)
        {
            SchoolBo schooldetails;
            using (var context = new EduErpEntities())
            {
                schooldetails = (from s in context.SchoolMasters
                                 join c in context.CityMasters on s.CityId equals c.Id
                                 join st in context.StateMasters on c.StateId equals st.Id
                                 where s.Id == schoolId
                                 select new SchoolBo()
                                 {
                                     Id = s.Id,
                                     Code = s.Code,
                                     ParentSchoolId = s.ParentSchoolId,
                                     ContactNo = s.ContactNo,
                                     Name = s.Name,
                                     Address1 = s.Address1,
                                     Address2 = s.Address2,
                                     CityId = s.CityId,
                                     Pin = s.PIN,
                                     CreatedOn = s.CreatedOn,
                                     OwnerName = s.OwnerName,
                                     CityName = c.Name,
                                     ImagePath = s.ImagePath,
                                     StateName = st.Name,
                                     StateId = st.Id,

                                 }).FirstOrDefault();


            }
            return schooldetails;
        }
        public static bool SaveMessageSetting(List<MessageSettings> model, int schoolId)
        {
            using (var context = new EduErpEntities())
            {
                if (model == null) return false;
                var settings = context.MessageAutomateMappings.Where(a => a.SchoolId == schoolId).ToList();
                foreach (var s in settings)
                {
                    context.MessageAutomateMappings.Remove(s);
                    context.SaveChanges();
                }
                foreach (var item in model)
                {
                    var messageSetting = new Model.MessageAutomateMapping
                    {
                        MessageAutomateId = item.MessageAutomaticId,
                        TemplateId = item.TemplateId,
                        SchoolId = schoolId
                    };
                    context.MessageAutomateMappings.Add(messageSetting);
                    context.SaveChanges();
                }
                return true;
            }
        }

        public static List<AutomateMessageBo> GetMessageList()
        {
            List<AutomateMessageBo> messageList;
            using (var context = new EduErpEntities())
            {
                messageList = (from am in context.AutomateMessages
                               select new AutomateMessageBo
                               {
                                   Id = am.Id,
                                   ParentId = am.ParentId,
                                   Name = am.Name,
                                   Description = am.Description
                               }).ToList();
            }
            return messageList;
        }

        public static List<BO.MessageAutomateMapping> GetMessageSettingsBySchoolId(int schoolId)
        {
            List<BO.MessageAutomateMapping> messageList;
            using (var context = new EduErpEntities())
            {
                messageList = (from am in context.MessageAutomateMappings
                               where am.SchoolId == schoolId
                               select new BO.MessageAutomateMapping
                    {
                        Id = am.Id,
                        MessageAutomateId = am.MessageAutomateId,
                        TemplateId = am.TemplateId
                    }).ToList();
            }
            return messageList;
        }

        public static AccountDetailBo GetAccountDetailBySchoolId(int schoolId)
        {
            AccountDetailBo account;
            using (var context = new EduErpEntities())
            {
                account = (from ac in context.AccountDetails
                           join s in context.SchoolMasters on ac.SchoolId equals s.Id
                           join p in context.PlanMasters on ac.PlanId equals p.Id
                           where ac.SchoolId == schoolId
                           select new AccountDetailBo
                           {
                               Id = ac.Id,
                               SchoolId = ac.SchoolId,
                               AppStartDate = ac.AppStartDate,
                               NextPaymentDate = ac.NextPaymentDate,
                               DueDate = ac.DueDate,
                               LeftUserCount = ac.LeftUserCount,
                               LeftStudentCount=ac.LeftStudentCount,
                               LeftMessageCount = ac.LeftMessageCount,
                               StatusId = ac.StatusId,
                               PlanId = ac.PlanId,
                               SchoolName = s.Name,
                               PaymentPlan = p.Name
                           }).FirstOrDefault();
            }
            return account;
        }

        public static List<AccountFeeDtailBo> GetAccountFeeDetailsBySchoolId(int schoolId)
        {
            List<AccountFeeDtailBo> details = new List<AccountFeeDtailBo>();
            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                using (var dr = spdb.ExecuteReader("GetAccountFeeDetailBySchoolId", CommandType.StoredProcedure))
                {
                    var drgetter = new DataReaderObjectLoader<AccountFeeDtailBo>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true);//use tool to load object from dr.row
                        details.Add(obj);
                    }
                }

            }
            return details;
        }
        public static List<PlanMaster> GetPlans(int schoolId)
        {
            List<PlanMaster> plans;
            using (var context = new EduErpEntities())
            {
                if (schoolId == 0)
                {
                    plans = (from p in context.PlanMasters
                             where p.StatusId != (int)UserStatus.Deleted 
                             select new PlanMaster
                             {
                                 Id = p.Id,
                                 Name = p.Name,
                                 MaxMessageLimit = p.MaxMessageLimit,
                                 MaxStudentLimit = p.MaxStudentLimit,
                                 MaxUserLimit = p.MaxUserLimit,
                                 Detail = p.Detail,
                                 StatusId = p.StatusId,
                                 // ValidityInMonth = p.ValidityInMonth

                             }).ToList();
                }
                else {
                    plans = (from p in context.PlanMasters join spm in context.SchoolPlanMappings on p.Id equals spm.PlanId
                             where p.StatusId != (int)UserStatus.Deleted  && spm.SchoolId==schoolId
                             select new PlanMaster
                             {
                                 Id = p.Id,
                                 Name = p.Name,
                                 MaxMessageLimit = p.MaxMessageLimit,
                                 MaxStudentLimit = p.MaxStudentLimit,
                                 MaxUserLimit = p.MaxUserLimit,
                                 Detail = p.Detail,
                                 StatusId = p.StatusId,
                                 // ValidityInMonth = p.ValidityInMonth

                             }).ToList();
                
                }
            }
            return plans;
        }
    }
}
