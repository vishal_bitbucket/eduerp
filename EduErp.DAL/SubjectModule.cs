﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using EduErp.BO;
using EduErp.DAL.Utilities;
using EduErp.Model;
using ClassMaster = EduErp.BO.ClassMaster;
using SubjectClassMapping = EduErp.Model.SubjectClassMapping;
using SubjectMaster = EduErp.Model.SubjectMaster;

namespace EduErp.DAL
{
    public class SubjectModule
    {
        public static int AddSuject(SubjectBo model)
        {
            using (var context = new EduErpEntities())
            {
                var result = context.SubjectMasters.FirstOrDefault(a => a.Status != (int)UserStatus.Deleted && a.Name.ToLower().Equals(model.Name.ToLower()) && a.SchoolId == model.SchoolId);
                if (result != null)
                {
                    return -1;
                }
                var subject = new SubjectMaster()
                {
                    // Id = model.Id,
                    Name = model.Name,
                    // ClassId = model.ClassId,
                    SchoolId = model.SchoolId,
                    CreatedBy = model.CreatedBy,
                    CreatedOn = model.CreatedOn,
                    Status = model.Status
                };
                context.SubjectMasters.Add(subject);
                context.SaveChanges();
                foreach (var item in model.ClassIdes)
                {
                    var classDetail = new SubjectClassMapping()
                    {
                        SubjectId = subject.Id,
                        StatusId = subject.Status ?? 0,
                        CreatedBy = subject.CreatedBy,
                        CreatedOn = subject.CreatedOn,
                        ClassId = item

                    };
                    context.SubjectClassMappings.Add(classDetail);
                    context.SaveChanges();
                }

                return 1;
            }
        }

        //public static List<ListOfClass> GetClassList(int schoolId)
        //{
        //    List<ListOfClass> clssList;
        //    using (var context = new EduErpEntities())
        //    {
        //        clssList = (from s in context.ClassMasters
        //                    where s.Status == (int)UserStatus.Active && s.SchoolId==schoolId
        //                    select new ListOfClass
        //                    {
        //                        ListClassId = s.Id,
        //                        ClassName = s.Name

        //                    }).ToList();
        //    }

        //    return clssList;
        //}
        public static List<ClassMaster> GetClasses(int schoolId)
        {
            List<ClassMaster> classes;
            using (var context = new EduErpEntities())
            {

                classes = (from c in context.ClassMasters
                           where c.SchoolId == schoolId && c.Status==(int)UserStatus.Active
                           select new ClassMaster
                           {
                               Id = c.Id,
                               Name = c.Name
                           }).ToList();
            }
            return classes;
        }

        public static List<ClassMaster> GetClassesBySubjectId(int schoolId)
        {
            List<ClassMaster> classes;
            using (var context = new EduErpEntities())
            {

                classes = (from c in context.ClassMasters
                           join scm in context.SubjectClassMappings on c.Id equals scm.ClassId
                           where c.SchoolId == schoolId
                           select new ClassMaster
                           {
                               Id = scm.ClassId,
                               Name = c.Name
                           }).ToList();
            }
            return classes;
        }

        //public static List<SubjectBO> GetSubjects( int schoolId)
        //{
        //    List<SubjectBO> subjects;
        //    using (var context = new EduErpEntities())
        //    {
        //        subjects = (from s in context.SubjectMasters join c in context.ClassMasters on s.ClassId equals c.Id
        //                   where  ( s.Status != (int)UserStatus.Deleted) &&(s.SchoolId==schoolId || s.SchoolId==0) 
        //                   select new SubjectBO
        //                   {

        //                       Id=s.Id,
        //                       ClassName=c.Name,
        //                       Name=s.Name,
        //                       ClassId=s.ClassId,
        //                       CreatedBy=s.CreatedBy,
        //                       CreatedOn=s.CreatedOn,
        //                       SchoolId=s.SchoolId,
        //                       Status=s.Status
        //                   }).ToList();
        //    }
        //    return subjects;
        //}

        public static List<int> GetClassId(int id)
        {
            List<int> ids;
            using (var context = new EduErpEntities())
            {
                ids = (from ftd in context.SubjectClassMappings
                       where ftd.SubjectId == id
                       select ftd.ClassId).ToList();
            }

            return ids;
        }

        public static SubjectBo GetSubjects(int schoolId)
        {
            SubjectBo subjects = new SubjectBo();
            List<SubjectBo> obj = new List<SubjectBo>();

            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                using (var dr = spdb.ExecuteReader("GetClassBySubject", CommandType.StoredProcedure))
                {

                    while (dr.Read())
                    {
                        SubjectBo subject = new SubjectBo();
                        subject.SubjectId = Convert.ToInt32(dr["SubjectId"]);
                        subject.ClassName = dr["ClassNames"].ToString();
                        subject.Name = dr["SubjectName"].ToString();
                        subject.Status = Convert.ToInt32(dr["SubjectStatus"]);
                        obj.Add(subject);
                    }
                    subjects.SubjectBoList = obj;
                }
            }
            return subjects;
        }




        public static bool UpdateSubject(SubjectBo model)
        {
            using (var context = new EduErpEntities())
            {
                var subject = context.SubjectMasters.FirstOrDefault(a => a.Id == model.Id );
                if (subject == null) return false;
                     subject.Id = model.Id;
                    subject.Name = model.Name;
                    // subject.ClassId = model.ClassId;
                    subject.Status = model.Status;
                    subject.LastUpdatedBy = model.LastUpdatedBy;
                    subject.LastUpdatedOn = model.LastUpdatedOn;
                    subject.SchoolId = model.SchoolId;
                    
                  if (model.ClassIdes.Length != 0)
                    {
                        context.SubjectClassMappings.Where(a => a.SubjectId == model.Id).ToList().ForEach(a => context.SubjectClassMappings.Remove(a));
                        context.SaveChanges();
                     foreach (var item in model.ClassIdes)
                      {
                        var newSubjectDetail = new SubjectClassMapping
                        {
                            SubjectId = model.Id,
                            StatusId = model.Status??0,
                            LastUpdatedBy = model.LastUpdatedBy,
                            LastUpdatedOn = model.LastUpdatedOn,
                            CreatedBy = model.CreatedBy,
                            CreatedOn = model.CreatedOn,
                            ClassId = item
                        };
                        context.SubjectClassMappings.Add(newSubjectDetail);
                        context.SaveChanges();

                    }
                }
                
                context.SaveChanges();
                return false;
            }
        }

        public static SubjectBo GetSubjectById(int id)
        {
            SubjectBo subject = new SubjectBo();

            List<SubjectBo> subjectList;
            using (var context = new EduErpEntities())
            {
                subjectList = (from s in context.SubjectMasters
                               join scm in context.SubjectClassMappings on s.Id equals scm.SubjectId
                              // join c in context.ClassMasters on scm.ClassId equals c.Id
                               where s.Id == id && s.Status == (int)UserStatus.Active
                               select new SubjectBo()
                             {

                                 Name = s.Name,
                                 SchoolId = s.SchoolId,
                                 Status = s.Status,
                                 ClassId = scm.ClassId,
                                // ClassName = c.Name
                                 //  ClassId=s.ClassId
                             }).ToList();

                subject.ClassIdes = new int[subjectList.Count()];
                for (int i = 0; i < subjectList.Count(); i++)
                {
                    subject.ClassIdes[i] = subjectList[i].ClassId;
                    if (i == 0)
                    {
                        subject.Name = subjectList[i].Name;
                    }

                }
                subject.ClassIds = string.Join(",", subject.ClassIdes);

            }
            return subject;
        }
        public static bool UpdateStatus(int id, int status, int userId)
        {
            using (var context = new EduErpEntities())
            {
                var subject = context.SubjectMasters.FirstOrDefault(a => a.Id == id);
                if (subject == null) return false;
                subject.Status = status;
                subject.LastUpdatedOn = DateTime.Now;
                subject.LastUpdatedBy = userId;
                context.SaveChanges();
                return true;
            }
        }
    }
}
