﻿using System;
using System.Linq;
using EduErp.Base;
using EduErp.BO;
using EduErp.DAL.Utilities;
using EduErp.Model;
using ApplicationLog = EduErp.BO.ApplicationLog;
using EmailSend = EduErp.BO.EmailSend;

namespace EduErp.DAL
{
    public class LogModule
    {
        public static ServiceResult<bool> AddApplicationLog(ApplicationLog applicationLog)
        {
            if (applicationLog == null) throw new ArgumentNullException("applicationLog");
            using (var context = new EduErpEntities())
            {
                var modelApplicationLog = applicationLog.Convert<Model.ApplicationLog, ApplicationLog>();
                context.ApplicationLogs.Add(modelApplicationLog);
                context.SaveChanges();
            }
            return Responses.SuccessDataResult(true);
        }

        #region # Application Setting #

        public static string GetAppSetting(HostSettingNames key)
        {
            using (var context = new EduErpEntities())
            {

                var k = key.ToString();
                var applicationSetting = context.ApplicationSettings.FirstOrDefault(a => a.Name.Equals(k));
                if (applicationSetting == null) return string.Empty;
                return applicationSetting.Value;
            }
        }

        #endregion

        #region # Email Send #

        public static ServiceResult<bool> AddSendEmail(EmailSend email)
        {
            using (var context = new EduErpEntities())
            {
                var data = email.Convert<Model.EmailSend, EmailSend>();
                context.EmailSends.Add(data);
                context.SaveChanges();
                return Responses.SuccessDataResult(true);
            }
        }

        public static NotificationUserBo GetUserById(long userId)
        {
            using (var context = new EduErpEntities())
            {
                var userInfo = (from a in context.UserMasters
                                where a.Id == userId && a.StatusId == 1
                                select new NotificationUserBo
                                {
                                    Id = a.Id,
                                    DisplayName = a.Name,
                                    Email = a.Email,
                                }).FirstOrDefault();
                return userInfo;
            }
        }

        #endregion

        #region # Amazon Setting #

        public static BO.AmazonSetting GetAmazonSetting()
        {
            using (var context = new EduErpEntities())
            {
                var amazonSetting =
                    context.AmazonSettings.FirstOrDefault().Convert<BO.AmazonSetting, Model.AmazonSetting>();
                return amazonSetting ?? new BO.AmazonSetting();
            }
        }

        #endregion
    }
}
