﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using EduErp.BO;
using EduErp.DAL.Utilities;
using EduErp.Model;
using FeeHeadMaster = EduErp.Model.FeeHeadMaster;
using FeeHeadMonthMapping = EduErp.Model.FeeHeadMonthMapping;

namespace EduErp.DAL
{
    public class FeeHeadModule
    {
        public static int AddFeeHead(FeeHeadBo model)
        {
            using (var context = new EduErpEntities())
            {
                if (model.FeeHeadTypeId == (int)FeeHeadType.ClassBy)
                {
                    var feehead = (from f in context.FeeHeadMasters
                                   join fhhm in context.FeeHeadMonthMappings on f.Id equals fhhm.FeeHeadId
                                   where f.StatusId != (int)UserStatus.Deleted && f.Name.ToLower()==model.Name.ToLower() && f.SchoolId == model.SchoolId && fhhm.ClassId == model.ClassId
                                   select new BO.FeeHeadBo
                                   {
                                       Name = f.Name,
                                       Description = f.Description,
                                       FeeHeadTypeId = f.FeeHeadTypeId
                                   }).FirstOrDefault();
                    if (feehead != null)
                    {
                        return -1;
                    }
                }
                else
                {
                    var result = context.FeeHeadMasters.FirstOrDefault(a => a.StatusId != (int)UserStatus.Deleted && a.Name.ToLower().Equals(model.Name.ToLower()) && a.SchoolId == model.SchoolId);
                    if (result != null)
                    {
                        return -1;
                    }
                }
                var feeHeadType = new FeeHeadMaster
                {
                    Name = model.Name,
                    FeeHeadTypeId = model.FeeHeadTypeId,
                    IsDiscount = model.IsDiscount,
                    Description = model.Description,
                    CreatedBy = model.CreatedBy,
                    CreatedOn = model.CreatedOn,
                    StatusId = model.StatusId,
                    SchoolId = model.SchoolId
                };
                context.FeeHeadMasters.Add(feeHeadType);
                context.SaveChanges();

                foreach (var month in model.ListOfMonths)
                {
                    if (!month.Selected) continue;
                    var feeHeadMapping = new FeeHeadMonthMapping
                    {
                        FeeHeadId = feeHeadType.Id,
                        ClassId = model.ClassId,
                        MonthId = month.MonthId,
                        SessionId = model.SessionId,
                        Amount = model.Amount
                    };

                    context.FeeHeadMonthMappings.Add(feeHeadMapping);
                    context.SaveChanges();

                }
                return 1;
            }
        }

        public static List<FeeHeadBo> GetFeeHeads(int schoolId)
        {
            List<FeeHeadBo> feeHeads = new List<FeeHeadBo>();
            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                using (var dr = spdb.ExecuteReader("GetFeeHeads", CommandType.StoredProcedure))
                {
                    var drFeeHead = new DataReaderObjectLoader<FeeHeadBo>(dr, false);
                    while (dr.Read())
                    {

                        var objec = drFeeHead.GetObjectFromDataReader(dr, true);
                        feeHeads.Add(objec);
                    }
                }
            }
            return feeHeads;
        }

        public static FeeHeadBo GetFeeHeadById(int feeHeadId)
        {
            FeeHeadBo feeHead;
            using (var context = new EduErpEntities())
            {
               var feeHeadTypeId = (from ftd in context.FeeHeadMasters
                                 where ftd.Id == feeHeadId
                                 select ftd.FeeHeadTypeId).FirstOrDefault();
                if (feeHeadTypeId == (int)FeeHeadType.ClassBy || feeHeadTypeId == (int)FeeHeadType.General)
                {
                    feeHead = (from f in context.FeeHeadMasters
                               join fm in context.FeeHeadMonthMappings on f.Id equals fm.FeeHeadId
                               where f.Id == feeHeadId && f.StatusId == (int)UserStatus.Active
                               select new FeeHeadBo()
                               {
                                   Id = f.Id,
                                   IsDiscount = f.IsDiscount,
                                   FeeHeadTypeId = f.FeeHeadTypeId,
                                   Description = f.Description,
                                   Name = f.Name,
                                   Amount = fm.Amount,
                                   ClassId = fm.ClassId ?? 0,
                               }).FirstOrDefault();
                    var monthList = context.FeeHeadMonthMappings.Where(a => a.FeeHeadId == feeHeadId).ToList();

                    if (feeHead != null)
                    {
                        feeHead.ListOfMonths = new List<MonthMenu>();


                        foreach (var month in monthList)
                        {
                            var listofmonth = new MonthMenu
                            {
                                MonthId = month.MonthId ?? 0,
                                Selected = true,
                                ClassId = month.ClassId ?? 0,
                                SessionId = month.SessionId ?? 0,
                                Amount = month.Amount
                            };
                            feeHead.ListOfMonths.Add(listofmonth);
                        }
                    }
                }
                else
                {

                    feeHead = (from f in context.FeeHeadMasters
                               where f.Id == feeHeadId && f.StatusId == (int)UserStatus.Active
                               select new FeeHeadBo()
                               {
                                   Id = f.Id,
                                   IsDiscount = f.IsDiscount,
                                   FeeHeadTypeId = f.FeeHeadTypeId,
                                   Description = f.Description,
                                   Name = f.Name
                               }).FirstOrDefault();
                }

            }
            return feeHead;
        }

        public static List<Common> GetFeeHeadName(int schoolId)
        {
            List<Common> fname;
            using (var context = new EduErpEntities())
            {
                fname = (from s in context.FeeHeadMasters
                         where s.StatusId == (int)UserStatus.Active && (s.SchoolId == schoolId || s.SchoolId == 0)

                         select new Common
                         {
                             Id = s.Id,
                             Name = s.Name
                         }).ToList();
            }
            return fname;
        }

        public static List<Common> GetClasses(int schoolId)
        {
            List<Common> clss;
            using (var context = new EduErpEntities())
            {
                clss = (from s in context.ClassMasters
                        where s.Status == (int)UserStatus.Active && (s.SchoolId == schoolId || s.SchoolId == 0)

                        select new Common
                        {
                            Id = s.Id,
                            Name = s.Name
                        }).ToList();
            }
            return clss;
        }

        public static int UpdateFeeHead(FeeHeadBo model)
        {
            var context = new EduErpEntities();
            int feeHeadMappingId = (from s in context.FeeHeadMonthMappings
                                    where s.FeeHeadId == model.Id
                                    select s.Id).FirstOrDefault();
            //if (model.FeeHeadTypeId == (int)FeeHeadType.ClassBy)
            //{
            //    var feehead = (from f in context.FeeHeadMasters
            //                   join fhhm in context.FeeHeadMonthMappings on f.Id equals fhhm.FeeHeadId
            //                   where f.StatusId != (int)UserStatus.Deleted && f.Name.ToLower() == model.Name.ToLower() && f.SchoolId == model.SchoolId && fhhm.ClassId == model.ClassId
            //                   select new BO.FeeHeadBo
            //                   {
            //                       Name = f.Name,
            //                       Description = f.Description,
            //                       FeeHeadTypeId = f.FeeHeadTypeId
            //                   }).FirstOrDefault();

            //    if (feehead==null)
            //    {
            //        return -2;
            //    }
            //}
            if (context.FeeTransactionDetails.Any(a => a.FeeHeadMappingId == feeHeadMappingId))
            {
                return -1;
            }
            var feeheadEdit = context.FeeHeadMasters.FirstOrDefault(a => a.Id == model.Id);
            if (feeheadEdit == null) return 0;
            feeheadEdit.Name = model.Name;
            feeheadEdit.IsDiscount = model.IsDiscount;
            feeheadEdit.FeeHeadTypeId = model.FeeHeadTypeId;
            feeheadEdit.Description = model.Description;
            feeheadEdit.LastModifiedBy = model.LastModifiedBy;
            feeheadEdit.LastModifiedOn = model.LastModifiedOn;
            context.SaveChanges();
            if (model.FeeHeadTypeId == (int)FeeHeadType.General || model.FeeHeadTypeId == (int)FeeHeadType.ClassBy)
            {
                if (model.ListOfMonths == null) return 1;
                context.FeeHeadMonthMappings.Where(a => a.FeeHeadId == model.Id).ToList().ForEach(a => context.FeeHeadMonthMappings.Remove(a));
                context.SaveChanges();
                foreach (var item in model.ListOfMonths)
                {
                    if (!item.Selected) continue;
                    var newMonth = new FeeHeadMonthMapping
                    {
                        FeeHeadId = model.Id,
                        ClassId = model.ClassId,
                        MonthId = item.MonthId,
                        SessionId = model.SessionId,
                        Amount = model.Amount
                    };
                    context.FeeHeadMonthMappings.Add(newMonth);
                    context.SaveChanges();
                }
            }
            context.SaveChanges();
            return 1;

        }

        public static int UpdateStatus(int id, int statusId, int userId)
        {
            using (var context = new EduErpEntities())
            {
                int feeHeadMappingId = (from s in context.FeeHeadMonthMappings
                                        where s.FeeHeadId == id
                                        select s.Id).FirstOrDefault();
                if (context.FeeTransactionDetails.Any(a => a.FeeHeadMappingId == feeHeadMappingId))
                {
                    return -1;
                }
                var feeHead = context.FeeHeadMasters.FirstOrDefault(a => a.Id == id);
                if (feeHead == null) return 0;
                feeHead.StatusId = statusId;
                feeHead.LastModifiedOn = DateTime.Now;
                feeHead.LastModifiedBy = userId;
                context.SaveChanges();
                return 1;
            }
        }
        public static List<Common> GetFeeHeadType()
        {
            List<Common> feeHead;
            using (var context = new EduErpEntities())
            {
                feeHead = (from fee in context.KeyWordMasters
                           where fee.Name == "FeeHeadType"
                           select new Common
                           {
                               Id = fee.KeyWordId,
                               Name = fee.KeyWordName
                           }).ToList();
            }
            return feeHead;
        }

        public static List<MonthMenu> GetFeeHeadMonth()
        {
            List<MonthMenu> feeHeadMonth;
            using (var context = new EduErpEntities())
            {
                feeHeadMonth = (from fee in context.KeyWordMasters
                                where fee.Name == "Month"
                                select new MonthMenu
                                {
                                    MonthId = fee.KeyWordId,
                                    MonthName = fee.KeyWordName
                                }).ToList();
            }
            return feeHeadMonth;
        }

    }
}
