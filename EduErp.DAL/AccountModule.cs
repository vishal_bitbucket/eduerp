﻿using System;
using System.Linq;
using EduErp.Base;
using EduErp.BO;
using EduErp.DAL.Utilities;
using EduErp.Model;
using UserLoginHistory = EduErp.Model.UserLoginHistory;
using UserMaster = EduErp.BO.UserMaster;

namespace EduErp.DAL
{
    public class AccountModule
    {
        public static ServiceResult<AuthMessageType> AuthenticateUser(string email, string password, string ipAddress, out AuthenticateUserBo user, out string token)
        {
            user = null;
            token = null;

            if (string.IsNullOrWhiteSpace(email) || string.IsNullOrWhiteSpace(password))
                return Responses.SuccessDataResult(AuthMessageType.UserNotFound);

            using (var context = new EduErpEntities())
            {
                var userInDb =
                   context.UserMasters.FirstOrDefault(a => a.UserId.ToLower() == email.ToLower() || a.Email.ToLower() == email.ToLower());
                //var createdDate = (from s in context.SchoolMasters
                //                   where s.Id == userInDb.SchoolId && s.StatusId == (int)UserStatus.Active
                //                   select s.CreatedOn).FirstOrDefault();
                //int i = (from s in context.PlanMasters
                //         where s.SchoolId == userInDb.SchoolId && s.StatusId == (int)UserStatus.Active
                //         select s.ValidityInMonth ?? 0).FirstOrDefault();
                //var date1 = createdDate.AddMonths(i);
                //var result = DateTime.Compare(date1,createdDate);
                //if (result<0)
                //{
                //    return Responses.SuccessDataResult(AuthMessageType.SubscriptionExpired);
                //}
                if (userInDb == null)
                    return Responses.SuccessDataResult(AuthMessageType.UserNotFound);

                if (userInDb.StatusId == (int)UserStatus.Deleted) return Responses.SuccessDataResult(AuthMessageType.AccountDeleted);
                if (userInDb.StatusId == (int)UserStatus.InActive) return Responses.SuccessDataResult(AuthMessageType.AccountInactive);

                var encryptedPassword = EncryptionHelper.EncryptPassword(userInDb.PasswordSalt.ToString(), password);
                if (userInDb.Password != encryptedPassword) return Responses.SuccessDataResult(AuthMessageType.UserNotFound);

                user = userInDb.Convert<AuthenticateUserBo, Model.UserMaster>();
                user.Password = string.Empty;
                user.PasswordSalt = Guid.Empty;
                var schoolDetail = context.SchoolMasters.FirstOrDefault(a => a.Id == userInDb.SchoolId);
                if (schoolDetail != null) user.SchoolName = schoolDetail.Name;
                if (schoolDetail != null) user.InstituteTypeId = schoolDetail.InstituteTypeId??0;
                var sessionDetail = context.SessionMasters.FirstOrDefault(a => a.SchoolId == userInDb.SchoolId && a.StatusId==(int)UserStatus.Active &&  a.FromDate<=DateTime.Now && a.ToDate>=DateTime.Now);
                if (sessionDetail != null)
                {
                    user.SessionId = sessionDetail.Id;
                    user.SessionName = sessionDetail.Name;
                }


                context.UserLoginHistories.Add(new UserLoginHistory
                {
                    LoginDate = DateTime.UtcNow,
                    UserId = userInDb.Id,
                    IpAddress = ipAddress
                });
                context.SaveChanges();
                return Responses.SuccessDataResult(AuthMessageType.Success);

            }
        }

    }
}
