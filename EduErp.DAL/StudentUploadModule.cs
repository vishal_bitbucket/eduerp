﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using EduErp.BO;
using EduErp.Model;
using EduErp.Mvc.Utilities;
using StudentShadow = EduErp.BO.StudentShadow;

namespace EduErp.DAL
{
    public class StudentUploadModule
    {
        public static StudentUploadBo SaveStudentDetail(DataTable dt, int userId,int sessionId,int classId, int sectionId)
        {
            //_database = GeneralFunctionDAL.GetDatabase();
            var date = DateTime.Now.ToString(CultureInfo.InvariantCulture);
            var schoolId = SessionItems.SchoolId;
            const int statusId = (int) UserStatus.Active;
            dt.Columns.Add("CreatedBy", typeof(Int32));
            dt.Columns.Add("CreatedOn", typeof(DateTime));
            dt.Columns.Add("SessionId", typeof (int));
            dt.Columns.Add("ClassId", typeof (int));
            dt.Columns.Add("SectionId", typeof (int));
            dt.Columns.Add("SchoolId", typeof (int));
            dt.Columns.Add("StatusId", typeof (int));

            foreach (DataRow dr in dt.Rows)
            {  
                dr["CreatedBy"] = userId;
                dr["CreatedOn"] = date;
                dr["SessionId"] = sessionId;
                dr["ClassId"] = classId;
                dr["SectionId"] = sectionId;
                dr["SchoolId"] = schoolId;
                dr["StatusId"] = statusId;
                dr.AcceptChanges();
            }
           //using (var context = new EduErpEntities())
            //{
            //    var model = new Model.StudentShadow
            //    {
            //       SessionId = sectionId,
            //       ClassId = classId,
            //       SectionId = sectionId

            //    };
            //    context.StudentShadows.Add(model);
            //    context.SaveChanges();
            //}
            var objbulk = new SqlBulkCopy(SqlDBConnection.GetConnectionString())
            {
                DestinationTableName = "Student"
            };
            objbulk.ColumnMappings.Add("FirstName", "FirstName");
            objbulk.ColumnMappings.Add("MiddileName", "MiddileName");
            objbulk.ColumnMappings.Add("LastName", "LastName");
            objbulk.ColumnMappings.Add("Email", "Email");
            objbulk.ColumnMappings.Add("StudentMobile", "StudentMobile");
            objbulk.ColumnMappings.Add("StudentOtherInfo", "StudentOtherInfo");
            objbulk.ColumnMappings.Add("GuardianAddress", "GuardianAddress");
            objbulk.ColumnMappings.Add("GuardianOccupation", "GuardianOccupation");
            objbulk.ColumnMappings.Add("GuardianRelationWithStudent", "GuardianRelationWithStudent");
            objbulk.ColumnMappings.Add("GuardianMobileNo", "GuardianMobileNo");
            objbulk.ColumnMappings.Add("GuardianName", "GuardianName");
            objbulk.ColumnMappings.Add("MessageMobileNumber", "MessageMobileNumber");
            objbulk.ColumnMappings.Add("MotherName", "MotherName");
            objbulk.ColumnMappings.Add("FatherName", "FatherName");
            objbulk.ColumnMappings.Add("CorrAddress", "CorrAddress");

            objbulk.ColumnMappings.Add("PermanentAddress", "PermanentAddress");
            objbulk.ColumnMappings.Add("ParentMobileNo", "ParentMobileNo");
            objbulk.ColumnMappings.Add("ParentContactNo", "ParentContactNo");
            objbulk.ColumnMappings.Add("PermanentPinCode", "PermanentPinCode");
            objbulk.ColumnMappings.Add("DOB", "DOB");
            objbulk.ColumnMappings.Add("DOJ", "DOJ");
            objbulk.ColumnMappings.Add("Uid", "Uid");
            //objbulk.ColumnMappings.Add("SessionId", "SessionId");
           // objbulk.ColumnMappings.Add("ClassId", "ClassId");
            //objbulk.ColumnMappings.Add("SectionId", "SectionId");
            objbulk.ColumnMappings.Add("CreatedBy", "CreatedBy");
            objbulk.ColumnMappings.Add("CreatedOn", "CreatedOn");
            objbulk.ColumnMappings.Add("SchoolId", "SchoolId");
            objbulk.ColumnMappings.Add("StatusId", "StatusId");

            objbulk.WriteToServer(dt);
           var reslt = new StudentUploadBo
            {
                ErrorDetailsList=null
            };
            return reslt;
        }



    }
}
