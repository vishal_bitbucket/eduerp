﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using EduErp.BO;
using EduErp.DAL.Utilities;
using EduErp.Model;
using EduErp.Mvc.Utilities;
using CityMaster = EduErp.BO.CityMaster;
using SchoolMaster = EduErp.Model.SchoolMaster;
using StateMaster = EduErp.BO.StateMaster;

//using System.Data.Common;
namespace EduErp.DAL
{
    public class SchoolModule
    {
        public static bool AddSchool(SchoolBo school)
        {

            using (var context = new EduErpEntities())
            {
                var model = new SchoolMaster
                {

                    Code = school.Code,
                    ParentSchoolId = school.ParentSchoolId,
                    Name = school.Name,
                    StateId = school.StateId,
                    CityId = school.CityId,
                    Address1 = school.Address1,
                    Address2 = school.Address2,
                    PIN = school.Pin,
                    InstituteTypeId = school.InstituteTypeId,
                    OwnerName = school.OwnerName,
                    CreatedBy = school.CreatedBy,
                    StatusId = school.StatusId,
                    ContactNo = school.ContactNo,
                    CreatedOn = school.CreatedOn,
                    ImagePath = school.ImagePath,
                    S3key = school.S3key
                    };
                context.SchoolMasters.Add(model);
                context.SaveChanges();
                if (school.PlanMappingList == null) return true;
                foreach (var plan in school.PlanMappingList)
                {
                    var planMapping = new Model.SchoolPlanMapping
                    {
                        PlanId = plan.PlanId,
                        SchoolId = model.Id,
                        StartDate = plan.StartDate,
                        EndDate = plan.EndDate,
                        StatusId = model.StatusId
                    };
                    context.SchoolPlanMappings.Add(planMapping);
                    context.SaveChanges();
                }
                return true;
            }
        }
        public static List<SchoolBo> GetSchools()
        {
            List<SchoolBo> schools = new List<SchoolBo>();

            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                using (var dr = spdb.ExecuteReader("GetSchools", CommandType.StoredProcedure))
                {
                    var drgetter = new DataReaderObjectLoader<SchoolBo>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true);//use tool to load object from dr.row
                        schools.Add(obj);
                    }
                }
            }

            return schools;
        }
        public static InvoiceBo GetInvoicData(int schoolId)
        {
            List<InvoiceBo> schoolData = new List<InvoiceBo>();

            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                using (var dr = spdb.ExecuteReader("GetInvoiceData", CommandType.StoredProcedure))
                {
                    var drgetter = new DataReaderObjectLoader<InvoiceBo>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true);//use tool to load object from dr.row
                        schoolData.Add(obj);
                    }
                }
            }

            return schoolData.FirstOrDefault();
        }


        public static SchoolBo GetSchool(int id)
        {
            SchoolBo schooldetails;
            using (var context = new EduErpEntities())
            {
                schooldetails = (from s in context.SchoolMasters join spm in context.SchoolPlanMappings on s.Id equals spm.SchoolId
                                 join pm in context.PlanMasters on spm.PlanId equals pm.Id
                                 join c in context.CityMasters on s.CityId equals c.Id
                                 join st in context.StateMasters on c.StateId equals st.Id
                                 where s.Id == id
                                 select new SchoolBo()
                                 {
                                     //RequestEntityId = StringCipher s.Id,
                                     Id = s.Id,
                                     Code = s.Code,
                                     ParentSchoolId = s.ParentSchoolId,
                                     ContactNo = s.ContactNo,
                                     Name = s.Name,
                                     Address1 = s.Address1,
                                     Address2 = s.Address2,
                                     CityId = s.CityId,
                                     Pin = s.PIN,
                                     InstituteTypeId = s.InstituteTypeId??0,
                                     CreatedOn = s.CreatedOn,
                                     OwnerName = s.OwnerName,
                                     CityName = c.Name,
                                     ImagePath = s.ImagePath,
                                     StateName = st.Name,
                                     StateId = s.StateId??0,
                                     StatusId = s.StatusId,
                                    S3key = s.S3key??Guid.Empty,
                                    StartDate=spm.StartDate??DateTime.Now,
                                    EndDate=spm.EndDate??DateTime.Now,
                                    PlanTypeId=pm.PlanTypeId
                                }).FirstOrDefault();
                }
            return schooldetails;
        }
        public static List<PlanMapping> GetPlanMappingList(int id)
        {
            List<PlanMapping> mappings;
            using (var context = new EduErpEntities())
            {
                mappings = (from spm in context.SchoolPlanMappings
                            join pm in context.PlanMasters on spm.PlanId equals pm.Id
                            where spm.SchoolId == id
                            select new PlanMapping
                            {
                                StartDate = spm.StartDate??DateTime.Now,
                                EndDate = spm.EndDate ?? DateTime.Now,
                                 PlanName= pm.Name,
                                 PlanId = spm.PlanId
                              }).ToList();
            }
            return mappings;
        }
        public static bool UpdateSchool(SchoolBo school)
        {
            var context = new EduErpEntities();
            var schl = context.SchoolMasters.SingleOrDefault(a => a.Id == school.Id);
            if (schl != null)
            {
                schl.Id = school.Id;
                schl.Code = school.Code;
                schl.ParentSchoolId = school.ParentSchoolId;
                schl.Name = school.Name;
                schl.Address1 = school.Address1;
                schl.Address2 = school.Address2;
                schl.CityId = school.CityId;
                schl.StateId = school.StateId;
                schl.PIN = school.Pin;
                schl.OwnerName = school.OwnerName;
                schl.LastUpdatedBy = school.LastUpdateBy;
                schl.LastUpdatedOn = school.LastUpdatedOn;
                schl.InstituteTypeId = school.InstituteTypeId;
                schl.ImagePath = school.ImagePath;
                schl.S3key = school.S3key;
            }
            if (school.PlanMappingList == null) return true;
            var planIds = (from spm in context.SchoolPlanMappings where spm.SchoolId == school.Id select spm.PlanId).ToList();
            foreach (var planId in planIds)
            {
                context.SchoolPlanMappings.Where(a=>a.PlanId==planId).ToList().ForEach(a=>context.SchoolPlanMappings.Remove(a));
                context.SaveChanges();
            }
            foreach (var item in school.PlanMappingList)
            {
                //using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
                //{
                //    spdb.AddParameter("@PlanId", item.PlanId);
                //    spdb.AddParameter("@StartDate", item.StartDate);
                //    spdb.AddParameter("@EndDate", item.EndDate);
                //    spdb.AddParameter("@SchoolId", SessionItems.SchoolId);
                //    spdb.AddParameter("@StauId", (int)UserStatus.Active);
                //    spdb.AddParameter("@LastUpdatedBy", SessionItems.UserId);
                //    spdb.AddParameter("@LastUpdatedOn", DateTime.Now);
                //    spdb.ExecuteNonQuery("UpdateStudentExamDetail", CommandType.StoredProcedure);

                //}

                var newPlanDetail = new Model.SchoolPlanMapping
                {
                    PlanId = item.PlanId,
                    StartDate = item.StartDate,
                    EndDate = item.EndDate,
                    LastUpdatedOn = school.LastUpdatedOn,
                    LastUpdateBy = school.LastUpdateBy,
                    SchoolId = school.Id,
                    StatusId = school.StatusId
                };
                context.SchoolPlanMappings.Add(newPlanDetail);
                context.SaveChanges();
            }
            context.SaveChanges();
            return true;
        }

        public static bool Delete(SchoolBo model)
        {
            var context = new EduErpEntities();
            var school = context.SchoolMasters.SingleOrDefault(a => a.Id == model.Id);
            if (school != null)
            {
                school.StatusId = Convert.ToInt32(UserStatus.Deleted);
            }
            context.SaveChanges();

            return true;
        }

        public static IEnumerable<SchoolBo> GetParentSchoolList()
        {
            IEnumerable<SchoolBo> schoolList;
            using (var context = new EduErpEntities())
            {
                schoolList = (from s in context.SchoolMasters
                              where s.ParentSchoolId == 0 && s.StatusId == (int)UserStatus.Active
                              select new SchoolBo
                              {
                                  Id = s.Id,
                                  Name = s.Name
                              }).ToList();
            }

            return schoolList;
        }

        public static bool UpdateStatus(int id, int statusId)
        {
            using (var context = new EduErpEntities())
            {
                var school = context.SchoolMasters.FirstOrDefault(a => a.Id == id);
                if (school == null) return false;
                school.StatusId = statusId;
                context.SaveChanges();
                return true;
            }

        }


        public static List<StateMaster> GetStates()
        {
            List<StateMaster> states;
            using (var context = new EduErpEntities())
            {
                states = (from s in context.StateMasters
                          select new StateMaster
                          {
                              Id = s.Id,
                              Name = s.Name
                          }).ToList();
            }

            return states;
        }
        public static List<Common> GetInstituteTypeList()
        {
            List<Common> typList;
            using (var context = new EduErpEntities())
            {
                typList = (from s in context.KeyWordMasters
                           where s.Name=="InstituteType"
                          select new Common
                          {
                              Id = s.KeyWordId,
                              Name = s.KeyWordName
                          }).ToList();
            }

            return typList;
        }
        public static List<CityMaster> GetCities(int stateid)
        {
            List<CityMaster> cities;
            using (var context = new EduErpEntities())
            {

                cities = (from c in context.CityMasters
                          where c.StateId == stateid
                          select new CityMaster
                          {
                              Id = c.Id,
                              Name = c.Name
                          }).ToList();
            }
            return cities;
        }
        public static List<PlanBo> GetPlanList(int planTypeId)
        {
            List<PlanBo> plans;
            using (var context = new EduErpEntities())
            {

                plans = (from c in context.PlanMasters
                         where c.PlanTypeId == planTypeId
                          select new PlanBo()
                          {
                              Id = c.Id,
                              Name = c.Name
                          }).ToList();
            }
            return plans;
        }

        public static IEnumerable<Common> GetSchoolsList()
        {
            IEnumerable<Common> schools;
            using (var context = new EduErpEntities())
            {
                schools = (from s in context.SchoolMasters
                           where s.StatusId == (int)UserStatus.Active
                           select new Common
                           {
                               Id = s.Id,
                               Name = s.Name
                           }).ToList();

            }
            return schools;

        }

        public static bool UpdateProfilePic(int id, Guid? s3Key)
        {
            using (var context = new EduErpEntities())
            {
                var modelUser = context.SchoolMasters.FirstOrDefault(a => a.Id == id);
                if (modelUser == null) return false;
                modelUser.S3key = s3Key;
                context.SaveChanges();
                return true;
            }
        }
    }
}
