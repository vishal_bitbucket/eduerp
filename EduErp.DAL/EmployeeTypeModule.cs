﻿using System;
using System.Collections.Generic;
using System.Linq;
using EduErp.BO;
using EduErp.Model;
using EmployeeTypeMaster = EduErp.Model.EmployeeTypeMaster;

namespace EduErp.DAL
{
   public class EmployeeTypeModule
    {
       public static int AddEmployeeType(EmployeeTypeBo model)
       {

           using (var context = new EduErpEntities())
           {
               var result = context.EmployeeTypeMasters.FirstOrDefault(a => a.StatusId != (int)UserStatus.Deleted && a.EmployeeName.ToLower().Equals(model.EmployeeName.ToLower()) && a.SchoolId == model.SchoolId);
               if (result != null)
               {
                   return -1;
               }
               var emptype = new EmployeeTypeMaster
               {
                   EmployeeName = model.EmployeeName,
                   EmployeeGrade = model.EmployeeGrade,
                   CreatedBy = model.CreatedBy,
                   CreatedOn = model.CreatedOn,
                   StatusId = model.StatusId,
                   SchoolId = model.SchoolId
               };
               context.EmployeeTypeMasters.Add(emptype);
               context.SaveChanges();
           }
           return 1;
       }

       public static List<EmployeeTypeBo> GetEmployeeType(int schoolId)
       {
           var context = new EduErpEntities();
               var employeeTypeList = (from emptype in context.EmployeeTypeMasters
                   join sch in context.SchoolMasters on emptype.SchoolId equals sch.Id
                   where emptype.SchoolId == schoolId && emptype.StatusId != (int) UserStatus.Deleted
                   select new EmployeeTypeBo
                   {    Id =emptype.Id,
                       EmployeeName = emptype.EmployeeName,
                       EmployeeGrade = emptype.EmployeeGrade,
                       StatusId = emptype.StatusId,
                       SchoolName = sch.Name,
                       SchoolId = emptype.SchoolId

                   }).ToList();
           return employeeTypeList;
       }

       public static bool UpdateEmployeeType(EmployeeTypeBo model)
       {
           using (var context=new EduErpEntities())
           {
               var emptype = context.EmployeeTypeMasters.FirstOrDefault(a => a.Id == model.Id);
               if (emptype == null) return true;
               emptype.EmployeeName = model.EmployeeName;
               emptype.EmployeeGrade = model.EmployeeGrade;
               emptype.SchoolId = model.SchoolId;
               emptype.LastUpdatedBy = model.LastUpdatedBy;
               emptype.LastUpdatedOn = model.LastUpdatedOn;
               context.SaveChanges();
           }
           return true;
       }

       public static EmployeeTypeBo GetEmpType(int id)
       {
           var context = new EduErpEntities();
           var employeeTypeList = (from emptype in context.EmployeeTypeMasters
                                   where emptype.Id == id && emptype.StatusId != (int)UserStatus.Deleted
                                   select new EmployeeTypeBo
                                   {    Id = emptype.Id,
                                       EmployeeName = emptype.EmployeeName,
                                       EmployeeGrade = emptype.EmployeeGrade,
                                       StatusId = emptype.StatusId,
                                      SchoolId = emptype.SchoolId

                                   }).FirstOrDefault();
           return employeeTypeList;
       }

       public static bool UpdateStatus(int id, int status, int userId)
       {
           using (var context = new EduErpEntities())
           {
               var emptype = context.EmployeeTypeMasters.FirstOrDefault(a => a.Id == id);
               if (emptype == null) return false;
               emptype.StatusId = status;
               emptype.LastUpdatedOn = DateTime.Now;
               emptype.LastUpdatedBy = userId;
               context.SaveChanges();
               return true;

           }


       }
    }
}
