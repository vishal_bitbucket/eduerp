﻿using System;
using System.Collections.Generic;
using System.Data.Common.CommandTrees;
using System.Linq;
using EduErp.BO;
using EduErp.BO.Enumerators;
using EduErp.DAL.Utilities;
using EduErp.Model;
using EduErp.Mvc.Utilities;
using UserMaster = EduErp.Model.UserMaster;
using UserRight = EduErp.Model.UserRight;

namespace EduErp.DAL
{
    public class UserModule
    {
        public static int AddUser(UserBo user)
        {
            using (var context = new EduErpEntities())
            {
                var result = context.UserMasters.FirstOrDefault(a => a.StatusId != (int)UserStatus.Deleted && a.UserId.ToLower().Equals(user.UserId.ToLower()));
                if (result != null)
                {
                    return -1;
                }
                var result1 = context.UserMasters.FirstOrDefault(a => a.StatusId != (int)UserStatus.Deleted && a.Email.ToLower().Equals(user.Email.ToLower()) && a.SchoolId == user.SchoolId);
                if (result1 != null)
                {
                    return -2;
                }
                var result2 = context.UserMasters.FirstOrDefault(a => a.StatusId != (int)UserStatus.Deleted && a.Mobile.ToLower().Equals(user.MobileNo.ToLower()) && a.SchoolId == user.SchoolId);
                if (result2 != null)
                {
                    return -4;
                }
                var usedUser = MaxUserCount(user.SchoolId??0);
               var leftUser = LeftUser(user.SchoolId??0);
               if (leftUser ==0)
               {
                   return -3;
               }
               if (leftUser >=1)
               {
                   var updatedLeftUser = leftUser-1;
                   var accountDetail = context.AccountDetails.FirstOrDefault(a => a.SchoolId == user.SchoolId);
                   if (accountDetail != null)
                   {
                       accountDetail.LeftUserCount = updatedLeftUser;
                       context.SaveChanges();
                   }
               }
              
              
                var model = new UserMaster()
                {
                    Name = user.Name,
                    UserId = user.UserId,
                    SchoolId = user.SchoolId,
                    Email = user.Email,
                    Password = EncryptionHelper.EncryptPassword(user.Salt.ToString(), user.NewPassword),
                    PasswordSalt = user.Salt,
                    Mobile = user.MobileNo,
                    StatusId = user.StatusId,
                    CreatedBy = user.CreatedBy,
                    CreatedOn = user.CreatedOn,
                    UserTypeId = user.UserTypeId
                };
                context.UserMasters.Add(model);
                context.SaveChanges();
                var userId = model.Id;
                foreach (var menuItem in user.MenuList)
                {
                    if (!menuItem.IsSelected) continue;
                    var right = new UserRight
                    {
                        UserId = userId,
                        MenuId = menuItem.MenuId,
                        RightId = menuItem.RightId,
                        CreatedBy = user.CreatedBy ?? 1,
                        CreatedOn = user.CreatedOn
                    };
                    context.UserRights.Add(right);
                    context.SaveChanges();
                }
                return 1;
            }
        }

        public static List<UserBo> GetUsers(int schoolId)
        {
            List<UserBo> users;
            using (var context = new EduErpEntities())
            {
                if (schoolId != 0)
                {
                    users = (from u in context.UserMasters
                             where u.StatusId != (int)UserStatus.Deleted && u.SchoolId == schoolId
                             select new UserBo
                             {
                                 Id = u.Id,
                                 Name = u.Name,
                                 UserId = u.UserId,
                                 Email = u.Email,
                                 MobileNo = u.Mobile,
                                 LastModifiedBy = u.Id,
                                 LastModifiedOn = u.LastModifiedOn,
                                 CreatedBy = u.CreatedBy,
                                 CreatedOn = u.CreatedOn,
                                 StatusId = u.StatusId,
                                 UserTypeId = u.UserTypeId ?? 0
                             }).ToList();
                }
                else {
                    users = (from u in context.UserMasters
                             where u.StatusId != (int)UserStatus.Deleted
                             select new UserBo
                             {
                                 Id = u.Id,
                                 Name = u.Name,
                                 UserId = u.UserId,
                                 Email = u.Email,
                                 MobileNo = u.Mobile,
                                 LastModifiedBy = u.Id,
                                 LastModifiedOn = u.LastModifiedOn,
                                 CreatedBy = u.CreatedBy,
                                 CreatedOn = u.CreatedOn,
                                 StatusId = u.StatusId,
                                 UserTypeId = u.UserTypeId ?? 0
                             }).ToList();
                }

            }
            return users;
        }
        public static int MaxUserCount(int schoolId)
        {
            using (var context = new EduErpEntities())
            {
                var maxUserCount = (from spm in context.SchoolPlanMappings
                                       join pm in context.PlanMasters on spm.PlanId equals pm.Id
                                       where spm.SchoolId == schoolId
                                       select pm.MaxUserLimit ?? 0).FirstOrDefault();
                return maxUserCount;
            }
        }

        public static int LeftUser(int schoolId)
        {
            using (var context = new EduErpEntities())
            {
                var leftUserCount = (from ad in context.AccountDetails where ad.SchoolId == schoolId select ad.LeftUserCount).FirstOrDefault();
                return leftUserCount ?? 0;
            }

        }
        public static UserBo GetUser(int id)
        {
            UserBo user;
            using (var context = new EduErpEntities())
            {
                user = (from u in context.UserMasters
                        where u.Id == id && u.StatusId == (int)UserStatus.Active
                        select new UserBo()
                        {
                            Id = u.Id,
                            Name = u.Name,
                            UserId = u.UserId,
                            Email = u.Email,
                            SchoolId = u.SchoolId,
                            MobileNo = u.Mobile,
                            UserTypeId = u.UserTypeId??0
                        }).FirstOrDefault();
                var menuList = context.UserRights.Where(a => a.UserId == id).ToList();
                if (user != null)
                {
                    user.MenuList = new List<MenuItem>();
                    foreach (var right in menuList)
                    {
                        var menuItem = new MenuItem
                        {
                            MenuId = right.MenuId,
                            RightId = right.RightId,
                            IsSelected = true
                        };
                        user.MenuList.Add(menuItem);
                    }
                }
            }
            return user;
        }

        public static UserBo GetUserByEmail(string email)
        {
            var context = new EduErpEntities();
            var userDetailByMail = context.UserMasters.FirstOrDefault(u => u.Email.ToLower() == email.ToLower());
            UserBo result = null;
            if (userDetailByMail != null)
            {
                result = new UserBo
                {
                    Id = userDetailByMail.Id,
                    UserId = userDetailByMail.UserId,
                    UserTypeId = userDetailByMail.UserTypeId ?? 0,
                    Name = userDetailByMail.Name,
                    Email = userDetailByMail.Email,
                    MobileNo = userDetailByMail.Mobile,
                    SchoolId = userDetailByMail.SchoolId,
                    StatusId = userDetailByMail.StatusId,
                    IsPasswordCreated = userDetailByMail.IsPasswordCreated??false
                };
            }
            return result;
        }
        public static UserBo GetUserByNumber(string mobileNumber)
        {
            var context = new EduErpEntities();
            var userDetailByNumber = context.UserMasters.FirstOrDefault(u => u.Mobile == mobileNumber);
            UserBo result = null;
            if (userDetailByNumber != null)
            {
                result = new UserBo
                {
                    Id = userDetailByNumber.Id,
                    UserId = userDetailByNumber.UserId,
                    UserTypeId = userDetailByNumber.UserTypeId ?? 0,
                    Name = userDetailByNumber.Name,
                    Email = userDetailByNumber.Email,
                    MobileNo = userDetailByNumber.Mobile,
                    SchoolId = userDetailByNumber.SchoolId,
                    StatusId = userDetailByNumber.StatusId,
                    IsPasswordCreated = userDetailByNumber.IsPasswordCreated ?? false
                };
            }
            return result;
        }
        public static UserBo AuthenticateUserByOldPassword(int id,string password)
        {
            UserBo user;
            Guid salt=new Guid();
            string encryptedpassword = EncryptionHelper.EncryptPassword(salt.ToString(), password);
            using (var context=new EduErpEntities())
            {
                user = (from u in context.UserMasters
                        where u.Password == encryptedpassword && u.Id == id && u.StatusId == (int)UserStatus.Active
                    select new UserBo
                    {
                        Id = u.Id,
                        Name = u.Name,
                        Email = u.Email,
                        UserTypeId = u.UserTypeId??0
                    }).FirstOrDefault();
            }
            return user;
        }
        public static UserBo GetUserByVerificationCode(Guid verificationCode)
        {
            UserBo userByCode;
            using (var context=new EduErpEntities())
            {
                userByCode = (from u in context.UserMasters
                    where u.VerificationCode == verificationCode && u.StatusId==(int)UserStatus.Active
                    select new UserBo
                    {
                        Id = u.Id,
                        UserId = u.UserId,
                        UserTypeId = u.UserTypeId??0,
                        Name = u.Name,
                        VerificationCode = u.VerificationCode??Guid.Empty,
                        IsPasswordCreated = u.IsPasswordCreated??false,
                        StatusId = u.StatusId,
                        SchoolId = u.SchoolId,
                        VerificationCodeGeneratedOnUtc = u.VerificationCodeGeneratedOnUtc??DateTime.Now
                        }).FirstOrDefault();
            }
            return userByCode;
        }
        public static UserBo GetUserByOtp(string otp)
        {
            UserBo userByOtp;
            using (var context = new EduErpEntities())
            {
                userByOtp = (from u in context.UserMasters
                             where u.OneTimePassword == otp && u.StatusId == (int)UserStatus.Active
                              select new UserBo
                              {
                                  Id = u.Id,
                                  UserId = u.UserId,
                                  UserTypeId = u.UserTypeId ?? 0,
                                  Name = u.Name,
                                  VerificationCode = u.VerificationCode ?? Guid.Empty,
                                  IsPasswordCreated = u.IsPasswordCreated ?? false,
                                  StatusId = u.StatusId,
                                  SchoolId = u.SchoolId,
                                  Otp = u.OneTimePassword,
                                  VerificationCodeGeneratedOnUtc = u.VerificationCodeGeneratedOnUtc ?? DateTime.Now
                              }).FirstOrDefault();
            }
            return userByOtp;
        }
        public static Guid UpdateVerificationCode(int id ,Guid verificationCode)
        {
            var context = new EduErpEntities();
            var user = context.UserMasters.FirstOrDefault(a => a.Id == id);
            if (user != null)
            {
                user.VerificationCode = verificationCode;
                user.VerificationCodeGeneratedOnUtc = DateTime.Now;
                context.SaveChanges();
            }
            if (user != null) return user.VerificationCode??new Guid();
            return verificationCode;
        }
        public static string UpdateOtp(int id, string otp)
        {
            var context = new EduErpEntities();
            var user = context.UserMasters.FirstOrDefault(a => a.Id == id);
            if (user != null)
            {
                user.OneTimePassword = Convert.ToString(otp);
                user.VerificationCodeGeneratedOnUtc = DateTime.Now;
                context.SaveChanges();
            }
            if (user != null) return user.OneTimePassword ??Convert.ToString(new Random());
            return otp;
        }

        public static bool ForgetPassWordCreateNew(string userId,string password)
        {
            var context = new EduErpEntities();
            var user = context.UserMasters.FirstOrDefault(a => a.UserId == userId);
            if (user == null) return false;
            user.PasswordSalt = new Guid();
            user.Password = EncryptionHelper.EncryptPassword(user.PasswordSalt.ToString(), password);
            context.SaveChanges();
            return true;
        }

        public static bool ChangePassword(int userId, string password)
        {
            var salt=new Guid();
            var encryptedPassword = EncryptionHelper.EncryptPassword(salt.ToString(), password);
            var context = new EduErpEntities();
            var user = context.UserMasters.FirstOrDefault(a => a.Id == userId);
            if (user == null) return false;
            user.PasswordSalt = salt;
            user.Password = encryptedPassword;
            context.SaveChanges();
            return true;
        }
        public static bool Update(UserBo model)
        {
            var context = new EduErpEntities();
            var user = context.UserMasters.FirstOrDefault(u => u.Id == model.Id);
            if (user == null) return false;
            user.Id = model.Id;
            user.UserId = model.UserId;
            user.Name = model.Name;
            user.SchoolId = model.SchoolId;
            user.Email = model.Email;
            user.Mobile = model.MobileNo;
            user.SchoolId = model.SchoolId;
            user.UserTypeId = model.UserTypeId;
            foreach (var menuItem in model.MenuList)
            {
                var oldRight = context.UserRights.FirstOrDefault(a => a.UserId == model.Id && a.MenuId == menuItem.MenuId);
                if (oldRight != null)
                {
                    context.UserRights.Remove(oldRight);
                    context.SaveChanges();
                }
                if (!menuItem.IsSelected) continue;
                var right = new UserRight
                {
                    UserId = model.Id,
                    MenuId = menuItem.MenuId,
                    RightId = menuItem.RightId,
                    CreatedBy = model.LastModifiedBy ?? 1,
                    CreatedOn = model.LastModifiedOn ?? DateTime.Now
                };
                context.UserRights.Add(right);
                context.SaveChanges();
            }
            context.SaveChanges();
            return true;
        }

        public static bool Delete(UserBo model)
        {
            var context = new EduErpEntities();
            var user = context.UserMasters.SingleOrDefault(a => a.Id == model.Id);
            if (user != null)
            {
                user.StatusId = Convert.ToInt32(UserStatus.Deleted);
            }
            context.SaveChanges();

            return true;
        }

        public static bool UpdateStatus(int id, int statusId, int userId)
        {
            using (var context = new EduErpEntities())
            {
                var user = context.UserMasters.SingleOrDefault(a => a.Id == id);
                if (user == null) return false;
                user.StatusId = statusId;
                user.LastModifiedOn = DateTime.Now;
                user.LatModifiedBy = userId;
                context.SaveChanges();
                return true;
            }

        }

        public static List<Common> GetUserType()
        {
            List<Common> useList;
            using (var context = new EduErpEntities())
            {
                useList = (from u in context.KeyWordMasters
                          where u.Name == "UserType" && u.KeyWordId!=0 
                          select new Common
                          {
                              Id = u.KeyWordId,
                              Name = u.KeyWordName
                          }).ToList();
            }
            return useList;
        }

        public static List<Common> GetRights()
        {
            List<Common> result;
            using (var context = new EduErpEntities())
            {
                result = (from u in context.KeyWordMasters
                          where u.Name == "Right"
                          select new Common
                          {
                              Id = u.KeyWordId,
                              Name = u.KeyWordName
                          }).ToList();
            }
            return result;
        }

        public static List<MenuItem> GetMenu()
        {
            List<MenuItem> result;
            using (var context = new EduErpEntities())
            {
                result = (from u in context.MenuMasters
                          where u.IsSuperAdmin == false
                          select new MenuItem
                          {
                              MenuId = u.Id,
                              MenuName = u.Name,
                              IsSelected = false,
                              RightId = 0,
                              OrderId = u.MenuOrder
                          }).OrderBy(b => b.OrderId).ToList();
            }
            return result;
        }

        private static List<PermissionTree> BuildPermissionTree(List<BO.MenuMaster> permissions, IEnumerable<BO.MenuMaster> parents, List<int> permissionId)
        {
            var result = new List<PermissionTree>();
            foreach (var parent in parents)
            {
                var item = new PermissionTree() { Permission = parent, IsSelected = permissionId.Any(a => a == parent.Id) };
                var children = permissions.Where(a => a.ParentId == parent.Id).ToList();
                item.Children = BuildPermissionTree(permissions, children, permissionId);
                result.Add(item);
            }
            return result;
        }

        public static Permission GetPermissionByUserId(int userId, int schoolId)
        {
            var result = new Permission();
            using (var context = new EduErpEntities())
            {
                var menu = (from u in context.MenuMasters
                    join ur in context.UserRights on u.Id equals ur.MenuId
                    where ur.UserId == userId
                    select new MenuMasterBo
                    {
                        Id = u.Id,
                        ActionName = u.ActionName,
                        ClassName = u.ClassName,
                        Controller = u.Controller,
                        IconClass = u.IconClass,
                        IsSuperAdmin = u.IsSuperAdmin,
                        MenuOrder = u.MenuOrder,
                        Name = u.Name,
                        StatusId = u.StatusId,
                        ParentId = u.ParentId,
                        IsAjax = u.IsAjax
                    }).OrderBy(b => b.MenuOrder).ToList();
                foreach (var i in menu)
                {
                    switch (i.Id)
                    {
                       
                        case (int)MenuMasters.User:
                            i.Count = context.UserMasters.Count(a => a.SchoolId == schoolId && a.StatusId == (int)UserStatus.Active);
                            break;
                        case (int)MenuMasters.Session:
                            i.Count = context.SessionMasters.Count(a => a.SchoolId == schoolId && a.StatusId == (int)UserStatus.Active);
                            break;
                        case (int)MenuMasters.Class:
                            i.Count = context.ClassMasters.Count(a => a.SchoolId == schoolId && a.Status == (int)UserStatus.Active);
                            break;
                        case (int)MenuMasters.Subject:
                            i.Count = context.SubjectMasters.Count(a => a.SchoolId == schoolId && a.Status == (int)UserStatus.Active);
                            break;
                        case (int)MenuMasters.Holiday:
                            i.Count = context.HolidayMasters.Count(a => a.SchoolId == schoolId && a.StatusId == (int)UserStatus.Active);
                            break;
                        case (int)MenuMasters.Section:
                            i.Count = (from sm in context.SectionMasters join cm in context.ClassMasters on sm.ClassId equals cm.Id where cm.SchoolId == SessionItems.SchoolId && sm.Status==(int)UserStatus.Active select sm.Id).Count();
                            break;
                        case (int)MenuMasters.FeeHead:
                            i.Count = context.FeeHeadMasters.Count(a => a.SchoolId == schoolId && a.StatusId == (int)UserStatus.Active);
                            break;
                        case (int)MenuMasters.EmployeeType:
                            i.Count = context.EmployeeTypeMasters.Count(a => a.SchoolId == schoolId && a.StatusId == (int)UserStatus.Active);
                            break;
                        case (int)MenuMasters.Item:
                            i.Count = context.ItemMasters.Count(a => a.SchoolId == schoolId && a.StatusId == (int)UserStatus.Active);
                            break;
                        case (int)MenuMasters.Vendor:
                            i.Count = context.VendorMasters.Count(a => a.SchoolId == schoolId && a.StatusId == (int)UserStatus.Active);
                            break;
                        case (int)MenuMasters.Exam:
                            i.Count = context.ExamMasters.Count(a => a.SchoolId == schoolId && a.StatusId == (int)UserStatus.Active);
                            break;
                        case (int)MenuMasters.OnlineTest:
                            i.Count = context.OnlineTestMasters.Count( a => a.SchoolId == schoolId && a.StatusId == (int) UserStatus.Active);
                            break;
                        case (int)MenuMasters.Certificate:
                            i.Count = (from ctm in context.CertificateTemplateMasters join ctsm in context.CertificateTemplateSchoolMappings on ctm.Id equals ctsm.CertificateTemplateId where ctsm.SchoolId == SessionItems.SchoolId && ctsm.StatusId == (int)UserStatus.Active select ctm.Id).Count();
                            break;
                        case (int)MenuMasters.StudentReport:
                            i.Count = (from s in context.Students join scm in context.StudentClassMappings on s.Id equals scm.StudentId where s.SchoolId == SessionItems.SchoolId && s.StatusId == (int)UserStatus.Active select s.Id).Count();
                            break;
                        case (int)MenuMasters.ExamReport:
                            i.Count = context.ExamMasters.Count(a => a.SchoolId == schoolId && a.StatusId == (int)UserStatus.Active);
                            break;
                        case (int)MenuMasters.FeesReport:
                            i.Count = context.FeeTransactions.Count(a => a.StatusId == (int)UserStatus.Active);
                            break;
                        case (int)MenuMasters.AttendenceReport:
                            i.Count = context.AttendanceMasters.Count(a => a.SchoolId == schoolId && a.DayStatusId == (int)DayStatus.Present);
                            break;
                        case (int)MenuMasters.ExpenseReport:
                            i.Count = context.Inventories.Count(a => a.SchoolId == schoolId && a.StatusId == (int)UserStatus.Active);
                            break;
                        case (int)MenuMasters.OnlineTestQuestion:
                            i.Count = context.OnlineTestQuestionMasters.Count(a => a.SchoolId == schoolId && a.StatusId == (int)UserStatus.Active);
                           break;
                        case (int)MenuMasters.OnlineTestSubject:
                           i.Count = context.OnlineTestSubjectMasters.Count(a => a.SchoolId == schoolId && a.StatusId == (int)UserStatus.Active);
                           break;
                        case (int)MenuMasters.OTExamSubjectMapping:
                           i.Count = context.OTExamSubjectMappings.Count(a => a.SchoolId == schoolId && a.StatusId == (int)UserStatus.Active);
                           break;
                        case (int)MenuMasters.OTQuestionAnswerMapping:
                           i.Count = context.OTQuestionAnswerMappings.Count(a => a.SchoolId == schoolId && a.StatusId == (int)UserStatus.Active);
                           break;
                        case (int)MenuMasters.OTExamQuestionMapping:
                           i.Count = context.OTExamQuestionMappings.Count(a => a.SchoolId == schoolId && a.StatusId == (int)UserStatus.Active);
                           break;
                        
                    }
                }

                result.MenuList = menu; //BuildPermissionTree(menu, menu.Where(a => a.ParentId == null), new List<int>());
                result.UserRights = (from ur in context.UserRights
                    where ur.UserId == userId
                    select new BO.UserRight
                    {
                        Id = ur.Id,
                        MenuId = ur.MenuId,
                        RightId = ur.RightId,
                        UserId = ur.UserId
                    }).ToList();
            }
            return result;
        }

    }
}
