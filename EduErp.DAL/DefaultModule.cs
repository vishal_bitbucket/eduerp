﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using EduErp.BO;
using EduErp.DAL.Utilities;
using EduErp.Model;

namespace EduErp.DAL
{ 
    public class DefaultModule
    {
        public static List<CountDetails> GetCounts(int schoolId)
        {
            var model = new List<CountDetails>();
            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                using (var dr = spdb.ExecuteReader("GetCountDetails", CommandType.StoredProcedure))
                {
                    var drFeeHead = new DataReaderObjectLoader<CountDetails>(dr, false);
                    while (dr.Read())
                    {
                        var objec = drFeeHead.GetObjectFromDataReader(dr, true);
                        model.Add(objec);
                    }
                }
                return model;
            }
        }
        public static List<FeeAndExpenseDetail> GetExpeneDetail(int schoolId,int sessionId)
        {
            var detail = new List<FeeAndExpenseDetail>();
            using(var spdb=new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                spdb.AddParameter("@SessionId", sessionId);
                using (var dr = spdb.ExecuteReader("GetTotalFeeAndExpensesForChart", CommandType.StoredProcedure))
                {
                    var drDetail = new DataReaderObjectLoader<FeeAndExpenseDetail>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drDetail.GetObjectFromDataReader(dr, true);
                        detail.Add(obj);
                    }
                }
                return detail;
            }
        }


        public static BO.ConfigurationBo GetConfigurationCounts(int schoolId)
        {
            var model = new List<BO.ConfigurationBo>();
            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                using (var dr = spdb.ExecuteReader("GetConfigurationCountDetails", CommandType.StoredProcedure))
                {
                    var drFeeHead = new DataReaderObjectLoader<BO.ConfigurationBo>(dr, false);
                    while (dr.Read())
                    {
                        var objec = drFeeHead.GetObjectFromDataReader(dr, true);
                        model.Add(objec);
                    }
                }
                return model.FirstOrDefault();
            }
        }

        public static List<TodayBirthDetailList> TodayBirthDay(int schoolId)
        {
            var list = new List<TodayBirthDetailList>();
            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                using (var dr = spdb.ExecuteReader("GetTodaysBirthDay", CommandType.StoredProcedure))
                {
                    var birthday = new DataReaderObjectLoader<TodayBirthDetailList>(dr, false);
                    while (dr.Read())
                    {
                        var objec = birthday.GetObjectFromDataReader(dr, true);
                        list.Add(objec);
                    }
                }
                return list;
            }
        }

        public static List<ClassStudentChartBo> ClassStudentList(int schoolId)
        {
            var list = new List<ClassStudentChartBo>();
            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                using (var dr = spdb.ExecuteReader("GetStudentClassChart", CommandType.StoredProcedure))
                {
                    var countList = new DataReaderObjectLoader<ClassStudentChartBo>(dr, false);
                    while (dr.Read())
                    {
                        var objec = countList.GetObjectFromDataReader(dr, true);
                        list.Add(objec);
                    }
                }
                return list;
            }
        }

        public static List<UpcomingBirthDetailList> UpcomingBirthDay(int schoolId)
        {
            var list = new List<UpcomingBirthDetailList>();
            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                using (var dr = spdb.ExecuteReader("GetUpcomingBirthDay", CommandType.StoredProcedure))
                {
                    var birthday = new DataReaderObjectLoader<UpcomingBirthDetailList>(dr, false);
                    while (dr.Read())
                    {
                        var objec = birthday.GetObjectFromDataReader(dr, true);
                        list.Add(objec);
                    }
                }
                return list;
            }
        }


        public static List<GeneralTypeNotificationList> GeneralNotice(int schoolId)
        {
            List<GeneralTypeNotificationList> generalTypeNotification;
            using (var context = new EduErpEntities())
            {
                generalTypeNotification = (from nm in context.NoticeMasters
                                           where
                                               nm.StatusId == (int)UserStatus.Active && nm.NoticeTypeId == 1 &&
                                               nm.SchoolId == schoolId
                                           select new GeneralTypeNotificationList
                                           {
                                               
                                               Subject = nm.Subject,
                                               Description = nm.Description
                                           }).ToList();

            }
            return generalTypeNotification;
        }
        public static List<EmployeeTypeNotificationList> EmployeeNotice(int schoolId)
        {
            List<EmployeeTypeNotificationList> employeeTypeNotification;
            using (var context = new EduErpEntities())
            {
                employeeTypeNotification = (from nm in context.NoticeMasters
                                            where
                                                nm.StatusId == (int)UserStatus.Active && nm.NoticeTypeId == 2 &&
                                                nm.SchoolId == schoolId
                                            select new EmployeeTypeNotificationList
                                           {
                                               NoticeDate = nm.CreatedOn,
                                               Subject = nm.Subject,
                                               Description = nm.Description
                                           }).ToList();

            }
            return employeeTypeNotification;
        }

        public static List<StudentTypeNotificationList> StudentNotice(int schoolId)
        {
            List<StudentTypeNotificationList> studentTypeNotification;
            using (var context = new EduErpEntities())
            {
                studentTypeNotification = (from nm in context.NoticeMasters
                                           where
                                               nm.StatusId == (int)UserStatus.Active && nm.NoticeTypeId == 4 &&
                                               nm.SchoolId == schoolId
                                           select new StudentTypeNotificationList
                                           {
                                               Subject = nm.Subject,
                                               Description = nm.Description
                                           }).ToList();

            }
            return studentTypeNotification;
        }
        public static List<SchoolTypeNotificationList> SchoolNotice(int schoolId)
        {
            List<SchoolTypeNotificationList> schoolTypeNotification;
            using (var context = new EduErpEntities())
            {
                schoolTypeNotification = (from nm in context.NoticeMasters
                                          where
                                              nm.StatusId == (int)UserStatus.Active && nm.NoticeTypeId == 3 &&
                                              nm.SchoolId == schoolId
                                          select new SchoolTypeNotificationList
                                          {
                                              Subject = nm.Subject,
                                              Description = nm.Description
                                          }).ToList();

            }
            return schoolTypeNotification;
        }
    }
}
