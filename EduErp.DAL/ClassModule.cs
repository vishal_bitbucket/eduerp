﻿using System;
using System.Collections.Generic;
using System.Linq;
using EduErp.BO;
using EduErp.Model;
using ClassMaster = EduErp.Model.ClassMaster;

namespace EduErp.DAL
{
    public class ClassModule
    {

        public static int AddClass(ClassBo classUser)
        {
            using (var context = new EduErpEntities())
            {
                var result = context.ClassMasters.FirstOrDefault(a => a.Status != (int)UserStatus.Deleted && a.Name.ToLower().Equals(classUser.Name.ToLower()) && a.SchoolId == classUser.SchoolId);
                if (result != null)
                {
                    return -1;
                }
                var model = new ClassMaster()
                {

                    Name = classUser.Name,
                    CreatedOn = classUser.CreatedOn,
                    CreatedBy = classUser.CreatedBy,
                    SchoolId = classUser.SchoolId,
                    Status = classUser.Status

                };
                context.ClassMasters.Add(model);
                context.SaveChanges();
                return 1;

            }

        }

        public static List<ClassBo> GetClasses(int schoolId)
        {


            List<ClassBo> clss;
            using (var context = new EduErpEntities())
            {
                clss = (from s in context.ClassMasters
                        where s.Status != (int)UserStatus.Deleted && (s.SchoolId == schoolId || s.SchoolId == 0)

                        select new ClassBo
                        {
                            Id = s.Id,
                            SchoolId = s.SchoolId,
                            Name = s.Name,
                            Status = s.Status
                        }).ToList();
            }
            return clss;
        }

        public static bool UpdateClass(ClassBo model)
        {

            using (var context = new EduErpEntities())
            {
                var clss = context.ClassMasters.SingleOrDefault(a => a.Id == model.Id);

                if (clss != null)
                {
                    clss.Id = model.Id;
                    clss.Name = model.Name;
                    clss.SchoolId = model.SchoolId;
                    clss.LastUpdatedBy = model.LastUpdatedBy;
                    clss.LastUpdatedOn = model.LastUpdatedOn;
                    clss.Status = model.Status;
                    clss.SchoolId = model.SchoolId;
                    context.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public static ClassBo GetClassById(int id)
        {
            ClassBo clss;
            using (var context = new EduErpEntities())
            {
                clss = (from c in context.ClassMasters
                        where id == c.Id

                        select new ClassBo()
                        {

                            Name = c.Name,
                            SchoolId = c.SchoolId,
                            Status = c.Status
                        }).FirstOrDefault();
            }
            return clss;
        }


        public static bool UpdateStatus(int id, int status, int userId)
        {
            using (var context = new EduErpEntities())
            {
                var clss = context.ClassMasters.FirstOrDefault(a => a.Id == id);
                if (clss == null) return false;
                clss.Status = status;
                clss.LastUpdatedOn = DateTime.Now;
                clss.LastUpdatedBy = userId;
                context.SaveChanges();
                return true;

            }


        }




    }
}
