﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EduErp.BO;
using EduErp.Model;
using System.Data;
using EduErp.DAL.Utilities;

namespace EduErp.DAL
{
   public class CertificateModule
    {

       public static bool AddMapping(CertificationBo model)
       {
           using (var context = new EduErpEntities())
           {
               if (model.MappingList != null)
               { 
                   foreach(var item in model.MappingList)
                   {
                       var mappingdetail = new Model.CertificateTemplateSchoolMapping
                       {
                           CertificateTemplateId=item.TemplateId,
                           CreatedBy=model.CreatedBy,
                           CreatedOn=model.CreatedOn,
                           StatusId=model.StatusId,
                           SchoolId=model.SchoolId                         
                       };
                       context.CertificateTemplateSchoolMappings.Add(mappingdetail);
                       context.SaveChanges();
                  }
               }
               return true;
           }
       }
       public static bool UpdateCertificate(CertificationBo model)
       {
           using (var context = new EduErpEntities())
           {
               var certificate = context.CertificateTemplateSchoolMappings.SingleOrDefault(a => a.Id == model.Id);
               if (certificate != null)
               {
                   certificate.CertificateTemplateId = model.CertificateTemplateId;
                   certificate.LastUpdatedBy = model.LastUpdatedBy;
                   certificate.LastUpdatedOn = model.LastUpdatedOn;
                   context.SaveChanges();
                   return true;
               }
               return false;
           }
       }

       public static CertificationBo GetCertificateById(int id,int schoolId)
       {
           CertificationBo certificateList;
           using (var context = new EduErpEntities())
           {
               certificateList = (from ctsm in context.CertificateTemplateSchoolMappings
                                  join ctm in context.CertificateTemplateMasters on ctsm.CertificateTemplateId equals ctm.Id
                                  join km in context.KeyWordMasters on ctm.TemplateTypeId equals km.KeyWordId
                                  where ctsm.SchoolId == schoolId && km.Name == "TemplateType" && ctsm.Id==id
                                  select new CertificationBo
                                  {
                                      Id = ctsm.Id,
                                      CertificateTemplateTypeId=ctm.TemplateTypeId,
                                      CertificateTemplateId = ctsm.CertificateTemplateId,
                                      StatusId = ctsm.StatusId,
                                      CertificateTemplateTypeName = km.KeyWordName,
                                      CertificateTemplateName = ctm.TemplateName
                                  }).FirstOrDefault(); 
           }
           return certificateList;
       }



       public static List<CertificationBo> GetCerticicateList(int schoolId)
       {
           List<CertificationBo> certificateList;
           using (var context = new EduErpEntities())
           {
               certificateList = (from ctsm in context.CertificateTemplateSchoolMappings
                                  join ctm in context.CertificateTemplateMasters on ctsm.CertificateTemplateId equals ctm.Id
                                  join km in context.KeyWordMasters on ctm.TemplateTypeId equals km.KeyWordId
                                  where ctsm.SchoolId == schoolId && km.Name == "TemplateType" && ctsm.StatusId!=(int)UserStatus.Deleted
                                  select new CertificationBo
                                  {
                                     Id=ctsm.Id,
                                     CertificateTemplateId=ctsm.CertificateTemplateId,
                                     StatusId=ctsm.StatusId,
                                     CertificateTemplateTypeName=km.KeyWordName,
                                     CertificateTemplateName=ctm.TemplateName
                                  }).ToList(); 
           }
           return certificateList;
       }
       public static List<Common> GetCertificateTemplateTypeList()
       {
           List<Common>TemplateTypeList;
           using(var context=new EduErpEntities())
           {
           TemplateTypeList=(from km in context.KeyWordMasters where km.Name=="TemplateType"
                                 select new Common{
                                                                     
                                     Id=km.KeyWordId,
                                     Name=km.KeyWordName
                                 }).ToList();
           }
           return TemplateTypeList;
       }

       public static List<Common> GetTemplateList(int schoolId)
       {
           List<Common> certificateList;
           using (var context = new EduErpEntities())
           {
               certificateList = (from ctsm in context.CertificateTemplateSchoolMappings
                                  join ctm in context.CertificateTemplateMasters on ctsm.CertificateTemplateId equals ctm.Id
                                  join km in context.KeyWordMasters on ctm.TemplateTypeId equals km.KeyWordId
                                  where ctsm.SchoolId == schoolId && km.Name == "TemplateType" && ctsm.StatusId == (int)UserStatus.Active
                                  select new Common
                                  {
                                      Id = ctsm.CertificateTemplateId??0,                             
                                      Name = ctm.TemplateName
                                  }).ToList();
           }
           return certificateList;
       }

       public static StudentBo GetStudentDetailForCertificate(int sessionId, int classId, int sectionId, int studentId)
       {
           var student = new List<StudentBo>();
           using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
           {
               spdb.ClearParameters();
               spdb.AddParameter("@SectionId", sectionId);
               spdb.AddParameter("@SessionId", sessionId);
               spdb.AddParameter("@ClassId", classId);
               spdb.AddParameter("@StudentId", studentId);
               using (var dr = spdb.ExecuteReader("GetStudentDetailForCertificate", CommandType.StoredProcedure))
               {
                   var drgetter = new DataReaderObjectLoader<StudentBo>(dr, false);
                   while (dr.Read())
                   {
                       var obj = drgetter.GetObjectFromDataReader(dr, true);//use tool to load object from dr.row
                       student.Add(obj);
                   }
               }
               return student.FirstOrDefault();
           }
       }
       public static List<Common> CertificateTemplateName(int templateTypeId)
       {
       List<Common>TemplateNameList;
           using(var context=new EduErpEntities())
           {
           TemplateNameList=(from ctm in context.CertificateTemplateMasters where ctm.TemplateTypeId==templateTypeId
                                 select new Common{
                                                                     
                                     Id=ctm.Id,
                                     Name=ctm.TemplateName
                                 }).ToList();
           }
           return TemplateNameList;
       }

       public static string GetTextTemplate(int templateId, int schoolId)
       {
           string textTemplate;
           using (var context = new EduErpEntities())
           {
               textTemplate = (from ctm in context.CertificateTemplateMasters
                               join ctsm in context.CertificateTemplateSchoolMappings on ctm.Id equals ctsm.CertificateTemplateId
                               where ctsm.SchoolId == schoolId && ctm.Id == templateId && ctsm.StatusId == (int)UserStatus.Active
                               select ctm.HtmlTemplate).FirstOrDefault();
           }
           return textTemplate.ToString();
       }

       public static bool UpdateStatus(int id, int status, int userId)
       {
           using (var context = new EduErpEntities())
           {
               var certype = context.CertificateTemplateSchoolMappings.FirstOrDefault(a => a.Id == id);
               if (certype == null) return false;
               certype.StatusId = status;
               certype.LastUpdatedOn = DateTime.Now;
               certype.LastUpdatedBy = userId;
               context.SaveChanges();
               return true;

           }


       }
    }
}
