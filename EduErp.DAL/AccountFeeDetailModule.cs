﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using EduErp.BO;
using EduErp.DAL.Utilities;
using EduErp.Model;
using AccountFeeDetail = EduErp.Model.AccountFeeDetail;

namespace EduErp.DAL
{
    public class AccountFeeDetailModule
    {

        public static bool AddAccountFeeDetail(AccountFeeDtailBo account)
        {
            using (var context = new EduErpEntities())
            {
                var model = new AccountFeeDetail
                {
                    SchoolId = account.SchoolId,
                    PaymentModeId = account.PaymentModeId,
                    PaymentAmount = account.PaymentAmount,
                    PaymentDate = account.PaymentDate,
                    FromDate = account.FromDate,
                    ToDate = account.ToDate,
                    PlanId = account.PlanId,
                    StatusId = account.StatusId,
                    CreatedOn = account.CreatedOn,
                    CreatedBy = account.CreatedBy
                };
                context.AccountFeeDetails.Add(model);
                context.SaveChanges();
            }
            return true;
        }
        public static AccountFeeDtailBo GetAccountFeeDetailById(int id)
        {
            AccountFeeDtailBo account;
            using (var context = new EduErpEntities())
            {
                account = (from ac in context.AccountFeeDetails
                           where ac.Id == id
                           select new AccountFeeDtailBo
                           {
                               Id = ac.Id,
                               SchoolId = ac.SchoolId,
                               PaymentAmount = ac.PaymentAmount,
                               PaymentDate = ac.PaymentDate,
                               PaymentModeId = ac.PaymentModeId,
                               FromDate = ac.FromDate,
                               ToDate = ac.ToDate,
                               StatusId = ac.StatusId,
                               PlanId = ac.PlanId
                           }).FirstOrDefault();
            }
            return account;
        }

        public static bool UpdateAccountFeeDetail(AccountFeeDtailBo model)
        {
            using (var context = new EduErpEntities())
            {
                var account = context.AccountFeeDetails.FirstOrDefault(a => a.Id == model.Id);
                if (account != null)
                {
                    account.SchoolId = model.SchoolId;
                    account.PaymentDate = model.PaymentDate;
                    account.PaymentAmount = model.PaymentAmount;
                    account.PaymentModeId = model.PaymentModeId;
                    account.FromDate = model.FromDate;
                    account.ToDate = model.ToDate;
                    account.UpadatedBy = model.UpadatedBy;
                    account.UpdatedOn = model.UpdatedOn;
                    account.PlanId = model.PlanId;
                    context.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public static List<AccountFeeDtailBo> GetAccountFeeDetails()
        {
            List<AccountFeeDtailBo> details = new List<AccountFeeDtailBo>();
            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
               // spdb.ClearParameters();
                //spdb.AddParameter("@SchoolId", schoolId);
                using (var dr = spdb.ExecuteReader("GetAccountFeeDetail", CommandType.StoredProcedure))
                {
                    var drgetter = new DataReaderObjectLoader<AccountFeeDtailBo>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true);//use tool to load object from dr.row
                        details.Add(obj);
                    }
                }

            }
            return details;
        }

        public static List<Common> GetPlans(int schoolId)
        {
            List<Common> plans;
            using (var context = new EduErpEntities())
            {

                plans = (from c in context.AccountDetails
                         join p in context.PlanMasters on c.PlanId equals p.Id
                         where c.SchoolId == schoolId && c.StatusId == (int)UserStatus.Active
                         select new Common
                          {
                              Id = p.Id,
                              Name = p.Name

                          }).ToList();
            }
            return plans;
        }

        public static List<Common> GetPaymentMode()
        {
            List<Common> paymentmodeList;
            using (var context = new EduErpEntities())
            {
                paymentmodeList = (from p in context.KeyWordMasters
                                   where p.Name == "PaymentMode"
                                   select new Common
                                   {
                                       Id = p.KeyWordId,
                                       Name = p.KeyWordName
                                   }).ToList();
            }
            return paymentmodeList;
        }

        public static bool UpdateStatus(int id, int statusid, int userId)
        {
            using (var context = new EduErpEntities())
            {
                var account = context.AccountFeeDetails.FirstOrDefault(a => a.Id == id);
                if (account == null) return false;
                account.StatusId = statusid;
                account.UpdatedOn = DateTime.Now;
                account.UpadatedBy = userId;
                context.SaveChanges();
                return true;
            }
        }
    }
}
