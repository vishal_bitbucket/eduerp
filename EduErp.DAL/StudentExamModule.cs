﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using EduErp.BO;
using EduErp.DAL.Utilities;
using EduErp.Model;
using EduErp.Mvc.Utilities;
using StudentExamMapping = EduErp.Model.StudentExamMapping;

namespace EduErp.DAL
{
    public class StudentExamModule
    {

        public static bool AddStudentExamMapping(StudentExamMappingBo model )
        {
            using (var context=new EduErpEntities())
            {
                if (model.StudentExamLists.Count != 0)
                {
                    foreach (var item in model.StudentExamLists)
                    {
                        var examDetail = new StudentExamMapping
                        {
                            ClassId = model.ClassId,
                            SectionId = model.SectionId,
                            StudentId = model.StudentId,
                            ExamSubjectMappingId = item.ExamSubjectMappingId,
                            ObtainedMarks = item.ObtainMarks,
                            CreatedBy = model.CreatedBy,
                            CreatedOn = model.CreatedOn,
                            StatusId = model.StatusId
                        };
                        context.StudentExamMappings.Add(examDetail);
                        context.SaveChanges();
                    }
                }
                return true;
            }
        }



        public static List<Common> ExamTypeList(int schoolId)
        {
            List<Common> examtype;
            using (var context = new EduErpEntities())
            {
                examtype = (from exm in context.ExamMasters where
                                exm.SchoolId == schoolId && exm.StatusId == (int)UserStatus.Active
                                 select new Common
                                 {
                                   Id = exm.Id,
                                   Name = exm.Name
                                   }).ToList();
            }
            return examtype;
        }

        public static List<StudentExamList> GetExamMappingDetail(int examTypeId, int studentId, int schoolId)
        {
            var studentsList = new List<StudentExamList>();

            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                spdb.AddParameter("@StudentId", studentId);
                spdb.AddParameter("@ExamTypeId", examTypeId);
                using (var dr = spdb.ExecuteReader("GetStudentMappingList", CommandType.StoredProcedure))
                {
                    var drgetter = new DataReaderObjectLoader<StudentExamList>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true);//use tool to load object from dr.row
                        studentsList.Add(obj);
                    }
                }

            }
            return studentsList;
        }

        public static StudentExamMappingBo GetStudentExamDetailById(int schoolId, int examId, int studentId)
        {
            StudentExamMappingBo mappingResult;
           
            using (var context= new EduErpEntities())
            {
                mappingResult = (from sem in context.StudentExamMappings join s in context.Students on sem.StudentId equals s.Id
                                 join esm in context.ExamSubjectMappings on sem.ExamSubjectMappingId equals esm.Id
                    where esm.ExamId == examId && sem.StudentId == studentId && s.SchoolId==schoolId
                    select new StudentExamMappingBo
                    {
                        ExamId = esm.ExamId,
                        ClassId = sem.ClassId,
                        SectionId = sem.SectionId,
                        StudentId = sem.StudentId
                    }).FirstOrDefault();
               
            }
            return mappingResult;
        }

        public static List<StudentExamList> GetStudentExamMappingList(int schoolId, int studentId, int examId)
        {
            var result=new List<StudentExamList>();
            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                spdb.AddParameter("@StudentId", studentId);
                spdb.AddParameter("@ExamTypeId", examId);
                using (var dr = spdb.ExecuteReader("GetStudentExamMappingListForEdit", CommandType.StoredProcedure))
                {
                    var drgetter = new DataReaderObjectLoader<StudentExamList>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true);//use tool to load object from dr.row
                        result.Add(obj);
                    }
                }
            }
            return result;
        }

        public static bool UpdateStudentExam(StudentExamMappingBo model)
        {
            using (var context = new EduErpEntities())
            {
              if (model.StudentExamLists == null) return true;
               foreach (var item in model.StudentExamLists)
                {
                    using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
                    {
                        spdb.AddParameter("@ExamSubjectMappingId", item.ExamSubjectMappingId);
                        spdb.AddParameter("@ObtainedMarks", item.ObtainMarks);
                        spdb.AddParameter("@LastUpdatedBy", SessionItems.UserId);
                        spdb.AddParameter("@LastUpdatedOn", DateTime.Now);
                        spdb.ExecuteNonQuery("UpdateStudentExamDetail", CommandType.StoredProcedure);

                    }
                }
            }
            return true;
        }


        public static List<StudentExamMappingBo> GetStudentExamList(int schoolId)
        {
            var studentsList = new List<StudentExamMappingBo>();

            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                using (var dr = spdb.ExecuteReader("GetStudentExamMappingDetail", CommandType.StoredProcedure))
                {
                    var drgetter = new DataReaderObjectLoader<StudentExamMappingBo>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true);//use tool to load object from dr.row
                        studentsList.Add(obj);
                    }
                }

            }
            return studentsList;
        }


        public static bool UpdateStatus(int id, int status, int userId)
        {
            using (var context = new EduErpEntities())
            {
                var exam = context.StudentExamMappings.FirstOrDefault(a => a.Id == id);
                if (exam == null) return false;
                exam.StatusId = status;
                exam.LastUpdatedOn = DateTime.Now;
                exam.LastUpdateBy = userId;
                context.SaveChanges();
                return true;
            }
        }
    }
}
