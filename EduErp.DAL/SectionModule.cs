﻿using System;
using System.Collections.Generic;
using System.Linq;
using EduErp.BO;
using EduErp.Model;
using SectionMaster = EduErp.Model.SectionMaster;

namespace EduErp.DAL
{
    public class SectionModule
    {

        public static int AddSection(SectionBo model)
        {
            using (var context = new EduErpEntities())
            {
                var result = context.SectionMasters.FirstOrDefault(a => a.Status != (int)UserStatus.Deleted && a.ClassId==model.ClassId && a.Name.ToLower().Equals(model.Name.ToLower()));
                if (result != null)
                {
                    return -1;
                }
                var section = new SectionMaster()
                {
                    Name = model.Name,
                    ClassId = model.ClassId,
                    Status = model.Status,
                    CreatedBy = model.CreatedBy,
                    CreatedOn = model.CreatedOn,
                };
                context.SectionMasters.Add(section);
                context.SaveChanges();
                return 1;
            }
        }

        public static List<SectionBo> GetSections(int schoolId)
        {
            List<SectionBo> sections;
            using (var context = new EduErpEntities())
            {
                sections = (from s in context.SectionMasters
                            join c in context.ClassMasters on s.ClassId equals c.Id
                            join sch in context.SchoolMasters on c.SchoolId equals sch.Id
                            where (c.SchoolId==schoolId ||c.SchoolId==0) && s.Status != (int)UserStatus.Deleted 
                            select new SectionBo
                            {
                                Id = s.Id,
                                Name = s.Name,
                                ClassName = c.Name,
                                SchoolName = sch.Name,
                                Status = s.Status,
                                ClassId = s.ClassId,
                                CreatedOn = s.CreatedOn
                            }).ToList();
            }
            return sections;

        }


        public static SectionBo GetSectionById(int id)
        {
            SectionBo section;
            using (var context = new EduErpEntities())
            {
                section = (from s in context.SectionMasters
                           join c in context.ClassMasters on s.ClassId equals c.Id
                           where s.Id == id
                           select new SectionBo
                           {
                               Id = s.Id,
                               Name = s.Name,
                               SchoolId = c.SchoolId,
                               ClassId = s.ClassId
                           }).FirstOrDefault();
            }
            return section;
        }

        public static bool UpdateSection(SectionBo model)
        {
            using (var context = new EduErpEntities())
            {
                var section = context.SectionMasters.FirstOrDefault(a => a.Id == model.Id);
                if (section != null)
                {
                    section.Name = model.Name;
                    section.ClassId = model.ClassId;
                    section.LastUpdatedBy = model.LastUpdatedBy;
                    section.LastUpdatedOn = model.LastUpdatedOn;
                    context.SaveChanges();
                    return true;
                }
                return false;
            }

        }
        public static bool UpdateStatus(int id, int status, int userId)
        {
            using (var context = new EduErpEntities())
            {
                var section = context.SectionMasters.FirstOrDefault(a => a.Id == id);
                if (section == null) return false;
                section.Status = status;
                section.LastUpdatedOn = DateTime.Now;
                section.LastUpdatedBy = userId;
                context.SaveChanges();
                return true;
            }
        }

        public static List<Common> GetClasses(int schoolId)
        {
            List<Common> classes;
            using (var context = new EduErpEntities())
            {
                classes = (from g in context.ClassMasters
                           where (g.SchoolId == schoolId || schoolId == 0) && g.Status==(int)UserStatus.Active
                           select new Common
                           {
                               Id = g.Id,
                               Name = g.Name
                           }).ToList();

            }
            return classes;
        }
    }
}
