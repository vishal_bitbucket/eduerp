﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EduErp.BO;
using EduErp.Model;


namespace EduErp.DAL
{
    public class QuestionAnswerModule
    {
        public static List<BO.QuestionAnswerMappingBo> GetQuestionAnswer(int SchoolId)
        {
            List<BO.QuestionAnswerMappingBo> onlin;
            using (var context = new EduErpEntities())
            {
                onlin = (from s in context.OnlineTestQuestionMasters
                         // join km in context.KeyWordMasters on s.QuestionTypeId equals km.KeyWordId && km.Name == "QuestionTypeId"
                         where s.StatusId != (int)UserStatus.Deleted && (s.SchoolId == SchoolId || s.SchoolId == 0)

                         select new BO.QuestionAnswerMappingBo
                         {
                             Id = s.Id,
                             Question = s.Questions,
                             QuestionTypeId = s.QuestionTypeId,
                            // QuestionType = km.KeyWordName,
                             Ismcq = s.Ismcq,
                             StatusId = s.StatusId,
                           
                             
                         }).ToList();
            }
            return onlin;
        }
        public static int AddQuestionAnswer(QuestionAnswerMappingBo OnlineUser)
        {
            int result = 0;
            using (var context = new EduErpEntities())
            {
                var ques = context.OnlineTestQuestionMasters.FirstOrDefault(a => a.Id == OnlineUser.Id);
                if (ques == null)
                {
                    var model = new Model.OnlineTestQuestionMaster()
                    {

                        Questions = OnlineUser.Question,
                        ChoiceTypeId = OnlineUser.ChoiceTypeId,
                        
                        CreatedOn = OnlineUser.CreatedOn,
                        CreatedBy = OnlineUser.CreatedBy,
                        SchoolId = OnlineUser.SchoolId,
                        StatusId = OnlineUser.StatusId ?? 0,
                        SubjectId = OnlineUser.SubjectId,
                        Description = OnlineUser.Description
                    };
                    context.OnlineTestQuestionMasters.Add(model);
                    context.SaveChanges();
                    foreach (var item in OnlineUser.Answers)
                    {
                        var model1 = new Model.OTQuestionAnswerMapping()
                        {

                            QuestionId = model.Id,
                            CreatedOn = OnlineUser.CreatedOn,
                            CreatedBy = OnlineUser.CreatedBy,
                            SchoolId = OnlineUser.SchoolId ?? 0,
                            StatusId = OnlineUser.StatusId ?? 0,
                            Answer = item.Answer,
                            IsCorrectAnswer = item.IsCorrectAnswer,

                        };
                        context.OTQuestionAnswerMappings.Add(model1);
                        context.SaveChanges();
                    }
                    result = model.Id;
                }
                else
                {
                    ques.Questions = OnlineUser.Question;
                    ques.QuestionTypeId = OnlineUser.QuestionTypeId;
                    ques.ChoiceTypeId = OnlineUser.ChoiceTypeId;
                    ques.Description = OnlineUser.Description;
                    ques.LastUpdatedBy = OnlineUser.LastUpdatedBy;
                    ques.LastUpdatedOn = OnlineUser.LastUpdatedOn;
                    context.SaveChanges();

                  
               
                    foreach (var item in OnlineUser.Answers)
                    {
                        var answer = context.OTQuestionAnswerMappings.FirstOrDefault(b => b.Id == item.Id);
                        if (answer == null)
                        {
                            var model1 = new Model.OTQuestionAnswerMapping()
                            {

                                QuestionId = OnlineUser.Id,
                                CreatedOn = OnlineUser.CreatedOn,
                                CreatedBy = OnlineUser.CreatedBy,
                                SchoolId = OnlineUser.SchoolId ?? 0,
                                StatusId = OnlineUser.StatusId ?? 0,
                                Answer = item.Answer,
                                IsCorrectAnswer = item.IsCorrectAnswer,

                            };
                            context.OTQuestionAnswerMappings.Add(model1);
                            context.SaveChanges();
                        }
                        else
                        {
                            
                            answer.Answer = item.Answer;
                            answer.IsCorrectAnswer = item.IsCorrectAnswer;
                            answer.LastUpdatedBy = OnlineUser.LastUpdatedBy??0;
                            answer.LastUpdatedOn = OnlineUser.LastUpdatedOn.Value;
                            context.SaveChanges();
                        }
                        context.SaveChanges();
                    }
                    var answers = context.OTQuestionAnswerMappings.Where(a => a.QuestionId == OnlineUser.Id &&
                        !OnlineUser.Answers.Any(b => b.Id == a.Id)).ToList();
                    if (answers != null)
                    {
                        foreach (var item in answers)
                        {
                            context.OTQuestionAnswerMappings.Remove(item);
                            context.SaveChanges();
                        }
                    }

                    result = OnlineUser.Id;
                }

                return result;
            }
        }
        public static BO.QuestionAnswerMappingBo GetQuestionAnswerById(int id)
        {
            BO.QuestionAnswerMappingBo onlin = new QuestionAnswerMappingBo();
            using (var context = new EduErpEntities())
            {
                onlin = (from q in context.OnlineTestQuestionMasters
                         where q.Id == id
                         select new BO.QuestionAnswerMappingBo
                         {
                             Id = q.Id,
                             Question = q.Questions,
                             Description = q.Description,
                             QuestionTypeId = q.QuestionTypeId,
                             ChoiceTypeId = q.ChoiceTypeId ?? 0,
                             SchoolId = q.SchoolId,
                             StatusId = q.StatusId
                         }).FirstOrDefault();

                if (context.OTQuestionAnswerMappings.FirstOrDefault(a => a.QuestionId == id) != null)
                {
                    onlin.Answers = (from q in context.OTQuestionAnswerMappings
                                     where q.QuestionId == id
                                     select new BO.QuestionAnswerMapping
                                     {
                                         QuestionId = q.Id,
                                         Answer = q.Answer,
                                         IsCorrectAnswer = q.IsCorrectAnswer,
                                         Id = q.Id,
                                         StatusId = q.StatusId
                                     }).ToList();
                }
            }
            return onlin;
        }
        public static List<Common> GetChoiceTypeId()
        {
            List<Common> ChoiceTypeIdList;
            using (var context = new EduErpEntities())
            {
                ChoiceTypeIdList = (from g in context.KeyWordMasters
                                    where g.Name == "ChoiceType"
                                    select new Common
                                    {
                                        Id = g.KeyWordId,
                                        Name = g.KeyWordName
                                    }).ToList();

            }
            return ChoiceTypeIdList;
        }
        public static bool UpdateStatus(int id, int statusId, int userId)
        {
            using (var context = new EduErpEntities())
            {
                var onlin = context.OnlineTestQuestionMasters.FirstOrDefault(a => a.Id == id);
                if (onlin == null) return false;
                onlin.StatusId = statusId;
                onlin.LastUpdatedOn = DateTime.Now;
                onlin.LastUpdatedBy = userId;
                context.SaveChanges();
                return true;

            }


        }

    }
}





