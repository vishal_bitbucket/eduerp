﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EduErp.BO;
using EduErp.Model;

namespace EduErp.DAL
{
   public  class OnlineTestQuestionModule
    {

       public static List<Common> GetQuestionTypeId()
       {
           List<Common> QuestionTypeIdList;
           using (var context = new EduErpEntities())
           {
               QuestionTypeIdList = (from g in context.KeyWordMasters
                                     where g.Name == "QuestionTypeId"
                        select new Common
                        {
                            Id = g.KeyWordId,
                            Name = g.KeyWordName
                        }).ToList();

           }
           return QuestionTypeIdList;
       }
       public static List<Common> GetSubjects(int schoolId)
       {
           List<Common> subjects;
           using (var context = new EduErpEntities())
           {
               subjects = (from s in context.OnlineTestSubjectMasters
                           where s.StatusId == (int)UserStatus.Active && s.SchoolId == schoolId
                           select new Common
                           {
                               Id = s.Id,
                               Name = s.Name
                           }).ToList();
           }
           return subjects;
       }
       public static int AddOnlineTestQuestion(OnlineExamTestQuestionBo OnlineUser)
       {
           using (var context = new EduErpEntities())
           {
               //var result = context.OnlineTestQuestionMasters.FirstOrDefault(a => a.StatusId != (int)UserStatus.Deleted && a.SubjectId == OnlineUser.SubjectId && a.SchoolId == OnlineUser.SchoolId);
               //if (result != null)
               //{
               //    return -1;
               //}
               var model = new Model.OnlineTestQuestionMaster()
               {
                  
                   CreatedOn = OnlineUser.CreatedOn,
                   CreatedBy = OnlineUser.CreatedBy,
                   Questions = OnlineUser.Questions,
                   SubjectId = OnlineUser.SubjectId,
                   Description = OnlineUser.Description,
                    QuestionTypeId = OnlineUser.QuestionTypeId,
                    Ismcq = OnlineUser.Ismcq, 
                   SchoolId = OnlineUser.SchoolId,
                   StatusId = OnlineUser.Status,
                   
               };
               context.OnlineTestQuestionMasters.Add(model);
               context.SaveChanges();
               return model.Id;
           }
       }
       public static List<OnlineExamTestQuestionBo> GetOnlineTestQuestion(int SchoolId)
       {


           List<OnlineExamTestQuestionBo> onlin;
           using (var context = new EduErpEntities())
           {
               onlin = (from s in context.OnlineTestQuestionMasters join sm in context.OnlineTestSubjectMasters on s.SubjectId equals sm.Id  
                                                                    join km in context.KeyWordMasters on s.QuestionTypeId equals km.KeyWordId
                        where s.StatusId != (int)UserStatus.Deleted && (s.SchoolId == SchoolId || SchoolId == 0) && km.Name =="QuestionTypeId"
                        
                        select new OnlineExamTestQuestionBo
                        {
                            Id = s.Id,
                            SchoolId = s.SchoolId ?? 0,
                            Description = s.Description,
                            Questions = s.Questions,
                            SubjectId = s.SubjectId ?? 0,
                            SubjectName = sm.Name,
                            QuestionTypeId = s.QuestionTypeId ?? 0,
                            QuestionType = km.KeyWordName,
                            Ismcq = s.Ismcq.Value,
                            Status = s.StatusId
                           
                        }).ToList();
           }
           return onlin;
       }
       public static bool UpdateStatus(int id, int status, int userId)
       {
           using (var context = new EduErpEntities())
           {
               var onlin = context.OnlineTestQuestionMasters.FirstOrDefault(a => a.Id == id);
               if (onlin == null) return false;
               onlin.StatusId = status;
               onlin.LastUpdatedOn = DateTime.Now;
               onlin.LastUpdatedBy = userId;
               context.SaveChanges();
               return true;

           }


       }
       public static OnlineExamTestQuestionBo GetOnlineTestQuestionById(int id)
       {
           OnlineExamTestQuestionBo onlin;
           using (var context = new EduErpEntities())
           {
               onlin = (from o in context.OnlineTestQuestionMasters
                        where id == o.Id

                        select new OnlineExamTestQuestionBo()
                        {

                            
                           Description = o.Description,
                           Questions = o.Questions,
                           SubjectId = o.SubjectId ?? 0,
                           QuestionTypeId = o.QuestionTypeId??0,
                           Ismcq = o.Ismcq.Value,
                            SchoolId = o.SchoolId ?? 0,
                            Status = o.StatusId
                        }).FirstOrDefault();
           }
           return onlin;
       }
       public static bool UpdateOnlineTestQuestion(OnlineExamTestQuestionBo model)
       {

           using (var context = new EduErpEntities())
           {
               var onlin = context.OnlineTestQuestionMasters.SingleOrDefault(a => a.Id == model.Id);

               if (onlin != null)
               {
                   onlin.Id = model.Id;
                  
                   onlin.Description = model.Description;
                   onlin.Questions = model.Questions;
                   onlin.SubjectId = model.SubjectId;
                   onlin.QuestionTypeId = model.QuestionTypeId;
                   onlin.LastUpdatedBy = model.LastUpdatedBy;
                   onlin.LastUpdatedOn = model.LastUpdatedOn;
                   onlin.StatusId = model.Status;
                   onlin.SchoolId = model.SchoolId;
                   context.SaveChanges();
                   return true;
               }
               return false;
           }
       }
    }
}
