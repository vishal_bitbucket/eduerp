﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using EduErp.BO;
using EduErp.DAL.Utilities;
using EduErp.Model;

namespace EduErp.DAL
{
    public class ReportModule
    {
        public static List<Common> GetPayStatus()
        {
            List<Common> typList;
            using (var context = new EduErpEntities())
            {
                typList = (from k in context.KeyWordMasters
                           where k.Name == "PayStatus"
                           select new Common
                           {
                               Name = k.KeyWordName,
                               Id = k.KeyWordId

                           }).ToList();
            }
            return typList;
        }
        public static List<Common> GetResultStatus()
        {
            List<Common> typList;
            using (var context = new EduErpEntities())
            {
                typList = (from k in context.KeyWordMasters
                           where k.Name == "ResultStatus"
                           select new Common
                           {
                               Name = k.KeyWordName,
                               Id = k.KeyWordId

                           }).ToList();
            }
            return typList;
        }
        public static List<Common> GetMonths()
        {
            List<Common> monthList;
            using (var context = new EduErpEntities())
            {
                monthList = (from k in context.KeyWordMasters
                             where k.Name == "Month"
                             select new Common
                             {
                                 Name = k.KeyWordName,
                                 Id = k.KeyWordId

                             }).ToList();
            }
            return monthList;
        }

        public static List<Common> GetClasses(int schoolId)
        {
            List<Common> classList;
            using (var context = new EduErpEntities())
            {
                classList = (from k in context.ClassMasters
                             where k.Status == (int)UserStatus.Active && k.SchoolId == schoolId
                             select new Common
                             {
                                 Name = k.Name,
                                 Id = k.Id

                             }).ToList();
            }
            return classList;
        }


        public static List<Common> GetAdmissionStatus()
        {
            List<Common> statusList;
            using (var context = new EduErpEntities())
            {
                statusList = (from k in context.KeyWordMasters
                             where k.Name=="AdmissionStatus"
                             select new Common
                             {
                                 Name = k.KeyWordName,
                                 Id = k.Id

                             }).ToList();
            }
            return statusList;
        }
        public static List<Common> GetExams(int schoolId)
        {
            List<Common> examsList;
            using (var context = new EduErpEntities())
            {
                examsList = (from k in context.ExamMasters
                             where k.StatusId == (int)UserStatus.Active && k.SchoolId == schoolId
                             select new Common
                             {
                                 Name = k.Name,
                                 Id = k.Id

                             }).ToList();
            }
            return examsList;
        }

        public static List<Common> GetSubjectsByExamId(int examId,int schoolId)
        {
            List<Common> subjectList;
            using (var context = new EduErpEntities())
            {
                subjectList = (from k in context.SubjectMasters join esm in context.ExamSubjectMappings on k.Id equals esm.SubjectId
                             where k.Status == (int)UserStatus.Active && k.SchoolId == schoolId && (esm.ExamId==examId || examId == 0)
                             select new Common
                             {
                                 Name = k.Name,
                                 Id = k.Id
                             }).ToList();
            }
            return subjectList;
        }
        public static List<Common> GetClassBySubjectId(int subjectId, int schoolId)
        {
            List<Common> classList;
            using (var context = new EduErpEntities())
            {
                classList = (from k in context.ClassMasters
                               join esm in context.SubjectClassMappings on k.Id equals esm.ClassId
                               where k.Status == (int)UserStatus.Active && k.SchoolId == schoolId && (esm.SubjectId == subjectId || subjectId == 0)
                               select new Common
                               {
                                   Name = k.Name,
                                   Id = k.Id
                               }).Distinct().ToList();
            }
            return classList;
        }
      
        public static CategoryWiseStudentReport GetStudentCategoryDetail(int schoolId)
        {
            CategoryWiseStudentReport report = new CategoryWiseStudentReport();
            List<CategoryWiseDetail> categoryDetail = new List<CategoryWiseDetail>();

            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {

                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);

                using (var dr = spdb.ExecuteReader("GetStudentChartCategorywise", CommandType.StoredProcedure))
                {

                    var drgetter = new DataReaderObjectLoader<CategoryWiseDetail>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true); //use tool to load object from dr.row
                        categoryDetail.Add(obj);
                    }
                    report.categoryDetail = categoryDetail;
                    //return model;
                }
                return report;
            }
        }

        public static SessionWiseStudentReport GetStudentSessionWise(int schoolId)
        {
            SessionWiseStudentReport report = new SessionWiseStudentReport();

            List<SessionWiseDetail> sessionWiseDetail = new List<SessionWiseDetail>();

            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {

                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);

                using (var dr = spdb.ExecuteReader("GetStudentSessionWiseForChart", CommandType.StoredProcedure))
                {

                    var drgetter = new DataReaderObjectLoader<SessionWiseDetail>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true); //use tool to load object from dr.row
                        sessionWiseDetail.Add(obj);
                    }
                    report.sessionWiseDetail = sessionWiseDetail;
                    //return model;
                }
                return report;
            }
        }


        public static GendderWiseStudentReport GetStudentGenderDetail(int schoolId)
        {
            GendderWiseStudentReport genderReport = new GendderWiseStudentReport();
            List<GenderDetailClassWiseDrillDown> drill = new List<GenderDetailClassWiseDrillDown>();
            List<GenderDetail> det = new List<GenderDetail>();

            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                
                using (var dr = spdb.ExecuteReader("GetStudentChartGenderwise", CommandType.StoredProcedure))
                {

                    var drgetter = new DataReaderObjectLoader<GenderDetail>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true); //use tool to load object from dr.row
                        det.Add(obj);
                    }
                    genderReport.genderDetail = det;
                    //return model;
                }
            }

            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                spdb.AddParameter("@GenderId", 1);
                spdb.AddParameter("@SessionId", 6);
                using (var dr = spdb.ExecuteReader("GetdataForGenderWiseDrilldown", CommandType.StoredProcedure))
                {

                    var drgetter = new DataReaderObjectLoader<GenderDetailClassWiseDrillDown>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true); //use tool to load object from dr.row
                        drill.Add(obj);
                    }
                    genderReport.genderDetailClassWiseDrillDown = drill;
                   
                }
            }
            return genderReport;
        
        }

        public static List<ExpensesBo> GetExpenseDetail(int schoolId, int sessionId,int expenseId,int fromMonthId,int toMonthId)
        {
            List<ExpensesBo> expList = new List<ExpensesBo>();
            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                spdb.AddParameter("@SessionId", sessionId);
                spdb.AddParameter("@ExpenseTypeId", expenseId);
                spdb.AddParameter("@FromMonthId", fromMonthId);
                spdb.AddParameter("@ToMonthId", toMonthId);

                using (var dr = spdb.ExecuteReader("GetExpenseReport", CommandType.StoredProcedure))
                {
                    var drgetter = new DataReaderObjectLoader<ExpensesBo>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true); //use tool to load object from dr.row
                        expList.Add(obj);
                    }
                }
            }
            return expList;
        }
        public static List<ExamBo> GetExamDetail(int schoolId,int sessionId, int examId, int subjectId, int classId, int sectionId, int studentId)
        {
            List<ExamBo> exmList = new List<ExamBo>();
            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                spdb.AddParameter("@SessionId", sessionId);
                spdb.AddParameter("@ExamId", examId);
                spdb.AddParameter("@StudentId", studentId);
                spdb.AddParameter("@SubjectId", subjectId);
                spdb.AddParameter("@ClassId", classId);
                spdb.AddParameter("@SectionId", sectionId);

                using (var dr = spdb.ExecuteReader("GetExamReport", CommandType.StoredProcedure))
                {
                    var drgetter = new DataReaderObjectLoader<ExamBo>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true); //use tool to load object from dr.row
                        exmList.Add(obj);
                    }
                }
            }
            return exmList;
        }
        public static TypeWiseExpensesReport GetTypeWiseExpensesDetail(int sessionId, int schoolId)
        {
            TypeWiseExpensesReport report = new TypeWiseExpensesReport();
            List<ExpensesChart> TypewiseDetail = new List<ExpensesChart>();

            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {

                spdb.ClearParameters();
                spdb.AddParameter("@SessionId", sessionId);
                spdb.AddParameter("@SchoolId", schoolId);

                using (var dr = spdb.ExecuteReader("GetExpenseChart", CommandType.StoredProcedure))
                {

                    var drgetter = new DataReaderObjectLoader<ExpensesChart>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true); //use tool to load object from dr.row
                        TypewiseDetail.Add(obj);
                    }
                    report.TypewiseDetail = TypewiseDetail;
                    //return model;
                }
                return report;
            }
        }
        public static TypeWiseExamReport GetTypeExamWiseChartList(int sessionId, int schoolId,int ExamId)
        {
            TypeWiseExamReport report = new TypeWiseExamReport();
            List<ExamChart> TypewiseDetail = new List<ExamChart>();

            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {

                spdb.ClearParameters();
                spdb.AddParameter("@SessionId", sessionId);
                spdb.AddParameter("@SchoolId", schoolId);
                spdb.AddParameter("@ExamId", ExamId);
          


                using (var dr = spdb.ExecuteReader("GetStudentExamReportExamWise", CommandType.StoredProcedure))
                {

                    var drgetter = new DataReaderObjectLoader<ExamChart>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true); //use tool to load object from dr.row
                        TypewiseDetail.Add(obj);
                    }
                    report.TypewiseDetail = TypewiseDetail;
                    //return model;
                }
                return report;
            }
        }
        public static TypeStudentWiseExamReport GetTypeStudentWiseChartList(int sessionId, int schoolId, int ExamId, int studentId)
        {
            TypeStudentWiseExamReport report = new TypeStudentWiseExamReport();
            List<StudentWiseExamChart> TypeStudentWiseDetail = new List<StudentWiseExamChart>();

            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {

                spdb.ClearParameters();
                spdb.AddParameter("@SessionId", sessionId);
                spdb.AddParameter("@SchoolId", schoolId);
                spdb.AddParameter("@ExamId", ExamId);
                spdb.AddParameter("@StudentId", studentId);



                using (var dr = spdb.ExecuteReader("[GetStudentExamReportStudentwise]", CommandType.StoredProcedure))
                {

                    var drgetter = new DataReaderObjectLoader<StudentWiseExamChart>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true); //use tool to load object from dr.row
                        TypeStudentWiseDetail.Add(obj);
                    }
                    report.TypeStudentWiseDetail = TypeStudentWiseDetail;
                    //return model;
                }
                return report;
            }
        }
        public static FeeTransactionTemplateBo GetFeeStatusDetail(int schoolId, int sessionId, int classId, int sectionId, int fromMonthId,
            int toMonthId, int PayStatusId)
        {
            List<FeetransactionDetailList> payPaidLists = new List<FeetransactionDetailList>();
            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                spdb.AddParameter("@SessionId", sessionId);
                spdb.AddParameter("@ClassId", classId);
                spdb.AddParameter("@SectionId", sectionId);
                spdb.AddParameter("@FromMonthId", fromMonthId);
                spdb.AddParameter("@ToMonthId", toMonthId);
                if(PayStatusId == 0)
                    spdb.AddParameter("@PayStatusId", System.DBNull.Value);
                else 
                    spdb.AddParameter("@PayStatusId", PayStatusId);
                using (var dr = spdb.ExecuteReader("GetFeeTransactionReport", CommandType.StoredProcedure))
                {
                    var drgetter = new DataReaderObjectLoader<FeetransactionDetailList>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true); //use tool to load object from dr.row
                        payPaidLists.Add(obj);
                    }
                }
                var result = new FeeTransactionTemplateBo
                {
                    FeetransactionDetailList = payPaidLists
                };

                return result;
            }
        }


    }
}