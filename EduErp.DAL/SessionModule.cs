﻿using System;
using System.Collections.Generic;
using System.Linq;
using EduErp.BO;
using EduErp.DAL.Utilities;
using EduErp.Model;
using SessionMaster = EduErp.BO.SessionMaster;

namespace EduErp.DAL
{
    public class SessionModule
    {
        public static int AddSesion(SessionMaster model)
        {
            using (var context = new EduErpEntities())
            {
                var result = context.SessionMasters.FirstOrDefault(a => a.StatusId != (int) UserStatus.Deleted && a.Name.ToLower().Equals(model.Name.ToLower()) && a.SchoolId == model.SchoolId);
                if(result!=null)
                {
                    return -1;
                }

                var result1 = context.SessionMasters.FirstOrDefault(a => a.StatusId == (int)UserStatus.Active  && a.SchoolId == model.SchoolId);
                if (result1 != null)
                {
                    return -2;
                }
                var session = model.Convert<Model.SessionMaster, SessionMaster>();
                context.SessionMasters.Add(session);
                context.SaveChanges();
                return 1;
            }
        }

        public static bool UpdateSession(SessionMaster model)
        {
            using (var context = new EduErpEntities())
            {
                var session = context.SessionMasters.FirstOrDefault(a => a.Id == model.Id);
                if (session != null)
                {
                    session.Name = model.Name;
                    session.SchoolId = model.SchoolId;
                    session.FromDate = model.FromDate;
                    session.ToDate = model.ToDate;
                    session.LastModifiedBy = model.LastModifiedBy;
                    session.LastModifiedOn = model.LastModifiedOn;
                    context.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public static int UpdateStatus(int sessionId, int statusId, int userId ,int schoolId)
        {
            using (var context = new EduErpEntities())
            {
                if(statusId==1)
                {
                    var count = context.SessionMasters.Count(a => a.StatusId == (int)UserStatus.Active && a.SchoolId == schoolId);
                    if(count>=1)
                    {
                    return -2;
                    }
            }
                var session = context.SessionMasters.FirstOrDefault(a => a.Id == sessionId);
                if (session == null) return 0;
                session.StatusId = statusId;
                session.LastModifiedBy = userId;
                session.LastModifiedOn = DateTime.Now;
                context.SaveChanges();
                return 1;
            }
        }

        public static List<SessionMaster> GetSessions(int schoolId)
        {
            using (var context = new EduErpEntities())
            {
                return (from s in context.SessionMasters
                        where s.StatusId != (int)UserStatus.Deleted && (s.SchoolId == 0 || s.SchoolId == schoolId)
                        select new SessionMaster
                        {
                            Id = s.Id,
                            Name = s.Name,
                            FromDate = s.FromDate,
                            ToDate = s.ToDate,
                            SchoolId = s.SchoolId,
                            StatusId = s.StatusId,
                            CreatedOn = s.CreatedOn
                        }).ToList();
            }
        }

        public static SessionMaster GetSessionById(int id)
        {
            using (var context = new EduErpEntities())
            {
                var session = context.SessionMasters.FirstOrDefault(a => a.Id == id);
                return session.Convert<SessionMaster, Model.SessionMaster>();
            }
        }
    }
}
