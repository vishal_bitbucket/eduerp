﻿using System;
using System.Collections.Generic;
using System.Linq;
using EduErp.BO;
using EduErp.Model;

namespace EduErp.DAL
{
    public class AttendanceModule
    {
        public static List<Common> GetDayStatus()
        {
            List<Common> result;
            using (var context = new EduErpEntities())
            {
                result = (from u in context.KeyWordMasters
                          where u.Name == "DayStatusId"
                          select new Common
                          {
                              Id = u.KeyWordId,
                              Name = u.KeyWordName
                          }).ToList();
            }
            return result;
        }
        public static List<Common> GetMonthList()
        {
            List<Common> monthList;
            using (var context = new EduErpEntities())
            {
                monthList = (from s in context.KeyWordMasters
                             where s.Name == "Month"
                             select new Common
                             {
                                 Id = s.KeyWordId,
                                 Name = s.KeyWordName
                             }).ToList();
            }
            return monthList;
        }
        public static AttendanceTemplateBo GetStudentAttendance(int schoolId, int sessionId, int classId, int sectionId)
        {
            using (var context = new EduErpEntities())
            {
                var data = (from a in context.Students
                        join b in context.StudentClassMappings on a.Id equals b.StudentId
                        where a.SchoolId == schoolId && b.ClassId == classId && b.SectionId == sectionId && b.SessionId == sessionId && a.StatusId==(int)UserStatus.Active
                        select new AttendanceList
                        {
                            StudentId = a.Id,
                            StudentName = a.FirstName,
                            DayStatusId = (int)DayStatus.Present,
                            Remark = string.Empty,
                            StudentCode = b.StudentCode
                        }).ToList();
                var result = new AttendanceTemplateBo
                {
                    AttendanceLists = data,
                    DayStatusList = GetDayStatus()
                };
                return result;
            }
        }
        //public static AttendanceTemplateBo GetStudentAttendances(int schoolId, int sessionId, int classId, int sectionId)
        //{
        //    using (var context = new EduErpEntities())
        //    {
        //        var data = (from a in context.Students
        //                    join b in context.StudentClassMappings on a.Id equals b.StudentId
        //                    where a.SchoolId == schoolId && b.ClassId == classId && b.SectionId == sectionId && b.SessionId == sessionId && a.StatusId == (int)UserStatus.Active
        //                    select new AttendanceList
        //                    {
        //                        StudentId = a.Id,
        //                        StudentName = a.FirstName,
        //                        DayStatusId = (int)DayStatus.Present,
        //                        Remark = string.Empty,
        //                        StudentCode = b.StudentCode
        //                    }).ToList();
        //        var result = new AttendanceTemplateBo
        //        {
        //            AttendanceLists = data,
        //            DayStatusList = GetDayStatus()
        //        };
        //        return result;
        //    }
        //}

        public static bool AddAttendance(AttendanceBo model,int schoolId)
        {
            using (var context = new EduErpEntities())
            {
                var availableMessage = MessageModule.LeftMessage(schoolId);
                if (availableMessage >= model.MsgCount)
                {
                    var updatedLeftMessage = availableMessage - model.MsgCount;
                    var accountDetail = context.AccountDetails.FirstOrDefault(a => a.SchoolId == schoolId);
                    if (accountDetail != null)
                    {
                        accountDetail.LeftMessageCount = updatedLeftMessage;
                        context.SaveChanges();
                    }
                }
                var students = model.AttendanceTemplateBo.AttendanceLists.Where(a => a.DayStatusId != (int)DayStatus.Present).ToList();
                foreach (var student in students)
                {
                    var attendance = new Model.AttendanceMaster
                    {
                        StudentId = student.StudentId,
                        SchoolId = model.SchoolId,
                        SessionId = model.SessionId,
                        DayStatusId = student.DayStatusId,
                        Date = model.Date,
                        Remark = student.Remark
                    };
                    context.AttendanceMasters.Add(attendance);
                    context.SaveChanges();
                }
            }
            return true;
        }

        public static List<AttendanceList> GetAllAttendace(DateTime startDate, DateTime endDate, int schoolId, int sessionId, int classId, int sectionId)
        {
            using (var context = new EduErpEntities())
            {
                //var result = new List<AttendanceList>();
                var data = (from a in context.Students
                    join b in context.StudentClassMappings on a.Id equals b.StudentId
                    join c in context.AttendanceMasters on a.Id equals c.StudentId
                    where a.SchoolId == schoolId && b.ClassId == classId && b.SectionId == sectionId && b.SessionId == sessionId && a.StatusId==(int)UserStatus.Active
                    && c.Date >= startDate && c.Date <= endDate
                    select new AttendanceList
                    {
                        StudentId = a.Id,
                        StudentName = a.FirstName,
                        DayStatusId = c.DayStatusId,
                        Remark = c.Remark,
                        StudentCode = b.StudentCode,
                        AttendanceDate = c.Date,
                        //studentStatusId=a.StatusId
                    }).ToList();
                //var abc = data.Select(d => d.StudentId).ToArray();
                var count = endDate.Subtract(startDate).Days + 1;
                
                for (var i = 0; i < count; i++)
                {
                    var cdate = startDate.AddDays(i);
                    //var day = cdate.DayOfWeek;
                    var data2 = (from a in context.Students
                        join b in context.StudentClassMappings on a.Id equals b.StudentId
                        where a.SchoolId == schoolId && b.ClassId == classId && b.SectionId == sectionId && b.SessionId == sessionId && a.StatusId==(int)UserStatus.Active 
                              //&& data.Where(d => d.StudentId == a.Id && EntityFunctions.TruncateTime(d.AttendanceDate) == EntityFunctions.TruncateTime(cdate1)) == null
                        select new AttendanceList
                        {
                            StudentId = a.Id,
                            StudentName = a.FirstName,
                            DayStatusId = (cdate.DayOfWeek == DayOfWeek.Sunday || context.HolidayMasters.Where(h => h.FromDate >= cdate && h.ToDate <= cdate &&
                                            h.SchoolId == schoolId && h.SessionId == sessionId && h.StatusId.Value == (int)UserStatus.Active).Count() > 0) ?
                                            (int)DayStatus.Holiday : (int)DayStatus.Present,
                            Remark = string.Empty,
                            StudentCode = b.StudentCode,
                            AttendanceDate = cdate
                        }).ToList();
                    data.AddRange(data2);
                }
                return data;
            }
        }

        public static bool UpdateAttendance(BO.AttendanceMaster model)
        {
            using (var context = new EduErpEntities())
            {
                var attendance = context.AttendanceMasters.FirstOrDefault(a => a.StudentId == model.StudentId && a.SessionId == model.SessionId && a.SchoolId == model.SchoolId && a.Date == model.Date);
                if (attendance == null)
                {
                    var newAttendance = new Model.AttendanceMaster
                    {
                        StudentId = model.StudentId,
                        SchoolId = model.SchoolId,
                        SessionId = model.SessionId,
                        DayStatusId = model.DayStatusId,
                        Date = model.Date,
                        Remark = model.Remark,
                        LastModifiedBy = model.LastModifiedBy,
                        LastModifiedOn = model.LastModifiedOn
                    };
                    context.AttendanceMasters.Add(newAttendance);
                    context.SaveChanges();
                }
                else
                {
                    attendance.DayStatusId = model.DayStatusId;
                    attendance.LastModifiedBy = model.LastModifiedBy;
                    attendance.LastModifiedOn = model.LastModifiedOn;
                    attendance.Remark = model.Remark;
                    context.SaveChanges();
                }
            }
            return true;
        }
    }
}
