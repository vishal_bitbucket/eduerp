﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using EduErp.BO;
using EduErp.DAL.Utilities;
using EduErp.Model;
using MessageMaster = EduErp.Model.MessageMaster;

namespace EduErp.DAL
{
    public class MessageModule
    {
        public static int AddMessage(MessageBo model)
        {

            using (var context = new EduErpEntities())
            {
                var availableMessage = LeftMessage(model.SchoolId ?? 0);
                if (availableMessage >= model.MsgCount)
                {
                    var updatedLeftMessage = availableMessage - model.MsgCount;
                    var accountDetail = context.AccountDetails.FirstOrDefault(a => a.SchoolId == model.SchoolId);
                    if (accountDetail != null)
                    {
                        accountDetail.LeftMessageCount = updatedLeftMessage;
                        context.SaveChanges();
                    }
                }
                
                //if (consumeMessage > maxMessage)
                //{
                //    return -1;
                //}
                var maxMessageId = context.MessageMasters.Max(a => a.MessageId);
                model.MessageId = maxMessageId + 1;
                foreach (var item in model.StudentLists)
                {
                    if (!item.Selected)
                        continue;
                    var message = new MessageMaster
                    {
                        SectionId = model.SectionId ?? 0,
                        StudentId = item.StudentIdForMessage,
                        SchoolId = model.SchoolId,
                        Message = model.Message,
                        Subject = model.Subject,
                        StatusId = model.StatusId,
                        CreatedBy = model.CreatedBy,
                        CreatedOn = model.CreatedOn,
                        ClassId = model.ClassId,
                        MessageSentDate = model.MessageSentDate,
                        MessageSentTime = model.MessageSentTime,
                        IsMessageSent = true,
                        MessageId = model.MessageId ?? 0
                    };

                    context.MessageMasters.Add(message);
                    context.SaveChanges();
                }

            }
            return 1;
        }
        public static int MaxMessageCount(int schoolId)
        {
            using (var context=new EduErpEntities())
            {
                var maxMessageCount = (from spm in context.SchoolPlanMappings
                                       join pm in context.PlanMasters on spm.PlanId equals pm.Id
                                       where spm.SchoolId == schoolId
                                       select pm.MaxMessageLimit ?? 0).FirstOrDefault();
                return maxMessageCount;
            }
           }

        public static int LeftMessage(int schoolId)
        {
            using (var context=new EduErpEntities())
            {
                var leftMessageCount = (from ad in context.AccountDetails where ad.SchoolId==schoolId select ad.LeftMessageCount).FirstOrDefault();
                return leftMessageCount??0;
            }
            
        }
        public static string GetNumberByStudentId(int studentId)
        {
            using (var context=new EduErpEntities())
            {
                var number = (from s in context.Students where s.Id == studentId select s.StudentMobile).FirstOrDefault();
                return number;
            }
            }
        public static List<MessageBo> GetMessages(int schoolId)
        {
            List<MessageBo> messages = new List<MessageBo>();
            using (var spdb = new SpdbHelper(SqlDBConnection.GetConnectionString()))
            {
                spdb.ClearParameters();
                spdb.AddParameter("@SchoolId", schoolId);
                using (var dr = spdb.ExecuteReader("GetMessages", CommandType.StoredProcedure))
                {
                    var drgetter = new DataReaderObjectLoader<MessageBo>(dr, false);
                    while (dr.Read())
                    {
                        var obj = drgetter.GetObjectFromDataReader(dr, true);//use tool to load object from dr.row
                        messages.Add(obj);
                    }
                }

            }
            return messages;
        }

        public static List<Common> GetTemplateList()
        {
            List<Common> templateList;
            using (var context=new EduErpEntities())
            {
                templateList = (from tm in context.TemplateMasters
                    select new Common
                    {
                         Id = tm.Id,
                         Name = tm.Text
                    }).ToList();
            }
            return templateList;
        }
        public static List<StudentList> GetStudentsForMessage(int classId, int sectionId, int schoolId)
        {

            List<StudentList> students;
            using (var context = new EduErpEntities())
            {
                if (classId == 0 && sectionId == 0)
                {
                    students = (from s in context.Students
                                where s.SchoolId == schoolId && s.StatusId == (int)UserStatus.Active
                                select new StudentList
                        {
                            StudentIdForMessage = s.Id,
                            StudentName = s.FirstName
                        }).ToList();
                }
                else if ((classId != 0 && sectionId == 0) || sectionId == 0)
                {
                    students = (from s in context.Students
                                join sm in context.StudentClassMappings on s.Id equals sm.StudentId
                                where sm.ClassId == classId && s.StatusId == (int)UserStatus.Active
                                select new StudentList
                                {
                                    StudentIdForMessage = s.Id,
                                    StudentName = s.FirstName
                                }).ToList();
                }
                else
                {
                    students = (from s in context.Students
                                join sm in context.StudentClassMappings on s.Id equals sm.StudentId
                                where sm.SectionId == sectionId && sm.ClassId == classId && s.StatusId == (int)UserStatus.Active
                                select new StudentList
                                {
                                    StudentIdForMessage = s.Id,
                                    StudentName = s.FirstName
                                }).ToList();
                }

            }
            return students;

        }
        public static List<Common> GetClasses(int schoolId)
        {
            List<Common> classes;
            using (var context = new EduErpEntities())
            {
                classes = (from c in context.ClassMasters
                           where c.SchoolId == schoolId && c.Status == (int)UserStatus.Active
                           select new Common
                           {
                               Id = c.Id,
                               Name = c.Name

                           }).ToList();

            }
            return classes;
        }

        public static List<Common> GetStudentBySectionId(int sectionId)
        {
            List<Common> students;
            using (var context = new EduErpEntities())
            {

                students = (from s in context.Students
                            join sm in context.StudentClassMappings on s.Id equals sm.StudentId
                            where sm.SectionId == sectionId && s.StatusId != (int)UserStatus.Deleted
                            select new Common
                            {
                                Id = s.Id,
                                Name = s.FirstName
                            }).ToList();
            }
            return students;
        }
        public static List<Common> GetSections(int classId)
        {
            List<Common> sections;
            using (var context = new EduErpEntities())
            {
                sections = (from c in context.SectionMasters
                            where c.Status == (int)UserStatus.Active && c.ClassId == classId
                            select new Common
                            {
                                Id = c.Id,
                                Name = c.Name
                            }).ToList();
            }
            return sections;
        }

        public static bool UpdateStatus(int id, int statusId, int userId)
        {
            using (var context = new EduErpEntities())
            {
                var message = context.MessageMasters.FirstOrDefault(a => a.Id == id);
                if (message == null) return false;
                message.StatusId = statusId;
                message.UpdatedBy = userId;
                message.UpdatedOn = DateTime.Now;
                context.SaveChanges();
                return true;
            }
        }
    }
}
