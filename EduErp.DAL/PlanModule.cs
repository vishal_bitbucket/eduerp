﻿using System;
using System.Collections.Generic;
using System.Linq;
using EduErp.BO;
using EduErp.Model;
namespace EduErp.DAL
{
    public class PlanModule
    {
        public static bool AddPlan(PlanBo plan)
        {
            using (var context = new EduErpEntities())
            {
                //var result = context.PlanMasters.FirstOrDefault(a => a.StatusId != (int)UserStatus.Deleted && a.Name.Equals(plan.Name.ToLower()));
                //if (result != null)
                //{
                //    return -1;
                //}
                var model = new Model.PlanMaster
                {
                    Name = plan.Name,
                   // SchoolId = plan.SchoolId,
                    PlanTypeId = plan.PlanTypeId??0,
                    MaxMessageLimit = plan.MaxMessageLimit,
                    Detail = plan.Detail,
                    MaxStudentLimit = plan.MaxStudentLimit,
                    MaxUserLimit = plan.MaxUserLimit,
                    StatusId = plan.StatusId,
                    CreatedOn = plan.CreatedOn,
                    CreatedBy = plan.CreatedBy
                };
                context.PlanMasters.Add(model);
                context.SaveChanges();
            }
            return true;
        }

        public static List<Common> GetPlanType()
        {
            var context = new EduErpEntities();
            var planlist = (from plan in context.KeyWordMasters
                           where plan.Name == "PlanType" 
                           select new Common
                           {
                               Id = plan.KeyWordId,
                               Name = plan.KeyWordName
                           }).ToList();
            return planlist;
        }
        public static List<PlanBo> GetPlans()
        {
            List<PlanBo> plans;
            using (var context = new EduErpEntities())
            {
                plans = (from p in context.PlanMasters
                         where p.StatusId != (int)UserStatus.Deleted
                         select new PlanBo
                         {
                             Id = p.Id,
                             Name = p.Name,
                             PlanTypeId = p.PlanTypeId,
                             MaxMessageLimit = p.MaxMessageLimit,
                             MaxStudentLimit = p.MaxStudentLimit,
                             MaxUserLimit = p.MaxUserLimit,
                             Detail = p.Detail,
                             StatusId = p.StatusId,
                            
                         }).ToList();
            }
            return plans;
        }

        public static PlanBo GetPlan(int id)
        {
            PlanBo plan;
            using (var context = new EduErpEntities())
            {
                plan = (from p in context.PlanMasters
                        where p.Id == id && p.StatusId != (int)UserStatus.Deleted
                        select new PlanBo
                        {
                            Id = p.Id,
                            PlanTypeId = p.PlanTypeId,
                            MaxStudentLimit = p.MaxStudentLimit,
                            MaxUserLimit = p.MaxUserLimit,
                            Detail = p.Detail,
                            Name = p.Name,
                            MaxMessageLimit = p.MaxMessageLimit
                        }).FirstOrDefault();
            }
            return plan;
        }

        public static bool UpdatePlan(PlanBo model)
        {
            using (var context = new EduErpEntities())
            {
                var plan = context.PlanMasters.FirstOrDefault(a => a.Id == model.Id);
                if (plan != null)
                {
                    plan.Name = model.Name;
                    plan.PlanTypeId = model.PlanTypeId;
                    plan.MaxStudentLimit = model.MaxStudentLimit;
                    plan.MaxUserLimit = model.MaxUserLimit;
                    plan.MaxMessageLimit = model.MaxMessageLimit;
                   plan.Detail = model.Detail;
                    plan.UpdatedBy = model.UpdatedBy;
                    plan.UpdatedOn = model.UpdatedOn;
                    context.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public static bool UpdateStatus(int id, int statusid, int userId)
        {
            using (var context = new EduErpEntities())
            {
                var account = context.PlanMasters.FirstOrDefault(a => a.Id == id);
                if (account == null) return false;
                account.StatusId = statusid;
                account.UpdatedOn = DateTime.Now;
                account.UpdatedBy = userId;
                context.SaveChanges();
                return true;
            }
        }



    }
}
