USE [EduErp]
GO
/****** Object:  StoredProcedure [dbo].[GetFeeTransactionByClassSection]    Script Date: 03/28/2018 19:42:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--GetFeeTransactionByClassSection 6,6,25,8 
ALTER Proc [dbo].[GetFeeTransactionByClassSection]
@SchoolId int ,
@SessionId int,
@ClassId int,
@SectionId int
AS
BEGIN
Declare @FineAmount decimal(18,2)
  Declare @TotalAmount numeric(18,2)
Select * from (

Select ft1.Id,ft1.StudentId,ft1.TotalAmountDeposit,ft1.PreviousBalance,ft1.TotalPayableAmount,ft1.TotalAmount,ft1.IsSettled,ft1.BalanceAmount,ft1.Remark,ft1.TransactionDate,ft1.CreatedOn,ft1.CreatedBy,ft1.LastUpdatedBy,
ft1.LastUpdatedOn,ft1.StatusId,
a.Total,s.FirstName + ' ' + s.LastName  as StudentName
From FeeTransaction ft1
Inner join
(Select ft.Id,ft.StudentId, ftdr.Total
From  FeeTransaction ft  Inner JOIN
(Select  ftd.FeeTransactionId , sum(fhm.Amount) AS Total
FROM    FeeTransactionDetail ftd
Left Outer JOIN FeeHeadMonthMapping fhm ON  ftd.FeeHeadMappingId = fhm.Id
group by ftd.FeeTransactionId)  ftdr on ftdr.FeeTransactionId = ft.Id) as a ON Ft1.Id = a.Id
Inner join
(Select StudentId , Max(Id)  as LastTransactionId From (
Select ft.Id,ft.StudentId, ftdr.Total
From  FeeTransaction ft  Inner JOIN
(Select  ftd.FeeTransactionId , sum(fhm.Amount) AS Total
FROM    FeeTransactionDetail ftd
Left Outer JOIN FeeHeadMonthMapping fhm ON  ftd.FeeHeadMappingId = fhm.Id
group by ftd.FeeTransactionId)  ftdr on ftdr.FeeTransactionId = ft.Id) AS Res
Group By Res.StudentId) b
ON b.LastTransactionId = ft1.Id
Inner join Student s ON s.Id = ft1.StudentId
Left Outer JOIN StudentClassMapping scm ON scm.StudentId = s.Id
Where @SchoolId = s.SchoolId AND @SessionId = scm.SessionId AND @ClassId = scm.ClassId AND @SectionId = scm.SectionId
) res1

left outer join

(
Select fine.TransactionId,sum(fine.FeeHeadAmount) as Discount from FineDiscountTransactionDetail fine Inner JOIN FeeHeadMaster fhm ON fine.FeeHeadId = fhm.Id
Where  fhm.FeeHeadTypeId = 3 --fine.TransactionId=64 and
Group By fine.TransactionId,fhm.FeeHeadTypeId
) res2

ON res2.TransactionId = res1.Id
left  outer join
(
Select fine.TransactionId,sum(fine.FeeHeadAmount) as Fine from FineDiscountTransactionDetail fine Inner JOIN FeeHeadMaster fhm ON fine.FeeHeadId = fhm.Id
Where  fhm.FeeHeadTypeId = 4 --fine.TransactionId=64 and
Group By fine.TransactionId,fhm.FeeHeadTypeId
) res3

ON res3.TransactionId = res1.Id

END