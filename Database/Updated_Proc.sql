USE [EduErp]
GO
/****** Object:  StoredProcedure [dbo].[GetSchools]    Script Date: 01/22/2018 18:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetSchools]
AS
BEGIN
	Select sch.Id,sch.Code,sch.ParentSchoolId,sch.Name,sch.CityId,sch.Address1,sch.Address2,sch.Address1+' '+sch.Address2 as [Address],sch.PIN,sch.OwnerName,sch.ContactNo,sch.CreatedOn,
	sch.CreatedBy,sch.StatusId,sch.ImagePath,psch.Name AS ParentSchoolName,cty.Name AS CityName,PM.MaxStudentLimit,PM.MaxUserLimit,PM.ValidityInMonth 
	from SchoolMaster sch 	 LEFT OUTER JOIN SchoolMaster psch ON sch.Id = psch.ParentSchoolId
	INNER JOIN PlanMaster PM ON sch.Id=PM.SchoolId	
	INNER JOIN CityMaster cty ON sch.CityId = cty.Id
	Where sch.StatusId <> 3
END
GO
/****** Object:  StoredProcedure [dbo].[GetUpcomingBirthDay]    Script Date: 01/22/2018 18:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetUpcomingBirthDay]
@SchoolId int
--@ClassId int,
--@SectionId int
as
begin
select
   s.FirstName as StudentName,c.Name as ClassName,sec.Name as SectionName ,s.DOB as BirthDate
    from Student s  inner join StudentClassMapping scm on s.Id=scm.StudentId
    inner join ClassMaster c on c.Id=scm.ClassId
    inner join SectionMaster sec on sec.Id=scm.SectionId
     where s.SchoolId=@SchoolId  and  MONTH(s.DOB)=MONTH(GETDATE()) and MONTH(s.DOB)=MONTH(DATEADD(DD,7,GETDATE())) and DAY(s.DOB)>DAY(GETDATE()) and DAY(s.DOB)<DAY(DATEADD(dd,7,GETDATE()))
              
end
GO
/****** Object:  StoredProcedure [dbo].[GetTodaysBirthDay]    Script Date: 01/22/2018 18:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetTodaysBirthDay]
@SchoolId int
as
begin
select
   s.FirstName as StudentName,c.Name as ClassName,sec.Name as SectionName
    from Student s  inner join StudentClassMapping scm  on s.Id=scm.StudentId
    inner join  ClassMaster c on c.Id=scm.ClassId
                inner join SectionMaster sec on sec.Id=scm.SectionId
                where s.SchoolId=@SchoolId and MONTH(s.DOB)=MONTH(GETDATE()) and DAY(s.DOB)=DAY(GETDATE())
                
end
GO
