USE [EduErp]
GO
/****** Object:  StoredProcedure [dbo].[GetStudentDetailForProfile]    Script Date: 03/31/2018 19:07:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[GetStudentDetailForProfile]
@StudentId int
as
begin
select s.Id,s.TitleId,k.KeyWordName as TitleName,s.StudentMobile,s.FirstName,s.LastName,s.MiddileName,s.DOB,s.DOJ,s.Email,s.Uid,s.ImagePath,s.S3key,s.StatusId  
,s.CorrAddress as CuurentAddress,s.CorrPinCode as CurrentPinCode,s.FatherName,s.MotherName,s.GuardianAddress,s.GuardianMobileNo as GuardianMobile,s.GuardianName  
,s.GuardianOccupation,s.GuardianRelationWithStudent as GuardianRelation,s.ParentContactNo,s.ParentMobileNo as ParentMobileNumber,s.PermanentAddress  
,s.PermanentPinCode,s.CategoryId,s.GenderId,cm.Name as ClassName,sm.Name as SectionName,cmm.Name as CurrentCityName,smmm.Name as PermanentStateName  
,ks.KeyWordName as RelegionCategoryName,ssn.Name as SessionName ,scm.StudentCode,smm.Name as CurrentStateName,cmmm.Name as PermanentCityName,km.KeyWordName as GenderName  
,kmm.KeyWordName as NationlityName,kmmm.KeyWordName as RelegionName  
from  Student s left outer join KeyWordMaster k on s.TitleId=k.KeyWordId  and k.Name='Title'
               left outer join KeyWordMaster ks on s.CategoryId=ks.KeyWordId and ks.Name='Category' 
               left outer join KeyWordMaster km on s.GenderId=km.KeyWordId and km.Name='Gender' 
               left outer join KeyWordMaster kmm on s.NationlityId=kmm.KeyWordId  and kmm.Name='Nationlity'
               left outer join KeyWordMaster kmmm on s.RelegionId=kmmm.KeyWordId   and kmmm.Name='Relegion' 
               left outer join StudentClassMapping scm on s.id=scm.StudentId  
               left outer  join SectionMaster sm on scm.SectionId=sm.Id  
               left outer join ClassMaster cm on cm.Id=scm.ClassId  
               left outer join SessionMaster ssn on ssn.Id=scm.SessionId  
               left outer join CityMaster cmm on cmm.Id= s.CorrCityId   
               left outer join CityMaster cmmm on cmmm.Id=s.PermanentCityId  
               left outer join StateMaster smm on smm.Id=cmm.StateId    
               left outer join StateMaster smmm on smmm.Id=cmmm.StateId               
where s.Id=@StudentId and s.StatusId=1  
end