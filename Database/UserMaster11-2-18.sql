/*
   Sunday, January 28, 201812:34:03 PM
   User: sa
   Server: DELL-PC
   Database: EduErp
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_UserMaster
	(
	Id int NOT NULL IDENTITY (1, 1),
	Name varchar(200) NOT NULL,
	UserTypeId int NULL,
	UserId varchar(50) NOT NULL,
	Password nvarchar(MAX) NOT NULL,
	PasswordSalt uniqueidentifier NOT NULL,
	Email varchar(100) NOT NULL,
	Mobile varchar(10) NOT NULL,
	SchoolId int NULL,
	CreatedOn datetime NOT NULL,
	CreatedBy int NULL,
	StatusId int NOT NULL,
	LastModifiedOn datetime NULL,
	LatModifiedBy int NULL,
	OneTimePassword int NULL,
	VerificationCode uniqueidentifier NULL,
	VerificationCodeGeneratedOnUtc datetime NULL,
	IsPasswordCreated bit NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_UserMaster SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_UserMaster ON
GO
IF EXISTS(SELECT * FROM dbo.UserMaster)
	 EXEC('INSERT INTO dbo.Tmp_UserMaster (Id, Name, UserTypeId, UserId, Password, PasswordSalt, Email, Mobile, SchoolId, CreatedOn, CreatedBy, StatusId, LastModifiedOn, LatModifiedBy, VerificationCode, VerificationCodeGeneratedOnUtc, IsPasswordCreated)
		SELECT Id, Name, UserTypeId, UserId, Password, PasswordSalt, Email, Mobile, SchoolId, CreatedOn, CreatedBy, StatusId, LastModifiedOn, LatModifiedBy, VerificationCode, VerificationCodeGeneratedOnUtc, IsPasswordCreated FROM dbo.UserMaster WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_UserMaster OFF
GO
ALTER TABLE dbo.AmazonSettings
	DROP CONSTRAINT FK_AmazonSettings_UserMaster
GO
DROP TABLE dbo.UserMaster
GO
EXECUTE sp_rename N'dbo.Tmp_UserMaster', N'UserMaster', 'OBJECT' 
GO
ALTER TABLE dbo.UserMaster ADD CONSTRAINT
	PK_UserMaster PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.AmazonSettings ADD CONSTRAINT
	FK_AmazonSettings_UserMaster FOREIGN KEY
	(
	ModifiedById
	) REFERENCES dbo.UserMaster
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.AmazonSettings SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
