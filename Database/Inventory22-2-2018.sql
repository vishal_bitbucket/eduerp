/*
   Thursday, February 22, 20183:00:46 PM
   User: sa
   Server: DELL-PC
   Database: EduErp
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Inventory
	DROP CONSTRAINT FK_Inventory_SchoolMaster
GO
ALTER TABLE dbo.SchoolMaster SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Inventory
	DROP CONSTRAINT FK_Inventory_SessionMaster
GO
ALTER TABLE dbo.SessionMaster SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Inventory
	(
	Id int NOT NULL IDENTITY (1, 1),
	SchoolId int NOT NULL,
	ItemId int NULL,
	ItemDescription varchar(250) NULL,
	Amount numeric(6, 2) NOT NULL,
	VenderId int NULL,
	InvetoryTypeId int NOT NULL,
	PaymentModeTypeId int NULL,
	StartDate datetime NULL,
	EndDate datetime NULL,
	SessionId int NULL,
	CreatedOn datetime NOT NULL,
	CreatedBy int NOT NULL,
	LastUpdatedBy int NULL,
	LastUpdatedOn date NULL,
	StatusId int NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Inventory SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_Inventory ON
GO
IF EXISTS(SELECT * FROM dbo.Inventory)
	 EXEC('INSERT INTO dbo.Tmp_Inventory (Id, SchoolId, ItemId, ItemDescription, Amount, VenderId, InvetoryTypeId, StartDate, EndDate, SessionId, CreatedOn, CreatedBy, LastUpdatedBy, LastUpdatedOn, StatusId)
		SELECT Id, SchoolId, ItemId, ItemDescription, Amount, VenderId, InvetoryTypeId, StartDate, EndDate, SessionId, CreatedOn, CreatedBy, LastUpdatedBy, LastUpdatedOn, StatusId FROM dbo.Inventory WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Inventory OFF
GO
DROP TABLE dbo.Inventory
GO
EXECUTE sp_rename N'dbo.Tmp_Inventory', N'Inventory', 'OBJECT' 
GO
ALTER TABLE dbo.Inventory ADD CONSTRAINT
	PK_Inventory PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Inventory ADD CONSTRAINT
	FK_Inventory_SessionMaster FOREIGN KEY
	(
	SessionId
	) REFERENCES dbo.SessionMaster
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Inventory ADD CONSTRAINT
	FK_Inventory_SchoolMaster FOREIGN KEY
	(
	SchoolId
	) REFERENCES dbo.SchoolMaster
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
