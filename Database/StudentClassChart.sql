USE EduErp
GO

CREATE PROCEDURE GetStudentClassChart
@SchoolId INT

AS BEGIN

SELECT        ClassMaster.Id, ClassMaster.Name, COUNT(StudentClassMapping.StudentId) AS TotalStudents
FROM            StudentClassMapping Left Outer JOIN
                         ClassMaster ON StudentClassMapping.ClassId = ClassMaster.Id
						 WHERE ClassMaster.SchoolId = @SchoolId
						 GROUP BY ClassMaster.Name, ClassMaster.Id						 

UNION

SELECT Id, Name, 0 AS TotalStudents FROM ClassMaster WHERE ClassMaster.SchoolId = @SchoolId AND Id NOT IN
 (SELECT        ClassMaster.Id
FROM            StudentClassMapping Left Outer JOIN
                         ClassMaster ON StudentClassMapping.ClassId = ClassMaster.Id
						 WHERE ClassMaster.SchoolId = @SchoolId)

END