/*
   Wednesday, January 31, 201812:40:07 PM
   User: sa
   Server: DELL-PC
   Database: EduErp
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.PlanMaster
	DROP CONSTRAINT FK_PlanMaster_SchoolMaster
GO
ALTER TABLE dbo.SchoolMaster SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.PlanMaster
	DROP COLUMN SchoolId
GO
ALTER TABLE dbo.PlanMaster SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
