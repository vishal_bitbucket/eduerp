USE [EduErp]
GO
/****** Object:  StoredProcedure [dbo].[GetExpenseReport]    Script Date: 05/19/2018 17:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author: Shubham Mishra
-- Exec Param : GetExpenseReport 6,6,1,1,5
-- Description: All Expense Report
-- =============================================
ALTER PROCEDURE [dbo].[GetExpenseReport]
@SchoolId INT ,
@SessionId INT,
@ExpenseTypeId INT,
@FromMonthId INT,
@ToMonthId INT
AS
BEGIN

	SELECT item.Name AS ItemName, i.StartDate, i.EndDate, v.Name AS VendorName, i.Amount,
		km.KeyWordName AS InventoryName, s.Name AS SessionName
	FROM dbo.Inventory AS i LEFT JOIN
	dbo.VendorMaster AS v ON i.VenderId = v.Id LEFT JOIN
	dbo.ItemMaster AS item ON i.ItemId = item.Id LEFT JOIN
	dbo.SessionMaster AS s ON i.SessionId = s.Id LEFT JOIN
	dbo.KeyWordMaster AS km ON i.InvetoryTypeId = km.KeyWordId AND km.Name = 'InventoryType'
	WHERE i.InvetoryTypeId = @ExpenseTypeId AND i.SchoolId = @SchoolId AND i.SessionId = @SessionId
	AND DATEPART(M,i.CreatedOn) >= @FromMonthId AND DATEPART(M, i.CreatedOn) <= @ToMonthId

END
