USE [EduErp]
GO
/****** Object:  StoredProcedure [dbo].[GetStudentChartCategorywise]    Script Date: 04/12/2018 17:32:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetStudentChartCategorywise] 
@SchoolId INT
AS BEGIN
Select CategoryId,KeyWordMaster.KeyWordName AS CategoryName ,Count(student.Id) as CountNo from Student  LEFT OUTER JOIN KeyWordMaster on Student.CategoryId=KeyWordMaster.KeyWordId
Where SchoolId = 6 AND KeyWordMaster.Name='Category' AND CategoryId is not null
Group by CategoryId,KeyWordMaster.KeyWordName
END
GO
/****** Object:  StoredProcedure [dbo].[GetStudentsForAllSchoolForReport]    Script Date: 04/12/2018 17:32:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetStudentsForAllSchoolForReport]
@SessionId int,
@SchoolId int,
@ClassId int,
@SectionId int,
@StatusId int,
@AddmissionStatus int
AS
Select s.Id as StudentId,s.FirstName as StudentName,s.MiddileName,s.LastName,s.TitleId,s.Email,s.StudentMobile as StudentContactNumber,scm.StudentCode as StudentRollNumber,
  s.MotherName,s.FatherName,s.CorrCityId,s.GenderId,s.SchoolId,s.Uid,s.PermanentAddress,s.CorrAddress,
  s.DOJ,s.DOB,s.DOE,s.StatusId as StudentStatusId,s.CreatedOn,s.CreatedBy,s.LastUpdatedOn,s.LastUpdatedBy,s.ImagePath,
  s.RelegionId,s.CategoryId,s.ReasonForExit,s.NationlityId,s.GuardianAddress,s.GuardianName,s.GuardianOccupation,s.GuardianRelationWithStudent,
  kg.KeyWordName as StudentGender, kn.KeyWordName as NationlityName, kr.KeyWordName as  RelegionName,
  kt.KeyWordName as TitleName ,kc.KeyWordName as StudentCategory
 From Student s inner join StudentClassMapping scm on s.Id=scm.StudentId
 left outer join CityMaster cm ON cm.Id=s.CorrCityId 
 left outer join KeyWordMaster kg ON kg.KeyWordId = s.GenderId AND kg.Name = 'Gender'
left outer join KeyWordMaster kn ON kn.KeyWordId = s.NationlityId AND kn.Name = 'Nationlity'
 left outer join KeyWordMaster kr ON kr.KeyWordId = s.RelegionId AND kr.Name = 'Relegion'
 left outer  join KeyWordMaster kc ON kc.KeyWordId = s.CategoryId AND kc.Name = 'Category'
 left outer join KeyWordMaster kt ON kt.KeyWordId = s.TitleId AND kt.Name = 'Title'
Where s.SchoolId = @SchoolId and scm.SessionId=@SessionId 
And ((@ClassId = 0 ) OR (@ClassId = scm.ClassId) ) AND ((@AddmissionStatus=1) OR (@AddmissionStatus=2) OR(@AddmissionStatus=0)) AND
 ((@SectionId = 0 ) OR (@SectionId = scm.SectionId) ) AND
 ((@StatusId = 0 ) OR (@StatusId = s.StatusId)) AND s.StatusId<>3
GO
/****** Object:  StoredProcedure [dbo].[GetStudentsForAllSchool]    Script Date: 04/12/2018 17:32:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetStudentsForAllSchool]
@SessionId int,
@SchoolId int,
@ClassId int,
@SectionId int,
@StatusId int
AS
Select s.Id as StudentId,s.FirstName as StudentName,s.MiddileName,s.LastName,s.TitleId,s.Email,s.StudentMobile as StudentContactNumber,scm.StudentCode as StudentRollNumber,
  s.MotherName,s.FatherName,s.CorrCityId,s.GenderId,s.SchoolId,s.Uid,s.PermanentAddress,s.CorrAddress,
  s.DOJ,s.DOB,s.DOE,s.StatusId as StudentStatusId,s.CreatedOn,s.CreatedBy,s.LastUpdatedOn,s.LastUpdatedBy,s.ImagePath,
  s.RelegionId,s.CategoryId,s.ReasonForExit,s.NationlityId,s.GuardianAddress,s.GuardianName,s.GuardianOccupation,s.GuardianRelationWithStudent,
  kg.KeyWordName as StudentGender, kn.KeyWordName as NationlityName, kr.KeyWordName as  RelegionName,
  kt.KeyWordName as TitleName ,kc.KeyWordName as StudentCategory
 From Student s inner join StudentClassMapping scm on s.Id=scm.StudentId
 left outer join CityMaster cm ON cm.Id=s.CorrCityId 
 left outer join KeyWordMaster kg ON kg.KeyWordId = s.GenderId AND kg.Name = 'Gender'
left outer join KeyWordMaster kn ON kn.KeyWordId = s.NationlityId AND kn.Name = 'Nationlity'
 left outer join KeyWordMaster kr ON kr.KeyWordId = s.RelegionId AND kr.Name = 'Relegion'
 left outer  join KeyWordMaster kc ON kc.KeyWordId = s.CategoryId AND kc.Name = 'Category'
 left outer join KeyWordMaster kt ON kt.KeyWordId = s.TitleId AND kt.Name = 'Title'
Where s.SchoolId = @SchoolId and scm.SessionId=@SessionId 
And ((@ClassId = 0 ) OR (@ClassId = scm.ClassId) ) AND
 ((@SectionId = 0 ) OR (@SectionId = scm.SectionId) ) AND
 ((@StatusId = 0 ) OR (@StatusId = s.StatusId)) AND s.StatusId<>3
GO
/****** Object:  StoredProcedure [dbo].[GetStudentSessionWiseForChart]    Script Date: 04/12/2018 17:32:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[GetStudentSessionWiseForChart]
@SchoolId int
AS
BEGIN
Select sm.Name as SessionName, Count(s.Id) as CountNo from Student s 
Inner join StudentClassMapping scm On s.Id = scm.StudentId --AND scm.SessionId = 6
Inner join SessionMaster sm ON sm.Id = scm.SessionId
Where s.SchoolId = @SchoolId
Group by scm.SessionId,sm.Name
END
GO
