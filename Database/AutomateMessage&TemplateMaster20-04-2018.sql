USE [EduErp]
GO
/****** Object:  Table [dbo].[TemplateMaster]    Script Date: 04/20/2018 15:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TemplateMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Text] [varchar](500) NOT NULL,
 CONSTRAINT [PK_TemplateMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TemplateMaster] ON
INSERT [dbo].[TemplateMaster] ([Id], [Name], [Text]) VALUES (1, N'Attendence', N'Dear Parent:Today,your child is Absent')
INSERT [dbo].[TemplateMaster] ([Id], [Name], [Text]) VALUES (2, N'Attendence', N'Your attendence is below.Please cover your attendence to appear in exam ')
INSERT [dbo].[TemplateMaster] ([Id], [Name], [Text]) VALUES (3, N'Fee', N' Please pay due fee immediately to attend exams')
INSERT [dbo].[TemplateMaster] ([Id], [Name], [Text]) VALUES (4, N'Fee', N'Your child is paid amount today and balance is still remaining,thank tou for fee payment')
INSERT [dbo].[TemplateMaster] ([Id], [Name], [Text]) VALUES (5, N'User', N'User has been created successfully')
SET IDENTITY_INSERT [dbo].[TemplateMaster] OFF
/****** Object:  Table [dbo].[AutomateMessage]    Script Date: 04/20/2018 15:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AutomateMessage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NULL,
	[Name] [varchar](150) NULL,
	[Description] [varchar](500) NULL,
 CONSTRAINT [PK_AutomateMessage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[AutomateMessage] ON
INSERT [dbo].[AutomateMessage] ([Id], [ParentId], [Name], [Description]) VALUES (1, 0, N'Attendance', N'This is for attendance module')
INSERT [dbo].[AutomateMessage] ([Id], [ParentId], [Name], [Description]) VALUES (2, 1, N'Absent', N'if you check the checkbox and select template then message will be send automatically after creation of Absent and Attendance is below')
INSERT [dbo].[AutomateMessage] ([Id], [ParentId], [Name], [Description]) VALUES (3, 0, N'Fees', N'This is fee module')
INSERT [dbo].[AutomateMessage] ([Id], [ParentId], [Name], [Description]) VALUES (4, 3, N'DueFees', N'if you check the checkbox and select template,then message will be send automatically after due fee')
INSERT [dbo].[AutomateMessage] ([Id], [ParentId], [Name], [Description]) VALUES (5, 3, N'DueFees', N'if you check the checkbox and select template,then message will be send automatically Remaining fee after submit fee')
INSERT [dbo].[AutomateMessage] ([Id], [ParentId], [Name], [Description]) VALUES (6, 0, N'User', N'If you check then message will be send automatically after creation of user')
INSERT [dbo].[AutomateMessage] ([Id], [ParentId], [Name], [Description]) VALUES (7, 6, N'User', N'if you check the checkbox and select template then message will be send automatically after creation of User')
SET IDENTITY_INSERT [dbo].[AutomateMessage] OFF
