USE [EduErp]
GO
/****** Object:  StoredProcedure [dbo].[GetFeeTransactionByStudentId]    Script Date: 03/28/2018 19:40:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Proc [dbo].[GetFeeTransactionByStudentId]
@StudentId int,
@SessionId int
AS
BEGIN
 SELECT ft.Id,ft.StudentId,ft.TotalAmountDeposit,ft.PreviousBalance,ft.TotalAmount,ft.TotalPayableAmount,ft.IsSettled,ft.BalanceAmount,ft.Remark,ft.TransactionDate,ft.CreatedOn,ft.CreatedBy,ft.LastUpdatedBy,
 ft.LastUpdatedOn,ft.StatusId,
 ftdr.Total
 FROM  FeeTransaction ft  Inner JOIN 
   (SELECT  ftd.FeeTransactionId , SUM(fhm.Amount) AS Total
   FROM    FeeTransactionDetail ftd 
   Left Outer JOIN FeeHeadMonthMapping fhm 
   ON  ftd.FeeHeadMappingId = fhm.Id
   GROUP BY ftd.FeeTransactionId)  ftdr 
 ON ftdr.FeeTransactionId = ft.Id
 Left Outer JOIN StudentClassMapping s on s.StudentId = ft.StudentId
 WHERE ft.StudentId =@StudentId AND s.SessionId = @SessionId AND ft.StatusId<>3
END