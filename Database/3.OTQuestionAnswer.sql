USE [EduErp]
GO
/****** Object:  Table [dbo].[OTQuestionAnswerMapping]    Script Date: 07/07/2018 14:00:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OTQuestionAnswerMapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[QuestionId] [int] NOT NULL,
	[Answer] [nvarchar](50) NOT NULL,
	[IsCorrectAnswer] [bit] NULL,
	[SchoolId] [int] NOT NULL,
	[StatusId] [int] NOT NULL,
	[LastUpdatedOn] [datetime2](7) NOT NULL,
	[LastUpdatedBy] [int] NOT NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[CreatedBy] [int] NOT NULL,
 CONSTRAINT [PK_OTQuestionAnswerMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[OTQuestionAnswerMapping] ON
INSERT [dbo].[OTQuestionAnswerMapping] ([Id], [QuestionId], [Answer], [IsCorrectAnswer], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (1, 2, N'ans2', 1, 6, 1, CAST(0x07C7F0523CC36E3E0B AS DateTime2), 11, CAST(0x07A2E31015C26E3E0B AS DateTime2), 11)
INSERT [dbo].[OTQuestionAnswerMapping] ([Id], [QuestionId], [Answer], [IsCorrectAnswer], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (2, 2, N'Ans2', NULL, 6, 1, CAST(0x07C7F0523CC36E3E0B AS DateTime2), 11, CAST(0x07A2E31015C26E3E0B AS DateTime2), 11)
INSERT [dbo].[OTQuestionAnswerMapping] ([Id], [QuestionId], [Answer], [IsCorrectAnswer], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (3, 3, N'Ans3', 1, 6, 1, CAST(0x070000000000000000 AS DateTime2), 0, CAST(0x073DA6C995C46E3E0B AS DateTime2), 11)
INSERT [dbo].[OTQuestionAnswerMapping] ([Id], [QuestionId], [Answer], [IsCorrectAnswer], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (4, 3, N'ans3', NULL, 6, 1, CAST(0x070000000000000000 AS DateTime2), 0, CAST(0x073DA6C995C46E3E0B AS DateTime2), 11)
INSERT [dbo].[OTQuestionAnswerMapping] ([Id], [QuestionId], [Answer], [IsCorrectAnswer], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (5, 4, N'Ans5', 1, 6, 1, CAST(0x070000000000000000 AS DateTime2), 0, CAST(0x079BC690CFC46E3E0B AS DateTime2), 11)
INSERT [dbo].[OTQuestionAnswerMapping] ([Id], [QuestionId], [Answer], [IsCorrectAnswer], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (6, 4, N'ans5', NULL, 6, 1, CAST(0x070000000000000000 AS DateTime2), 0, CAST(0x079BC690CFC46E3E0B AS DateTime2), 11)
INSERT [dbo].[OTQuestionAnswerMapping] ([Id], [QuestionId], [Answer], [IsCorrectAnswer], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (7, 5, N'ans4', 1, 6, 1, CAST(0x070000000000000000 AS DateTime2), 0, CAST(0x075C2D863EC56E3E0B AS DateTime2), 11)
INSERT [dbo].[OTQuestionAnswerMapping] ([Id], [QuestionId], [Answer], [IsCorrectAnswer], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (8, 5, N'Ans4', NULL, 6, 1, CAST(0x070000000000000000 AS DateTime2), 0, CAST(0x075C2D863EC56E3E0B AS DateTime2), 11)
INSERT [dbo].[OTQuestionAnswerMapping] ([Id], [QuestionId], [Answer], [IsCorrectAnswer], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (9, 6, N'ans6', 1, 6, 1, CAST(0x070000000000000000 AS DateTime2), 0, CAST(0x073FA847A6C56E3E0B AS DateTime2), 11)
INSERT [dbo].[OTQuestionAnswerMapping] ([Id], [QuestionId], [Answer], [IsCorrectAnswer], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (10, 6, N'Ans6', NULL, 6, 1, CAST(0x070000000000000000 AS DateTime2), 0, CAST(0x073FA847A6C56E3E0B AS DateTime2), 11)
SET IDENTITY_INSERT [dbo].[OTQuestionAnswerMapping] OFF
