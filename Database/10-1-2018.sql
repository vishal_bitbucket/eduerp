USE [EduErp]
GO
/****** Object:  StoredProcedure [dbo].[GetStudentDetailForProfile]    Script Date: 01/10/2018 18:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetStudentDetailForProfile]
@StudentId int
as
begin
select s.Id,s.TitleId,k.KeyWordName as TitleName,s.FirstName,s.LastName,s.MiddileName,s.DOB,s.DOJ,s.Email,s.Uid,s.ImagePath,s.S3key,s.StatusId
,s.CorrAddress as CuurentAddress,s.CorrPinCode as CurrentPinCode,s.FatherName,s.MotherName,s.GuardianAddress,s.GuardianMobileNo as GuardianMobile,s.GuardianName
,s.GuardianOccupation,s.GuardianRelationWithStudent as GuardianRelation,s.ParentContactNo,s.ParentMobileNo as ParentMobileNumber,s.PermanentAddress
,s.PermanentPinCode,s.CategoryId,s.GenderId,cm.Name as ClassName,sm.Name as SectionName,cmm.Name as CurrentCityName,smmm.Name as PermanentStateName
,ks.KeyWordName as RelegionCategoryName,ssn.Name as SessionName ,scm.StudentCode,smm.Name as CurrentStateName,cmmm.Name as PermanentCityName,km.KeyWordName as GenderName
,kmm.KeyWordName as NationlityName,kmmm.KeyWordName as RelegionName
from  Student s inner join KeyWordMaster k on s.TitleId=k.KeyWordId
               left outer join KeyWordMaster ks on s.CategoryId=ks.KeyWordId
               inner join KeyWordMaster km on s.GenderId=km.KeyWordId
               inner join KeyWordMaster kmm on s.NationlityId=kmm.KeyWordId
               inner join KeyWordMaster kmmm on s.RelegionId=kmmm.KeyWordId
               inner join StudentClassMapping scm on s.id=scm.StudentId
               inner  join SectionMaster sm on scm.SectionId=sm.Id
               inner join ClassMaster cm on cm.Id=scm.ClassId
               inner join SessionMaster ssn on ssn.Id=scm.SessionId
               inner join CityMaster cmm on cmm.Id= s.CorrCityId 
               inner join CityMaster cmmm on cmmm.Id=s.PermanentCityId
               inner join StateMaster smm on smm.Id=cmm.StateId  
               inner join StateMaster smmm on smmm.Id=cmmm.StateId             
where s.Id=@StudentId and s.StatusId=1 and k.Name='Title' and ks.Name='Category' and km.Name='Gender' and kmm.Name='Nationlity' and kmmm.Name='Relegion'
end
GO
