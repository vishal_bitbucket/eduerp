USE [EduErp]
GO
/****** Object:  StoredProcedure [dbo].[GetInvoiceData]    Script Date: 01/25/2018 18:38:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetInvoiceData]
@SchoolId int
AS BEGIN
Select(
Select Count(*)  from Student Where ( SchoolId=@SchoolId or SchoolId=0) and StatusId=1 AND CreatedOn=GETDATE() ) as TotalStudent,
(Select Count(*)  from MessageMaster Where ( SchoolId=@SchoolId or SchoolId=0) and StatusId=1 AND CreatedOn=GETDATE()) as TotalMessage,
(Select Count(*)  from UserMaster Where  (SchoolId=@SchoolId or SchoolId=0) and StatusId=1 AND CreatedOn=GETDATE()) as TotalUser
FROM SchoolMaster S 
 WHERE S.Id=@SchoolId AND S.StateId<>3 
END
GO
