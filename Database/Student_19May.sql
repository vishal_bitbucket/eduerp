/*
   Saturday, May 19, 20185:05:51 PM
   User: sa
   Server: EDUERPONLINE-PC
   Database: EduerpOnline
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Student
	(
	Id int NOT NULL IDENTITY (1, 1),
	FirstName varchar(50) NOT NULL,
	MiddileName varchar(50) NULL,
	LastName varchar(50) NULL,
	TitleId int NULL,
	RegistrationNumber nvarchar(50) NULL,
	Email varchar(100) NULL,
	StudentMobile varchar(10) NULL,
	StudentOtherInfo varchar(500) NULL,
	GuardianAddress varchar(500) NULL,
	GuardianOccupation varchar(100) NULL,
	GuardianRelationWithStudent varchar(100) NULL,
	GuardianMobileNo varchar(10) NULL,
	GuardianName varchar(100) NULL,
	MessageMobileNumber varchar(10) NULL,
	MotherName varchar(100) NULL,
	FatherName varchar(100) NULL,
	CorrAddress varchar(500) NULL,
	CorrCityId int NULL,
	CorrPinCode varchar(6) NULL,
	PermanentAddress varchar(500) NULL,
	PermanentCityId int NULL,
	CurrentStateId int NULL,
	PermanentStateId int NULL,
	ParentMobileNo varchar(10) NULL,
	ParentContactNo varchar(20) NULL,
	PermanentPinCode varchar(6) NULL,
	SchoolId int NOT NULL,
	DOJ datetime NULL,
	DOB datetime NULL,
	DOE datetime NULL,
	Uid varchar(12) NULL,
	StatusId int NOT NULL,
	CreatedOn datetime NOT NULL,
	CreatedBy int NOT NULL,
	LastUpdatedOn datetime NULL,
	LastUpdatedBy int NULL,
	ImagePath varchar(200) NULL,
	RelegionId int NULL,
	CategoryId int NULL,
	ReasonForExit varchar(500) NULL,
	NationlityId int NULL,
	GenderId int NULL,
	S3key uniqueidentifier NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Student SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_Student ON
GO
IF EXISTS(SELECT * FROM dbo.Student)
	 EXEC('INSERT INTO dbo.Tmp_Student (Id, FirstName, MiddileName, LastName, TitleId, RegistrationNumber, Email, StudentMobile, StudentOtherInfo, GuardianAddress, GuardianOccupation, GuardianRelationWithStudent, GuardianMobileNo, GuardianName, MessageMobileNumber, MotherName, FatherName, CorrAddress, CorrCityId, CorrPinCode, PermanentAddress, PermanentCityId, CurrentStateId, PermanentStateId, ParentMobileNo, ParentContactNo, PermanentPinCode, SchoolId, DOJ, DOB, DOE, Uid, StatusId, CreatedOn, CreatedBy, LastUpdatedOn, LastUpdatedBy, ImagePath, RelegionId, CategoryId, ReasonForExit, NationlityId, GenderId, S3key)
		SELECT Id, FirstName, MiddileName, LastName, TitleId, CONVERT(nvarchar(50), RegistrationNumber), Email, StudentMobile, StudentOtherInfo, GuardianAddress, GuardianOccupation, GuardianRelationWithStudent, GuardianMobileNo, GuardianName, MessageMobileNumber, MotherName, FatherName, CorrAddress, CorrCityId, CorrPinCode, PermanentAddress, PermanentCityId, CurrentStateId, PermanentStateId, ParentMobileNo, ParentContactNo, PermanentPinCode, SchoolId, DOJ, DOB, DOE, Uid, StatusId, CreatedOn, CreatedBy, LastUpdatedOn, LastUpdatedBy, ImagePath, RelegionId, CategoryId, ReasonForExit, NationlityId, GenderId, S3key FROM dbo.Student WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Student OFF
GO
ALTER TABLE dbo.AttendanceMaster
	DROP CONSTRAINT FK_AttendanceMaster_Student
GO
ALTER TABLE dbo.FeeTransaction
	DROP CONSTRAINT FK_FeeTransaction_Student
GO
ALTER TABLE dbo.StudentClassMapping
	DROP CONSTRAINT FK_StudentClassMapping_SessionMaster
GO
ALTER TABLE dbo.StudentClassMapping
	DROP CONSTRAINT FK_StudentClassMapping_Student
GO
ALTER TABLE dbo.StudentExamMapping
	DROP CONSTRAINT FK_StudentExamMapping_Student
GO
DROP TABLE dbo.Student
GO
EXECUTE sp_rename N'dbo.Tmp_Student', N'Student', 'OBJECT' 
GO
ALTER TABLE dbo.Student ADD CONSTRAINT
	PK_Student PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.StudentExamMapping ADD CONSTRAINT
	FK_StudentExamMapping_Student FOREIGN KEY
	(
	StudentId
	) REFERENCES dbo.Student
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.StudentExamMapping SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.StudentClassMapping ADD CONSTRAINT
	FK_StudentClassMapping_SessionMaster FOREIGN KEY
	(
	StudentId
	) REFERENCES dbo.Student
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.StudentClassMapping ADD CONSTRAINT
	FK_StudentClassMapping_Student FOREIGN KEY
	(
	StudentId
	) REFERENCES dbo.Student
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.StudentClassMapping SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.FeeTransaction ADD CONSTRAINT
	FK_FeeTransaction_Student FOREIGN KEY
	(
	StudentId
	) REFERENCES dbo.Student
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.FeeTransaction SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.AttendanceMaster ADD CONSTRAINT
	FK_AttendanceMaster_Student FOREIGN KEY
	(
	StudentId
	) REFERENCES dbo.Student
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.AttendanceMaster SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
