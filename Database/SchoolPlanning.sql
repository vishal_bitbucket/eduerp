USE [EduErp]
GO
/****** Object:  Table [dbo].[SchoolPlanMapping]    Script Date: 01/31/2018 18:48:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolPlanMapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SchoolId] [int] NOT NULL,
	[PlanId] [int] NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[StatusId] [int] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastUpdateBy] [int] NULL,
	[LastUpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_SchoolPlanMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  ForeignKey [FK_SchoolPlanMapping_PlanMaster]    Script Date: 01/31/2018 18:48:58 ******/
ALTER TABLE [dbo].[SchoolPlanMapping]  WITH CHECK ADD  CONSTRAINT [FK_SchoolPlanMapping_PlanMaster] FOREIGN KEY([PlanId])
REFERENCES [dbo].[PlanMaster] ([Id])
GO
ALTER TABLE [dbo].[SchoolPlanMapping] CHECK CONSTRAINT [FK_SchoolPlanMapping_PlanMaster]
GO
/****** Object:  ForeignKey [FK_SchoolPlanMapping_SchoolMaster]    Script Date: 01/31/2018 18:48:58 ******/
ALTER TABLE [dbo].[SchoolPlanMapping]  WITH CHECK ADD  CONSTRAINT [FK_SchoolPlanMapping_SchoolMaster] FOREIGN KEY([SchoolId])
REFERENCES [dbo].[SchoolMaster] ([Id])
GO
ALTER TABLE [dbo].[SchoolPlanMapping] CHECK CONSTRAINT [FK_SchoolPlanMapping_SchoolMaster]
GO
