USE [EduErp]
GO
/****** Object:  Table [dbo].[ExamMaster]    Script Date: 04/15/2018 18:02:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExamMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](150) NOT NULL,
	[SchoolId] [int] NOT NULL,
	[SessionId] [int] NULL,
	[StatusId] [int] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedBy] [int] NULL,
	[LastUpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_ExamMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[ExamMaster] ON
INSERT [dbo].[ExamMaster] ([Id], [Name], [SchoolId], [SessionId], [StatusId], [CreatedOn], [CreatedBy], [LastUpdatedBy], [LastUpdatedOn]) VALUES (2, N'Half-Yearly', 6, NULL, 2, CAST(0x0000A85F0104B9B8 AS DateTime), 11, 11, CAST(0x0000A8AC00F6DE36 AS DateTime))
INSERT [dbo].[ExamMaster] ([Id], [Name], [SchoolId], [SessionId], [StatusId], [CreatedOn], [CreatedBy], [LastUpdatedBy], [LastUpdatedOn]) VALUES (3, N'Annual-Exam', 6, NULL, 1, CAST(0x0000A85F01064453 AS DateTime), 11, 11, CAST(0x0000A87500E18F51 AS DateTime))
INSERT [dbo].[ExamMaster] ([Id], [Name], [SchoolId], [SessionId], [StatusId], [CreatedOn], [CreatedBy], [LastUpdatedBy], [LastUpdatedOn]) VALUES (4, N'Monthly-Exam', 6, NULL, 1, CAST(0x0000A86000DF65C4 AS DateTime), 11, 11, CAST(0x0000A8AC00F6D9CA AS DateTime))
INSERT [dbo].[ExamMaster] ([Id], [Name], [SchoolId], [SessionId], [StatusId], [CreatedOn], [CreatedBy], [LastUpdatedBy], [LastUpdatedOn]) VALUES (5, N'Annual', 6, 6, 1, CAST(0x0000A8C300FCDAB8 AS DateTime), 11, NULL, NULL)
INSERT [dbo].[ExamMaster] ([Id], [Name], [SchoolId], [SessionId], [StatusId], [CreatedOn], [CreatedBy], [LastUpdatedBy], [LastUpdatedOn]) VALUES (6, N'Annual2', 6, 6, 1, CAST(0x0000A8C301004C72 AS DateTime), 11, 11, CAST(0x0000A8C30103D154 AS DateTime))
SET IDENTITY_INSERT [dbo].[ExamMaster] OFF
/****** Object:  StoredProcedure [dbo].[GetFeeTransactionByStudentId]    Script Date: 04/15/2018 18:02:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetFeeTransactionByStudentId]
@StudentId int,
@SessionId int
AS
BEGIN

Create table #temp
(
TransactionId int,
Fine numeric(18,2),
)

--Fine Records
Insert  into #temp (TransactionId,Fine)
select ftd.TransactionId,
ISNULL( Sum(ftd.FeeHeadAmount),0) as Fine
from FineDiscountTransactionDetail ftd Inner join
FeeHeadMaster fhm on fhm.Id = ftd.FeeHeadId
Where fhm.FeeHeadTypeId = 4
Group By ftd.TransactionId


Select * into #temp1
from (
select ftd.TransactionId,isnull(t.Fine,0) as Fine,
ISNULL( Sum(ftd.FeeHeadAmount),0) as Discount
from FineDiscountTransactionDetail ftd
Inner join
FeeHeadMaster fhm on fhm.Id = ftd.FeeHeadId
LEft OUTER join
#temp t ON t.TransactionId = ftd.TransactionId
Where fhm.FeeHeadTypeId = 3
Group By ftd.TransactionId,t.TransactionId,t.Fine ) as res



SELECT ft.Id,ft.StudentId,ft.TotalAmountDeposit,ft.PreviousBalance,ft.TotalAmount,ft.TotalPayableAmount,ft.IsSettled,ft.BalanceAmount,ft.Remark,ft.TransactionDate,ft.CreatedOn,ft.CreatedBy,ft.LastUpdatedBy,
ft.LastUpdatedOn,ft.StatusId,
ISNULL( t1.Discount,0) as Discount, ISNULL(t1.Fine,0) as Fine
FROM  FeeTransaction ft  Left outer join #temp1 t1 On t1.TransactionId = ft.Id 
WHERE ft.StudentId =@StudentId AND ft.SessionId = @SessionId AND ft.StatusId<>3

END
GO
