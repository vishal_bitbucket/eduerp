USE [EduErp]
GO
/****** Object:  Table [dbo].[OTExamSubjectMapping]    Script Date: 07/07/2018 13:59:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OTExamSubjectMapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ExamId] [int] NOT NULL,
	[SubjectId] [int] NOT NULL,
	[Sequence] [int] NOT NULL,
	[TimeInMinutes] [int] NOT NULL,
	[SchoolId] [int] NOT NULL,
	[StatusId] [int] NOT NULL,
	[LastUpdatedOn] [datetime2](7) NOT NULL,
	[LastUpdatedBy] [int] NOT NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[CreatedBy] [int] NOT NULL,
 CONSTRAINT [PK_OTExamSubjectMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[OTExamSubjectMapping] ON
INSERT [dbo].[OTExamSubjectMapping] ([Id], [ExamId], [SubjectId], [Sequence], [TimeInMinutes], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (1, 2, 3, 2, 20, 6, 3, CAST(0x07A07429E5BB6E3E0B AS DateTime2), 11, CAST(0x07A3BFC2D3746C3E0B AS DateTime2), 11)
INSERT [dbo].[OTExamSubjectMapping] ([Id], [ExamId], [SubjectId], [Sequence], [TimeInMinutes], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (2, 2, 3, 2, 20, 6, 3, CAST(0x07137E402B816C3E0B AS DateTime2), 11, CAST(0x071B12B749756C3E0B AS DateTime2), 11)
INSERT [dbo].[OTExamSubjectMapping] ([Id], [ExamId], [SubjectId], [Sequence], [TimeInMinutes], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (3, 1, 3, 3, 20, 6, 3, CAST(0x07DA5C8944816C3E0B AS DateTime2), 11, CAST(0x074042C14C786C3E0B AS DateTime2), 11)
INSERT [dbo].[OTExamSubjectMapping] ([Id], [ExamId], [SubjectId], [Sequence], [TimeInMinutes], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (4, 3, 3, 2, 20, 6, 3, CAST(0x0780A5DB117E6C3E0B AS DateTime2), 11, CAST(0x0772395E85786C3E0B AS DateTime2), 11)
INSERT [dbo].[OTExamSubjectMapping] ([Id], [ExamId], [SubjectId], [Sequence], [TimeInMinutes], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (5, 1, 2, 2, 20, 6, 3, CAST(0x073C81EB9ABB6E3E0B AS DateTime2), 11, CAST(0x070374A3A37B6C3E0B AS DateTime2), 11)
INSERT [dbo].[OTExamSubjectMapping] ([Id], [ExamId], [SubjectId], [Sequence], [TimeInMinutes], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (6, 3, 2, 1, 20, 6, 3, CAST(0x07A2F5B832816C3E0B AS DateTime2), 11, CAST(0x07219056357E6C3E0B AS DateTime2), 11)
INSERT [dbo].[OTExamSubjectMapping] ([Id], [ExamId], [SubjectId], [Sequence], [TimeInMinutes], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (7, 3, 3, 2, 20, 6, 3, CAST(0x0762F1F037816C3E0B AS DateTime2), 11, CAST(0x07219056357E6C3E0B AS DateTime2), 11)
INSERT [dbo].[OTExamSubjectMapping] ([Id], [ExamId], [SubjectId], [Sequence], [TimeInMinutes], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (8, 3, 3, 2, 60, 6, 3, CAST(0x07B18A043E816C3E0B AS DateTime2), 11, CAST(0x07002DB5A77E6C3E0B AS DateTime2), 11)
INSERT [dbo].[OTExamSubjectMapping] ([Id], [ExamId], [SubjectId], [Sequence], [TimeInMinutes], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (9, 3, 3, 4, 30, 6, 3, CAST(0x071B05B8DBBB6E3E0B AS DateTime2), 11, CAST(0x07002DB5A77E6C3E0B AS DateTime2), 11)
INSERT [dbo].[OTExamSubjectMapping] ([Id], [ExamId], [SubjectId], [Sequence], [TimeInMinutes], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (10, 1, 5, 1, 20, 6, 1, CAST(0x070000000000000000 AS DateTime2), 0, CAST(0x0714631FB7BB6E3E0B AS DateTime2), 11)
INSERT [dbo].[OTExamSubjectMapping] ([Id], [ExamId], [SubjectId], [Sequence], [TimeInMinutes], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (11, 1, 6, 2, 20, 6, 1, CAST(0x070000000000000000 AS DateTime2), 0, CAST(0x0714631FB7BB6E3E0B AS DateTime2), 11)
INSERT [dbo].[OTExamSubjectMapping] ([Id], [ExamId], [SubjectId], [Sequence], [TimeInMinutes], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (12, 2, 5, 1, 30, 6, 1, CAST(0x070000000000000000 AS DateTime2), 0, CAST(0x07C765AD65BC6E3E0B AS DateTime2), 11)
INSERT [dbo].[OTExamSubjectMapping] ([Id], [ExamId], [SubjectId], [Sequence], [TimeInMinutes], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (13, 2, 6, 2, 30, 6, 1, CAST(0x070000000000000000 AS DateTime2), 0, CAST(0x07C765AD65BC6E3E0B AS DateTime2), 11)
INSERT [dbo].[OTExamSubjectMapping] ([Id], [ExamId], [SubjectId], [Sequence], [TimeInMinutes], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (14, 1, 7, 3, 20, 6, 1, CAST(0x070000000000000000 AS DateTime2), 0, CAST(0x0762C0FF90BC6E3E0B AS DateTime2), 11)
INSERT [dbo].[OTExamSubjectMapping] ([Id], [ExamId], [SubjectId], [Sequence], [TimeInMinutes], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (15, 2, 7, 3, 20, 6, 1, CAST(0x070000000000000000 AS DateTime2), 0, CAST(0x071A0A93B7BC6E3E0B AS DateTime2), 11)
INSERT [dbo].[OTExamSubjectMapping] ([Id], [ExamId], [SubjectId], [Sequence], [TimeInMinutes], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (16, 3, 5, 1, 20, 6, 1, CAST(0x070000000000000000 AS DateTime2), 0, CAST(0x076E2C9BBFBD6E3E0B AS DateTime2), 11)
INSERT [dbo].[OTExamSubjectMapping] ([Id], [ExamId], [SubjectId], [Sequence], [TimeInMinutes], [SchoolId], [StatusId], [LastUpdatedOn], [LastUpdatedBy], [CreatedOn], [CreatedBy]) VALUES (17, 3, 7, 2, 20, 6, 1, CAST(0x070000000000000000 AS DateTime2), 0, CAST(0x07457C0D73C46E3E0B AS DateTime2), 11)
SET IDENTITY_INSERT [dbo].[OTExamSubjectMapping] OFF
