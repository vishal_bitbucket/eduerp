/*
   Sunday, April 15, 20183:03:11 PM
   User: sa
   Server: EDUERPONLINE-PC
   Database: EduErp
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ApplicationLog
	DROP CONSTRAINT FK_ApplicationLog_LogSource
GO
ALTER TABLE dbo.LogSource SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_ApplicationLog
	(
	ID uniqueidentifier NOT NULL,
	UserID bigint NULL,
	IpAddress nvarchar(50) NULL,
	LogSourceID int NOT NULL,
	SchoolId int NULL,
	OSInfo text NULL,
	RequestUrl text NULL,
	Module varchar(250) NOT NULL,
	ClassName varchar(250) NOT NULL,
	MethodName varchar(250) NOT NULL,
	ExceptionMessage text NOT NULL,
	StackTrace text NOT NULL,
	InnerExceptionMessage text NOT NULL,
	InnerExceptionStackTrace text NOT NULL,
	LoggedDate datetime NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_ApplicationLog SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.ApplicationLog)
	 EXEC('INSERT INTO dbo.Tmp_ApplicationLog (ID, UserID, IpAddress, LogSourceID, OSInfo, RequestUrl, Module, ClassName, MethodName, ExceptionMessage, StackTrace, InnerExceptionMessage, InnerExceptionStackTrace, LoggedDate)
		SELECT ID, UserID, IpAddress, LogSourceID, OSInfo, RequestUrl, Module, ClassName, MethodName, ExceptionMessage, StackTrace, InnerExceptionMessage, InnerExceptionStackTrace, LoggedDate FROM dbo.ApplicationLog WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.ApplicationLog
GO
EXECUTE sp_rename N'dbo.Tmp_ApplicationLog', N'ApplicationLog', 'OBJECT' 
GO
ALTER TABLE dbo.ApplicationLog ADD CONSTRAINT
	PK_ApplicationLog PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.ApplicationLog ADD CONSTRAINT
	FK_ApplicationLog_LogSource FOREIGN KEY
	(
	LogSourceID
	) REFERENCES dbo.LogSource
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
