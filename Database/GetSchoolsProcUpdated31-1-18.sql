USE [EduErp]
GO
/****** Object:  StoredProcedure [dbo].[GetSchools]    Script Date: 01/31/2018 18:52:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[GetSchools]
AS
BEGIN
	Select sch.Id,sch.Code,sch.ParentSchoolId,sch.Name,sch.CityId,sch.Address1,sch.Address2,sch.Address1+' '+sch.Address2 as [Address],sch.PIN,sch.OwnerName,sch.ContactNo,sch.CreatedOn,
	sch.CreatedBy,sch.StatusId,sch.ImagePath,psch.Name AS ParentSchoolName,cty.Name AS CityName--PM.MaxStudentLimit,PM.MaxUserLimit,PM.ValidityInMonth 
	from SchoolMaster sch 	 LEFT OUTER JOIN SchoolMaster psch ON sch.Id = psch.ParentSchoolId
	--INNER JOIN PlanMaster PM ON sch.Id=PM.SchoolId	
	INNER JOIN CityMaster cty ON sch.CityId = cty.Id
	Where sch.StatusId <> 3
END
GO
