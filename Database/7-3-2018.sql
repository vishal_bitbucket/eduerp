USE [EduErp]
GO
/****** Object:  Table [dbo].[CertificateTemplateMaster]    Script Date: 03/07/2018 18:31:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CertificateTemplateMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TemplateName] [nvarchar](50) NULL,
	[TemplateTypeId] [int] NULL,
	[HtmlTemplate] [nvarchar](max) NULL,
 CONSTRAINT [PK_CertificateTemplateMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[CertificateTemplateMaster] ON
INSERT [dbo].[CertificateTemplateMaster] ([Id], [TemplateName], [TemplateTypeId], [HtmlTemplate]) VALUES (1, N'Tc Type1', 1, N'')
INSERT [dbo].[CertificateTemplateMaster] ([Id], [TemplateName], [TemplateTypeId], [HtmlTemplate]) VALUES (2, N'Tc Type2', 1, N'')
INSERT [dbo].[CertificateTemplateMaster] ([Id], [TemplateName], [TemplateTypeId], [HtmlTemplate]) VALUES (3, N'Character', 5, N'')
INSERT [dbo].[CertificateTemplateMaster] ([Id], [TemplateName], [TemplateTypeId], [HtmlTemplate]) VALUES (4, N'Bonafide', 3, N'')
INSERT [dbo].[CertificateTemplateMaster] ([Id], [TemplateName], [TemplateTypeId], [HtmlTemplate]) VALUES (5, N'Participation', 2, N'')
SET IDENTITY_INSERT [dbo].[CertificateTemplateMaster] OFF
/****** Object:  Table [dbo].[MenuMaster]    Script Date: 03/07/2018 18:31:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MenuMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[ParentId] [int] NULL,
	[StatusId] [int] NOT NULL,
	[ActionName] [varchar](50) NOT NULL,
	[Controller] [varchar](50) NOT NULL,
	[IconClass] [varchar](50) NULL,
	[ClassName] [varchar](50) NULL,
	[IsSuperAdmin] [bit] NOT NULL,
	[MenuOrder] [int] NOT NULL,
	[IsAjax] [bit] NULL,
 CONSTRAINT [PK_MenuMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[MenuMaster] ON
INSERT [dbo].[MenuMaster] ([Id], [Name], [ParentId], [StatusId], [ActionName], [Controller], [IconClass], [ClassName], [IsSuperAdmin], [MenuOrder], [IsAjax]) VALUES (30, N'Certificate', 25, 1, N'Index', N'Certificate', N'fa fa-certificate', N'', 0, 28, 0)
SET IDENTITY_INSERT [dbo].[MenuMaster] OFF
/****** Object:  Table [dbo].[KeyWordMaster]    Script Date: 03/07/2018 18:31:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[KeyWordMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[KeyWordId] [int] NOT NULL,
	[KeyWordName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_KeyWordMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[KeyWordMaster] ON

INSERT [dbo].[KeyWordMaster] ([Id], [Name], [KeyWordId], [KeyWordName]) VALUES (88, N'TemplateType', 1, N'Transfer Certificate')
INSERT [dbo].[KeyWordMaster] ([Id], [Name], [KeyWordId], [KeyWordName]) VALUES (89, N'TemplateType', 2, N'Participation Certificate')
INSERT [dbo].[KeyWordMaster] ([Id], [Name], [KeyWordId], [KeyWordName]) VALUES (90, N'TemplateType', 3, N'Bonafide Certificate')
INSERT [dbo].[KeyWordMaster] ([Id], [Name], [KeyWordId], [KeyWordName]) VALUES (91, N'TemplateType', 4, N'Performa Study Certificate')
INSERT [dbo].[KeyWordMaster] ([Id], [Name], [KeyWordId], [KeyWordName]) VALUES (92, N'TemplateType', 5, N'Character Certificate')
SET IDENTITY_INSERT [dbo].[KeyWordMaster] OFF
/****** Object:  Table [dbo].[CertificateTemplateSchoolMapping]    Script Date: 03/07/2018 18:31:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CertificateTemplateSchoolMapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CertificateTemplateId] [int] NULL,
	[SchoolId] [int] NOT NULL,
	[StatusId] [int] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedBy] [int] NULL,
	[LastUpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_CertificateTemplateSchoolMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_CertificateTemplateSchoolMapping_CertificateTemplateMaster]    Script Date: 03/07/2018 18:31:27 ******/
ALTER TABLE [dbo].[CertificateTemplateSchoolMapping]  WITH CHECK ADD  CONSTRAINT [FK_CertificateTemplateSchoolMapping_CertificateTemplateMaster] FOREIGN KEY([CertificateTemplateId])
REFERENCES [dbo].[CertificateTemplateMaster] ([Id])
GO
ALTER TABLE [dbo].[CertificateTemplateSchoolMapping] CHECK CONSTRAINT [FK_CertificateTemplateSchoolMapping_CertificateTemplateMaster]
GO
