USE [EduErp]
GO
/****** Object:  StoredProcedure [dbo].[GetFeeHeads]    Script Date: 02/15/2018 18:05:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetFeeHeads]
@SchoolId int
as
begin
select  f.Id,f.Name,f.Description,f.FeeHeadTypeId,f.SchoolId,f.StatusId,k.KeyWordName as FeeHeadTypeName
       from FeeHeadMaster f 
       join KeyWordMaster k on f.FeeHeadTypeId=k.KeyWordId 
      where f.StatusId<>3 and f.SchoolId=@SchoolId and k.Name='FeeHeadType'

end
GO
/****** Object:  Table [dbo].[FineDiscountTransactionDetail]    Script Date: 02/15/2018 18:05:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FineDiscountTransactionDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FeeHeadId] [int] NOT NULL,
	[TransactionId] [int] NOT NULL,
	[FeeHeadAmount] [decimal](18, 2) NULL,
 CONSTRAINT [PK_FineDiscountTransactionDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[FineDiscountTransactionDetail] ON
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (1, 150, 53, CAST(200.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (2, 149, 53, CAST(100.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (3, 150, 54, CAST(200.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (4, 149, 54, CAST(100.00 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[FineDiscountTransactionDetail] OFF
/****** Object:  StoredProcedure [dbo].[GetFeeHeadDetial]    Script Date: 02/15/2018 18:05:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[GetFeeHeadDetial] 6,6,25,1,3,2,19
CREATE Proc [dbo].[GetFeeHeadDetial]
@SchoolId int,
@SessionId int,
@ClassId int ,
@FromMonth int,
@ToMonth int,
@HeadTypeId int,
@StudentId int
AS
BEGIN
	Select 
		fhm.Id as FeeHeadMasterId,fhm.Name,fhm.Description,fhm.IsDiscount,fhm.FeeHeadTypeId,fhm.SchoolId,fhm.CreatedOn,fhm.CreatedBy,fhm.StatusId,fhm.LastModifiedOn,fhm.LastModifiedBy,
		fhmm.Id as FeeHeadMappingId,fhmm.FeeHeadId,fhmm.ClassId,fhmm.SessionId,fhmm.MonthId,fhmm.Amount,
		km.KeyWordName as FeeMonthName,
		kh.KeyWordName as FeeHeadTypeName,
		cm.Name as ClassName , sm.Name as SessionName
	From 
		[dbo].[FeeHeadMaster] fhm 
		INNER JOIN [dbo].[FeeHeadMonthMapping] fhmm ON fhmm.FeeHeadId = fhm.Id
		Left OUTER JOIN KeyWordMaster km ON km.KeyWordId = fhmm.MonthId
		Left OUTER JOIN KeyWordMaster kh ON kh.KeyWordId = fhm.Id
		Left OUTER JOIN ClassMaster cm ON cm.Id = fhmm.ClassId
		Left OUTER JOIN SessionMaster sm ON sm.Id = fhmm.SessionId
	Where fhm.StatusId = 1 AND km.Name='Month' AND fhm.SchoolId = @SchoolId AND fhmm.SessionId  = @SessionId AND fhmm.ClassId = @ClassId AND fhm.FeeHeadTypeId = @HeadTypeId
	AND fhmm.MonthId BETWEEN @FromMonth and @ToMonth
	AND
	fhmm.Id Not In (
	Select id from FeeHeadMonthMapping Where FeeHeadMonthMapping.Id in (
select fd.FeeHeadMappingId  from feetransaction f inner join FeeTransactionDetail fd ON fd.FeeTransactionId = f.Id 
where StudentId = @StudentId and f.FromMonthId >= @FromMonth and f.ToMonthId <= @ToMonth))
	END
	
	--select * from [FeeHeadMaster]
	--select * from FeeHeadMonthMapping
		--select * from FeeTransactionDetail
		--select * from FeeHeadMonthMapping
		
--select * from sessionMaster where SchoolId = 6 and GETDATE() between FromDate and todate

--truncate table finediscounttransactiondetail
--delete from  feetransaction
--select * from FeeTransaction

--sp_helptext gettotalpaidunpaid
--drop proc gettotalpaidunpaid
GO
/****** Object:  ForeignKey [FK_FineDiscountTransactionDetail_FeeHeadMaster]    Script Date: 02/15/2018 18:05:18 ******/
ALTER TABLE [dbo].[FineDiscountTransactionDetail]  WITH CHECK ADD  CONSTRAINT [FK_FineDiscountTransactionDetail_FeeHeadMaster] FOREIGN KEY([FeeHeadId])
REFERENCES [dbo].[FeeHeadMaster] ([Id])
GO
ALTER TABLE [dbo].[FineDiscountTransactionDetail] CHECK CONSTRAINT [FK_FineDiscountTransactionDetail_FeeHeadMaster]
GO
/****** Object:  ForeignKey [FK_FineDiscountTransactionDetail_FeeTransaction]    Script Date: 02/15/2018 18:05:18 ******/
ALTER TABLE [dbo].[FineDiscountTransactionDetail]  WITH CHECK ADD  CONSTRAINT [FK_FineDiscountTransactionDetail_FeeTransaction] FOREIGN KEY([TransactionId])
REFERENCES [dbo].[FeeTransaction] ([Id])
GO
ALTER TABLE [dbo].[FineDiscountTransactionDetail] CHECK CONSTRAINT [FK_FineDiscountTransactionDetail_FeeTransaction]
GO
