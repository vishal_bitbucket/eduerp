USE [EduErp]
GO
/****** Object:  StoredProcedure [dbo].[GetAccountDetail]    Script Date: 02/22/2018 18:25:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetAccountDetail]

As
Begin
Select
 ac.Id,ac.SchoolId,ac.StatusId,ac.LeftStudentCount,ac.LeftUserCount,ac.LeftMessageCount,ac.PlanId,ac.DueDate,ac.NextPaymentDate
,s.Name as SchoolName

From  AccountDetail ac
               INNER JOIN SchoolMaster s on s.Id=ac.SchoolId
      where  ac.StatusId<>3
End
GO
