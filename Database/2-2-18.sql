USE [EduErp]
GO
/****** Object:  Table [dbo].[AutomateMessage]    Script Date: 02/02/2018 17:22:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AutomateMessage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NULL,
	[Name] [varchar](150) NULL,
	[Description] [varchar](500) NULL,
 CONSTRAINT [PK_AutomateMessage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[AutomateMessage] ON
INSERT [dbo].[AutomateMessage] ([Id], [ParentId], [Name], [Description]) VALUES (1, 0, N'Attendance', N'This is for attendance module')
INSERT [dbo].[AutomateMessage] ([Id], [ParentId], [Name], [Description]) VALUES (2, 1, N'Absent', N'DEAR PARENT: Your child  is absent today')
INSERT [dbo].[AutomateMessage] ([Id], [ParentId], [Name], [Description]) VALUES (3, 0, N'Fees', N'This is fee module')
INSERT [dbo].[AutomateMessage] ([Id], [ParentId], [Name], [Description]) VALUES (4, 3, N'DueFees', N'Please pay due fee immediately to attend exams')
INSERT [dbo].[AutomateMessage] ([Id], [ParentId], [Name], [Description]) VALUES (5, 3, N'DueFees', N'Your child  is paid Amount today and Balance is still remaining, thank you for fee payment')
SET IDENTITY_INSERT [dbo].[AutomateMessage] OFF
/****** Object:  Table [dbo].[TemplateMaster]    Script Date: 02/02/2018 17:22:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TemplateMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Text] [varchar](500) NOT NULL,
 CONSTRAINT [PK_TemplateMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TemplateMaster] ON
INSERT [dbo].[TemplateMaster] ([Id], [Name], [Text]) VALUES (1, N'User', N'User has been created successfully')
INSERT [dbo].[TemplateMaster] ([Id], [Name], [Text]) VALUES (2, N'Test', N'This is a test sms from template')
SET IDENTITY_INSERT [dbo].[TemplateMaster] OFF
/****** Object:  Table [dbo].[MessageAutomateMapping]    Script Date: 02/02/2018 17:22:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MessageAutomateMapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MessageAutomateId] [int] NOT NULL,
	[TemplateId] [int] NOT NULL,
 CONSTRAINT [PK_MessageAutomateMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_MessageAutomateMapping_AutomateMessage]    Script Date: 02/02/2018 17:22:10 ******/
ALTER TABLE [dbo].[MessageAutomateMapping]  WITH CHECK ADD  CONSTRAINT [FK_MessageAutomateMapping_AutomateMessage] FOREIGN KEY([MessageAutomateId])
REFERENCES [dbo].[AutomateMessage] ([Id])
GO
ALTER TABLE [dbo].[MessageAutomateMapping] CHECK CONSTRAINT [FK_MessageAutomateMapping_AutomateMessage]
GO
/****** Object:  ForeignKey [FK_MessageAutomateMapping_TemplateMaster]    Script Date: 02/02/2018 17:22:10 ******/
ALTER TABLE [dbo].[MessageAutomateMapping]  WITH CHECK ADD  CONSTRAINT [FK_MessageAutomateMapping_TemplateMaster] FOREIGN KEY([TemplateId])
REFERENCES [dbo].[TemplateMaster] ([Id])
GO
ALTER TABLE [dbo].[MessageAutomateMapping] CHECK CONSTRAINT [FK_MessageAutomateMapping_TemplateMaster]
GO
