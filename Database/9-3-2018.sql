USE [EduErp]
GO
/****** Object:  Table [dbo].[CertificateTemplateMaster]    Script Date: 03/09/2018 18:33:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CertificateTemplateMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TemplateName] [nvarchar](50) NULL,
	[TemplateTypeId] [int] NULL,
	[HtmlTemplate] [nvarchar](max) NULL,
 CONSTRAINT [PK_CertificateTemplateMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[CertificateTemplateMaster] ON
INSERT [dbo].[CertificateTemplateMaster] ([Id], [TemplateName], [TemplateTypeId], [HtmlTemplate]) VALUES (1, N'Tc Type1', 1, N'<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Transfer Certificate</title>
    <style>
        .space {
            padding-left: 290px;
        }
        .mid {
            padding-left: 30px;
            font-size: 10px;
            
        }
        .adjust {
            padding-left: 43px;
        }
        .side-adjust {
            padding-left: 215px;
        }
        .sidejust {
            padding-left: 47px;
        }
        .adjust-top {
            padding-top: 70px;
        }
        .adjust-topp {
            padding-top: 70px;
            padding-left: 290px;
        }
        .adjust-toper {
            padding-top: 70px;
            padding-left: 350px;
        }
        .border {
            padding-left: 120px;
            line-height: 1.8;
        }
        .side{ padding-left: 350px;}
    </style>
</head>
<body  style="border:3px; border-style:solid; border-color:black; padding: 1em;">

<table class="border">
    <tr>
        <td>
            <img src="NewFolder1/Aryabhatta_international_school_logo.jpg" alt=""/>
        </td>

        <td class="side-adjust">
            <h2 class="adjust">
                [#SchoolName]
            </h2>
            <h4 class="sidejust">
          [#SchoolAddress]
            </h4>
        [#SchoolWebsite] &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[#SchoolEmail]<br>
            <h2 class="mid">
                <input type="text" name="firstname" value="TRANSFER CERTIFICATE" style="font-weight: bolder; border-color: black"/>
            </h2>
            <h2 class="space">Academic year:[#Year]</h2>

        </td>

    </tr>

</table>

<table class="border">
    <tr>
        <td>
            Serial No.:[#SerialNo.]
        </td>
        <td class="side">
            Admission No.: [#AdmissionNo.]
        </td>
    </tr>

    <tr>
        <td>
            Name of the Student
        </td>
        <td class="side">
            : [#StudentName]
        </td>
    </tr>

    <tr>
        <td>
            Gender
        </td>
        <td class="side">
            : [#StudentGender]
        </td>
    </tr>

    <tr>
        <td>
            Father/Gaurdian Name
        </td>
        <td class="side">
            : [#StudentFatherName]
        </td>
    </tr>
    <tr>
        <td>
            Mothers Name
        </td>
        <td class="side">
            : [#StudentMotherName]
        </td>
    </tr>
    <tr>
        <td>
            Date of Admission
        </td>
        <td class="side">
            : [#StudentDateOfAdmission]
        </td>
    </tr>
    <tr>
        <td>
            Class into which Admitted
        </td>
        <td class="side">
            : ________________________________________
        </td>
    </tr>
    <tr>
        <td>
            Date of Birth as per Admission Register
        </td>
        <td class="side">
            : [#StudentDateOfBirth]
        </td>
    </tr>
    <tr>
        <td>
            Nationality
        </td>
        <td class="side">
            : [#StudentNationality]
        </td>
    </tr>
    <tr>
        <td>
            Religion
        </td>
        <td class="side">
            : [#StudentReligion]
        </td>
    </tr>
    <tr>
        <td>
            Whether belonging to
        </td>
        <td class="side">
            : [#StudentWhetherBelongingTo]
        </td>
    </tr>

    <tr>
        <td>
            Class in which the Student studied last
        </td>
        <td class="side">
            : (in figures)____________(in words)___________
        </td>
    </tr>
    <tr>
        <td>
            Whether qualified for promotion to the higher class
        </td>
        <td class="side">
            : ________________________________________
        </td>
    </tr>
    <tr>
        <td>
            If Yes,to which class
        </td>
        <td class="side">
            : ________________________________________
        </td>
    </tr>
    <tr>
        <td>
            Subjects studied
        </td>
        <td class="side">
            : [#SubjectsStudied]
        </td>
    </tr>
    <tr>
        <td>
            School fee dues if any
        </td>
        <td class="side">
            : [#SchoolFeeDuesIfAny]
        </td>
    </tr>
    <tr>
        <td>
            Total No. of Working days
        </td>
        <td class="side">
            : ________________________________________
        </td>
    </tr>
    <tr>
        <td>
            Total No. of days present
        </td>
        <td class="side">
            : ________________________________________
        </td>
    </tr>
    <tr>
        <td>
            Games played or extra-curricular activities in which<br>
            the pupil has participated<br>
            (AchievementLevel therein)<br>
        </td>
        <td class="side">
            : ________________________________________
        </td>
    </tr>
    <tr>
        <td>
            General Conduct
        </td>
        <td class="side">
            : ________________________________________
        </td>
    </tr>
    <tr>
        <td>
            Date of application for the Certificate
        </td>
        <td class="side">
            : [#DateOfApplicationForTheCertificate]
        </td>
    </tr>
    <tr>
        <td>
            Date of issue of the Certificate
        </td>
        <td class="side">
            : [#DateOfIssueOfTheCertificate]
        </td>
    </tr>
    <tr>
        <td>
            Reason  leaving the school
        </td>
        <td class="side">
            : ________________________________________
        </td>
    </tr>
    <tr>
        <td>
            Other remarks 
        </td>
        <td class="side">
            : ________________________________________
        </td>
    </tr>

</table>
<table class="border">

    <th class="adjust-top">
        Signature of the Parent/Guardian
    </th>

    <th class="adjust-topp">
        Checked By
    </th>
    <th class="adjust-toper">
        Principal
    </th>
</table>
    
</body>
</html>')
INSERT [dbo].[CertificateTemplateMaster] ([Id], [TemplateName], [TemplateTypeId], [HtmlTemplate]) VALUES (2, N'Tc Type2', 1, N'')
INSERT [dbo].[CertificateTemplateMaster] ([Id], [TemplateName], [TemplateTypeId], [HtmlTemplate]) VALUES (3, N'Character', 5, N'')
INSERT [dbo].[CertificateTemplateMaster] ([Id], [TemplateName], [TemplateTypeId], [HtmlTemplate]) VALUES (4, N'Bonafide', 3, N'')
INSERT [dbo].[CertificateTemplateMaster] ([Id], [TemplateName], [TemplateTypeId], [HtmlTemplate]) VALUES (5, N'Participation', 2, N'')
SET IDENTITY_INSERT [dbo].[CertificateTemplateMaster] OFF
/****** Object:  StoredProcedure [dbo].[GetStudentDetailForCertificate]    Script Date: 03/09/2018 18:33:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetStudentDetailForCertificate]
@SessionId int,
@ClassId int,
@SectionId int,
@StudentId int

as
begin
select s.Id,s.TitleId,k.KeyWordName as TitleName,s.StudentMobile,s.FirstName,s.LastName,s.MiddileName,s.DOB,s.DOJ,s.Email,s.Uid,s.ImagePath,s.S3key,s.StatusId
,s.CorrAddress as CuurentAddress,s.CorrPinCode as CurrentPinCode,s.FatherName,s.MotherName,s.GuardianAddress,s.GuardianMobileNo as GuardianMobile,s.GuardianName
,s.GuardianOccupation,s.GuardianRelationWithStudent as GuardianRelation,s.ParentContactNo,s.ParentMobileNo as ParentMobileNumber,s.PermanentAddress
,s.PermanentPinCode,s.CategoryId,s.GenderId,cm.Name as ClassName,sm.Name as SectionName,cmm.Name as CurrentCityName,smmm.Name as PermanentStateName
,ks.KeyWordName as RelegionCategoryName,ssn.Name as SessionName ,scm.StudentCode,smm.Name as CurrentStateName,cmmm.Name as PermanentCityName,km.KeyWordName as GenderName
,kmm.KeyWordName as NationlityName,kmmm.KeyWordName as RelegionName,sclm.Name as SchoolName,sclm.Address1 as SchoolAddress
from  Student s inner join KeyWordMaster k on s.TitleId=k.KeyWordId
               left outer join KeyWordMaster ks on s.CategoryId=ks.KeyWordId
               inner join KeyWordMaster km on s.GenderId=km.KeyWordId
               inner join KeyWordMaster kmm on s.NationlityId=kmm.KeyWordId
               inner join KeyWordMaster kmmm on s.RelegionId=kmmm.KeyWordId
               inner join StudentClassMapping scm on s.id=scm.StudentId
               inner  join SectionMaster sm on scm.SectionId=sm.Id
               inner join ClassMaster cm on cm.Id=scm.ClassId
               inner join SessionMaster ssn on ssn.Id=scm.SessionId
               inner join CityMaster cmm on cmm.Id= s.CorrCityId 
               inner join CityMaster cmmm on cmmm.Id=s.PermanentCityId
               inner join StateMaster smm on smm.Id=cmm.StateId  
               inner join StateMaster smmm on smmm.Id=cmmm.StateId
               inner join SchoolMaster sclm on s.SchoolId=sclm.Id             
where s.Id=@StudentId and scm.SessionId=@SessionId and scm.ClassId=@ClassId  and s.StatusId=1 and k.Name='Title' and ks.Name='Category' and km.Name='Gender' and kmm.Name='Nationlity' and kmmm.Name='Relegion'
end
GO
