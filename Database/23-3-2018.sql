USE [EduErp]
GO
/****** Object:  StoredProcedure [dbo].[GetPreviousBalance]    Script Date: 03/23/2018 19:24:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetPreviousBalance]
@StudentId int
AS
BEGIN
       SELECT TOP 1 BalanceAmount FROM (
  select TOP 2 * from FeeTransaction where StudentId=@StudentId order By FeeTransaction.Id desc)  T  where StudentId=@StudentId ORDER BY Id asc;
END
GO
/****** Object:  StoredProcedure [dbo].[GetStudents]    Script Date: 03/23/2018 19:24:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetStudents]
@SessionId int,
@ClassId int,
@SectionId int,
@SchoolId int
AS
Select s.Id as StudentId,s.FirstName as StudentName,s.MiddileName,s.LastName,s.TitleId,s.Email,s.StudentMobile as StudentContactNumber,scm.StudentCode as StudentRollNumber,
  s.MotherName,s.FatherName,s.CorrCityId,s.GenderId,s.SchoolId,s.Uid,
  s.DOJ,s.DOB,s.DOE,s.StatusId as StudentStatusId,s.CreatedOn,s.CreatedBy,s.LastUpdatedOn,s.LastUpdatedBy,s.ImagePath,
  s.RelegionId,s.CategoryId,s.ReasonForExit,s.NationlityId,
  kg.KeyWordName as StudentGender, kn.KeyWordName as NationlityName, kr.KeyWordName as  RelegionName,
  kt.KeyWordName as TitleName ,kc.KeyWordName as StudentCategory
 From Student s inner join StudentClassMapping scm on s.Id=scm.StudentId
  left outer join KeyWordMaster kg ON kg.KeyWordId = s.GenderId AND kg.Name = 'Gender'
left outer join KeyWordMaster kn ON kn.KeyWordId = s.NationlityId AND kn.Name = 'Nationlity'
 left outer join KeyWordMaster kr ON kr.KeyWordId = s.RelegionId AND kr.Name = 'Relegion'
 left outer  join KeyWordMaster kc ON kc.KeyWordId = s.CategoryId AND kc.Name = 'Category'
 left outer join KeyWordMaster kt ON kt.KeyWordId = s.TitleId AND kt.Name = 'Title'
Where s.SchoolId = @SchoolId  and scm.ClassId=@ClassId and scm.SectionId=@SectionId and scm.SessionId=@SessionId
GO
/****** Object:  StoredProcedure [dbo].[GetInActiveStudents]    Script Date: 03/23/2018 19:24:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetInActiveStudents]
@SessionId int,
@ClassId int,
@SectionId int,
@SchoolId int
AS
Select s.Id as StudentId,s.FirstName as StudentName,s.MiddileName,s.LastName,s.TitleId,s.Email,s.StudentMobile as StudentContactNumber,scm.StudentCode as StudentRollNumber,
  s.MotherName,s.FatherName,s.CorrCityId,s.GenderId,s.SchoolId,s.Uid,
  s.DOJ,s.DOB,s.DOE,s.StatusId as StudentStatusId,s.CreatedOn,s.CreatedBy,s.LastUpdatedOn,s.LastUpdatedBy,s.ImagePath,
  s.RelegionId,s.CategoryId,s.ReasonForExit,s.NationlityId,
  kg.KeyWordName as StudentGender, kn.KeyWordName as NationlityName, kr.KeyWordName as  RelegionName,
  kt.KeyWordName as TitleName ,kc.KeyWordName as StudentCategory
 From Student s inner join StudentClassMapping scm on s.Id=scm.StudentId
  left outer join KeyWordMaster kg ON kg.KeyWordId = s.GenderId AND kg.Name = 'Gender'
left outer join KeyWordMaster kn ON kn.KeyWordId = s.NationlityId AND kn.Name = 'Nationlity'
 left outer join KeyWordMaster kr ON kr.KeyWordId = s.RelegionId AND kr.Name = 'Relegion'
 left outer  join KeyWordMaster kc ON kc.KeyWordId = s.CategoryId AND kc.Name = 'Category'
 left outer join KeyWordMaster kt ON kt.KeyWordId = s.TitleId AND kt.Name = 'Title'
Where s.SchoolId = @SchoolId  and scm.ClassId=@ClassId and scm.SectionId=@SectionId and scm.SessionId=@SessionId AND s.StatusId =2
GO
/****** Object:  StoredProcedure [dbo].[GetDeleteStudents]    Script Date: 03/23/2018 19:24:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetDeleteStudents]
@SessionId int,
@ClassId int,
@SectionId int,
@SchoolId int
AS
Select s.Id as StudentId,s.FirstName as StudentName,s.MiddileName,s.LastName,s.TitleId,s.Email,s.StudentMobile as StudentContactNumber,scm.StudentCode as StudentRollNumber,
  s.MotherName,s.FatherName,s.CorrCityId,s.GenderId,s.SchoolId,s.Uid,
  s.DOJ,s.DOB,s.DOE,s.StatusId as StudentStatusId,s.CreatedOn,s.CreatedBy,s.LastUpdatedOn,s.LastUpdatedBy,s.ImagePath,
  s.RelegionId,s.CategoryId,s.ReasonForExit,s.NationlityId,
  kg.KeyWordName as StudentGender, kn.KeyWordName as NationlityName, kr.KeyWordName as  RelegionName,
  kt.KeyWordName as TitleName ,kc.KeyWordName as StudentCategory
 From Student s inner join StudentClassMapping scm on s.Id=scm.StudentId
  left outer join KeyWordMaster kg ON kg.KeyWordId = s.GenderId AND kg.Name = 'Gender'
left outer join KeyWordMaster kn ON kn.KeyWordId = s.NationlityId AND kn.Name = 'Nationlity'
 left outer join KeyWordMaster kr ON kr.KeyWordId = s.RelegionId AND kr.Name = 'Relegion'
 left outer  join KeyWordMaster kc ON kc.KeyWordId = s.CategoryId AND kc.Name = 'Category'
 left outer join KeyWordMaster kt ON kt.KeyWordId = s.TitleId AND kt.Name = 'Title'
Where s.SchoolId = @SchoolId  and scm.ClassId=@ClassId and scm.SectionId=@SectionId and scm.SessionId=@SessionId AND s.StatusId =3
GO
/****** Object:  StoredProcedure [dbo].[GetActiveStudents]    Script Date: 03/23/2018 19:24:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetActiveStudents]
@SessionId int,
@ClassId int,
@SectionId int,
@SchoolId int
AS
Select s.Id as StudentId,s.FirstName as StudentName,s.MiddileName,s.LastName,s.TitleId,s.Email,s.StudentMobile as StudentContactNumber,scm.StudentCode as StudentRollNumber,
  s.MotherName,s.FatherName,s.CorrCityId,s.GenderId,s.SchoolId,s.Uid,
  s.DOJ,s.DOB,s.DOE,s.StatusId as StudentStatusId,s.CreatedOn,s.CreatedBy,s.LastUpdatedOn,s.LastUpdatedBy,s.ImagePath,
  s.RelegionId,s.CategoryId,s.ReasonForExit,s.NationlityId,
  kg.KeyWordName as StudentGender, kn.KeyWordName as NationlityName, kr.KeyWordName as  RelegionName,
  kt.KeyWordName as TitleName ,kc.KeyWordName as StudentCategory
 From Student s inner join StudentClassMapping scm on s.Id=scm.StudentId
  Left outer join KeyWordMaster kg ON kg.KeyWordId = s.GenderId AND kg.Name = 'Gender'
left outer join KeyWordMaster kn ON kn.KeyWordId = s.NationlityId AND kn.Name = 'Nationlity'
 left outer join KeyWordMaster kr ON kr.KeyWordId = s.RelegionId AND kr.Name = 'Relegion'
 left outer  join KeyWordMaster kc ON kc.KeyWordId = s.CategoryId AND kc.Name = 'Category'
 left outer join KeyWordMaster kt ON kt.KeyWordId = s.TitleId AND kt.Name = 'Title'
Where s.SchoolId = @SchoolId  and scm.ClassId=@ClassId and scm.SectionId=@SectionId and scm.SessionId=@SessionId AND s.StatusId =1
GO
