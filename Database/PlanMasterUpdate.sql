/*
   Monday, January 22, 201812:20:11 PM
   User: sa
   Server: DELL-PC
   Database: EduErp
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.SchoolMaster SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.PlanMaster ADD CONSTRAINT
	FK_PlanMaster_SchoolMaster FOREIGN KEY
	(
	SchoolId
	) REFERENCES dbo.SchoolMaster
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.PlanMaster SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
