USE [EduErp]
GO
/****** Object:  Table [dbo].[PlanMaster]    Script Date: 02/11/2018 17:48:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlanMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Detail] [nvarchar](200) NULL,
	[PlanTypeId] [int] NULL,
	[MaxStudentLimit] [int] NULL,
	[MaxUserLimit] [int] NULL,
	[MaxMessageLimit] [int] NULL,
	[StatusId] [int] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_PlanMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[PlanMaster] ON
INSERT [dbo].[PlanMaster] ([Id], [Name], [Detail], [PlanTypeId], [MaxStudentLimit], [MaxUserLimit], [MaxMessageLimit], [StatusId], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (3, N'Half-yearly', N'ABC', NULL, 2, 4, 20, 1, 1, CAST(0x0000A82100000000 AS DateTime), 1, CAST(0x0000A822010D2764 AS DateTime))
INSERT [dbo].[PlanMaster] ([Id], [Name], [Detail], [PlanTypeId], [MaxStudentLimit], [MaxUserLimit], [MaxMessageLimit], [StatusId], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (4, N'Half-yearly', N'XYZ', NULL, 14, 1, 30, 1, 1, CAST(0x0000A82200E48DAD AS DateTime), 9, CAST(0x0000A87201221690 AS DateTime))
INSERT [dbo].[PlanMaster] ([Id], [Name], [Detail], [PlanTypeId], [MaxStudentLimit], [MaxUserLimit], [MaxMessageLimit], [StatusId], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (5, N'Quaterly', N'XYZ', NULL, 20, 24, 10, 1, 1, CAST(0x0000A822015FF2D2 AS DateTime), 1, CAST(0x0000A82201602B9A AS DateTime))
INSERT [dbo].[PlanMaster] ([Id], [Name], [Detail], [PlanTypeId], [MaxStudentLimit], [MaxUserLimit], [MaxMessageLimit], [StatusId], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (7, N'asa', N'retr', 1, 34, 55, 45, 1, 9, CAST(0x0000A87900D56022 AS DateTime), NULL, NULL)
INSERT [dbo].[PlanMaster] ([Id], [Name], [Detail], [PlanTypeId], [MaxStudentLimit], [MaxUserLimit], [MaxMessageLimit], [StatusId], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (8, N'Message Booster', N'sf', 2, NULL, NULL, 44, 1, 9, CAST(0x0000A87900D68014 AS DateTime), 9, CAST(0x0000A87900D6DC91 AS DateTime))
SET IDENTITY_INSERT [dbo].[PlanMaster] OFF
/****** Object:  Table [dbo].[AutomateMessage]    Script Date: 02/11/2018 17:48:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AutomateMessage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NULL,
	[Name] [varchar](150) NULL,
	[Description] [varchar](500) NULL,
 CONSTRAINT [PK_AutomateMessage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[AutomateMessage] ON
INSERT [dbo].[AutomateMessage] ([Id], [ParentId], [Name], [Description]) VALUES (1, 0, N'Attendance', N'This is for attendance module')
INSERT [dbo].[AutomateMessage] ([Id], [ParentId], [Name], [Description]) VALUES (2, 1, N'Absent', N'DEAR PARENT: Your child  is absent today')
INSERT [dbo].[AutomateMessage] ([Id], [ParentId], [Name], [Description]) VALUES (3, 0, N'Fees', N'This is fee module')
INSERT [dbo].[AutomateMessage] ([Id], [ParentId], [Name], [Description]) VALUES (4, 3, N'DueFees', N'Please pay due fee immediately to attend exams')
INSERT [dbo].[AutomateMessage] ([Id], [ParentId], [Name], [Description]) VALUES (5, 3, N'DueFees', N'Your child  is paid Amount today and Balance is still remaining, thank you for fee payment')
INSERT [dbo].[AutomateMessage] ([Id], [ParentId], [Name], [Description]) VALUES (6, 0, N'User', N'If you check then message will be send automatically after creation of user')
INSERT [dbo].[AutomateMessage] ([Id], [ParentId], [Name], [Description]) VALUES (7, 6, N'User', N'If you check then message will be send automatically after creation of user')
SET IDENTITY_INSERT [dbo].[AutomateMessage] OFF
/****** Object:  Table [dbo].[TemplateMaster]    Script Date: 02/11/2018 17:48:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TemplateMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Text] [varchar](500) NOT NULL,
 CONSTRAINT [PK_TemplateMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TemplateMaster] ON
INSERT [dbo].[TemplateMaster] ([Id], [Name], [Text]) VALUES (1, N'User', N'User has been created successfully')
INSERT [dbo].[TemplateMaster] ([Id], [Name], [Text]) VALUES (2, N'Test', N'This is a test sms from template')
SET IDENTITY_INSERT [dbo].[TemplateMaster] OFF
/****** Object:  Table [dbo].[SchoolPlanMapping]    Script Date: 02/11/2018 17:48:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolPlanMapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SchoolId] [int] NOT NULL,
	[PlanId] [int] NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[StatusId] [int] NULL,
	[LastUpdateBy] [int] NULL,
	[LastUpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_SchoolPlanMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[SchoolPlanMapping] ON
INSERT [dbo].[SchoolPlanMapping] ([Id], [SchoolId], [PlanId], [StartDate], [EndDate], [StatusId], [LastUpdateBy], [LastUpdatedOn]) VALUES (9, 14, 8, CAST(0x0000A8D400000000 AS DateTime), CAST(0x0000A93000000000 AS DateTime), 1, 9, CAST(0x0000A87A00BE2D2E AS DateTime))
INSERT [dbo].[SchoolPlanMapping] ([Id], [SchoolId], [PlanId], [StartDate], [EndDate], [StatusId], [LastUpdateBy], [LastUpdatedOn]) VALUES (11, 11, 7, CAST(0x0000A91100000000 AS DateTime), CAST(0x0000A93000000000 AS DateTime), 1, 9, CAST(0x0000A87A00C65A73 AS DateTime))
SET IDENTITY_INSERT [dbo].[SchoolPlanMapping] OFF
/****** Object:  StoredProcedure [dbo].[GetSchools]    Script Date: 02/11/2018 17:48:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetSchools]
AS
BEGIN
	Select sch.Id,sch.Code,sch.ParentSchoolId,sch.Name,sch.CityId,sch.Address1,sch.Address2,sch.Address1+' '+sch.Address2 as [Address],sch.PIN,sch.OwnerName,sch.ContactNo,sch.CreatedOn,
	sch.CreatedBy,sch.StatusId,sch.ImagePath,psch.Name AS ParentSchoolName,cty.Name AS CityName--PM.MaxStudentLimit,PM.MaxUserLimit,PM.ValidityInMonth 
	from SchoolMaster sch 	 LEFT OUTER JOIN SchoolMaster psch ON sch.Id = psch.ParentSchoolId
	--INNER JOIN PlanMaster PM ON sch.Id=PM.SchoolId	
	INNER JOIN CityMaster cty ON sch.CityId = cty.Id
	Where sch.StatusId <> 3
END
GO
/****** Object:  Table [dbo].[MessageAutomateMapping]    Script Date: 02/11/2018 17:48:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MessageAutomateMapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MessageAutomateId] [int] NOT NULL,
	[TemplateId] [int] NOT NULL,
	[SchoolId] [int] NULL,
 CONSTRAINT [PK_MessageAutomateMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[GetInventoryByInventoryId]    Script Date: 02/11/2018 17:48:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetInventoryByInventoryId]
@Id int
as
begin
select i.Id,i.Amount,i.CreatedBy,i.CreatedOn,i.EndDate,i.StartDate,i.InvetoryTypeId,i.ItemDescription,i.VenderId
,i.ItemId,itm.Name as ItemName,vm.Name as VendorName,i.SessionId,i.StatusId,i.SchoolId,s.Name as SessionName,k.KeyWordName as InventoryName
from Inventory i join ItemMaster itm on i.InvetoryTypeId=itm.ExpensesTypeId
  join VendorMaster vm on vm.TypeId=i.InvetoryTypeId
join SessionMaster s on i.SessionId =s.Id
join KeyWordMaster k on k.KeyWordId=i.InvetoryTypeId  where i.id=@Id and k.Name='InventoryType'
end
GO
/****** Object:  StoredProcedure [dbo].[GetCountDetails]    Script Date: 02/11/2018 17:48:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetCountDetails]
@SchoolId int
as
begin
Select(
Select Count(*)  from Student Where ( SchoolId=@SchoolId or SchoolId=0) and StatusId=1 ) as TotalStudents,
(Select Count(*)  from ClassMaster Where ( SchoolId=@SchoolId or SchoolId=0) and Status=1) as TotalClasses,
(Select Count(*)  from SubjectMaster Where  (SchoolId=@SchoolId or SchoolId=0) and Status=1) as TotalSubjects,
( Select COUNT(*) from SectionMaster SM INNER JOIN ClassMaster CM ON SM.ClassId=CM.Id where  SM.Status=1 AND CM.SchoolId=@SchoolId ) as TotalSections
end
GO
/****** Object:  StoredProcedure [dbo].[GetStudentExamMappingListForEdit]    Script Date: 02/11/2018 17:48:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetStudentExamMappingListForEdit]
@StudentId int,
@SchoolId int,
@ExamTypeId int
AS BEGIN
SELECT        SubjectMaster.Name AS SubjectName, StudentExamMapping.ObtainedMarks AS ObtainMarks, ExamSubjectMapping.TotalMarks,StudentExamMapping.ExamSubjectMappingId
FROM            ExamMaster INNER JOIN
                         ExamSubjectMapping ON ExamMaster.Id = ExamSubjectMapping.ExamId INNER JOIN
                         SubjectMaster ON ExamSubjectMapping.SubjectId = SubjectMaster.Id INNER JOIN
                         StudentExamMapping ON ExamSubjectMapping.Id = StudentExamMapping.ExamSubjectMappingId
                         where StudentExamMapping.StudentId=@StudentId AND SubjectMaster.SchoolId=@SchoolId AND ExamMaster.Id=@ExamTypeId AND StudentExamMapping.StatusId<>3
                         
 END
GO
/****** Object:  StoredProcedure [dbo].[GetStudentExamMappingDetail]    Script Date: 02/11/2018 17:48:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetStudentExamMappingDetail]
@SchoolId int
AS
BEGIN
SELECT     ClassMaster.Name AS ClassName,ExamMaster.Id AS ExamId, ExamMaster.Name AS ExamName, SectionMaster.Name AS SectionName, Student.FirstName AS StudentName, StudentExamMapping.Id, StudentExamMapping.ObtainedMarks, 
                      StudentExamMapping.StatusId, StudentExamMapping.StudentId,StudentExamMapping.ExamSubjectMappingId,StudentExamMapping.ObtainedMarks AS ObtainMarks
FROM               StudentExamMapping INNER JOIN ClassMaster ON  StudentExamMapping.ClassId=ClassMaster.Id
                INNER JOIN SectionMaster ON SectionMaster.Id=StudentExamMapping.SectionId
                INNER JOIN ExamSubjectMapping ON ExamSubjectMapping.Id=StudentExamMapping.ExamSubjectMappingId
                INNER JOIN ExamMaster ON ExamMaster.Id=ExamSubjectMapping.ExamId
                INNER JOIN  Student ON Student.Id=StudentExamMapping.StudentId
               -- INNER JOIN ExamSubjectMapping ON ExamSubjectMapping.ExamId=ExamMaster.Id
                      WHERE  Student.SchoolId=@SchoolId AND Student.StatusId=1 AND StudentExamMapping.StatusId<>3
END
GO
/****** Object:  StoredProcedure [dbo].[GetStudentClassChart]    Script Date: 02/11/2018 17:48:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetStudentClassChart]
@SchoolId INT

AS BEGIN

SELECT        ClassMaster.Id, ClassMaster.Name, COUNT(StudentClassMapping.StudentId) AS TotalStudents
FROM            StudentClassMapping Left Outer JOIN
                         ClassMaster ON StudentClassMapping.ClassId = ClassMaster.Id
						 WHERE ClassMaster.SchoolId = @SchoolId AND ClassMaster.Status=1
						 GROUP BY ClassMaster.Name, ClassMaster.Id						 

UNION

SELECT Id, Name, 0 AS TotalStudents FROM ClassMaster WHERE ClassMaster.SchoolId = @SchoolId AND ClassMaster.Status=1 AND Id NOT IN
 (SELECT        ClassMaster.Id
FROM            StudentClassMapping Left Outer JOIN
                         ClassMaster ON StudentClassMapping.ClassId = ClassMaster.Id
						 WHERE ClassMaster.SchoolId = @SchoolId AND ClassMaster.Status=1)

END
GO
/****** Object:  ForeignKey [FK_MessageAutomateMapping_AutomateMessage]    Script Date: 02/11/2018 17:48:02 ******/
ALTER TABLE [dbo].[MessageAutomateMapping]  WITH CHECK ADD  CONSTRAINT [FK_MessageAutomateMapping_AutomateMessage] FOREIGN KEY([MessageAutomateId])
REFERENCES [dbo].[AutomateMessage] ([Id])
GO
ALTER TABLE [dbo].[MessageAutomateMapping] CHECK CONSTRAINT [FK_MessageAutomateMapping_AutomateMessage]
GO
/****** Object:  ForeignKey [FK_MessageAutomateMapping_TemplateMaster]    Script Date: 02/11/2018 17:48:02 ******/
ALTER TABLE [dbo].[MessageAutomateMapping]  WITH CHECK ADD  CONSTRAINT [FK_MessageAutomateMapping_TemplateMaster] FOREIGN KEY([TemplateId])
REFERENCES [dbo].[TemplateMaster] ([Id])
GO
ALTER TABLE [dbo].[MessageAutomateMapping] CHECK CONSTRAINT [FK_MessageAutomateMapping_TemplateMaster]
GO
/****** Object:  ForeignKey [FK_SchoolPlanMapping_PlanMaster]    Script Date: 02/11/2018 17:48:02 ******/
ALTER TABLE [dbo].[SchoolPlanMapping]  WITH CHECK ADD  CONSTRAINT [FK_SchoolPlanMapping_PlanMaster] FOREIGN KEY([PlanId])
REFERENCES [dbo].[PlanMaster] ([Id])
GO
ALTER TABLE [dbo].[SchoolPlanMapping] CHECK CONSTRAINT [FK_SchoolPlanMapping_PlanMaster]
GO
/****** Object:  ForeignKey [FK_SchoolPlanMapping_SchoolMaster]    Script Date: 02/11/2018 17:48:02 ******/
ALTER TABLE [dbo].[SchoolPlanMapping]  WITH CHECK ADD  CONSTRAINT [FK_SchoolPlanMapping_SchoolMaster] FOREIGN KEY([SchoolId])
REFERENCES [dbo].[SchoolMaster] ([Id])
GO
ALTER TABLE [dbo].[SchoolPlanMapping] CHECK CONSTRAINT [FK_SchoolPlanMapping_SchoolMaster]
GO
