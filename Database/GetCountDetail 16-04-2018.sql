USE [EduErp]
GO
/****** Object:  StoredProcedure [dbo].[GetCountDetails]    Script Date: 04/16/2018 21:42:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetCountDetails]
@SchoolId int
as
begin
Select(
Select Count(*)  from Student Where ( SchoolId=@SchoolId or SchoolId=0) and StatusId=1 ) as TotalStudents,
(Select Count(*)  from ClassMaster Where ( SchoolId=@SchoolId or SchoolId=0) and Status=1) as TotalClasses,
(Select Count(*)  from SubjectMaster Where  (SchoolId=@SchoolId or SchoolId=0) and Status=1) as TotalSubjects,
( Select COUNT(*) from SectionMaster SM INNER JOIN ClassMaster CM ON SM.ClassId=CM.Id where  SM.Status=1 AND CM.SchoolId=@SchoolId ) as TotalSections,
(Select COUNT(*) from UserMaster where SchoolId=@SchoolId  and StatusId=1) as TotalUsers,
( Select ac.LeftMessageCount  from AccountDetail ac where SchoolId=@SchoolId and ac.StatusId=1) as LeftMessage
end
GO
