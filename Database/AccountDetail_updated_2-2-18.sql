/*
   Friday, February 02, 20185:27:03 PM
   User: sa
   Server: DELL-PC
   Database: EduErp
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_AccountDetail
	(
	Id int NOT NULL IDENTITY (1, 1),
	PlanId int NULL,
	DueDate datetime NULL,
	NextPaymentDate datetime NULL,
	AppStartDate datetime NULL,
	SchoolId int NOT NULL,
	LeftUserCount int NULL,
	LeftStudentCount int NULL,
	TotalMessageCount int NULL,
	LeftMessageCount int NULL,
	WeeklyHolidays nvarchar(8) NULL,
	CreatedOn datetime NOT NULL,
	CreatedBy int NOT NULL,
	UpdatedOn datetime NULL,
	UpdatedBy int NULL,
	StatusId int NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_AccountDetail SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_AccountDetail ON
GO
IF EXISTS(SELECT * FROM dbo.AccountDetail)
	 EXEC('INSERT INTO dbo.Tmp_AccountDetail (Id, PlanId, DueDate, NextPaymentDate, AppStartDate, SchoolId, TotalMessageCount, LeftMessageCount, WeeklyHolidays, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, StatusId)
		SELECT Id, PlanId, DueDate, NextPaymentDate, AppStartDate, SchoolId, TotalMessageCount, LeftMessageCount, WeeklyHolidays, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, StatusId FROM dbo.AccountDetail WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_AccountDetail OFF
GO
DROP TABLE dbo.AccountDetail
GO
EXECUTE sp_rename N'dbo.Tmp_AccountDetail', N'AccountDetail', 'OBJECT' 
GO
ALTER TABLE dbo.AccountDetail ADD CONSTRAINT
	PK_AccountMaster PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
