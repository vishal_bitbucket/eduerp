USE [EduErp]
GO
/****** Object:  StoredProcedure [dbo].[GetSchools]    Script Date: 02/21/2018 18:10:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetSchools]
AS
BEGIN
	Select sch.Id,sch.Code,sch.ParentSchoolId,sch.Name,sch.CityId,sch.Address1,sch.Address2,sch.Address1+' '+sch.Address2 as [Address],sch.PIN,sch.OwnerName,sch.ContactNo,sch.CreatedOn,
	sch.CreatedBy,sch.StatusId,sch.ImagePath,psch.Name AS ParentSchoolName,cty.Name AS CityName--PM.MaxStudentLimit,PM.MaxUserLimit,PM.ValidityInMonth 
	from SchoolMaster sch left outer JOIN SchoolMaster psch ON sch.ParentSchoolId = psch.Id
	--INNER JOIN PlanMaster PM ON sch.Id=PM.SchoolId	
	INNER JOIN CityMaster cty ON sch.CityId = cty.Id
	Where sch.StatusId <> 3
END
GO
/****** Object:  StoredProcedure [dbo].[GetFeeHeads]    Script Date: 02/21/2018 18:10:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetFeeHeads]
@SchoolId int
as
begin
select  f.Id,f.Name,f.Description,f.FeeHeadTypeId,f.SchoolId,f.StatusId,k.KeyWordName as FeeHeadTypeName
       from FeeHeadMaster f 
       join KeyWordMaster k on f.FeeHeadTypeId=k.KeyWordId 
      where f.StatusId<>3 and f.SchoolId=@SchoolId and k.Name='FeeHeadType'

end
GO
/****** Object:  Table [dbo].[FineDiscountTransactionDetail]    Script Date: 02/21/2018 18:10:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FineDiscountTransactionDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FeeHeadId] [int] NOT NULL,
	[TransactionId] [int] NOT NULL,
	[FeeHeadAmount] [decimal](18, 2) NULL,
 CONSTRAINT [PK_FineDiscountTransactionDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[FineDiscountTransactionDetail] ON
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (1, 150, 53, CAST(200.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (2, 149, 53, CAST(100.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (5, 150, 58, CAST(200.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (6, 149, 58, CAST(100.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (9, 150, 60, CAST(200.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (10, 149, 60, CAST(100.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (21, 150, 59, CAST(100.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (22, 149, 59, CAST(200.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (23, 150, 58, CAST(200.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (24, 150, 58, CAST(100.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (25, 149, 58, CAST(100.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (26, 150, 58, CAST(200.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (27, 150, 58, CAST(200.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (28, 150, 58, CAST(100.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (29, 149, 58, CAST(100.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (30, 149, 58, CAST(100.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (31, 150, 58, CAST(200.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (32, 150, 58, CAST(200.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (33, 150, 58, CAST(100.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (34, 150, 58, CAST(200.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (35, 150, 58, CAST(200.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (36, 150, 58, CAST(100.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (37, 149, 58, CAST(100.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (38, 149, 58, CAST(100.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (39, 149, 58, CAST(100.00 AS Decimal(18, 2)))
INSERT [dbo].[FineDiscountTransactionDetail] ([Id], [FeeHeadId], [TransactionId], [FeeHeadAmount]) VALUES (40, 149, 58, CAST(100.00 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[FineDiscountTransactionDetail] OFF
/****** Object:  StoredProcedure [dbo].[GetFeeHeadDetailForEdit]    Script Date: 02/21/2018 18:10:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[GetFeeHeadDetial] 6,6,25,1,3,2,19  
CREATE Proc [dbo].[GetFeeHeadDetailForEdit]  
@SchoolId int,  
@SessionId int,  
@ClassId int ,  
@FromMonth int,  
@ToMonth int,  
@HeadTypeId int,  
@StudentId int  
AS  
BEGIN  
 Select   
  fhm.Id as FeeHeadMasterId,fhm.Name,fhm.Description,fhm.IsDiscount,fhm.FeeHeadTypeId,fhm.SchoolId,fhm.CreatedOn,fhm.CreatedBy,fhm.StatusId,fhm.LastModifiedOn,fhm.LastModifiedBy,  
  fhmm.Id as FeeHeadMappingId,fhmm.FeeHeadId,fhmm.ClassId,fhmm.SessionId,fhmm.MonthId,fhmm.Amount,  
  km.KeyWordName as FeeMonthName,  
  kh.KeyWordName as FeeHeadTypeName,  
  cm.Name as ClassName , sm.Name as SessionName  
 From   
  [dbo].[FeeHeadMaster] fhm   
  INNER JOIN [dbo].[FeeHeadMonthMapping] fhmm ON fhmm.FeeHeadId = fhm.Id  
  Left OUTER JOIN KeyWordMaster km ON km.KeyWordId = fhmm.MonthId  
  Left OUTER JOIN KeyWordMaster kh ON kh.KeyWordId = fhm.Id  
  Left OUTER JOIN ClassMaster cm ON cm.Id = fhmm.ClassId  
  Left OUTER JOIN SessionMaster sm ON sm.Id = fhmm.SessionId  
 Where fhm.StatusId = 1 AND km.Name='Month' AND fhm.SchoolId =@SchoolId AND fhmm.SessionId  = @SessionId AND fhmm.ClassId = @ClassId AND fhm.FeeHeadTypeId = @HeadTypeId  
 AND  fhmm.MonthId >= @FromMonth   and fhmm.MonthId <=  @ToMonth  
 END  
   
--select * from FeeTransactionDetail
--select * from FeeTransaction
GO
/****** Object:  StoredProcedure [dbo].[GetGeneralwiseFeeHeadDetial]    Script Date: 02/21/2018 18:10:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetGeneralwiseFeeHeadDetial]
@SchoolId int,
@SessionId int,
@FromMonth int,
@ToMonth int,
@HeadTypeId int,
@StudentId int = 19
AS
BEGIN
	Select 
		 fhm.Id as FeeHeadMasterId,fhm.Name,fhm.Description,fhm.IsDiscount,fhm.FeeHeadTypeId,fhm.SchoolId,fhm.CreatedOn,fhm.CreatedBy,fhm.StatusId,fhm.LastModifiedOn,fhm.LastModifiedBy,
		fhmm.Id as FeeHeadMappingId,fhmm.FeeHeadId,fhmm.ClassId,fhmm.SessionId,fhmm.MonthId,fhmm.Amount,
		km.KeyWordName as FeeMonthName,
		kh.KeyWordName as FeeHeadTypeName,
		 sm.Name as SessionName
	From 
		[dbo].[FeeHeadMaster] fhm 
		INNER JOIN [dbo].[FeeHeadMonthMapping] fhmm ON fhmm.FeeHeadId = fhm.Id
		Left OUTER JOIN KeyWordMaster km ON km.KeyWordId = fhmm.MonthId
		Left OUTER JOIN KeyWordMaster kh ON kh.KeyWordId = fhm.Id
		Left OUTER JOIN SessionMaster sm ON sm.Id = fhmm.SessionId
	Where fhm.StatusId = 1 AND km.Name='Month' AND fhm.SchoolId = @SchoolId AND fhmm.SessionId  = @SessionId AND fhmm.ClassId =0 AND fhm.FeeHeadTypeId = @HeadTypeId
	AND fhmm.MonthId BETWEEN @FromMonth and @ToMonth 
	AND  
 fhmm.Id Not In (  
	Select id from FeeHeadMonthMapping Where FeeHeadMonthMapping.Id in (  
	select fd.FeeHeadMappingId  from feetransaction f inner join FeeTransactionDetail fd ON fd.FeeTransactionId = f.Id 
	inner join FeeHeadMonthMapping fhmm1 on fd.FeeHeadMappingId = fhmm1.Id Where fhmm1.SessionId = @SessionId 
	and StudentId = @StudentId --and ((1 between f.FromMonthId and f.ToMonthId) Or  (7 between f.FromMonthId and f.ToMonthId))
	))
END
GO
/****** Object:  StoredProcedure [dbo].[GetFeeTransactionByTransactionId]    Script Date: 02/21/2018 18:10:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetFeeTransactionByTransactionId]
@FeetransactionId int
AS
BEGIN
 SELECT ft.Id,ft.PaymentModeId,ft.StudentId,std.FirstName+' '+''+std.LastName as StudentName,ft.TotalAmountDeposit,ft.IsSettled,ft.BalanceAmount,ft.Remark,ft.TransactionDate,ft.CreatedOn,ft.CreatedBy,ft.LastUpdatedBy,
 ft.LastUpdatedOn,sec.Name as SectionName,cls.Name as ClassName,sess.Name as SessionName,km.KeyWordName as FromMonthName,kms.KeyWordName as ToMonthName,ft.StatusId,ft.FromMonthId ,ft.ToMonthId,s.SessionId,s.ClassId,s.SectionId,
 ftdr.TotalAmount,kmss.KeyWordName as PaymentModeName,sm.ImagePath as SchoolImagePath ,sm.S3key
 FROM  FeeTransaction ft  Inner JOIN 
   (SELECT  ftd.FeeTransactionId , SUM(fhm.Amount) AS TotalAmount
   FROM    FeeTransactionDetail ftd 
   Left Outer JOIN FeeHeadMonthMapping fhm 
   ON  ftd.FeeHeadMappingId = fhm.Id
   GROUP BY ftd.FeeTransactionId)  ftdr 
 ON ftdr.FeeTransactionId = ft.Id
 Left Outer JOIN StudentClassMapping s on s.StudentId = ft.StudentId
 inner join Student std on std.Id =s.StudentId 
 inner join SchoolMaster sm on sm.Id=std.SchoolId
 inner join SectionMaster sec on sec.Id=s.SectionId
 inner join SessionMaster sess on sess.Id=s.SessionId
 inner join ClassMaster cls on cls.Id=s.ClassId
 inner join KeyWordMaster km on km.KeyWordId= ft.FromMonthId
 inner join KeyWordMaster kms on kms.KeyWordId=ft.ToMonthId
 inner join KeyWordMaster kmss on kmss.KeyWordId=ft.PaymentModeId
 WHERE ft.Id = @FeeTransactionId and km.Name='Month' and kms.Name='Month' and kmss.Name='PaymentMode' AND ft.StatusId<>3
 END
GO
/****** Object:  StoredProcedure [dbo].[GetFeeTransactionByClassSection]    Script Date: 02/21/2018 18:10:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetFeeTransactionByClassSection]
@SchoolId int ,
@SessionId int,
@ClassId int,
@SectionId int
AS
BEGIN
 Declare @TotalAmount numeric(18,2)

  Select ft1.Id,ft1.StudentId,ft1.TotalAmountDeposit,ft1.IsSettled,ft1.BalanceAmount,ft1.Remark,ft1.TransactionDate,ft1.CreatedOn,ft1.CreatedBy,ft1.LastUpdatedBy,
 ft1.LastUpdatedOn,ft1.StatusId,
 a.Total,s.FirstName + ' ' + s.LastName  as StudentName
 From FeeTransaction ft1
 Inner join 
 (Select ft.Id,ft.StudentId, ftdr.Total
 From  FeeTransaction ft  Inner JOIN 
 (Select  ftd.FeeTransactionId , sum(fhm.Amount) AS Total
 FROM    FeeTransactionDetail ftd 
 Left Outer JOIN FeeHeadMonthMapping fhm ON  ftd.FeeHeadMappingId = fhm.Id
 group by ftd.FeeTransactionId)  ftdr on ftdr.FeeTransactionId = ft.Id) as a ON Ft1.Id = a.Id
 Inner join 
 (Select StudentId , Max(Id)  as LastTransactionId From (
 Select ft.Id,ft.StudentId, ftdr.Total
 From  FeeTransaction ft  Inner JOIN 
 (Select  ftd.FeeTransactionId , sum(fhm.Amount) AS Total
 FROM    FeeTransactionDetail ftd 
 Left Outer JOIN FeeHeadMonthMapping fhm ON  ftd.FeeHeadMappingId = fhm.Id
 group by ftd.FeeTransactionId)  ftdr on ftdr.FeeTransactionId = ft.Id) AS Res
 Group By Res.StudentId) b
 ON b.LastTransactionId = ft1.Id
 Inner join Student s ON s.Id = ft1.StudentId
 Left Outer JOIN StudentClassMapping scm ON scm.StudentId = s.Id
 Where @SchoolId = s.SchoolId AND @SessionId = scm.SessionId AND @ClassId = scm.ClassId AND @SectionId = scm.SectionId
END
GO
/****** Object:  StoredProcedure [dbo].[GetStudentsList]    Script Date: 02/21/2018 18:10:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[GetStudentsList]
@SchoolId int
AS
Select s.Id ,s.FirstName ,s.MiddileName,s.LastName,s.TitleId,s.Email,s.StudentMobile,scm.StudentCode ,
  s.MotherName,s.FatherName,s.CorrCityId,s.GenderId,s.SchoolId,s.Uid,
  s.DOJ,s.DOB,s.DOE,s.StatusId ,s.CreatedOn,s.CreatedBy,s.LastUpdatedOn,s.LastUpdatedBy,s.ImagePath,
  s.RelegionId,s.CategoryId,s.ReasonForExit,s.NationlityId,
  kg.KeyWordName , kn.KeyWordName as NationlityName, kr.KeyWordName as  RelegionName,
  kt.KeyWordName as TitleName ,kc.KeyWordName as StudentCategory
 From Student s inner join StudentClassMapping scm on s.Id=scm.StudentId
 
 left outer join KeyWordMaster kg ON kg.KeyWordId = s.GenderId AND kg.Name = 'Gender'
 left outer join KeyWordMaster kn ON kn.KeyWordId = s.NationlityId AND kn.Name = 'Nationlity'
 left outer join KeyWordMaster kr ON kr.KeyWordId = s.RelegionId AND kr.Name = 'Relegion'
 left outer join KeyWordMaster kc ON kc.KeyWordId = s.CategoryId AND kc.Name = 'Category'
 left outer join KeyWordMaster kt ON kt.KeyWordId = s.TitleId AND kt.Name = 'Title'
Where s.SchoolId = @SchoolId  AND s.StatusId <> 3
GO
/****** Object:  StoredProcedure [dbo].[GetFeeHeadDetial]    Script Date: 02/21/2018 18:10:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[GetFeeHeadDetial] 6,6,25,1,3,2,19  
CREATE Proc [dbo].[GetFeeHeadDetial]  
@SchoolId int,  
@SessionId int,  
@ClassId int ,  
@FromMonth int,  
@ToMonth int,  
@HeadTypeId int,  
@StudentId int  
AS  
BEGIN  
 Select   
  fhm.Id as FeeHeadMasterId,fhm.Name,fhm.Description,fhm.IsDiscount,fhm.FeeHeadTypeId,fhm.SchoolId,fhm.CreatedOn,fhm.CreatedBy,fhm.StatusId,fhm.LastModifiedOn,fhm.LastModifiedBy,  
  fhmm.Id as FeeHeadMappingId,fhmm.FeeHeadId,fhmm.ClassId,fhmm.SessionId,fhmm.MonthId,fhmm.Amount,  
  km.KeyWordName as FeeMonthName,  
  kh.KeyWordName as FeeHeadTypeName,  
  cm.Name as ClassName , sm.Name as SessionName  
 From   
  [dbo].[FeeHeadMaster] fhm   
  INNER JOIN [dbo].[FeeHeadMonthMapping] fhmm ON fhmm.FeeHeadId = fhm.Id  
  Left OUTER JOIN KeyWordMaster km ON km.KeyWordId = fhmm.MonthId  
  Left OUTER JOIN KeyWordMaster kh ON kh.KeyWordId = fhm.Id  
  Left OUTER JOIN ClassMaster cm ON cm.Id = fhmm.ClassId  
  Left OUTER JOIN SessionMaster sm ON sm.Id = fhmm.SessionId  
 Where fhm.StatusId = 1 AND km.Name='Month' AND fhm.SchoolId = @SchoolId AND fhmm.SessionId  = @SessionId AND fhmm.ClassId = @ClassId AND fhm.FeeHeadTypeId = @HeadTypeId  
 AND  fhmm.MonthId >= @FromMonth   and fhmm.MonthId <=  @ToMonth  
 AND  
 fhmm.Id Not In (  
 Select id from FeeHeadMonthMapping Where FeeHeadMonthMapping.Id in (  
select fd.FeeHeadMappingId  from feetransaction f inner join FeeTransactionDetail fd ON fd.FeeTransactionId = f.Id 
inner join FeeHeadMonthMapping fhmm1 on fd.FeeHeadMappingId = fhmm1.Id Where fhmm1.SessionId = @SessionId 
and StudentId = @StudentId --and ((1 between f.FromMonthId and f.ToMonthId) Or  (7 between f.FromMonthId and f.ToMonthId))
)  )
 END  
   
--select * from FeeTransactionDetail
--select * from FeeTransaction
GO
/****** Object:  StoredProcedure [dbo].[FeeTranPrintDetail]    Script Date: 02/21/2018 18:10:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[FeeTranPrintDetail]
@FeeTranId int
as
Begin
Begin Try
--selct * from 
select fhm.id ,fhm.name ,SUM(fhmm.Amount) Amount,fhm.feeheadtypeid
--ftd.FeeHeadMappingId,fhmm.*,fhm.Name 
from FeeTransactionDetail ftd 
Inner join FeeHeadMonthMapping fhmm ON fhmm.Id = ftd.FeeHeadMappingId
Inner join FeeHeadMaster fhm ON fhmm.feeheadid = fhm.id  
--Inner join finediscounttransactiondetail fdtd on fhm.id = fdtd.feeheadid and fdtd.transactionid = 53
Where ftd.FeeTransactionId = @FeeTranId 
Group By fhm.id ,fhm.name,fhm.feeheadtypeid
Union
select fhm.id ,fhm.name ,SUM( fdtd.FeeHeadAmount) Amount,fhm.feeheadtypeid
--ftd.FeeHeadMappingId,fhmm.*,fhm.Name 
from finediscounttransactiondetail fdtd 
--Inner join FeeHeadMonthMapping fhmm ON fhmm.Id = fdtd.FeeMappingId
Inner join FeeHeadMaster fhm ON fdtd.FeeheadId = fhm.id
--Inner join finediscounttransactiondetail fdtd on fhm.id = fdtd.feeheadid and fdtd.transactionid = 53
Where fdtd.TransactionId = @FeeTranId 
Group By fhm.id ,fhm.name,fhm.feeheadtypeid

	--Select ft.[StudentId],ft.[TotalAmountDeposit],ft.[IsSettled],ft.[BalanceAmount],ft.[Remark],
	--ft.[TransactionDate],ft.[CreatedOn],ft.[CreatedBy],ft.[PaymentModeId],ft.[FromMonthId],ft.[ToMonthId],
	--ft.[LastUpdatedBy],ft.[LastUpdatedOn],ft.[StatusId],
	--ftd.FeeTransactionId , ftd.FeeHeadMappingId,
	--fhmm.MonthId
	--from FeeTransaction ft inner join feetransactiondetail ftd on ft.id = ftd.FeeTransactionId
	--Inner join finediscounttransactiondetail fdtd ON  ft.id = fdtd.TransactionId
	--Inner join FeeHeadMonthMapping fhmm on fhmm.Id = ftd.FeeHeadMappingId
	--Where ft.id = 53
End Try
Begin Catch
End Catch
End

--select * from finediscounttransactiondetail
--select * from FeeHeadMaster
--sp_help feetransactiondetail

--select * from finediscounttransactiondetail
--delete From finediscounttransactiondetail Where transactionId = 54
--delete  from feetransaction where ID = 54
GO
/****** Object:  ForeignKey [FK_FineDiscountTransactionDetail_FeeHeadMaster]    Script Date: 02/21/2018 18:10:00 ******/
ALTER TABLE [dbo].[FineDiscountTransactionDetail]  WITH CHECK ADD  CONSTRAINT [FK_FineDiscountTransactionDetail_FeeHeadMaster] FOREIGN KEY([FeeHeadId])
REFERENCES [dbo].[FeeHeadMaster] ([Id])
GO
ALTER TABLE [dbo].[FineDiscountTransactionDetail] CHECK CONSTRAINT [FK_FineDiscountTransactionDetail_FeeHeadMaster]
GO
/****** Object:  ForeignKey [FK_FineDiscountTransactionDetail_FeeTransaction]    Script Date: 02/21/2018 18:10:00 ******/
ALTER TABLE [dbo].[FineDiscountTransactionDetail]  WITH CHECK ADD  CONSTRAINT [FK_FineDiscountTransactionDetail_FeeTransaction] FOREIGN KEY([TransactionId])
REFERENCES [dbo].[FeeTransaction] ([Id])
GO
ALTER TABLE [dbo].[FineDiscountTransactionDetail] CHECK CONSTRAINT [FK_FineDiscountTransactionDetail_FeeTransaction]
GO
