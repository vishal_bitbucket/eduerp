INSERT INTO KeyWordMaster VALUES ('PayType',3,'Balanced')
INSERT INTO KeyWordMaster VALUES ('PayType',4,'Settled')
GO

UPDATE MenuMaster SET ActionName='FeesReport' where Id=34
GO

SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[GetFeeTransactionReport]    Script Date: 05/12/2018 18:36:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author: Vishal Kumar
-- Exec Param : GetFeeTransactionReport 6,6,25,8 ,1,5,1
-- Description:All Students Fee Report
-- =============================================
CREATE Proc [dbo].[GetFeeTransactionReport]
@SchoolId int ,
@SessionId int,
@ClassId int,
@SectionId int,
@FromMonthId int,
@ToMonthId int,
@PayStatusId int
AS
BEGIN
Declare @FineAmount decimal(18,2)
Declare @TotalAmount numeric(18,2)


  Select * into #temp From(
Select  Max(ft.Id) as StudentLastTranId,ft.StudentId
From  FeeTransaction ft
Where  @SessionId = ft.SessionId
Group By ft.StudentId
) as res

Select * from (

Select ft1.Id,ft1.StudentId,ISNULL( ft1.TotalAmountDeposit,0) as TotalAmountDeposit,ISNULL( ft1.PreviousBalance,0) as PreviousBalance,ISNULL( ft1.TotalPayableAmount,0) as TotalPayableAmount ,ISNULL( ft1.TotalAmount,0) as TotalAmount ,ft1.IsSettled,ISNULL(

ft1.BalanceAmount,0) as BalanceAmount ,ft1.Remark,ft1.TransactionDate,ft1.CreatedOn,ft1.CreatedBy,ft1.LastUpdatedBy,
ft1.LastUpdatedOn,ft1.StatusId,
km1.KeyWordName as FromMonth , km2.KeyWordName as ToMonth,
s.FirstName + ' ' + s.LastName  as StudentName
From FeeTransaction ft1
Inner join
#temp t ON Ft1.Id = t.StudentLastTranId
Left Outer JOIN Student s ON s.Id = ft1.StudentId
Left Outer JOIN StudentClassMapping scm ON scm.StudentId = s.Id
--Left Outer JOIN ClassMaster cm ON scm.ClassId = cm.Id
--Left Outer JOIN SectionMaster sm on sm.ClassId = cm.id
Left Outer Join  KeyWordMaster km1 ON km1.KeyWordId = ft1.FromMonthId and km1.Name = 'Month'
Left Outer Join  KeyWordMaster km2 ON km2.KeyWordId = ft1.ToMonthId and km2.Name = 'Month'
Where @SchoolId = s.SchoolId AND @SessionId = scm.SessionId AND
ISNULL(@ClassId,scm.ClassId) = scm.ClassId AND
ISNULL(@SectionId,scm.SectionId) = scm.SectionId and
((@FromMonthId is null ) OR ( @FromMonthId >= ft1.FromMonthId)) AND
((@ToMonthId is null ) OR ( @ToMonthId >= ft1.ToMonthId))

) res1

left outer join

(
Select fine.TransactionId,sum(fine.FeeHeadAmount) as Discount from FineDiscountTransactionDetail fine Inner JOIN FeeHeadMaster fhm ON fine.FeeHeadId = fhm.Id
Where  fhm.FeeHeadTypeId = 3 --fine.TransactionId=64 and
Group By fine.TransactionId,fhm.FeeHeadTypeId
) res2

ON res2.TransactionId = res1.Id
left  outer join
(
Select fine.TransactionId,sum(fine.FeeHeadAmount) as Fine from FineDiscountTransactionDetail fine Inner JOIN FeeHeadMaster fhm ON fine.FeeHeadId = fhm.Id
Where  fhm.FeeHeadTypeId = 4
Group By fine.TransactionId,fhm.FeeHeadTypeId
) res3

ON res3.TransactionId = res1.Id

END
GO
