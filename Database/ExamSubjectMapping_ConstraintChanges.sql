/*
   Sunday, January 14, 20183:17:52 PM
   User: sa
   Server: DELL-PC
   Database: EduErp
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.SubjectMaster SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ExamMaster SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ExamSubjectMapping ADD CONSTRAINT
	FK_ExamSubjectMapping_ExamMaster FOREIGN KEY
	(
	ExamId
	) REFERENCES dbo.ExamMaster
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ExamSubjectMapping ADD CONSTRAINT
	FK_ExamSubjectMapping_SubjectMaster1 FOREIGN KEY
	(
	SubjectId
	) REFERENCES dbo.SubjectMaster
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ExamSubjectMapping SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
