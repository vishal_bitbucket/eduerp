USE [EduErp]
GO
/****** Object:  StoredProcedure [dbo].[GetFeeTransactionByStudentIdForProfile]    Script Date: 03/21/2018 18:50:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetFeeTransactionByStudentIdForProfile]
@StudentId int,
@SessionId int,
@FromMonth int,
@ToMonth int
AS
BEGIN
 SELECT ft.Id,ft.StudentId,ft.TotalAmountDeposit,ft.FromMonthId,ft.ToMonthId,ft.IsSettled,ft.BalanceAmount,ft.Remark,ft.TransactionDate,ft.CreatedOn,ft.CreatedBy,ft.LastUpdatedBy,
 ft.LastUpdatedOn,ft.StatusId,
 ftdr.Total
 FROM  FeeTransaction ft  Inner JOIN 
   (SELECT  ftd.FeeTransactionId , SUM(fhm.Amount) AS Total
   FROM    FeeTransactionDetail ftd 
   Left Outer JOIN FeeHeadMonthMapping fhm 
   ON  ftd.FeeHeadMappingId = fhm.Id
   GROUP BY ftd.FeeTransactionId)  ftdr 
 ON ftdr.FeeTransactionId = ft.Id
 Left Outer JOIN StudentClassMapping s on s.StudentId = ft.StudentId
 WHERE ft.StudentId =@StudentId AND s.SessionId = @SessionId AND ft.StatusId<>3 AND ft.FromMonthId>=@FromMonth AND ft.ToMonthId<=@ToMonth
END
GO
