USE [master]
GO
/****** Object:  Database [EduErp]    Script Date: 07/01/2018 16:25:45 ******/
CREATE DATABASE [EduErp] ON  PRIMARY 
( NAME = N'EduErp', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\EduErp.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'EduErp_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\EduErp_1.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [EduErp] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [EduErp].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [EduErp] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [EduErp] SET ANSI_NULLS OFF
GO
ALTER DATABASE [EduErp] SET ANSI_PADDING OFF
GO
ALTER DATABASE [EduErp] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [EduErp] SET ARITHABORT OFF
GO
ALTER DATABASE [EduErp] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [EduErp] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [EduErp] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [EduErp] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [EduErp] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [EduErp] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [EduErp] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [EduErp] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [EduErp] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [EduErp] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [EduErp] SET  DISABLE_BROKER
GO
ALTER DATABASE [EduErp] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [EduErp] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [EduErp] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [EduErp] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [EduErp] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [EduErp] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [EduErp] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [EduErp] SET  READ_WRITE
GO
ALTER DATABASE [EduErp] SET RECOVERY SIMPLE
GO
ALTER DATABASE [EduErp] SET  MULTI_USER
GO
ALTER DATABASE [EduErp] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [EduErp] SET DB_CHAINING OFF
GO
USE [EduErp]
GO
/****** Object:  Table [dbo].[VendorMaster]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VendorMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](250) NOT NULL,
	[CityId] [int] NOT NULL,
	[StateId] [int] NOT NULL,
	[TypeId] [int] NULL,
	[SchoolId] [int] NULL,
	[ContactNumber] [varchar](10) NOT NULL,
	[Address] [varchar](500) NOT NULL,
	[PinCode] [varchar](6) NOT NULL,
	[StatusId] [int] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedBy] [int] NULL,
	[LastUpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_VendorMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserRight]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRight](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[RightId] [int] NOT NULL,
	[MenuId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
 CONSTRAINT [PK_UserRight] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserMaster]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[UserTypeId] [int] NULL,
	[UserId] [varchar](50) NOT NULL,
	[Password] [nvarchar](max) NOT NULL,
	[PasswordSalt] [uniqueidentifier] NOT NULL,
	[Email] [varchar](100) NOT NULL,
	[Mobile] [varchar](10) NOT NULL,
	[SchoolId] [int] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NULL,
	[StatusId] [int] NOT NULL,
	[LastModifiedOn] [datetime] NULL,
	[LatModifiedBy] [int] NULL,
	[OneTimePassword] [varchar](6) NULL,
	[VerificationCode] [uniqueidentifier] NULL,
	[VerificationCodeGeneratedOnUtc] [datetime] NULL,
	[IsPasswordCreated] [bit] NULL,
 CONSTRAINT [PK_UserMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserLoginHistory]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserLoginHistory](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[LoginDate] [datetime] NOT NULL,
	[IpAddress] [nvarchar](50) NULL,
 CONSTRAINT [PK_UserLoginHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TemplateMaster]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TemplateMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Text] [varchar](500) NOT NULL,
 CONSTRAINT [PK_TemplateMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StudentShadow]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StudentShadow](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NULL,
	[MiddleName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[SessionId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[TitleId] [int] NULL,
	[Email] [varchar](50) NULL,
	[StudentMobile] [varchar](10) NULL,
	[StudentOtherInfo] [varchar](500) NULL,
	[GuardianAddress] [varchar](500) NULL,
	[GuardianOccupation] [varchar](100) NULL,
	[GuardianRelationWithStudent] [varchar](100) NULL,
	[GuardianMobileNo] [varchar](10) NULL,
	[GuardianName] [varchar](100) NULL,
	[MessageMobileNumber] [varchar](10) NULL,
	[MotherName] [varchar](100) NULL,
	[FatherName] [varchar](100) NULL,
	[CorrAddress] [varchar](500) NULL,
	[CorrCityId] [int] NULL,
	[CorrPinCode] [varchar](6) NULL,
	[PermanentAddress] [varchar](500) NULL,
	[PermanentCityId] [int] NULL,
	[CurrentStateId] [int] NULL,
	[PermanentStateId] [int] NULL,
	[ParentMobileNo] [varchar](10) NULL,
	[ParentContactNo] [varchar](10) NULL,
	[PermanentPinCode] [varchar](6) NULL,
	[SchoolId] [int] NULL,
	[DOJ] [datetime] NULL,
	[DOB] [datetime] NULL,
	[DOE] [datetime] NULL,
	[Uid] [varchar](12) NULL,
	[StatusId] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[LastUpdatedOn] [datetime] NULL,
	[LastUpdatedBy] [int] NULL,
	[ImagePath] [varchar](200) NULL,
	[RelegionId] [int] NULL,
	[CategoryId] [int] NULL,
	[ReasonForExit] [varchar](500) NULL,
	[NationlityId] [int] NULL,
	[GenderId] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TeacherMaster]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TeacherMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NULL,
	[MiddleName] [varchar](50) NULL,
	[EmployeeTypeId] [int] NULL,
	[EmployeeCode] [varchar](50) NULL,
	[TitleId] [int] NULL,
	[Email] [varchar](100) NULL,
	[MobileNumber] [varchar](10) NULL,
	[MotherName] [varchar](100) NULL,
	[FatherName] [varchar](100) NULL,
	[CurrentAddress] [varchar](500) NULL,
	[PermanentAddress] [varchar](500) NULL,
	[CurrentCityId] [int] NULL,
	[PermanentCityId] [int] NULL,
	[CurrentStateId] [int] NULL,
	[PermanentStateId] [int] NULL,
	[UniversityName] [varchar](250) NULL,
	[Graduation] [varchar](50) NULL,
	[GraduationPassingYear] [datetime] NULL,
	[GraduationMarks] [decimal](18, 2) NULL,
	[HighSchoolBoardName] [varchar](250) NULL,
	[HighschoolPassingYear] [datetime] NULL,
	[HighSchoolMediumId] [int] NULL,
	[HighSchoolMarks] [decimal](18, 2) NULL,
	[InterSchoolBoardName] [varchar](250) NULL,
	[InterPassingYear] [datetime] NULL,
	[InterSchoolMediumId] [int] NULL,
	[InterSchoolMarks] [decimal](18, 2) NULL,
	[CurrentPinCode] [varchar](6) NULL,
	[PermanentPinCode] [varchar](6) NULL,
	[SchoolId] [int] NOT NULL,
	[DOJ] [datetime] NULL,
	[DateOfBirth] [datetime] NULL,
	[DateOfExit] [datetime] NULL,
	[UID] [varchar](12) NULL,
	[PanNumber] [varchar](10) NULL,
	[StatusId] [int] NULL,
	[ImagePath] [varchar](200) NULL,
	[RelegionId] [int] NULL,
	[CategoryId] [int] NULL,
	[NationalityId] [int] NULL,
	[GenderId] [int] NULL,
	[ReasonForExit] [varchar](500) NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedBy] [int] NULL,
	[LastUpdatedOn] [datetime] NULL,
	[S3key] [uniqueidentifier] NULL,
 CONSTRAINT [PK_TeacherMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Student]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Student](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[MiddileName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[TitleId] [int] NULL,
	[RegistrationNumber] [int] NULL,
	[Email] [varchar](100) NULL,
	[StudentMobile] [varchar](10) NULL,
	[StudentOtherInfo] [varchar](500) NULL,
	[GuardianAddress] [varchar](500) NULL,
	[GuardianOccupation] [varchar](100) NULL,
	[GuardianRelationWithStudent] [varchar](100) NULL,
	[GuardianMobileNo] [varchar](10) NULL,
	[GuardianName] [varchar](100) NULL,
	[MessageMobileNumber] [varchar](10) NULL,
	[MotherName] [varchar](100) NULL,
	[FatherName] [varchar](100) NULL,
	[CorrAddress] [varchar](500) NULL,
	[CorrCityId] [int] NULL,
	[CorrPinCode] [varchar](6) NULL,
	[PermanentAddress] [varchar](500) NULL,
	[PermanentCityId] [int] NULL,
	[CurrentStateId] [int] NULL,
	[PermanentStateId] [int] NULL,
	[ParentMobileNo] [varchar](10) NULL,
	[ParentContactNo] [varchar](20) NULL,
	[PermanentPinCode] [varchar](6) NULL,
	[SchoolId] [int] NOT NULL,
	[DOJ] [datetime] NULL,
	[DOB] [datetime] NULL,
	[DOE] [datetime] NULL,
	[Uid] [varchar](12) NULL,
	[StatusId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedOn] [datetime] NULL,
	[LastUpdatedBy] [int] NULL,
	[ImagePath] [varchar](200) NULL,
	[RelegionId] [int] NULL,
	[CategoryId] [int] NULL,
	[ReasonForExit] [varchar](500) NULL,
	[NationlityId] [int] NULL,
	[GenderId] [int] NULL,
	[S3key] [uniqueidentifier] NULL,
 CONSTRAINT [PK_Student] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StateMaster]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StateMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_StateMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SessionMaster]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[SessionMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[FromDate] [datetime] NOT NULL,
	[ToDate] [datetime] NOT NULL,
	[SchoolId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[StatusId] [int] NOT NULL,
	[LastModifiedOn] [datetime] NULL,
	[LastModifiedBy] [int] NULL,
 CONSTRAINT [PK_SessionMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SchoolMaster]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SchoolMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NOT NULL,
	[ParentSchoolId] [int] NULL,
	[Name] [varchar](200) NOT NULL,
	[StateId] [int] NULL,
	[CityId] [int] NOT NULL,
	[InstituteTypeId] [int] NULL,
	[Address1] [varchar](250) NOT NULL,
	[Address2] [varchar](250) NULL,
	[PIN] [varchar](6) NOT NULL,
	[OwnerName] [varchar](150) NOT NULL,
	[ContactNo] [varchar](50) NOT NULL,
	[LastUpdatedOn] [datetime] NULL,
	[LastUpdatedBy] [int] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[StatusId] [int] NOT NULL,
	[ImagePath] [varchar](200) NULL,
	[S3key] [uniqueidentifier] NULL,
 CONSTRAINT [PK_SchoolMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ApplicationSetting]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ApplicationSetting](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](150) NOT NULL,
	[Value] [text] NOT NULL,
 CONSTRAINT [PK_ApplicationSetting] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PlanMaster]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlanMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Detail] [nvarchar](200) NULL,
	[PlanTypeId] [int] NULL,
	[MaxStudentLimit] [int] NULL,
	[MaxUserLimit] [int] NULL,
	[MaxMessageLimit] [int] NULL,
	[StatusId] [int] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_PlanMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OTQuestionAnswerMapping]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OTQuestionAnswerMapping](
	[Id] [int] NOT NULL,
	[QuestionId] [int] NOT NULL,
	[Answer] [nvarchar](50) NOT NULL,
	[IsCorrectAnswer] [bit] NULL,
	[SchoolId] [int] NOT NULL,
	[StatusId] [int] NOT NULL,
	[LastUpdatedOn] [datetime] NOT NULL,
	[LastUpdatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
 CONSTRAINT [PK_OTQuestionAnswerMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OTExamSubjectMapping]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OTExamSubjectMapping](
	[Id] [int] NOT NULL,
	[ExamId] [int] NOT NULL,
	[SubjectId] [int] NOT NULL,
	[Sequence] [int] NOT NULL,
	[TimeInMinutes] [int] NOT NULL,
	[SchoolId] [int] NOT NULL,
	[StatusId] [int] NOT NULL,
	[LastUpdatedOn] [datetime] NOT NULL,
	[LastUpdatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
 CONSTRAINT [PK_OTExamSubjectMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OTExamQuestionMapping]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OTExamQuestionMapping](
	[Id] [int] NOT NULL,
	[ExamId] [int] NOT NULL,
	[SubjectId] [int] NOT NULL,
	[QuestionId] [int] NOT NULL,
	[SchoolId] [int] NOT NULL,
	[StatusId] [int] NOT NULL,
	[LastUpdatedOn] [datetime] NOT NULL,
	[LastUpdatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
 CONSTRAINT [PK_OTExamQuestionMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OnlineTestSubjectMaster]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OnlineTestSubjectMaster](
	[Id] [int] NOT NULL,
	[Name] [varchar](250) NOT NULL,
	[SchoolId] [int] NULL,
	[StatusId] [int] NULL,
	[LastUpdatedBy] [int] NULL,
	[LastUpdatedOn] [datetime] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_OnlineTestSubjectMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OnlineTestQuestionMaster]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OnlineTestQuestionMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](250) NOT NULL,
	[Questions] [varchar](150) NULL,
	[SubjectId] [int] NULL,
	[Description] [varchar](500) NULL,
	[StatusId] [int] NULL,
	[LastUpdatedOn] [datetime] NULL,
	[LastUpdatedBy] [int] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[SchoolId] [int] NULL,
	[QuestionTypeId] [int] NULL,
	[Ismcq] [bit] NULL,
	[ImagePath] [varchar](50) NULL,
	[ChoiceTypeId] [int] NULL,
	[ChoiceType] [varchar](50) NULL,
 CONSTRAINT [PK_OnlineTestQuestionMaster_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OnlineTestMaster]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OnlineTestMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](250) NOT NULL,
	[SchoolId] [int] NOT NULL,
	[StatusId] [int] NULL,
	[LastUpdatedBy] [int] NULL,
	[LastUpdatedOn] [datetime] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[TimeInMinutes] [int] NULL,
	[StartDate] [datetime] NULL,
	[StartTime] [char](4) NULL,
 CONSTRAINT [PK_OnlineTestMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NoticeMaster]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NoticeMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NoticeTypeId] [int] NULL,
	[SchoolId] [int] NULL,
	[Subject] [varchar](50) NULL,
	[Description] [varchar](350) NULL,
	[StatusId] [int] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_NoticeMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MessageMaster]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MessageMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SchoolId] [int] NULL,
	[MessageId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[StudentId] [int] NULL,
	[Subject] [varchar](100) NULL,
	[MessageSentDate] [date] NULL,
	[MessageSentTime] [time](7) NULL,
	[Message] [varchar](300) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [date] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [date] NULL,
	[IsMessageSent] [bit] NULL,
	[StatusId] [int] NULL,
 CONSTRAINT [PK_MessageMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AccountFeeDetail]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountFeeDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SchoolId] [int] NOT NULL,
	[PaymentDate] [datetime] NULL,
	[PaymentAmount] [numeric](18, 2) NULL,
	[PaymentModeId] [int] NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
	[StatusId] [int] NULL,
	[PlanId] [int] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[UpadatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_AccountFeeDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccountDetail]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PlanId] [int] NULL,
	[DueDate] [datetime] NULL,
	[NextPaymentDate] [datetime] NULL,
	[AppStartDate] [datetime] NULL,
	[SchoolId] [int] NOT NULL,
	[LeftUserCount] [int] NULL,
	[LeftStudentCount] [int] NULL,
	[TotalMessageCount] [int] NULL,
	[LeftMessageCount] [int] NULL,
	[WeeklyHolidays] [nvarchar](8) NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[StatusId] [int] NULL,
 CONSTRAINT [PK_AccountMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CertificateTemplateMaster]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CertificateTemplateMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TemplateName] [nvarchar](50) NULL,
	[TemplateTypeId] [int] NULL,
	[HtmlTemplate] [nvarchar](max) NULL,
 CONSTRAINT [PK_CertificateTemplateMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AutomateMessage]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AutomateMessage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NULL,
	[Name] [varchar](150) NULL,
	[Description] [varchar](500) NULL,
 CONSTRAINT [PK_AutomateMessage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CityMaster]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CityMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[StateId] [int] NOT NULL,
 CONSTRAINT [PK_CityMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ExamMaster]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExamMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](150) NOT NULL,
	[SchoolId] [int] NOT NULL,
	[SessionId] [int] NULL,
	[StatusId] [int] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedBy] [int] NULL,
	[LastUpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_ExamMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmployeeTypeMaster]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmployeeTypeMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeName] [varchar](50) NOT NULL,
	[EmployeeGrade] [varchar](50) NULL,
	[SchoolId] [int] NOT NULL,
	[StatusId] [int] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedBy] [int] NULL,
	[LastUpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_EmployeeTypeMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmailSend]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailSend](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[MailType] [nchar](100) NOT NULL,
	[Email] [nvarchar](max) NOT NULL,
	[SentBy] [bigint] NULL,
	[SentOn] [datetime] NULL,
	[IsSend] [bit] NOT NULL,
	[Exception] [nvarchar](max) NULL,
 CONSTRAINT [PK_EmailSend] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MenuMaster]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MenuMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[ParentId] [int] NULL,
	[StatusId] [int] NOT NULL,
	[ActionName] [varchar](50) NOT NULL,
	[Controller] [varchar](50) NOT NULL,
	[IconClass] [varchar](50) NULL,
	[ClassName] [varchar](50) NULL,
	[IsSuperAdmin] [bit] NOT NULL,
	[MenuOrder] [int] NOT NULL,
	[IsAjax] [bit] NULL,
 CONSTRAINT [PK_MenuMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LogSource]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogSource](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[Description] [nvarchar](250) NOT NULL,
 CONSTRAINT [PK_LogSourceType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KeyWordMaster]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[KeyWordMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[KeyWordId] [int] NOT NULL,
	[KeyWordName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_KeyWordMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ItemMaster]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ItemMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](250) NOT NULL,
	[ExpensesTypeId] [int] NULL,
	[StatusId] [int] NULL,
	[SchoolId] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastUpdatedBy] [int] NULL,
	[LastUpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_ItemMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Inventory]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Inventory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SchoolId] [int] NOT NULL,
	[ItemId] [int] NULL,
	[ItemDescription] [varchar](250) NULL,
	[Amount] [numeric](6, 2) NOT NULL,
	[VenderId] [int] NULL,
	[InvetoryTypeId] [int] NOT NULL,
	[PaymentModeTypeId] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[SessionId] [int] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedBy] [int] NULL,
	[LastUpdatedOn] [date] NULL,
	[StatusId] [int] NULL,
 CONSTRAINT [PK_Inventory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HolidayMaster]    Script Date: 07/01/2018 16:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HolidayMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Description] [varchar](250) NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
	[SessionId] [int] NOT NULL,
	[SchoolId] [int] NOT NULL,
	[LastUpdatedBy] [int] NULL,
	[LastUpdatedOn] [datetime] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[StatusId] [int] NULL,
 CONSTRAINT [PK_HolidayMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[GetAccountFeeDetailBySchoolId]    Script Date: 07/01/2018 16:25:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[GetAccountFeeDetailBySchoolId]
@SchoolId int
As
Begin
Select
 ac.Id,ac.SchoolId,ac.StatusId,ac.PaymentDate,ac.PaymentAmount,ac.PaymentModeId,ac.PlanId,ac.FromDate,ac.ToDate,
km.KeyWordName as PaymentMode,km.Name as PaymentPlan,sm.Name as SchoolName

From  AccountFeeDetail ac 
         inner join PlanMaster pm on ac.PlanId=pm.Id
         inner join KeyWordMaster km on km.KeyWordId=ac.PaymentModeId
         inner join SchoolMaster sm on sm.Id=ac.SchoolId  
      where ac.SchoolId=@SchoolId AND km.Name='PaymentMode' AND ac.StatusId<>3
End
GO
/****** Object:  StoredProcedure [dbo].[GetAccountFeeDetail]    Script Date: 07/01/2018 16:25:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetAccountFeeDetail]

As
Begin
Select
 ac.Id,ac.SchoolId,ac.StatusId,ac.PaymentDate,ac.PaymentAmount,ac.PaymentModeId,ac.PlanId,ac.FromDate,ac.ToDate,
km.KeyWordName as PaymentMode,km.Name as PaymentPlan,sm.Name as SchoolName

From  AccountFeeDetail ac 
         inner join PlanMaster pm on ac.PlanId=pm.Id
         inner join KeyWordMaster km on km.KeyWordId=ac.PaymentModeId
         inner join SchoolMaster sm on sm.Id=ac.SchoolId  
      where km.Name='PaymentMode' AND ac.StatusId<>3
End
GO
/****** Object:  StoredProcedure [dbo].[GetAccountDetail]    Script Date: 07/01/2018 16:25:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetAccountDetail]

As
Begin
Select
 ac.Id,ac.SchoolId,ac.StatusId,ac.LeftStudentCount,ac.LeftUserCount,ac.LeftMessageCount,ac.PlanId,ac.DueDate,ac.NextPaymentDate
,s.Name as SchoolName

From  AccountDetail ac
               INNER JOIN SchoolMaster s on s.Id=ac.SchoolId
      where  ac.StatusId<>3
End
GO
/****** Object:  StoredProcedure [dbo].[GetAccountById]    Script Date: 07/01/2018 16:25:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetAccountById]
@Id int
As
Begin
Select
 ac.Id,ac.SchoolId,ac.StatusId,ac.PaymentDate,ac.PaymentAmount,ac.PaymentModeId,ac.PlanId,ac.NextPaymentDate,
 ac.LeftMessageCount,ac.TotalMessageCount,ac.FromDate,ac.DueDate,
k.KeyWordName as PaymentMode,km.KeyWordName as PaymentPlan,s.Name as SchoolName

From  AccountMaster ac
      INNER JOIN KeyWordMaster k on ac.PaymentModeId =k.KeyWordId
      LEFT OUTER JOIN KeyWordMaster km on ac.PlanId =km.KeyWordId
      LEFT OUTER JOIN SchoolMaster s on s.Id=ac.SchoolId
      where ac.Id=@Id And k.Name='PaymentMode' and km.Name='Plan'
End
GO
/****** Object:  Table [dbo].[ClassMaster]    Script Date: 07/01/2018 16:25:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClassMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[SchoolId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedOn] [datetime] NULL,
	[LastUpdatedBy] [int] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_ClassMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CertificateTemplateSchoolMapping]    Script Date: 07/01/2018 16:25:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CertificateTemplateSchoolMapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CertificateTemplateId] [int] NULL,
	[SchoolId] [int] NOT NULL,
	[StatusId] [int] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedBy] [int] NULL,
	[LastUpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_CertificateTemplateSchoolMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AttendanceMaster]    Script Date: 07/01/2018 16:25:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AttendanceMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[SchoolId] [int] NOT NULL,
	[StudentId] [int] NOT NULL,
	[SessionId] [int] NOT NULL,
	[DayStatusId] [int] NOT NULL,
	[Date] [date] NOT NULL,
	[Remark] [nvarchar](150) NULL,
	[LastModifiedOn] [datetime] NULL,
	[LastModifiedBy] [int] NULL,
 CONSTRAINT [PK_AttendanceMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FeeTransaction]    Script Date: 07/01/2018 16:25:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FeeTransaction](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StudentId] [int] NOT NULL,
	[SessionId] [int] NULL,
	[TotalAmountDeposit] [numeric](18, 2) NULL,
	[TotalPayableAmount] [numeric](18, 2) NULL,
	[PreviousBalance] [numeric](18, 2) NULL,
	[TotalAmount] [numeric](18, 2) NULL,
	[IsSettled] [bit] NULL,
	[BalanceAmount] [numeric](18, 2) NULL,
	[Remark] [varchar](100) NULL,
	[TransactionDate] [datetime] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[PaymentModeId] [int] NULL,
	[FromMonthId] [int] NULL,
	[ToMonthId] [int] NULL,
	[LastUpdatedBy] [int] NULL,
	[LastUpdatedOn] [datetime] NULL,
	[StatusId] [int] NULL,
 CONSTRAINT [PK_FeeTransaction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FeeHeadMaster]    Script Date: 07/01/2018 16:25:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FeeHeadMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Description] [varchar](500) NULL,
	[IsDiscount] [bit] NOT NULL,
	[FeeHeadTypeId] [int] NOT NULL,
	[SchoolId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[StatusId] [int] NOT NULL,
	[LastModifiedOn] [datetime] NULL,
	[LastModifiedBy] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MessageAutomateMapping]    Script Date: 07/01/2018 16:25:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MessageAutomateMapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MessageAutomateId] [int] NOT NULL,
	[TemplateId] [int] NOT NULL,
	[SchoolId] [int] NULL,
 CONSTRAINT [PK_MessageAutomateMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicationLog]    Script Date: 07/01/2018 16:25:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ApplicationLog](
	[ID] [uniqueidentifier] NOT NULL,
	[UserID] [bigint] NULL,
	[IpAddress] [nvarchar](50) NULL,
	[LogSourceID] [int] NOT NULL,
	[SchoolId] [int] NULL,
	[OSInfo] [text] NULL,
	[RequestUrl] [text] NULL,
	[Module] [varchar](250) NOT NULL,
	[ClassName] [varchar](250) NOT NULL,
	[MethodName] [varchar](250) NOT NULL,
	[ExceptionMessage] [text] NOT NULL,
	[StackTrace] [text] NOT NULL,
	[InnerExceptionMessage] [text] NOT NULL,
	[InnerExceptionStackTrace] [text] NOT NULL,
	[LoggedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ApplicationLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AmazonSettings]    Script Date: 07/01/2018 16:25:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AmazonSettings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BucketName] [nvarchar](50) NOT NULL,
	[AccessKey] [nvarchar](50) NOT NULL,
	[SecretAccessKey] [nvarchar](50) NOT NULL,
	[EncryptionKey] [nvarchar](50) NOT NULL,
	[ModifiedById] [int] NULL,
	[DateModified] [datetime] NULL,
 CONSTRAINT [PK_AmazonSettings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[GetMessages]    Script Date: 07/01/2018 16:25:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetMessages]
@SchoolId int
AS
Begin
Select
Id ,
MessageId 
,Subject 
,MessageSentDate
,MessageSentTime
,Message 
,CreatedBy 
,CreatedOn
,StatusId 
From MessageMaster where SchoolId =@SchoolId and StatusId<>3
Group By MessageId,id,Subject,MessageSentDate,MessageSentTime,Message,CreatedBy,CreatedOn,StatusId
End
GO
/****** Object:  StoredProcedure [dbo].[GetInvoiceData]    Script Date: 07/01/2018 16:25:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetInvoiceData]
@SchoolId int
AS BEGIN
Select(
Select Count(*)  from Student Where ( SchoolId=@SchoolId or SchoolId=0) and StatusId=1 AND MONTH(CreatedOn)=MONTH(GETDATE()) ) as TotalStudent,
(Select Count(*)  from MessageMaster Where ( SchoolId=@SchoolId or SchoolId=0) and StatusId=1 AND MONTH(CreatedOn)=MONTH(GETDATE()) ) as TotalMessage,
(Select Count(*)  from UserMaster Where  (SchoolId=@SchoolId or SchoolId=0) and StatusId=1 AND MONTH(CreatedOn)=MONTH(GETDATE()) ) as TotalUser
FROM SchoolMaster S 
 WHERE S.Id=@SchoolId AND S.StateId<>3 
END
GO
/****** Object:  StoredProcedure [dbo].[GetTeacherByID]    Script Date: 07/01/2018 16:25:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetTeacherByID]
@SchoolId int
as
begin
select t.Id,t.EmployeeCode, t.PermanentStateId,t.EmployeeTypeId, t.CurrentStateId, t.MotherName,t.DateOfExit ,t.CurrentPinCode,t.ImagePath,t.InterSchoolMarks,
        t.UniversityName,t.FatherName, t.PermanentPinCode,t.PermanentAddress,t.CurrentAddress,t.StatusId,t.ReasonForExit,t.SchoolId,t.InterSchoolMediumId,
 t.FirstName+' '+ t.LastName as Name,t.LastName ,t.FirstName,t.MiddleName,  t.MobileNumber, t.Email,t.CurrentCityId,t.Graduation,t.UID,t.GraduationMarks,t.PanNumber,
   t.GenderId, t.DateOfBirth, t.TitleId,t.RelegionId, t.CategoryId,t.PermanentCityId,t.GraduationPassingYear,t.HighSchoolBoardName,
  t.NationalityId,  t.DOJ,t.HighSchoolMarks,t.HighSchoolMediumId,t.HighschoolPassingYear,t.InterPassingYear,t.InterSchoolBoardName
  from TeacherMaster t where t.SchoolId=@SchoolId and t.StatusId<>3
end
GO
/****** Object:  StoredProcedure [dbo].[GetTeacher]    Script Date: 07/01/2018 16:25:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetTeacher]
@SchoolId int,
@Id int
as
begin
select t.Id,t.EmployeeCode, t.PermanentStateId,t.S3key,t.EmployeeTypeId, t.CurrentStateId, t.MotherName,t.DateOfExit   ,t.CurrentPinCode,t.ImagePath,t.InterSchoolMarks,
        t.UniversityName,t.FatherName, t.PermanentPinCode,t.PermanentAddress,t.CurrentAddress,t.StatusId,t.ReasonForExit,t.SchoolId,t.InterSchoolMediumId,
 t.FirstName,t.MiddleName,t.LastName  ,  t.MobileNumber, t.Email,t.CurrentCityId,t.Graduation,t.UID,t.GraduationMarks,t.PanNumber,
   t.GenderId, t.DateOfBirth  , t.TitleId,t.RelegionId, t.CategoryId,t.PermanentCityId,t.GraduationPassingYear,t.HighSchoolBoardName,
  t.NationalityId,  t.DOJ  ,t.HighSchoolMarks,t.HighSchoolMediumId,t.HighschoolPassingYear,t.InterPassingYear,t.InterSchoolBoardName
  from TeacherMaster t 
  where t.SchoolId=@SchoolId and t.Id=@Id and t.StatusId<>3
end
GO
/****** Object:  Table [dbo].[SchoolPlanMapping]    Script Date: 07/01/2018 16:25:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolPlanMapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SchoolId] [int] NOT NULL,
	[PlanId] [int] NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[StatusId] [int] NULL,
	[LastUpdateBy] [int] NULL,
	[LastUpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_SchoolPlanMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[GetStudentChartGenderwise]    Script Date: 07/01/2018 16:25:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[GetStudentChartGenderwise] 
@SchoolId INT
AS BEGIN
Select GenderId,KeyWordMaster.KeyWordName AS GenderName ,Count(student.Id) as CountNo from Student  LEFT OUTER JOIN KeyWordMaster on Student.GenderId=KeyWordMaster.KeyWordId
Where SchoolId = @SchoolId AND KeyWordMaster.Name='Gender' AND GenderId is not null
Group by GenderId,KeyWordMaster.KeyWordName
END
GO
/****** Object:  StoredProcedure [dbo].[GetStudentChartCategorywise]    Script Date: 07/01/2018 16:25:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author: Vishal Kumar
-- Exec Param : [GetStudentChartCategorywise] '6'
-- Description: chart in report module for student category wise
-- =============================================
CREATE PROCEDURE [dbo].[GetStudentChartCategorywise] 
@SchoolId INT
AS BEGIN
Select CategoryId,KeyWordMaster.KeyWordName AS CategoryName ,Count(student.Id) as CountNo from Student  LEFT OUTER JOIN KeyWordMaster on Student.CategoryId=KeyWordMaster.KeyWordId
Where SchoolId = @SchoolId AND KeyWordMaster.Name='Category' AND CategoryId is not null and StatusId = 1
Group by CategoryId,KeyWordMaster.KeyWordName
END
GO
/****** Object:  StoredProcedure [dbo].[GetSchools]    Script Date: 07/01/2018 16:25:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetSchools]
AS
BEGIN
	Select sch.Id,sch.Code,sch.ParentSchoolId,sch.Name,sch.CityId,sch.Address1,sch.Address2,sch.Address1+' '+sch.Address2 as [Address],sch.PIN,sch.OwnerName,sch.ContactNo,sch.CreatedOn,
	sch.CreatedBy,sch.StatusId,sch.ImagePath,psch.Name AS ParentSchoolName,cty.Name AS CityName--PM.MaxStudentLimit,PM.MaxUserLimit,PM.ValidityInMonth 
	from SchoolMaster sch left outer JOIN SchoolMaster psch ON sch.ParentSchoolId = psch.Id
	--INNER JOIN PlanMaster PM ON sch.Id=PM.SchoolId	
	INNER JOIN CityMaster cty ON sch.CityId = cty.Id
	Where sch.StatusId <> 3
END
GO
/****** Object:  Table [dbo].[SubjectMaster]    Script Date: 07/01/2018 16:25:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubjectMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[SchoolId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[Status] [int] NULL,
	[LastUpdatedOn] [datetime] NULL,
	[LastUpdatedBy] [int] NULL,
 CONSTRAINT [PK_SubjectMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[StudentAttendanceReportDateWise]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Vishal Kumar
-- Exec Param : StudentAttendanceReportDateWise '6','6','2017-11-14','2017-11-21','25','8'
-- Description: For the report module of attendance
-- =============================================

create Proc [dbo].[StudentAttendanceReportDateWise]
@SchoolId varchar (10),
@SessionId varchar (10),
@FromDate  date,
@ToDate date,
@ClassId varchar (10),
@SectionId varchar (10)
AS
Begin

-- Getting all distinct dates into a temporary table #Dates
SELECT DISTINCT am.[Date]  INTO #Dates
FROM AttendanceMaster am
Where am.[Date] between @FromDate and @ToDate
AND  am.SchoolId = @SchoolId and am.SessionId = @SessionId
ORDER BY am.[Date]


-- The number of days will be dynamic. So building
-- a comma seperated value string from the dates in #Dates
DECLARE @cols NVARCHAR(4000)
SELECT  @cols = COALESCE(@cols + ',[' + CONVERT(varchar, [Date], 106)
+ ']','[' + CONVERT(varchar, [Date], 106) + ']')
FROM    #Dates
ORDER BY [Date]

print @cols

-- Building the query with dynamic dates
IF OBJECT_ID('tempdb..##temp' , 'U') IS NOT NULL
   drop TABLE ##temp

DECLARE @qry NVARCHAR(max)
SET @qry =

'SELECT * into ##temp from
(SELECT a.StudentId, ISNULL(s.FirstName,'''') + ISNULL(s.MiddileName,'''') + ISNULL(s.LastName,'''') as StudentName, a.DayStatusId , a.[Date]
FROM AttendanceMaster a
Left Outer Join Student s on s.Id = a.StudentId and s.SchoolId = '+@SchoolId+'
Left Outer Join StudentClassMapping scm on scm.StudentId = s.Id and scm.ClassId = '+@ClassId+' and scm.SectionId = '+@SectionId+'
Where a.SchoolId = '+@SchoolId+' and a.SessionId = '+@SessionId+' and scm.ClassId = '+@ClassId+' and scm.SectionId = '+@SectionId+')  emp
PIVOT (MAX(DayStatusId) FOR [Date] IN (' + @cols + ')) AS stat'
--select * from Student
print @qry

-- Executing the query
EXEC(@qry)

EXEC('Select * from ##temp')


end
GO
/****** Object:  Table [dbo].[SubjectClassMapping]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubjectClassMapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SubjectId] [int] NOT NULL,
	[ClassId] [int] NOT NULL,
	[StatusId] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastUpdatedBy] [int] NULL,
	[LastUpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_SubjectClassMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SectionMaster]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SectionMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[ClassId] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[Status] [int] NOT NULL,
	[LastUpdatedBy] [int] NULL,
	[LastUpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_SectionMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[GetSchoolBySchoolId]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetSchoolBySchoolId]
@SchoolId int
AS 
BEGIN
SELECT
        s.Id, s.Code, s.ParentSchoolId, s.ContactNo, s.Name, s.Address1, s.Address2, s.CityId, s.PIN, s.InstituteTypeId,
         s.CreatedOn, s.OwnerName, c.Name as CityName, s.ImagePath, st.Name as StateName, s.StateId, s.StatusId,
         s.S3key, spm.StartDate, spm.EndDate, pm.PlanTypeId
        FROM SchoolMaster s INNER JOIN SchoolPlanMapping spm on s.Id=spm.SchoolId 
        INNER JOIN PlanMaster pm on spm.PlanId=pm.Id 
        INNER JOIN CityMaster c on s.CityId=c.Id
        INNER JOIN StateMaster st on c.StateId=st.Id
        where s.Id=6 
END
GO
/****** Object:  StoredProcedure [dbo].[GetPreviousBalance]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetPreviousBalance]
@StudentId int
AS
BEGIN
       SELECT TOP 1 BalanceAmount FROM (
  select TOP 2 * from FeeTransaction where StudentId=@StudentId order By FeeTransaction.Id desc)  T  where StudentId=@StudentId ORDER BY Id asc;
END
GO
/****** Object:  StoredProcedure [dbo].[GetTotalPaidUnpaid]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetTotalPaidUnpaid]  
@SchoolId INT  
AS BEGIN  
SELECT(  
SELECT COUNT(FeeTransaction.StudentId)  FROM   
 FeeTransaction LEFT OUTER JOIN  Student ON Student.Id = FeeTransaction.StudentId  
    WHERE Student.SchoolId=@SchoolId AND FeeTransaction.IsSettled=0 AND FeeTransaction.BalanceAmount=0 ) AS TotalPaid,  
    (SELECT COUNT(FeeTransaction.StudentId)  FROM   
 FeeTransaction LEFT OUTER JOIN  Student ON Student.Id = FeeTransaction.StudentId  
    WHERE Student.SchoolId=@SchoolId AND FeeTransaction.IsSettled=0 AND FeeTransaction.BalanceAmount=0 ) AS TotalUnpaid  
END
GO
/****** Object:  StoredProcedure [dbo].[GetTotalFeeAndExpensesForChart]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Vishal Kumar
-- Exec Param : GetTotalFeeAndExpensesForChart 6,6
-- Description: To Get Total fee Collection of a school in a session
-- =============================================

CREATE Proc [dbo].[GetTotalFeeAndExpensesForChart]
@SchoolId int ,
@SessionId int
AS
BEGIN
Select * from ( Select sum(Amount) as TotalExpense from Inventory Where SchoolId = @SchoolId and SessionId = @SessionId AND Inventory.StatusId=1) as TotalExpense
,
(Select Sum(ISNULL( ft.TotalAmountDeposit,0)) as TotalFeeCollection from FeeTransaction ft Left Outer join Student s on s.id = ft.StudentId
Where s.SchoolId = @SchoolId and ft.SessionId = @SessionId AND ft.StatusId=1) as TotalFeeCollection
END
GO
/****** Object:  StoredProcedure [dbo].[GetInventoryByInventoryId]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetInventoryByInventoryId]
@Id int
as
begin
select i.Id,i.Amount,i.PaymentModeTypeId,i.CreatedBy,i.CreatedOn,i.EndDate,i.StartDate,i.InvetoryTypeId,i.ItemDescription,i.VenderId
,i.ItemId,itm.Name as ItemName,vm.Name as VendorName,i.SessionId,i.StatusId,i.SchoolId,s.Name as SessionName,k.KeyWordName as InventoryName
from Inventory i join ItemMaster itm on i.InvetoryTypeId=itm.ExpensesTypeId
  join VendorMaster vm on vm.TypeId=i.InvetoryTypeId
join SessionMaster s on i.SessionId =s.Id
join KeyWordMaster k on k.KeyWordId=i.InvetoryTypeId  where i.id=@Id and k.Name='InventoryType'
end
GO
/****** Object:  StoredProcedure [dbo].[GetInventory]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetInventory]
@SchoolId int
AS
Begin
	Select 
		i.Id,i.SchoolId,i.ItemId,i.VenderId,s.Name as SessionName,i.ItemDescription,k.KeyWordName as InventoryName,i.Amount,
		i.InvetoryTypeId,i.StartDate,i.EndDate,i.SessionId,i.CreatedOn,im.Name as ItemName,vm.Name as VendorName,
		i.CreatedBy,i.LastUpdatedBy,i.LastUpdatedOn,i.StatusId	
	From Inventory i 
	join SessionMaster s on i.SessionId =s.Id 
	join ItemMaster im on im.Id=i.ItemId
	join VendorMaster vm on vm.Id=i.VenderId
	join KeyWordMaster k on i.InvetoryTypeId=k.KeyWordId and k.Name='InventoryType' 
	Where i.StatusId <> 3 and i.SchoolId=@SchoolId
END
GO
/****** Object:  StoredProcedure [dbo].[GetFeeHeads]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetFeeHeads]
@SchoolId int
as
begin
select  f.Id,f.Name,f.Description,f.FeeHeadTypeId,f.SchoolId,f.StatusId,k.KeyWordName as FeeHeadTypeName
       from FeeHeadMaster f 
       join KeyWordMaster k on f.FeeHeadTypeId=k.KeyWordId 
      where f.StatusId<>3 and f.SchoolId=@SchoolId and k.Name='FeeHeadType'

end
GO
/****** Object:  Table [dbo].[ExamSubjectMapping]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExamSubjectMapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ExamId] [int] NOT NULL,
	[SubjectId] [int] NOT NULL,
	[Date] [datetime] NULL,
	[TotalMarks] [numeric](18, 2) NOT NULL,
 CONSTRAINT [PK_ExamSubjectMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FeeHeadMonthMapping]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FeeHeadMonthMapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FeeHeadId] [int] NOT NULL,
	[ClassId] [int] NULL,
	[SessionId] [int] NULL,
	[MonthId] [int] NULL,
	[Amount] [numeric](18, 2) NULL,
 CONSTRAINT [PK_FeeHeadMonthMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[GetFeeHeadById]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetFeeHeadById]
@Id int
as
begin
select f.Id,f.Name,f.Description,f.FeeHeadTypeId,f.SchoolId,f.StatusId,f.IsDiscount
       from FeeHeadMaster f where f.Id=id 
end
GO
/****** Object:  StoredProcedure [dbo].[GetExpenseReport]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author: Shubham Mishra
-- Exec Param : GetExpenseReport 6,6,1,1,5
-- Description: All Expense Report
-- =============================================
CREATE PROCEDURE [dbo].[GetExpenseReport]
@SchoolId INT ,
@SessionId INT,
@ExpenseTypeId INT,
@FromMonthId INT,
@ToMonthId INT
AS
BEGIN

	SELECT item.Name AS ItemName, i.StartDate, i.EndDate, v.Name AS VendorName, i.Amount,
		km.KeyWordName AS InventoryName, s.Name AS SessionName
	FROM dbo.Inventory AS i LEFT JOIN
	dbo.VendorMaster AS v ON i.VenderId = v.Id LEFT JOIN
	dbo.ItemMaster AS item ON i.ItemId = item.Id LEFT JOIN
	dbo.SessionMaster AS s ON i.SessionId = s.Id LEFT JOIN
	dbo.KeyWordMaster AS km ON i.InvetoryTypeId = km.KeyWordId AND km.Name = 'InventoryType'
	WHERE i.InvetoryTypeId = @ExpenseTypeId AND i.SchoolId = @SchoolId AND i.SessionId = @SessionId
	AND DATEPART(M,i.CreatedOn) >= @FromMonthId AND DATEPART(M, i.CreatedOn) <= @ToMonthId

END
GO
/****** Object:  Table [dbo].[FineDiscountTransactionDetail]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FineDiscountTransactionDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FeeHeadId] [int] NOT NULL,
	[TransactionId] [int] NOT NULL,
	[FeeHeadAmount] [decimal](18, 2) NULL,
 CONSTRAINT [PK_FineDiscountTransactionDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FeeTransactionDetail]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FeeTransactionDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FeeTransactionId] [int] NOT NULL,
	[FeeHeadMappingId] [int] NOT NULL,
 CONSTRAINT [PK_FeeTransactionDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[GetCountDetails]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetCountDetails]
@SchoolId int
as
begin
Select(
Select Count(*)  from Student Where ( SchoolId=@SchoolId or SchoolId=0) and StatusId=1 ) as TotalStudents,
(Select Count(*)  from ClassMaster Where ( SchoolId=@SchoolId or SchoolId=0) and Status=1) as TotalClasses,
(Select Count(*)  from SubjectMaster Where  (SchoolId=@SchoolId or SchoolId=0) and Status=1) as TotalSubjects,
( Select COUNT(*) from SectionMaster SM INNER JOIN ClassMaster CM ON SM.ClassId=CM.Id where  SM.Status=1 AND CM.SchoolId=@SchoolId ) as TotalSections,
(Select COUNT(*) from UserMaster where SchoolId=@SchoolId  and StatusId=1) as TotalUsers,
( Select ac.LeftMessageCount  from AccountDetail ac where SchoolId=@SchoolId and ac.StatusId=1) as LeftMessage
end
GO
/****** Object:  StoredProcedure [dbo].[GetConfigurationCountDetails]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetConfigurationCountDetails]
@SchoolId int
as
begin
Select(
Select Count(*)  from Student Where ( SchoolId=@SchoolId or SchoolId=0) and StatusId=1 ) as TotalStudent,
(Select Count(*)  from ClassMaster Where ( SchoolId=@SchoolId or SchoolId=0) and Status=1) as TotalClass,
(Select Count(*)  from SubjectMaster Where  (SchoolId=@SchoolId or SchoolId=0) and Status=1) as TotalSubject,
(Select Count(*)  from ItemMaster Where  (SchoolId=@SchoolId or SchoolId=0) and StatusId=1) as TotalItem,
(Select Count(*)  from VendorMaster Where  (SchoolId=@SchoolId or SchoolId=0) and StatusId=1) as TotalVendor,
(Select Count(*)  from TeacherMaster Where  (SchoolId=@SchoolId or SchoolId=0) and StatusId=1) as TotalEmployee,
(Select Count(*)  from UserMaster Where  (SchoolId=@SchoolId or SchoolId=0) and StatusId=1) as TotalUser,
(Select Count(*)  from SchoolMaster Where  StatusId=1) as TotalSchool,
(Select Count(*)  from SessionMaster Where  (SchoolId=@SchoolId or SchoolId=0) and StatusId=1) as TotalSession,
(Select Count(*)  from AttendanceMaster Where  (SchoolId=@SchoolId or SchoolId=0) ) as TotalAttendance,
(Select Count(*)  from MessageMaster Where  (SchoolId=@SchoolId or SchoolId=0) and StatusId=1) as TotalMessage,
(Select Count(*)  from FeeHeadMaster Where  (SchoolId=@SchoolId or SchoolId=0) and StatusId=1) as TotalFeeHead,
(Select Count(*)  from PlanMaster Where StatusId=1) as TotalPlan,
( Select COUNT(*) from SectionMaster where  Status=1) as TotalSection
end
GO
/****** Object:  StoredProcedure [dbo].[GetClassBySubject]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetClassBySubject]
@SchoolId int
AS
BEGIN
Select * into #temp 
From(
Select scm.* , sm.Name as SubjectName , cm.Name as ClassName ,sm.SchoolId,sm.[Status] as SubjectStatus From SubjectClassMapping scm 
Left Outer Join SubjectMaster sm ON sm.Id = scm.SubjectId
Left Outer Join ClassMaster cm ON cm.Id = scm.ClassId where sm.Status<>3
) a
Where a.SchoolId = @SchoolId 


Select distinct ST2.SubjectId, ST2.SubjectName,ST2.SubjectStatus,
    substring(
        (
            Select ','+ST1.ClassName  AS [text()]
            From #temp ST1
            Where ST1.SubjectId = ST2.SubjectId
            ORDER BY ST1.SubjectId
            For XML PATH ('')
        ), 2, 1000) ClassNames
From #temp ST2
END
GO
/****** Object:  StoredProcedure [dbo].[GetFeeTransactionByStudentId]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Vishal Kumar
-- Exec Param : [GetFeeTransactionByStudentId] 19,6
-- Description: To Get Total fee Collection of a student within Session
-- =============================================
CREATE Proc [dbo].[GetFeeTransactionByStudentId]
@StudentId int,
@SessionId int
AS
BEGIN

Create table #temp
(
TransactionId int,
Fine numeric(18,2),
)

--Fine Records
Insert  into #temp (TransactionId,Fine)
select ftd.TransactionId,
ISNULL( Sum(ftd.FeeHeadAmount),0) as Fine
from FineDiscountTransactionDetail ftd Inner join
FeeHeadMaster fhm on fhm.Id = ftd.FeeHeadId
Where fhm.FeeHeadTypeId = 4
Group By ftd.TransactionId


Select * into #temp1
from (
select ftd.TransactionId,isnull(t.Fine,0) as Fine,
ISNULL( Sum(ftd.FeeHeadAmount),0) as Discount
from FineDiscountTransactionDetail ftd
Inner join
FeeHeadMaster fhm on fhm.Id = ftd.FeeHeadId
LEft OUTER join
#temp t ON t.TransactionId = ftd.TransactionId
Where fhm.FeeHeadTypeId = 3
Group By ftd.TransactionId,t.TransactionId,t.Fine ) as res



SELECT ft.Id,ft.StudentId,ft.TotalAmountDeposit,ft.PreviousBalance,ft.TotalAmount,ft.TotalPayableAmount,ft.IsSettled,ft.BalanceAmount,ft.Remark,ft.TransactionDate,ft.CreatedOn,ft.CreatedBy,ft.LastUpdatedBy,
ft.LastUpdatedOn,ft.StatusId,ft.FromMonthId ,ft.ToMonthId,
km1.KeyWordName as  FromMonth,
km2.KeyWordName as ToMonth,
ISNULL( t1.Discount,0) as Discount, ISNULL(t1.Fine,0) as Fine
FROM  FeeTransaction ft  Left outer join #temp1 t1 On t1.TransactionId = ft.Id 
Left Outer Join  KeyWordMaster km1 ON km1.KeyWordId = ft.FromMonthId and km1.Name = 'Month' 
Left Outer Join  KeyWordMaster km2 ON km2.KeyWordId = ft.ToMonthId and km2.Name = 'Month'
WHERE ft.StudentId =@StudentId AND ft.SessionId = @SessionId AND ft.StatusId<>3

END
GO
/****** Object:  StoredProcedure [dbo].[GetGeneralwiseFeeHeadDetialForEdit]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetGeneralwiseFeeHeadDetialForEdit]
@SchoolId int,
@SessionId int,
@FromMonth int,
@ToMonth int,
@HeadTypeId int
AS
BEGIN
	Select 
		 fhm.Id as FeeHeadMasterId,fhm.Name,fhm.Description,fhm.IsDiscount,fhm.FeeHeadTypeId,fhm.SchoolId,fhm.CreatedOn,fhm.CreatedBy,fhm.StatusId,fhm.LastModifiedOn,fhm.LastModifiedBy,
		fhmm.Id as FeeHeadMappingId,fhmm.FeeHeadId,fhmm.ClassId,fhmm.SessionId,fhmm.MonthId,fhmm.Amount,
		km.KeyWordName as FeeMonthName,
		kh.KeyWordName as FeeHeadTypeName,
		 sm.Name as SessionName
	From 
		[dbo].[FeeHeadMaster] fhm 
		INNER JOIN [dbo].[FeeHeadMonthMapping] fhmm ON fhmm.FeeHeadId = fhm.Id
		Left OUTER JOIN KeyWordMaster km ON km.KeyWordId = fhmm.MonthId
		Left OUTER JOIN KeyWordMaster kh ON kh.KeyWordId = fhm.Id
		Left OUTER JOIN SessionMaster sm ON sm.Id = fhmm.SessionId
	Where fhm.StatusId = 1 AND km.Name='Month' AND fhm.SchoolId = @SchoolId AND fhmm.SessionId  =@SessionId AND fhmm.ClassId =0 AND fhm.FeeHeadTypeId = @HeadTypeId
	AND fhmm.MonthId BETWEEN @FromMonth and @ToMonth 
	END
GO
/****** Object:  StoredProcedure [dbo].[SaveExamDetail]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SaveExamDetail]
@ExamId int,
@SubjectId int,
@Date datetime,
@TotalMarks numeric(18,2)
as
begin
update ExamSubjectMapping set SubjectId=@SubjectId,Date=@Date,TotalMarks=@TotalMarks
       where ExamId=@ExamId and SubjectId=@SubjectId
end
GO
/****** Object:  Table [dbo].[StudentExamMapping]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentExamMapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ExamSubjectMappingId] [int] NOT NULL,
	[ClassId] [int] NOT NULL,
	[SectionId] [int] NOT NULL,
	[ObtainedMarks] [decimal](18, 2) NOT NULL,
	[StudentId] [int] NOT NULL,
	[StatusId] [int] NULL,
	[LastUpdateBy] [int] NULL,
	[LastUpdatedOn] [datetime] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
 CONSTRAINT [PK_StudentExamMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StudentClassMapping]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StudentClassMapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StudentId] [int] NOT NULL,
	[ClassId] [int] NOT NULL,
	[SectionId] [int] NOT NULL,
	[SessionId] [int] NOT NULL,
	[StudentCode] [varchar](100) NULL,
 CONSTRAINT [PK_StudentClassMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TeacherSubjectMapping]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeacherSubjectMapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TeacherId] [int] NOT NULL,
	[ClassId] [int] NOT NULL,
	[SectionId] [int] NOT NULL,
	[SubjectId] [int] NOT NULL,
 CONSTRAINT [PK_TeacherSubjectMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[GetFeeHeadDetailForEdit]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[GetFeeHeadDetial] 6,6,25,1,3,2,19  
CREATE Proc [dbo].[GetFeeHeadDetailForEdit]  
@SchoolId int,  
@SessionId int,  
@ClassId int ,  
@FromMonth int,  
@ToMonth int,  
@HeadTypeId int,  
@StudentId int  
AS  
BEGIN  
 Select   
  fhm.Id as FeeHeadMasterId,fhm.Name,fhm.Description,fhm.IsDiscount,fhm.FeeHeadTypeId,fhm.SchoolId,fhm.CreatedOn,fhm.CreatedBy,fhm.StatusId,fhm.LastModifiedOn,fhm.LastModifiedBy,  
  fhmm.Id as FeeHeadMappingId,fhmm.FeeHeadId,fhmm.ClassId,fhmm.SessionId,fhmm.MonthId,fhmm.Amount,  
  km.KeyWordName as FeeMonthName,  
  kh.KeyWordName as FeeHeadTypeName,  
  cm.Name as ClassName , sm.Name as SessionName  
 From   
  [dbo].[FeeHeadMaster] fhm   
  INNER JOIN [dbo].[FeeHeadMonthMapping] fhmm ON fhmm.FeeHeadId = fhm.Id  
  Left OUTER JOIN KeyWordMaster km ON km.KeyWordId = fhmm.MonthId  
  Left OUTER JOIN KeyWordMaster kh ON kh.KeyWordId = fhm.Id  
  Left OUTER JOIN ClassMaster cm ON cm.Id = fhmm.ClassId  
  Left OUTER JOIN SessionMaster sm ON sm.Id = fhmm.SessionId  
 Where fhm.StatusId = 1 AND km.Name='Month' AND fhm.SchoolId =@SchoolId AND fhmm.SessionId  = @SessionId AND fhmm.ClassId = @ClassId AND fhm.FeeHeadTypeId = @HeadTypeId  
 AND  fhmm.MonthId >= @FromMonth   and fhmm.MonthId <=  @ToMonth  
 END  
   
--select * from FeeTransactionDetail
--select * from FeeTransaction
GO
/****** Object:  StoredProcedure [dbo].[UpdateStudentExamDetail]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UpdateStudentExamDetail]
@ExamSubjectMappingId int,
@ObtainedMarks decimal,
@LastUpdatedBy int,
@LastUpdatedOn datetime
AS BEGIN
        UPDATE StudentExamMapping SET ObtainedMarks=@ObtainedMarks,LastUpdateBy=@LastUpdatedBy,LastUpdatedOn=@LastUpdatedOn
        WHERE StudentExamMapping.ExamSubjectMappingId=@ExamSubjectMappingId
END
GO
/****** Object:  StoredProcedure [dbo].[GetTodaysBirthDay]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetTodaysBirthDay]
@SchoolId int
as
begin
select
   s.FirstName as StudentName,c.Name as ClassName,sec.Name as SectionName
    from Student s  inner join StudentClassMapping scm  on s.Id=scm.StudentId
    inner join  ClassMaster c on c.Id=scm.ClassId
                inner join SectionMaster sec on sec.Id=scm.SectionId
                where s.SchoolId=@SchoolId and MONTH(s.DOB)=MONTH(GETDATE()) and DAY(s.DOB)=DAY(GETDATE())
                
end
GO
/****** Object:  StoredProcedure [dbo].[GetStudentsList]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[GetStudentsList]
@SchoolId int
AS
Select s.Id ,s.FirstName ,s.MiddileName,s.LastName,s.TitleId,s.Email,s.StudentMobile,scm.StudentCode ,
  s.MotherName,s.FatherName,s.CorrCityId,s.GenderId,s.SchoolId,s.Uid,
  s.DOJ,s.DOB,s.DOE,s.StatusId ,s.CreatedOn,s.CreatedBy,s.LastUpdatedOn,s.LastUpdatedBy,s.ImagePath,
  s.RelegionId,s.CategoryId,s.ReasonForExit,s.NationlityId,
  kg.KeyWordName , kn.KeyWordName as NationlityName, kr.KeyWordName as  RelegionName,
  kt.KeyWordName as TitleName ,kc.KeyWordName as StudentCategory
 From Student s inner join StudentClassMapping scm on s.Id=scm.StudentId
 
 left outer join KeyWordMaster kg ON kg.KeyWordId = s.GenderId AND kg.Name = 'Gender'
 left outer join KeyWordMaster kn ON kn.KeyWordId = s.NationlityId AND kn.Name = 'Nationlity'
 left outer join KeyWordMaster kr ON kr.KeyWordId = s.RelegionId AND kr.Name = 'Relegion'
 left outer join KeyWordMaster kc ON kc.KeyWordId = s.CategoryId AND kc.Name = 'Category'
 left outer join KeyWordMaster kt ON kt.KeyWordId = s.TitleId AND kt.Name = 'Title'
Where s.SchoolId = @SchoolId  AND s.StatusId <> 3
GO
/****** Object:  StoredProcedure [dbo].[GetStudentsForAllSchoolForReport]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetStudentsForAllSchoolForReport]
@SessionId int,
@SchoolId int,
@ClassId int,
@SectionId int,
@StatusId int,
@AddmissionStatus int
AS
Select s.Id as StudentId,s.FirstName as StudentName,s.MiddileName,s.LastName,s.TitleId,s.Email,s.StudentMobile as StudentContactNumber,scm.StudentCode as StudentRollNumber,
  s.MotherName,s.FatherName,s.CorrCityId,s.GenderId,s.SchoolId,s.Uid,s.PermanentAddress,s.CorrAddress,
  s.DOJ,s.DOB,s.DOE,s.StatusId as StudentStatusId,s.CreatedOn,s.CreatedBy,s.LastUpdatedOn,s.LastUpdatedBy,s.ImagePath,
  s.RelegionId,s.CategoryId,s.ReasonForExit,s.NationlityId,s.GuardianAddress,s.GuardianName,s.GuardianOccupation,s.GuardianRelationWithStudent,
  kg.KeyWordName as StudentGender, kn.KeyWordName as NationlityName, kr.KeyWordName as  RelegionName,
  kt.KeyWordName as TitleName ,kc.KeyWordName as StudentCategory
 From Student s inner join StudentClassMapping scm on s.Id=scm.StudentId
 left outer join CityMaster cm ON cm.Id=s.CorrCityId 
 left outer join KeyWordMaster kg ON kg.KeyWordId = s.GenderId AND kg.Name = 'Gender'
left outer join KeyWordMaster kn ON kn.KeyWordId = s.NationlityId AND kn.Name = 'Nationlity'
 left outer join KeyWordMaster kr ON kr.KeyWordId = s.RelegionId AND kr.Name = 'Relegion'
 left outer  join KeyWordMaster kc ON kc.KeyWordId = s.CategoryId AND kc.Name = 'Category'
 left outer join KeyWordMaster kt ON kt.KeyWordId = s.TitleId AND kt.Name = 'Title'
Where s.SchoolId = @SchoolId and scm.SessionId=@SessionId 
And ((@ClassId = 0 ) OR (@ClassId = scm.ClassId) ) AND ((@AddmissionStatus=1) OR (@AddmissionStatus=2) OR(@AddmissionStatus=0)) AND
 ((@SectionId = 0 ) OR (@SectionId = scm.SectionId) ) AND
 ((@StatusId = 0 ) OR (@StatusId = s.StatusId)) AND s.StatusId<>3
GO
/****** Object:  StoredProcedure [dbo].[GetStudentsForAllSchool]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetStudentsForAllSchool]
@SessionId int,
@SchoolId int,
@ClassId int,
@SectionId int,
@StatusId int
AS
Select s.Id as StudentId,s.FirstName as StudentName,s.MiddileName,s.LastName,s.TitleId,s.Email,s.StudentMobile as StudentContactNumber,scm.StudentCode as StudentRollNumber,
  s.MotherName,s.FatherName,s.CorrCityId,s.GenderId,s.SchoolId,s.Uid,s.PermanentAddress,s.CorrAddress,
  s.DOJ,s.DOB,s.DOE,s.StatusId as StudentStatusId,s.CreatedOn,s.CreatedBy,s.LastUpdatedOn,s.LastUpdatedBy,s.ImagePath,
  s.RelegionId,s.CategoryId,s.ReasonForExit,s.NationlityId,s.GuardianAddress,s.GuardianName,s.GuardianOccupation,s.GuardianRelationWithStudent,
  kg.KeyWordName as StudentGender, kn.KeyWordName as NationlityName, kr.KeyWordName as  RelegionName,
  kt.KeyWordName as TitleName ,kc.KeyWordName as StudentCategory
 From Student s inner join StudentClassMapping scm on s.Id=scm.StudentId
 left outer join CityMaster cm ON cm.Id=s.CorrCityId 
 left outer join KeyWordMaster kg ON kg.KeyWordId = s.GenderId AND kg.Name = 'Gender'
left outer join KeyWordMaster kn ON kn.KeyWordId = s.NationlityId AND kn.Name = 'Nationlity'
 left outer join KeyWordMaster kr ON kr.KeyWordId = s.RelegionId AND kr.Name = 'Relegion'
 left outer  join KeyWordMaster kc ON kc.KeyWordId = s.CategoryId AND kc.Name = 'Category'
 left outer join KeyWordMaster kt ON kt.KeyWordId = s.TitleId AND kt.Name = 'Title'
Where s.SchoolId = @SchoolId and scm.SessionId=@SessionId 
And ((@ClassId = 0 ) OR (@ClassId = scm.ClassId) ) AND
 ((@SectionId = 0 ) OR (@SectionId = scm.SectionId) ) AND
 ((@StatusId = 0 ) OR (@StatusId = s.StatusId)) AND s.StatusId<>3
GO
/****** Object:  StoredProcedure [dbo].[GetStudentSessionWiseForChart]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[GetStudentSessionWiseForChart]
@SchoolId int
AS
BEGIN
Select sm.Name as SessionName, Count(s.Id) as CountNo from Student s 
Inner join StudentClassMapping scm On s.Id = scm.StudentId --AND scm.SessionId = 6
Inner join SessionMaster sm ON sm.Id = scm.SessionId
Where s.SchoolId = @SchoolId
Group by scm.SessionId,sm.Name
END
GO
/****** Object:  StoredProcedure [dbo].[GetStudents]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetStudents]
@SessionId int,
@ClassId int,
@SectionId int,
@SchoolId int
AS
Select s.Id as StudentId,s.FirstName as StudentName,s.MiddileName,s.LastName,s.TitleId,s.Email,s.StudentMobile as StudentContactNumber,scm.StudentCode as StudentRollNumber,
  s.MotherName,s.FatherName,s.CorrCityId,s.GenderId,s.SchoolId,s.Uid,
  s.DOJ,s.DOB,s.DOE,s.StatusId as StudentStatusId,s.CreatedOn,s.CreatedBy,s.LastUpdatedOn,s.LastUpdatedBy,s.ImagePath,
  s.RelegionId,s.CategoryId,s.ReasonForExit,s.NationlityId,
  kg.KeyWordName as StudentGender, kn.KeyWordName as NationlityName, kr.KeyWordName as  RelegionName,
  kt.KeyWordName as TitleName ,kc.KeyWordName as StudentCategory
 From Student s inner join StudentClassMapping scm on s.Id=scm.StudentId
  left outer join KeyWordMaster kg ON kg.KeyWordId = s.GenderId AND kg.Name = 'Gender'
left outer join KeyWordMaster kn ON kn.KeyWordId = s.NationlityId AND kn.Name = 'Nationlity'
 left outer join KeyWordMaster kr ON kr.KeyWordId = s.RelegionId AND kr.Name = 'Relegion'
 left outer  join KeyWordMaster kc ON kc.KeyWordId = s.CategoryId AND kc.Name = 'Category'
 left outer join KeyWordMaster kt ON kt.KeyWordId = s.TitleId AND kt.Name = 'Title'
Where s.SchoolId = @SchoolId  and scm.ClassId=@ClassId and scm.SectionId=@SectionId and scm.SessionId=@SessionId
GO
/****** Object:  StoredProcedure [dbo].[GetStudentMappingList]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetStudentMappingList]
@StudentId int,
@SchoolId int,
@ExamTypeId int
AS
BEGIN
SELECT        SubjectMaster.Name AS SubjectName,ExamSubjectMapping.Id as ExamSubjectMappingId, ExamSubjectMapping.TotalMarks, SubjectMaster.Id
FROM            SubjectMaster INNER JOIN
                         SubjectClassMapping ON SubjectMaster.Id = SubjectClassMapping.SubjectId INNER JOIN
                         ExamSubjectMapping ON SubjectMaster.Id = ExamSubjectMapping.SubjectId INNER JOIN
                         Student INNER JOIN
                         StudentClassMapping ON Student.Id = StudentClassMapping.StudentId AND Student.Id = StudentClassMapping.StudentId ON 
                         SubjectClassMapping.ClassId = StudentClassMapping.ClassId
       WHERE Student.Id =@StudentId AND ExamSubjectMapping.ExamId =@ExamTypeId AND Student.SchoolId =@SchoolId
END
GO
/****** Object:  StoredProcedure [dbo].[GetStudentExamMappingListForEdit]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetStudentExamMappingListForEdit]
@StudentId int,
@SchoolId int,
@ExamTypeId int
AS BEGIN
SELECT        SubjectMaster.Name AS SubjectName, StudentExamMapping.ObtainedMarks AS ObtainMarks, ExamSubjectMapping.TotalMarks,StudentExamMapping.ExamSubjectMappingId
FROM            ExamMaster INNER JOIN
                         ExamSubjectMapping ON ExamMaster.Id = ExamSubjectMapping.ExamId INNER JOIN
                         SubjectMaster ON ExamSubjectMapping.SubjectId = SubjectMaster.Id INNER JOIN
                         StudentExamMapping ON ExamSubjectMapping.Id = StudentExamMapping.ExamSubjectMappingId
                         where StudentExamMapping.StudentId=@StudentId AND SubjectMaster.SchoolId=@SchoolId AND ExamMaster.Id=@ExamTypeId AND StudentExamMapping.StatusId<>3
                         
 END
GO
/****** Object:  StoredProcedure [dbo].[GetStudentExamMappingDetail]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetStudentExamMappingDetail]
@SchoolId int
AS
BEGIN
SELECT     ClassMaster.Name AS ClassName,ExamMaster.Id AS ExamId, ExamMaster.Name AS ExamName, SectionMaster.Name AS SectionName, Student.FirstName AS StudentName, StudentExamMapping.Id, StudentExamMapping.ObtainedMarks, 
                      StudentExamMapping.StatusId, StudentExamMapping.StudentId,StudentExamMapping.ExamSubjectMappingId,StudentExamMapping.ObtainedMarks AS ObtainMarks
FROM               StudentExamMapping INNER JOIN ClassMaster ON  StudentExamMapping.ClassId=ClassMaster.Id
                INNER JOIN SectionMaster ON SectionMaster.Id=StudentExamMapping.SectionId
                INNER JOIN ExamSubjectMapping ON ExamSubjectMapping.Id=StudentExamMapping.ExamSubjectMappingId
                INNER JOIN ExamMaster ON ExamMaster.Id=ExamSubjectMapping.ExamId
                INNER JOIN  Student ON Student.Id=StudentExamMapping.StudentId
               -- INNER JOIN ExamSubjectMapping ON ExamSubjectMapping.ExamId=ExamMaster.Id
                      WHERE  Student.SchoolId=@SchoolId AND Student.StatusId=1 AND StudentExamMapping.StatusId<>3
END
GO
/****** Object:  StoredProcedure [dbo].[GetStudentDetailForProfile]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetStudentDetailForProfile]
@StudentId int
as
begin
select s.Id,s.TitleId,k.KeyWordName as TitleName,s.StudentMobile,s.FirstName,s.LastName,s.MiddileName,s.RegistrationNumber,s.DOB,s.DOJ,s.Email,s.Uid,s.ImagePath,s.S3key,s.StatusId  
,s.CorrAddress as CuurentAddress,s.CorrPinCode as CurrentPinCode,s.FatherName,s.MotherName,s.GuardianAddress,s.GuardianMobileNo as GuardianMobile,s.GuardianName  
,s.GuardianOccupation,s.GuardianRelationWithStudent as GuardianRelation,s.ParentContactNo,s.ParentMobileNo as ParentMobileNumber,s.PermanentAddress  
,s.PermanentPinCode,s.CategoryId,s.GenderId,cm.Name as ClassName,sm.Name as SectionName,cmm.Name as CurrentCityName,smmm.Name as PermanentStateName  
,ks.KeyWordName as RelegionCategoryName,ssn.Name as SessionName ,scm.StudentCode,smm.Name as CurrentStateName,cmmm.Name as PermanentCityName,km.KeyWordName as GenderName  
,kmm.KeyWordName as NationlityName,kmmm.KeyWordName as RelegionName  
from  Student s left outer join KeyWordMaster k on s.TitleId=k.KeyWordId  and k.Name='Title'
               left outer join KeyWordMaster ks on s.CategoryId=ks.KeyWordId and ks.Name='Category' 
               left outer join KeyWordMaster km on s.GenderId=km.KeyWordId and km.Name='Gender' 
               left outer join KeyWordMaster kmm on s.NationlityId=kmm.KeyWordId  and kmm.Name='Nationlity'
               left outer join KeyWordMaster kmmm on s.RelegionId=kmmm.KeyWordId   and kmmm.Name='Relegion' 
               left outer join StudentClassMapping scm on s.id=scm.StudentId  
               left outer  join SectionMaster sm on scm.SectionId=sm.Id  
               left outer join ClassMaster cm on cm.Id=scm.ClassId  
               left outer join SessionMaster ssn on ssn.Id=scm.SessionId  
               left outer join CityMaster cmm on cmm.Id= s.CorrCityId   
               left outer join CityMaster cmmm on cmmm.Id=s.PermanentCityId  
               left outer join StateMaster smm on smm.Id=cmm.StateId    
               left outer join StateMaster smmm on smmm.Id=cmmm.StateId               
where s.Id=@StudentId and s.StatusId=1  
end
GO
/****** Object:  StoredProcedure [dbo].[GetStudentDetailForCertificate]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetStudentDetailForCertificate]
@SessionId int,
@ClassId int,
@SectionId int,
@StudentId int

as
begin
select s.Id,s.TitleId,k.KeyWordName as TitleName,s.StudentMobile,s.FirstName,s.LastName,s.MiddileName,s.DOB,s.DOJ,s.Email,s.Uid,s.ImagePath,s.S3key,s.StatusId,scm.StudentCode
,s.CorrAddress as CuurentAddress,s.CorrPinCode as CurrentPinCode,s.FatherName,s.MotherName,s.GuardianAddress,s.GuardianMobileNo as GuardianMobile,s.GuardianName
,s.GuardianOccupation,s.GuardianRelationWithStudent as GuardianRelation,s.ParentContactNo,s.ParentMobileNo as ParentMobileNumber,s.PermanentAddress
,s.PermanentPinCode,s.CategoryId,s.GenderId,cm.Name as ClassName,sm.Name as SectionName,cmm.Name as CurrentCityName,smmm.Name as PermanentStateName
,ks.KeyWordName as RelegionCategoryName,ssn.Name as SessionName ,scm.StudentCode,smm.Name as CurrentStateName,cmmm.Name as PermanentCityName,km.KeyWordName as GenderName
,kmm.KeyWordName as NationlityName,kmmm.KeyWordName as RelegionName,sclm.Name as SchoolName,sclm.Address1 as SchoolAddress ,sclm.Code as SchoolCode,s.ReasonForExit
from  Student s inner join KeyWordMaster k on s.TitleId=k.KeyWordId
               left outer join KeyWordMaster ks on s.CategoryId=ks.KeyWordId
               inner join KeyWordMaster km on s.GenderId=km.KeyWordId
               inner join KeyWordMaster kmm on s.NationlityId=kmm.KeyWordId
               inner join KeyWordMaster kmmm on s.RelegionId=kmmm.KeyWordId
               inner join StudentClassMapping scm on s.id=scm.StudentId
               inner  join SectionMaster sm on scm.SectionId=sm.Id
               inner join ClassMaster cm on cm.Id=scm.ClassId
               inner join SessionMaster ssn on ssn.Id=scm.SessionId
               inner join CityMaster cmm on cmm.Id= s.CorrCityId 
               inner join CityMaster cmmm on cmmm.Id=s.PermanentCityId
               inner join StateMaster smm on smm.Id=cmm.StateId  
               inner join StateMaster smmm on smmm.Id=cmmm.StateId
               inner join SchoolMaster sclm on s.SchoolId=sclm.Id             
where s.Id=@StudentId and scm.SessionId=@SessionId and scm.ClassId=@ClassId  and s.StatusId=1 and k.Name='Title' and ks.Name='Category' and km.Name='Gender' and kmm.Name='Nationlity' and kmmm.Name='Relegion'
end
GO
/****** Object:  StoredProcedure [dbo].[GetStudentClassChart]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetStudentClassChart]
@SchoolId INT

AS BEGIN

SELECT        ClassMaster.Id, ClassMaster.Name, COUNT(StudentClassMapping.StudentId) AS TotalStudents
FROM        Student LEFT Outer join StudentClassMapping on Student.Id=StudentClassMapping.StudentId Left Outer JOIN
                         ClassMaster ON StudentClassMapping.ClassId = ClassMaster.Id
						 WHERE ClassMaster.SchoolId = @SchoolId AND ClassMaster.Status=1 AND Student.StatusId=1 
						 GROUP BY ClassMaster.Name, ClassMaster.Id						 

UNION

SELECT Id, Name, 0 AS TotalStudents FROM ClassMaster WHERE ClassMaster.SchoolId = @SchoolId AND ClassMaster.Status=1 AND Id NOT IN
 (SELECT        ClassMaster.Id
FROM             Student LEFT Outer join StudentClassMapping on Student.Id=StudentClassMapping.StudentId Left Outer JOIN
                         ClassMaster ON StudentClassMapping.ClassId = ClassMaster.Id
						 WHERE ClassMaster.SchoolId = @SchoolId AND ClassMaster.Status=1 AND Student.StatusId=1)

END
GO
/****** Object:  StoredProcedure [dbo].[GetInActiveStudents]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetInActiveStudents]
@SessionId int,
@ClassId int,
@SectionId int,
@SchoolId int
AS
Select s.Id as StudentId,s.FirstName as StudentName,s.MiddileName,s.LastName,s.TitleId,s.Email,s.StudentMobile as StudentContactNumber,scm.StudentCode as StudentRollNumber,
  s.MotherName,s.FatherName,s.CorrCityId,s.GenderId,s.SchoolId,s.Uid,
  s.DOJ,s.DOB,s.DOE,s.StatusId as StudentStatusId,s.CreatedOn,s.CreatedBy,s.LastUpdatedOn,s.LastUpdatedBy,s.ImagePath,
  s.RelegionId,s.CategoryId,s.ReasonForExit,s.NationlityId,
  kg.KeyWordName as StudentGender, kn.KeyWordName as NationlityName, kr.KeyWordName as  RelegionName,
  kt.KeyWordName as TitleName ,kc.KeyWordName as StudentCategory
 From Student s inner join StudentClassMapping scm on s.Id=scm.StudentId
  left outer join KeyWordMaster kg ON kg.KeyWordId = s.GenderId AND kg.Name = 'Gender'
left outer join KeyWordMaster kn ON kn.KeyWordId = s.NationlityId AND kn.Name = 'Nationlity'
 left outer join KeyWordMaster kr ON kr.KeyWordId = s.RelegionId AND kr.Name = 'Relegion'
 left outer  join KeyWordMaster kc ON kc.KeyWordId = s.CategoryId AND kc.Name = 'Category'
 left outer join KeyWordMaster kt ON kt.KeyWordId = s.TitleId AND kt.Name = 'Title'
Where s.SchoolId = @SchoolId  and scm.ClassId=@ClassId and scm.SectionId=@SectionId and scm.SessionId=@SessionId AND s.StatusId =2
GO
/****** Object:  StoredProcedure [dbo].[GetGeneralwiseFeeHeadDetial]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[GetGeneralwiseFeeHeadDetial] 17,23,1,1,1,98
CREATE Proc [dbo].[GetGeneralwiseFeeHeadDetial] 
@SchoolId int,
@SessionId int,
@FromMonth int,
@ToMonth int,
@HeadTypeId int,
@StudentId int
AS
BEGIN
	Select 
		 fhm.Id as FeeHeadMasterId,fhm.Name,fhm.Description,fhm.IsDiscount,fhm.FeeHeadTypeId,fhm.SchoolId,fhm.CreatedOn,fhm.CreatedBy,fhm.StatusId,fhm.LastModifiedOn,fhm.LastModifiedBy,
		fhmm.Id as FeeHeadMappingId,fhmm.FeeHeadId,fhmm.ClassId,fhmm.SessionId,fhmm.MonthId,fhmm.Amount,
		km.KeyWordName as FeeMonthName,
		kh.KeyWordName as FeeHeadTypeName,
		 sm.Name as SessionName
	From 
		[dbo].[FeeHeadMaster] fhm 
		INNER JOIN [dbo].[FeeHeadMonthMapping] fhmm ON fhmm.FeeHeadId = fhm.Id
		Left OUTER JOIN KeyWordMaster km ON km.KeyWordId = fhmm.MonthId AND km.Name='Month'
		Left OUTER JOIN KeyWordMaster kh ON kh.KeyWordId = fhm.Id and kh.Name = 'FeeHeadType'
		Left OUTER JOIN SessionMaster sm ON sm.Id = fhmm.SessionId
	Where fhm.StatusId = 1  AND fhm.SchoolId = @SchoolId AND fhmm.SessionId  = @SessionId AND fhmm.ClassId =0 AND fhm.FeeHeadTypeId = @HeadTypeId
	AND fhmm.MonthId BETWEEN @FromMonth and @ToMonth 
	AND  
 fhmm.Id Not In (  
	Select id from FeeHeadMonthMapping Where FeeHeadMonthMapping.Id in (  
	select fd.FeeHeadMappingId  from feetransaction f inner join FeeTransactionDetail fd ON fd.FeeTransactionId = f.Id 
	inner join FeeHeadMonthMapping fhmm1 on fd.FeeHeadMappingId = fhmm1.Id Where fhmm1.SessionId = @SessionId 
	and StudentId = @StudentId --and ((1 between f.FromMonthId and f.ToMonthId) Or  (7 between f.FromMonthId and f.ToMonthId))
	))
END
GO
/****** Object:  StoredProcedure [dbo].[GetFeeTransactionReport]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author: Vishal Kumar
-- Exec Param : GetFeeTransactionReport 6,6,25,8 ,1,5,1
-- Description:All Students Fee Report
-- =============================================
CREATE Proc [dbo].[GetFeeTransactionReport]
@SchoolId int ,
@SessionId int,
@ClassId int,
@SectionId int,
@FromMonthId int,
@ToMonthId int,
@PayStatusId int
AS
BEGIN
Declare @FineAmount decimal(18,2)
Declare @TotalAmount numeric(18,2)


  Select * into #temp From(
Select  Max(ft.Id) as StudentLastTranId,ft.StudentId
From  FeeTransaction ft
Where  @SessionId = ft.SessionId
Group By ft.StudentId
) as res

Select * from (

Select ft1.Id,ft1.StudentId,ISNULL( ft1.TotalAmountDeposit,0) as TotalAmountDeposit,ISNULL( ft1.PreviousBalance,0) as PreviousBalance,ISNULL( ft1.TotalPayableAmount,0) as TotalPayableAmount ,ISNULL( ft1.TotalAmount,0) as TotalAmount ,ft1.IsSettled,ISNULL(

ft1.BalanceAmount,0) as BalanceAmount ,ft1.Remark,ft1.TransactionDate,ft1.CreatedOn,ft1.CreatedBy,ft1.LastUpdatedBy,
ft1.LastUpdatedOn,ft1.StatusId,
km1.KeyWordName as FromMonth , km2.KeyWordName as ToMonth,
s.FirstName + ' ' + s.LastName  as StudentName
From FeeTransaction ft1
Inner join
#temp t ON Ft1.Id = t.StudentLastTranId
Left Outer JOIN Student s ON s.Id = ft1.StudentId
Left Outer JOIN StudentClassMapping scm ON scm.StudentId = s.Id
--Left Outer JOIN ClassMaster cm ON scm.ClassId = cm.Id
--Left Outer JOIN SectionMaster sm on sm.ClassId = cm.id
Left Outer Join  KeyWordMaster km1 ON km1.KeyWordId = ft1.FromMonthId and km1.Name = 'Month'
Left Outer Join  KeyWordMaster km2 ON km2.KeyWordId = ft1.ToMonthId and km2.Name = 'Month'
Where @SchoolId = s.SchoolId AND @SessionId = scm.SessionId AND
ISNULL(@ClassId,scm.ClassId) = scm.ClassId AND
ISNULL(@SectionId,scm.SectionId) = scm.SectionId and
((@FromMonthId is null ) OR ( @FromMonthId >= ft1.FromMonthId)) AND
((@ToMonthId is null ) OR ( @ToMonthId >= ft1.ToMonthId))

) res1

left outer join

(
Select fine.TransactionId,sum(fine.FeeHeadAmount) as Discount from FineDiscountTransactionDetail fine Inner JOIN FeeHeadMaster fhm ON fine.FeeHeadId = fhm.Id
Where  fhm.FeeHeadTypeId = 3 --fine.TransactionId=64 and
Group By fine.TransactionId,fhm.FeeHeadTypeId
) res2

ON res2.TransactionId = res1.Id
left  outer join
(
Select fine.TransactionId,sum(fine.FeeHeadAmount) as Fine from FineDiscountTransactionDetail fine Inner JOIN FeeHeadMaster fhm ON fine.FeeHeadId = fhm.Id
Where  fhm.FeeHeadTypeId = 4
Group By fine.TransactionId,fhm.FeeHeadTypeId
) res3

ON res3.TransactionId = res1.Id

END
GO
/****** Object:  StoredProcedure [dbo].[GetFeeTransactionByTransactionId]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetFeeTransactionByTransactionId]
@FeetransactionId int
AS
BEGIN
 SELECT ft.Id,ft.PaymentModeId,ft.StudentId,std.FirstName+' '+''+std.LastName as StudentName,ft.PreviousBalance,ft.TotalAmount as TotalClassGeneralAmount,ft.PreviousBalance,ft.TotalPayableAmount,ft.TotalAmountDeposit,ft.IsSettled,ft.BalanceAmount,ft.Remark,
ft.TransactionDate,ft.CreatedOn,ft.CreatedBy,ft.LastUpdatedBy,
 ft.LastUpdatedOn,sec.Name as SectionName,cls.Name as ClassName,sess.Name as SessionName,km.KeyWordName as FromMonthName,kms.KeyWordName as ToMonthName,ft.StatusId,ft.FromMonthId ,ft.ToMonthId,s.SessionId,s.ClassId,s.SectionId,
 ftdr.TotalAmount,kmss.KeyWordName as PaymentModeName,sm.ImagePath as SchoolImagePath ,sm.S3key
 FROM  FeeTransaction ft  Inner JOIN 
   (SELECT  ftd.FeeTransactionId , SUM(fhm.Amount) AS TotalAmount
   FROM    FeeTransactionDetail ftd 
   Left Outer JOIN FeeHeadMonthMapping fhm 
   ON  ftd.FeeHeadMappingId = fhm.Id
   GROUP BY ftd.FeeTransactionId)  ftdr 
 ON ftdr.FeeTransactionId = ft.Id
 Left Outer JOIN StudentClassMapping s on s.StudentId = ft.StudentId
 inner join Student std on std.Id =s.StudentId 
 inner join SchoolMaster sm on sm.Id=std.SchoolId
 inner join SectionMaster sec on sec.Id=s.SectionId
 inner join SessionMaster sess on sess.Id=s.SessionId
 inner join ClassMaster cls on cls.Id=s.ClassId
 inner join KeyWordMaster km on km.KeyWordId= ft.FromMonthId
 inner join KeyWordMaster kms on kms.KeyWordId=ft.ToMonthId
 inner join KeyWordMaster kmss on kmss.KeyWordId=ft.PaymentModeId
 WHERE ft.Id = @FeeTransactionId and km.Name='Month' and kms.Name='Month' and kmss.Name='PaymentMode' AND ft.StatusId<>3
 END
GO
/****** Object:  StoredProcedure [dbo].[GetFeeTransactionByStudentIdForProfile]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetFeeTransactionByStudentIdForProfile]
@StudentId int,
@SessionId int,
@FromMonth int,
@ToMonth int
AS
BEGIN
 SELECT ft.Id,ft.StudentId,ft.TotalAmountDeposit,ft.FromMonthId,ft.ToMonthId,ft.IsSettled,ft.BalanceAmount,ft.Remark,ft.TransactionDate,ft.CreatedOn,ft.CreatedBy,ft.LastUpdatedBy,
 ft.LastUpdatedOn,ft.StatusId,
 ftdr.Total
 FROM  FeeTransaction ft  Inner JOIN 
   (SELECT  ftd.FeeTransactionId , SUM(fhm.Amount) AS Total
   FROM    FeeTransactionDetail ftd 
   Left Outer JOIN FeeHeadMonthMapping fhm 
   ON  ftd.FeeHeadMappingId = fhm.Id
   GROUP BY ftd.FeeTransactionId)  ftdr 
 ON ftdr.FeeTransactionId = ft.Id
 Left Outer JOIN StudentClassMapping s on s.StudentId = ft.StudentId
 WHERE ft.StudentId =@StudentId AND s.SessionId = @SessionId AND ft.StatusId<>3 AND ft.FromMonthId>=@FromMonth AND ft.ToMonthId<=@ToMonth
END
GO
/****** Object:  StoredProcedure [dbo].[GetFeeTransactionByClassSection]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Vishal Kumar
-- Exec Param : GetFeeTransactionByClassSection 6,6,25,8 
-- Description:All Students Last Month Fee Transaction
-- =============================================
CREATE Proc [dbo].[GetFeeTransactionByClassSection]
@SchoolId int ,
@SessionId int,
@ClassId int,
@SectionId int
AS
BEGIN
Declare @FineAmount decimal(18,2)
Declare @TotalAmount numeric(18,2)


  Select * into #temp From(
Select  Max(ft.Id) as StudentLastTranId,ft.StudentId
From  FeeTransaction ft 
Where  @SessionId = ft.SessionId
Group By ft.StudentId
) as res

Select * from (

Select ft1.Id,ft1.StudentId,ISNULL( ft1.TotalAmountDeposit,0) as TotalAmountDeposit,ISNULL( ft1.PreviousBalance,0) as PreviousBalance,ISNULL( ft1.TotalPayableAmount,0) as TotalPayableAmount ,ISNULL( ft1.TotalAmount,0) as TotalAmount ,ft1.IsSettled,ISNULL(
 ft1.BalanceAmount,0) as BalanceAmount ,ft1.Remark,ft1.TransactionDate,ft1.CreatedOn,ft1.CreatedBy,ft1.LastUpdatedBy,
ft1.LastUpdatedOn,ft1.StatusId,
km1.KeyWordName as FromMonth , km2.KeyWordName as ToMonth,
s.FirstName + ' ' + s.LastName  as StudentName
From FeeTransaction ft1
Inner join
#temp t ON Ft1.Id = t.StudentLastTranId
Left Outer JOIN Student s ON s.Id = ft1.StudentId
Left Outer JOIN StudentClassMapping scm ON scm.StudentId = s.Id
Left Outer Join  KeyWordMaster km1 ON km1.KeyWordId = ft1.FromMonthId and km1.Name = 'Month' 
Left Outer Join  KeyWordMaster km2 ON km2.KeyWordId = ft1.ToMonthId and km2.Name = 'Month'
Where @SchoolId = s.SchoolId AND @SessionId = scm.SessionId AND @ClassId = scm.ClassId AND @SectionId = scm.SectionId
) res1

left outer join

(
Select fine.TransactionId,sum(fine.FeeHeadAmount) as Discount from FineDiscountTransactionDetail fine Inner JOIN FeeHeadMaster fhm ON fine.FeeHeadId = fhm.Id
Where  fhm.FeeHeadTypeId = 3 --fine.TransactionId=64 and
Group By fine.TransactionId,fhm.FeeHeadTypeId
) res2

ON res2.TransactionId = res1.Id
left  outer join
(
Select fine.TransactionId,sum(fine.FeeHeadAmount) as Fine from FineDiscountTransactionDetail fine Inner JOIN FeeHeadMaster fhm ON fine.FeeHeadId = fhm.Id
Where  fhm.FeeHeadTypeId = 4 --fine.TransactionId=64 and
Group By fine.TransactionId,fhm.FeeHeadTypeId
) res3

ON res3.TransactionId = res1.Id

END
GO
/****** Object:  StoredProcedure [dbo].[GetFeeTransactioById]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[GetFeeTransactioById]
@Id int 
AS
BEGIN
 Declare @TotalAmount numeric(18,2)

  Select ft1.Id,ft1.StudentId,ft1.TotalAmountDeposit,ft1.IsSettled,ft1.BalanceAmount,ft1.Remark,ft1.TransactionDate,ft1.CreatedOn,ft1.CreatedBy,ft1.LastUpdatedBy,
 ft1.LastUpdatedOn,ft1.StatusId,
 a.Total,s.FirstName + ' '+ s.MiddileName + ' ' + s.LastName  as StudentName
 From FeeTransaction ft1
 Inner join 
 (Select ft.Id,ft.StudentId, ftdr.Total
 From  FeeTransaction ft  Inner JOIN 
 (Select  ftd.FeeTransactionId , sum(fhm.Amount) AS Total
 FROM    FeeTransactionDetail ftd 
 Left Outer JOIN FeeHeadMonthMapping fhm ON  ftd.FeeHeadMappingId = fhm.Id
 group by ftd.FeeTransactionId)  ftdr on ftdr.FeeTransactionId = ft.Id) as a ON Ft1.Id = a.Id
 Inner join 
 (Select StudentId , Max(Id)  as LastTransactionId From (
 Select ft.Id,ft.StudentId, ftdr.Total
 From  FeeTransaction ft  Inner JOIN 
 (Select  ftd.FeeTransactionId , sum(fhm.Amount) AS Total
 FROM    FeeTransactionDetail ftd 
 Left Outer JOIN FeeHeadMonthMapping fhm ON  ftd.FeeHeadMappingId = fhm.Id
 group by ftd.FeeTransactionId)  ftdr on ftdr.FeeTransactionId = ft.Id) AS Res
 Group By Res.StudentId) b
 ON b.LastTransactionId = ft1.Id
 Inner join Student s ON s.Id = ft1.StudentId
 Left Outer JOIN StudentClassMapping scm ON scm.StudentId = s.Id
 --Where @SchoolId = s.SchoolId AND @SessionId = scm.SessionId AND @ClassId = scm.ClassId AND @SectionId = scm.SectionId
END
GO
/****** Object:  StoredProcedure [dbo].[GetFeeStatus]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetFeeStatus]

@SessionId INT,
@SectionId INT,
@ClassId INT,
@FromMonthId INT,
@ToMonthId INT,
@PayTypeId INT
AS 

SELECT ft.Id, ft.StudentId, ft.TotalAmountDeposit, ft.IsSettled, ft.BalanceAmount, ft.Remark, ft.TransactionDate, ft.CreatedOn, 
ft.CreatedBy, ft.LastUpdatedBy, ft.LastUpdatedOn, ft.StatusId, ft.FromMonthId, ft.ToMonthId

FROM dbo.FeeTransaction ft INNER JOIN FeeTransactionDetail ftd ON ftd.FeeTransactionId = ft.Id
INNER JOIN FeeHeadMonthMapping fhm ON fhm.Id = ftd.FeeHeadMappingId INNER JOIN FeeHeadMaster ON FeeHeadMaster.Id = fhm.FeeHeadId 
INNER JOIN Student s ON s.Id = ft.StudentId
INNER JOIN StudentClassMapping scm ON scm.StudentId = s.Id

WHERE
((@SessionId = 0) OR (fhm.SessionId = @SessionId)) AND 
((@ClassId = 0) OR (scm.ClassId = @ClassId)) AND 
((@SectionId = 0) OR  (scm.SectionId = @SectionId)) AND
--CASE @PayTypeId WHEN  1 THEN ft.IsSettled = 'true' 
 ft.FromMonthId >= @FromMonthId  AND ft.ToMonthId <= @ToMonthId AND
 ((ft.IsSettled =  CASE  WHEN  @PayTypeId = 1 
 THEN   'true'     -- fetch setteled and non setteled
 WHEN @PayTypeId =2 then 'false'  END) AND  ft.BalanceAmount > 0)
GO
/****** Object:  StoredProcedure [dbo].[GetFeeHeadDetial]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[GetFeeHeadDetial] 6,6,25,1,3,2,19  
CREATE Proc [dbo].[GetFeeHeadDetial]  
@SchoolId int,  
@SessionId int,  
@ClassId int ,  
@FromMonth int,  
@ToMonth int,  
@HeadTypeId int,  
@StudentId int  
AS  
BEGIN  
 Select   
  fhm.Id as FeeHeadMasterId,fhm.Name,fhm.Description,fhm.IsDiscount,fhm.FeeHeadTypeId,fhm.SchoolId,fhm.CreatedOn,fhm.CreatedBy,fhm.StatusId,fhm.LastModifiedOn,fhm.LastModifiedBy,  
  fhmm.Id as FeeHeadMappingId,fhmm.FeeHeadId,fhmm.ClassId,fhmm.SessionId,fhmm.MonthId,fhmm.Amount ,
  km.KeyWordName as FeeMonthName,  
  kh.KeyWordName as FeeHeadTypeName,  
  cm.Name as ClassName , sm.Name as SessionName  
 From   
  [dbo].[FeeHeadMaster] fhm   
  INNER JOIN [dbo].[FeeHeadMonthMapping] fhmm ON fhmm.FeeHeadId = fhm.Id  
  Left OUTER JOIN KeyWordMaster km ON km.KeyWordId = fhmm.MonthId  AND km.Name='Month'
  Left OUTER JOIN KeyWordMaster kh ON kh.KeyWordId = fhm.FeeHeadTypeId AND kh.Name = 'FeeHeadType'  
  Left OUTER JOIN ClassMaster cm ON cm.Id = fhmm.ClassId  
  Left OUTER JOIN SessionMaster sm ON sm.Id = fhmm.SessionId  
 Where fhm.StatusId = 1  AND fhm.SchoolId = @SchoolId AND fhmm.SessionId  = @SessionId AND fhmm.ClassId = @ClassId AND fhm.FeeHeadTypeId = @HeadTypeId  
 AND  fhmm.MonthId >= @FromMonth   and fhmm.MonthId <=  @ToMonth  
 AND  
 fhmm.Id Not In (  
 Select id from FeeHeadMonthMapping Where FeeHeadMonthMapping.Id in (  
select fd.FeeHeadMappingId  from feetransaction f inner join FeeTransactionDetail fd ON fd.FeeTransactionId = f.Id 
inner join FeeHeadMonthMapping fhmm1 on fd.FeeHeadMappingId = fhmm1.Id Where fhmm1.SessionId = @SessionId 
and StudentId = @StudentId --and ((1 between f.FromMonthId and f.ToMonthId) Or  (7 between f.FromMonthId and f.ToMonthId))
)  )
 END
GO
/****** Object:  StoredProcedure [dbo].[GetUpcomingBirthDay]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetUpcomingBirthDay]
@SchoolId int
--@ClassId int,
--@SectionId int
as
begin
select
   s.FirstName as StudentName,c.Name as ClassName,sec.Name as SectionName ,s.DOB as BirthDate
    from Student s  inner join StudentClassMapping scm on s.Id=scm.StudentId
    left outer join ClassMaster c on c.Id=scm.ClassId
    left outer join SectionMaster sec on sec.Id=scm.SectionId
     where s.SchoolId=@SchoolId  and  MONTH(s.DOB)=MONTH(GETDATE()) and MONTH(s.DOB)=MONTH(DATEADD(DD,3,GETDATE())) and DAY(s.DOB)>DAY(GETDATE()) and DAY(s.DOB)<DAY(DATEADD(dd,3,GETDATE()))
              
end
GO
/****** Object:  StoredProcedure [dbo].[GetDeleteStudents]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetDeleteStudents]
@SessionId int,
@ClassId int,
@SectionId int,
@SchoolId int
AS
Select s.Id as StudentId,s.FirstName as StudentName,s.MiddileName,s.LastName,s.TitleId,s.Email,s.StudentMobile as StudentContactNumber,scm.StudentCode as StudentRollNumber,
  s.MotherName,s.FatherName,s.CorrCityId,s.GenderId,s.SchoolId,s.Uid,
  s.DOJ,s.DOB,s.DOE,s.StatusId as StudentStatusId,s.CreatedOn,s.CreatedBy,s.LastUpdatedOn,s.LastUpdatedBy,s.ImagePath,
  s.RelegionId,s.CategoryId,s.ReasonForExit,s.NationlityId,
  kg.KeyWordName as StudentGender, kn.KeyWordName as NationlityName, kr.KeyWordName as  RelegionName,
  kt.KeyWordName as TitleName ,kc.KeyWordName as StudentCategory
 From Student s inner join StudentClassMapping scm on s.Id=scm.StudentId
  left outer join KeyWordMaster kg ON kg.KeyWordId = s.GenderId AND kg.Name = 'Gender'
left outer join KeyWordMaster kn ON kn.KeyWordId = s.NationlityId AND kn.Name = 'Nationlity'
 left outer join KeyWordMaster kr ON kr.KeyWordId = s.RelegionId AND kr.Name = 'Relegion'
 left outer  join KeyWordMaster kc ON kc.KeyWordId = s.CategoryId AND kc.Name = 'Category'
 left outer join KeyWordMaster kt ON kt.KeyWordId = s.TitleId AND kt.Name = 'Title'
Where s.SchoolId = @SchoolId  and scm.ClassId=@ClassId and scm.SectionId=@SectionId and scm.SessionId=@SessionId AND s.StatusId =3
GO
/****** Object:  StoredProcedure [dbo].[GetdataForGenderWiseDrilldown]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROC [dbo].[GetdataForGenderWiseDrilldown]
@SessionId int,
@SchoolId int,
@GenderId int
AS
BEGIN
Select cm.Name as ClassName, k.KeyWordName AS GenderName ,Count(s.Id) as CountNo from Student s 
Inner join StudentClassMapping scm On s.Id = scm.StudentId AND SessionId = @SessionId
Inner join ClassMaster cm ON cm.Id = scm.ClassId
Left outer join KeyWordMaster k ON s.GenderId = k.KeyWordId AND k.Name = 'Gender'
Where s.SchoolId = @SchoolId AND s.GenderId is not null And s.GenderId = @GenderId
Group by GenderId,k.KeyWordName,cm.Name
END
GO
/****** Object:  StoredProcedure [dbo].[GetActiveStudents]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetActiveStudents]
@SessionId int,
@ClassId int,
@SectionId int,
@SchoolId int
AS
Select s.Id as StudentId,s.FirstName as StudentName,s.MiddileName,s.LastName,s.TitleId,s.Email,s.StudentMobile as StudentContactNumber,scm.StudentCode as StudentRollNumber,
  s.MotherName,s.FatherName,s.CorrCityId,s.GenderId,s.SchoolId,s.Uid,
  s.DOJ,s.DOB,s.DOE,s.StatusId as StudentStatusId,s.CreatedOn,s.CreatedBy,s.LastUpdatedOn,s.LastUpdatedBy,s.ImagePath,
  s.RelegionId,s.CategoryId,s.ReasonForExit,s.NationlityId,
  kg.KeyWordName as StudentGender, kn.KeyWordName as NationlityName, kr.KeyWordName as  RelegionName,
  kt.KeyWordName as TitleName ,kc.KeyWordName as StudentCategory
 From Student s inner join StudentClassMapping scm on s.Id=scm.StudentId
  Left outer join KeyWordMaster kg ON kg.KeyWordId = s.GenderId AND kg.Name = 'Gender'
left outer join KeyWordMaster kn ON kn.KeyWordId = s.NationlityId AND kn.Name = 'Nationlity'
 left outer join KeyWordMaster kr ON kr.KeyWordId = s.RelegionId AND kr.Name = 'Relegion'
 left outer  join KeyWordMaster kc ON kc.KeyWordId = s.CategoryId AND kc.Name = 'Category'
 left outer join KeyWordMaster kt ON kt.KeyWordId = s.TitleId AND kt.Name = 'Title'
Where s.SchoolId = @SchoolId  and scm.ClassId=@ClassId and scm.SectionId=@SectionId and scm.SessionId=@SessionId AND s.StatusId =1
GO
/****** Object:  StoredProcedure [dbo].[FeeTranPrintDetail]    Script Date: 07/01/2018 16:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[FeeTranPrintDetail]
@FeeTranId int
as
Begin
Begin Try
--selct * from 
select fhm.id ,fhm.name ,SUM(fhmm.Amount) Amount,fhm.feeheadtypeid
--ftd.FeeHeadMappingId,fhmm.*,fhm.Name 
from FeeTransactionDetail ftd 
Inner join FeeHeadMonthMapping fhmm ON fhmm.Id = ftd.FeeHeadMappingId
Inner join FeeHeadMaster fhm ON fhmm.feeheadid = fhm.id  
--Inner join finediscounttransactiondetail fdtd on fhm.id = fdtd.feeheadid and fdtd.transactionid = 53
Where ftd.FeeTransactionId = @FeeTranId 
Group By fhm.id ,fhm.name,fhm.feeheadtypeid
Union
select fhm.id ,fhm.name ,SUM( fdtd.FeeHeadAmount) Amount,fhm.feeheadtypeid
--ftd.FeeHeadMappingId,fhmm.*,fhm.Name 
from finediscounttransactiondetail fdtd 
--Inner join FeeHeadMonthMapping fhmm ON fhmm.Id = fdtd.FeeMappingId
Inner join FeeHeadMaster fhm ON fdtd.FeeheadId = fhm.id
--Inner join finediscounttransactiondetail fdtd on fhm.id = fdtd.feeheadid and fdtd.transactionid = 53
Where fdtd.TransactionId = @FeeTranId 
Group By fhm.id ,fhm.name,fhm.feeheadtypeid

	--Select ft.[StudentId],ft.[TotalAmountDeposit],ft.[IsSettled],ft.[BalanceAmount],ft.[Remark],
	--ft.[TransactionDate],ft.[CreatedOn],ft.[CreatedBy],ft.[PaymentModeId],ft.[FromMonthId],ft.[ToMonthId],
	--ft.[LastUpdatedBy],ft.[LastUpdatedOn],ft.[StatusId],
	--ftd.FeeTransactionId , ftd.FeeHeadMappingId,
	--fhmm.MonthId
	--from FeeTransaction ft inner join feetransactiondetail ftd on ft.id = ftd.FeeTransactionId
	--Inner join finediscounttransactiondetail fdtd ON  ft.id = fdtd.TransactionId
	--Inner join FeeHeadMonthMapping fhmm on fhmm.Id = ftd.FeeHeadMappingId
	--Where ft.id = 53
End Try
Begin Catch
End Catch
End

--select * from finediscounttransactiondetail
--select * from FeeHeadMaster
--sp_help feetransactiondetail

--select * from finediscounttransactiondetail
--delete From finediscounttransactiondetail Where transactionId = 54
--delete  from feetransaction where ID = 54
GO
/****** Object:  Default [DF_SchoolMaster_ParentSchoolId]    Script Date: 07/01/2018 16:25:48 ******/
ALTER TABLE [dbo].[SchoolMaster] ADD  CONSTRAINT [DF_SchoolMaster_ParentSchoolId]  DEFAULT ((0)) FOR [ParentSchoolId]
GO
/****** Object:  Default [DF_FeeHeadMaster_IsDiscount]    Script Date: 07/01/2018 16:25:50 ******/
ALTER TABLE [dbo].[FeeHeadMaster] ADD  CONSTRAINT [DF_FeeHeadMaster_IsDiscount]  DEFAULT ((0)) FOR [IsDiscount]
GO
/****** Object:  ForeignKey [FK_OnlineTestSubjectMaster_OnlineTestSubjectMaster]    Script Date: 07/01/2018 16:25:48 ******/
ALTER TABLE [dbo].[OnlineTestSubjectMaster]  WITH CHECK ADD  CONSTRAINT [FK_OnlineTestSubjectMaster_OnlineTestSubjectMaster] FOREIGN KEY([Id])
REFERENCES [dbo].[OnlineTestSubjectMaster] ([Id])
GO
ALTER TABLE [dbo].[OnlineTestSubjectMaster] CHECK CONSTRAINT [FK_OnlineTestSubjectMaster_OnlineTestSubjectMaster]
GO
/****** Object:  ForeignKey [FK_Inventory_SchoolMaster]    Script Date: 07/01/2018 16:25:48 ******/
ALTER TABLE [dbo].[Inventory]  WITH CHECK ADD  CONSTRAINT [FK_Inventory_SchoolMaster] FOREIGN KEY([SchoolId])
REFERENCES [dbo].[SchoolMaster] ([Id])
GO
ALTER TABLE [dbo].[Inventory] CHECK CONSTRAINT [FK_Inventory_SchoolMaster]
GO
/****** Object:  ForeignKey [FK_Inventory_SessionMaster]    Script Date: 07/01/2018 16:25:48 ******/
ALTER TABLE [dbo].[Inventory]  WITH CHECK ADD  CONSTRAINT [FK_Inventory_SessionMaster] FOREIGN KEY([SessionId])
REFERENCES [dbo].[SessionMaster] ([Id])
GO
ALTER TABLE [dbo].[Inventory] CHECK CONSTRAINT [FK_Inventory_SessionMaster]
GO
/****** Object:  ForeignKey [FK_HolidayMaster_SchoolMaster]    Script Date: 07/01/2018 16:25:48 ******/
ALTER TABLE [dbo].[HolidayMaster]  WITH CHECK ADD  CONSTRAINT [FK_HolidayMaster_SchoolMaster] FOREIGN KEY([SchoolId])
REFERENCES [dbo].[SchoolMaster] ([Id])
GO
ALTER TABLE [dbo].[HolidayMaster] CHECK CONSTRAINT [FK_HolidayMaster_SchoolMaster]
GO
/****** Object:  ForeignKey [FK_HolidayMaster_SessionMaster]    Script Date: 07/01/2018 16:25:48 ******/
ALTER TABLE [dbo].[HolidayMaster]  WITH CHECK ADD  CONSTRAINT [FK_HolidayMaster_SessionMaster] FOREIGN KEY([SessionId])
REFERENCES [dbo].[SessionMaster] ([Id])
GO
ALTER TABLE [dbo].[HolidayMaster] CHECK CONSTRAINT [FK_HolidayMaster_SessionMaster]
GO
/****** Object:  ForeignKey [FK_ClassMaster_SchoolMaster]    Script Date: 07/01/2018 16:25:50 ******/
ALTER TABLE [dbo].[ClassMaster]  WITH CHECK ADD  CONSTRAINT [FK_ClassMaster_SchoolMaster] FOREIGN KEY([SchoolId])
REFERENCES [dbo].[SchoolMaster] ([Id])
GO
ALTER TABLE [dbo].[ClassMaster] CHECK CONSTRAINT [FK_ClassMaster_SchoolMaster]
GO
/****** Object:  ForeignKey [FK_CertificateTemplateSchoolMapping_CertificateTemplateMaster]    Script Date: 07/01/2018 16:25:50 ******/
ALTER TABLE [dbo].[CertificateTemplateSchoolMapping]  WITH CHECK ADD  CONSTRAINT [FK_CertificateTemplateSchoolMapping_CertificateTemplateMaster] FOREIGN KEY([CertificateTemplateId])
REFERENCES [dbo].[CertificateTemplateMaster] ([Id])
GO
ALTER TABLE [dbo].[CertificateTemplateSchoolMapping] CHECK CONSTRAINT [FK_CertificateTemplateSchoolMapping_CertificateTemplateMaster]
GO
/****** Object:  ForeignKey [FK_AttendanceMaster_SchoolMaster]    Script Date: 07/01/2018 16:25:50 ******/
ALTER TABLE [dbo].[AttendanceMaster]  WITH CHECK ADD  CONSTRAINT [FK_AttendanceMaster_SchoolMaster] FOREIGN KEY([SchoolId])
REFERENCES [dbo].[SchoolMaster] ([Id])
GO
ALTER TABLE [dbo].[AttendanceMaster] CHECK CONSTRAINT [FK_AttendanceMaster_SchoolMaster]
GO
/****** Object:  ForeignKey [FK_AttendanceMaster_SessionMaster]    Script Date: 07/01/2018 16:25:50 ******/
ALTER TABLE [dbo].[AttendanceMaster]  WITH CHECK ADD  CONSTRAINT [FK_AttendanceMaster_SessionMaster] FOREIGN KEY([SessionId])
REFERENCES [dbo].[SessionMaster] ([Id])
GO
ALTER TABLE [dbo].[AttendanceMaster] CHECK CONSTRAINT [FK_AttendanceMaster_SessionMaster]
GO
/****** Object:  ForeignKey [FK_AttendanceMaster_Student]    Script Date: 07/01/2018 16:25:50 ******/
ALTER TABLE [dbo].[AttendanceMaster]  WITH CHECK ADD  CONSTRAINT [FK_AttendanceMaster_Student] FOREIGN KEY([StudentId])
REFERENCES [dbo].[Student] ([Id])
GO
ALTER TABLE [dbo].[AttendanceMaster] CHECK CONSTRAINT [FK_AttendanceMaster_Student]
GO
/****** Object:  ForeignKey [FK_FeeTransaction_Student]    Script Date: 07/01/2018 16:25:50 ******/
ALTER TABLE [dbo].[FeeTransaction]  WITH CHECK ADD  CONSTRAINT [FK_FeeTransaction_Student] FOREIGN KEY([StudentId])
REFERENCES [dbo].[Student] ([Id])
GO
ALTER TABLE [dbo].[FeeTransaction] CHECK CONSTRAINT [FK_FeeTransaction_Student]
GO
/****** Object:  ForeignKey [FK_FeeHeadMaster_SchoolMaster]    Script Date: 07/01/2018 16:25:50 ******/
ALTER TABLE [dbo].[FeeHeadMaster]  WITH CHECK ADD  CONSTRAINT [FK_FeeHeadMaster_SchoolMaster] FOREIGN KEY([SchoolId])
REFERENCES [dbo].[SchoolMaster] ([Id])
GO
ALTER TABLE [dbo].[FeeHeadMaster] CHECK CONSTRAINT [FK_FeeHeadMaster_SchoolMaster]
GO
/****** Object:  ForeignKey [FK_MessageAutomateMapping_AutomateMessage]    Script Date: 07/01/2018 16:25:50 ******/
ALTER TABLE [dbo].[MessageAutomateMapping]  WITH CHECK ADD  CONSTRAINT [FK_MessageAutomateMapping_AutomateMessage] FOREIGN KEY([MessageAutomateId])
REFERENCES [dbo].[AutomateMessage] ([Id])
GO
ALTER TABLE [dbo].[MessageAutomateMapping] CHECK CONSTRAINT [FK_MessageAutomateMapping_AutomateMessage]
GO
/****** Object:  ForeignKey [FK_MessageAutomateMapping_TemplateMaster]    Script Date: 07/01/2018 16:25:50 ******/
ALTER TABLE [dbo].[MessageAutomateMapping]  WITH CHECK ADD  CONSTRAINT [FK_MessageAutomateMapping_TemplateMaster] FOREIGN KEY([TemplateId])
REFERENCES [dbo].[TemplateMaster] ([Id])
GO
ALTER TABLE [dbo].[MessageAutomateMapping] CHECK CONSTRAINT [FK_MessageAutomateMapping_TemplateMaster]
GO
/****** Object:  ForeignKey [FK_ApplicationLog_LogSource]    Script Date: 07/01/2018 16:25:50 ******/
ALTER TABLE [dbo].[ApplicationLog]  WITH CHECK ADD  CONSTRAINT [FK_ApplicationLog_LogSource] FOREIGN KEY([LogSourceID])
REFERENCES [dbo].[LogSource] ([ID])
GO
ALTER TABLE [dbo].[ApplicationLog] CHECK CONSTRAINT [FK_ApplicationLog_LogSource]
GO
/****** Object:  ForeignKey [FK_AmazonSettings_UserMaster]    Script Date: 07/01/2018 16:25:50 ******/
ALTER TABLE [dbo].[AmazonSettings]  WITH CHECK ADD  CONSTRAINT [FK_AmazonSettings_UserMaster] FOREIGN KEY([ModifiedById])
REFERENCES [dbo].[UserMaster] ([Id])
GO
ALTER TABLE [dbo].[AmazonSettings] CHECK CONSTRAINT [FK_AmazonSettings_UserMaster]
GO
/****** Object:  ForeignKey [FK_SchoolPlanMapping_PlanMaster]    Script Date: 07/01/2018 16:25:50 ******/
ALTER TABLE [dbo].[SchoolPlanMapping]  WITH CHECK ADD  CONSTRAINT [FK_SchoolPlanMapping_PlanMaster] FOREIGN KEY([PlanId])
REFERENCES [dbo].[PlanMaster] ([Id])
GO
ALTER TABLE [dbo].[SchoolPlanMapping] CHECK CONSTRAINT [FK_SchoolPlanMapping_PlanMaster]
GO
/****** Object:  ForeignKey [FK_SchoolPlanMapping_SchoolMaster]    Script Date: 07/01/2018 16:25:50 ******/
ALTER TABLE [dbo].[SchoolPlanMapping]  WITH CHECK ADD  CONSTRAINT [FK_SchoolPlanMapping_SchoolMaster] FOREIGN KEY([SchoolId])
REFERENCES [dbo].[SchoolMaster] ([Id])
GO
ALTER TABLE [dbo].[SchoolPlanMapping] CHECK CONSTRAINT [FK_SchoolPlanMapping_SchoolMaster]
GO
/****** Object:  ForeignKey [FK_SubjectMaster_SchoolMaster]    Script Date: 07/01/2018 16:25:50 ******/
ALTER TABLE [dbo].[SubjectMaster]  WITH CHECK ADD  CONSTRAINT [FK_SubjectMaster_SchoolMaster] FOREIGN KEY([SchoolId])
REFERENCES [dbo].[SchoolMaster] ([Id])
GO
ALTER TABLE [dbo].[SubjectMaster] CHECK CONSTRAINT [FK_SubjectMaster_SchoolMaster]
GO
/****** Object:  ForeignKey [FK_SubjectClassMapping_ClassMaster]    Script Date: 07/01/2018 16:25:51 ******/
ALTER TABLE [dbo].[SubjectClassMapping]  WITH CHECK ADD  CONSTRAINT [FK_SubjectClassMapping_ClassMaster] FOREIGN KEY([ClassId])
REFERENCES [dbo].[ClassMaster] ([Id])
GO
ALTER TABLE [dbo].[SubjectClassMapping] CHECK CONSTRAINT [FK_SubjectClassMapping_ClassMaster]
GO
/****** Object:  ForeignKey [FK_SubjectClassMapping_SubjectMaster]    Script Date: 07/01/2018 16:25:51 ******/
ALTER TABLE [dbo].[SubjectClassMapping]  WITH CHECK ADD  CONSTRAINT [FK_SubjectClassMapping_SubjectMaster] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[SubjectMaster] ([Id])
GO
ALTER TABLE [dbo].[SubjectClassMapping] CHECK CONSTRAINT [FK_SubjectClassMapping_SubjectMaster]
GO
/****** Object:  ForeignKey [FK_SectionMaster_ClassMaster]    Script Date: 07/01/2018 16:25:51 ******/
ALTER TABLE [dbo].[SectionMaster]  WITH CHECK ADD  CONSTRAINT [FK_SectionMaster_ClassMaster] FOREIGN KEY([ClassId])
REFERENCES [dbo].[ClassMaster] ([Id])
GO
ALTER TABLE [dbo].[SectionMaster] CHECK CONSTRAINT [FK_SectionMaster_ClassMaster]
GO
/****** Object:  ForeignKey [FK_ExamSubjectMapping_ExamMaster]    Script Date: 07/01/2018 16:25:51 ******/
ALTER TABLE [dbo].[ExamSubjectMapping]  WITH CHECK ADD  CONSTRAINT [FK_ExamSubjectMapping_ExamMaster] FOREIGN KEY([ExamId])
REFERENCES [dbo].[ExamMaster] ([Id])
GO
ALTER TABLE [dbo].[ExamSubjectMapping] CHECK CONSTRAINT [FK_ExamSubjectMapping_ExamMaster]
GO
/****** Object:  ForeignKey [FK_ExamSubjectMapping_SubjectMaster1]    Script Date: 07/01/2018 16:25:51 ******/
ALTER TABLE [dbo].[ExamSubjectMapping]  WITH CHECK ADD  CONSTRAINT [FK_ExamSubjectMapping_SubjectMaster1] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[SubjectMaster] ([Id])
GO
ALTER TABLE [dbo].[ExamSubjectMapping] CHECK CONSTRAINT [FK_ExamSubjectMapping_SubjectMaster1]
GO
/****** Object:  ForeignKey [FK_FeeHeadMonthMapping_FeeHeadMaster]    Script Date: 07/01/2018 16:25:51 ******/
ALTER TABLE [dbo].[FeeHeadMonthMapping]  WITH CHECK ADD  CONSTRAINT [FK_FeeHeadMonthMapping_FeeHeadMaster] FOREIGN KEY([FeeHeadId])
REFERENCES [dbo].[FeeHeadMaster] ([Id])
GO
ALTER TABLE [dbo].[FeeHeadMonthMapping] CHECK CONSTRAINT [FK_FeeHeadMonthMapping_FeeHeadMaster]
GO
/****** Object:  ForeignKey [FK_FineDiscountTransactionDetail_FeeHeadMaster]    Script Date: 07/01/2018 16:25:51 ******/
ALTER TABLE [dbo].[FineDiscountTransactionDetail]  WITH CHECK ADD  CONSTRAINT [FK_FineDiscountTransactionDetail_FeeHeadMaster] FOREIGN KEY([FeeHeadId])
REFERENCES [dbo].[FeeHeadMaster] ([Id])
GO
ALTER TABLE [dbo].[FineDiscountTransactionDetail] CHECK CONSTRAINT [FK_FineDiscountTransactionDetail_FeeHeadMaster]
GO
/****** Object:  ForeignKey [FK_FineDiscountTransactionDetail_FeeTransaction]    Script Date: 07/01/2018 16:25:51 ******/
ALTER TABLE [dbo].[FineDiscountTransactionDetail]  WITH CHECK ADD  CONSTRAINT [FK_FineDiscountTransactionDetail_FeeTransaction] FOREIGN KEY([TransactionId])
REFERENCES [dbo].[FeeTransaction] ([Id])
GO
ALTER TABLE [dbo].[FineDiscountTransactionDetail] CHECK CONSTRAINT [FK_FineDiscountTransactionDetail_FeeTransaction]
GO
/****** Object:  ForeignKey [FK_FeeTransactionDetail_FeeHeadMonthMapping]    Script Date: 07/01/2018 16:25:51 ******/
ALTER TABLE [dbo].[FeeTransactionDetail]  WITH CHECK ADD  CONSTRAINT [FK_FeeTransactionDetail_FeeHeadMonthMapping] FOREIGN KEY([FeeHeadMappingId])
REFERENCES [dbo].[FeeHeadMonthMapping] ([Id])
GO
ALTER TABLE [dbo].[FeeTransactionDetail] CHECK CONSTRAINT [FK_FeeTransactionDetail_FeeHeadMonthMapping]
GO
/****** Object:  ForeignKey [FK_FeeTransactionDetail_FeeTransaction]    Script Date: 07/01/2018 16:25:51 ******/
ALTER TABLE [dbo].[FeeTransactionDetail]  WITH CHECK ADD  CONSTRAINT [FK_FeeTransactionDetail_FeeTransaction] FOREIGN KEY([FeeTransactionId])
REFERENCES [dbo].[FeeTransaction] ([Id])
GO
ALTER TABLE [dbo].[FeeTransactionDetail] CHECK CONSTRAINT [FK_FeeTransactionDetail_FeeTransaction]
GO
/****** Object:  ForeignKey [FK_StudentExamMapping_ClassMaster]    Script Date: 07/01/2018 16:25:51 ******/
ALTER TABLE [dbo].[StudentExamMapping]  WITH CHECK ADD  CONSTRAINT [FK_StudentExamMapping_ClassMaster] FOREIGN KEY([ClassId])
REFERENCES [dbo].[ClassMaster] ([Id])
GO
ALTER TABLE [dbo].[StudentExamMapping] CHECK CONSTRAINT [FK_StudentExamMapping_ClassMaster]
GO
/****** Object:  ForeignKey [FK_StudentExamMapping_ExamSubjectMapping]    Script Date: 07/01/2018 16:25:51 ******/
ALTER TABLE [dbo].[StudentExamMapping]  WITH CHECK ADD  CONSTRAINT [FK_StudentExamMapping_ExamSubjectMapping] FOREIGN KEY([ExamSubjectMappingId])
REFERENCES [dbo].[ExamSubjectMapping] ([Id])
GO
ALTER TABLE [dbo].[StudentExamMapping] CHECK CONSTRAINT [FK_StudentExamMapping_ExamSubjectMapping]
GO
/****** Object:  ForeignKey [FK_StudentExamMapping_SectionMaster]    Script Date: 07/01/2018 16:25:51 ******/
ALTER TABLE [dbo].[StudentExamMapping]  WITH CHECK ADD  CONSTRAINT [FK_StudentExamMapping_SectionMaster] FOREIGN KEY([SectionId])
REFERENCES [dbo].[SectionMaster] ([Id])
GO
ALTER TABLE [dbo].[StudentExamMapping] CHECK CONSTRAINT [FK_StudentExamMapping_SectionMaster]
GO
/****** Object:  ForeignKey [FK_StudentExamMapping_Student]    Script Date: 07/01/2018 16:25:51 ******/
ALTER TABLE [dbo].[StudentExamMapping]  WITH CHECK ADD  CONSTRAINT [FK_StudentExamMapping_Student] FOREIGN KEY([StudentId])
REFERENCES [dbo].[Student] ([Id])
GO
ALTER TABLE [dbo].[StudentExamMapping] CHECK CONSTRAINT [FK_StudentExamMapping_Student]
GO
/****** Object:  ForeignKey [FK_StudentClassMapping_ClassMaster]    Script Date: 07/01/2018 16:25:51 ******/
ALTER TABLE [dbo].[StudentClassMapping]  WITH CHECK ADD  CONSTRAINT [FK_StudentClassMapping_ClassMaster] FOREIGN KEY([ClassId])
REFERENCES [dbo].[ClassMaster] ([Id])
GO
ALTER TABLE [dbo].[StudentClassMapping] CHECK CONSTRAINT [FK_StudentClassMapping_ClassMaster]
GO
/****** Object:  ForeignKey [FK_StudentClassMapping_SectionMaster]    Script Date: 07/01/2018 16:25:51 ******/
ALTER TABLE [dbo].[StudentClassMapping]  WITH CHECK ADD  CONSTRAINT [FK_StudentClassMapping_SectionMaster] FOREIGN KEY([SectionId])
REFERENCES [dbo].[SectionMaster] ([Id])
GO
ALTER TABLE [dbo].[StudentClassMapping] CHECK CONSTRAINT [FK_StudentClassMapping_SectionMaster]
GO
/****** Object:  ForeignKey [FK_StudentClassMapping_SessionMaster]    Script Date: 07/01/2018 16:25:51 ******/
ALTER TABLE [dbo].[StudentClassMapping]  WITH CHECK ADD  CONSTRAINT [FK_StudentClassMapping_SessionMaster] FOREIGN KEY([StudentId])
REFERENCES [dbo].[Student] ([Id])
GO
ALTER TABLE [dbo].[StudentClassMapping] CHECK CONSTRAINT [FK_StudentClassMapping_SessionMaster]
GO
/****** Object:  ForeignKey [FK_StudentClassMapping_Student]    Script Date: 07/01/2018 16:25:51 ******/
ALTER TABLE [dbo].[StudentClassMapping]  WITH CHECK ADD  CONSTRAINT [FK_StudentClassMapping_Student] FOREIGN KEY([StudentId])
REFERENCES [dbo].[Student] ([Id])
GO
ALTER TABLE [dbo].[StudentClassMapping] CHECK CONSTRAINT [FK_StudentClassMapping_Student]
GO
/****** Object:  ForeignKey [FK_TeacherSubjectMapping_ClassMaster]    Script Date: 07/01/2018 16:25:51 ******/
ALTER TABLE [dbo].[TeacherSubjectMapping]  WITH CHECK ADD  CONSTRAINT [FK_TeacherSubjectMapping_ClassMaster] FOREIGN KEY([TeacherId])
REFERENCES [dbo].[TeacherMaster] ([Id])
GO
ALTER TABLE [dbo].[TeacherSubjectMapping] CHECK CONSTRAINT [FK_TeacherSubjectMapping_ClassMaster]
GO
/****** Object:  ForeignKey [FK_TeacherSubjectMapping_SectionMaster]    Script Date: 07/01/2018 16:25:51 ******/
ALTER TABLE [dbo].[TeacherSubjectMapping]  WITH CHECK ADD  CONSTRAINT [FK_TeacherSubjectMapping_SectionMaster] FOREIGN KEY([SectionId])
REFERENCES [dbo].[SectionMaster] ([Id])
GO
ALTER TABLE [dbo].[TeacherSubjectMapping] CHECK CONSTRAINT [FK_TeacherSubjectMapping_SectionMaster]
GO
/****** Object:  ForeignKey [FK_TeacherSubjectMapping_SubjectMaster]    Script Date: 07/01/2018 16:25:51 ******/
ALTER TABLE [dbo].[TeacherSubjectMapping]  WITH CHECK ADD  CONSTRAINT [FK_TeacherSubjectMapping_SubjectMaster] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[SubjectMaster] ([Id])
GO
ALTER TABLE [dbo].[TeacherSubjectMapping] CHECK CONSTRAINT [FK_TeacherSubjectMapping_SubjectMaster]
GO
/****** Object:  ForeignKey [FK_TeacherSubjectMapping_TeacherSubjectMapping]    Script Date: 07/01/2018 16:25:51 ******/
ALTER TABLE [dbo].[TeacherSubjectMapping]  WITH CHECK ADD  CONSTRAINT [FK_TeacherSubjectMapping_TeacherSubjectMapping] FOREIGN KEY([Id])
REFERENCES [dbo].[TeacherSubjectMapping] ([Id])
GO
ALTER TABLE [dbo].[TeacherSubjectMapping] CHECK CONSTRAINT [FK_TeacherSubjectMapping_TeacherSubjectMapping]
GO
