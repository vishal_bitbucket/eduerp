USE [EduErp]
GO
/****** Object:  StoredProcedure [dbo].[GetdataForGenderWiseDrilldown]    Script Date: 04/05/2018 19:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROC [dbo].[GetdataForGenderWiseDrilldown]
@SessionId int,
@SchoolId int,
@GenderId int
AS
BEGIN
Select cm.Name as ClassName, k.KeyWordName AS GenderName ,Count(s.Id) as CountNo from Student s 
Inner join StudentClassMapping scm On s.Id = scm.StudentId AND SessionId = @SessionId
Inner join ClassMaster cm ON cm.Id = scm.ClassId
Left outer join KeyWordMaster k ON s.GenderId = k.KeyWordId AND k.Name = 'Gender'
Where s.SchoolId = @SchoolId AND s.GenderId is not null And s.GenderId = @GenderId
Group by GenderId,k.KeyWordName,cm.Name
END