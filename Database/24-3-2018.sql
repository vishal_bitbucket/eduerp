USE [EduErp]
GO
/****** Object:  StoredProcedure [dbo].[GetStudentClassChart]    Script Date: 03/24/2018 19:22:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetStudentClassChart]
@SchoolId INT

AS BEGIN

SELECT        ClassMaster.Id, ClassMaster.Name, COUNT(StudentClassMapping.StudentId) AS TotalStudents
FROM        Student LEFT Outer join StudentClassMapping on Student.Id=StudentClassMapping.StudentId Left Outer JOIN
                         ClassMaster ON StudentClassMapping.ClassId = ClassMaster.Id
						 WHERE ClassMaster.SchoolId = @SchoolId AND ClassMaster.Status=1 AND Student.StatusId=1 
						 GROUP BY ClassMaster.Name, ClassMaster.Id						 

UNION

SELECT Id, Name, 0 AS TotalStudents FROM ClassMaster WHERE ClassMaster.SchoolId = @SchoolId AND ClassMaster.Status=1 AND Id NOT IN
 (SELECT        ClassMaster.Id
FROM             Student LEFT Outer join StudentClassMapping on Student.Id=StudentClassMapping.StudentId Left Outer JOIN
                         ClassMaster ON StudentClassMapping.ClassId = ClassMaster.Id
						 WHERE ClassMaster.SchoolId = @SchoolId AND ClassMaster.Status=1 AND Student.StatusId=1)

END
GO
