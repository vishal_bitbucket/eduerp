USE [EduErp]
GO
/****** Object:  StoredProcedure [dbo].[GetStudentChartGenderwise]    Script Date: 04/05/2018 19:51:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetStudentChartGenderwise] 
@SchoolId INT
AS BEGIN
Select GenderId,KeyWordMaster.KeyWordName AS GenderName ,Count(student.Id) as CountNo from Student  LEFT OUTER JOIN KeyWordMaster on Student.GenderId=KeyWordMaster.KeyWordId
Where SchoolId = @SchoolId AND KeyWordMaster.Name='Gender' AND GenderId is not null
Group by GenderId,KeyWordMaster.KeyWordName
END

