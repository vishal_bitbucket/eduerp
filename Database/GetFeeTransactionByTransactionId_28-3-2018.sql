USE [EduErp]
GO
/****** Object:  StoredProcedure [dbo].[GetFeeTransactionByTransactionId]    Script Date: 03/28/2018 19:43:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Proc [dbo].[GetFeeTransactionByTransactionId]
@FeetransactionId int
AS
BEGIN
 SELECT ft.Id,ft.PaymentModeId,ft.StudentId,std.FirstName+' '+''+std.LastName as StudentName,ft.PreviousBalance,ft.TotalAmount as TotalClassGeneralAmount,ft.PreviousBalance,ft.TotalPayableAmount,ft.TotalAmountDeposit,ft.IsSettled,ft.BalanceAmount,ft.Remark,ft.TransactionDate,ft.CreatedOn,ft.CreatedBy,ft.LastUpdatedBy,
 ft.LastUpdatedOn,sec.Name as SectionName,cls.Name as ClassName,sess.Name as SessionName,km.KeyWordName as FromMonthName,kms.KeyWordName as ToMonthName,ft.StatusId,ft.FromMonthId ,ft.ToMonthId,s.SessionId,s.ClassId,s.SectionId,
 ftdr.TotalAmount,kmss.KeyWordName as PaymentModeName,sm.ImagePath as SchoolImagePath ,sm.S3key
 FROM  FeeTransaction ft  Inner JOIN 
   (SELECT  ftd.FeeTransactionId , SUM(fhm.Amount) AS TotalAmount
   FROM    FeeTransactionDetail ftd 
   Left Outer JOIN FeeHeadMonthMapping fhm 
   ON  ftd.FeeHeadMappingId = fhm.Id
   GROUP BY ftd.FeeTransactionId)  ftdr 
 ON ftdr.FeeTransactionId = ft.Id
 Left Outer JOIN StudentClassMapping s on s.StudentId = ft.StudentId
 inner join Student std on std.Id =s.StudentId 
 inner join SchoolMaster sm on sm.Id=std.SchoolId
 inner join SectionMaster sec on sec.Id=s.SectionId
 inner join SessionMaster sess on sess.Id=s.SessionId
 inner join ClassMaster cls on cls.Id=s.ClassId
 inner join KeyWordMaster km on km.KeyWordId= ft.FromMonthId
 inner join KeyWordMaster kms on kms.KeyWordId=ft.ToMonthId
 inner join KeyWordMaster kmss on kmss.KeyWordId=ft.PaymentModeId
 WHERE ft.Id = @FeeTransactionId and km.Name='Month' and kms.Name='Month' and kmss.Name='PaymentMode' AND ft.StatusId<>3
 END