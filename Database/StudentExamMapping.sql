USE [EduErp]
GO
/****** Object:  Table [dbo].[StudentExamMapping]    Script Date: 01/07/2018 18:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentExamMapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ExamMappingId] [int] NOT NULL,
	[ClassId] [int] NOT NULL,
	[SectionId] [int] NOT NULL,
	[ObtainedMarks] [decimal](18, 2) NOT NULL,
	[StudentId] [int] NOT NULL,
	[StatusId] [int] NULL,
	[LastUpdateBy] [int] NULL,
	[LastUpdatedOn] [datetime] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
 CONSTRAINT [PK_StudentExamMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
