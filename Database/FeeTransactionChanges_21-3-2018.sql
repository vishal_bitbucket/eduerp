/*
   Wednesday, March 21, 20181:47:35 PM
   User: sa
   Server: DELL-PC
   Database: EduErp
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.FeeTransaction
	DROP CONSTRAINT FK_FeeTransaction_Student
GO
ALTER TABLE dbo.Student SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_FeeTransaction
	(
	Id int NOT NULL IDENTITY (1, 1),
	StudentId int NOT NULL,
	SessionId int NULL,
	TotalAmountDeposit numeric(18, 2) NULL,
	IsSettled bit NULL,
	BalanceAmount numeric(18, 2) NULL,
	Remark varchar(100) NULL,
	TransactionDate datetime NULL,
	CreatedOn datetime NOT NULL,
	CreatedBy int NOT NULL,
	PaymentModeId int NULL,
	FromMonthId int NULL,
	ToMonthId int NULL,
	LastUpdatedBy int NULL,
	LastUpdatedOn datetime NULL,
	StatusId int NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_FeeTransaction SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_FeeTransaction ON
GO
IF EXISTS(SELECT * FROM dbo.FeeTransaction)
	 EXEC('INSERT INTO dbo.Tmp_FeeTransaction (Id, StudentId, TotalAmountDeposit, IsSettled, BalanceAmount, Remark, TransactionDate, CreatedOn, CreatedBy, PaymentModeId, FromMonthId, ToMonthId, LastUpdatedBy, LastUpdatedOn, StatusId)
		SELECT Id, StudentId, TotalAmountDeposit, IsSettled, BalanceAmount, Remark, TransactionDate, CreatedOn, CreatedBy, PaymentModeId, FromMonthId, ToMonthId, LastUpdatedBy, LastUpdatedOn, StatusId FROM dbo.FeeTransaction WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_FeeTransaction OFF
GO
ALTER TABLE dbo.FineDiscountTransactionDetail
	DROP CONSTRAINT FK_FineDiscountTransactionDetail_FeeTransaction
GO
ALTER TABLE dbo.FeeTransactionDetail
	DROP CONSTRAINT FK_FeeTransactionDetail_FeeTransaction
GO
DROP TABLE dbo.FeeTransaction
GO
EXECUTE sp_rename N'dbo.Tmp_FeeTransaction', N'FeeTransaction', 'OBJECT' 
GO
ALTER TABLE dbo.FeeTransaction ADD CONSTRAINT
	PK_FeeTransaction PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.FeeTransaction ADD CONSTRAINT
	FK_FeeTransaction_Student FOREIGN KEY
	(
	StudentId
	) REFERENCES dbo.Student
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.FeeTransactionDetail ADD CONSTRAINT
	FK_FeeTransactionDetail_FeeTransaction FOREIGN KEY
	(
	FeeTransactionId
	) REFERENCES dbo.FeeTransaction
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.FeeTransactionDetail SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.FineDiscountTransactionDetail ADD CONSTRAINT
	FK_FineDiscountTransactionDetail_FeeTransaction FOREIGN KEY
	(
	TransactionId
	) REFERENCES dbo.FeeTransaction
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.FineDiscountTransactionDetail SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
