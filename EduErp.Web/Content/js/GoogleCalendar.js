﻿    // Client ID and API key from the Developer Console
    var CLIENT_ID = '198221302924-34eaa5956p8eoq9rl5tt4blihhv4iv44.apps.googleusercontent.com';

    // Array of API discovery doc URLs for APIs used by the quickstart
    var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];

    // Authorization scopes required by the API; multiple scopes can be
    // included, separated by spaces.
    var SCOPES = "https://www.googleapis.com/auth/calendar";

    var authorizeButton = document.getElementById('authorize-button');
    var signoutButton = document.getElementById('signout-button');

    /**
     *  On load, called to load the auth2 library and API client library.
     */
    function handleClientLoad() {
        gapi.load('client:auth2', initClient);
    }

    /**
     *  Initializes the API client library and sets up sign-in state
     *  listeners.
     */
    function initClient() {
        gapi.client.init({
            discoveryDocs: DISCOVERY_DOCS,
            clientId: CLIENT_ID,
            scope: SCOPES
        }).then(function () {
            // Listen for sign-in state changes.
            gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);
            //listUpcomingEvents();
            
            if (window.location.href.indexOf("profile") > -1) {
                // Handle the initial sign-in state.
                updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
                authorizeButton.onclick = handleAuthClick;
                signoutButton.onclick = handleSignoutClick;
            }            
        });
    }

    /**
     *  Called when the signed in status changes, to update the UI
     *  appropriately. After a sign-in, the API is called.
     */
    function updateSigninStatus(isSignedIn) {
        if (isSignedIn) {
            authorizeButton.style.display = 'none';
            signoutButton.style.display = 'block';
            //listUpcomingEvents();
        } else {
            authorizeButton.style.display = 'block';
            signoutButton.style.display = 'none';
        }
    }

    /**
     *  Sign in the user upon button click.
     */
    function handleAuthClick(event) {
        gapi.auth2.getAuthInstance().signIn();
        //listUpcomingEvents();
    }

    /**
     *  Sign out the user upon button click.
     */
    function handleSignoutClick(event) {
        gapi.auth2.getAuthInstance().signOut();
    }

    /**
     * Append a pre element to the body containing the given message
     * as its text node. Used to display the results of the API call.
     *
     * param {string} message Text to be placed in pre element.
     */
    function appendPre(message) {
        var pre = document.getElementById('content');
        var textContent = document.createTextNode(message + '\n');
        pre.appendChild(textContent);
    }

    /**
     * Print the summary and start datetime/date of the next ten events in
     * the authorized user's calendar. If no events are found an
     * appropriate message is printed.
     */
    function listUpcomingEvents(job) {
        if (gapi.auth2.getAuthInstance().isSignedIn.get()) {
            gapi.auth2.getAuthInstance().signIn();
        }        
        console.log(job);
        //gapi.client.calendar.events.list({
        //    'calendarId': 'primary',
        //    'timeMin': (new Date()).toISOString(),
        //    'showDeleted': false,
        //    'singleEvents': true,
        //    'maxResults': 10,
        //    'orderBy': 'startTime'
        //}).then(function(response) {
        //    var events = response.result.items;
        //    appendPre('Upcoming events:');

        //    if (events.length > 0) {
        //        for (i = 0; i < events.length; i++) {
        //            var event = events[i];
        //            var when = event.start.dateTime;
        //            if (!when) {
        //                when = event.start.date;
        //            }
        //            appendPre(event.summary + ' (' + when + ')')
        //        }
        //    } else {
        //        appendPre('No upcoming events found.');
        //    }
        //});

            var event = {
                'summary': 'Google I/O 2017',
                'location': '800 Howard St., San Francisco, CA 94103',
                'description': 'A chance to hear more about Google\'s developer products.',
                'start': {
                  'dateTime': '2017-05-28T09:00:00-07:00',
                  'timeZone': 'America/Los_Angeles'
                },
                'end': {
                  'dateTime': '2017-05-28T17:00:00-07:00',
                  'timeZone': 'America/Los_Angeles'
                },
                'recurrence': [
                  'RRULE:FREQ=DAILY;COUNT=2'
                ],
                'attendees': [
                  {'email': 'lpage@example.com'},
                  {'email': 'sbrin@example.com'}
                ],
                'reminders': {
                  'useDefault': false,
                  'overrides': [
                    {'method': 'email', 'minutes': 24 * 60},
                    {'method': 'popup', 'minutes': 10}
                  ]
                }
            };

        var request = gapi.client.calendar.events.insert({
            'calendarId': 'primary',
            'resource': event
        });

        request.execute(function (event) {
            //debugger;
            if (event.status != "confirmed") {
                toastr.error('Error on google calendar sync :  ' + event.message);
            } else {
                toastr.success('Event created: ' + event.htmlLink);
            }            
        });
    }












// Other Method for impletmenting
/*var OAUTHURL    =   'https://accounts.google.com/o/oauth2/auth?';
    var VALIDURL    =   'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=';
    var SCOPE       =   'https://www.googleapis.com/auth/calendar'; //https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email
    var CLIENTID    =   '198221302924-34eaa5956p8eoq9rl5tt4blihhv4iv44.apps.googleusercontent.com';
    var REDIRECT    =   'http://localhost:7200/scheduling/my'
    var LOGOUT      =   'http://accounts.google.com/Logout';
    var TYPE        =   'token';
    var _url        =   OAUTHURL + 'scope=' + SCOPE + '&client_id=' + CLIENTID + '&redirect_uri=' + REDIRECT + '&response_type=' + TYPE;
    var acToken;
    var tokenType;
    var expiresIn;
    var user;
    var loggedIn    =   false;

    function login() {
        var win         =   window.open(_url, "windowname1", 'width=800, height=600');

        var pollTimer   =   window.setInterval(function() {
            try {
                console.log(win.document.URL);
                if (win.document.URL.indexOf(REDIRECT) != -1) {
                    window.clearInterval(pollTimer);
                    var url =   win.document.URL;
                    acToken =   gup(url, 'access_token');
                    tokenType = gup(url, 'token_type');
                    expiresIn = gup(url, 'expires_in');
                    win.close();

                    validateToken(acToken);
                }
            } catch(e) {
            }
        }, 500);
    }

    function validateToken(token) {
        $.ajax({
            url: VALIDURL + token,
            data: null,
            success: function(responseText){
                syncScheduling();
                loggedIn = true;
                //$('#loginText').hide();
                //$('#logoutText').show();
                alert("Logged in with Google");
            },
            dataType: "jsonp"
        });
    }

    function syncScheduling() {
        $.ajax({
            url: '@Url.Action("SyncWithGoogle", "Profile")',
            data: null,
            success: function(resp) {
                user    =   resp;
                console.log(user);
                //$('#uName').append(user.name);
                //$('#imgHolder').attr('src', user.picture);
            },
            dataType: "json"
        });

    //    var event = {
    //        'summary': 'Google I/O 2015',
    //        'location': '800 Howard St., San Francisco, CA 94103',
    //        'description': 'A chance to hear more about Google\'s developer products.',
    //        'start': {
    //          'dateTime': '2015-05-28T09:00:00-07:00',
    //          'timeZone': 'America/Los_Angeles'
    //        },
    //        'end': {
    //          'dateTime': '2015-05-28T17:00:00-07:00',
    //          'timeZone': 'America/Los_Angeles'
    //        },
    //        'recurrence': [
    //          'RRULE:FREQ=DAILY;COUNT=2'
    //        ],
    //        'attendees': [
    //          {'email': 'lpage@example.com'},
    //          {'email': 'sbrin@example.com'}
    //        ],
    //        'reminders': {
    //          'useDefault': false,
    //          'overrides': [
    //            {'method': 'email', 'minutes': 24 * 60},
    //            {'method': 'popup', 'minutes': 10}
    //          ]
    //        }
    //    };

    //var request = gapi.client.calendar.events.insert({
    //    'calendarId': 'primary',
    //    'resource': event
    //});

    //request.execute(function(event) {
    //    toastr.success('Event created: ' + event.htmlLink);
    //});
}

    //credits: http://www.netlobo.com/url_query_string_javascript.html
    function gup(url, name) {
        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
        var regexS = "[\\#&]"+name+"=([^&#]*)";
        var regex = new RegExp( regexS );
        var results = regex.exec( url );
        if( results == null )
            return "";
        else
            return results[1];
    }

    function startLogoutPolling() {
        $('#loginText').show();
        $('#logoutText').hide();
        loggedIn = false;
        $('#uName').text('Welcome ');
        $('#imgHolder').attr('src', 'none.jpg');
    }*/