﻿$(document).ready(function () {
    var maxDate = new Date();
    var endDate = new Date(maxDate.getFullYear() - 0, maxDate.getMonth(), maxDate.getDate(), 0, 0, 0, 0);
    var startDate = new Date(maxDate.getFullYear() - 90, maxDate.getMonth(), 1, 0, 0, 0, 0);

    //$("#DateOfBirth").val($("#dob").val());

    var dob = $("#DateOfBirth").datepicker({
        format: "mm/dd/yyyy",
        onRender: function (date) {
            return (date.valueOf() <= endDate.valueOf() && date.valueOf() >= startDate.valueOf()) ? "" : "disabled";
        },
        show: function (date) {
            return (date.valueOf() <= endDate.valueOf() && date.valueOf() >= startDate.valueOf()) ? "" : "disabled";
        }
    }).on("changeDate", function (ev) {
        $("#DateOfBirth").removeClass("required");
        showToolTip("DateOfBirth", "");
        if (ev.date.valueOf() < startDate) {
            dob.setValue(startDate);
        }
        else if (ev.date.valueOf() > endDate) {
            dob.setValue(endDate);
        }
        $(this).datepicker("hide");
    }).data("datepicker");

    $("#DateOfBirth").keyup(function () {
        if (!validateDate($("#DateOfBirth").val())) {
            $("#DateOfBirth").addClass("required");
            showToolTip("DateOfBirth", "Please Enter Valid Date Of Birth.");
            return false;
        }
        else {
            var age = getAge($("#DateOfBirth").val());
            if (age < 1) {
                $("#DateOfBirth").addClass("required");
                showToolTip("DateOfBirth", "Age cannot be more than today.");
                return false;
            } else if (age > 90) {
                $("#DateOfBirth").addClass("required");
                showToolTip("DateOfBirth", "Age cannot be more than 90 years.");
                return false;
            } else {
                $("#DateOfBirth").removeClass("required");
                showToolTip("DateOfBirth", "");
            }
        }
    });

    var specialKeys1 = new Array();
    specialKeys1.push(8);
    specialKeys1.push(9);
    specialKeys1.push(47);

    $("#DateOfBirth").keypress(function (e) {
        var keyCode = e.which ? e.which : e.keyCode;
        var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys1.indexOf(keyCode) != -1);
        return ret;
    });
    //End Manual DOB Validation
});

function showToolTip(id, message) {
    $("#" + id).tooltip("hide")
          .attr("data-original-title", message)
          .tooltip("fixTitle")
          .tooltip({ 'show': true, 'placement': "bottom" });
}

//Manual DOB Validation

function validateDate(dateStr) {
    // Checks for the following valid date formats:
    // MM/DD/YY   MM/DD/YYYY   MM-DD-YY   MM-DD-YYYY
    // Also separates date into month, day, and year variables

    var datePat = /^(\d{1,2})(\/)(\d{1,2})\2(\d{4})$/;

    // To require a 4 digit year entry, use this line instead:
    // var datePat = /^(\d{1,2})(\/|-)(\d{1,2})\2(\d{4})$/;

    var matchArray = dateStr.match(datePat); // is the format ok?
    if (matchArray == null) {
        //alert("Date is not in a valid format.");
        return false;
    }
    var month = matchArray[1]; // parse date into variables
    var day = matchArray[3];
    var year = matchArray[4];
    if (month < 1 || month > 12) { // check month range
        //  alert("Month must be between 1 and 12.");
        return false;
    }
    if (day < 1 || day > 31) {
        //  alert("Day must be between 1 and 31.");
        return false;
    }
    if ((month == 4 || month == 6 || month == 9 || month == 11) && day == 31) {
        // alert("Month " + month + " doesn't have 31 days!");
        return false;
    }
    if (month == 2) { // check for february 29th
        var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
        if (day > 29 || (day == 29 && !isleap)) {
            //   alert("February " + year + " doesn't have " + day + " days!");
            return false;
        }
    }
    return true;  // date is valid
}

function getAge(dob1) {
    var today = new Date();
    var birthDate = new Date(dob1);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}