﻿function changeDoc(chkBox) {
    var did;
    var docTypeId;
    var docName;
    var docTypeName;
    if (chkBox.attr == undefined) {
        did = chkBox.getAttribute("data-did");
        docTypeId = chkBox.getAttribute("data-docTypeId");
        docName = chkBox.getAttribute("data-dname");
        docTypeName = chkBox.getAttribute("data-doctypeName");
    } else {
        did = chkBox.attr("data-did");
        docTypeId = chkBox.attr("data-docTypeId");
        docName = chkBox.attr("data-dname");
        docTypeName = chkBox.attr("data-doctypeName");
    }
    if ($(".chkFile_" + did).is(":checked")) {
        selectedFile.push(did);
        selectedFilesObjects.push({ id: did, typeId: docTypeId, name: docName, typeName: docTypeName });
        if (uniqueDocTypeIDs.indexOf(docTypeId) === -1) {
            uniqueDocTypeIDs.push(docTypeId);
        }
    } else {
        selectedFile.splice($.inArray(did, selectedFile), 1);
        selectedFilesObjects = selectedFilesObjects.filter(function (obj) {
            return obj.id !== did;
        });
        if (selectedFilesObjects.filter(function (e) { return e.typeId === docTypeId; }).length <= 0)
            uniqueDocTypeIDs.splice($.inArray(docTypeId, uniqueDocTypeIDs), 1);
    }
    $("#delete_file, #share_file, #lnkToFolder").attr('disabled', selectedFile.length <= 0);
}

function checkAllDocuments() {
    if ($("#chkAll").is(":checked")) {
        clearSelectedDocuments();
        $("input[name=chkSelectedFile]").each(function (e) {
            $(this).prop("checked", true);
            changeDoc($(this));
        });
    } else {
        $("input[name=chkSelectedFile]").each(function (e) {
            $(this).prop("checked", false);
        });
        clearSelectedDocuments();
        $("#delete_file, #share_file, #lnkToFolder").attr('disabled', selectedFile.length <= 0);
    }
}

function clearSelectedDocuments(parameters) {
    selectedFile = [];
    uniqueDocTypeIDs = [];
    selectedFilesObjects = [];
}
function shareDocuments(id, docTypeName) {
    var html = "";
    for (var i = 0; i < uniqueDocTypeIDs.length; i++) {
        var files = selectedFilesObjects.filter(function (e) { return e.typeId === uniqueDocTypeIDs[i]; });
        if (files.length > 1) {
            html += files[0].typeName + "(";
            for (var j = 0; j < files.length; j++) {
                html += files[j].name + (j + 1 === files.length ? "" : " , ");
            }
            html += ")" + (i + 1 === uniqueDocTypeIDs.length ? "" : " , ");
        } else {
            html += files[0].typeName + (i + 1 === uniqueDocTypeIDs.length ? "" : " , ");
        }
    }
    if (id == undefined) {
        $("#filesName").html(html);
    } else {
        $("#filesName").html(docTypeName);
    }
    $("#Message").val('');
    $("#Name").val('');
    $("#error").hide();
    $("#hideFolderId").val("");
    $("#modalTitle").text("Share File(s)");
    $("#lblName").text("Selected File(s) Name");
    $("#lblShareWith").text("Share File(s) With");
    $("#shareModal").modal('show');
    $("#hdnDocIds").val((id == undefined ? selectedFile : id));
}
function payForDocumentType(docId, fid, docTypePrice, cardNumber, cardCode, expiryYear, expiryMonth) {
    $(".loader").show();
    $("#btnPayNow").attr("disabled", true);
    $.ajax({
        url: urlToPayForDocumentType,
        data: { docId: docId, cardNumber: cardNumber, cardCode: cardCode, expiryMonth: expiryMonth, expiryYear: expiryYear, amount: docTypePrice },
        type: "post",
        success: function (result) {
            $("#btnPayNow").attr("disabled", false);
            $(".loader").hide();
            if (result.IsErr) {
                toastr.error(result.Message);
                return;
            }
            toastr.success(result.Message);
            $("#paymentModal").modal('hide');
            refreshDocumentListing(fid);
        }
    });
}

function refreshDocumentListing(fid) {
    $.ajax({
        url: urlToGetDocuments,
        data: {folderId: fid, userId: editUserId, },
        dataType: "html",
        type: "post",
        success: function (result) {
            $("#fileList").empty();
            $("#fileList").html(result);
            $('[data-toggle="tooltip"]').tooltip();
        }
    });
}