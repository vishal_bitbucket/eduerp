CURRENT_DATEFORMAT = "dd-M-yyyy";

function showToolTip(id, message) {
    $("#" + id).tooltip("hide")
          .attr("data-original-title", message)
          .tooltip("fixTitle")
          .tooltip({ 'show': true, 'placement': "bottom" });
}

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};

function deleteConfirmationFeehead(callBackFunction, parameter, parameter1) {
    //if (message === undefined) {
    //    message = "Are you sure want to delete ?";
    //}
    debugger;
    bootbox.dialog({
        message: "If Feehead is already in used then feehead will not be deleted. Are you sure want to delete ?",
        title: "Delete Confirmation",
        buttons: {
            Cancel: {
                label: "Cancel",
                className: "btn btn-default"
            },
            danger: {
                label: "Delete",
                className: "btn-danger",
                callback: function () {
                    if (parameter1 === undefined || parameter1 === 0) {
                        callBackFunction(parameter);
                    } else {
                        callBackFunction(parameter, parameter1);
                        //if (parameter2 === undefined || parameter2 === 0) {
                        //    callBackFunction(parameter, parameter1);
                        //} else {
                        //    callBackFunction(parameter, parameter1, parameter2);
                        //}
                    }
                }
            }
        }
    });
}


function deleteConfirmation(callBackFunction, parameter, parameter1) {
    //if (message === undefined) {
    //    message = "Are you sure want to delete ?";
    //}
    debugger;
    bootbox.dialog({
        message: "Are you sure want to delete ?",
        title: "Delete Confirmation",
        buttons: {
            Cancel: {
                label: "Cancel",
                className: "btn btn-default"
            },
            danger: {
                label: "Delete",
                className: "btn-danger",
                callback: function () {
                    if (parameter1 === undefined || parameter1 === 0) {
                        callBackFunction(parameter);
                    } else {
                        callBackFunction(parameter, parameter1);
                        //if (parameter2 === undefined || parameter2 === 0) {
                        //    callBackFunction(parameter, parameter1);
                        //} else {
                        //    callBackFunction(parameter, parameter1, parameter2);
                        //}
                    }
                }
            }
        }
    });
}

function updateConfirmation(callBackFunction, title, message, parameter) {
    bootbox.dialog({
        message: message,
        title: title,
        buttons: {
            Cancel: {
                label: "No",
                className: "btn btn-default"
            },
            danger: {
                label: "Yes",
                className: "btn-danger",
                callback: function() {
                   callBackFunction(parameter);
                    }
            }
        }
    });
}
function setStatusConfirmation(callBackFunction, title, message, parameter, parameter1) {
    //if (message === undefined) {
    //    message = "Are you sure want to activate ?";
    //}
    bootbox.dialog({
        message: message,
        title: title,
        buttons: {
            Cancel: {
                label: "No",
                className: "btn btn-default"
            },
            danger: {
                label: "Yes",
                className: "btn-danger",
                callback: function () {
                    if (parameter1 === undefined || parameter1 === 0) {
                        callBackFunction(parameter);
                    } else {
                        callBackFunction(parameter, parameter1);
                        //if (parameter2 === undefined || parameter2 === 0) {
                        //    callBackFunction(parameter, parameter1);
                        //} else {
                        //    callBackFunction(parameter, parameter1, parameter2);
                        //}
                    }
                }
            }
        }
    });
}


function publishConfirmation(callBackFunction, parameter) {
    bootbox.dialog({
        message: "<b>Are you sure you want to publish these changes ?</b>",
        title: "Publish",
        buttons: {
            Cancel: {
                label: "Cancel",
                className: "btn btn-default"
            },
            danger: {
                label: "Publish",
                className: "btn-warning",
                callback: function () {
                    callBackFunction(parameter);
                }
            }
        }
    });
}

function activateConfirmation(callBackFunction, parameter) {
    bootbox.dialog({
        message: "<b>Are you sure you want to activate ?</b>",
        title: "Activate Confirmation",
        buttons: {
            Cancel: {
                label: "Cancel",
                className: "btn btn-default"
            },
            danger: {
                label: "Activate",
                className: "btn-primary",
                callback: function () {
                    callBackFunction(parameter);
                }
            }
        }
    });
}

function archiveConfirmation(callBackFunction, parameter) {
    bootbox.dialog({
        message: "<b>Are you sure you want to archive ?</b>",
        title: "Archive Confirmation",
        buttons: {
            Cancel: {
                label: "Cancel",
                className: "btn btn-default"
            },
            danger: {
                label: "Archive",
                className: "btn-danger",
                callback: function () {
                    callBackFunction(parameter);
                }
            }
        }
    });
}

function archiveConfirmationForScheduling(callBackFunction, parameter) {
    bootbox.dialog({
        message: "<b>Are you sure you want to archive ?</b>",
        title: "Archive Confirmation",
        buttons: {
            Cancel: {
                label: "Cancel",
                className: "btn btn-default"
            },
            danger: {
                label: "Archive",
                className: "btn-danger",
                callback: function () {
                    callBackFunction(parameter);
                }
            }
        }
    });
}

function approveConfirmation(callBackFunction, parameter) {
    bootbox.dialog({
        message: "Are you sure you want to approve ?",
        title: "Approve Confirmation",
        buttons: {
            Cancel: {
                label: "Cancel",
                className: "btn btn-default"
            },
            danger: {
                label: "Approve",
                className: "btn-primary",
                callback: function () {
                    callBackFunction(parameter);
                }
            }
        }
    });
}

function revertConfirmation(callBackFunction, parameter) {
    bootbox.dialog({
        message: "Are you sure you want to revert ?",
        title: "Revert Confirmation",
        buttons: {
            Cancel: {
                label: "Cancel",
                className: "btn btn-default"
            },
            danger: {
                label: "Revert",
                className: "btn-primary",
                callback: function () {
                    callBackFunction(parameter);
                }
            }
        }
    });
}

function rejectConfirmation(callBackFunction, parameter) {
    bootbox.dialog({
        message: "Are you sure you want to reject ?",
        title: "Reject Confirmation",
        buttons: {
            Cancel: {
                label: "Cancel",
                className: "btn btn-default"
            },
            danger: {
                label: "Reject",
                className: "btn-danger",
                callback: function () {
                    callBackFunction(parameter);
                }
            }
        }
    });
}

function removePicConfirmation(callBackFunction, parameter1, parameter2) {
    bootbox.dialog({
        message: "Are you sure you want to delete profile photo ?",
        title: "Profile Photo Confirmation",
        buttons: {
            Cancel: {
                label: "Cancel",
                className: "btn btn-default"
            },
            danger: {
                label: "Delete",
                className: "btn-danger",
                callback: function () {
                    callBackFunction(parameter1, parameter2);
                }
            }
        }
    });
}

function detectBrowserIE() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        return true;
    } else {
        return false;

    }
}
function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}


function showHeader(title) {
    //$('#gridHeader').removeClass('hidden');
    $('#headerText').html("<p>"+ title + "</p>");
}

function removeGridPanelBorder() {
    $('#gridPanel').addClass('no-border');
}

function businessRequired() {
    bootbox.dialog({
        message: "You have not any active company. Please add a new company first.",
        title: "Company Required",
        buttons: {
            Cancel: {
                label: "Ok",
                className: "btn btn-default"
            }
        }
    });
}

function getFormattedDate(startDate) {
    var d = startDate.getDate();
    if (d.toString().length === 1) {
        d = "0" + d;
    }
    var m = startDate.getMonth();
    m += 1;  // JavaScript months are 0-11
    if (m.toString().length === 1) {
        m = "0" + m;
    }
    var y = startDate.getFullYear();
    return d + "-" + m + "-" + y;
}

function isValidMobileNumber(mobilenumber) {
    var pattern = new RegExp("^\\(?([7-9]{1})\\)?([0-9]{9})$");
    return pattern.test(mobilenumber);
}

function is_int(value) {
    if ((parseFloat(value) == parseInt(value)) && !isNaN(value)) {
        return true;
    } else {
        return false;
    }
}

function isValidAadharnumber(aadharnumber) {
    var pattern = new RegExp("^\\(?([7-9]{1})\\)?([0-9]{9})$");
    return pattern.test(aadharnumber);
}

function isValidText(text) {
    if ((text < 97 || text > 122) && (text < 65 || text > 90) && (text !== 32))
        return false;
    else {
        return true;
    }
   
}

function showErrorMessage(msg) {
    $("#stickyNote").append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>' +
        '<i class="fa fa-ban-circle"></i>'+ msg +'</div>');
}