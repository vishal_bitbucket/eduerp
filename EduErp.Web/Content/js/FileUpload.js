$(document).ready(function () {
    $("#success").hide();
    $("#failure").hide();
});

var _0x47ec = ["\x41\x4B\x49\x41\x49\x35\x49\x50\x34\x46\x4E\x46\x53\x52\x48\x47\x37\x50\x4B\x41", "\x33\x4A\x7A\x64\x62\x32\x4F\x32\x71\x35\x2F\x31\x6D\x6B\x55\x30\x44\x6D\x34\x44\x64\x64\x42\x6A\x63\x4B\x50\x46\x48\x73\x58\x42\x69\x6E\x52\x53\x6A\x36\x77\x34", "\x75\x70\x64\x61\x74\x65", "\x63\x6F\x6E\x66\x69\x67"];
//AWS[_0x47ec[3]][_0x47ec[2]]({
//    accessKeyId: _0x47ec[0],
//    secretAccessKey: _0x47ec[1]
//});
AWS.config.update({ accessKeyId: _0x47ec[0], secretAccessKey: _0x47ec[1] });
AWS.config.region = 'ap-south-1';
AWS.config.signatureVersion = 'v4';

var _0x90fb = ["\x65\x64\x75\x65\x72\x70\x6F\x6E\x6C\x69\x6E\x65"];
var url = window.location.host;
var single = 1;
var bucketName = _0x90fb[0];
var schoolProfileFolderName = "School";
var EmployeeProfileFolderName = "Employee";
var studentProfileFolderName = "Student";

var bucket = new AWS.S3({
    params: {
        Bucket: bucketName
    }
});

function ServerUpdateError(error, data) { console.log(error); }

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
};

function setImageUrl(key, imageId) {
    if (key !== "") {
        (new AWS.S3()).getSignedUrl("getObject", {
            Bucket: bucketName,
            Key: key
        }, function (err, url) {
            $("#" + imageId).attr("src", url);
        });
    }
}

function removeStudentProfilePic(key) {
    
    (new AWS.S3()).deleteObject({
        Bucket: bucketName,
        Key: key
    }, function (err, data) {
        if (err) {
            console.log(err, err.stack); // an error occurred
        } else {
            //window.studentProfilePic = "";
            $("#DeleteStudentPic").attr("style", "display:none;");
            $("#studentProfilePic").attr("src", window.studentProfilePic);
            $(".loader").hide();
        }
    });
}

function removeSchoolProfileImage(key) {

    (new AWS.S3()).deleteObject({
        Bucket: bucketName,
        Key: key
    }, function (err, data) {
        if (err) {
            console.log(err, err.stack); // an error occurred
        } else {
            //window.studentProfilePic = "";
            $("#DeleteSchoolPic").attr("style", "display:none;");
            $("#schoolProfilePic").attr("src", window.schoolProfilePic);
            $(".loader").hide();
        }
    });
}

function removeEmployeeProfileImage(key) {

    (new AWS.S3()).deleteObject({
        Bucket: bucketName,
        Key: key
    }, function (err, data) {
        if (err) {
            console.log(err, err.stack); // an error occurred
        } else {
            //window.studentProfilePic = "";
            $("#DeleteEmployeePic").attr("style", "display:none;");
            $("#EmployeeProfilePic").attr("src", window.EmployeeProfilePic);
            $(".loader").hide();
        }
    });
}

/* Incident File Start */
function uploadFile(folder, file) {
    showLoader();
    debugger;
    var error = false;
    var s3Key = "";
    var count = 0;
    for (var i = 0; i < file.length; i++) {
        if (error) {
            toastr.error("Some error occured!");
            break;
        }
        if (file[i].size > 4194304) {
            toastr.error("file couldn't be uploaded. file should be less than 4 MB.");
            removeLoader();
            $("#btnCreateReport").attr("disabled", false);
            return;
        }
        var guidname = guid();
        if (s3Key != "") {
            s3Key = s3Key + ",";
        }
        $('#S3Key').val(s3Key + guidname);
        s3Key = s3Key + guidname;
        var objKey = folder + "/" + guidname;
        var params = {
            Key: objKey,
            ContentType: file[i].type,
            Body: file[i],
            ACL: "private",
            Bucket: bucketName,
            partSize: 1,
            ContentDisposition: 'attachment; filename=' + file[i].name + ';',
        };
        var upload = new AWS.S3.ManagedUpload({
            params: params
        });
        //sleepFor(6000);
        upload.on("httpUploadProgress", function (evt) {
            $("#progressBar").show();
            if (evt.lengthComputable) {
                var percentComplete = evt.loaded * 100 / evt.total;
                percentComplete += "%";
                $("#progressBarContainer").css("width", percentComplete);
                $("#progressBarContainer").attr("data-original-title", percentComplete);
            }
        }).send(function (err, data) {
            if (err) {
                toastr.error("Some error occured.");
                error = true;
                removeLoader();
            }
            count = count + 1;
            if (file.length == count) {
                submitForm();
            }
        });
    }
}

function removeFile(id, key, type) {
    (new AWS.S3()).deleteObject({
        Bucket: bucketName,
        Key: key
    }, function (err, data) {
        if (err) {
            console.log(err, err.stack); // an error occurred
        } else {
            //console.log(data);           // successful response
            if (type == "Licence") {
                deleteLicenceImage(id);
            } else {
                if (type == "Position") {
                    deletePositionHeldImage(id);
                } else {
                    deleteTradeExpImage(id);
                }
            }
            removeLoader();
        }
    });
}

function downloadFile(key) {
    var s3 = new AWS.S3();
    s3.getSignedUrl("getObject", {
        Bucket: bucketName,
        Key: key
    }, function (err, url) {
        if (url) window.open(url, "_blank");
    });
}
/* Incident File End */
