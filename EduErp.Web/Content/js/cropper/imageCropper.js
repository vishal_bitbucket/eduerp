$(document).ready(function () {
    debugger;
    var existingSource = $("#uploadeImages").val();

    if (existingSource != '') {
        $('#originalProfileImage').attr('src', existingSource);
        $("#btnProfilePic").removeClass("disabled");
        $("#uploadeImages").val(existingSource);
        AttachCropper();
    } else {
        $('.profile-image-preview-region').hide();
    }
    document.getElementById("selectProfileImage").onchange = function () {

        if (this.files[0] != null) {
            var file = this.files[0];
            if (!file.type.match('image')) {
                $("#errorMsgProfile").text("Incorrect file type.Please select images only.");
                $("#errorMsgProfile").removeClass('hidden');
                $("#profileImgInnerContainer").addClass('hidden');
                $("#btnProfilePic").addClass("disabled");
                return;
            } else {
                $("#btnProfilePic").removeClass("disabled");
                $("#errorMsgProfile").text("");
                $("#errorMsgProfile").addClass('hidden');
                $("#profileImgInnerContainer").removeClass('hidden');
                var reader = new window.FileReader();

                reader.onload = function (e) {
                    debugger;
                    var src = e.target.result;
                    $('#selectedProfileImageName').text($('#selectProfileImage').val());
                    if (isCropperAttached)
                        $('#originalProfileImage').attr('src', src);
                    else
                        $('#originalProfileImage').attr('src', src);
                    AttachCropper();
                    $("#uploadeImages").val(src);
                    $('#originalProfileImage').attr('src', src);
                    image.src = src;

                };
                reader.readAsDataURL(file);
            }

        }
    };
    $("#leftRotate").click(function () {
        cropper.rotate(-90);
        //$image.cropper('rotate', -90);
    });
    $("#rightRotate").click(function () {
        cropper.rotate(90);
        //$image.cropper('rotate', 90);
    });
});

function AttachCropper() {
    debugger;
    if (isCropperAttached)
        cropper.destroy();

    var $dataX = $("#data-x"),
        $dataY = $("#data-y"),
        $dataHeight = $("#data-height"),
        $dataWidth = $("#data-width"),
        $rotate = $("#data-rotate");
    var dataX = 100;
    var dataY = 60;
    var dataHeight = 400;
    var dataWidth = 900;
    var dataRotate = 0;
    var dataScaleX = 1;
    var dataScaleY = 1;
    $('.profile-image-preview-region').removeClass('hidden');
    $('.profile-image-preview-region').css("display", "");
    $('#rotateControl').removeClass('hidden');
    $('.cropper - crop - box').css("left", "10px !important");
    var options = {
        aspectRatio: 16 / 9,
        preview: '.profile-image-preview',
        ready: function (e) {
            console.log(e.type);
        },
        cropstart: function (e) {
            console.log(e.type, e.detail.action);
        },
        cropmove: function (e) {
            console.log(e.type, e.detail.action);
        },
        cropend: function (e) {
            console.log(e.type, e.detail.action);
        },
        crop: function (e) {
            var data = e.detail;

            console.log(e.type);
            dataX.value = Math.round(data.x);
            dataY.value = Math.round(data.y);
            dataHeight.value = Math.round(data.height);
            dataWidth.value = Math.round(data.width);
            dataRotate.value = typeof data.rotate !== 'undefined' ? data.rotate : '';
            dataScaleX.value = typeof data.scaleX !== 'undefined' ? data.scaleX : '';
            dataScaleY.value = typeof data.scaleY !== 'undefined' ? data.scaleY : '';
        },
        zoom: function (e) {
            console.log(e.type, e.detail.ratio);
        }
    };

    container = document.querySelector('.profile-image-container');
    image = container.getElementsByTagName('img').item(0);
    cropper = new Cropper(image, options);
    isCropperAttached = true;
}