﻿$(document).ready(function (){
    var options = {
        types: ['geocode'],
        componentRestrictions: { country: "usa" }
    };
    var placeSearch, autocomplete;
    var componentForm = {
        //  street_number: 'long_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'long_name',
        administrative_area_level_2: 'long_name',
        //country: 'long_name',
        postal_code: 'long_name'
    };

    var input = document.getElementById("keyword");
    var autocomplete = new google.maps.places.Autocomplete(input, options);
    autocomplete.addListener('place_changed', fillInAddress);
});

function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();
    for (var component in componentForm) {
        if (component == "route") continue;
        document.getElementById(component).value = '';
        document.getElementById(component).disabled = false;
    }

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
            if (addressType == 'route') {
                var val = place.address_components[i][componentForm[addressType]];
                //  document.getElementById('street_number').value += " " + val;
                //  document.getElementById('street_number').value = document.getElementById('street_number').value.trim();
                continue;
            }
            if (addressType == 'administrative_area_level_1') {
                var val = place.address_components[i][componentForm[addressType]];
                $.ajax({
                    url: '@Url.Action("SetId", "Account")',
                    data: { name: val },
                    success: function (data) {
                        if (!data.isErr) {
                            $('#administrative_area_level_1').val(data.id);
                        }
                    }
                });
                continue;
            }
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
        }
    }
}