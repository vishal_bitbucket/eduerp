﻿$.fn.videoHTML = function (flashVars) {
    var settings = $.extend({
        // These are the defaults.
        MovieFullUrl: "",
        postbackUrl: "",
        TreatmentId: ""
    }, flashVars);
    var autoPlay = 'autoplay="true"';
    
    autoPlay = '';
    //}

    var htmlStr = '<div id="flowplayer" class="flowplayer" data-ratio="0.56" data-key="$129257063627105"><video' + autoPlay + ' preload="none" ><source type="application/x-mpegURL"  src="' + settings.iOSStreamingURL + '"><source id="videoSourceId" type="video/mp4" src="' + settings.MovieFullUrl + '"></video></div>';

    htmlStr += '<form  name="next" method="post" action="' + settings.postbackUrl + '">' +
        '<a href="#" id="videoFinish" style="display:none;" title="submit" >Click here to continue</a>' +
        '</form>';

    $(this).html(htmlStr);
    loadFlowPlayer();
    var player = $('#flowplayer');
    player.flowplayer();

    addTapToPlay();
    var api = player.data('flowplayer');
    api.bind('finish', function () {
        $('#divTapToPlay').remove();
//        CcbtSubmit($('#videoFinish'));
    }).bind("load", function (e, api, video) {
        $('.flowplayer').find('a').hide();
        $('#divTapToPlay').remove();
    }).bind("play", function () {
        $('.flowplayer').find('a').hide();
        $('#divTapToPlay').remove();

    }).bind("pause", function () {
        $('.flowplayer').find('a').hide();
        addTapToPlay();

    }).bind("resume", function () {
        $('.flowplayer').find('a').hide();
        $("#divTapToPlay").remove();
    });

    function addTapToPlay() {
        if (!($('.fp-ui').find('#divTapToPlay').length > 0)) {
            var tapText = "";
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                if ($('#hdnTapToPlayText').length) {
                    tapText = $.trim($('#hdnTapToPlayText').html());
                }
            } else {
                if ($('#hdnClickToPlayText').length) {
                    tapText = $.trim($('#hdnClickToPlayText').html());
                }
            }
            $('.fp-ui').prepend("<div id='divTapToPlay' class='tapToPlay' style='width: 100%; position: absolute;bottom: 25%;text-align:center;' onclick='api.play()' style='margin-top:" + ($('#flowplayer').height() / 2 + 70) + "px'><h2 class='tapToPlayMessage' style='color:#38A2CD; font-weight:bold; font-size:22px; text-shadow: 1px 1px 1px #A0A0A0'>" + tapText + "</h2></div>");
        }
    }

    function loadFlowPlayer() {
        try {
            $('#videoSourceId').attr('src', settings.MovieFullUrl);
        }
        catch (err) { }

        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {

        } else {
            if (detectBrowser()) {
                flowplayer.defaults.engine = 'html5';
            } else {
                if (isFlashInstalled()) {
                    try { $('#videoSourceId').attr('src', ''); }
                    catch (err) { }
                    try { $('#videoSourceId').attr('src', settings.flashStreamingURL); }
                    catch (err) { }
                    flowplayer.defaults.engine = 'flash';
                    // will not work on local, to make it run force else part.
                    if (flashVars.TreatmentId == '14') {
                        flowplayer.defaults.swf = $('#flashFilePath').val();
                    } else {
                        flowplayer.defaults.swf = '../../../../FlowPlayer/flowplayer.swf';
                    }
                } else {
                    $('#flowplayer').html("<div id='flow-message' class='fp-message' style='display:block !important'><h2>In order to use this treatment you must have the latest version of Adobe Flash Player installed.." +
                                            "</h2><a href='http://get.adobe.com/flashplayer/' target='_blank'>click here to download flash player </a></div>");
                }
            }
        }
        flowplayer.conf.embed = false;
    }

    function detectBrowser() {
        var isIE11 = !!navigator.userAgent.match(/Trident.*rv[ :]*11\./);
        if (isIE11 == true) {
            return false;
        } else {
            if ($.browser.mozilla || $.browser.chrome || $.browser.safari) {
                return true;
            }
            if (($.browser.msie && parseInt($.browser.version) <= 10)) {
                return false;
            }
        }
        return false;
    }

    function isFlashInstalled() {
        var hasFlash = false;
        try {
            hasFlash = Boolean(new ActiveXObject('ShockwaveFlash.ShockwaveFlash'));
        } catch (exception) {
            if (navigator.mimeTypes && navigator.mimeTypes['application/x-shockwave-flash'] != undefined && navigator.mimeTypes['application/x-shockwave-flash'].enabledPlugin) {
                hasFlash = true;
            }
        }
        return hasFlash;
    }
}