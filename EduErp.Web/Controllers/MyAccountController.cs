﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BLL.EmailNotifications;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;
using Newtonsoft.Json;

namespace EduErp.Web.Controllers
{
    public class MyAccountController : BaseController
    {
        //
        // GET: /MyAccount/
        [HttpGet]
        public ActionResult Index()
        {
            var result = MyAccountService.GetMyAccuontDetail(SessionItems.SchoolId).Result;
            var model = result;
           // var citystate = new CityStateBo { ListState = StateList };
            model.SchoolDetails.ListOfSchools = ParentSchoolList;
            model.ListOfTemplateText = MessageService.GetTemplateList().Result;
           // model.SchoolDetails.CityState = citystate;
            // model.AccountDetails.ListOfPlans =AccountDetailService.GetPlan().Result;
            // model.AccountDetails.ListOfSchools = Schools;
            if (model.AccountDetails.NextPaymentDate != null)
                model.AccountDetails.NextPaymentDateStr = model.AccountDetails.NextPaymentDate.Value.ToFormattedDate();
            if (model.AccountDetails.DueDate != null)
                model.AccountDetails.DueDateStr = model.AccountDetails.DueDate.Value.ToFormattedDate();
            if (model.AccountDetails.AppStartDate != null)
                model.AccountDetails.AppStartDateStr = model.AccountDetails.AppStartDate.Value.ToFormattedDate();
            return View(model);
        }

        [AjaxOnly]
        public ActionResult SendQuery()
        {
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Index(string data)
        {
            var model = JsonConvert.DeserializeObject<List<MessageSettings>>(data);
            var result = MyAccountService.SaveMessageSetting(model, SessionItems.SchoolId).Result;
            return Json(result ? new { isErr = false, msg = Messages.AddSuccessfully } : new { isErr = true, msg = Messages.AddError }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendQuery(MyAccountBo model)
        {
            model.SchoolDetails.Id = SessionItems.SchoolId;
            model.SchoolDetails.Name = SessionItems.SchoolName;
            
            NotificationSender.SendQueryMail(model, SessionItems.UserId, SessionItems.DisplayName);
            return Json(new { isErr = false, msg = Messages.MailSentSuccessfully }, JsonRequestBehavior.AllowGet);

        }

        [AjaxOnly]
        public JsonResult GetCityList(string stateId)
        {
            var cityList = (from a in SchoolService.GetCityList(int.Parse(stateId)).Result
                            select new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.Id.ToString()
                            }).ToList();

            return Json(cityList);
        }

    }
}
