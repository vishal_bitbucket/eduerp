﻿using System;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;

namespace EduErp.Web.Controllers
{
    public class NotificationController : BaseController
    {
        //
        // GET: /Notification/

        public ActionResult Index()
        {
            return View(NotificationService.GetNotificationsForIndex().Result);
        }
        [AjaxOnly]
        public ActionResult Add()
        {
            NotificationBo model = new NotificationBo
            {
                ListOfNotifications = NotificationService.GetNotifications().Result
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(NotificationBo model)
        {
            if (!ModelState.IsValid)
                return View(model);
            model.ListOfNotifications = NotificationService.GetNotifications().Result;
            model.SchoolId = SessionItems.SchoolId;
            model.CreatedBy = SessionItems.UserId;
            model.CreatedOn = DateTime.Now;
            model.StatusId = (int)UserStatus.Active;
            var result = NotificationService.AddNotification(model).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.AddSuccessfully;
            else
                TempData["ErrMessage"] = Messages.AddError;
            return Json(new { isErr = false, msg = Messages.AddSuccessfully }, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        public ActionResult Edit(int id)
        {
            var result = NotificationService.GetNotificationById(id).Result;
            result.ListOfNotifications = NotificationService.GetNotifications().Result;
            return View(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(NotificationBo model)
        {
            if (!ModelState.IsValid)
                return View(model);
            model.UpdatedBy = SessionItems.UserId;
            model.UpdatedOn = DateTime.Now;
            model.ListOfNotifications = NotificationService.GetNotifications().Result;
            var result = NotificationService.UpdateNotification(model).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(new { isErr = false, msg = Messages.UpdateSuccessfully }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateStatus(int id, int statusId)
        {
            var result = NotificationService.UpdateStatus(id, statusId, SessionItems.UserId).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(result);
        }

    }
}
