﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;

namespace EduErp.Web.Controllers
{
    public class ExamQuestionController : BaseController
    {
        //
        // GET: /ExamQuestion/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Add()
        {
            var subjectList = OnlineTestQuestionService.GetSubjects(SessionItems.SchoolId).Result;
            var examList = ExamQuestionService.GetExams(SessionItems.SchoolId).Result;
            var ExamQuestion = new ExamQuestionMapping
            {
                ListOfSubject = subjectList,
                ListOfExam = examList,
            };
            return View(ExamQuestion);
        }
        [HttpPost]
        public ActionResult GetQuestionListForQuestionMapping(int subjectId)
        {
            ExamQuestionMapping model = new ExamQuestionMapping { QuestionLists = GetQuestions(subjectId) };
            return PartialView("_ExamQuestionTemplate", model);
        }


        [NonAction]
        public List<QuestionList> GetQuestions( int subjectId)
        {
            var questionLists = ExamQuestionService.GetQuestionsForQuestionMapping(subjectId).Result;
            return questionLists;
        }
       
    }
}
