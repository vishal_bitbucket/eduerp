﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EduErp.Mvc.Utilities;
using EduErp.BO;
using EduErp.Common;

namespace EduErp.Web.Controllers
{
    public class CertificateController : BaseController
    {
        //
        // GET: /Certificate/

        public ActionResult Index()
        {
            return View(BLL.CertificateService.GetCertificates(SessionItems.SchoolId).Result);
        }
        public ActionResult Add()
        {
            var model = new BO.CertificationBo();
            model.ListOfCertificateTemplateType = BLL.CertificateService.GetCertificateTemplateType().Result;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(CertificationBo model)
        {
            if (!ModelState.IsValid)
            {
                model.ListOfCertificateTemplateType = BLL.CertificateService.GetCertificateTemplateType().Result;
                return View(model);
            }
            model.StatusId = (int)UserStatus.Active;
            model.CreatedBy = SessionItems.UserId;
            model.CreatedOn = System.DateTime.Now;
            model.SchoolId = SessionItems.SchoolId;
            var result = BLL.CertificateService.AddMapping(model).Result;
               if (result)
                    TempData["SuccessMessage"] = Messages.AddSuccessfully;
                else
                    TempData["ErrMessage"] = Messages.AddError;
                return Json(new { isErr = false, msg = Messages.AddSuccessfully }, JsonRequestBehavior.AllowGet);
        }
        [AjaxOnly]
        public ActionResult Edit(int id)
        {
            var result = BLL.CertificateService.GetCertificateById(id, SessionItems.SchoolId).Result;
            result.ListOfCertificateTemplateType = BLL.CertificateService.GetCertificateTemplateType().Result;
            return PartialView(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CertificationBo model)
        { 
            if(!ModelState.IsValid)
            {
                model.ListOfCertificateTemplateType=BLL.CertificateService.GetCertificateTemplateType().Result;
                return PartialView(model);
            }
            model.LastUpdatedBy=SessionItems.UserId;
            model.LastUpdatedOn=DateTime.Now;
            var result=BLL.CertificateService.UpdateCertificate(model).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(new { isErr = false, msg = Messages.UpdateSuccessfully }, JsonRequestBehavior.AllowGet);

        }

        [AjaxOnly]
        public JsonResult GetCertificateTemplateNameByTemplateTypeId(string templateTypeId)
        {
            var templateNameList = (from s in BLL.CertificateService.GetCertificateTemplateNameByTeplateTypeId(int.Parse(templateTypeId)).Result
                                select new SelectListItem()
                                {
                                    Text = s.Name,
                                    Value = s.Id.ToString()
                                }).ToList();
            return Json(templateNameList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateStatus(int id, int statusId)
        {
            var result = BLL.CertificateService.UpdateStatus(id, statusId, SessionItems.UserId).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(result);
        }


        public ActionResult Download()
        {
            DownloadBo model = new DownloadBo
            {
                //ListOfSessions = SessionlList,
                ListOfClasses = BLL.FeeTransactionService.GetClasses(SessionItems.SchoolId).Result,
                ListOfTemplateName=BLL.CertificateService.GetTemplateList(SessionItems.SchoolId).Result
                
            };
            return PartialView(model);
        }
        [HttpPost]
        public JsonResult GenerateCertificate(string classId, string sectionId, string studentId, string templateId)
        {
            var result = BLL.CertificateService.GetTemplate(SessionItems.SessionId, int.Parse(classId), int.Parse(sectionId), int.Parse(studentId), int.Parse(templateId), SessionItems.SchoolId).Result;
            return Json(result);
        }
        [AjaxOnly]
        public JsonResult GetSectionList(string classId)
        {
            var sectionList = (from a in BLL.FeeTransactionService.GetSections(int.Parse(classId)).Result
                               select new SelectListItem()
                               {
                                   Text = a.Name,
                                   Value = a.Id.ToString()
                               }).ToList();

            return Json(sectionList);
        }
        [AjaxOnly]
        public JsonResult GetStudentsListBySectionId(string sectionId)
        {
            var studentsList = (from s in BLL.FeeTransactionService.GetStudentsBySectionId(int.Parse(sectionId),SessionItems.SessionId).Result
                                select new SelectListItem()
                                {
                                    Text = s.Name,
                                    Value = s.Id.ToString()
                                }).ToList();
            return Json(studentsList);
        }

    }
}
