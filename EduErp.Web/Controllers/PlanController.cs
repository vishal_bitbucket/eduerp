﻿using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;

namespace EduErp.Web.Controllers
{
    public class PlanController : BaseController
    {
        //
        // GET: /Plan/

        public ActionResult Index()
        {
            return View(BLL.PlanService.GetPlans().Result);
        }

        public ActionResult Add()
        {
            var model = new PlanBo {PlanList = PlanService.GetPlanType().Result};
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(PlanBo model)
        {
            if (!ModelState.IsValid)
                return View(model);
                model.CreatedOn = System.DateTime.Now;
                model.CreatedBy = SessionItems.UserId;
                model.StatusId =(int) UserStatus.Active;
                model.PlanList = PlanService.GetPlanType().Result;
                var result = BLL.PlanService.AddPlan(model).Result;
                if (result)
                    TempData["SuccessMessage"] = Messages.AddSuccessfully;
                else
                    TempData["ErrMessage"] = Messages.AddError;
                return Json(new { isErr = false, msg = Messages.AddSuccessfully }, JsonRequestBehavior.AllowGet);

           }

        public ActionResult Edit(int id)
        {
            var result = BLL.PlanService.GetPlan(id).Result;
            result.PlanList = PlanService.GetPlanType().Result;
            return View(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PlanBo model)
        {
            if (!ModelState.IsValid) return View(model);
            model.UpdatedBy = SessionItems.UserId;
            model.UpdatedOn = System.DateTime.Now;
            model.PlanList = PlanService.GetPlanType().Result;
            var result = BLL.PlanService.UpdatePlan(model).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.AddError;
            return Json(new { isErr = false, msg = Messages.UpdateSuccessfully }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateStatus(int id, int statusId)
        {
            var result = BLL.PlanService.UpdateStatus(id, statusId, SessionItems.UserId).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(result);
        }
    }
}
