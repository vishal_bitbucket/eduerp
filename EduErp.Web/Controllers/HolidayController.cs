﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;

namespace EduErp.Web.Controllers
{
    public class HolidayController : BaseController
    {
        //
        // GET: /Holiday/

        public ActionResult Index()
        {

            return View(HolidayService.GetHolidays(SessionItems.SchoolId).Result);
        }
          [AjaxOnly]
        public ActionResult Add()
        {
            var model=new HolidayBo();
            var sesstionList= HolidayService.GetSessionList(SessionItems.SchoolId).Result;
            //model.SessionList = sesstionList;
            //default Selection of session;
            model.SessionId = SessionItems.SessionId;
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(HolidayBo model)
        {
            //DateTime fdate;
            //DateTime.TryParseExact(model.FromDateStr, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out fdate);
            //DateTime tdate;
            //DateTime.TryParseExact(model.ToDateStr, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out tdate);
            //model.FromDate = fdate;
            //model.ToDate = tdate;
            if (!ModelState.IsValid)
            {
               // model.ListOfSchools = Schools;
                return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            model.CreatedBy = SessionItems.UserId;
            model.CreatedOn = DateTime.Now;
            model.StatusId = (int)UserStatus.Active;
            model.SchoolId = SessionItems.SchoolId;
            model.SessionId = SessionItems.SessionId;
            var result = HolidayService.AddHoliday(model).Result;
            if (result == 1)
            {
                TempData["SuccessMessage"] = Messages.AddSuccessfully;
                return Json(new { isErr = false, errMsg = Messages.AddSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            return Json(result == -1 ? new { isErr = true, errMsg = Messages.ExistError } : new { isErr = true, errMsg = Messages.AddError }, JsonRequestBehavior.AllowGet);
        
        }

        [AjaxOnly]
        public ActionResult Edit( int id)
        {
            var holiday = HolidayService.GetHolidayById(id).Result;

            var model = new HolidayBo
            {
                Name = holiday.Name,
                //SessionList = HolidayService.GetSessionList(SessionItems.SchoolId).Result,
                ToDate = holiday.ToDate,
                FromDate = holiday.FromDate,
                StatusId = holiday.StatusId,
                Description = holiday.Description,
                FromDateStr = holiday.FromDate.HasValue ? holiday.FromDate.Value.ToFormattedDate() : "",
                ToDateStr = holiday.ToDate.HasValue ? holiday.ToDate.Value.ToFormattedDate() : ""

            };

            return PartialView(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(HolidayBo model)
        {

            DateTime fdate;
            DateTime.TryParseExact(model.FromDateStr, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out fdate);
            DateTime tdate;
            DateTime.TryParseExact(model.ToDateStr, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out tdate);
            model.FromDate = fdate;
            model.ToDate = tdate;
            if (!ModelState.IsValid)
            {
               // model.ListOfSchools = Schools;
                return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            model.LastUpdatedBy = SessionItems.UserId;
            model.LastUpdatedOn = DateTime.Now;
            model.SessionId = SessionItems.SessionId;
            var result = HolidayService.UpdateHoliday(model).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(new { isErr = false, msg = Messages.UpdateSuccessfully }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateStatus(int id, int statusId)
        {
            var result = HolidayService.UpdateStatus(id, statusId, SessionItems.UserId).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(result);
        }


    }
}
