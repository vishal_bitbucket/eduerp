﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;

namespace EduErp.Web.Controllers
{
    public class MessageController : BaseController
    {
        //
        // GET: /Message/

        public ActionResult Index()
        {
            return View(MessageService.GetMessages(SessionItems.SchoolId).Result);
        }

        public ActionResult Add()
        {
            MessageBo model = new MessageBo
            {
                ListOfClasses = MessageService.GetClasses(SessionItems.SchoolId).Result,
                ListOfTemplateText = MessageService.GetTemplateList().Result

            };
            model.ListOfClasses.Insert(0, new BO.Common() { Id = 0, Name = "All" });
            //call  getstudents method 
            // model.StudentLists = GetStudents(model.ClassId??0, model.SectionId??0);
            // GetStudentListForMessage(model.ClassId ?? 0, model.SectionId ?? 0);
            return View(model);
        }

        [HttpPost]
        public ActionResult GetStudentListForMessage(int classId, int sectionId)
        {
            MessageBo model = new MessageBo { StudentLists = GetStudents(classId, sectionId) };
            return PartialView("_MessageTemplate", model);
        }


        [NonAction]
        public List<StudentList> GetStudents(int classId, int sectionId)
        {
            var studentLists = MessageService.GetStudentsForMessage(classId, sectionId, SessionItems.SchoolId).Result;
            return studentLists;
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(MessageBo model)
        {

            //DateTime mdate;
            //DateTime.TryParseExact(model.MessageDateStr, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out mdate);
            //model.MessageSentDate = mdate;

            var mtime = Convert.ToDateTime(model.MessageTimeStr);
            //TimeSpan.TryParseExact(model.MessageTimeStr, "hh:mm tt", CultureInfo.InvariantCulture, TimeSpanStyles.None,out mtime);
            model.MessageSentTime = TimeSpan.FromTicks(mtime.TimeOfDay.Ticks);
            if (!ModelState.IsValid)
            {
                model.ListOfClasses = StudentService.GetClasses(SessionItems.SchoolId).Result;
                model.ListOfClasses.Insert(0, new BO.Common() { Id = 0, Name = "All" });
                return Json(new { isErr = true, errMsg = Messages.AddError }, JsonRequestBehavior.AllowGet);
            }
            if (!model.IsSendLater)
            {
                model.MessageSentDate = DateTime.Now;
                model.MessageSentTime = DateTime.Now.TimeOfDay;
            }
            model.ListOfClasses = MessageService.GetClasses(SessionItems.SchoolId).Result;
            model.StatusId = (int)UserStatus.Active;
            model.CreatedBy = SessionItems.UserId;
            model.CreatedOn = DateTime.Now;
            model.SchoolId = SessionItems.SchoolId;
            model.ListOfTemplateText = MessageService.GetTemplateList().Result;
            var str = (from item in model.StudentLists where item.Selected select MessageService.GetNumberByStudentId(item.StudentIdForMessage).Result).ToList();
            var count = model.StudentLists.Count(item => item.Selected);
            model.MsgCount = count;
            var leftsms = MessageService.LeftMessage(SessionItems.SchoolId).Result;
            if (leftsms >= model.MsgCount)
            {
                for (int i = 0; i < str.Count;i++ )
                {
                    if (str[i] != "")
                    {
                        var smsResult = SendSms.SendTextSms(string.Join(",", str[i].ToString()), model.Message);
                        if (smsResult.ErrorMessage == "Done")
                        {

                            TempData["SuccessMessage"] = Messages.AddSuccessfully;
                        }
                    }
                }
            }
            else
            {
                // TempData["ErrMessage"] = Messages.MessageLimitExceedError;
                model.ListOfClasses = StudentService.GetClasses(SessionItems.SchoolId).Result;
                return Json(new { isErr = true, errMsg = Messages.MessageLimitExceedError }, JsonRequestBehavior.AllowGet);
                }
            var result = MessageService.AddMessage(model).Result;
            if (result == 1)
            {

                TempData["SuccessMessage"] = Messages.AddSuccessfully;
                return Json(new { isErr = false, msg = Messages.AddSuccessfully }, JsonRequestBehavior.AllowGet);
            }
           model.ListOfClasses = StudentService.GetClasses(SessionItems.SchoolId).Result;
           model.ListOfClasses.Insert(0, new BO.Common() { Id = 0, Name = "All" });
           return Json(new { isErr = true, errMsg = Messages.AddError }, JsonRequestBehavior.AllowGet);
        }



        [AjaxOnly]
        public JsonResult GetSectionList(string classId)
        {
            var sectionList = (from a in MessageService.GetSections(int.Parse(classId)).Result
                               select new SelectListItem()
                               {
                                   Text = a.Name,
                                   Value = a.Id.ToString()
                               }).ToList();

            return Json(sectionList);
        }

        //[AjaxOnly]
        //public JsonResult GetStudentsList(int classId,int sectionId )
        //{

        //  // call get student list
        //    var result = GetStudents(classId, sectionId);
        //   // GetStudentListForMessage(classId, sectionId);
        //    return Json(result);

        //}

        [AjaxOnly]
        public JsonResult GetStudentsListBySectionId(string sectionId)
        {
            var studentsList = (from s in MessageService.GetStudentsBySectionId(int.Parse(sectionId)).Result
                                select new SelectListItem()
                                {
                                    Text = s.Name,
                                    Value = s.Id.ToString()
                                }).ToList();
            return Json(studentsList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateStatus(int id, int statusId)
        {
            var result = MessageService.UpdateStatus(id, statusId, SessionItems.UserId).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(result);
        }

    }
}
