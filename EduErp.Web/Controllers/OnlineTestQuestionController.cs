﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;
using EduErp.BLL;
using System.IO;

namespace EduErp.Web.Controllers
{
    public class OnlineTestQuestionController : BaseController
    {
        //
        // GET: /OnlineTestQuestion/

        public ActionResult Index()
        {
            var subjectList = OnlineTestQuestionService.GetSubjects(SessionItems.SchoolId).Result;
            var OnlineTestQuestion = new OnlineExamTestQuestionBo
            {
                ListOfSubject = subjectList,
                
            };
            return View(OnlineTestQuestionService.GetOnlineTestQuestion(SessionItems.SchoolId).Result);
        }
        public ActionResult Add()
        {
            var ListOfQuestionTypeId = OnlineTestQuestionService.GetQuestionTypeId().Result;
            var subjectList = OnlineTestQuestionService.GetSubjects(SessionItems.SchoolId).Result;
            var OnlineTestQuestion = new OnlineExamTestQuestionBo
            {
                ListOfSubject = subjectList,
                ListOfQuestionType = ListOfQuestionTypeId,
            };
            return View(OnlineTestQuestion);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(OnlineExamTestQuestionBo model, HttpPostedFileBase file)
        {
            if (!ModelState.IsValid)
            {
                //model.ListOfSchools = Schools;
                return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            if (file != null && file.ContentLength > 0)
            {
                var uploadDir = "~/Temp";
                var imagePath = Path.Combine(Server.MapPath(uploadDir), file.FileName);
                var imageUrl = Path.Combine(uploadDir, file.FileName);
                file.SaveAs(imagePath);
                model.ImagePath = imageUrl;
            }
            model.SchoolId = SessionItems.SchoolId;
            model.CreatedOn = DateTime.Now;
            model.CreatedBy = SessionItems.UserId;
            model.Status = (int)UserStatus.Active;
            //model.ListOfSchools = Schools;
            var result = OnlineTestQuestionService.AddOnlineTestQuestion(model).Result;
            if (result > 0)
            {
                TempData["SuccessMessage"] = Messages.AddSuccessfully;
                return Json(new { isErr = false, errMsg = Messages.AddSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            return Json(result == -1 ? new { isErr = true, errMsg = Messages.ExistError } : new { isErr = true, errMsg = Messages.AddError }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateStatus(int id, int status)
        {
            var result = OnlineTestQuestionService.UpdateStatus(id, status, SessionItems.UserId).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(result);
        }
        [AjaxOnly]
        public ActionResult Edit(int id)
        {
            var result = OnlineTestQuestionService.GetOnlineTestQuestionById(id).Result;
            var ListOfQuestionTypeId = OnlineTestQuestionService.GetQuestionTypeId().Result;
            var subjectList = OnlineTestQuestionService.GetSubjects(SessionItems.SchoolId).Result;
            var model = new OnlineExamTestQuestionBo
            {
                
               Description= result.Description,
               Questions = result.Questions,
               SubjectId = result.SubjectId,

                ListOfQuestionType = ListOfQuestionTypeId,
                ListOfSubject = subjectList,
                //ListOfQuestionType = result.ListOfQuestionType,
               QuestionTypeId = result.QuestionTypeId,
               Ismcq = result.Ismcq,
                Status = result.Status,
                SchoolId = result.SchoolId
            };
            return PartialView(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(OnlineExamTestQuestionBo model)
        {

            if (!ModelState.IsValid)
            {
                //model.ListOfSchools = Schools;
                return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            model.SchoolId = SessionItems.SchoolId;
            model.Status = (int)UserStatus.Active;
            model.LastUpdatedBy = SessionItems.UserId;
            model.LastUpdatedOn = DateTime.Now;
            var result = OnlineTestQuestionService.UpdateOnlineTestQuestion(model).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(new { isErr = false, msg = Messages.UpdateSuccessfully }, JsonRequestBehavior.AllowGet);

        }
    }
}
