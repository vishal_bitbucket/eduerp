﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;
using EduErp.Web.Models;

namespace EduErp.Web.Controllers
{
    public class SessionController : BaseController
    {
        public ActionResult Index()
        {
            return View(SessionService.GetSessions(SessionItems.SchoolId).Result);
        }

        [AjaxOnly]
        public ActionResult Add()
        {
            return PartialView(new SessionVm { ListOfSchools = Schools });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(SessionVm model)
        {
            //DateTime fdate;
            //DateTime.TryParseExact(model.FromDateStr, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out fdate);
            //DateTime tdate;
            //DateTime.TryParseExact(model.ToDateStr, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out tdate);
            //model.FromDate = fdate;
            //model.ToDate = tdate;
            if (!ModelState.IsValid)
            {
                model.ListOfSchools = Schools;
                return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            model.SchoolId = SessionItems.SchoolId;
            model.CreatedBy = SessionItems.UserId;
            model.CreatedOn = DateTime.Now;
            model.StatusId = (int)UserStatus.Active;
            var result = SessionService.AddSession(model).Result;
            if (result == 1)
            {
                TempData["SuccessMessage"] = Messages.AddSuccessfully;
                return Json(new { isErr = false, errMsg = Messages.AddSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            if (result == -2)
            {
                TempData["SuccessMessage"] = Messages.ActiveSessionError;
                return Json(new { isErr = true, errMsg = Messages.ActiveSessionError }, JsonRequestBehavior.AllowGet);
            }
            return Json(result == -1 ? new { isErr = true, errMsg = Messages.ExistError } : new { isErr = true, errMsg = Messages.AddError }, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        public ActionResult Edit(int id)
        {
            var session = SessionService.GetSessionById(id).Result;
            var model = new SessionVm
            {
                ListOfSchools = Schools,
                Id = session.Id,
                SchoolId = session.SchoolId,
                Name = session.Name,
                FromDate = session.FromDate,
                ToDate = session.ToDate,
                StatusId = session.StatusId,
                FromDateStr = session.FromDate.ToString(CURRENT_DATEFORMAT),
                ToDateStr = session.ToDate.ToString(CURRENT_DATEFORMAT)
            };
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SessionVm model)
        {
            //DateTime fdate;
            //DateTime.TryParseExact(model.FromDateStr, CURRENT_DATEFORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out fdate);
            //DateTime tdate;
            //DateTime.TryParseExact(model.ToDateStr, CURRENT_DATEFORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out tdate);
            //model.FromDate = fdate;
            //model.ToDate = tdate;
            if (!ModelState.IsValid)
            {
                model.ListOfSchools = Schools;
                return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            model.LastModifiedBy = SessionItems.UserId;
            model.LastModifiedOn = DateTime.Now;
            var result = SessionService.UpdateSession(model).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(new { isErr = false, msg = Messages.UpdateSuccessfully }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateStatus(int sessionId, int statusId)
        {
            var result = SessionService.UpdateStatus(sessionId, statusId, SessionItems.UserId,SessionItems.SchoolId).Result;
            if (result == 1)
            {
                TempData["SuccessMessage"] = Messages.AddSuccessfully;
                return Json(new { isErr = false, errMsg = Messages.AddSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            return Json(result == -2 ? new { isErr = true, errMsg = Messages.SessionInactiveRestriction } : new { isErr = true, errMsg = Messages.UpdateError }, JsonRequestBehavior.AllowGet);
        }

    }
}
