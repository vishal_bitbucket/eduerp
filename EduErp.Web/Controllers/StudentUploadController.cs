﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;
using System.Text.RegularExpressions;

namespace EduErp.Web.Controllers
{
    public class StudentUploadController : BaseController
    {
        //
        // GET: /StudentUpload/

        public ActionResult Index()
        {
            //var session = StudentService.GetSessions(SessionItems.SchoolId).Result;
            var classes = StudentService.GetClasses(SessionItems.SchoolId).Result;
            var model = new StudentUploadBo
            {
                ListOfClasses = classes,
                //ListOfSessions = session
            };
            return View(model);
        }
        [HttpGet]
        public FilePathResult GetExportUnbilledRequestsAndExport()
        {
             
               string fileInfo = Server.MapPath("~/Content/ExcelFile/StudentTemplate.xls");
               string fileName = "StudentTemplate" + DateTime.Now.ToShortTimeString() + ".xls";
               return File(fileInfo, "application/vnd.ms-excel", fileName);
                                  
            //DataTable dt = new DataTable();
            //dt.Columns.Add("FirstName", typeof(string));
            //dt.Columns.Add("MiddleName", typeof(string));
            //dt.Columns.Add("LastName", typeof(string));
            //dt.Columns.Add("Email", typeof(string));
            //dt.Columns.Add("StudentMobile", typeof(string));
            //dt.Columns.Add("StudentOtherInfo", typeof(string));
            //dt.Columns.Add("GuardianAddress", typeof(string));
            //dt.Columns.Add("GuardianOccupation", typeof(string));
            //dt.Columns.Add("GuardianRelationWithStudent", typeof(string));
            //dt.Columns.Add("GuardianMobileNo", typeof(string));
            //dt.Columns.Add("GuardianName", typeof(string));
            //dt.Columns.Add("MessageMobileNumber", typeof(string));
            //dt.Columns.Add("MotherName", typeof(string));
            //dt.Columns.Add("FatherName", typeof(string));
            //dt.Columns.Add("CorrAddress", typeof(string));
            //dt.Columns.Add("PermanentAddress", typeof(string));
            //dt.Columns.Add("ParentMobileNo", typeof(string));
            //dt.Columns.Add("ParentContactNo", typeof(string));
            //dt.Columns.Add("PermanentPinCode", typeof(string));
            //dt.Columns.Add("DOB", typeof(DateTime));
            //dt.Columns.Add("DOJ", typeof(DateTime));
            //dt.Columns.Add("Uid", typeof(string));
            //dt.Columns.Add("CreatedOn", typeof(DateTime));
            //dt.Columns.Add("CreatedBy", typeof(int));


            //if (dt.Columns.Count <= 0) return "#FAIL#";
            //StudentUploadService.ExportToExcelFromDataTable(dt, "Records");
            //return "#SUCCESS#";
        }

       

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(StudentUploadBo model, HttpPostedFileBase file)
        {
            if (!ModelState.IsValid && file == null)
            {
                //model.ListOfSessions = StudentService.GetSessions(SessionItems.SchoolId).Result;
                model.ListOfClasses = StudentService.GetClasses(SessionItems.SchoolId).Result;
                return View(model);
            }

            if (file == null)
            {
               // model.ListOfSessions = StudentService.GetSessions(SessionItems.SchoolId).Result;
                model.ListOfClasses = StudentService.GetClasses(SessionItems.SchoolId).Result;
                return View(model);
              }

        var path = string.Concat(Server.MapPath("~/Content/UploadFiles/" + file.FileName));
            file.SaveAs(path);

            DataTable result = StudentUploadService.GetExcelData(path).Result;
            var errlist = new List<ErrorDetail>();
            #region find out fields in datatable with errorns records.

            foreach (DataRow dts in result.Rows)
            {
                
                //var email = dts["Email"];
                //if (!string.IsNullOrEmpty( Convert.ToString(email) ))
                //{
                //    var res = IsValidEmail(Convert.ToString(email));
                //    if (!res)
                //   {
                //       var errorDetail = new ErrorDetail
                //        {
                //           StudentName = dts["FirstName"].ToString(),
                //           Error = "Please enter valid Email",
                //           Description = dts.GetColumnError("Email")
                //        };
                //        errlist.Add(errorDetail);
                //    }
                //}

                //int mobileno;
                //var mobile = dts.Field<string>("MessageMobileNumber");
                //if (mobile != null)
                //{
                //    if (int.TryParse(mobile, out mobileno) && mobile.Length == 10)
                //    {
                //        var errorDetail = new ErrorDetail
                //        {
                //            StudentName = dts["FirstName"].ToString(),
                //            Error = "Please check mobile number",
                //            Description = dts.GetColumnError("StudentMobile")
                //        };
                //        errlist.Add(errorDetail);
                //    }
                //}

                DateTime dobdateTime;
                var date = Convert.ToString( dts["DOB(mm/dd/yyyy)"]);
                if(!string.IsNullOrEmpty(date))
                {
                    if (!DateTime.TryParse(date, out dobdateTime))
                {
                    var errorDetail = new ErrorDetail
                    {
                        StudentName = dts["FirstName"].ToString(),
                        Error = "Please enter valid Date Of Birth",
                        Description = dts.GetColumnError("DOB")
                    };
                    errlist.Add(errorDetail);


                }
                }
                DateTime dateTime;
                string datejoin = Convert.ToString( dts["DOJ(mm/dd/yyyy)"]);
               if (!string.IsNullOrEmpty(datejoin))
                {
                    if (!DateTime.TryParse(datejoin, out dateTime))
                {
                    var errorDetail = new ErrorDetail
                    {
                        StudentName = dts["FirstName"].ToString(),
                        Error = "Please enter valid Date of joining",
                        Description = dts.GetColumnError("DOJ")
                    };
                    errlist.Add(errorDetail);


                }
                }
                var firstName = dts.Field<string>("FirstName");
                if (firstName == null)
                {
                    var errorDetail = new ErrorDetail
                    {
                        StudentName = dts["FirstName"].ToString(),
                        Error = "Please enter Required Name",
                        Description = dts.GetColumnError("FirstName")
                    };
                    errlist.Add(errorDetail);
                }
            }
            #endregion  

            #region get left Count of student no out of total student limit
            var leftStudent = StudentService.LeftStudent(SessionItems.SchoolId).Result;
            #endregion

            model.ErrorCount = leftStudent;
              if (result.Rows.Count > leftStudent)
              {

                TempData["ExceedError"] = "Your student limit exceeds from max student. Your left student is" +" "+ model.ErrorCount;
                //model.ListOfSessions = StudentService.GetSessions(SessionItems.SchoolId).Result;
                model.ListOfClasses = StudentService.GetClasses(SessionItems.SchoolId).Result;
                return View(model);
                  
              }
            if (errlist.Count == 0)
            {
                int count = 1;
                foreach (DataRow item in result.Rows)
                {
                    StudentBo student = new StudentBo();
                    student.FirstName = Convert.ToString(item["FirstName"]);
                    student.LastName = Convert.ToString(item["LastName"]);
                    student.MiddileName = Convert.ToString(item["MiddleName"]);
                    student.MotherName = Convert.ToString(item["MotherName"]);
                    student.FatherName = Convert.ToString(item["FatherName"]);
                    student.Email = Convert.ToString(item["Email"]);
                    student.StudentMobile = Convert.ToString(item["MessageMobileNumber"]);
                    student.StudentOtherInfo = Convert.ToString(item["StudentOtherInfo"]);
                    student.GuardianAddress = Convert.ToString(item["GuardianAddress"]);
                    student.GuardianMobile = Convert.ToString(item["GuardianMobileNo"]);
                    student.GuardianName = Convert.ToString(item["GuardianName"]);
                    student.GuardianOccupation = Convert.ToString(item["GuardianOccupation"]);
                    student.GuardianRelation = Convert.ToString(item["GuardianRelationWithStudent"]);
                    student.CuurentAddress = Convert.ToString(item["CorrAddress"]);
                    student.PermanentAddress = Convert.ToString(item["PermanentAddress"]);
                    student.ParentMobileNumber = Convert.ToString(item["ParentMobileNo"]);
                    student.ParentContactNo = Convert.ToString(item["ParentContactNo"]);
                    student.PermanentPinCode = Convert.ToString(item["PermanentPinCode"]);
                    var dob = item["DOB(mm/dd/yyyy)"];
                   if(!(dob is DBNull))
                   {
                    student.Dob =Convert.ToDateTime(item["DOB(mm/dd/yyyy)"]) ;
                   }else{
                       student.Dob = null ;
                   }
                     var doj = item["DOJ(mm/dd/yyyy)"];
                     if (!(doj is DBNull))
                     {
                         student.Doj = Convert.ToDateTime(item["DOJ(mm/dd/yyyy)"]);
                     }
                     else
                     {
                         student.Doj = null;
                     }
                    student.Uid = Convert.ToString(item["Aadhar"]);
                    student.SessionId = SessionItems.SessionId; // Convert.ToInt32(SessionItems.SessionId);
                    student.SectionId = Convert.ToInt32(model.SectionId);
                    student.ClassId = Convert.ToInt32(model.ClassId);
                    student.CreatedBy = Convert.ToInt32(SessionItems.UserId);
                    student.CreatedOn = Convert.ToDateTime(DateTime.Now);
                    student.SchoolId = Convert.ToInt32(SessionItems.SchoolId);
                    student.StatusId = Convert.ToInt32((int)UserStatus.Active);
                  
                    //if (leftStudent > 0)
                    //{
                        var result1 = BLL.StudentService.AddStudent(student).Result;
                        if (result1 == 1)
                        {
                            model.Count = count++;
                            //model.ErrorDetailsList = null;
                        }
                   // }
                   // else {
                       // model.ErrorCount = -1;
                   // }
                }

                if (model.Count !=0)
                {
                    TempData["Success"] = "record(s) uploaded successfully";
                   // model.ListOfSessions = StudentService.GetSessions(SessionItems.SchoolId).Result;
                    model.ListOfClasses = StudentService.GetClasses(SessionItems.SchoolId).Result;
                    return View(model);
                }
               
            }
            else {
                model.ErrorDetailsList = errlist;
               // model.ListOfSessions = StudentService.GetSessions(SessionItems.SchoolId).Result;
                model.ListOfClasses = StudentService.GetClasses(SessionItems.SchoolId).Result;
                return View(model);
            }
            //if (result1 == 1)
            //{
            //    TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            //    return Json(new { isErr = false, errMsg = Messages.UpdateSuccessfully }, JsonRequestBehavior.AllowGet);
            //}

           // model.ListOfSessions = StudentService.GetSessions(SessionItems.SchoolId).Result;
            model.ListOfClasses = StudentService.GetClasses(SessionItems.SchoolId).Result;
            return View(model);
        }

        [AjaxOnly]
        public JsonResult GetSectionList(string classId)
        {
            var sectionList = (from a in StudentService.GetSections(int.Parse(classId)).Result
                               select new SelectListItem()
                               {
                                   Text = a.Name,
                                   Value = a.Id.ToString()
                               }).ToList();

            return Json(sectionList);
        }
        public static bool IsValidEmail(string emailaddress)
        {
            return new Regex(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$").IsMatch(emailaddress);
        }
    }
}
