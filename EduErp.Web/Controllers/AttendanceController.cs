﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;
using EduErp.Web.Models;
using EduErp.Web.Utilities;

namespace EduErp.Web.Controllers
{
    public class AttendanceController : BaseController
    {
        public ActionResult Index(int? classId, int? sectionId, bool? ssdr, string sd, string ed)
        {
            return View(GetAttendance(SessionItems.SessionId, classId??0, sectionId??0, ssdr, sd, ed));
        }

        public ActionResult Add()
        {
            var model = new AttendanceBo
            {
                SchoolId = SessionItems.SchoolId,
               // SessionId = SessionItems.SessionId,
               // SessionList = StudentService.GetSessions(SessionItems.SchoolId).Result,
                ClassList = StudentService.GetClasses(SessionItems.SchoolId).Result,
                DateStr = DateTime.Now.ToFormattedDate()
            };
            var firstOrDefault = model.ClassList.FirstOrDefault();
            if (firstOrDefault != null)
                model.SectionList = StudentService.GetSections(firstOrDefault.Id).Result;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(AttendanceBo model, string Go)
        {
           // model.SessionList = StudentService.GetSessions(SessionItems.SchoolId).Result;
            model.ClassList = StudentService.GetClasses(SessionItems.SchoolId).Result;
            model.SessionId = SessionItems.SessionId;
            var firstOrDefault = model.ClassList.FirstOrDefault();
            if (firstOrDefault != null)
                model.SectionList = StudentService.GetSections(firstOrDefault.Id).Result;
            
            if (string.IsNullOrEmpty(Go))
            {
                DateTime fdate;
                DateTime.TryParseExact(model.DateStr, CURRENT_DATEFORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out fdate);
                model.Date = fdate;
                var str = (from item in model.AttendanceTemplateBo.AttendanceLists where item.DayStatusId==(int)DayStatus.Absent select MessageService.GetNumberByStudentId(item.StudentId).Result).ToList();
                var count = model.AttendanceTemplateBo.AttendanceLists.Count(item => item.DayStatusId==(int)DayStatus.Absent);
                model.MsgCount = count;
                var result = AttendanceService.AddAttendance(model, SessionItems.SchoolId);
                if (result.Result)
                {
                    
                    var leftsms = MessageService.LeftMessage(SessionItems.SchoolId).Result;
                    if (leftsms >= model.MsgCount)
                    {
                        foreach (var item in FeeTransactionService.GetAutomateMessageIds(SessionItems.SchoolId).Result)
                        {
                              
                                if (item.MessageAutomateId == (int)AutomateMesageFor.Absent)
                                {
                                    for (int i = 0; i < str.Count; i++)
                                    {
                                        if (str[i] != "")
                                        {
                                            var smsResult = SendSms.SendTextSms(string.Join(",", str[i].ToString()), item.TemplateMessage);
                                            if (smsResult.ErrorMessage == "Done")
                                            {
                                                TempData["SuccessMessage"] = Messages.AddSuccessfully;
                                            }
                                        }
                                }
                            }
                        }
                    }
                    else
                    {
                        // TempData["ErrMessage"] = Messages.MessageLimitExceedError;
                        return Json(new { isErr = true, errMsg = Messages.MessageLimitExceedError }, JsonRequestBehavior.AllowGet);
                    }
                    TempData["SuccessMessage"] = Messages.AddSuccessfully;
                    return Json(new { isErr = false, errMsg = Messages.AddSuccessfully }, JsonRequestBehavior.AllowGet);
                }

                TempData["ErrMessage"] = Messages.AddError;
                return Json(new { isErr = false, errMsg = Messages.AddError }, JsonRequestBehavior.AllowGet);
               
                
            }
            model.AttendanceTemplateBo = AttendanceService.GetStudentAttendance(model.SchoolId, model.SessionId, model.ClassId, model.SectionId).Result;
            return View(model);
            //if (!ModelState.IsValid)
            //{
                
            //    return View(model);
            //}
            //return Json(new { isErr = false, msg = Messages.AddSuccessfully }, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult GetStudentList(int sessionId, int classId, int sectionId)
        //{
        //    var model = new AttendanceBo
        //    {
        //        SchoolId = SessionItems.SchoolId,
        //        SectionId = SessionItems.SessionId,
        //        SessionList = StudentService.GetSessions(SessionItems.SchoolId).Result,
        //        ClassList = StudentService.GetClasses(SessionItems.SchoolId).Result,
        //        AttendanceTemplateBo = AttendanceService.GetStudentAttendance(SessionItems.SchoolId, sessionId, classId, sectionId).Result
        //    };
        //    var firstOrDefault = model.ClassList.FirstOrDefault();
        //    if (firstOrDefault != null)
        //        model.SectionList = StudentService.GetSections(firstOrDefault.Id).Result;
        //    return PartialView("Add", model);
        //}

        public ActionResult Update(int studentId, int statusId, string date)
        {
            var model = new EditAttendanceVm
            {
                StudentId = studentId,
                DayStatusId = statusId,
                DayStatusList = AttendanceService.GetDayStatus().Result,
                SessionId = SessionItems.SessionId,
                DateStr = date
            };
            return PartialView(model);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public JsonResult Update(EditAttendanceVm model)
        {
            DateTime date;
            DateTime.TryParseExact(model.DateStr, CURRENT_DATEFORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out date);
            var data = new AttendanceMaster
            {
                StudentId = model.StudentId,
                Date = date,
                DayStatusId = model.DayStatusId,
                LastModifiedBy = SessionItems.UserId,
                LastModifiedOn = DateTime.Now,
                Remark = model.Remark,
                SchoolId = SessionItems.SchoolId,
                SessionId = SessionItems.SessionId
            };
            var result = AttendanceService.UpdateAttendance(data).Result;
            if (!result) return Json(new { isErr = true, errMsg = Messages.UpdateError }, JsonRequestBehavior.AllowGet);
            TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            return Json(new {isErr = false, errMsg = Messages.UpdateSuccessfully}, JsonRequestBehavior.AllowGet);
        }

        private static SearchAttendancVM GetAttendance(int sessionId, int classId, int sectionId, bool? ssdr, string sd, string ed)
        {
            var model = new SearchAttendancVM
            {
                SearchSpecificDateRange = ssdr.HasValue && ssdr.Value,
                StartDate = DateTime.Now.AddDays(-7).Date,
                EndDate = DateTime.Now.Date,
                StudentListItems = StudentService.GetStudents(SessionItems.SchoolId).Result,
                SchoolId = SessionItems.SchoolId,
                SessionId = sessionId == 0 ? SessionItems.SessionId : sessionId,
                SessionList = StudentService.GetSessions(SessionItems.SchoolId).Result,
                ClassList = StudentService.GetClasses(SessionItems.SchoolId).Result,
                AttendanceVM = new AttendanceVm{AttendanceInfoVMs = new List<AttendanceInfoVm>()}
            };
            var firstOrDefault = model.ClassList.FirstOrDefault();
            model.SectionList = firstOrDefault != null ? StudentService.GetSections(firstOrDefault.Id).Result : new List<BO.Common>();
            if (sessionId == 0 || classId == 0 || sectionId == 0)
                return model;
            var predicate = PredicateBuilder.True<AttendanceList>();
            DateTime startDate;
            var isStartDateValid = DateTime.TryParseExact(sd, CURRENT_DATEFORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out startDate);
            
            DateTime endDate;
            var isEndDateValid = DateTime.TryParseExact(ed, CURRENT_DATEFORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out endDate);

            if (isStartDateValid && isEndDateValid)
            {
                model.StartDate = startDate;
                model.EndDate = endDate;
            }
            predicate = predicate.And(a => a.AttendanceDate >= model.StartDate && a.AttendanceDate <= model.EndDate);
            var attendanceInfoBos = AttendanceService.GetAllAttendace(model.StartDate.Value, model.EndDate.Value, SessionItems.SchoolId, sessionId, classId, sectionId).Result;

            var result = attendanceInfoBos.AsQueryable().Where(predicate).ToList();

            var dateTimes = result.GroupBy(a => a.AttendanceDate.Date).Select(a => a.First()).Select(a => a.AttendanceDate.Date).OrderByDescending(a => a.Date).ToList();

            var infoBos = result.GroupBy(a => a.StudentId).Select(a => a.First()).ToList();

            dateTimes = dateTimes.OrderBy(a => a.Date).ToList();

            var attendanceVM = new AttendanceVm
            {
                HeaderDates = dateTimes,
                AttendanceInfoVMs = new List<AttendanceInfoVm>()
                //DayStatuses = BLL.PublicLayer.GetDayStatuses().Result

            };
            foreach (var attendanceInfoBo in infoBos)
            {
                var attendanceInfoVM = new AttendanceInfoVm
                {
                    StudentName = attendanceInfoBo.StudentName,
                    StudentId = attendanceInfoBo.StudentId,
                    AttendanceBos = new List<AttendanceMaster>()
                };

                foreach (var dateTime in dateTimes)
                {
                    var infoBo = result.FirstOrDefault(a => a.StudentId == attendanceInfoBo.StudentId && a.AttendanceDate.Date == dateTime);
                    var attendanceBo = new AttendanceMaster
                    {
                        //Id = attendanceInfoBo.Id,
                        StudentId = attendanceInfoBo.StudentId,
                        DayStatusId = 1,
                        SessionId = model.SessionId
                    };
                    if (infoBo != null)
                    {
                        //attendanceBo.Id = infoBo.ID;
                        attendanceBo.Date = infoBo.AttendanceDate;
                        attendanceBo.DayStatusId = infoBo.DayStatusId;
                        //attendanceBo.InTime = infoBo.InTime.GetValueOrDefault();
                        //attendanceBo.OutTime = infoBo.OutTime.GetValueOrDefault();
                    }

                    attendanceInfoVM.AttendanceBos.Add(attendanceBo);
                }

                attendanceVM.AttendanceInfoVMs.Add(attendanceInfoVM);
            }
            model.AttendanceVM = attendanceVM;
            return model;
        }

    }
}
