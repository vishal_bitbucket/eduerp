﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;
using EduErp.Web.Models;
using EduErp.Web.Utilities;

namespace EduErp.Web.Controllers
{
    public class ReportController : BaseController
    {
        //
        // GET: /Report/

        public ActionResult Index()
        {
            var permission = UserService.GetPermissionByUserId(SessionItems.UserId, SessionItems.SchoolId).Result;
            //if(SessionItems.InstituteTypeId==)
            SessionItems.MenuItems = permission.MenuList;
            SessionItems.UserRights = permission.UserRights;
            return View();
        }

        public ActionResult StudentReport()
        {
            var session = StudentService.GetSessions(SessionItems.SchoolId).Result;
            var classes = StudentService.GetClasses(SessionItems.SchoolId).Result;
            var model = new StudentBo
              {
                ListOfClasses = classes,
                 ListOfSessions = session,
                ListOfState = StateList,
                ListOfStatus = StudentService.GetStatus().Result,
               ListOfAdmissionStatus=ReportServices.GetAdmissionStatus().Result
              };
            model.ListOfStatus.Insert(0, new BO.Common() { Id = 0, Name = "All" });
            model.ListOfClasses.Insert(0, new BO.Common() { Id = 0, Name = "All" });
            model.ListOfAdmissionStatus.Insert(0, new BO.Common() { Id = 0, Name = "All" });
            return View(model);
        }
        public ActionResult FeesReport()
        {
            ReportBo model = new ReportBo
            {
                ListOfPayStatus = ReportServices.GetPayStatus().Result,
                ListOfMonthes = ReportServices.GetMonths().Result,
                ListOfSessions = SessionlList,
                ListOfClasses = ReportServices.Getclasses(SessionItems.SchoolId).Result
            };
            model.ListOfPayStatus.Insert(0, new BO.Common() { Id = 0, Name = "All" });
           
            return View(model);
        }
        public ActionResult GetFeeTransactionDetailByStudentId(int studentId, string studentName)
        {
            TempData["StudentName"] = studentName;
            var result = FeeTransactionService.GetFeeTransactionDetailByStudentId(studentId, SessionItems.SessionId).Result;
            // for(int i=0;i< result.FeetransactionDetailList.Count;i++)
            //{
            //    result.FeetransactionDetailList[i].TotalPayableAmount=(result.FeetransactionDetailList[i].TotalPayableAmount);
            //}
            return PartialView("~/views/FeeTransaction/_FeeTransactionDetailById.cshtml", result);
        }
        public ActionResult ExamReport()
        {
            ExamBo model = new ExamBo
            {
                ListOfExams = ReportServices.GetExams(SessionItems.SchoolId).Result,
                ListOfSessions = SessionlList,
                ListOfStatus = ReportServices.GetResultStatus().Result
            };
            model.ListOfStatus.Insert(0, new BO.Common() { Id = 0, Name = "All" });
            return View(model);
        }
        public ActionResult ExpenseReport()
        {
            ExpensesBo model = new ExpensesBo
            {
                ListOfMonths = ReportServices.GetMonths().Result,
                ListOfSessions = SessionlList,
                ExpensesList = ExpensesService.GetExpensesType().Result,
            };
            return View(model);
        }

        //public JsonResult GetExpensesChart()
        //{
        //    var data = new List<ExpensesChart>();
        //    data.Add(new ExpensesChart { Name = "Employee Salary", Total = 10000 });
        //    data.Add(new ExpensesChart { Name = "Goods", Total = 5000 });
        //    data.Add(new ExpensesChart { Name = "Service", Total = 3000 });
        //    return Json(data, JsonRequestBehavior.AllowGet);
        //}
        //public JsonResult GetExamChart()
        //{
        //    var data = new List<ExamChart>();
        //    data.Add(new ExamChart { Name = "Class Test", TotalMarks = 100 });
        //    data.Add(new ExamChart { Name = "Annual-Exam", TotalMarks = 100 });
        //    data.Add(new ExamChart { Name = "Half-yearly", TotalMarks = 100 });
        //    return Json(data, JsonRequestBehavior.AllowGet);
        //}
       
        public JsonResult GetFeesChart()
        {
            var data = new List<FeesChart>();
            data.Add(new FeesChart { ClassName = "Class Ist", TotalFees = 4000 });
            data.Add(new FeesChart { ClassName = "Class 2nd", TotalFees = 6000 });
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetExpensesChart()
        {
            var data = ReportServices.GetTypewiseExpensesChartList(SessionItems.SessionId, SessionItems.SchoolId).Result;
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetExamChart(string examId)
        {
            if(!string.IsNullOrEmpty(examId))
            {
            var data = ReportServices.GetTypeExamWiseChartList(SessionItems.SessionId, SessionItems.SchoolId, int.Parse(examId)).Result;
            return Json(data, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetExamChartStudentWise( string examId , string studentId)
        {
            //if (!string.IsNullOrEmpty(examId))
            //{
            var data = ReportServices.GetTypeStudentWiseChartList(SessionItems.SessionId, SessionItems.SchoolId, int.Parse(examId) , int.Parse(studentId)).Result;
                return Json(data, JsonRequestBehavior.AllowGet);
            //}
           // return Json(null, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AttendenceReport(int? sessionId,int? classId, int? sectionId,int? studentId,int? statusId, bool? ssdr, string sd, string ed)
        {
            return View(GetAttendance(sessionId??0, classId ?? 0, sectionId ?? 0, ssdr, sd, ed));
        }
        public ActionResult FeesFilter()
        {
            ReportBo model = new ReportBo
            {
            ListOfPayStatus = ReportServices.GetPayStatus().Result,
            ListOfMonthes = ReportServices.GetMonths().Result,
            ListOfSessions = SessionlList,
            ListOfClasses = ReportServices.Getclasses(SessionItems.SchoolId).Result
            };
            model.ListOfClasses.Insert(0, new BO.Common() { Id = 0, Name = "All" });
            model.ListOfPayStatus.Insert(0, new BO.Common() { Id = 0, Name = "All" });
            return View(model);
        }


        public JsonResult GetDataForCategoryWiseChart()
        {
            var data = ReportServices.GetStudentCategorywiseChartList(SessionItems.SchoolId).Result;
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDataForSessionWiseChart()
        {
            var data = ReportServices.GetStudentSessionwiseChartList(SessionItems.SchoolId).Result;
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetClassStudentStatusForChart()
        {

            var data = ReportServices.GetStudentStatusChartList(SessionItems.SchoolId).Result;

            //foreach (var item in data.genderDetail)
            //{
            //    if (item.GenderId == 1)
            //    {
            //        data.TotalMaleCount = item.CountNo;
            //    }
            //    else {
            //        data.TotalFemaleMaleCount = item.CountNo;
            //    }
            //}
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetStudentDetailForReport(int classId, int sectionId, int statusId, int admsnstatusId,int sessionId)
        {
            return PartialView("_StudentReportTemplate", StudentService.GetStudentsListForReport(classId, sectionId, statusId, admsnstatusId, sessionId, SessionItems.SchoolId).Result);
        }
        public ActionResult GetPayStutus(int sessionId, int classId, int sectionId, int fromMonthId, int toMonthId, int PayStatusId)
        {
            if (PayStatusId == 0 || PayStatusId == 1 || PayStatusId == 2)
            {
                return PartialView("_PaidTemplate", ReportServices.GetFeeStatus(SessionItems.SchoolId, sessionId, classId, sectionId, fromMonthId, toMonthId, PayStatusId).Result);
            }
            else {
                return PartialView("_BalancedAndSettledTemplate");
            }
        }

        public ActionResult GetExamDetailForReport(int sessionId, int examId, int subjectId, int classId, int sectionId, int studentId)
        {
            return PartialView("_ExamReportTemplate", ReportServices.GetExamListReport(SessionItems.SchoolId,sessionId, examId, subjectId, classId, sectionId, studentId).Result);
           // return Json(true);
        }

        
        public ActionResult GetExpenseDetailForReport(int sessionId, int expenseId, int fromMonthId, int toMonthId)
        {
            return PartialView("_ExpenseReportTemplate",ReportServices.GetExpenseListReport(SessionItems.SchoolId, sessionId, expenseId, fromMonthId, toMonthId).Result);
        }
        

        [AjaxOnly]
        public JsonResult GetSectionList(string classId)
        {
            var sectionList = (from a in FeeTransactionService.GetSections(int.Parse(classId)).Result
                               select new SelectListItem()
                               {
                                   Text = a.Name,
                                   Value = a.Id.ToString()
                               }).ToList();
          // sectionList.Insert(0, new BO.Common() { Id = 0, Name = "All" });
            return Json(sectionList);
        }

        [AjaxOnly]
        public JsonResult GetStudentsListBySectionId(string sectionId)
        {
            var studentsList = (from s in FeeTransactionService.GetStudentsBySectionId(int.Parse(sectionId), SessionItems.SessionId).Result
                                select new SelectListItem()
                                {
                                    Text = s.Name,
                                    Value = s.Id.ToString()
                                }).ToList();
            return Json(studentsList);
        }

        [AjaxOnly]
        public JsonResult GetsubjectByExamId(string examId)
        {
            var subjectsList = (from s in ReportServices.GetSubjectByExamId(int.Parse(examId), SessionItems.SchoolId).Result
                                select new SelectListItem()
                                {
                                    Text = s.Name,
                                    Value = s.Id.ToString()
                                }).ToList();
            return Json(subjectsList);
        }
        [AjaxOnly]
        public JsonResult GetclassBySujectId(string subjectId)
        {
            var classList = (from s in ReportServices.GetClassBySubjectId(int.Parse(subjectId), SessionItems.SchoolId).Result
                                select new SelectListItem()
                                {
                                    Text = s.Name,
                                    Value = s.Id.ToString()
                                }).ToList();
            return Json(classList);
        }
      
        private static SearchAttendancVM GetAttendance(int sessionId, int classId, int sectionId, bool? ssdr, string sd, string ed)
        {
            var model = new SearchAttendancVM
            {
                SearchSpecificDateRange = ssdr.HasValue && ssdr.Value,
                StartDate = DateTime.Now.AddDays(-7).Date,
                EndDate = DateTime.Now.Date,
                StudentListItems = StudentService.GetStudents(SessionItems.SchoolId).Result,
                SchoolId = SessionItems.SchoolId,
                SessionId = sessionId == 0 ? SessionItems.SessionId : sessionId,
                SessionList = StudentService.GetSessions(SessionItems.SchoolId).Result,
                ClassList = StudentService.GetClasses(SessionItems.SchoolId).Result,
               // DateStr = DateTime.Now.ToFormattedDate(),
                DayStatusList = AttendanceService.GetDayStatus().Result,
                AttendanceVM = new AttendanceVm { AttendanceInfoVMs = new List<AttendanceInfoVm>() }
            };
            model.ClassList.Insert(0, new BO.Common() { Id = 0, Name = "All" });
            // model.SectionList.Insert(0, new BO.Common() { Id = 0, Name = "All" });
            model.DayStatusList.Insert(0, new BO.Common() { Id = 0, Name = "All" });
            var firstOrDefault = model.ClassList.FirstOrDefault();
            model.SectionList = firstOrDefault != null ? StudentService.GetSections(firstOrDefault.Id).Result : new List<BO.Common>();
            if (sessionId == 0 || classId == 0 || sectionId == 0)
                return model;
            var predicate = PredicateBuilder.True<AttendanceList>();
            DateTime startDate;
            var isStartDateValid = DateTime.TryParseExact(sd, CURRENT_DATEFORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out startDate);

            DateTime endDate;
            var isEndDateValid = DateTime.TryParseExact(ed, CURRENT_DATEFORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out endDate);

            if (isStartDateValid && isEndDateValid)
            {
                model.StartDate = startDate;
                model.EndDate = endDate;
            }
            predicate = predicate.And(a => a.AttendanceDate >= model.StartDate && a.AttendanceDate <= model.EndDate);
            var attendanceInfoBos = AttendanceService.GetAllAttendace(model.StartDate.Value, model.EndDate.Value, SessionItems.SchoolId, sessionId, classId, sectionId).Result;

            var result = attendanceInfoBos.AsQueryable().Where(predicate).ToList();

            var dateTimes = result.GroupBy(a => a.AttendanceDate.Date).Select(a => a.First()).Select(a => a.AttendanceDate.Date).OrderByDescending(a => a.Date).ToList();

            var infoBos = result.GroupBy(a => a.StudentId).Select(a => a.First()).ToList();

            dateTimes = dateTimes.OrderBy(a => a.Date).ToList();

            var attendanceVM = new AttendanceVm
            {
                HeaderDates = dateTimes,
                AttendanceInfoVMs = new List<AttendanceInfoVm>()
                //DayStatuses = BLL.PublicLayer.GetDayStatuses().Result

            };
            foreach (var attendanceInfoBo in infoBos)
            {
                var attendanceInfoVM = new AttendanceInfoVm
                {
                    StudentName = attendanceInfoBo.StudentName,
                    StudentId = attendanceInfoBo.StudentId,
                    AttendanceBos = new List<AttendanceMaster>()
                };

                foreach (var dateTime in dateTimes)
                {
                    var infoBo = result.FirstOrDefault(a => a.StudentId == attendanceInfoBo.StudentId && a.AttendanceDate.Date == dateTime);
                    var attendanceBo = new AttendanceMaster
                    {
                        //Id = attendanceInfoBo.Id,
                        StudentId = attendanceInfoBo.StudentId,
                        DayStatusId = 1,
                        SessionId = model.SessionId
                    };
                    if (infoBo != null)
                    {
                        //attendanceBo.Id = infoBo.ID;
                        attendanceBo.Date = infoBo.AttendanceDate;
                        attendanceBo.DayStatusId = infoBo.DayStatusId;
                        //attendanceBo.InTime = infoBo.InTime.GetValueOrDefault();
                        //attendanceBo.OutTime = infoBo.OutTime.GetValueOrDefault();
                    }

                    attendanceInfoVM.AttendanceBos.Add(attendanceBo);
                }

                attendanceVM.AttendanceInfoVMs.Add(attendanceInfoVM);
            }
            model.AttendanceVM = attendanceVM;
            return model;
        }

    }

}
