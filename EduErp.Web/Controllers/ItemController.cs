﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;

namespace EduErp.Web.Controllers
{
    public class ItemController : BaseController
    {
        //
        // GET: /Item/

        public ActionResult Index()
        {
            return View(ItemService.GetItemes(SessionItems.SchoolId).Result);
        }
        [AjaxOnly]
        public ActionResult Add(string value,string expenseTypeId)
        {
            var requestId = Convert.ToInt32(value);
            TempData["RequestId"] = requestId;
            var expenseTypeIds = Convert.ToInt32(expenseTypeId);
            TempData["ExpenseTypeId"] = expenseTypeIds;
            var model = new ItemBo {ListOfInventoryType = ExpensesService.GetExpensesType().Result};
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(ItemBo model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            model.SchoolId = SessionItems.SchoolId;
            model.CreatedOn = DateTime.Now;
            model.CreatedBy = SessionItems.UserId;
            model.StatusId = (int)UserStatus.Active;
            var result = ItemService.AddItem(model).Result;
            if (result == 1)
            {
                TempData["SuccessMessage"] = Messages.AddSuccessfully;
                return Json(new { isErr = false, errMsg = Messages.AddSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            return Json(result == -1 ? new { isErr = true, errMsg = Messages.ExistError } : new { isErr = true, errMsg = Messages.AddError }, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        public ActionResult Edit(int id)
        {
          
            var item = ItemService.GetitemById(id).Result;
            item.ListOfInventoryType = ExpensesService.GetExpensesType().Result;
            return View(item);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ItemBo model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            model.SchoolId = SessionItems.SchoolId;
            model.LastUpdatedBy = SessionItems.UserId;
            model.LastUpdatedOn = DateTime.Now;
            var result = ItemService.UpdateItem(model).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(new { isErr = false, msg = Messages.UpdateSuccessfully }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateStatus(int id, int status)
        {
            var result = ItemService.UpdateStatus(id, status, SessionItems.UserId).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(result);
        }

        [AjaxOnly]
        public JsonResult GetItemList(string expenseTypeId)
        {
            var itemList = (from a in ExpensesService.GetItemList(int.Parse(expenseTypeId), SessionItems.SchoolId).Result
                            select new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.Id.ToString()
                            }).ToList();

            return Json(itemList);
        }
    }
}
