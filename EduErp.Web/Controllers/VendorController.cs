﻿using System;
using System.Linq;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;

namespace EduErp.Web.Controllers
{
    public class VendorController : BaseController
    {
        //
        // GET: /Vendor/

        public ActionResult Index()
        {
            
            return View(VendorService.Getvendors(SessionItems.SchoolId).Result);
        }

        public ActionResult Add(string value,string expenseTypeId)
        {
            var requestId = Convert.ToInt32(value);
            TempData["Request"] = requestId;
            var typeId = Convert.ToInt32(expenseTypeId);
            TempData["ExpenseTypeId"] = typeId;
            var model = new VendorBo
            {
                ListOfStates = StateList,
                ListOfInventoryType = ExpensesService.GetExpensesType().Result
            };
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(VendorBo model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            model.SchoolId = SessionItems.SchoolId;
            model.CreatedOn = DateTime.Now;
            model.CreatedBy = SessionItems.UserId;
            model.StatusId = (int)UserStatus.Active;
            var result = VendorService.AddVendor(model).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.AddSuccessfully;
            else
                TempData["ErrMessage"] = Messages.AddError;
            return Json(new { isErr = false, msg = Messages.AddSuccessfully }, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        public ActionResult Edit(int id)
        {
            var result = VendorService.GetVendorById(id).Result;
            result.ListOfStates = StateList;
            result.ListOfInventoryType = ExpensesService.GetExpensesType().Result;
            return View(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(VendorBo model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            model.SchoolId = SessionItems.SchoolId;
            model.LastUpdatedBy = SessionItems.UserId;
            model.LastUpdatedOn = DateTime.Now;
            var result = VendorService.UpdateVedor(model).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(new { isErr = false, msg = Messages.UpdateSuccessfully }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateStatus(int id, int status)
        {
            var result = VendorService.UpdateStatus(id, status, SessionItems.UserId).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(result);
        }
        
        [AjaxOnly]
        public JsonResult GetCityList(string stateId)
        {
            var cityList = (from a in StudentService.GetCityList(int.Parse(stateId)).Result
                            select new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.Id.ToString()
                            }).ToList();

            return Json(cityList);
        }

        [AjaxOnly]
        public JsonResult GetVendorList(string expenseTypeId)
        {
            var vendorList = (from a in ExpensesService.GetVendorList(int.Parse(expenseTypeId), SessionItems.SchoolId).Result
                              select new SelectListItem()
                              {
                                  Text = a.Name,
                                  Value = a.Id.ToString()
                              }).ToList();

            return Json(vendorList);
        }
    }
}
