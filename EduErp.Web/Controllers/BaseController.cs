﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BLL.EmailNotifications;
using EduErp.BO;
using EduErp.BO.Enumerators;
using EduErp.Mvc.Utilities;
using EduErp.Mvc.Utilities.Filters;

namespace EduErp.Web.Controllers
{
    [NoCache, AuthenticationRequired]
    public class BaseController : Controller
    {
        internal static void AddApplicationLog(Exception exception, string fullMessage = null)
        {
            var os = Environment.OSVersion;
            object osName = "Unknown";
            var applicationLog = new ApplicationLogBo
            {
                ID = Guid.NewGuid(),
                UserID = SessionItems.UserId,
                SchoolId = SessionItems.SchoolId,
                SchoolName = SessionItems.SchoolName,
                Module = exception.TargetSite.Module.ToString(),
                ClassName = (exception.TargetSite != null && exception.TargetSite.ReflectedType != null
                        ? exception.TargetSite.ReflectedType.ToString()
                        : string.Empty),
                MethodName = (exception.TargetSite != null ? exception.TargetSite.Name : string.Empty),
                ExceptionMessage = fullMessage ?? string.Empty,
                StackTrace = exception.StackTrace ?? string.Empty,
                InnerExceptionMessage = exception.InnerException != null ? exception.InnerException.Message : string.Empty,
                InnerExceptionStackTrace = exception.InnerException != null ? exception.InnerException.StackTrace : string.Empty,
                LoggedDate = DateTime.UtcNow,
                OSInfo = "OS Name: {osName}, Version: {os.VersionString}, Service Pack: {os.ServicePack}",
                RequestUrl = System.Web.HttpContext.Current.Request.Url != null ? System.Web.HttpContext.Current.Request.Url.AbsoluteUri : "GIS Call",
                LogSourceID = (int)LogSources.Website
            };
            try
            {
                LogService.AddApplicationLog(applicationLog);
            }
            catch { }
            finally
            {
                NotificationSender.SendExceptionMails(applicationLog, applicationLog.RequestUrl);
            }
        }

        // Default Date Format
        internal const string CURRENT_DATEFORMAT = "dd-MMM-yyyy";

        private static List<SelectListItem> _parentschoolList;
        internal static List<SelectListItem> ParentSchoolList
        {
            get
            {
                if (_parentschoolList != null && _parentschoolList.Count != 0) return _parentschoolList;

                _parentschoolList = (from a in SchoolService.GetParentSchoolList().Result
                                     select new SelectListItem()
                                     {
                                         Text = a.Name,
                                         Value = a.Id.ToString()
                                     }).ToList();

                return _parentschoolList;
            }
        }

        private static List<SelectListItem> _sessionList;
        internal static List<SelectListItem> SessionlList
        {
            get
            {
               // if (_sessionList != null && _sessionList.Count != 0) return _sessionList;

                _sessionList = (from a in StudentService.GetSessions(SessionItems.SchoolId).Result
                                select new SelectListItem()
                                {
                                    Text = a.Name,
                                    Value = a.Id.ToString()
                                }).ToList();

                return _sessionList;
            }
        }

        private static List<SelectListItem> _schools;
        internal static List<SelectListItem> Schools
        {

            get
            {
                if (_schools != null && _schools.Count != 0) return _schools;
                _schools = (from s in SchoolService.GetSchoolsList().Result

                            select new SelectListItem()
                            {
                                Text = s.Name,
                                Value = s.Id.ToString()
                            }).ToList();
                return _schools;
            }

        }

        //State City
        private static List<SelectListItem> _stateList;
        internal static List<SelectListItem> StateList
        {
            get
            {
                if (_stateList != null && _stateList.Count != 0) return _stateList;

                _stateList = (from a in SchoolService.GetStateList().Result
                              select new SelectListItem()
                              {
                                  Text = a.Name,
                                  Value = a.Id.ToString()
                              }).ToList();

                return _stateList;
            }
        }

        private static AmazonSetting _amazonSettings;
        internal static AmazonSetting AmazonSettings
        {
            get
            {
                if (_amazonSettings != null) return _amazonSettings;
                _amazonSettings = LogService.GetAmazonSetting().Result;
                return _amazonSettings;
            }
        }
       
    }
}
