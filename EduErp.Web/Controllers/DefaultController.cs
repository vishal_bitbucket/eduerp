﻿using System.Web.Mvc;
using System.Linq;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Mvc.Utilities;
using System;

namespace EduErp.Web.Controllers
{
    public class DefaultController : BaseController
    {
        //
        // GET: /Default/

        public ActionResult Index()
        {

          var model = BLL.DefaultService.GetCountDetails(SessionItems.SchoolId).Result;
           // model.TotalPaid = 8;
            return View(model);
        }

        public JsonResult GetData()
        {
            var modalData = DefaultService.GetFeeExpenseDtail(SessionItems.SchoolId,SessionItems.SessionId).Result;
            var data = new BO.DashboardBo();
            for (int i = 0; i < modalData.Count; i++)
            {
                data.TotalExpense = modalData[i].TotalExpense;
                data.TotalFeeCollection = modalData[i].TotalFeeCollection;
            }
                return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SetSessionId(string sessionId)
        {
            SessionItems.SessionId = Convert.ToInt32(sessionId);
            return Json( SessionItems.SessionId);
        }
        public JsonResult GetClassStudent()
        {
            var modelData = DefaultService.GetStudentClassList(SessionItems.SchoolId).Result;
            return Json(modelData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Configuration()
        {
            var permission = UserService.GetPermissionByUserId(SessionItems.UserId, SessionItems.SchoolId).Result;
            //if(SessionItems.InstituteTypeId==)
            SessionItems.MenuItems = permission.MenuList;
            SessionItems.UserRights = permission.UserRights;            
            if (SessionItems.InstituteTypeId == (int)InstituteType.Coaching)
            {
                SessionItems.MenuItems.FirstOrDefault(a => a.Name == "Class").Name =  "Course";
                SessionItems.MenuItems.FirstOrDefault(a => a.Name == "Section").Name = "Batch";
            }
            //var model = BLL.DefaultService.GetConfigurationCount(SessionItems.SchoolId).Result;
            return View();
        }

    }
}
