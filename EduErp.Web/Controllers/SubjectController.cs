﻿using System;
using System.Linq;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;

namespace EduErp.Web.Controllers
{
    public class SubjectController : BaseController
    {
        //
        // GET: /Subject/

        public ActionResult Index()
        {

            SubjectBo result = SubjectService.GetSubjects(SessionItems.SchoolId).Result;

            return View(result);
        }

        //[NonAction]
        //private  List<SelectListItem> GetClassList()
        //{
        //      var res   = (from a in EduErp.BLL.SubjectService.GetClassList().Result
        //                             select new SelectListItem()
        //                             {
        //                                 Text = a.Name,
        //                                 Value = a.Id.ToString()
        //                             }).ToList();

        //      return res;

        //}


        public ActionResult Add()
        {
            var model = new SubjectBo
            {
                ListOfClasses = (from a in SubjectService.GetClasses(SessionItems.SchoolId).Result
                                 select new BO.Common
                                 {
                                     Name = a.Name,
                                     Id = a.Id
                                 }).ToList()
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(SubjectBo model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }

            model.CreatedOn = DateTime.Now;
            model.CreatedBy = SessionItems.UserId;
            model.Status = (int)UserStatus.Active;
            model.SchoolId = SessionItems.SchoolId;
            var result = SubjectService.AddSubject(model).Result;
            if (result == 1)
            {
                TempData["SuccessMessage"] = Messages.AddSuccessfully;
                return Json(new { isErr = false, errMsg = Messages.AddSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            return Json(result == -1 ? new { isErr = true, errMsg = Messages.ExistError } : new { isErr = true, errMsg = Messages.AddError }, JsonRequestBehavior.AllowGet);

        }


        [AjaxOnly]
        public ActionResult Edit(int id)
        {
            var model = SubjectService.GetSubjectById(id).Result;
            //model.ListOfClasses = (from a in SubjectService.GetClassesBySubjectId(SessionItems.SchoolId).Result
            //                       select new BO.Common
            //                       {
            //                           Name = a.Name,
            //                           Id = a.Id
            //                       }).ToList();
            model.ListOfClasses = (from a in SubjectService.GetClasses(SessionItems.SchoolId).Result
                select new BO.Common
                {
                    Name = a.Name,
                    Id = a.Id
                }).ToList();
       



            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SubjectBo model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            model.CreatedBy = SessionItems.UserId;
            model.CreatedOn = DateTime.Now;
            model.LastUpdatedBy = SessionItems.UserId;
            model.LastUpdatedOn = DateTime.Now;
            model.Status = (int)UserStatus.Active;
            model.SchoolId = SessionItems.SchoolId;
            var result = SubjectService.UpdateSubject(model).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(new { isErr = false, msg = Messages.UpdateSuccessfully }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateStatus(int id, int status)
        {
            var result = SubjectService.UpdateStatus(Convert.ToInt32(id), Convert.ToInt32(status), SessionItems.UserId).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(result);
        }

        [AjaxOnly]
        public JsonResult GetClassList()
        {
            var classList = (from a in SubjectService.GetClasses(SessionItems.SchoolId).Result
                             select new SelectListItem()
                             {
                                 Text = a.Name,
                                 Value = a.Id.ToString()
                             }).ToList();

            return Json(classList);
        }

    }
}
