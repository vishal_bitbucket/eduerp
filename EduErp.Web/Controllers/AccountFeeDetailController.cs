﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;

namespace EduErp.Web.Controllers
{
    public class AccountFeeDetailController : BaseController
    {
        //
        // GET: /AccountFeeDetail/

        public ActionResult Index()
        {
            return View(AccountFeeDetailService.GetAccountFeeDetails().Result);
        }

        public ActionResult Add()
        {
            AccountFeeDtailBo model = new AccountFeeDtailBo
            {
                ListOfSchools = Schools,
                ListOfPaymentModes = AccountFeeDetailService.GetPaymentMode().Result,

            };

            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(AccountFeeDtailBo model)
        {

            DateTime fdate;
            DateTime.TryParseExact(model.FromDateStr, CURRENT_DATEFORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out fdate);
            model.FromDate = fdate;
            DateTime tdate;
            DateTime.TryParseExact(model.ToDateStr, CURRENT_DATEFORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out tdate);
            model.ToDate = tdate;
            DateTime pdate;
            DateTime.TryParseExact(model.PaymentDateStr, CURRENT_DATEFORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out pdate);
            model.PaymentDate = pdate;
            if (!ModelState.IsValid) return View(model);
            model.ListOfSchools = Schools;
            model.ListOfPaymentModes = AccountFeeDetailService.GetPaymentMode().Result;
            model.CreatedBy = SessionItems.UserId;
            model.CreatedOn = DateTime.Now;
            model.StatusId = (int)UserStatus.Active;
            var result = AccountFeeDetailService.AddAcountFeeDetail(model).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.AddSuccessfully;
            else
                TempData["ErrMessage"] = Messages.AddError;
            return Json(new { isErr = false, msg = Messages.AddSuccessfully }, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        public JsonResult GetPlanList(string schoolId)
        {
            var planList = (from a in AccountFeeDetailService.GetPlanList(int.Parse(schoolId)).Result
                            select new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.Id.ToString()
                            }).ToList();

            return Json(planList);
        }

        public ActionResult Edit(int id)
        {
            var model = AccountFeeDetailService.GetAccountFeeDetailById(id).Result;
            model.ListOfSchools = Schools;
            model.ListOfPaymentModes = AccountDetailService.GetPaymentMode().Result;
            if (model.PaymentDate != null) model.PaymentDateStr = model.PaymentDate.Value.ToFormattedDate();
            if (model.FromDate != null) model.FromDateStr = model.FromDate.Value.ToFormattedDate();
            if (model.ToDate != null) model.ToDateStr = model.ToDate.Value.ToFormattedDate();
            return View(model);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AccountFeeDtailBo model)
        {
            DateTime fdate;
            DateTime.TryParseExact(model.FromDateStr, CURRENT_DATEFORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out fdate);
            model.FromDate = fdate;
            DateTime tdate;
            DateTime.TryParseExact(model.ToDateStr, CURRENT_DATEFORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out tdate);
            model.ToDate = tdate;
            DateTime pdate;
            DateTime.TryParseExact(model.PaymentDateStr, CURRENT_DATEFORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out pdate);
            model.PaymentDate = pdate;
            if (!ModelState.IsValid) return View(model);
            model.ListOfSchools = Schools;
            model.ListOfPaymentModes = AccountDetailService.GetPaymentMode().Result;
            model.UpadatedBy = SessionItems.UserId;
            model.UpdatedOn = DateTime.Now;
            var result = AccountFeeDetailService.UpdateAccountFeeDetail(model).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.AddError;
            return Json(new { isErr = false, msg = Messages.UpdateSuccessfully }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateStatus(int id, int statusId)
        {
            var result = AccountFeeDetailService.UpdateStatus(id, statusId, SessionItems.UserId).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(result);
        }
    }
}
