﻿using System;
using System.Linq;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;

namespace EduErp.Web.Controllers
{
    public class UserController : BaseController
    {
        //
        // GET: /User/

        public ActionResult Index()
        {
            return View(UserService.GetUsers(SessionItems.SchoolId).Result);
        }


        public ActionResult Add()
        {
            return View(new UserBo { ListOfSchools = Schools, Rights = UserService.GetRights().Result, MenuList = UserService.GetMenu().Result,UserTypeList = UserService.GetUserType().Result});
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(UserBo model)
        {
            model.ListOfSchools = Schools;
            model.Rights = UserService.GetRights().Result;
           // model.MenuList = UserService.GetMenu().Result;
            model.CreatedBy = SessionItems.UserId;
            model.CreatedOn = DateTime.Now;
            model.UserTypeList = UserService.GetUserType().Result;
            model.StatusId = (int)UserStatus.Active;
            model.IsPasswordCreated = true;
            if (!SessionItems.IsSuperAdmin)
                model.SchoolId = SessionItems.SchoolId;
            model.Salt = Guid.NewGuid();
            if (!ModelState.IsValid) return View(model);
            var leftUser = UserService.LeftUser(model.SchoolId??0).Result;
            if (leftUser >= 1)
            {
                foreach (var item in FeeTransactionService.GetAutomateMessageIds(SessionItems.SchoolId).Result)
                {
                    if (item.MessageAutomateId == (int)AutomateMesageFor.UserCreated)
                    {
                        if (model.MobileNo.ToString() != "")
                        {
                            var smsResult = SendSms.SendTextSms(model.MobileNo.ToString(), item.TemplateMessage);
                            if (smsResult.ErrorMessage == "Done")
                            {
                                TempData["SuccessMessage"] = Messages.SentSuccessfully;
                                //return Json(new { isErr = false, msg = Messages.SentSuccessfully }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
            }
            else
            {

                ModelState.AddModelError("UserLimitExceedError", Messages.UserLimitExceedError);
                model.Rights = UserService.GetRights().Result;
                return View(model);
            }
            var result = UserService.AddUser(model).Result;
            if (result == 1)
            {
                TempData["SuccessMessage"] = Messages.AddSuccessfully;
                return RedirectToAction("Index");
            }
            if (result == -1)
            {
                ModelState.AddModelError("UserId", Messages.UserAlreadyRegistered);
                model.Rights = UserService.GetRights().Result;
                return View(model);
            }
            if (result == -2)
            {
                ModelState.AddModelError("Email", Messages.UserAlreadyRegisteredByEmail);
                model.Rights = UserService.GetRights().Result;
                return View(model);
            }
            if (result == -3)
            {
                ModelState.AddModelError("UserId", Messages.UserLimitExceedError);
                model.Rights = UserService.GetRights().Result;
                return View(model);
            }
            if (result == -4)
            {
                ModelState.AddModelError("MobileNo", Messages.UserRegisteredWithSameNumber);
                model.Rights = UserService.GetRights().Result;
                return View(model);
            }
           // ModelState.AddModelError();
            model.Rights = UserService.GetRights().Result;
            return View(model);
        }


        [AjaxOnly]
        public ActionResult Edit(int id)
        {
            var user = UserService.GetUser(id).Result;
            var model = new UserBo
            {
                Name = user.Name,
                UserTypeId = user.UserTypeId,
                Email = user.Email,
                UserId = user.UserId,
                MobileNo = user.MobileNo,
                ListOfSchools = Schools,
                SchoolId = user.SchoolId,
                Rights = UserService.GetRights().Result,
                MenuList = UserService.GetMenu().Result,
                UserTypeList = UserService.GetUserType().Result
            };
            foreach (var m in user.MenuList)
            {
                var menu = model.MenuList.FirstOrDefault(a => a.MenuId == m.MenuId);
                if (menu != null)
                {
                    menu.IsSelected = m.IsSelected;
                    menu.RightId = m.RightId;
                }
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserBo model)
        {
            if (!ModelState.IsValid)
            {
                model.ListOfSchools = Schools;
                model.Rights = UserService.GetRights().Result;
                model.MenuList = UserService.GetMenu().Result;
                model.UserTypeList = UserService.GetUserType().Result;
                return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            model.LastModifiedBy = SessionItems.UserId;
            model.LastModifiedOn = DateTime.Now;
            model.IsPasswordCreated = true;
            var result = UserService.Update(model);
            if (result.Result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrorMessage"] = Messages.AddError;
            return Json(new { isErr = false, msg = Messages.UpdateSuccessfully }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(UserBo model)
        {
            var result = UserService.Delete(model);
            if (result.Result)
                TempData["SuccessMessage"] = Messages.DeletedSucessfully;
            else
                TempData["ErrorMessage"] = Messages.AddError;
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateStatus(int userId, int statusId)
        {
            var result = UserService.UpdateStatus(userId, statusId, SessionItems.UserId).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(result);
        }

        //public ActionResult UserLimitExceed()
        //{
        //    return View();
        //}
    }
}
