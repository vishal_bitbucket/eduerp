﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;
namespace EduErp.Web.Controllers
{
    public class QuestionAnswerController : Controller
    {
        //
        // GET: /QuestionAnswer/

        public ActionResult Index()
        {
            return View(QuestionAnswerService.GetQuestionAnswer(SessionItems.SchoolId).Result);
        }
        public ActionResult Add()
        {
            var subjectList = OnlineTestQuestionService.GetSubjects(SessionItems.SchoolId).Result;
            var ListOfChoiceTypeId = QuestionAnswerService.GetChoiceTypeId().Result;
            var QuestionAnswer = new QuestionAnswerMappingBo
            {
                ListOfSubject = subjectList,
                ListOfChoice = ListOfChoiceTypeId,
            };
            return View(QuestionAnswer);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Add(QuestionAnswerMappingBo model)
        {
            //ModelState.Remove("Id");
            //if (!ModelState.IsValid)
            //{
            //    //model.ListOfSchools = Schools;
            //    return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            //}
            model.SchoolId = SessionItems.SchoolId;
            model.CreatedOn = DateTime.Now;
            model.CreatedBy = SessionItems.UserId;
            model.StatusId = (int)UserStatus.Active;
            //model.ListOfSchools = Schools;
            var result = QuestionAnswerService.AddQuestionAnswer(model).Result;
            if (result > 0)
            {
                TempData["SuccessMessage"] = Messages.AddSuccessfully;
                return Json(new { isErr = false, errMsg = Messages.AddSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            return Json(result == -1 ? new { isErr = true, errMsg = Messages.ExistError } : new { isErr = true, errMsg = Messages.AddError }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateStatus(int id, int statusId)
        {
            var result = QuestionAnswerService.UpdateStatus(id, statusId, SessionItems.UserId).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(result);
        }
       
        public ActionResult Edit(int id)
        {
            var result = QuestionAnswerService.GetQuestionAnswerById(id).Result;
            var ListOfChoiceTypeId = QuestionAnswerService.GetChoiceTypeId().Result;
           

            var model = new BO.QuestionAnswerMappingBo
            {


                ListOfChoice = ListOfChoiceTypeId,
                ChoiceTypeId=result.ChoiceTypeId,
                Id = result.Id,
                Question =result.Question,
                Answers = result.Answers,
                //IsCorrectAnswer = result.IsCorrectAnswer,
                Description=result.Description,
                SchoolId = result.SchoolId,
                StatusId = result.StatusId
               


            };
            return PartialView(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(QuestionAnswerMappingBo model)
        {
            
            //if (!ModelState.IsValid)
            //{
            //    //model.ListOfSchools = Schools;
            //    return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            //}
            model.SchoolId = SessionItems.SchoolId;
            model.StatusId = (int)UserStatus.Active;
            model.LastUpdatedBy = SessionItems.UserId;
            model.LastUpdatedOn = DateTime.Now;
           
            var result = QuestionAnswerService.AddQuestionAnswer(model).Result;
            if (result > 0)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(new { isErr = false, msg = Messages.UpdateSuccessfully }, JsonRequestBehavior.AllowGet);
          

        }
    }
}
