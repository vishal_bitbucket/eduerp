﻿using System;
using System.Linq;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;

namespace EduErp.Web.Controllers
{
    public class ClassController : BaseController
    {
        //
        // GET: /Class/

        public ActionResult Index()
        {
            return View(ClassService.GetClasses(SessionItems.SchoolId).Result);
        }
        [AjaxOnly]
        public ActionResult Add()
        {
            return PartialView(new ClassBo { ListOfSchools = Schools });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(ClassBo model)
        {
            if (!ModelState.IsValid)
            {
                model.ListOfSchools = Schools;
                return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            model.SchoolId = SessionItems.SchoolId;
            model.CreatedOn = DateTime.Now;
            model.CreatedBy = SessionItems.UserId;
            model.Status = (int)UserStatus.Active;
            model.ListOfSchools = Schools;
            var result = ClassService.AddClass(model).Result;
            if (result == 1)
            {
                TempData["SuccessMessage"] = Messages.AddSuccessfully;
                return Json(new { isErr = false, errMsg = Messages.AddSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            return Json(result == -1 ? new { isErr = true, errMsg = Messages.ExistError } : new { isErr = true, errMsg = Messages.AddError }, JsonRequestBehavior.AllowGet);

        }

        [AjaxOnly]
        public ActionResult Edit(int id)
        {
            var result = ClassService.GetClassById(id).Result;
            var model = new ClassBo
            {
                Name = result.Name,
                ListOfSchools = Schools,
                Status = result.Status,
                SchoolId = result.SchoolId
            };
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ClassBo model)
        {

            if (!ModelState.IsValid)
            {
                model.ListOfSchools = Schools;
                return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            model.Status = (int)UserStatus.Active;
            model.LastUpdatedBy = SessionItems.UserId;
            model.LastUpdatedOn = DateTime.Now;
            var result = ClassService.UpdateClass(model).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(new { isErr = false, msg = Messages.UpdateSuccessfully }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateStatus(int id, int status)
        {
            var result = ClassService.UpdateStatus(id, status, SessionItems.UserId).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(result);
        }




    }
}
