﻿using System;
using System.Linq;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;

namespace EduErp.Web.Controllers
{
    public class EmployeeTypeController : BaseController
    {
        //
        // GET: /EmployeeType/

        public ActionResult Index()
        {
            return View(EmployeeTypeService.GetEmployeeTypes(SessionItems.SchoolId).Result);
        }

        public ActionResult Add()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(EmployeeTypeBo model)
        {
            if (!ModelState.IsValid)
            {
               return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            model.SchoolId = SessionItems.SchoolId;
            model.CreatedOn = DateTime.Now;
            model.CreatedBy = SessionItems.UserId;
            model.StatusId = (int)UserStatus.Active;
           var result = EmployeeTypeService.AddEmployeeType(model).Result;
           if (result == 1)
           {
               TempData["SuccessMessage"] = Messages.AddSuccessfully;
               return Json(new { isErr = false, errMsg = Messages.AddSuccessfully }, JsonRequestBehavior.AllowGet);
           }
           return Json(result == -1 ? new { isErr = true, errMsg = Messages.ExistError } : new { isErr = true, errMsg = Messages.AddError }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int id)
        {
            var employeeType = EmployeeTypeService.GetEmployeeType(id).Result;
            return View(employeeType);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EmployeeTypeBo model)
        {
            if (!ModelState.IsValid)
            {
              return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            model.SchoolId = SessionItems.SchoolId;
            model.LastUpdatedBy = SessionItems.UserId;
            model.LastUpdatedOn = DateTime.Now;
            var result = EmployeeTypeService.UpdateEmployeeType(model).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(new { isErr = false, msg = Messages.UpdateSuccessfully }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateStatus(int id, int status)
        {
            var result = EmployeeTypeService.UpdateStatus(id, status, SessionItems.UserId).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(result);
        }
    }
}
