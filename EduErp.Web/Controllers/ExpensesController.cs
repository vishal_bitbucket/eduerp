﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;

namespace EduErp.Web.Controllers
{
    public class ExpensesController : BaseController
    {
        //
        // GET: /Expenses/

        public ActionResult Index()
        {

            return View(ExpensesService.GetInventories(SessionItems.SchoolId).Result);
        }

        public ActionResult Add()
        {
           var model = new ExpensesBo
            {
               // SessionId = SessionItems.SessionId,
                ExpensesList = ExpensesService.GetExpensesType().Result,
               // SessionList = SessionlList,
               // VendorList = ExpensesService.GetVendorList(SessionItems.SchoolId).Result,
                //ItemList = ExpensesService.GetItemList(SessionItems.SchoolId).Result,
                EmployeeNameList =  ExpensesService.GetEmpNameList(SessionItems.SchoolId).Result,
                ListOfPaymentMode = FeeTransactionService.GetPaymentModeList().Result
            };
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(ExpensesBo model)
        {
            //DateTime fdate;
            //DateTime.TryParseExact(model.FromDateStr, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out fdate);
            //DateTime tdate;
            //DateTime.TryParseExact(model.ToDateStr, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out tdate);
            //model.StartDate = fdate;
            //model.EndDate = tdate;
            model.EmployeeNameList = ExpensesService.GetEmpNameList(SessionItems.SchoolId).Result;
            if (model.InvetoryTypeId == 1)
            {
                model.StartDate = model.Date;
            }
            else if (model.InvetoryTypeId==3)
            {
                model.ItemId = model.EmployeeId;
               model.StartDate = model.FromMonth;
                model.EndDate = model.ToMonth;
            }
            else
            {
                model.StartDate = model.StartDate;
                model.EndDate = model.EndDate;
            }
            if (!ModelState.IsValid)
            {
               // model.SessionList = SessionlList;
                model.ListOfPaymentMode = FeeTransactionService.GetPaymentModeList().Result;
                return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            model.SessionId = SessionItems.SessionId;
            model.SchoolId = SessionItems.SchoolId;
            model.CreatedBy = SessionItems.UserId;
            model.CreatedOn = DateTime.Now;
            model.StatusId = (int)UserStatus.Active;
            var result = ExpensesService.AddExpenses(model).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.AddSuccessfully;
            else
                TempData["ErrMessage"] = Messages.AddError;
            return Json(new { isErr = false, msg = Messages.AddSuccessfully }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int id)
        {
             
            var Expenses = ExpensesService.GetExpensesByExpensesId(id).Result;
            //DateTime fdate;
            //DateTime.TryParseExact(Expenses.FromDateStr, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out fdate);
            //DateTime tdate;
            //DateTime.TryParseExact(Expenses.ToDateStr, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out tdate);
            //Expenses.StartDate = fdate;
            //Expenses.EndDate = tdate;
            var model = new ExpensesBo
            {
                ExpensesList = ExpensesService.GetExpensesType().Result,
               // SessionList = SessionlList,
                StartDate = Expenses.StartDate,
                EndDate = Expenses.EndDate,
                SessionId = Expenses.SessionId,
                InventoryName = Expenses.InventoryName,
                VenderId = Expenses.VenderId,
                PaymentModeTypeId=Expenses.PaymentModeTypeId,
                InvetoryTypeId = Expenses.InvetoryTypeId,
                ItemDescription = Expenses.ItemDescription,
                Amount = Expenses.Amount,
                ItemId = Expenses.ItemId,
                EmployeeId = Expenses.ItemId,
                ListOfPaymentMode=FeeTransactionService.GetPaymentModeList().Result,
                EmployeeNameList = ExpensesService.GetEmpNameList(SessionItems.SchoolId).Result
            };

          return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ExpensesBo model)
        {
            var ExpensesTypeList = ExpensesService.GetExpensesType().Result;
            //DateTime fdate;
            //DateTime.TryParseExact(model.FromDateStr, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out fdate);
            //DateTime tdate;
            //DateTime.TryParseExact(model.ToDateStr, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out tdate);
            //model.StartDate = fdate;
            //model.EndDate = tdate;
            if (model.InvetoryTypeId == 1)
            {
                model.StartDate = model.StartDate;
            }
            else if (model.InvetoryTypeId == 3)
            {
                model.ItemId = model.EmployeeId;
               model.StartDate = model.StartDate;
                model.EndDate = model.EndDate;
            }
            else
            {
                model.StartDate = model.StartDate;
                model.EndDate = model.EndDate;
            }
            model.ExpensesList = ExpensesTypeList;
            if (!ModelState.IsValid)
            {
                return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            model.SessionId = SessionItems.SessionId;
            model.LastUpdatedBy = SessionItems.UserId;
            model.LastUpdatedOn = DateTime.Now;
            model.SchoolId = SessionItems.SchoolId;
            var result = ExpensesService.UpdateExpenses(model).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(new { isErr = false, msg = Messages.UpdateSuccessfully }, JsonRequestBehavior.AllowGet);

        }

        [AjaxOnly]
        public JsonResult GetItemList(string expenseTypeId)
        {
            var itemList = (from a in ExpensesService.GetItemList(int.Parse(expenseTypeId),SessionItems.SchoolId).Result
                            select new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.Id.ToString()
                            }).ToList();

            return Json(itemList);
        }

        [AjaxOnly]
        public JsonResult GetVendorList(string expenseTypeId)
        {
            var vendorList = (from a in ExpensesService.GetVendorList(int.Parse(expenseTypeId), SessionItems.SchoolId).Result
                            select new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.Id.ToString()
                            }).ToList();

            return Json(vendorList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateStatus(int id, int statusid)
        {
            var result = ExpensesService.UpdateStatus(id, statusid, SessionItems.UserId).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(result);
        }
    }
}
