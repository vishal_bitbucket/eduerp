﻿using System;
using System.Security.Cryptography;
using System.Web.Mvc;
using System.Web.Security;
using EduErp.Base;
using EduErp.BLL;
using EduErp.BLL.EmailNotifications;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;
using EduErp.Web.Models;

namespace EduErp.Web.Controllers
{
    public class AccountController :Controller
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
            return RedirectToAction("Login");
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (SessionItems.IsUserAuthenticated)
            {
                return RedirectToAction("Index", "Default");
            }

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginVM model, string returnUrl)
        {
            if (SessionItems.IsUserAuthenticated)
                return RedirectToAction("Index", "Default");

            var messageType = AccountService.AuthenticateUser(model.Username, model.Password);
            model.Password = string.Empty;

            switch (messageType.MessageType)
            {
                case MessageType.Success:
                    break;

                case MessageType.InvalidUser:
                    ModelState.AddModelError("Username", Messages.InvalidCredentials);
                    return View(model);

                case MessageType.AccountNotVerified:
                    ModelState.AddModelError("Username", Messages.AccountNotVerified);
                    return View(model);

                case MessageType.AccountNotApproved:
                    ModelState.AddModelError("Username", Messages.AccountNotApproved);
                    return View(model);

                case MessageType.AccountInactive:
                    ModelState.AddModelError("Username", Messages.AccountNotActive);
                    return View(model);

                case MessageType.AccessDeniedToProduct:
                    ModelState.AddModelError("Username", Messages.AccessDenied);
                    return View(model);
                case MessageType.SubscriptionExpired:
                    ModelState.AddModelError("Username", Messages.SubscriptionExpired);
                    return View(model);

                default:
                    ModelState.AddModelError("", Messages.ServerError);
                    return View(model);
            }
            SessionItems.UserId = messageType.Result.User.Id;
            SessionItems.EmailAddress = messageType.Result.User.Email;
            SessionItems.DisplayName = messageType.Result.User.Name;
            SessionItems.SchoolId = messageType.Result.User.SchoolId ?? 0;
            SessionItems.SchoolName = messageType.Result.User.SchoolName;
            SessionItems.InstituteTypeId = messageType.Result.User.InstituteTypeId;
            SessionItems.SessionId = messageType.Result.User.SessionId;
            SessionItems.SessionName = messageType.Result.User.SessionName;
            var permission = UserService.GetPermissionByUserId(SessionItems.UserId, SessionItems.SchoolId).Result;
            SessionItems.MenuItems = permission.MenuList;
            SessionItems.UserRights = permission.UserRights;
            //SessionItems.SessionLists = StudentService.GetSessions(SessionItems.SchoolId).Result;
            //SessionItems.IsAdministrator = (int)Roles.Administrator == user.RoleID;
            //SessionItems.IsHRExecutive = (int)Roles.HR == user.RoleID
            //                            || (int)Roles.Administrator == user.RoleID;
            //SessionItems.IsEmployee = (int)Roles.Administrator == user.RoleID
            //                            || (int)Roles.HR == user.RoleID
            //                            || (int)Roles.Employee == user.RoleID;

            //SessionItems.CASToken = token;
            FormsAuthentication.SetAuthCookie(model.Username, false);
            //var business = EMS.BLL.PublicLayer.GetActiveBusinessByUserId(user.ID).FirstOrDefault();
            //SessionItems.BusinessId = business == null ? Guid.Empty : business.Id;
            //if (!string.IsNullOrWhiteSpace(returnUrl) && Url.IsLocalUrl(returnUrl) && returnUrl.ToLower() != Url.Action("Logout", "Account").ToLower())
            //    return Redirect(returnUrl);
            return RedirectToAction("Index", "Default");
        }

        public ActionResult Logout()
        {
            if (!SessionItems.IsUserAuthenticated)
                return RedirectToAction("Login", "Account");

            Session.Abandon();
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }

        #region Forgot Password

        [AllowAnonymous]
        [AjaxOnly]
        public ActionResult ForgotPassword()
        {
            return View(new ForgotPasswordVM());
        }

        [HttpPost, AllowAnonymous, ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(ForgotPasswordVM model)
        {
            if (model.VerificationTypeId == (int)VerificationType.Sms)
            {
                var user = UserService.GetUserByMobileNumber(model.MobileNumber).Result;
                if (user == null)
                {
                    return Json(new { isErr = true, errMsg = Messages.UserNotFoundByNumber }, JsonRequestBehavior.AllowGet);
                }
                if (user.StatusId == (int)UserStatus.InActive || user.StatusId == (int)UserStatus.Deleted)
                {
                    return Json(new { isErr = true, errMsg = Messages.AccountNotVerified }, JsonRequestBehavior.AllowGet);
                }
                if (!user.IsPasswordCreated)
                {
                    return Json(new { isErr = true, errMsg = Messages.AccountPasswordNotCreated }, JsonRequestBehavior.AllowGet);
                }
                var random = RandomNumber.GenerateRandomNumber();
                var otp = UserService.UpdateOtp(user.Id, random.ToString()).Result;
                SendSms.SendTextSms(user.MobileNo, "Your Otp is" + " " + otp);
                return Json(new { isErr = false, errMsg = Messages.SmsSendSuccessfuly }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var user = UserService.GetUserByEmail(model.Email).Result;
                if (user == null)
                {
                    return Json(new { isErr = true, errMsg = Messages.UserNotFoundByEmail }, JsonRequestBehavior.AllowGet);
                }
                if (user.StatusId == (int)UserStatus.InActive || user.StatusId == (int)UserStatus.Deleted)
                {
                    return Json(new { isErr = true, errMsg = Messages.AccountNotVerified }, JsonRequestBehavior.AllowGet);
                }
                if (!user.IsPasswordCreated)
                {
                    return Json(new { isErr = true, errMsg = Messages.AccountPasswordNotCreated }, JsonRequestBehavior.AllowGet);
                }
                var verificationcode = UserService.UpdateVerificationCode(user.Id, Guid.NewGuid()).Result;
                var resetPasswordUrl = Url.AbsoluteAction("RetrievePassword", "Account", new { id = verificationcode });
                var applicatioUrl = Url.AbsoluteAction("Index", "Account");
                var vm = new ResetPasswordMailBO
                {
                    ApplicationUrl = applicatioUrl,
                    RecipientDisplayName = user.Name,
                    RecipientEmail = user.Email,
                    ResetPasswordUrl = resetPasswordUrl
                };
                NotificationSender.SendForgetPasswordMail(vm.RecipientDisplayName, vm.RecipientEmail, vm.ResetPasswordUrl);
                return Json(new { isErr = false, errMsg = Url.Action("ForgotPasswordLinkSent", "Account", new { verificationTypeId = model.VerificationTypeId, email = model.Email }) }, JsonRequestBehavior.AllowGet);
                //return RedirectToAction("ForgotPasswordLinkSent", new { verificationTypeId=model.VerificationTypeId,email=model.Email });
            }
        }

        //TOASK
        //private void SendVerificationCode(BO.UserBo user)
        //{
        //    var code = Guid.NewGuid();
        //    var everificationcode = GuidHelper.Encode(code);
        //    adminlayer.updateuserverificationcode(user.id, user.usertype, code);
        //    var verificationUrl = Url.AbsoluteAction("V", "Account", new { id = everificationcode });
        //    verificationUrl = Verificationu.replace(everificationcode.tolower(), everificationcode);
        //    notificationsender.senduserverificationmail(user, verificationurl);
        //}

        #endregion

        #region Retrieve Password

        [AllowAnonymous]
        public ActionResult RetrievePassword(string id)
        {
            ViewBag.IsPasswordCreated = false;
            //TO ASK
            Guid verificationCode;
            if (!Guid.TryParse(id, out verificationCode))
            {
                ViewBag.IsInvalidRequest = true;
                return View();
            }

            var user = UserService.GetUserByVerificationCode(verificationCode).Result;
            if (user == null)
            {
                ViewBag.IsInvalidRequest = true;
                return View();
            }

            if (user.VerificationCodeGeneratedOnUtc.AddDays(1) < DateTime.UtcNow)
            {
                ViewBag.IsInvalidRequest = true;
                return View();
            }

            ViewBag.IsInvalidRequest = false;

            var model = new RetrievePasswordVM
            {
                UserId = user.UserId
            };
            return View(model);
        }

    
        public ActionResult ForgotPasswordLinkSent(int verificationTypeId,string email)
        {
            return View(new ForgotPasswordVM { VerificationTypeId = verificationTypeId, Email = email });
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPasswordLinkSent(ForgotPasswordVM model)
        {
            ModelState.Remove("Email");
            var userByotp = UserService.GetUserByOtp(model.Otp).Result;
            if (userByotp == null)
            {
                ModelState.AddModelError("MobileNumber", Messages.OtpInvaild);
                return View("ForgotPasswordLinkSent", model);
            }
            if (userByotp.VerificationCodeGeneratedOnUtc.AddMinutes(10) < DateTime.UtcNow)
            {
                ModelState.AddModelError("MobileNumber", Messages.OtpExpired);
                return View("ForgotPasswordLinkSent", model);
            }
            ViewBag.IsInvalidRequest = false;
            var data = new RetrievePasswordVM
            {
                UserId = userByotp.UserId
            };
            return View("RetrievePassword", data);
        }

        [HttpPost, AllowAnonymous, ValidateAntiForgeryToken]
        public ActionResult RetrievePassword(RetrievePasswordVM model)
        {
            ViewBag.IsPasswordCreated = false;
            var result = UserService.ForgetPasswordCreateNew(model.UserId, model.Password).Result;
            if (!result)
            {
                ViewBag.IsInvalidRequest = true;
                return View(model);
            }
            ViewBag.IsPasswordCreated = true;
            ViewBag.IsInvalidRequest = false;
            return View();
        }

        #endregion

        #region  Change Password
        public ActionResult ChangePassword(int id)
        {
            TempData["UserId"] = id;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangePasswordVM model)
        {
            int userId = Convert.ToInt32(TempData["UserId"]);
            var user = UserService.AuthenticateUserByOldPassword(userId, model.OldPassword).Result;
            if (user == null)
            {
                ModelState.AddModelError("OldPasswordNotMatchedError", Messages.OldPasswordNotMatchedError);
                return View();
            }

            var result = UserService.ChangePassword(user.Id, model.Password).Result;
            if (result)
            {
                TempData["ChangedSuccess"] = "Your password has been updated successfully.";
            }
            return View();

        }
        #endregion

        #region Contact Us Website

        public JsonResult SendMessage(ContactUs model)
        {
            NotificationSender.SendContactUsMail(model);
            return Json(true);
        }

        #endregion
    }
}
