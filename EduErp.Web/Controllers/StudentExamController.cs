﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;

namespace EduErp.Web.Controllers
{
    public class StudentExamController : BaseController
    {
        //
        // GET: /StudentExam/

        public ActionResult Index()
        {
            return View(StudentExamMappingService.GetStudentExamMappingDetails(SessionItems.SchoolId).Result);
        }

        public ActionResult Add()
        {
            var model = new StudentExamMappingBo
            {
                ListOfExamType = StudentExamMappingService.ExamType(SessionItems.SchoolId).Result,
                ListOfClasses = FeeTransactionService.GetClasses(SessionItems.SchoolId).Result
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(StudentExamMappingBo model)
        {
            if (!ModelState.IsValid)
            {
                var examStudentModel = new StudentExamMappingBo
                {
                    ListOfExamType = StudentExamMappingService.ExamType(SessionItems.SchoolId).Result,
                    ListOfClasses = FeeTransactionService.GetClasses(SessionItems.SchoolId).Result,
                    StudentExamLists = model.StudentExamLists
                };
                return View(examStudentModel);
            }
            model.CreatedBy = SessionItems.UserId;
            model.CreatedOn=DateTime.Now;
            model.StatusId = (int) UserStatus.Active;
            var result = StudentExamMappingService.AddStudentExam(model).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.AddSuccessfully;
                 else
                TempData["ErrMessage"] = Messages.AddError;
                return Json(new {isErr = false, msg = Messages.AddSuccessfully}, JsonRequestBehavior.AllowGet);
            
        }

        public ActionResult Edit(int examId,int studentId)
        {
            var result =StudentExamMappingService.GetStudentExamDetailById(SessionItems.SchoolId, examId, studentId).Result;
            result.ListOfExamType = StudentExamMappingService.ExamType(SessionItems.SchoolId).Result;
            result.ListOfClasses = FeeTransactionService.GetClasses(SessionItems.SchoolId).Result;
            return View(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(StudentExamMappingBo model)
        {
            if (!ModelState.IsValid)
            {
                model.ListOfExamType = StudentExamMappingService.ExamType(SessionItems.SchoolId).Result;
                model.ListOfClasses = FeeTransactionService.GetClasses(SessionItems.SchoolId).Result;
                return View(model);
            }
            var result = StudentExamMappingService.UpdateStudentExam(model).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(new { isErr = false, msg = Messages.UpdateSuccessfully }, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        public JsonResult GetSectionList(string classId)
        {
            var sectionList = (from a in FeeTransactionService.GetSections(int.Parse(classId)).Result
                               select new SelectListItem()
                               {
                                   Text = a.Name,
                                   Value = a.Id.ToString()
                               }).ToList();

            return Json(sectionList);
        }

        [AjaxOnly]
        public JsonResult GetStudentsListBySectionId(string sectionId)
        {
            var studentsList = (from s in FeeTransactionService.GetStudentsBySectionId(int.Parse(sectionId),SessionItems.SessionId).Result
                                select new SelectListItem()
                                {
                                    Text = s.Name,
                                    Value = s.Id.ToString()
                                }).ToList();
            return Json(studentsList);
        }



        [HttpPost]
        public ActionResult GetStudentListForExamMapping( int examTypeId, int studentId)
        {
            StudentExamMappingBo model = new StudentExamMappingBo { StudentExamLists = GetStudentExamLists(examTypeId, studentId, SessionItems.SchoolId) };
                return PartialView("_StudentExamTemplate",model);
        }
        [NonAction]
        public List<StudentExamList> GetStudentExamLists(int examTypeId, int studentId, int schoolId)
        {
            var studentLists = StudentExamMappingService.GetExamMappingList(examTypeId, studentId, schoolId).Result;
            return studentLists;
        }

        [HttpPost]
        public ActionResult GetStudentListForExamMappingForEdit(int studentId, int examTypeId)
        {
            StudentExamMappingBo model = new StudentExamMappingBo { StudentExamLists = GetStudentExamListsForEdit(SessionItems.SchoolId, studentId, examTypeId) };
          return PartialView("_StudentExamTemplate", model);
        }
        [NonAction]
        public List<StudentExamList> GetStudentExamListsForEdit(int schoolId, int studentId, int examTypeId)
        {
            var studentLists = StudentExamMappingService.GetExamMappingDetailForEdit(schoolId, studentId, examTypeId).Result;
            return studentLists;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateStatus(int id, int status)
        {
            var result = StudentExamMappingService.UpdateStatus(id, status, SessionItems.UserId).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(result);
        }
        
    }
}
