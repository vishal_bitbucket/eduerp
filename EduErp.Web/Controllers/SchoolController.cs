﻿using System;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;
using EduErp.Web.Models;
using EduErp.Web.Utilities;

namespace EduErp.Web.Controllers
{
    [AdminRequired]
    public class SchoolController : BaseController
    {

        public ActionResult Index()
        {
            var school = SchoolService.GetSchools();
            return View(school.Result);
        }
        #region # Add
        public ActionResult Add()
        {
             
            var school = new SchoolBo {
                PlanTypeList = PlanService.GetPlanType().Result,
                ListOfSchools = ParentSchoolList,
                ListOfStates = StateList,
               // CityState = citystate ,
                ListOfInstituteType = SchoolService.GetInstituteType().Result
            };
            return View(school);
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(SchoolBo model)
        {
            if (!ModelState.IsValid) return View(model);
            var logoPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Temp", model.S3key + ".jpg");
            model.S3key = System.IO.File.Exists(logoPath) ? AmazonFileHelper.UploadFile("School", logoPath, AmazonSettings.AccessKey, AmazonSettings.SecretAccessKey, AmazonSettings.BucketName) : new Guid();
            model.CreatedBy = SessionItems.UserId;
            model.CreatedOn = DateTime.Now;
            model.StatusId = (int)UserStatus.Active;
            model.ParentSchoolId = model.ParentSchoolId ?? 0;

            var result = SchoolService.AddSchool(model);
            if (result.Result)
                TempData["SuccessMessage"] = Messages.AddSuccessfully;
            else
                TempData["ErrMessage"] = Messages.AddError;
            return RedirectToAction("Index");
        }
        #endregion
        #region  # Edit #
        public ActionResult Edit(int id)
        {
            var school = SchoolService.GetSchool(id).Result;
            school.PlanTypeList = PlanService.GetPlanType().Result;
            school.ListOfStates = StateList;
            school.ListOfSchools = ParentSchoolList;
            school.ListOfInstituteType = SchoolService.GetInstituteType().Result;
            //school.CityState = citystate;
            return View(school);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SchoolBo model)
        {
            if (!ModelState.IsValid)
            {
                model.PlanTypeList = PlanService.GetPlanType().Result;
                model.ListOfSchools = ParentSchoolList;
                model.ListOfStates = StateList;
               // CityState = citystate ,
                model.ListOfInstituteType = SchoolService.GetInstituteType().Result;
                return View(model);
            }
                
           // model.ListOfStates = StateList;
            model.LastUpdateBy = SessionItems.UserId;
            model.LastUpdatedOn = DateTime.Now;
            model.StatusId = (int) UserStatus.Active;
            model.ParentSchoolId = model.ParentSchoolId ?? 0;
            var result = SchoolService.UpdateSchool(model);
            if (result.Result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.AddError;
            return RedirectToAction("Index");
        }
        #endregion 
        //public ActionResult View(int id)
        //{

        //    return View(BLL.SchoolService.GetSchool(id).Result);
        //}

        public ActionResult Delete(SchoolBo model)
        {

            var result = SchoolService.Delete(model);
            if (result.Result)
                TempData["SuccessMessage"] = Messages.DeletedSucessfully;
            else
                TempData["ErrorMessage"] = Messages.AddError;
            return RedirectToAction("Index");

            //return View(model);
        }

        public JsonResult UpdateStatus(int id, int statusId)
        {
            var result = SchoolService.UpdateStatus(id, statusId).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrorMessage"] = Messages.AddError;
            return Json(result);
        }
        //[AjaxOnly]
        //public ActionResult UploadImage()
        //{

        //    return View();
        //}

        [AjaxOnly]
        public JsonResult GetCityList(string stateId)
        {
            var cityList = (from a in SchoolService.GetCityList(int.Parse(stateId)).Result
                            select new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.Id.ToString()
                            }).ToList();

            return Json(cityList);
        }
        [AjaxOnly]
        public JsonResult GetPlanList(string planTypeId)
        {
            var planList = (from a in SchoolService.GetPlanList(int.Parse(planTypeId)).Result
                            select new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.Id.ToString()
                            }).ToList();

            return Json(planList);
        }
        #region # Profile Image #

        public ActionResult ProfilePicture(int userId)
        {
            if (userId == 0) return PartialView(new ProfileImageSelector());
            var user = SchoolService.GetSchool(userId).Result;
            var imageData = string.Empty;
            if (user.S3key != Guid.Empty)
            {
                try
                {
                    var webClient = new WebClient();
                    var imageBytes = webClient.DownloadData(CommonUtil.GetUrlByKey("School/" + user.S3key));
                    imageData = string.Format("{0},{1}", "data:image/png;base64", Convert.ToBase64String(imageBytes));
                }
                catch (Exception)
                {
                    imageData = string.Empty;
                }
            }
            return PartialView(new ProfileImageSelector { UserId = user.Id, S3Key = user.S3key, ImageData = imageData, ImagePath = user.S3key == Guid.Empty ? Url.Content("~/Content/images/gravator.png") : CommonUtil.GetUrlByKey("School/" + user.S3key) });
        }

        [HttpPost, ValidateAntiForgeryToken, AuthenticationRequired]
        public ActionResult ProfilePicture(ProfileImageSelector model)
        {
            try
            {
                var mediumImage = ImageFunctions.Base64StringToImage(model.ImageData);
                var mediumImageBytes = ImageFunctions.ImageToByteArray(mediumImage, ImageFormat.Jpeg);
                if (model.UserId == 0)
                {
                    var logoPath = Url.Content("~/Temp/" + model.S3Key + ".jpg");
                    System.IO.File.WriteAllBytes(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Temp", model.S3Key + ".jpg"), mediumImageBytes);
                    return Json(new { IsError = false, message = logoPath }, JsonRequestBehavior.AllowGet);
                }
                var result = AmazonFileHelper.UploadToS3(mediumImageBytes, "School/" + model.S3Key, AmazonSettings.AccessKey, AmazonSettings.SecretAccessKey, AmazonSettings.BucketName);
                if (result)
                    SchoolService.UpdateProfilePic(model.UserId, model.S3Key);
                return Json(result ? new { IsError = false, message = Messages.ProfilePictureChangedSuccess } : new { IsError = true, message = Messages.ServerError }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { IsError = true, message = Messages.ServerError }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult RemoveProfilePicture(int id)
        {
            var result = SchoolService.UpdateProfilePic(id, null);
            if (id == SessionItems.UserId)
                SessionItems.UserImagePath = Url.Content("~/Content/images/gravator.png");
            //RemoveLocalFileData(userId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult Invoice(int id)
        {
            var result = SchoolService.GetInvoiceData(id).Result;
            return View(result);
        }
    }
}

