﻿using System;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;
using EduErp.Web.Models;
using EduErp.Web.Utilities;

namespace EduErp.Web.Controllers
{
    public class StudentController : BaseController
    {
        //
        // GET: /Student/

        public ActionResult Index()
        {
           // var session = StudentService.GetSessions(SessionItems.SchoolId).Result;
            var classes = StudentService.GetClasses(SessionItems.SchoolId).Result;
            var model = new StudentBo
            {
                ListOfClasses = classes,
               // ListOfSessions = session,
                ListOfState = StateList,
                ListOfStatus = StudentService.GetStatus().Result
            };
            model.ListOfStatus.Insert(0, new BO.Common() { Id = 0, Name = "All" });
            model.ListOfClasses.Insert(0, new BO.Common() { Id = 0, Name = "All" });
           // model.ListOfStatus.Insert(0, new BO.Common() { Id = 0, Name = "All" });
            return View(model);
        }

        public ActionResult Add()
        {
            //var citystate = new CityStateBo { ListState = StateList };
            var genderList = StudentService.GetGender().Result;
            var titleList = StudentService.GetTitle().Result;
            var relegionList = StudentService.GetRelegion().Result;
            var categoryList = StudentService.GetCategory().Result;
            var nationalityList = StudentService.GetNationality().Result;
            //var sections = StudentService.GetSections().Result;
            var session = StudentService.GetSessions(SessionItems.SchoolId).Result;
            var classes = StudentService.GetClasses(SessionItems.SchoolId).Result;

            var student = new StudentBo
            {
                ListOfGender = genderList,
                ListOfReligion = relegionList,
                ListOfTitle = titleList,
                ListOfCategory = categoryList,
                ListOfNationality = nationalityList,
                ListOfState = StateList,
                ListOfClasses = classes,
                //ListOfSessions = session,
               
                SessionId = SessionItems.SessionId
            };
            
            // student.ListOfSections = sections;
            return View(student);
        }

        [HttpPost]
        public ActionResult GetFeeTransactionDetailForProfile(string fromMonth,string toMonth,int studentId)
        {

            return PartialView("_FeeTransactionDetailTemplateForProfile", StudentService.GetFeeTransactionDetailForProfile(Int32.Parse(fromMonth), Int32.Parse(toMonth), studentId, SessionItems.SessionId).Result.FeetransactionDetail);
        }
        [HttpPost]
        public ActionResult GetAttendanceForProfile(string fromMonth,string toMonth,int studentId)
        {
            
            return PartialView("_AttendanceDetailTemplateForProfile", StudentService.GetAttendanceDetailForProfile(Int32.Parse(fromMonth), Int32.Parse(toMonth), studentId, SessionItems.SessionId).Result.AttendanceDetail);
                
        }
        public ActionResult ViewProfile(int studentId)
        {
            var result = StudentService.GetStudentsDetail(studentId).Result;
            result.ListOfFromMonths = FeeTransactionService.GetMonthList().Result;
            result.ListOfToMonths = FeeTransactionService.GetMonthList().Result;
            result.ListOfFromMonths = AttendanceService.GetMonthList().Result;
            result.ListOfToMonths = AttendanceService.GetMonthList().Result;
           

            return View(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(StudentBo model)
        {
            //DateTime bdate;
            //DateTime.TryParseExact(model.DobStr, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out bdate);
            //DateTime jdate;
            //DateTime.TryParseExact(model.DojStr, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out jdate);
            //model.Dob = bdate;
            //model.Doj =jdate;
            if (!ModelState.IsValid) return View(model);
            var logoPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Temp", model.S3key + ".jpg");
            model.S3key = System.IO.File.Exists(logoPath) ? AmazonFileHelper.UploadFile("Student", logoPath, AmazonSettings.AccessKey, AmazonSettings.SecretAccessKey, AmazonSettings.BucketName) : new Guid();

            model.StatusId = (int)UserStatus.Active;
            model.CreatedOn = DateTime.Now;
            model.CreatedBy = SessionItems.UserId;
            model.SchoolId = SessionItems.SchoolId;
            model.SessionId = SessionItems.SessionId;
            model.ReasonForExit = "None";
           // model.ListOfStatus.Insert(0, new BO.Common() { Id = 0, Name = "All" });
            var leftStudent = StudentService.LeftStudent(SessionItems.SchoolId).Result;
            if (leftStudent >= 1)
            {
                var result = StudentService.AddStudent(model).Result;
                if (result == 1)
                {
                    TempData["SuccessMessage"] = Messages.AddSuccessfully;
                    return RedirectToAction("Index");
                }
                //if(model.StudentMobile)
                //foreach (var item in FeeTransactionService.GetAutomateMessageIds().Result)
                //{
                //    if (item.MessageAutomateId == (int)AutomateMesageFor.StudentCreated)
                //    {
                //        var smsResult = SendSms.SendTextSms(model.GuardianMobile.ToString(),item.TemplateMessage);
                //        if (smsResult.ErrorMessage == "Done")
                //        {
                //            TempData["SuccessMessage"] = Messages.SentSuccessfully;
                //            //return Json(new { isErr = false, msg = Messages.SentSuccessfully }, JsonRequestBehavior.AllowGet);
                //        }
                //    }
                //}

              else if (result == -2)
               {
                ModelState.AddModelError("StudentExistWithSameStudentCode", Messages.StudentExistWithSameStudentCode);
                model.ListOfGender = StudentService.GetGender().Result;
                model.ListOfReligion = StudentService.GetRelegion().Result;
                model.ListOfTitle = StudentService.GetTitle().Result;
                model.ListOfCategory = StudentService.GetCategory().Result;
                model.ListOfNationality = StudentService.GetNationality().Result;
                model.ListOfState = StateList;
                model.ListOfStatus = StudentService.GetStatus().Result;
                model.ListOfClasses = StudentService.GetClasses(SessionItems.SchoolId).Result;
                // model.ListOfSessions = StudentService.GetSessions(SessionItems.SchoolId).Result;
                return View(model);
            }
        }
            else
            {

                ModelState.AddModelError("StudentLimitExceedError", Messages.StudentLimitExceedError);
               // model.ri = UserService.GetRights().Result;
                model.ListOfGender = StudentService.GetGender().Result;
                model.ListOfReligion = StudentService.GetRelegion().Result;
                model.ListOfTitle = StudentService.GetTitle().Result;
                model.ListOfCategory = StudentService.GetCategory().Result;
                model.ListOfNationality = StudentService.GetNationality().Result;
                model.ListOfState = StateList;
                model.ListOfStatus = StudentService.GetStatus().Result;
                model.ListOfClasses = StudentService.GetClasses(SessionItems.SchoolId).Result;
                return View(model);
            }
           
            //if (result == -1)
            //{
            //    ModelState.AddModelError("StudentLimitExceedError", Messages.StudentLimitExceedError);
            //    model.ListOfGender = StudentService.GetGender().Result;
            //    model.ListOfReligion = StudentService.GetRelegion().Result;
            //    model.ListOfTitle = StudentService.GetTitle().Result;
            //    model.ListOfCategory = StudentService.GetCategory().Result;
            //    model.ListOfNationality = StudentService.GetNationality().Result;
            //    model.ListOfState = StateList;
            //    model.ListOfStatus = StudentService.GetStatus().Result;
            //    model.ListOfClasses = StudentService.GetClasses(SessionItems.SchoolId).Result;
            //   // model.ListOfSessions = StudentService.GetSessions(SessionItems.SchoolId).Result;
            //   return View(model);
            //}
           
            model.ListOfStatus = StudentService.GetStatus().Result;
            model.ListOfGender = StudentService.GetGender().Result;
            model.ListOfReligion = StudentService.GetRelegion().Result;
            model.ListOfTitle = StudentService.GetTitle().Result;
            model.ListOfCategory = StudentService.GetCategory().Result;
            model.ListOfNationality = StudentService.GetNationality().Result;
            model.ListOfState = StateList;
            model.ListOfClasses = StudentService.GetClasses(SessionItems.SchoolId).Result;
           // model.ListOfSessions = StudentService.GetSessions(SessionItems.SchoolId).Result;
            return View(model);
        }


        public ActionResult Edit(int id)
        {
            var student = StudentService.GetStudentById(id,SessionItems.SessionId).Result;
            student.ListOfState = StateList;
            student.ListOfCity = (from a in StudentService.GetCityList(student.PermanentStateId).Result
                                  select new SelectListItem()
                                  {
                                      Text = a.Name,
                                      Value = a.Id.ToString()
                                  }).ToList();
            student.PermanentCityId = student.PermanentCityId;
            student.PermanentStateId = student.PermanentStateId;


            var genderList = StudentService.GetGender().Result;
            var titleList = StudentService.GetTitle().Result;
            var relegionList = StudentService.GetRelegion().Result;
            var categoryList = StudentService.GetCategory().Result;
            var nationalityList = StudentService.GetNationality().Result;
            //  var sections = StudentService.GetSections().Result;
            var session = StudentService.GetSessions(SessionItems.SchoolId).Result;
            var classes = StudentService.GetClasses(SessionItems.SchoolId).Result;

            student.ListOfGender = genderList;
            student.ListOfReligion = relegionList;
            student.ListOfTitle = titleList;
            student.ListOfCategory = categoryList;
            student.ListOfNationality = nationalityList;
            student.ListOfState = StateList;
            student.ListOfClasses = classes;
            // student.ListOfSections = sections;
            student.ListOfSessions = session;
            
            student.IsCheked = student.CuurentAddress == student.PermanentAddress;
            return View(student);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(StudentBo model)
        {
            if (ModelState.IsValid)
            {
                model.SchoolId = SessionItems.SchoolId;
                model.StatusId = (int)UserStatus.Active;
                model.LastUpdatedBy = SessionItems.UserId;
                model.LastUpdatedOn = DateTime.Now;
                model.SessionId = SessionItems.SessionId;
                var result = StudentService.UpdateStudent(model).Result;
                if (result)
                    TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
                else
                    TempData["ErrMessage"] = Messages.AddError;
                return RedirectToAction("Index");

            }
            return View(model);
        }

        public ActionResult GetStudentList(int classId, int sectionId,int statusId)
        {
            return PartialView("_StudentTemplate", StudentService.GetStudentsList(classId, sectionId,statusId,SessionItems.SessionId ,SessionItems.SchoolId).Result);
        }

        [AjaxOnly]
        public JsonResult GetCurrentCityList(string currentStateId)
        {
            var cityList = (from a in StudentService.GetCityList(int.Parse(currentStateId)).Result
                            select new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.Id.ToString()
                            }).ToList();

            return Json(cityList);
        }
        [AjaxOnly]
        public JsonResult GetPermanentCityList(string permanentStateId)
        {
            var cityList = (from a in StudentService.GetCityList(int.Parse(permanentStateId)).Result
                            select new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.Id.ToString()
                            }).ToList();

            return Json(cityList);
        }
        [AjaxOnly]
        public JsonResult GetSectionList(string classId)
        {
            var sectionList = (from a in StudentService.GetSections(int.Parse(classId)).Result
                               select new SelectListItem()
                               {
                                   Text = a.Name,
                                   Value = a.Id.ToString()
                               }).ToList();

            return Json(sectionList);
        }

        [AjaxOnly]
        public ActionResult UpdateStudentStatus( int studentId, int studentStatusId)
        {
            StudentBo model = new  StudentBo();
            model.StatusId=studentStatusId;
            model.Id = studentId;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateStudentStatus(StudentBo model)
        {
            model.StatusId = model.StatusId  == 1 ?(int)UserStatus.InActive : (int)UserStatus.Active;
            var result = StudentService.UpdateStudentStatus(model, SessionItems.UserId).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(result);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateStatus(int studentId, int studentStatusId)
        {
            var result = StudentService.UpdateStatus(Convert.ToInt32(studentId), Convert.ToInt32(studentStatusId), SessionItems.UserId).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(result);
        }
        #region # Profile Image #

        public ActionResult ProfilePicture(int userId)
        {
            if (userId == 0) return PartialView(new ProfileImageSelector());
            var user = StudentService.GetStudentById(userId, 0).Result;
            var imageData = string.Empty;
            if (user.S3key != Guid.Empty)
            {
                try
                {
                    var webClient = new WebClient();
                    var imageBytes = webClient.DownloadData(CommonUtil.GetUrlByKey("Student/" + user.S3key));
                    imageData = string.Format("{0},{1}", "data:image/png;base64", Convert.ToBase64String(imageBytes));
                }
                catch (Exception)
                {
                    imageData = string.Empty;
                }
            }
            return PartialView(new ProfileImageSelector { UserId = user.Id, S3Key = user.S3key, ImageData = imageData, ImagePath = user.S3key == Guid.Empty ? Url.Content("~/Content/images/gravator.png") : CommonUtil.GetUrlByKey("Student/" + user.S3key) });
        }

        [HttpPost, ValidateAntiForgeryToken, AuthenticationRequired]
        public ActionResult ProfilePicture(ProfileImageSelector model)
        {
            try
            {
                var mediumImage = ImageFunctions.Base64StringToImage(model.ImageData);
                var mediumImageBytes = ImageFunctions.ImageToByteArray(mediumImage, ImageFormat.Jpeg);
                if (model.UserId == 0)
                {
                    var logoPath = Url.Content("~/Temp/" + model.S3Key + ".jpg");
                    System.IO.File.WriteAllBytes(Server.MapPath("~/Temp/" + model.S3Key + ".jpg"), mediumImageBytes);
                    return Json(new { IsError = false, message = logoPath }, JsonRequestBehavior.AllowGet);
                }
                var result = AmazonFileHelper.UploadToS3(mediumImageBytes, "Student/" + model.S3Key, AmazonSettings.AccessKey, AmazonSettings.SecretAccessKey, AmazonSettings.BucketName);
                if (result)
                    StudentService.UpdateProfilePic(model.UserId, model.S3Key);
                return Json(result ? new { IsError = false, message = Messages.ProfilePictureChangedSuccess } : new { IsError = true, message = Messages.ServerError }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Json(new { IsError = true, message = Messages.ServerError }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult RemoveProfilePicture(int id)
        {
            var result = StudentService.UpdateProfilePic(id, null);
            if (id == SessionItems.UserId)
                SessionItems.UserImagePath = Url.Content("~/Content/images/gravator.png");
            //RemoveLocalFileData(userId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion

    }
}
