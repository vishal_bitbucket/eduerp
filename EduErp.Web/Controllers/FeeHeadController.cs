﻿using System;
using System.Linq;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;

namespace EduErp.Web.Controllers
{
    public class FeeHeadController : BaseController
    {
        //
        // GET: /FeeHead/

        public ActionResult Index()
        {
            var result = FeeHeadService.GetFeeHeads(SessionItems.SchoolId).Result;
            return View(result);
        }

        public ActionResult Add()
        {
            var feetypeList = FeeHeadService.GetFeeHeadType().Result;
            var classList = FeeHeadService.GetClasses(SessionItems.SchoolId).Result;
            var feeHeadName = FeeHeadService.GetFeeHeadName(SessionItems.SchoolId).Result;
            var months = FeeHeadService.GetFeeHeadMonth().Result;
            FeeHeadBo model = new FeeHeadBo();
            model.ListOfMonths = months;
            model.FeeHeadTypeList = feetypeList;
            model.ListOfFeeHeadName = feeHeadName;
            model.ListOfClasses = classList;
            //model.ListOfSessions = SessionlList;
            model.SessionId = SessionItems.SessionId;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(FeeHeadBo model)
        {
            
            if (!ModelState.IsValid)
            {
                return Json(new { isErr = true, errMsg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            //model.FeeHeadId = model.Id;
            model.CreatedBy = SessionItems.UserId;
            model.CreatedOn = DateTime.Now;
            model.StatusId = (int)UserStatus.Active;
            model.SchoolId = SessionItems.SchoolId;
            model.SessionId = SessionItems.SessionId;
            var result = FeeHeadService.AddFeeHead(model).Result;
            if (result == 1)
            {
                TempData["SuccessMessage"] = Messages.AddSuccessfully;
                return Json(new { isErr = false, errMsg = Messages.AddSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            return Json(result == -1 ? new { isErr = true, errMsg = Messages.ExistError } : new { isErr = true, errMsg = Messages.AddError }, JsonRequestBehavior.AllowGet);
        }

       
        public ActionResult Edit(int id)
        {
            
            var feeHead = FeeHeadService.GetFeeHeadById(id).Result;
            if (feeHead.FeeHeadTypeId == (int)FeeHeadType.General || feeHead.FeeHeadTypeId == (int)FeeHeadType.ClassBy)
            {
                var model = new FeeHeadBo
                {
                    Name = feeHead.Name,
                    IsDiscount = feeHead.IsDiscount,
                    FeeHeadTypeId = feeHead.FeeHeadTypeId,
                    Description = feeHead.Description,
                    FeeHeadTypeList = FeeHeadService.GetFeeHeadType().Result,
                    ListOfClasses = FeeHeadService.GetClasses(SessionItems.SchoolId).Result,
                    ListOfFeeHeadName = FeeHeadService.GetFeeHeadName(SessionItems.SchoolId).Result,
                   // ListOfSessions = SessionlList,
                    Amount = feeHead.Amount,
                    ClassId = feeHead.ClassId,
                    ListOfMonths = FeeHeadService.GetFeeHeadMonth().Result
                };
                foreach (var lm in feeHead.ListOfMonths)
                {
                    var month = model.ListOfMonths.FirstOrDefault(a => a.MonthId == lm.MonthId);
                    if (month != null)
                    {
                        month.MonthId = lm.MonthId;
                        month.Selected = lm.Selected;
                        month.Amount = lm.Amount;

                    }
                }
                return View(model);
            }
            else {
                var model = new FeeHeadBo
                {
                    Name = feeHead.Name,
                    IsDiscount = feeHead.IsDiscount,
                    FeeHeadTypeId = feeHead.FeeHeadTypeId,
                    Description = feeHead.Description,
                    FeeHeadTypeList = FeeHeadService.GetFeeHeadType().Result,
                    ListOfClasses = FeeHeadService.GetClasses(SessionItems.SchoolId).Result,
                    ListOfFeeHeadName = FeeHeadService.GetFeeHeadName(SessionItems.SchoolId).Result,
                   // ListOfSessions = SessionlList,
                   // Amount = feeHead.Amount,
                   // ClassId = feeHead.ClassId,
                    ListOfMonths = FeeHeadService.GetFeeHeadMonth().Result
                };
                return View(model);
            }
            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FeeHeadBo model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { isErr = true, errMsg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            model.LastModifiedBy = SessionItems.UserId;
            model.LastModifiedOn = DateTime.Now;
            model.SchoolId = SessionItems.SchoolId;
            model.SessionId = SessionItems.SessionId;
            var result = FeeHeadService.UpdateFeeHead(model).Result;
            if (result == 1)
            {
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
                return Json(new { isErr = false, errMsg = Messages.UpdateSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            if (result == -2)
            {
                TempData["SuccessMessage"] = Messages.ExistError;
                return Json(new { isErr = true, errMsg = Messages.ExistError }, JsonRequestBehavior.AllowGet);
            }
            return Json(result == -1 ? new { isErr = true, errMsg = Messages.UpdateRestrictionError } : new { isErr = true, errMsg = Messages.AddError }, JsonRequestBehavior.AllowGet);


        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateStatus(int feeheadId, int statusId)
        {
            var result = FeeHeadService.UpdateStatus(feeheadId, statusId, SessionItems.UserId).Result;
            if (result == 1)
            {
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
                return Json(new { isErr = false, errMsg = Messages.UpdateSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            return Json(result == -1 ? new { isErr = true, errMsg = Messages.UpdateRestrictionError } : new { isErr = true, errMsg = Messages.UpdateError }, JsonRequestBehavior.AllowGet);
        }
    }
}
