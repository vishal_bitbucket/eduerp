﻿using System;
using System.Linq;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;

namespace EduErp.Web.Controllers
{
    public class ExamController : BaseController
    {
        //
        // GET: /Exam/

        public ActionResult Index()
        {
            return View(ExamService.GetExams(SessionItems.SchoolId).Result);
        }

        public ActionResult Add()
        {
            var model = new ExamBo
            {
                ListOfSubjects = EmployeeService.GetSubjects(SessionItems.SchoolId).Result,
                ListOfSessions=SessionlList
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(ExamBo model)
        {
            if (!ModelState.IsValid)
            {
                model.ListOfSubjects = EmployeeService.GetSubjects(SessionItems.SchoolId).Result;
                return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            model.SchoolId = SessionItems.SchoolId;
            model.CreatedOn = DateTime.Now;
            model.CreatedBy = SessionItems.UserId;
            model.StatusId = (int)UserStatus.Active;

            var result = ExamService.AddExam(model).Result;
            if (result == 1)
            {
                TempData["SuccessMessage"] = Messages.AddSuccessfully;
                return Json(new { isErr = false, errMsg = Messages.AddSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            return Json(result == -1 ? new { isErr = true, errMsg = Messages.ExistError } : new { isErr = true, errMsg = Messages.AddError }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var res = ExamService.GetExamById(id, SessionItems.SchoolId).Result;
            res.ListOfSubjects = EmployeeService.GetSubjects(SessionItems.SchoolId).Result;
            res.ListOfSessions = SessionlList;
            return View(res);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ExamBo model)
        {
            if (!ModelState.IsValid)
            {
                model.ListOfSubjects = EmployeeService.GetSubjects(SessionItems.SchoolId).Result;
                return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            model.SchoolId = SessionItems.SchoolId;
            model.LastUpdatedOn = DateTime.Now;
            model.LastUpdatedBy = SessionItems.UserId;
            var result = ExamService.UpdateExam(model).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.AddError;
            return Json(new { isErr = false, msg = Messages.UpdateSuccessfully }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateStatus(int id, int statusId)
        {
            var result = ExamService.UpdateStatus(id, statusId, SessionItems.UserId).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(result);
        }

    }
}
