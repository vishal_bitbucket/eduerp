﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EduErp.BO;
using EduErp.BLL;
using EduErp.Common;
using EduErp.Mvc.Utilities;

namespace EduErp.Web.Controllers
{
    public class OTExamSubjectController : BaseController
    {
        //
        // GET: /OTExamSubject/

        public ActionResult Index()
        {
            return View(OTExamSubjectService.GetExamSubject(SessionItems.SchoolId).Result);
        }
        public ActionResult Add() 
        {
            
            var examList = OTExamSubjectService.GetExams(SessionItems.SchoolId).Result;
            var subjectList = OTExamSubjectService.GetSubjects(SessionItems.SchoolId).Result;
            var ExamSubject = new OTExamSubjectMapping
            {
                ListOfSubject = subjectList,
                ListOfExam = examList,
            };
            return View(ExamSubject);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(OTExamSubjectMapping model)
        {
           
            if (!ModelState.IsValid)
            {
               
                return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            model.SchoolId = SessionItems.SchoolId;
            model.CreatedOn = DateTime.Now;
            model.CreatedBy = SessionItems.UserId;
            model.StatusId = (int)UserStatus.Active;

          
            var result = OTExamSubjectService.AddExamSubject(model).Result;
            if (result > 0)
            {
                TempData["SuccessMessage"] = Messages.AddSuccessfully;
                return Json(new { isErr = false, errMsg = Messages.AddSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            return Json(result == -1 ? new { isErr = true, errMsg = Messages.ExistError } : new { isErr = true, errMsg = Messages.AddError }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateStatus(int id, int statusId)
        {
            var result = OTExamSubjectService.UpdateStatus(id, statusId, SessionItems.UserId).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(result);
        }
        [AjaxOnly]
        public ActionResult Edit(int id)
        {
            
            var examList = OTExamSubjectService.GetExams(SessionItems.SchoolId).Result;
            var subjectList = OTExamSubjectService.GetSubjects(SessionItems.SchoolId).Result;
            var result = OTExamSubjectService.GetOTExamSubjectById(id).Result;
            var model = new OTExamSubjectMapping
            {
                SubjectId = result.SubjectId,
                SubjectName = result.SubjectName,
                ExamId = result.ExamId,
                ExamName = result.ExamName,
                TimeInMinutes = result.TimeInMinutes,
                Sequence = result.Sequence,
                StatusId = result.StatusId,
                SchoolId = result.SchoolId,
                ListOfSubject = subjectList,
                ListOfExam = examList
            };
            return PartialView(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(OTExamSubjectMapping model)
        {

            if (!ModelState.IsValid)
            {
                //model.ListOfSchools = Schools;
                return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            model.SchoolId = SessionItems.SchoolId;
            model.StatusId = (int)UserStatus.Active;
            model.LastUpdatedBy = SessionItems.UserId;
            model.LastUpdatedOn = DateTime.Now;
            var result = OTExamSubjectService.UpdateExamSubject(model).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(new { isErr = false, msg = Messages.UpdateSuccessfully }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult GetExamSubjectListForExamSubjectMapping(int examId)
        {
            OTExamSubjectMapping model = new OTExamSubjectMapping { ExamSubjectLists = GetExamSubjectLists(examId, SessionItems.SchoolId) };
            return PartialView("_ExamSubjectTemplate",model);
        }
        [NonAction]
        public List<ExamSubjectList> GetExamSubjectLists(int examId, int schoolId)
        {
            var examSubjectLists = OTExamSubjectService.GetExamSubjectMappingList(examId, schoolId).Result;
            return examSubjectLists;
        }
       
    }
}
