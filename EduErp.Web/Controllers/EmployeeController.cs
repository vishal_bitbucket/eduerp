﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;
using EduErp.Web.Models;
using EduErp.Web.Utilities;

namespace EduErp.Web.Controllers
{
    public class EmployeeController : BaseController
    {
        //
        // GET: /Employee/

        public ActionResult Index()
        {
            return View(EmployeeService.GetEmployees(SessionItems.SchoolId).Result);
        }

        public ActionResult Add()
        {

            var genderList = StudentService.GetGender().Result;
            var titleList = StudentService.GetTitle().Result;
            var relegionList = StudentService.GetRelegion().Result;
            var categoryList = StudentService.GetCategory().Result;
            var nationalityList = StudentService.GetNationality().Result;
            var mediumList = EmployeeService.GetMedium().Result;
            var subjectList = EmployeeService.GetSubjects(SessionItems.SchoolId).Result;
            var classes = StudentService.GetClasses(SessionItems.SchoolId).Result;
            var Employee = new EmployeeBo
             {
                 ListOfGender = genderList,
                 ListOfReligion = relegionList,
                 ListOfTitle = titleList,
                 ListOfCategory = categoryList,
                 ListOfNationality = nationalityList,
                 ListOfState = StateList,
                 ListOfMedium = mediumList,
                 ListOfClasses = classes,
                 ListOfSubjects = subjectList,
                 ListOfEmployeeType =   EmployeeService.GetEmployeeTypeList(SessionItems.SchoolId).Result,
             };

            return View(Employee);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(EmployeeBo model)
        {
            //DateTime bdate;
            //DateTime.TryParseExact(model.DateOfBirthStr, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out bdate);
            //DateTime jdate;
            //DateTime.TryParseExact(model.DateOfJoiningStr, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out jdate);
            //model.DateOfBirth = bdate;
            //model.DOJ = jdate;
            if (!ModelState.IsValid)
            {
                var genderList = StudentService.GetGender().Result;
                var titleList = StudentService.GetTitle().Result;
                var relegionList = StudentService.GetRelegion().Result;
                var categoryList = StudentService.GetCategory().Result;
                var nationalityList = StudentService.GetNationality().Result;
                var mediumList = EmployeeService.GetMedium().Result;
                var subjectList = EmployeeService.GetSubjects(SessionItems.SchoolId).Result;
                var classes = StudentService.GetClasses(SessionItems.SchoolId).Result;
                model.ListOfEmployeeType = EmployeeService.GetEmployeeTypeList(SessionItems.SchoolId).Result;
                model.ListOfGender = genderList;
                model.ListOfReligion = relegionList;
                model.ListOfTitle = titleList;
                model.ListOfCategory = categoryList;
                model.ListOfNationality = nationalityList;
                model.ListOfState = StateList;
                model.ListOfMedium = mediumList;
                model.ListOfClasses = classes;
                model.ListOfSubjects = subjectList;
                return View(model);
            }
            var logoPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Temp", model.S3key + ".jpg");
            model.S3key = System.IO.File.Exists(logoPath) ? AmazonFileHelper.UploadFile("Employee", logoPath, AmazonSettings.AccessKey, AmazonSettings.SecretAccessKey, AmazonSettings.BucketName) : new Guid();

            model.StatusId = (int)UserStatus.Active;
            model.CreatedOn = DateTime.Now;
            model.CreatedBy = SessionItems.UserId;
            model.SchoolId = SessionItems.SchoolId;
            var result = EmployeeService.AddEmployee(model).Result;
            if (result == 1)
            {
                TempData["SuccessMessage"] = Messages.AddSuccessfully;
                return RedirectToAction("Index");
            }
             if (result == -1)
            {
                ModelState.AddModelError("EmployeeCode", Messages.EmployeeExistWithSameEmployeeCode);
              
                model.ListOfEmployeeType = EmployeeService.GetEmployeeTypeList(SessionItems.SchoolId).Result;
                model.ListOfGender = StudentService.GetGender().Result;
                model.ListOfReligion = StudentService.GetRelegion().Result;
                model.ListOfTitle = StudentService.GetTitle().Result;
                model.ListOfCategory = StudentService.GetCategory().Result;
                model.ListOfNationality = StudentService.GetNationality().Result;
                model.ListOfState = StateList;
                model.ListOfMedium = EmployeeService.GetMedium().Result; 
                model.ListOfClasses = StudentService.GetClasses(SessionItems.SchoolId).Result;
                model.ListOfSubjects = EmployeeService.GetSubjects(SessionItems.SchoolId).Result; 
            }
            
             model.ListOfEmployeeType = EmployeeService.GetEmployeeTypeList(SessionItems.SchoolId).Result;
             model.ListOfGender = StudentService.GetGender().Result;
             model.ListOfReligion = StudentService.GetRelegion().Result;
             model.ListOfTitle = StudentService.GetTitle().Result;
             model.ListOfCategory = StudentService.GetCategory().Result;
             model.ListOfNationality = StudentService.GetNationality().Result;
             model.ListOfState = StateList;
             model.ListOfMedium = EmployeeService.GetMedium().Result;
             model.ListOfClasses = StudentService.GetClasses(SessionItems.SchoolId).Result;
             model.ListOfSubjects = EmployeeService.GetSubjects(SessionItems.SchoolId).Result; 
             return View(model);
           
        }
        public ActionResult Edit(int id)
        {
            var Employee = EmployeeService.GetEmployee(SessionItems.SchoolId, id).Result;
            var genderList = StudentService.GetGender().Result;
            var titleList = StudentService.GetTitle().Result;
            var relegionList = StudentService.GetRelegion().Result;
            var categoryList = StudentService.GetCategory().Result;
            var nationalityList = StudentService.GetNationality().Result;
            var mediumList = EmployeeService.GetMedium().Result;
            var classes = StudentService.GetClasses(SessionItems.SchoolId).Result;
            var subjectList = EmployeeService.GetSubjects(SessionItems.SchoolId).Result;
            Employee.ListOfEmployeeType = EmployeeService.GetEmployeeTypeList(SessionItems.SchoolId).Result;
            Employee.ListOfSubjects = subjectList;
            Employee.ListOfClasses = classes;
            Employee.ListOfGender = genderList;
            Employee.ListOfReligion = relegionList;
            Employee.ListOfTitle = titleList;
            Employee.ListOfCategory = categoryList;
            Employee.ListOfNationality = nationalityList;
            Employee.ListOfState = StateList;
            Employee.ListOfMedium = mediumList;
            if (Employee.DateOfBirth != null) Employee.DateOfBirthStr = Employee.DateOfBirth.Value.ToFormattedDate();
            if (Employee.DOJ != null) Employee.DateOfJoiningStr=Employee.DOJ.Value.ToFormattedDate();
            if (Employee.DateOfExit != null) Employee.DateOfExitStr = Employee.DateOfExit.Value.ToFormattedDate();
            Employee.IsChecked = Employee.CurrentAddress == Employee.PermanentAddress;
            //DateTime bdate;
            //DateTime.TryParseExact(Employee.DateOfBirthStr, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out bdate);
            //DateTime jdate;
            //DateTime.TryParseExact(Employee.DateOfJoiningStr, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out jdate);
            //Employee.DateOfBirth = bdate;
            //Employee.DOJ = jdate;
            return View(Employee);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EmployeeBo model)
        {
            //DateTime bdate;
            //DateTime.TryParseExact(model.DateOfBirthStr,"dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out bdate);
            //DateTime jdate;
            //DateTime.TryParseExact(model.DateOfJoiningStr, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out jdate);
            //model.DateOfBirth = bdate;
            //model.DOJ = jdate;
            if (!ModelState.IsValid)
            {

                var genderList = StudentService.GetGender().Result;
                var titleList = StudentService.GetTitle().Result;
                var relegionList = StudentService.GetRelegion().Result;
                var categoryList = StudentService.GetCategory().Result;
                var nationalityList = StudentService.GetNationality().Result;
                var mediumList = EmployeeService.GetMedium().Result;
                var subjectList = EmployeeService.GetSubjects(SessionItems.SchoolId).Result;
                var classes = StudentService.GetClasses(SessionItems.SchoolId).Result;
                model.ListOfClasses = classes;
                model.ListOfGender = genderList;
                model.ListOfReligion = relegionList;
                model.ListOfTitle = titleList;
                model.ListOfCategory = categoryList;
                model.ListOfNationality = nationalityList;
                model.ListOfState = StateList;
                model.ListOfMedium = mediumList;
                model.ListOfSubjects = subjectList;
                model.ListOfEmployeeType = EmployeeService.GetEmployeeTypeList(SessionItems.SchoolId).Result;
                return View(model);
            }
           model.StatusId = (int)UserStatus.Active;
            model.LastUpdatedOn = DateTime.Now;
            model.LastUpdatedBy = SessionItems.UserId;
            model.SchoolId = SessionItems.SchoolId;
            var result = EmployeeService.UpdateEmployee(model).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.AddError;
            return RedirectToAction("Index");

        }

        [AjaxOnly]
        public JsonResult GetCurrentCityList(string currentStateId)
        {
            var cityList = (from a in StudentService.GetCityList(int.Parse(currentStateId)).Result
                            select new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.Id.ToString()
                            }).ToList();

            return Json(cityList);
        }
        [AjaxOnly]
        public JsonResult GetPermanentCityList(string permanentStateId)
        {
            var cityList = (from a in StudentService.GetCityList(int.Parse(permanentStateId)).Result
                            select new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.Id.ToString()
                            }).ToList();

            return Json(cityList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateStatus(int id, int statusId)
        {
            var result = EmployeeService.UpdateStatus(id, statusId, SessionItems.UserId).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(result);
        }

        [AjaxOnly]
        public JsonResult GetSectionList(string classId)
        {
            var sectionList = (from a in StudentService.GetSections(int.Parse(classId)).Result
                               select new SelectListItem()
                               {
                                   Text = a.Name,
                                   Value = a.Id.ToString()
                               }).ToList();

            return Json(sectionList);
        }

        

        #region # Profile Image #

        public ActionResult ProfilePicture(int userId)
        {
            if (userId == 0) return PartialView(new ProfileImageSelector());
            var user = EmployeeService.GetEmployee( SessionItems.SchoolId,userId).Result;
            var imageData = string.Empty;
            if (user.EmployeeS3Key != Guid.Empty)
            {
                try
                {
                    var webClient = new WebClient();
                    var imageBytes = webClient.DownloadData(CommonUtil.GetUrlByKey("Employee/" + user.EmployeeS3Key));
                    imageData = string.Format("{0},{1}", "data:image/png;base64", Convert.ToBase64String(imageBytes));
                }
                catch (Exception)
                {
                    imageData = string.Empty;
                }
            }
            return PartialView(new ProfileImageSelector { UserId = user.Id, S3Key = user.EmployeeS3Key ?? Guid.Empty, ImageData = imageData, ImagePath = user.EmployeeS3Key == Guid.Empty ? Url.Content("~/Content/images/gravator.png") : CommonUtil.GetUrlByKey("Employee/" + user.EmployeeS3Key) });
        }

        [HttpPost, ValidateAntiForgeryToken, AuthenticationRequired]
        public ActionResult ProfilePicture(ProfileImageSelector model)
        {
            try
            {
                var mediumImage = ImageFunctions.Base64StringToImage(model.ImageData);
                var mediumImageBytes = ImageFunctions.ImageToByteArray(mediumImage, ImageFormat.Jpeg);
                if (model.UserId == 0)
                {
                    var logoPath = Url.Content("~/Temp/" + model.S3Key + ".jpg");
                    System.IO.File.WriteAllBytes(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Temp", model.S3Key + ".jpg"), mediumImageBytes);
                    return Json(new { IsError = false, message = logoPath }, JsonRequestBehavior.AllowGet);
                }
                var result = AmazonFileHelper.UploadToS3(mediumImageBytes, "Employee/" + model.S3Key, AmazonSettings.AccessKey, AmazonSettings.SecretAccessKey, AmazonSettings.BucketName);
                if (result)
                    EmployeeService.UpdateProfilePic(model.UserId, model.S3Key);
                return Json(result ? new { IsError = false, message = Messages.ProfilePictureChangedSuccess } : new { IsError = true, message = Messages.ServerError }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { IsError = true, message = Messages.ServerError }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult RemoveProfilePicture(int id)
        {
            var result = EmployeeService.UpdateProfilePic(id, null);
            if (id == SessionItems.UserId)
                SessionItems.UserImagePath = Url.Content("~/Content/images/gravator.png");
            //RemoveLocalFileData(userId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}
