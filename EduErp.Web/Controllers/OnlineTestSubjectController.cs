﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;
using EduErp.BLL;
using System.IO;

namespace EduErp.Web.Controllers
{
    public class OnlineTestSubjectController : BaseController
    {
        //
        // GET: /OnlineTestSubject/

        public ActionResult Index()
        {
            return View(OnlineTestSubjectService.GetOnlineTestSubject(SessionItems.SchoolId).Result);
        }
        public ActionResult Add()
        {
           
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(OnlineExamTestSubjectBo model)
        {
            if (!ModelState.IsValid)
            {
                //model.ListOfSchools = Schools;
                return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            model.SchoolId = SessionItems.SchoolId;
            model.CreatedOn = DateTime.Now;
            model.CreatedBy = SessionItems.UserId;
            model.Status = (int)UserStatus.Active;
            //model.ListOfSchools = Schools;
            var result = OnlineTestSubjectService.AddOnlineTestSubject(model).Result;
            if (result > 0)
            {
                TempData["SuccessMessage"] = Messages.AddSuccessfully;
                return Json(new { isErr = false, errMsg = Messages.AddSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            return Json(result == -1 ? new { isErr = true, errMsg = Messages.ExistError } : new { isErr = true, errMsg = Messages.AddError }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateStatus(int id, int status)
        {
            var result = OnlineTestSubjectService.UpdateStatus(id, status, SessionItems.UserId).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(result);
        }
        [AjaxOnly]
        public ActionResult Edit(int id)
        {
            var result = OnlineTestSubjectService.GetOnlineTestSubjectById(id).Result;
            var model = new OnlineExamTestSubjectBo
            {
               SubjectName = result.SubjectName,
                
                Status = result.Status,
                SchoolId = result.SchoolId
            };
            return PartialView(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(OnlineExamTestSubjectBo model)
        {

            if (!ModelState.IsValid)
            {
             
                return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            model.Status = (int)UserStatus.Active;
            model.LastUpdatedBy = SessionItems.UserId;
            model.LastUpdatedOn = DateTime.Now;
            var result = OnlineTestSubjectService.UpdateOnlineTestSubject(model).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(new { isErr = false, msg = Messages.UpdateSuccessfully }, JsonRequestBehavior.AllowGet);

        }
    }
}
