﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EduErp.Mvc.Utilities.Filters;

namespace EduErp.Web.Controllers
{
    [NoCache, AllowAnonymous]
    public class ErrorController : Controller
    {
        public ActionResult Index(string c)
        {
            int errorCode;
            if (!int.TryParse(c, out errorCode))
                return View("Index");

            switch (errorCode)
            {
                case 404:
                    return View("NotFound");

                default:
                    return View("Index");
            }
        }

        public ActionResult E404()
        {
            return View("NotFound");
        }

        public ActionResult E401()
        {
            return View();
        }

        public ActionResult E403()
        {
            return View();
        }

        public ActionResult E405()
        {
            return View();
        }

        public ActionResult E406()
        {
            return View();
        }

        public ActionResult E412()
        {
            return View();
        }

        public ActionResult E500()
        {
            return View();
        }

        public ActionResult E501()
        {
            return View();
        }

        public ActionResult E502()
        {
            return View();
        }
    }
}
