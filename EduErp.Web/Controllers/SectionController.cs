﻿using System;
using System.Linq;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;

namespace EduErp.Web.Controllers
{
    public class SectionController : BaseController
    {
        //
        // GET: /Section/

        public ActionResult Index()
        {
            var result = SectionService.GetSections(SessionItems.SchoolId).Result;
            return View(result);
        }
        [AjaxOnly]
        public ActionResult Add()
        {

            var classList = SectionService.GetClasses(SessionItems.SchoolId).Result;
            SectionBo model = new SectionBo();
            model.ListOfClasses = classList;
            model.SchoolList = Schools;
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(SectionBo model)
        {
            model.SchoolId = SessionItems.SchoolId;
            if (!ModelState.IsValid)
            {
                return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }

            model.CreatedOn = DateTime.Now;
            model.CreatedBy = SessionItems.UserId;
            model.Status = (int)UserStatus.Active;
            var result = SectionService.AddSection(model).Result;
            if (result == 1)
            {
                TempData["SuccessMessage"] = Messages.AddSuccessfully;
                return Json(new { isErr = false, errMsg = Messages.AddSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            return Json(result == -1 ? new { isErr = true, errMsg = Messages.ExistError } : new { isErr = true, errMsg = Messages.AddError }, JsonRequestBehavior.AllowGet);

        }

        [AjaxOnly]
        public ActionResult Edit(int id)
        {
            var classList = SectionService.GetClasses(SessionItems.SchoolId).Result;
            var result = SectionService.GetSectionById(id).Result;
            result.ListOfClasses = classList;
            result.SchoolList = Schools;
            return PartialView(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SectionBo model)
        {
            if (!ModelState.IsValid)
            {

                return Json(new { isErr = true, msg = ModelState.Values.SelectMany(x => x.Errors).ToArray().First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            model.LastUpdatedBy = SessionItems.UserId;
            model.LastUpdatedOn = DateTime.Now;
            var result = SectionService.UpdateSection(model).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(new { isErr = false, msg = Messages.UpdateSuccessfully }, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateStatus(int id, int status)
        {
            var result = SectionService.UpdateStatus(id, status, SessionItems.UserId).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(result);
        }

    }
}
