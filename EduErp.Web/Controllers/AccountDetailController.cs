﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;

namespace EduErp.Web.Controllers
{
    public class AccountDetailController : BaseController
    {
        //
        // GET: /AccountDetail/

        public ActionResult Index()
        {
            return View(AccountDetailService.GetAccountDetail().Result);
        }

        public ActionResult Add()
        {
            AccountDetailBo model = new AccountDetailBo
            {
                ListOfSchools = Schools,
                ListOfPlans = AccountDetailService.GetPlan().Result,
                ListOfDays = AccountDetailService.GetDay().Result
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(AccountDetailBo model)
        {

            model.WeeklyHolidays = string.Join(",", model.ListOfDays.Where(a => a.Selected).Select(b => b.DayId).ToArray());

            //DateTime npdate;
            //DateTime.TryParseExact(model.NextPaymentDateStr, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out npdate);
            //model.NextPaymentDate = npdate;

            //DateTime apdate;
            //DateTime.TryParseExact(model.AppStartDateStr, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out apdate);
            //model.AppStartDate = apdate;

            //DateTime duedate;
            //DateTime.TryParseExact(model.DueDateStr, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out duedate);
            //model.DueDate = duedate;
            if (!ModelState.IsValid) return View(model);
            model.ListOfSchools = Schools;
            model.ListOfPlans = AccountDetailService.GetPlan().Result;
            model.CreatedBy = SessionItems.UserId;
            model.CreatedOn = DateTime.Now;
            model.StatusId = (int)UserStatus.Active;
            var result = AccountDetailService.AddAcountDetail(model).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.AddSuccessfully;
            else
                TempData["ErrMessage"] = Messages.AddError;
            return Json(new { isErr = false, msg = Messages.AddSuccessfully }, JsonRequestBehavior.AllowGet);

        }


        public ActionResult Edit(int id)
        {
            var model = AccountDetailService.GetAccountDetailById(id).Result;
            model.ListOfSchools = Schools;
            model.ListOfPlans = AccountDetailService.GetPlan().Result;
            if (model.NextPaymentDate != null)
                model.NextPaymentDateStr = model.NextPaymentDate.Value.ToFormattedDate();
            if (model.DueDate != null)
                model.DueDateStr = model.DueDate.Value.ToFormattedDate();
            if (model.AppStartDate != null)
                model.AppStartDateStr = model.AppStartDate.Value.ToFormattedDate();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AccountDetailBo model)
        {
            //DateTime adate;
            //DateTime.TryParseExact(model.AppStartDateStr, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out adate);
            //model.AppStartDate = adate;
            //DateTime npdate;
            //DateTime.TryParseExact(model.NextPaymentDateStr, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out npdate);
            //model.NextPaymentDate = npdate;
            //DateTime duedate;
            //DateTime.TryParseExact(model.DueDateStr, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out duedate);
            //model.DueDate = duedate;
            if (!ModelState.IsValid) return View(model);
            model.ListOfSchools = Schools;
            model.ListOfPlans = AccountDetailService.GetPlan().Result;
            model.UpdatedBy = SessionItems.UserId;
            model.UpdatedOn = DateTime.Now;
            var result = AccountDetailService.UpdateAccountDetail(model).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.AddError;
            return Json(new { isErr = false, msg = Messages.UpdateSuccessfully }, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateStatus(int id, int statusId)
        {
            var result = AccountDetailService.UpdateStatus(id, statusId, SessionItems.UserId).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(result);
        }
    }
}
