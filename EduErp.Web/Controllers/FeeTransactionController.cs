﻿using System;
using System.Linq;
using System.Web.Mvc;
using EduErp.BLL;
using EduErp.BO;
using EduErp.Common;
using EduErp.Mvc.Utilities;

namespace EduErp.Web.Controllers
{
    public class FeeTransactionController : BaseController
    {
        //
        // GET: /FeeTransaction/

        public ActionResult Index(int? sectionId, int? classId)
        {
            FeeTransactionBo model = new FeeTransactionBo
            {
                //ListOfSessions = SessionlList,
                ListOfClasses = FeeTransactionService.GetClasses(SessionItems.SchoolId).Result,
                //FeeTransactionTemplateBo = BLL.FeeTransactionService.GetFeeTransactionDetailByStudentId(SessionItems.SchoolId, sessionId??0, sectionId??0, classId??0).Result
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult GetTransactionDetail(int sectionId, int classId)
        {
            //if (sessionId !=0 && sectionId!=0 && classId!=0)
            var result = FeeTransactionService.GetFeeTransactionDetailByClassSection(sectionId, classId, SessionItems.SchoolId, SessionItems.SessionId).Result;
            //for(int i=0;i< result.FeetransactionDetailList.Count;i++)
            //{
            //    result.FeetransactionDetailList[i].TotalPayableAmount=(result.FeetransactionDetailList[i].TotalPayableAmount);
            //}
            return PartialView("_TransactionTemplate", result);

        }

        [HttpPost]
        public ActionResult GetFeeTransactionDetailByStudentId(int studentId, string studentName)
        {
            TempData["StudentName"] = studentName;
            var result = FeeTransactionService.GetFeeTransactionDetailByStudentId(studentId, SessionItems.SessionId).Result;
           
            // for(int i=0;i< result.FeetransactionDetailList.Count;i++)
            //{
            //    result.FeetransactionDetailList[i].TotalPayableAmount=(result.FeetransactionDetailList[i].TotalPayableAmount);
            //}
            return PartialView("_FeeTransactionDetailById", result);
        }

        public ActionResult Add()
        {
            FeeTransactionBo model = new FeeTransactionBo
            {
                // ListOfSessions = SessionlList,
                ListOfDiscountFeeheads = FeeTransactionService.GetDiscountFeeHeadsList(SessionItems.SchoolId).Result,
                ListOfFineFeeheads = FeeTransactionService.GetFineFeeHeadsList(SessionItems.SchoolId).Result,
                ListOfMonthes = FeeTransactionService.GetMonthList().Result,
                ListOfClasses = FeeTransactionService.GetClasses(SessionItems.SchoolId).Result,
                ListOfPaymentMode = FeeTransactionService.GetPaymentModeList().Result
            };
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(FeeTransactionBo model)
        {
            //model.ListOfSessions = SessionlList;

            model.ListOfDiscountFeeheads = FeeTransactionService.GetDiscountFeeHeadsList(SessionItems.SchoolId).Result;
            model.ListOfFineFeeheads = FeeTransactionService.GetFineFeeHeadsList(SessionItems.SchoolId).Result;
            model.ListOfMonthes = FeeTransactionService.GetMonthList().Result;
            model.ListOfClasses = FeeTransactionService.GetClasses(SessionItems.SchoolId).Result;
            model.ListOfPaymentMode = FeeTransactionService.GetPaymentModeList().Result;
            if (ModelState.IsValid)
            {
                model.SessionId = SessionItems.SessionId;
                model.StatusId = (int)UserStatus.Active;
                model.CreatedBy = SessionItems.UserId;
                model.CreatedOn = DateTime.Now;
                model.SessionId = SessionItems.SessionId;
                model.TransactionDate = DateTime.Now;
                model.TotalClassGeneralAmount = model.ClassByTotalAmount + model.GeneralByTotalAmount;

                // var count = model.StudentLists.Count(item => item.Selected);
                var transactionId = FeeTransactionService.AddFeeTransaction(model, SessionItems.SchoolId).Result;
                if (transactionId > 0)
                {
                    TempData["SuccessMessage"] = Messages.AddSuccessfully;
                    return Json(new { isErr = false, errMsg = Messages.AddSuccessfully, transactionId = transactionId }, JsonRequestBehavior.AllowGet);
                    //  return RedirectToAction("FeeReciept", new { transactionId });
                }
                return Json(transactionId == -1 ? new { isErr = true, errMsg = Messages.ExistErrorForSave } : new { isErr = true, errMsg = Messages.AddError }, JsonRequestBehavior.AllowGet);

            }
            var str = MessageService.GetNumberByStudentId(model.StudentId);
            var leftsms = MessageService.LeftMessage(SessionItems.SchoolId).Result;
            if (leftsms >= 1)
            {
                foreach (var item in FeeTransactionService.GetAutomateMessageIds(SessionItems.SchoolId).Result)
                {
                    if (item.MessageAutomateId == (int)AutomateMesageFor.Dues)
                    {
                        var smsResult = SendSms.SendTextSms(str.ToString(), "TEST");
                        if (smsResult.ErrorMessage == "Done")
                        {
                            TempData["SuccessMessage"] = Messages.SentSuccessfully;
                            return Json(new { isErr = false, errMsg = Messages.SentSuccessfully }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            else
            {
                // TempData["ErrMessage"] = Messages.MessageLimitExceedError;
                model.ListOfClasses = StudentService.GetClasses(SessionItems.SchoolId).Result;
                return Json(new { isErr = true, errMsg = Messages.MessageLimitExceedError }, JsonRequestBehavior.AllowGet);
            }

            //var component= FeeTransactionService.GetComponent(model.SessionId,SessionItems.SchoolId, model.FromMonthId, model.ToMonthId, model.ClassId).Result;
            //model.FeeTransactionTemplateBo = component;
            return View(model);

        }

        public ActionResult Edit(int id)
        {
            FeeTransactionBo model = FeeTransactionService.GetTransactionByTransactionIdForEdit(id, SessionItems.SchoolId).Result;
            // model.ListOfSessions = SessionlList;
            model.ListOfDiscountFeeheads = FeeTransactionService.GetDiscountFeeHeadsList(SessionItems.SchoolId).Result;
            model.ListOfFineFeeheads = FeeTransactionService.GetFineFeeHeadsList(SessionItems.SchoolId).Result;
            model.ListOfMonthes = FeeTransactionService.GetMonthList().Result;
            model.ListOfPaymentMode = FeeTransactionService.GetPaymentModeList().Result;
            model.ListOfClasses = FeeTransactionService.GetClasses(SessionItems.SchoolId).Result;
            decimal? fineSum = 0;
            decimal? classwiseSum = 0;
            decimal? generalwiseSum = 0;
            decimal? discountSum = 0;
            foreach (var item in model.FeeTransactionTemplateBo.FineDetailList)
            {
                fineSum = fineSum + item.Amount;
            }
            model.TotalFine = fineSum;
            foreach (var item in model.FeeTransactionTemplateBo.ClassByComponentLists)
            {
                classwiseSum = classwiseSum + item.Amount;
            }

            foreach (var item in model.FeeTransactionTemplateBo.GeneralByComponentLists)
            {
                generalwiseSum = generalwiseSum + item.Amount;
            }

            foreach (var item in model.FeeTransactionTemplateBo.DiscountDetailList)
            {
                discountSum = discountSum + item.DiscountAmount;
            }
            model.TotalDiscount = discountSum;
            model.TotalPayableAmount = model.TotalPayableAmount;
            model.TotalAmount = classwiseSum + generalwiseSum;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FeeTransactionBo model)
        {
            //model.ListOfSessions = SessionlList;

            model.ListOfDiscountFeeheads = FeeTransactionService.GetDiscountFeeHeadsList(SessionItems.SchoolId).Result;
            model.ListOfFineFeeheads = FeeTransactionService.GetFineFeeHeadsList(SessionItems.SchoolId).Result;
            model.ListOfPaymentMode = FeeTransactionService.GetPaymentModeList().Result;
            model.ListOfMonthes = FeeTransactionService.GetMonthList().Result;
            model.ListOfClasses = FeeTransactionService.GetClasses(SessionItems.SchoolId).Result;
            if (ModelState.IsValid)
            {
                model.LastUpdatedOn = DateTime.Now;
                model.LastUpdatedBy = SessionItems.UserId;
                model.SessionId = SessionItems.SessionId;
                model.TotalClassGeneralAmount = model.ClassByTotalAmount + model.GeneralByTotalAmount;
                var result = FeeTransactionService.UpdateFeeTransaction(model).Result;
                if (result)
                {
                    TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
                    return Json(new { isErr = false, errMsg = Messages.UpdateSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                TempData["ErrMessage"] = Messages.AddError;
                return Json(new { isErr = false, errMsg = Messages.AddError }, JsonRequestBehavior.AllowGet);
            }
            return View(model);

        }

        public ActionResult FeeReciept(int transactionId)
        {
            FeeTransactionBo model = FeeTransactionService.GetReceiptData(transactionId, SessionItems.SchoolId).Result;
            // decimal? sum = 0;
            if (model == null) model = new FeeTransactionBo { FeeTransactionTemplateBo = new FeeTransactionTemplateBo { ComponentList = new System.Collections.Generic.List<ComponentList>() } };
            decimal? fine = 0;
            decimal? discount = 0;
            foreach (var item in model.FeeTransactionTemplateBo.ComponentList)
            {
                //if (item.FeeHeadTypeId == (int)FeeHeadType.ClassBy || item.FeeHeadTypeId == (int)FeeHeadType.General)
                //{
                //    sum = sum + item.Amount;
                //}
                if (item.FeeHeadTypeId == (int)FeeHeadType.Fine)
                {
                    fine = fine + item.Amount;
                }
                if (item.FeeHeadTypeId == (int)FeeHeadType.Discount)
                {
                    discount = discount + item.Amount;
                }
            }
            model.TotalAmount = model.TotalClassGeneralAmount;
            model.TotalDiscountSum = discount;
            model.TotalFineSum = fine;
            model.GrandTotal = model.TotalPayableAmount;
            return View(model);
        }

        [HttpPost]
        public ActionResult GetComponents(FeeTransactionBo model)
        {
            if (model.FromMonthId == null || model.ToMonthId == null || model.ClassId == 0 || model.StudentId == 0)
            {
                ModelState.AddModelError("FromMonthId", Messages.SelectMonth);
                model.ListOfDiscountFeeheads = FeeTransactionService.GetDiscountFeeHeadsList(SessionItems.SchoolId).Result;
                model.ListOfFineFeeheads = FeeTransactionService.GetFineFeeHeadsList(SessionItems.SchoolId).Result;
                model.ListOfMonthes = FeeTransactionService.GetMonthList().Result;
                model.ListOfClasses = FeeTransactionService.GetClasses(SessionItems.SchoolId).Result;
                model.ListOfPaymentMode = FeeTransactionService.GetPaymentModeList().Result;
                return View("Add", model);
            }
            model.ListOfPaymentMode = FeeTransactionService.GetPaymentModeList().Result;
            model.ListOfSessions = SessionlList;
            model.ListOfDiscountFeeheads = FeeTransactionService.GetDiscountFeeHeadsList(SessionItems.SchoolId).Result;
            model.ListOfFineFeeheads = FeeTransactionService.GetFineFeeHeadsList(SessionItems.SchoolId).Result;
            model.ListOfMonthes = FeeTransactionService.GetMonthList().Result;
            model.ListOfClasses = FeeTransactionService.GetClasses(SessionItems.SchoolId).Result;
            var component = FeeTransactionService.GetComponent(SessionItems.SessionId, SessionItems.SchoolId, model.FromMonthId ?? 0, model.ToMonthId ?? 0, model.ClassId, model.StudentId).Result;
            model.FeeTransactionTemplateBo = component;
            model.PreviousBalance = FeeTransactionService.GetPreviousAmount(model.StudentId).Result;
            model.TotalAmount = model.TotalAmount + model.PreviousBalance;
            return View("Add", model);

        }
        //[HttpPost]
        //public ActionResult UpdateComponents(FeeTransactionBo model)
        //{

        //    model.ListOfSessions = SessionlList;
        //    model.ListOfMonthes = FeeTransactionService.GetMonthList().Result;
        //    model.ListOfClasses = FeeTransactionService.GetClasses(SessionItems.SessionId).Result;
        //    var component = FeeTransactionService.UpdateComponent(model.SessionId, SessionItems.SchoolId, model.FromMonthId, model.ToMonthId, model.ClassId).Result;
        //    model.FeeTransactionTemplateBo = component;
        //    return View("Edit", model);
        //}

        [AjaxOnly]
        public JsonResult GetComponentDetail(int fromMonthId, int toMonthId, int classId, int studentId)
        {
            var component = FeeTransactionService.GetComponent(SessionItems.SessionId, SessionItems.SchoolId, fromMonthId, toMonthId, classId, studentId).Result;
            return Json(component);
        }

        [AjaxOnly]
        public JsonResult GetSectionList(string classId)
        {
            var sectionList = (from a in FeeTransactionService.GetSections(int.Parse(classId)).Result
                               select new SelectListItem()
                               {
                                   Text = a.Name,
                                   Value = a.Id.ToString()
                               }).ToList();

            return Json(sectionList);
        }

        [AjaxOnly]
        public JsonResult GetStudentsListBySectionId(string sectionId)
        {
            var studentsList = (from s in FeeTransactionService.GetStudentsBySectionId(int.Parse(sectionId), SessionItems.SessionId).Result
                                select new SelectListItem()
                                {
                                    Text = s.Name,
                                    Value = s.Id.ToString()
                                }).ToList();
            return Json(studentsList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateStatus(int id, int statusId)
        {
            var result = FeeTransactionService.UpdateStatus(id, statusId, SessionItems.UserId).Result;
            if (result)
                TempData["SuccessMessage"] = Messages.UpdateSuccessfully;
            else
                TempData["ErrMessage"] = Messages.UpdateError;
            return Json(result);
        }


    }
}
