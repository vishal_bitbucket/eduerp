﻿using System.Web;
using System.Web.Mvc;
using EduErp.Mvc.Utilities;
using EduErp.Mvc.Utilities.Filters;

namespace EduErp.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new NormalAuthenticationFilter());
            //filters.Add(new HandleErrorAttribute());
            filters.Add(new AdminAuthenticationFilter());
        }
    }
}