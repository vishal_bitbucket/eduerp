﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;

namespace EduErp.Web.Utilities
{
    public class ImageFunctions
    {
        /// <summary>
        /// Crops and resizes the image.
        /// </summary>
        /// <param name="img">The image to be processed</param>
        /// <param name="targetWidth">Width of the target</param>
        /// <param name="targetHeight">Height of the target</param>
        /// <param name="x1">The position x1</param>
        /// <param name="y1">The position y1</param>
        /// <param name="x2">The position x2</param>
        /// <param name="y2">The position y2</param>
        /// <param name="imageFormat">The image format</param>
        /// <returns>A cropped and resized image</returns>
        public static Image CropAndResizeImage(Image img, int targetWidth, int targetHeight, int x1, int y1, int x2, int y2, ImageFormat imageFormat)
        {
            var bmp = new Bitmap(targetWidth, targetHeight);
            var g = Graphics.FromImage(bmp);

            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            g.CompositingQuality = CompositingQuality.HighQuality;

            var width = x2 - x1;
            var height = y2 - y1;

            g.DrawImage(img, new Rectangle(0, 0, targetWidth, targetHeight), x1, y1, width, height, GraphicsUnit.Pixel);

            var memStream = new MemoryStream();
            bmp.Save(memStream, imageFormat);
            return Image.FromStream(memStream);
        }

        /// <summary>
        /// Resizes the image.
        /// </summary>
        /// <param name="img">The image to be resized</param>
        /// <param name="targetWidth">Width of the target</param>
        /// <param name="targetHeight">Height of the target</param>
        /// <param name="imageFormat">The image format</param>
        /// <returns>A resized image</returns>
        public static Image ResizeImage(Image img, int targetWidth, int targetHeight, System.Drawing.Imaging.ImageFormat imageFormat)
        {
            return CropAndResizeImage(img, targetWidth, targetHeight, 0, 0, img.Width, img.Height, imageFormat);
        }

        /// <summary>
        /// Crops the image.
        /// </summary>
        /// <param name="img">The image</param>
        /// <param name="x1">The position x1.</param>
        /// <param name="y1">The position y1.</param>
        /// <param name="x2">The position x2.</param>
        /// <param name="y2">The position y2.</param>
        /// <param name="imageFormat">The image format.</param>
        /// <returns>A cropped image.</returns>
        public static Image CropImage(Image img, int x1, int y1, int x2, int y2, System.Drawing.Imaging.ImageFormat imageFormat)
        {
            return CropAndResizeImage(img, x2 - x1, y2 - y1, x1, y1, x2, y2, imageFormat);
        }

        public static byte[] ImageToByteArray(Image imageIn, ImageFormat format)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, format);
                return ms.ToArray();
            }
        }

        public static Image Base64StringToImage(string base64String)
        {
            base64String = ImagePrefixes.Aggregate(base64String, (current, prefix) => current.Replace(prefix.Value, string.Empty));

            var bytes = Convert.FromBase64String(base64String);
            return new Bitmap(new MemoryStream(bytes));
        }

        public static Image ByteArrayToImage(byte[] byteArrayIn)
        {
            Image returnImage;
            using (var ms = new MemoryStream(byteArrayIn))
            {
                returnImage = Image.FromStream(ms);
            }
            return returnImage;
        }

        private static Dictionary<string, string> ImagePrefixes
        {
            get
            {
                var imageDataPrefixes = new Dictionary<string, string>
                                            {
                                                {"png", string.Format("data:image/{0};base64,", "png")},
                                                {"jpg", string.Format("data:image/{0};base64,", "jpg")},
                                                {"jpeg", string.Format("data:image/{0};base64,", "jpeg")},
                                                {"bmp", string.Format("data:image/{0};base64,", "bmp")},
                                                {"gif", string.Format("data:image/{0};base64,", "gif")},
                                            };
                return imageDataPrefixes;
            }
        }
    }
}