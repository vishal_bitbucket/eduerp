﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;
using EduErp.BLL;
using Amazon;
using Amazon.S3.Model;
using System.Web;
using Amazon.S3;

namespace EduErp.Web.Utilities
{
    public static class CommonUtil
    {
        public static List<SelectListItem> GetYears(string text)
        {
            var currentYear = DateTime.UtcNow.Year;
            var year = new List<SelectListItem> { new SelectListItem() { Selected = true, Text = text, Value = "" } };

            for (var i = currentYear; i >= currentYear - 100; i--)
            {
                year.Add(new SelectListItem() { Text = i.ToString(), Value = i.ToString() });

            }
            return year;
        }

        public static List<SelectListItem> GetMonths()
        {
            var monthNames = CultureInfo.CurrentCulture.DateTimeFormat.MonthNames;
            var months = new List<SelectListItem> { new SelectListItem() { Selected = true, Text = "Month", Value = "" } };
            for (int i = 0; i < monthNames.Length - 1; i++)
            {
                months.Add(new SelectListItem { Text = monthNames[i], Value = i.ToString() });
            }
            return months;
        }        

        public static long[] StringToLongArray(string id)
        {
            if (string.IsNullOrEmpty(id)) return null;
            var arr = id.Split(',');
            var result = new long[arr.Length];
            for (var i = 0; i < arr.Length; i++)
            {
                result[i] = Convert.ToInt64(arr[i]);
            }
            return result;
        }

        public static Guid[] StringToGuidArray(string id)
        {
            if (string.IsNullOrEmpty(id)) return null;
            var arr = id.Split(',');
            var result = new Guid[arr.Length];
            for (var i = 0; i < arr.Length; i++)
            {
                result[i] = Guid.Parse(arr[i]);
            }
            return result;
        }

        public static DateTime ConvertToAusTimezone(this DateTime date)
        {
            var timeZone = TimeZoneInfo.FindSystemTimeZoneById("E. Australia Standard Time");
            return TimeZoneInfo.ConvertTimeFromUtc(date, timeZone);
        }

        //public static List<SelectListItem> GetAllJobsByCompanyId()
        //{
        //    var jobs = new List<SelectListItem>();
        //    var allJobs = PublicLayer.GetPermittedAllJobs(SessionItems.UserId, SessionItems.BusinessId, SessionItems.IsCompanyAdmin);
        //    jobs.Add(new SelectListItem() { Text = "Select Job", Value = "0" });
        //    jobs.AddRange(allJobs.Select(item => new SelectListItem { Text = item.Title, Value = item.ID.ToString() }));
        //    return jobs;
        //}

        //public static IEnumerable<SelectListItem> GetActiveJobsByCompanyId()
        //{
        //    var jobs = new List<SelectListItem>();
        //    var allJobs = PublicLayer.GetPermittedActiveJobs(SessionItems.UserId, SessionItems.BusinessId, SessionItems.IsCompanyAdmin);
        //    jobs.Add(new SelectListItem { Text = "Select Job", Value = "0" });
        //    jobs.AddRange(allJobs.Select(item => new SelectListItem { Text = item.Title, Value = item.ID.ToString() }));
        //    return jobs;
        //}

        //public static void SetUserProfileImage(Guid id)
        //{
        //    var user = PublicLayer.GetUserProfilePictureById(id);
        //    if (user != null && user.S3Key != Guid.Empty) return;
        //    if (user != null) EduErp.Mvc.Utilities.SessionItems.UserImagePath = GetUrlByKey(user.S3Key.ToString());
        //}

        public static string GetUrlByKey(string key)
        {
            if (string.IsNullOrEmpty(key)) return string.Empty;
            var amazonSettings = LogService.GetAmazonSetting().Result;
            try
            {
                Amazon.Runtime.AWSCredentials credentials = new Amazon.Runtime.BasicAWSCredentials(amazonSettings.AccessKey, amazonSettings.SecretAccessKey);
                var config = new AmazonS3Config
                {
                    ServiceURL = "s3.amazonaws.com",
                    SignatureVersion = "v4",
                    RegionEndpoint = RegionEndpoint.GetBySystemName("ap-south-1")
                };
                AWSConfigs.S3UseSignatureVersion4 = true;
                IAmazonS3 client;
                using (client = new AmazonS3Client(credentials, config))
                {
                    GetPreSignedUrlRequest request;
                    if (HttpContext.Current.Request.IsSecureConnection)
                    {
                        request = new GetPreSignedUrlRequest()
                        {
                            BucketName = amazonSettings.BucketName,
                            Key = key,
                            Protocol = Protocol.HTTPS,
                            Expires = DateTime.Now.AddDays(1)
                        };

                    }
                    else
                    {
                        request = new GetPreSignedUrlRequest()
                        {
                            BucketName = amazonSettings.BucketName,
                            Key = key,
                            Protocol = Protocol.HTTP,
                            Expires = DateTime.Now.AddDays(1)
                        };
                    }
                    var url = client.GetPreSignedURL(request);
                    return url;
                }
            }
            catch (Exception e)
            {

            }
            return string.Empty;
        }

    }
}