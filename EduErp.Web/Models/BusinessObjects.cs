﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using EduErp.BO;

namespace EduErp.Web.Models
{
    public class LoginVM
    {
        [Required]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }

    public class ChangePasswordVM
    {
        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Old Password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The Password must be at least {2} characters long.", MinimumLength = 8)]
        [RegularExpression(@"(^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)[A-Za-z\d$@$!%*#?&\[\]\{\}\(\)]{8,}$)", ErrorMessage = "Password must contain at least 1 capital letter, 1 small letter, 1 symbol, 1 number and minimun 8 characters.")]
        [DataType(DataType.Password)]
        [DisplayName("New Password")]
        public string Password { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The Confirm Password must be at least {2} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [DisplayName("Confirm Password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Passwords do not match")]
        public string ConfirmPassword { get; set; }

        
        public string Email { get; set; }
        public long Id { get; set; }
        public Guid PasswordSalt { get; set; }
        public bool IsPasswordChanged { get; set; }
        
    }

    public class ForgotPasswordVM
    {
        [Required(ErrorMessage = "Email address is required")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Email Address is not valid")]
        [DisplayName("Email Address")]
        public string Email { get; set; }
        public int VerificationTypeId { get; set; }
        [StringLength(10, ErrorMessage = "Mobile Number must be  {2} digit long.", MinimumLength = 10)]
        public string MobileNumber { get; set; }
        [Required(ErrorMessage = "Enter OTP")]
        //[StringLength(6, ErrorMessage = "OTP must be  {2} digit long.", MinimumLength = 6)]
            [Range(100000,999999,ErrorMessage = "Otp must be 6 digits")]
        public string Otp { get; set; }

    }

    public class RetrievePasswordVM
    {
        public string UserId { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The Password must be at least {2} characters long.", MinimumLength = 8)]
        [RegularExpression(@"(^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)[A-Za-z\d$@$!%*#?&\[\]\{\}\(\)]{8,}$)", ErrorMessage = "Password must contain at least 1 capital letter, 1 small letter, 1 symbol, 1 number and minimun 8 characters.")]
        [DataType(DataType.Password)]
        [DisplayName("New Password")]
        public string Password { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The Confirm Password must be at least {2} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
       [DisplayName("Confirm Password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Passwords do not match")]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordMailBO
    {
        public string RecipientDisplayName { get; set; }

        public string RecipientEmail { get; set; }

        public string ResetPasswordUrl { get; set; }

        public string ApplicationUrl { get; set; }
    }

    public class SessionVm : SessionMaster
    {
        public List<SelectListItem> ListOfSchools { get; set; }
        public string FromDateStr { get; set; }
        public string ToDateStr { get; set; }
    }

    //public class HolidayVm : HolidayMaster
    //{
    //     public List<BO.Common> SessionList { get; set; }
    //    public string FromDateStr { get; set; }
    //    public string ToDateStr { get; set; }
    //}

    public class SearchAttendancVM
    {
        public int SchoolId { get; set; }
        public int SessionId { get; set; }
        public List<BO.Common> SessionList { get; set; }
        public int ClassId { get; set; }
        public List<BO.Common> ClassList { get; set; }
        public int SectionId { get; set; }
        public List<BO.Common> SectionList { get; set; }
        //public DateTime Date { get; set; }
        //public string DateStr { get; set; }
        public int? StudentId { get; set; }
        public bool SearchSpecificDateRange { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public AttendanceVm AttendanceVM { get; set; }
        public List<StudentBo> StudentListItems { get; set; }
        public List<BO.Common> DayStatusList { get; set; }
        public int StatusId { get; set; }
    }

    public class EditAttendanceVm : AttendanceMaster
    {
        public List<BO.Common> DayStatusList { get; set; }
        public string DateStr { get; set; }
    }

    public class EditCertificateVm : CertificateTemplateSchoolMapping
    {
        public List<BO.Common> TemplateTypeList { get; set; }
        public List<BO.Common> TemplateNameList { get; set; }
        public int CertificateTemplateTypeId { get; set; }
       
    }

    public class ProfileImageSelector
    {
        public int UserId { get; set; }
        public string ImageData { get; set; }
        public int X1 { get; set; }
        public int Y1 { get; set; }
        public int X2 { get; set; }
        public int Y2 { get; set; }
        public string ImagePath { get; set; }
        public Guid S3Key { get; set; }
    }
}