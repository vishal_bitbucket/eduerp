﻿using System;
using System.Collections.Specialized;
using System.Web;
using System.Web.Mvc;

namespace EduErp.Mvc.Utilities
{
    public static class AbsoluteUrl
    {
        public static string AbsoluteAction(this UrlHelper url, string actionName, string controllerName, object routeValues = null)
        {
            var publicFacingUrl = GetPublicFacingUrl(url.RequestContext.HttpContext.Request, url.RequestContext.HttpContext.Request.ServerVariables);
            var relAction = url.Action(actionName, controllerName, routeValues);
            //this will always have a / in front of it.
            var newPort = publicFacingUrl.Port == 80 || publicFacingUrl.Port == 443 ? "" : ":" + publicFacingUrl.Port;
            return publicFacingUrl.Scheme + Uri.SchemeDelimiter + publicFacingUrl.Host + newPort + relAction;
        }

        internal static Uri GetPublicFacingUrl(HttpRequestBase request, NameValueCollection serverVariables)
        {
            if (serverVariables["HTTP_HOST"] != null)
            {
                //ErrorUtilities.VerifySupported(request.Url.Scheme == Uri.UriSchemeHttps || request.Url.Scheme == Uri.UriSchemeHttp, "Only HTTP and HTTPS are supported protocols.");
                var scheme = serverVariables["HTTP_X_FORWARDED_PROTO"] ?? request.Url.Scheme;
                var hostAndPort = new Uri(scheme + Uri.SchemeDelimiter + serverVariables["HTTP_HOST"]);
                var publicRequestUri = new UriBuilder(request.Url);
                publicRequestUri.Scheme = scheme;
                publicRequestUri.Host = hostAndPort.Host;
                publicRequestUri.Port = hostAndPort.Port; // CC missing Uri.Port contract that's on UriBuilder.Port
                return publicRequestUri.Uri;
            }
            return new Uri(request.Url, request.RawUrl);
        }
             
    }
}
