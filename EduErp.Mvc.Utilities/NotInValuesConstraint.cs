﻿using System;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace EduErp.Mvc.Utilities
{
    public class NotInValuesConstraint : IRouteConstraint
    {
        public NotInValuesConstraint(params string[] values)
        {
            _values = values;
        }

        private string[] _values;

        public bool Match(HttpContextBase httpContext,
                          Route route,
                          string parameterName,
                          RouteValueDictionary values,
                          RouteDirection routeDirection)
        {
            string value = values[parameterName].ToString();
            return !_values.Contains(value, StringComparer.CurrentCultureIgnoreCase);
        }
    }
}