﻿using System;
using System.Web.Mvc;

namespace EduErp.Mvc.Utilities
{
    public class AuthorizeHelper : AuthorizeAttribute
    {
        /// <summary>
        /// Check to see if this user is authenticated and has a valid session object
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        protected override bool AuthorizeCore(System.Web.HttpContextBase httpContext)
        {
            if (httpContext == null) 
                throw new ArgumentNullException("httpContext");

            //Make sure the user is authenticated
            return httpContext.User.Identity.IsAuthenticated;

            //Check additional info if required
        }
    }
}