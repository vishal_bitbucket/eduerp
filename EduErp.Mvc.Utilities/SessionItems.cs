﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace EduErp.Mvc.Utilities
{
    public class SessionItems
    {
        public static int UserId
        {
            get { return GetAspNetSessionItem(() => UserId); }
            set { SetAspNetSessionItem(() => UserId, value); }
        }

        public static int SessionId
        {
            get { return GetAspNetSessionItem(() => SessionId); }
            set {SetAspNetSessionItem(() =>SessionId,value);}
        }

        public static string EmailAddress
        {
            get { return GetAspNetSessionItem(() => EmailAddress); }
            set { SetAspNetSessionItem(() => EmailAddress, value); }
        }

        public static string SessionName
        {
            get { return GetAspNetSessionItem(() => SessionName); }
            set { SetAspNetSessionItem(() => SessionName, value); }
        }
        public static string DisplayName
        {
            get { return GetAspNetSessionItem(() => DisplayName); }
            set { SetAspNetSessionItem(() => DisplayName, value); }
        }

        public static bool IsUserAuthenticated
        {
            get
            {
                return HttpContext.Current.User.Identity.IsAuthenticated && UserId != 0;
            }
        }


        public static bool IsSuperAdmin
        {
            get
            {
                return SchoolId == 0;
            }
        }

        //public static Linked.Role UserRole
        //{
        //    get { return GetAspNetSessionItem(() => UserRole); }
        //    set { SetAspNetSessionItem(() => UserRole, value); }
        //}



        public static int SchoolId
        {
            get { return GetAspNetSessionItem(() => SchoolId); }
            set { SetAspNetSessionItem(() => SchoolId, value); }
        }

        public static int statusId
        {
            get { return GetAspNetSessionItem(() => statusId); }
            set { SetAspNetSessionItem(() => statusId, value); }
        }
        public static string SchoolName
        {
            get { return GetAspNetSessionItem(() => SchoolName); }
            set { SetAspNetSessionItem(() => SchoolName, value); }
        }

        public static int InstituteTypeId
        {
            get { return GetAspNetSessionItem(() => InstituteTypeId); }
            set { SetAspNetSessionItem(() => InstituteTypeId, value); }
        }
        

        public static bool IsSessionExpired
        {
            get { return GetAspNetSessionItem(() => IsSessionExpired); }
            set { SetAspNetSessionItem(() => IsSessionExpired, value); }
        }



        public static string UserImagePath
        {
            get { return GetAspNetSessionItem(() => UserImagePath); }
            set { SetAspNetSessionItem(() => UserImagePath, value); }
        }

        public static List<BO.MenuMasterBo> MenuItems
        {
            get { return GetAspNetSessionItem(() => MenuItems); }
            set { SetAspNetSessionItem(() => MenuItems, value); }
        }

        public static List<BO.UserRight> UserRights
        {
            get { return GetAspNetSessionItem(() => UserRights); }
            set { SetAspNetSessionItem(() => UserRights, value); }
        }

        public static List<BO.Common> SessionLists
        {
            get { return GetAspNetSessionItem(() => SessionLists); }
            set { SetAspNetSessionItem(() => SessionLists, value); }
        }

        private static T GetAspNetSessionItem<T>(Expression<Func<T>> expression)
        {
            var memberExpression = expression.Body as MemberExpression;
            var item = HttpContext.Current.Session[memberExpression.Member.Name];
            return item is T ? (T)item : default(T);
        }

        private static void SetAspNetSessionItem<T>(Expression<Func<T>> expression, object value)
        {
            var memberExpression = expression.Body as MemberExpression;
            HttpContext.Current.Session[memberExpression.Member.Name] = value;
        }

        //public static string SampleItem
        //{
        //    get { return GetAspNetSessionItem(() => SampleItem); }
        //    set { SetAspNetSessionItem(() => SampleItem, value); }
        //}
    }
}