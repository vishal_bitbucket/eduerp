﻿namespace EduErp.Mvc.Utilities
{
    public class Level
    {
        public static string GetlabelName(int lbl)
        {
            string res = null;
            if (SessionItems.InstituteTypeId == 1 || SessionItems.InstituteTypeId == 0)//School
            {
                switch (lbl)
                {
                    case 1:
                        res = "Class";
                        break;
                    
                     case 2:
                        res = "Section";
                       break;
                    case 3:
                        res = "School";
                        break;
                    case 6:
                        res = "Users";
                        break;
                    case 7:
                        res = "Messages";
                        break;
                   
                }

            }

            if (SessionItems.InstituteTypeId == 2 || SessionItems.InstituteTypeId == 0)//Coaching
            {
                switch (lbl)
                {
                    case 1:
                        res = "Courses";
                        break;
                    case 2:
                        res = "Batches";
                        break;
                    case 3:
                        res = "Coaching";
                        break;
                    case 4:
                        res = "Users";
                        break;
                    case 5:
                        res = "Messages";
                        break;
                }

            }

            return res;
        }
    }
}
