﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EduErp.Mvc.Utilities
{
   public class RandomNumber
    {
       public static int GenerateRandomNumber()
       {
           var number = new Random();
           var generatednumber = number.Next(100000, 999999);
           return generatednumber;
       }
    }
}
