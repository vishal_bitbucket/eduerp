﻿namespace EduErp.Mvc.Utilities
{
    public class PageTitle
    {
        public static string Create(string postFix)
        {
            return string.Format("EduErp - {0}", postFix);
        }
    }
}
