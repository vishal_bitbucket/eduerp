﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using EduErp.Mvc.Utilities;

// ReSharper disable CheckNamespace

namespace EduErp.Mvc.Utilities.Filters
{
    public sealed class NormalAuthenticationFilter : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var attributeType = typeof(AuthenticationRequiredAttribute);

            var action = filterContext.ActionDescriptor;
            var controller = action.ControllerDescriptor;
            var authRequired = action.IsDefined(attributeType, true) || controller.IsDefined(attributeType, true);

            if (authRequired)
            {
                if (!SessionItems.IsUserAuthenticated)
                {
                    filterContext.Authorize();
                }
            }
        }
    }

    public class NoCacheAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            var response = filterContext.HttpContext.Response;
            response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            response.Cache.SetValidUntilExpires(false);
            response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            response.Cache.SetCacheability(HttpCacheability.NoCache);
            response.Cache.SetNoStore();
        }
    }

    public sealed class AdminAuthenticationFilter : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var attributeType = typeof(AdminRequiredAttribute);

            var action = filterContext.ActionDescriptor;
            var controller = action.ControllerDescriptor;
            var authRequired = action.IsDefined(attributeType, true) || controller.IsDefined(attributeType, true);

            if (authRequired)
            {
                if (!SessionItems.IsUserAuthenticated || !SessionItems.IsSuperAdmin)
                {
                    filterContext.Authorize();
                }
            }
        }
    }

    //TODO: Add the new filter in filter.config file of the MVC project

    internal static class AuthorizationHelper
    {
        public static void Authorize(this AuthorizationContext filterContext, bool encodeReturnUrl = true)
        {
            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {
                filterContext.Result = new JsonResult
                {
                    Data = new
                    {
                        IsSessionExpired = true,
                        Message = "session has timed out"
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
                return;
            }

            var loginUrl = FormsAuthentication.LoginUrl;
            if (filterContext.HttpContext.Request != null)
            {
                var uri = filterContext.HttpContext.Request.Url;
                if (uri != null)
                {
                    var formsAuthReturnUrlVar = ConfigurationManager.AppSettings["aspnet:FormsAuthReturnUrlVar"];
                    if (string.IsNullOrEmpty(formsAuthReturnUrlVar)) formsAuthReturnUrlVar = "ReturnUrl";

                    var requestUrl = uri.AbsolutePath; //.AbsoluteUri;
                    var encodedRequestUrl = new UrlHelper(filterContext.RequestContext).Encode(requestUrl);

                    var returnUrl = string.Format("?{0}={1}", formsAuthReturnUrlVar, requestUrl);
                    var encodedReturnUrl = string.Format("?{0}={1}", formsAuthReturnUrlVar, encodedRequestUrl);

                    loginUrl += encodeReturnUrl ? encodedReturnUrl : returnUrl;
                }
            }

            filterContext.Result = new RedirectResult(loginUrl);
        }

        public static void CompanyAuthorize(this AuthorizationContext filterContext, bool encodeReturnUrl = true)
        {
            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {
                filterContext.Result = new JsonResult
                {
                    Data = new
                    {
                        IsSessionExpired = true,
                        Message = "session has timed out"
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
                return;
            }

            var requestUrl = string.Empty;
            if (filterContext.HttpContext.Request != null)
            {
                var uri = filterContext.HttpContext.Request.Url;
                if (uri != null)
                {
                    var setupReturnUrlVar = ConfigurationManager.AppSettings["CompanyReturnUrlVar"];
                    if (string.IsNullOrEmpty(setupReturnUrlVar)) setupReturnUrlVar = "/company/";

                    requestUrl = GetSiteRoot() + setupReturnUrlVar;
                }
            }

            filterContext.Result = new RedirectResult(requestUrl);
        }

        public static string GetSiteRoot()
        {
            var port = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
            if (port == null || port == "80" || port == "443")
                port = "";
            else
                port = ":" + port;

            var protocol = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT_SECURE"];
            if (protocol == null || protocol == "0")
                protocol = "http://";
            else
                protocol = "https://";

            var rootUrl = protocol + System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + port + System.Web.HttpContext.Current.Request.ApplicationPath;

            if (rootUrl.EndsWith("/"))
            {
                rootUrl = rootUrl.Substring(0, rootUrl.Length - 1);
            }

            return rootUrl;
        }

    }

    public sealed class AjaxOnlyFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var attributeType = typeof(AjaxOnlyAttribute);

            var action = filterContext.ActionDescriptor;
            var controller = action.ControllerDescriptor;
            var authRequired = action.IsDefined(attributeType, true) || controller.IsDefined(attributeType, true);

            if (authRequired)
            {
                if (!filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    var loginUrl = FormsAuthentication.LoginUrl;
                    if (filterContext.HttpContext.Request != null)
                    {
                        var uri = filterContext.HttpContext.Request.Url;
                        if (uri != null)
                        {
                            var formsAuthReturnUrlVar = ConfigurationManager.AppSettings["aspnet:FormsAuthReturnUrlVar"];
                            if (string.IsNullOrEmpty(formsAuthReturnUrlVar)) formsAuthReturnUrlVar = "ReturnUrl";

                            var requestUrl = uri.AbsolutePath; //.AbsoluteUri;
                            var encodedRequestUrl = new UrlHelper(filterContext.RequestContext).Encode(requestUrl);

                            // var returnUrl = string.Format("?{0}={1}", formsAuthReturnUrlVar, requestUrl);
                            var encodedReturnUrl = string.Format("?{0}={1}", formsAuthReturnUrlVar, encodedRequestUrl);

                            loginUrl += encodedReturnUrl;
                        }
                    }
                    filterContext.Result = new RedirectResult(loginUrl);
                }
                else
                {
                    base.OnActionExecuting(filterContext);
                }
            }
        }
    }

    public sealed class BusinessAuthenticationFilter : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var attributeType = typeof(BusinessRequiredAttribute);

            var action = filterContext.ActionDescriptor;
            var controller = action.ControllerDescriptor;
            var authRequired = action.IsDefined(attributeType, true) || controller.IsDefined(attributeType, true);

            if (authRequired)
            {
                if (!SessionItems.IsUserAuthenticated || (SessionItems.SchoolId == 0))
                {
                    //SessionItems.IsBusinessId = true;
                    filterContext.CompanyAuthorize();
                }
            }
        }
    }
}