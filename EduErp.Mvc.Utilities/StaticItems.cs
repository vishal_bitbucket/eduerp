﻿using System;

namespace EduErp.Mvc.Utilities
{
    public static class StaticItems
    {
        public static DateTime EncashmentApplicationStartDate { get; set; }

        public static DateTime EncashmentApplicationEndDate { get; set; }
    }
}
