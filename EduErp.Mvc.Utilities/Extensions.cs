﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace EduErp.Mvc.Utilities
{
    public static class Extensions
    {
        public static MvcHtmlString MenuLink(this HtmlHelper htmlHelper, string linkText, string action, string controller, Dictionary<string, List<string>> caPairs, string activeClass)
        {
            var currentAction = htmlHelper.ViewContext.RouteData.GetRequiredString("action");
            var currentController = htmlHelper.ViewContext.RouteData.GetRequiredString("controller");

            if (!string.IsNullOrEmpty(currentAction)) currentAction = currentAction.ToLower();
            if (!string.IsNullOrEmpty(currentController)) currentController = currentController.ToLower();
            action = action.ToLower();
            controller = controller.ToLower();

            if (caPairs != null)
                foreach (var controllerActionsPair in caPairs)
                {
                    var controllerName = controllerActionsPair.Key.ToLower();
                    if (currentController != controllerName) continue;

                    foreach (var actionName in controllerActionsPair.Value)
                    {
                        if (currentAction != actionName.ToLower()) continue;
                        return htmlHelper.ActionLink(linkText, action, controller, null, new { @class = activeClass });
                    }
                }


            if (controller == currentController && action == currentAction)
            {
                return htmlHelper.ActionLink(linkText, action, controller, null, new { @class = activeClass });
            }

            return htmlHelper.ActionLink(linkText, action, controller);
        }


        private static Type GetNonNullableModelType(ModelMetadata modelMetadata)
        {
            Type realModelType = modelMetadata.ModelType;

            Type underlyingType = Nullable.GetUnderlyingType(realModelType);
            if (underlyingType != null)
            {
                realModelType = underlyingType;
            }
            return realModelType;
        }

        private static readonly SelectListItem[] SingleEmptyItem = new[] { new SelectListItem { Text = "", Value = "" } };

        public static string EnumDescription<TEnum>(this TEnum value)
        {
            var fi = value.GetType().GetField(value.ToString());
            var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return (attributes.Length > 0) ? attributes[0].Description : value.ToString();
        }

        public static MvcHtmlString EnumDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression)
        {
            return EnumDropDownListFor(htmlHelper, expression, null);
        }

        public static MvcHtmlString EnumDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, object htmlAttributes)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            Type enumType = GetNonNullableModelType(metadata);
            IEnumerable<TEnum> values = Enum.GetValues(enumType).Cast<TEnum>();

            IEnumerable<SelectListItem> items = from value in values
                                                select new SelectListItem
                                                {
                                                    Text = EnumDescription(value),
                                                    Value = value.ToString(),
                                                    Selected = value.Equals(metadata.Model)
                                                };

            // If the enum is nullable, add an 'empty' item to the collection
            if (metadata.IsNullableValueType)
                items = SingleEmptyItem.Concat(items);

            return htmlHelper.DropDownListFor(expression, items, htmlAttributes);
        }

        //public static List<SelectListItem> SelectListItem(this List<dynamic> list)
        //{
        //    return new list.Select(a => new SelectListItem()
        //        {
        //            Text = a.Name,
        //            Value = a.ID.ToString()
        //        }).ToList();
        //}

        //public static bool HasRole(this List<BO.Role> roles, int roleId)
        //{
        //    return roles.FirstOrDefault(a => a.ID ==roleId) != null;
        //}

        //public static bool HasRole(this List<EMS.BO.GlobalRole> roles, EMS.BO.Enumerators.GlobalRoles role)
        //{
        //    return roles.FirstOrDefault(a => a.Name == role.ToString()) != null;
        //}
    }

    public static class DateTimeExtensions
    {
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }

            return dt.AddDays(-1 * diff).Date;
        }

        public static string ToFormattedDate(this DateTime dt)
        {
            return dt.ToString("dd-MMM-yyyy");
        }
    }
}