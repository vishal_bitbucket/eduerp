﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace EduErp.Mvc.Utilities
{
    public class ReversibleEncryptionHelper
    {
        public static string Encrypt(string stringToEncrypt)
        {
            const string cryptoKey = "uGEU#-9_Q~ExK;u.rqdMN,Z^";
            var iv = new byte[16] { 90, 15, 10, 65, 30, 201, 197, 58, 31, 45, 65, 87, 91, 84, 19, 51 };
            var inputBuffer = Encoding.UTF8.GetBytes(stringToEncrypt);

            var tDesProvider = new AesManaged { IV = iv, Key = Encoding.UTF8.GetBytes(cryptoKey) };

            return Convert.ToBase64String(tDesProvider.CreateEncryptor().TransformFinalBlock(inputBuffer, 0, inputBuffer.Length));
        }

        public static string Decrypt(string stringToDecrypt)
        {
            string result;
            const string cryptoKey = "uGEU#-9_Q~ExK;u.rqdMN,Z^";
            var iv = new byte[16] { 90, 15, 10, 65, 30, 201, 197, 58, 31, 45, 65, 87, 91, 84, 19, 51 };
            var inputBuffer = Convert.FromBase64String(stringToDecrypt);

            using (var aesAlg = new AesManaged())
            {
                aesAlg.Key = Encoding.UTF8.GetBytes(cryptoKey);
                aesAlg.IV = iv;
                var decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                using (var msDecrypt = new MemoryStream(inputBuffer))
                using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                using (var srDecrypt = new StreamReader(csDecrypt))
                    result = srDecrypt.ReadToEnd();
            }


            return result;
        }

    }
}