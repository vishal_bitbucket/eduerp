﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace EduErp.Mvc.Utilities
{
    public static class MenuCategory
    {
        public static string GetListItemClass(this HtmlHelper htmlHelper, Dictionary<string, List<string>> caPairs, string activeClass)
        {
            var currentAction = htmlHelper.ViewContext.RouteData.GetRequiredString("action");
            var currentController = htmlHelper.ViewContext.RouteData.GetRequiredString("controller");

            if (!string.IsNullOrEmpty(currentAction)) currentAction = currentAction.ToLower();
            if (!string.IsNullOrEmpty(currentController)) currentController = currentController.ToLower();

            if (caPairs != null)
                foreach (var controllerActionsPair in caPairs)
                {
                    var controllerName = controllerActionsPair.Key.ToLower();
                    if (currentController != controllerName) continue;

                    foreach (var actionName in controllerActionsPair.Value)
                    {
                        if (currentAction != actionName.ToLower()) continue;
                        return activeClass;
                    }
                }

            return string.Empty;
        }

        public static string GetActiveSubMenu(this HtmlHelper htmlHelper, string activeClass, dynamic id)
        {
            var currentAction = htmlHelper.ViewContext.RouteData.GetRequiredString("action");
            var currentController = htmlHelper.ViewContext.RouteData.GetRequiredString("controller");
            if (!string.IsNullOrEmpty(currentAction)) currentAction = currentAction.ToLower();
            if (!string.IsNullOrEmpty(currentController)) currentController = currentController.ToLower();
            object param;
            htmlHelper.ViewContext.RouteData.Values.TryGetValue("id", out param);
            if (param == null) return string.Empty;
            if (string.IsNullOrEmpty(currentAction) || string.IsNullOrEmpty(currentController)) return string.Empty;
            switch (currentController)
            {
                case "business":
                    {
                        Guid currentId;
                        Guid.TryParse(htmlHelper.ViewContext.RouteData.GetRequiredString("id"), out currentId);
                        if (currentId != Guid.Empty && currentId.ToString() == id.ToString() && (currentAction == "businessmenulist")) return activeClass;
                    }
                    break;
            }
            return string.Empty;
        }
    }
}