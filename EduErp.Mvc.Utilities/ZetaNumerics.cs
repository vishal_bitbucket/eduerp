﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EduErp.Mvc.Utilities
{
    public static class ZetaNumerics
    {
        private const int ZetaBits = 5;
        private const char OctateSeperator = '-';

        public static string ToZetaString(this long id)
        {
            var encodedText = new StringBuilder();
            var bytes = Encoding.UTF8.GetBytes(id.ToString().ToCharArray());

            foreach (var b in bytes)
            {
                encodedText.Append(ToZetaDecimal(b.ToString())).Append(OctateSeperator);
            }

            return encodedText.ToString().TrimEnd(OctateSeperator);
        }

        public static long ToLong(this string zetaString)
        {
            try
            {
                var byteArray = zetaString.Split(OctateSeperator);
                var newByteArray = new byte[byteArray.Length];
                for (var i = 0; i < byteArray.Length; i++)
                {
                    var decValue = int.Parse((FromZetaDecimal(byteArray[i])).ToString());
                    newByteArray[i] = (byte)decValue;

                }
                var zetaresult = Encoding.UTF8.GetString(newByteArray);
                long result = 0;
                if (!long.TryParse(zetaresult, out result))
                    result = 0;
                return result;
            }
            catch (Exception)
            {

                return 0;
            }
        }

        private static string ToZetaDecimal(string number)
        {
            var longNumber = Convert.ToInt64(number);
            var binary = new StringBuilder(Convert.ToString(longNumber, 2));
            var length = binary.Length;
            int remainder;
            Math.DivRem(length, ZetaBits, out remainder);

            if (remainder > 0)
                for (int i = 0; i < ZetaBits - remainder; i++)
                {
                    binary = binary.Insert(0, "0");
                }

            var binaryString = binary.ToString();
            var chunks = new List<string>();

            for (var i = 0; i < binary.Length; i = i + ZetaBits)
            {
                chunks.Add(binaryString.Substring(i, ZetaBits));
            }

            var result = new StringBuilder();
            foreach (var chunk in chunks)
            {
                var character = ZetaCharacters[Convert.ToInt32(chunk, 2)];
                result.Append(character);
            }
            return result.ToString();
        }

        private static long FromZetaDecimal(string thetaDecimal)
        {
            var binaryString = new StringBuilder();
            foreach (var item in thetaDecimal)
            {
                binaryString.Append(BinaryNumbers[ZetaCharacters.IndexOf(item)]);
            }

            long dec = 0;
            for (var i = 0; i < binaryString.Length; i++)
            {
                if (binaryString[binaryString.Length - i - 1] == '0') continue;
                dec += (long)Math.Pow(2, i);
            }
            return dec;
        }

        private static List<char> ZetaCharacters
        {
            get
            {
                return new List<char>
                       {
                           '0',
                           '1',
                           '2',
                           '3',
                           '4',
                           '5',
                           '6',
                           '7',
                           '8',
                           '9',
                           'a',
                           'b',
                           'c',
                           'd',
                           'e',
                           'f',
                           'g',
                           'h',
                           'i',
                           'j',
                           'k',
                           'l',
                           'm',
                           'n',
                           'o',
                           'p',
                           'q',
                           'r',
                           's',
                           't',
                           'u',
                           'v'
                       };
            }
        }

        private static List<string> BinaryNumbers
        {
            get
            {
                return new List<string>
                       {
                           "00000",
                           "00001",
                           "00010",
                           "00011",
                           "00100",
                           "00101",
                           "00110",
                           "00111",
                           "01000",
                           "01001",
                           "01010",
                           "01011",
                           "01100",
                           "01101",
                           "01110",
                           "01111",
                           "10000",
                           "10001",
                           "10010",
                           "10011",
                           "10100",
                           "10101",
                           "10110",
                           "10111",
                           "11000",
                           "11001",
                           "11010",
                           "11011",
                           "11100",
                           "11101",
                           "11110",
                           "11111",
                       };
            }
        }
    }
}
