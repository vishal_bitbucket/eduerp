﻿using System.Web;

namespace EduErp.Mvc.Utilities
{
    public class MockHttpContextWrapper : HttpContextWrapper
    {
        public MockHttpContextWrapper(HttpContext httpContext, string method)
            : base(httpContext)
        {
            this.request = new MockHttpRequestWrapper(httpContext.Request, method);
        }

        private readonly HttpRequestBase request;
        public override HttpRequestBase Request
        {
            get { return request; }
        }

        class MockHttpRequestWrapper : HttpRequestWrapper
        {
            public MockHttpRequestWrapper(HttpRequest httpRequest, string httpMethod)
                : base(httpRequest)
            {
                this.httpMethod = httpMethod;
            }

            private readonly string httpMethod;
            public override string HttpMethod
            {
                get { return httpMethod; }
            }
        }
    }

}
