﻿using System.Collections.Generic;

namespace EduErp.Mvc.Utilities
{
    public class CAPairs : Dictionary<string, List<string>>
    {

    }

    public class ActiveTab
    {
        public static CAPairs ActiveCategoryLinks(List<CAPairs> innerActiveLinks)
        {
            var result = new CAPairs();

            foreach (var activeLink in innerActiveLinks)
            {
                foreach (var caPair in activeLink)
                {
                    if (result.ContainsKey(caPair.Key))
                    {
                        foreach (var action in caPair.Value)
                            if (!result[caPair.Key].Contains(action))
                                result[caPair.Key].Add(action);
                    }

                    else result.Add(caPair.Key, caPair.Value);
                }
            }

            return result;
        }
    }

    public static class ActiveLinks
    {
        public class DefaultTab
        {
            public static CAPairs Home
            {
                get
                {
                    return new CAPairs
                               {
                                   {"Default", new List<string> {"Index"}}
                               };
                }
            }

            public static CAPairs Report
            {
                get
                {
                    return new CAPairs
                               {
                                   {"Report", new List<string> {"Index"}}
                               };
                }
            }

            public static CAPairs Certificate
            {
                get
                {
                    return new CAPairs
                               {
                                   {"Certificate", new List<string> {"Download"}}
                               };
                }
            }
            public static CAPairs StudentExamMapping
            {
                get
                {
                    return new CAPairs
                               {
                                   {"StudentExam", new List<string> {"Index"}}
                               };
                }
            }
            public static CAPairs Exam
            {
                get
                {
                    return new CAPairs
                               {
                                   {"Exam", new List<string> {"Index"}}
                               };
                }
            }

            public static CAPairs Configuration
            {
                get
                {
                    return new CAPairs
                               {
                                   {"Default", new List<string> {"Configuration"}}
                               };
                }
            }

            public static CAPairs Item
            {
                get
                {
                    return new CAPairs
                               {
                                   {"Item", new List<string> {"Index"}}
                               };
                }
            }

            public static CAPairs Vendor
            {
                get
                {
                    return new CAPairs
                               {
                                   {"Vendor", new List<string> {"Index"}}
                               };
                }
            }

            public static CAPairs EmployeeType
            {
                get
                {
                    return new CAPairs
                               {
                                   {"EmployeeType", new List<string> {"Index"}}
                               };
                }
            }
            public static CAPairs Employee
            {
                get
                {
                    return new CAPairs
                               {
                                   {"Employee", new List<string> {"Index"}}
                               };
                }
            }

            public static CAPairs StudentUpload
            {
                get
                {
                    return new CAPairs
                               {
                                   {"StudentUpload", new List<string> {"Index"}}
                               };
                }
            }

         
            public static CAPairs Notification
            {
                get
                {
                    return new CAPairs
                               {
                                   {"Notification", new List<string> {"Index"}}
                               };
                }
            }

           
            public static CAPairs AccountFeeDetail
            {
                get
                {
                    return new CAPairs
                    {
                      {"AccountFeeDetail",new List<string>{"Index"}}
                    };
                }

            }
            public static CAPairs Plan
            {
                get
                {
                    return new CAPairs
                    {
                      {"Plan",new List<string>{"Index"}}
                    };
                }

            }

            public static CAPairs AccountDetail
            {
                get
                {
                    return new CAPairs
                    {
                      {"AccountDetail",new List<string>{"Index"}}
                    };
                }

            }

            public static CAPairs FeeTransaction
            {
                get
                {
                    return new CAPairs
                               {
                                   {"FeeTransaction", new List<string> {"Index"}}
                               };
                }
            }

          public static CAPairs Class
            {
                get
                {
                    return new CAPairs
                    {
                      {"Class", new List<string>{"Index"}}
                    };
                }

            }

            public static CAPairs School
            {
                get
                {
                    return new CAPairs
                               {
                                   {"School", new List<string> {"Index"}}
                               };
                }
            }

            public static CAPairs User
            {

                get
                {
                    return new CAPairs
                    {
                    
                    {"User", new List<string> {"Index"}}
                    };
                }

            }

            public static CAPairs Session
            {

                get
                {
                    return new CAPairs
                    {
                    
                    {"Session", new List<string> {"Index"}}
                    };
                }

            }
            public static CAPairs Section
            {

                get
                {
                    return new CAPairs
                    {
                    
                    {"Section", new List<string> {"Index"}}
                    };
                }

            }

            public static CAPairs FeeHead
            {
                get
                {
                    return new CAPairs
                    {
                        {"FeeHead", new List<string> {"Index"}}
                    };
                }
            }
            public static CAPairs Subject
            {
                get
                {
                    return new CAPairs
                     {
                       {"Subject", new List<string>{"Index"}}
                     };
                }

            }

            public static CAPairs Student
            {
                get
                {
                    return new CAPairs
                     {
                       {"Student", new List<string>{"Index"}}
                     };
                }

            }

            public static CAPairs Attendance
            {

                get
                {
                    return new CAPairs
                    {
                    
                    {"Attendance", new List<string> {"Index"}}
                    };
                }

            }

            public static CAPairs Message
            {

                get
                {
                    return new CAPairs
                    {
                    
                    {"Message", new List<string> {"Index"}}
                    };
                }

            }

            public static CAPairs Expenses
            {

                get
                {
                    return new CAPairs
                    {
                    
                    {"Expenses", new List<string> {"Index"}}
                    };
                }

            }
            public static CAPairs OnlineTest
            {
                get
                {
                    return new CAPairs
                               {
                                   {"OnlineTest", new List<string> {"Index"}}
                               };
                }
            }
            public static CAPairs Holiday
            {

                get
                {
                    return new CAPairs
                    {
                    
                    {"Holiday", new List<string> {"Index"}}
                    };
                }

            }
        }
    }
}