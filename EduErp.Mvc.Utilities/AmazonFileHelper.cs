﻿using System;
using System.IO;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;

namespace EduErp.Mvc.Utilities
{
    public class AmazonFileHelper
    {
        private static IAmazonS3 GetS3Client(string accessKey, string secretAccessKey)
        {
            Amazon.Runtime.AWSCredentials credentials = new Amazon.Runtime.BasicAWSCredentials(accessKey, secretAccessKey);
            var config = new AmazonS3Config
            {
                ServiceURL = "s3.amazonaws.com",
                SignatureVersion = "v4",
                RegionEndpoint = RegionEndpoint.GetBySystemName("ap-south-1")
            };
            AWSConfigs.S3UseSignatureVersion4 = true;
            IAmazonS3 client = new AmazonS3Client(credentials, config);
            return client;
        }

        public static bool UploadAllToS3(string localFilePath, string bucketName, string fileNameInS3, string accessKey, string secretAccessKey)
        {
            var client = AWSClientFactory.CreateAmazonS3Client(accessKey, secretAccessKey);

            // create a TransferUtility instance passing it the IAmazonS3 created in the first step
            var utility = new TransferUtility(client);
            // making a TransferUtilityUploadRequest instance
            var request = new TransferUtilityUploadRequest
            {
                BucketName = bucketName,
                Key = fileNameInS3,
                FilePath = localFilePath
            };

            //file name up in S3
            //local file name
            utility.Upload(request); //commensing the transfer
            return true; //indicate that the file was sent
        }

        public static Guid UploadFile(string folderName, string filePath, string accessKey, string secretAccessKey, string bucketName)
        {
            try
            {
                var fileTransferUtility = new TransferUtility(GetS3Client(accessKey, secretAccessKey));
                var fileName = Guid.NewGuid();
                var objKey = folderName + "/" + fileName;                

                var fileTransferUtilityRequest = new TransferUtilityUploadRequest
                {
                    BucketName = bucketName,
                    FilePath = filePath,
                    StorageClass = S3StorageClass.ReducedRedundancy,
                    Key = objKey,
                };
                fileTransferUtility.Upload(fileTransferUtilityRequest);
                return fileName;
                //return MakeUrl(objKey);
            }
            catch (AmazonS3Exception s3Exception)
            {
                return Guid.Empty;
                //Console.WriteLine(s3Exception.Message,s3Exception.InnerException);
            }
        }

        public static bool DeleteFile(string amazonFilePath, string accessKey, string secretAccessKey, string bucketName)
        {
            try
            {
                var request = new DeleteObjectRequest()
                    {
                        BucketName = bucketName,
                        Key = amazonFilePath
                    };
                var client = GetS3Client(accessKey, secretAccessKey);
                var response = client.DeleteObject(request);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static byte[] GetFile(string filepath, string accessKey, string secretAccessKey, string bucketName)
        {
            using (var s3Client = GetS3Client(accessKey, secretAccessKey))
            {
                var file = new MemoryStream();
                try
                {
                    var objKey = filepath;
                    var r = s3Client.GetObject(new GetObjectRequest()
                    {
                        BucketName = bucketName,
                        Key = objKey
                    });
                    try
                    {
                        var stream2 = new BufferedStream(r.ResponseStream);
                        var buffer = new byte[0x2000];
                        var count = 0;
                        while ((count = stream2.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            file.Write(buffer, 0, count);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                    return file.ToArray();
                }
                catch (AmazonS3Exception ex)
                {
                    return null;
                }
            }
        }

        public static string MakeUrl(string key, string accessKey, string secretAccessKey, string bucketName)
        {
            var s3Client = GetS3Client(accessKey, secretAccessKey);
            var preSignedUrl = s3Client.GetPreSignedURL(new GetPreSignedUrlRequest()
            {
                BucketName = bucketName,
                Key = key,
                //Expires = System.DateTime.Now.AddMinutes(30)
                Expires = DateTime.Now.AddDays(2)
            });
            return preSignedUrl;
        }

        public static bool UploadToS3(byte[] imageBytes, string amazonFilePath, string accessKey, string secretAccessKey, string bucketName)
        {
            try
            {
                using (var client = GetS3Client(accessKey, secretAccessKey))
                {
                    var request = new PutObjectRequest
                    {
                        BucketName = bucketName,
                        CannedACL = S3CannedACL.AuthenticatedRead,
                        Key = amazonFilePath,
                        ContentType = "image/png"
                    };
                    using (var ms = new MemoryStream(imageBytes))
                    {
                        request.InputStream = ms;
                        client.PutObject(request);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool UploadVideoToS3(Stream videoStream, string amazonFilePath, string accessKey, string secretAccessKey, string bucketName)
        {
            try
            {
                var request = new TransferUtilityUploadRequest
                    {
                        BucketName = bucketName,
                        //CannedACL = S3CannedACL.AuthenticatedRead,
                        Key = amazonFilePath,
                        ContentType = "Video/mp4",
                        InputStream = videoStream,
                        ServerSideEncryptionMethod = ServerSideEncryptionMethod.AES256,
                        CannedACL = S3CannedACL.PublicRead,
                    };
                //request.WithTimeout(100 * 60 * 60 * 1000); //100 min timeout
                //request.WithMetadata("fileName", fileName);
                //request.WithMetadata("fileDesc", fileDesc);
                request.StorageClass = S3StorageClass.ReducedRedundancy;
                //request.WithPartSize(5 * 1024 * 1024); // Upload in 5MB pieces 

                //request.UploadProgressEvent += uploadRequest_UploadPartProgressEvent;
                var fileTransferUtility = new TransferUtility(accessKey, secretAccessKey);
                fileTransferUtility.Upload(request);
                return true;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                //if (amazonS3Exception.ErrorCode != null &&(amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") ||amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                //{
                //    Log.Add(LogTypes.Debug, mediaId, "Please check the provided AWS Credentials.");
                //}
                //else
                //{
                //    Log.Add(LogTypes.Debug, mediaId, String.Format("An error occurred with the message '{0}' when writing an object", amazonS3Exception.Message));
                //}
                return false; //Failed
            }
            catch (Exception ex)
            {
                return false; //Failed
            }
        }

        // private static Dictionary<string, int> uploadTracker = new Dictionary<string, int>();

        //static void uploadRequest_UploadPartProgressEvent(object sender, UploadProgressArgs e)
        //{
        //    var req = sender as TransferUtilityUploadRequest;
        //    if (req == null) return;
        //    var fileName = req.FilePath.Split('\\').Last();
        //    if (!uploadTracker.ContainsKey(fileName))
        //        uploadTracker.Add(fileName, e.PercentDone);

        //    //When percentage done changes add logentry:
        //    if (uploadTracker[fileName] != e.PercentDone)
        //    {
        //        uploadTracker[fileName] = e.PercentDone;
        //        //Log.Add(LogTypes.Debug, 0, String.Format("WritingLargeFile progress: {1} of {2} ({3}%) for file '{0}'", fileName, e.TransferredBytes, e.TotalBytes, e.PercentDone));
        //    }
        //}

        //public static int GetAmazonUploadPercentDone(string fileName)
        //{
        //    return !uploadTracker.ContainsKey(fileName) ? 0 : uploadTracker[fileName];
        //}
    }
}
