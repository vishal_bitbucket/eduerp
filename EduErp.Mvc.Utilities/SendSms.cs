﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using EduErp.BO;
using Newtonsoft.Json;

namespace EduErp.Mvc.Utilities
{
   public class SendSms
    {
       public static string GetResponse(string sUrl)
        {
            var request = (HttpWebRequest)WebRequest.Create(sUrl);
            request.MaximumAutomaticRedirections = 4;
            request.Credentials = CredentialCache.DefaultCredentials;
            try
            {
                var response = (HttpWebResponse)request.GetResponse();
                var receiveStream = response.GetResponseStream();
                if (receiveStream != null)
                {
                    var readStream = new StreamReader(receiveStream, Encoding.UTF8);
                    var sResponse = readStream.ReadToEnd();
                    response.Close();
                    readStream.Close();
                    return sResponse;
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return string.Empty;
        }

        public static SmsResponse SendTextSms(string number, string message)
        {
            string sAPIKey = ConfigurationManager.AppSettings["ApiKey"];
            string sNumber = number;
            string sMessage = message;
            string sSenderID = ConfigurationManager.AppSettings["SenderId"];
            string sChannel = ConfigurationManager.AppSettings["Channel"];
            string sRoute = ConfigurationManager.AppSettings["Route"];
          //  string sUrl = "http://smppsmshub.in/api/mt/SendSMS?APIKEY=" + sAPIKey + "&senderid=" + sSenderID + "&channel=" + sChannel + "&DCS=0&flashsms=0&number=" + sNumber + "&text=" + sMessage + "&route=" + sRoute;
           string  sUrl = string.Format(ConfigurationManager.AppSettings["ApiUrl"], sAPIKey, sSenderID, sChannel, sNumber, sMessage, sRoute);
            string sResponse = GetResponse(sUrl);
            var result = JsonConvert.DeserializeObject<SmsResponse>(sResponse);
            return result;
        }
    }
}
