﻿namespace EduErp.Base
{
    public class ServiceResult<T>
    {
        public bool IsSessionExpired { get; set; }

        public bool IsSuccessful { get; set; }

        public string ErrorMessage { get; set; }

        public T Result { get; set; }

        public MessageType MessageType { get; set; }

        public ServiceResult()
        {
            MessageType = MessageType.None;
        }
    }

    public enum MessageType
    {
        None = 0,
        Failed = 1,
        Success = 2,
        AccountNotVerified = 3,
        AccountNotApproved = 4,
        AccountInactive = 5,
        AccessDeniedToProduct = 6,
        PasswordExpired = 7,
        AccountLocked = 8,
        CASServerError = 9,
        SessionExpired = 10,
        InvalidUser = 11,
        CompanyInactive = 12,
        ExceedNumberOfUsers = 13,
        AcountExistsWithSameEmail = 14,
        UnauthorizedUser = 15,
        NetworkNotAvailable = 16,
        DuplicateEnrollmentNumber = 17,
        DuplicateEmployeeID = 18,
        AlreadyJobExecuted = 19,
        DestinationDeviceConnectionFail = 20,
        SourceDeviceConnectionFail = 21,
        DeviceNotConnected = 22,
        DeviceLogNotFound = 23,
        DuplicateDeviceIP = 18,
        IDNotFound = 19,
        SubscriptionExpired=20
    }
}
