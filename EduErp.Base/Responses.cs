﻿namespace EduErp.Base
{
    public static class Responses
    {
        #region Service Result Definitions

        public static ServiceResult<T> SessionExpiredResult<T>()
        {
            return new ServiceResult<T>
            {
                MessageType = MessageType.SessionExpired,
                IsSessionExpired = true,
                IsSuccessful = false,
                Result = default(T),
            };
        }

        public static ServiceResult<bool> SuccessBoolResult
        {
            get
            {
                return new ServiceResult<bool>
                {
                    MessageType = MessageType.Success,
                    IsSessionExpired = false,
                    IsSuccessful = true,
                    Result = true
                };
            }
        }

        public static ServiceResult<T> SuccessDataResult<T>(T data)
        {
            return new ServiceResult<T>
            {
                MessageType = MessageType.Success,
                IsSessionExpired = false,
                IsSuccessful = true,
                Result = data
            };
        }

        public static ServiceResult<T> SuccessDataResult<T>(T data, MessageType messageType)
        {
            return new ServiceResult<T>
            {
                MessageType = messageType,
                IsSessionExpired = false,
                IsSuccessful = true,
                Result = data
            };
        }

        public static ServiceResult<T> SuccessDataResult<T>(T data, MessageType messageType, string errorMessage)
        {
            return new ServiceResult<T>
            {
                MessageType = messageType,
                ErrorMessage = errorMessage,
                IsSessionExpired = false,
                IsSuccessful = true,
                Result = data
            };
        }


        public static ServiceResult<bool> FailureBoolResult
        {
            get
            {
                return new ServiceResult<bool>
                {
                    MessageType = MessageType.Failed,
                    IsSessionExpired = false,
                    IsSuccessful = false,
                    Result = false
                };
            }
        }

        public static ServiceResult<T> FailureNullDataResult<T>()
        {
            return new ServiceResult<T>
            {
                MessageType = MessageType.Failed,
                IsSessionExpired = false,
                IsSuccessful = false,
                Result = default(T),
            };
        }

        public static ServiceResult<T> FailureDataResult<T>(T data)
        {
            return new ServiceResult<T>
            {
                MessageType = MessageType.Failed,
                IsSessionExpired = false,
                IsSuccessful = false,
                Result = data
            };
        }

        public static ServiceResult<T> FailureDataResult<T>(T data, MessageType messageType)
        {
            return new ServiceResult<T>
            {
                MessageType = messageType,
                IsSessionExpired = false,
                IsSuccessful = false,
                Result = data
            };
        }

        public static ServiceResult<T> FailureDataResult<T>(T data, string errorMessage)
        {
            return new ServiceResult<T>
            {
                MessageType = MessageType.Failed,
                ErrorMessage = errorMessage,
                IsSessionExpired = false,
                IsSuccessful = false,
                Result = data
            };
        }

        public static ServiceResult<T> FailureDataResult<T>(T data, MessageType messageType, string errorMessage)
        {
            return new ServiceResult<T>
            {
                MessageType = messageType,
                ErrorMessage = errorMessage,
                IsSessionExpired = false,
                IsSuccessful = false,
                Result = data
            };
        }
        #endregion
    }
}
