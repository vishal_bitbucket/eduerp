﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace EduErp.BO
{
    public class BaseBo
    {
        public string RequestEntityId { get; set; }
        public string RequestEntityCode { get; set; }
        public string ErrorMsg { get; set; }
        public string SuccessMsg { get; set; }
    }
    public enum AuthMessageType
    {
        None = 0,
        Failed = 1,
        Success = 2,
        UserNotFound = 3,
        PasswordDoNotMatch = 4,
        AccountNotVerified = 5,
        AccountNotApproved = 6,
        PasswordNotCreated = 7,
        AccountInactive = 8,
        AccountDeleted = 9,
        AccessDeniedToProduct = 10,
        AccessDeniedToWebsite = 11,
        CasServerError = 12,
        AuthMethodNotDefined = 14,
        NotAuthorized = 15,
        SubscriptionExpired = 16
    }

    public enum UserStatus
    {
        Active = 1,
        InActive = 2,
        Deleted = 3
    }

    public enum InstituteType
    {
        School = 1,
        Coaching = 2,
        College = 3
    }
    public enum Right
    {
        Read = 1,
        Write = 2
    }


    public class Common
    {
        public int Id { get; set; }
        public string Name { get; set; }

    }
    public enum LabelEnum
    {
        Class = 1,
        Batch = 2,
        School = 3,
        Coaching = 4,
        Institute = 5,
        User = 6,
        Message = 7,
        OnlineTest = 8,
        OnlineTestQuestion = 9,
        OnlineTestSubject = 10,
        QuestionAnswer = 11,
        OTExamSubject = 12,
        Student = 13
    }

    public class LevelName
    {
        public static string ParamClass { get; set; }
        public static string ParamSection { get; set; }
    }

    public enum HostSettingNames
    {
        ErrorLoggingEmailAddresses,
        SenderName,
        SenderEmailPassword,
        SmtpPort,
        SenderEmailAddress,
        SmtpHost,
        SmtpEnableSsl,
        TransactionLoginIdAndKey,
        AdminEmailAddresses
    }

    public class DashboardBo
    {
        public int TotalFeeCollection { get; set; }
        public int TotalExpense { get; set; }
        public List<FeeAndExpenseDetail> feeExpenseDetail { get; set; }
        public List<TodayBirthDetailList> TodayBirthDetailLists { get; set; }
        public List<UpcomingBirthDetailList> UpcomingBirthDetailLists { get; set; }
        public List<CountDetails> CountDetails { get; set; }
        public List<GeneralTypeNotificationList> GeneralTypeNotificationLists { get; set; }
        public List<SchoolTypeNotificationList> SchoolTypeNotificationLists { get; set; }
        public List<EmployeeTypeNotificationList> EmployeeTypeNotificationLists { get; set; }
        public List<StudentTypeNotificationList> StudentTypeNotificationLists { get; set; }
        //public List<ClassStudentChartBo> ClassStudentChartList { get; set; }
    }

    public class ClassStudentChartBo
    {
        public int TotalStudents { get; set; }
        public string Name { get; set; }
    }

    // public 

    public class GeneralTypeNotificationList
    {

        public string Subject { get; set; }
        public string Description { get; set; }

    }
    public class EmployeeTypeNotificationList
    {
        public DateTime NoticeDate { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
    }
    public class SchoolTypeNotificationList
    {
        public string Subject { get; set; }
        public string Description { get; set; }

    }
    public class StudentTypeNotificationList
    {
        public string Subject { get; set; }
        public string Description { get; set; }

    }
    public class TodayBirthDetailList
    {
        public string StudentName { get; set; }
        public string ClassName { get; set; }
        public string SectionName { get; set; }
        public DateTime BirthDate { get; set; }
    }
    public class UpcomingBirthDetailList
    {
        public string StudentName { get; set; }
        public string ClassName { get; set; }
        public string SectionName { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime BirthDateStr { get; set; }
    }

    public class ConfigurationBo
    {
        public int TotalStudent { get; set; }
        public int TotalSubject { get; set; }
        public int TotalClass { get; set; }
        public int TotalSection { get; set; }
        public int TotalUser { get; set; }
        public int TotalSession { get; set; }
        public int TotalMessage { get; set; }
        public int TotalFeeHead { get; set; }
        public int TotalItem { get; set; }
        public int TotalVendor { get; set; }
        public int TotalPlan { get; set; }
        public int TotalSchool { get; set; }
        public int TotalAttendance { get; set; }
        public int TotalEmployee { get; set; }

    }

    public class FeeAndExpenseDetail
    {
        public int TotalFeeCollection { get; set; }
        public int TotalExpense { get; set; }
    }
    public class PlanBo : PlanMaster
    {
        public List<Common> PlanList { get; set; }
    }
    public class CountDetails
    {
        public int TotalStudents { get; set; }
        public int TotalSubjects { get; set; }
        public int TotalClasses { get; set; }
        public int TotalSections { get; set; }
        public int TotalUsers { get; set; }
        public int LeftMessage { get; set; }
    }

    public class StudentUploadBo
    {
        public int Count { get; set; }
        public int ErrorCount { get; set; }
        public List<ErrorDetail> ErrorDetailsList { get; set; }
        public List<Common> ListOfClasses { get; set; }
        public List<Common> ListOfSections { get; set; }
        public List<Common> ListOfSessions { get; set; }
        public int SectionId { get; set; }
        public int ClassId { get; set; }
        public int SessionId { get; set; }

    }
    public class ErrorDetail
    {
        public string StudentName { get; set; }
        public string Error { get; set; }
        public string Description { get; set; }

    }

    public class NotificationUserBo
    {
        public string DisplayName { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public int RequestType { get; set; }
        public long Id { get; set; }
        public DateTime ExpiryDate { get; set; }
    }

    public class LoginResult
    {
        public AuthenticateUserBo User { get; set; }
        public string UploadServiceUrl { get; set; }
        public string DownloadServiceUrl { get; set; }

    }

    public class AuthenticateUserBo : UserMaster
    {
        public string SchoolName { get; set; }
        public int InstituteTypeId { get; set; }
        public int SessionId { get; set; }
        public string SessionName { get; set; }
    }
    public class ItemBo : ItemMaster
    {
        public string ItemTypeName { get; set; }
        public string SchoolName { get; set; }
        public List<Common> ListOfInventoryType { get; set; }

    }
    public class VendorBo : VendorMaster
    {
        public string SchoolName { get; set; }
        public List<Common> ListOfInventoryType { get; set; }
        public List<SelectListItem> ListOfStates { get; set; }
    }

    public class PlanMapping
    {
        public int PlanId { get; set; }
        public string PlanName { get; set; }
        // public string Detail { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
    public class SchoolBo
    {

        public int Id { get; set; }
        public int InstituteTypeId { get; set; }


        [Required(ErrorMessage = "Enter Code")]
        public string Code { get; set; }

        public int? ParentSchoolId { get; set; }
        public string ParentSchoolName { get; set; }

        [Required(ErrorMessage = "Enter Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Enter City Id")]
        public int CityId { get; set; }

        public string CityName { get; set; }

        public string StateName { get; set; }
        public int StateId { get; set; }

        [Required(ErrorMessage = "Enter Address")]
        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address { get; set; }

        [Required(ErrorMessage = "Enter PIN")]
        public string Pin { get; set; }

        [Required(ErrorMessage = "Enter Owner Name")]
        public string OwnerName { get; set; }


        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime CreatedOn { get; set; }
        public int LastUpdateBy { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public int CreatedBy { get; set; }

        public int StatusId { get; set; }
        public Guid S3key { get; set; }
        public string ImagePath { get; set; }
        [Required]
        public string ContactNo { get; set; }
        public List<PlanMapping> PlanMappingList { get; set; }
        public List<SelectListItem> ListOfSchools { get; set; }
        public List<SelectListItem> ListOfStates { get; set; }
        public List<Common> PlanTypeList { get; set; }
        public List<Common> ListOfInstituteType { get; set; }
        public List<Common> PlanList { get; set; }
        public int? PlanTypeId { get; set; }
        public int PlanId { get; set; }
        public string PlanName { get; set; }
        // public string Detail { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

    }


    public class HolidayBo : HolidayMaster
    {
        public string SessionName { get; set; }
        public List<Common> SessionList { get; set; }
        public string FromDateStr { get; set; }
        //   // public DateTime HoliodayToDate { get; set; }
        public string ToDateStr { get; set; }
        //    public DateTime FromDate { get; set; }
        //    public  DateTime ToDate { get; set; }
    }


    public class FeeHeadBo : FeeHeadMaster
    {
        // [Required(ErrorMessage = "Please enter name")]
        // public string Name { get; set; }
        public string ClassName { get; set; }
        public string FeeHeadName { get; set; }
        public List<Common> FeeHeadTypeList { get; set; }
        public string FeeHeadTypeName { get; set; }
        public int FeeHeadId { get; set; }
        public int ClassId { get; set; }
        public int SessionId { get; set; }
        // public int? Id { get; set; }
        public decimal? Amount { get; set; }
        public List<Common> ListOfClasses { get; set; }
        public List<SelectListItem> ListOfSessions { get; set; }
        public List<Common> ListOfFeeHeadName { get; set; }
        public List<MonthMenu> ListOfMonths { get; set; }


    }

    public class MyAccountBo
    {

        public SchoolBo SchoolDetails { get; set; }
        public AccountDetailBo AccountDetails { get; set; }
        public List<AccountFeeDtailBo> ListOfTransactions { get; set; }
        public List<MessagingTree> ListOfMessage { get; set; }
        public List<PlanMaster> PlansList { get; set; }
        public List<Common> ListOfTemplateText { get; set; }
        public string Subject { get; set; }
        public string Query { get; set; }
    }
    public class MessageAutomateMappingBo : MessageAutomateMapping
    {
        public string TemplateMessage { get; set; }
    }

    public class MessageSettings
    {
        public int MessageAutomaticId { get; set; }
        public int TemplateId { get; set; }
    }

    public class MonthMenu
    {
        public int MonthId { get; set; }
        public bool Selected { get; set; }
        public string MonthName { get; set; }
        public int ClassId { get; set; }
        public int SessionId { get; set; }
        public decimal? Amount { get; set; }
    }

    public class EmployeeTypeBo : EmployeeTypeMaster
    {
        public string SchoolName { get; set; }
    }
    public class ExpensesBo : Inventory
    {
        public List<Common> ListOfPaymentMode { get; set; }
        public List<SelectListItem> SessionList { get; set; }
        public int FromMothId { get; set; }
        public int ToMonthId { get; set; }
        public List<SelectListItem> ListOfSessions { get; set; }
        public List<Common> ListOfMonths { get; set; }
        public DateTime? FromMonth { get; set; }
        public DateTime? ToMonth { get; set; }
        public string FromDateStr { get; set; }
        public string ToDateStr { get; set; }
        public List<Common> ExpensesList { get; set; }
        public List<Common> ItemList { get; set; }
        public List<Common> VendorList { get; set; }
        public List<Common> EmployeeNameList { get; set; }
        public string SessionName { get; set; }
        public DateTime? Date { get; set; }
        public string InventoryName { get; set; }
        public int? EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string ItemName { get; set; }
        public string VendorName { get; set; }
        //public new DateTime  StartDate { get; set; }
        //public new DateTime EndDate { get; set; }

    }

    public class MessageBo : MessageMaster
    {
        public List<Common> ListOfClasses { get; set; }
        public List<Common> ListOfSections { get; set; }
        // public List<Common> ListOfStudents { get; set; }
        public List<StudentList> StudentLists { get; set; }
        public bool IsSendLater { get; set; }
        public string MessageDateStr { get; set; }
        public string MessageTimeStr { get; set; }
        public int MsgCount { get; set; }
        public int? TemplateId { get; set; }
        public List<Common> ListOfTemplateText { get; set; }

    }

    public class MessagingTree
    {
        public bool IsSelected { get; set; }
        public AutomateMessageBo Permission { get; set; }
        public List<MessagingTree> Children { get; set; }
    }

    public class AutomateMessageBo
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsSelected { get; set; }
        public int MessageAutomatedId { get; set; }
        public int TemplateId { get; set; }

        // public List<MessageList> messageList { get; set; }
    }

    public class MessageList
    {
        public bool IsSelected { get; set; }
        public int MessageAutomatedId { get; set; }
        public int TemplateId { get; set; }
    }

    public class CertificationBo : CertificateTemplateSchoolMapping
    {
        public List<Common> ListOfCertificateTemplateType { get; set; }
        public List<Common> ListOfCertificateType { get; set; }
        public List<Certificate> MappingList { get; set; }
        public string CertificateTemplateTypeName { get; set; }
        public string CertificateTemplateName { get; set; }
        public int? CertificateTemplateTypeId { get; set; }
    }
    public class Certificate
    {
        public int TemplateTypeId { get; set; }
        public int TemplateId { get; set; }
        public int TemplateName { get; set; }
        public int TemplateTypeName { get; set; }
    }
    public class AccountDetailBo : AccountDetail
    {
        public List<SelectListItem> ListOfSchools { get; set; }
        public List<Common> ListOfPlans { get; set; }
        public string NextPaymentDateStr { get; set; }
        public string DueDateStr { get; set; }
        public string AppStartDateStr { get; set; }
        public string SchoolName { get; set; }
        public string PaymentPlan { get; set; }
        public List<DayMenu> ListOfDays { get; set; }
        public string[] Ids { get; set; }
    }

    public class DayMenu
    {
        public int DayId { get; set; }
        public string DayName { get; set; }
        public bool Selected { get; set; }

    }


    public class AccountFeeDtailBo : AccountFeeDetail
    {
        public string SchoolName { get; set; }
        public string PaymentPlan { get; set; }
        public string PaymentMode { get; set; }
        public string PaymentDateStr { get; set; }
        public string FromDateStr { get; set; }
        public string ToDateStr { get; set; }
        public List<Common> ListOfPaymentModes { get; set; }
        public List<Common> ListOfPlans { get; set; }
        public List<SelectListItem> ListOfSchools { get; set; }
    }


    public class StudentList
    {
        public int StudentIdForMessage { get; set; }
        public string StudentName { get; set; }
        public bool Selected { get; set; }
    }
    public class QuestionList
    {
        public int QuestionIdForQuestionMapping { get; set; }
        public string Question { get; set; }
        public bool Selected { get; set; }
    }

    public class UserBo
    {

        public int Id { get; set; }
        public string Otp { get; set; }
        public Guid VerificationCode { get; set; }
        public DateTime VerificationCodeGeneratedOnUtc { get; set; }

        [Required(ErrorMessage = "Enter Name")]
        public string Name { get; set; }


        [Required(ErrorMessage = "Enter UserId")]
        public string UserId { get; set; }

        public bool IsPasswordCreated { get; set; }
        //[Required]
        [DataType(DataType.Password)]
        [DisplayName("New password")]
        public string NewPassword { get; set; }

        public int UserTypeId { get; set; }
        // [Required]
        [DataType(DataType.Password)]
        [DisplayName("Confirm Password")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "Password and Confirmation Password must match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Email")]
        [RegularExpression(@"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$",
        ErrorMessage = "Please Enter Correct Email Address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please Enter Mobile No")]
        [Display(Name = "Mobile")]
        [StringLength(10, ErrorMessage = "The Mobile must contains 10 digits", MinimumLength = 10)]
        public string MobileNo { get; set; }
        public int? SchoolId { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime CreatedOn { get; set; }

        public int? CreatedBy { get; set; }


        public int StatusId { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime? LastModifiedOn { get; set; }
        public int? LastModifiedBy { get; set; }
        public List<SelectListItem> ListOfSchools { get; set; }

        public Guid Salt { get; set; }
        public List<MenuItem> MenuList { get; set; }
        public List<Common> Rights { get; set; }
        public List<Common> UserTypeList { get; set; }
    }

    public class MenuItem
    {
        public int MenuId { get; set; }
        public string MenuName { get; set; }
        public bool IsSelected { get; set; }
        public int RightId { get; set; }
        public int OrderId { get; set; }
    }

    public class ClassBo
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter name")]
        public string Name { get; set; }
        public int SchoolId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public int LastUpdatedBy { get; set; }
        public int? Status { get; set; }
        public List<SelectListItem> ListOfSchools { get; set; }

    }

    public class OnlineExamTestBo 

    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter name")]
        public string Name { get; set; }
      
       // public int NumberOfQuestion { get; set; }
        public int TimeInMinutes { get; set; }
        public string StartTime { get; set; }
        public DateTime StartDate { get; set; }
        public int SchoolId { get; set; }
        public int? Status { get; set; }
        public int LastUpdatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
       // public List<OnlineExamTestBo> OnlineExamTestBoList { get; set; }
        public List<SelectListItem> ListOfSchools { get; set; }
    }
    public class OnlineExamTestQuestionBo
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter name")]
       // public string Name { get; set; }
        public string Questions { get; set; }
        public string Description { get; set; }
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public int SchoolId { get; set; }
        public int QuestionTypeId { get; set; }
        public string QuestionType { get; set; }
        public bool Ismcq { get; set; }
        public int LastUpdatedBy { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? Status { get; set; }
        public List<OnlineExamTestQuestionBo> OnlineExamTestQuestionBoList { get; set; }
        public List<SelectListItem> ListOfSchools { get; set; }
        public List<Common> ListOfQuestionType { get; set; }
        public List<Common> ListOfSubject { get; set; }
        public string ImagePath { get; set; }
    }
    public class OnlineExamTestSubjectBo
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter name")]
        public string SubjectName { get; set; }
        
        public int SchoolId { get; set; }
        
        public int LastUpdatedBy { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? Status { get; set; }
        public List<OnlineExamTestSubjectBo> OnlineExamTestSubjectBoList { get; set; }
        public List<SelectListItem> ListOfSchools { get; set; }
      
    }
    public class SubjectBo : SubjectClassMappingBo
    {

        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter name")]
        public string Name { get; set; }
        public string ClassName { get; set; }
        public int SchoolId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public int? Status { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public int? LastUpdatedBy { get; set; }
        public List<Common> ListOfClasses { get; set; }
        public string ClassIds { get; set; }
        public int[] ClassIdes { get; set; }

        public List<SubjectBo> SubjectBoList { get; set; }

    }

    public class NotificationBo : NoticeMaster
    {
        public string NotificationTypeName { get; set; }
        public string SchoolName { get; set; }
        public List<Common> ListOfNotifications { get; set; }

    }

    public class EmployeeBo : TeacherMaster
    {
        public bool IsChecked { get; set; }
        public Guid? EmployeeS3Key { get; set; }
        public string Name { get; set; }
        public string DateOfBirthStr { get; set; }
        public string DateOfJoiningStr { get; set; }
        public string DateOfExitStr { get; set; }
        public List<Common> ListOfTitle { get; set; }
        public List<Common> ListOfNationality { get; set; }
        public List<Common> ListOfCategory { get; set; }
        public List<Common> ListOfReligion { get; set; }
        public List<SelectListItem> ListOfCity { get; set; }
        public List<SelectListItem> ListOfState { get; set; }
        public List<Common> ListOfGender { get; set; }
        public List<Common> ListOfMedium { get; set; }
        public List<Mapping> MappingList { get; set; }
        public List<Common> ListOfClasses { get; set; }
        public List<Common> ListOfSections { get; set; }
        public List<Common> ListOfSubjects { get; set; }
        public int? ClassId { get; set; }
        public int? SectionId { get; set; }
        public int? SubjectId { get; set; }
        public List<Common> ListOfEmployeeType { get; set; }


    }
    public class ApplicationLogBo : ApplicationLog
    {
        public string SchoolName { get; set; }
    }
    public class ExamBo : ExamMaster
    {
        public List<ExamMapping> MappingList { get; set; }
        public List<Common> ListOfClasses { get; set; }
        public List<Common> ListOfExams { get; set; }
        public List<Common> ListOfStatus { get; set; }
        public List<Common> ListOfStudents { get; set; }
        //  public int StatusId { get; set; }
        public List<Common> ListOfSections { get; set; }
        public List<SelectListItem> ListOfSessions { get; set; }
        public List<Common> ListOfSubjects { get; set; }
        public int? SubjectId { get; set; }
        public int SessionId { get; set; }
        public int SectionId { get; set; }
        public string SubjectName { get; set; }
        public DateTime? Date { get; set; }
        public decimal? TotalMarks { get; set; }
        public int ExamId { get; set; }
        public int StudentId { get; set; }
    }

    public class ExamMapping
    {
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public DateTime? Date { get; set; }
        public decimal TotalMarks { get; set; }
    }
    public class ExamQuestionMapping 
    {
        public int ExamId { get; set; }
        public int SubjectId { get; set; }
        public int QuestionId { get; set; }
        public int Id { get; set; }
        public int StatusId { get; set; }
        public int SchoolId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public int LastUpdatedBy { get; set; }
        public List<Common> ListOfSubject { get; set; }
        public List<Common> ListOfExam { get; set; }
        public List<QuestionList> QuestionLists { get; set; }
    }
    public class OTExamSubjectMapping
    {
        public int ExamId { get; set; }
        public string ExamName { get; set; }
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public int Sequence { get; set; }
        public int TimeInMinutes { get; set; }
        public int Id { get; set; }
        public int StatusId { get; set; }
        public int SchoolId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public int LastUpdatedBy { get; set; }
        public List<Common> ListOfSubject { get; set; }
        public List<Common> ListOfExam { get; set; }
        public List<ExamSubjectList> ExamSubjectLists { get; set; }
        public List<ExamSubjectList> ExamSubjectMappingLists { get; set; }
    }
    public class InvoiceBo : SchoolBo
    {
        public int TotalMessage { get; set; }
        public int TotalStudent { get; set; }
        public int TotalUser { get; set; }
    }

    public class StudentProfileBo : StudentBo
    {
        public string ClassName { get; set; }
        public string SectionName { get; set; }
        public string SessionName { get; set; }
        //public string RegistrationNumber { get; set; }
        public int FromMonthId { get; set; }
        public int ToMonthId { get; set; }
        public List<FeetransactionDetailList> FeetransactionDetail { get; set; }
        public List<AttendanceDetailList> AttendanceDetail { get; set; }
        public List<Common> ListOfFromMonths { get; set; }
        public List<Common> ListOfToMonths { get; set; }


    }
    public class StudentBo : StudentClassMappingBo
    {
        public int Count { get; set; }
        public int AddCount { get; set; }
        public string Dues { get; set; }
        public string OtherRemarks { get; set; }
        public string SchoolWebsite { get; set; }
        public string SchoolEmail { get; set; }
        public string SchoolCode { get; set; }
        public string SchoolName { get; set; }
        public string SchoolAddress { get; set; }
        public string SessionName { get; set; }
        public int Id { get; set; }
        public string Uid { get; set; }
        public bool IsCheked { get; set; }
        public Guid S3key { get; set; }
        // public int StateId { get; set; }
        [Required(ErrorMessage = "Please Enter First Name")]
        public string FirstName { get; set; }
        public string MiddileName { get; set; }
        public int RegistrationNumber { get; set; }
        public string LastName { get; set; }
        public int? TitleId { get; set; }
        [RegularExpression(@"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$", ErrorMessage = "Please Enter Correct Email Address")]
        public string Email { get; set; }
        public string StudentMobile { get; set; }
        public string StudentOtherInfo { get; set; }
        public string ParentContactNo { get; set; }
        public string GenderName { get; set; }
        public string RelegionCategoryName { get; set; }
        public string TitleName { get; set; }
        public string NationlityName { get; set; }
        public string RelegionName { get; set; }
        public string ParentMobileNumber { get; set; }
        public string MotherName { get; set; }
        public string FatherName { get; set; }
        public string CuurentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string StateName { get; set; }
        public string CurrentStateName { get; set; }
        public string CityName { get; set; }
        public string CurrentCityName { get; set; }
        public string PermanentCityName { get; set; }
        public string PermanentStateName { get; set; }
        public int PermanentStateId { get; set; }
        public int CurrentStateId { get; set; }
        public int CurrentCityId { get; set; }
        public int PermanentCityId { get; set; }
        public string CurrentPinCode { get; set; }
        public string PermanentPinCode { get; set; }
        public StudentTmplateBo StudentTmplateBo { get; set; }
        public int? GenderId { get; set; }
        public string GuardianName { get; set; }
        public string GuardianRelation { get; set; }
        public string GuardianAddress { get; set; }
        public string GuardianOccupation { get; set; }
        public string GuardianMobile { get; set; }
        public int SchoolId { get; set; }
        public string DoeStr { get; set; }
        public string DobStr { get; set; }
        public string DojStr { get; set; }
        public DateTime? Doj { get; set; }
        public DateTime? Dob { get; set; }
        public DateTime? Doe { get; set; }
        public int StatusId { get; set; }
        public int StudentStatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public int LastUpdatedBy { get; set; }
        public string ImagePath { get; set; }
        public int? RelegionId { get; set; }
        public int? CategoryId { get; set; }
        public string ReasonForExit { get; set; }
        public int? NationlityId { get; set; }
        public List<Common> ListOfStatus { get; set; }
        public List<Common> ListOfTitle { get; set; }
        public List<Common> ListOfNationality { get; set; }
        public List<Common> ListOfCategory { get; set; }
        public List<Common> ListOfReligion { get; set; }
        public List<SelectListItem> ListOfCity { get; set; }
        public List<SelectListItem> ListOfState { get; set; }
        public List<Common> ListOfGender { get; set; }
        public List<Common> ListOfAdmissionStatus { get; set; }
        public int AdmissionStatusId { get; set; }

        public List<StudentAcademicRecord> StudentAcademicRecordList { get; set; }
        //public CityStateBo CityState { get; set; }


        //public List<StudentProfileBo> StudentProfileList { get; set; }
    }

    public class StudentAcademicRecord
    {
        public int StudentClassMappingId { get; set; }
        public int StudentId { get; set; }
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public int SectionId { get; set; }
        public string SectionName { get; set; }
        public int SessionId { get; set; }
        public string SessionName { get; set; }
    }
    public class StudentTmplateBo
    {
        public List<StudentFilterList> StudentFilterLists { get; set; }
    }

    public class StudentFilterList : StudentBo
    {
        public int StudentId { get; set; }
        public int StudentStatusId { get; set; }
        public string StudentRollNumber { get; set; }
        public string StudentName { get; set; }
        public string StudentContactNumber { get; set; }
        public string StudentGender { get; set; }
        public string StudentCategory { get; set; }

    }
    public class DownloadBo
    {
        public List<Common> ListOfTemplateName { get; set; }
        public List<Common> ListOfClasses { get; set; }
        public List<Common> ListOfSections { get; set; }
        public List<Common> ListOfStudents { get; set; }
        public List<SelectListItem> ListOfSessions { get; set; }
        public int SessionId { get; set; }
        public int StudentId { get; set; }
        public int ClassId { get; set; }
        public int SectionId { get; set; }
        public int TemplateId { get; set; }
    }
    public class FineDiscountTransactionDetailBo : FineDiscountTransactionDetail
    {
        public string FeeHeadName { get; set; }
    }
    public class FeeTransactionBo : FeeTransaction
    {
        public Guid S3key { get; set; }
        public string SchoolImagePath { get; set; }
        public decimal? GrandTotal { get; set; }
        public decimal? TotalComponentSum { get; set; }
        public decimal? TotalFineSum { get; set; }
        public decimal? TotalDiscountSum { get; set; }
        public string SessionName { get; set; }
        public string ClassName { get; set; }
        public string SectionName { get; set; }
        public string FromMonthName { get; set; }
        public string ToMonthName { get; set; }
        public string StudentName { get; set; }
        //public string FirstName { get; set; }
        //public string LastName { get; set; }
        public string PaymentModeName { get; set; }
        public List<Common> ListOfPaymentMode { get; set; }
        public List<Common> ListOfMonthes { get; set; }
        public List<Common> ListOfClasses { get; set; }
        public List<Common> ListOfSections { get; set; }
        public List<Common> ListOfStudents { get; set; }
        public List<SelectListItem> ListOfSessions { get; set; }
        public int SessionId { get; set; }
        public bool IsBalancedSettled { get; set; }
        public int ClassId { get; set; }
        public int SectionId { get; set; }
        //  public int FromMonthId { get; set; }
        //  public int ToMonthId { get; set; }
        public FeeTransactionTemplateBo FeeTransactionTemplateBo { get; set; }
        public decimal? PreviousBalance { get; set; }
        public decimal? TotalAmount { get; set; }
        public decimal? TotalClassGeneralAmount { get; set; }
        public decimal TotalAmountPaid { get; set; }
        public decimal NetBalance { get; set; }
        public decimal ClassByTotalAmount { get; set; }
        public decimal GeneralByTotalAmount { get; set; }
        public decimal? TotalFine { get; set; }
        public decimal? TotalDiscount { get; set; }
        public int FeeHeadMappingId { get; set; }
        public string Name { get; set; }
        public int? FineFeeheadTypeId { get; set; }
        public decimal? FineAmount { get; set; }
        public int? DiscountFeeheadTypeId { get; set; }
        public decimal? DiscountAmount { get; set; }
        public List<Common> ListOfFineFeeheads { get; set; }
        public List<Common> ListOfDiscountFeeheads { get; set; }

    }

    public class ReportBo
    {
        public List<Common> ListOfMonthes { get; set; }
        public List<Common> ListOfClasses { get; set; }
        public List<Common> ListOfSections { get; set; }
        public List<SelectListItem> ListOfSessions { get; set; }
        public int SessionId { get; set; }
        public int ClassId { get; set; }
        public int PayStatusId { get; set; }
        public int SectionId { get; set; }
        public int FromMonthId { get; set; }
        public int ToMonthId { get; set; }
        public List<Common> ListOfPayStatus { get; set; }
        public List<StudentStatuschartBo> ListOfStudentStatus { get; set; }
        // public StudentStatuschartBo StudentStatusChartBo { get; set; }

        public List<GenderDetail> GenderDetailList { get; set; }
        public int TotalMaleCount { get; set; }
        public int TotalFemaleMaleCount { get; set; }
        public string GenderName { get; set; }

    }
    //public class StudentStatusChartGenderWiseBo
    //{
    //    public List<GenderDetail> GenderDetailList { get; set; }
    //}
    public partial class Image
    {
        public int ID { get; set; }
        public string ImagePath { get; set; }
    }
    public class GenderDetail
    {
        public int CountNo { get; set; }
        public string GenderName { get; set; }
        public int GenderId { get; set; }

    }

    public class GenderDetailClassWiseDrillDown
    {
        public string ClassName { get; set; }
        public int ClassId { get; set; }

        public int CountNo { get; set; }

        public string GenderName { get; set; }
        public int GenderId { get; set; }

    }

    public class SessionWiseStudentReport
    {
        public List<SessionWiseDetail> sessionWiseDetail { get; set; }
    }
    public class SessionWiseDetail
    {
        public int CountNo { get; set; }
        public string SessionName { get; set; }
    }
    public class GendderWiseStudentReport
    {
        public List<GenderDetailClassWiseDrillDown> genderDetailClassWiseDrillDown { get; set; }
        public List<GenderDetail> genderDetail { get; set; }
    }

    public class ExpensesChart
    {
        public string Name { get; set; }
        public int Total { get; set; }
    }
    public class ExamChart
    {
        public string StudentName { get; set; }
       // public DateTime ExamDate { get; set; }
        public int Physics { get; set; }
        //public int ObtainedMarks { get; set; }
    }
    public class FeesChart 
    {
        public string ClassName { get; set; }
        public int TotalFees { get; set; }
    }
    public class TypeWiseExpensesReport
    {
        public List<ExpensesChart> TypewiseDetail { get; set; }
    }
    public class TypeWiseExamReport
    {
        public List<ExamChart> TypewiseDetail { get; set; }
    }
    public class TypeStudentWiseExamReport
    {
        public List<StudentWiseExamChart> TypeStudentWiseDetail { get; set; }
    }
    public class StudentWiseExamChart
    {
        public string Name { get; set; }
        // public DateTime ExamDate { get; set; }
       // public int Physics { get; set; }
       //public int ObtainedMarks { get; set; }
        public int TotalMarks { get; set; }
    }
    public class CategoryWiseStudentReport
    {
        public List<CategoryWiseDetail> categoryDetail { get; set; }
    }
    public class CategoryWiseDetail
    {
        public int CountNo { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }

    public class StudentStatuschartBo
    {
        public int TotalActiveStudents { get; set; }
        public int TotalInactiveStudents { get; set; }
        public int TotalStudents { get; set; }
        public string ClassName { get; set; }
    }

    public class StudentClassMappingBo
    {
        public int MappingId { get; set; }
        //[Required(ErrorMessage = "Select Class Name")]
        public int ClassId { get; set; }

        //[Required(ErrorMessage = "Select Section Name")]
        public int SectionId { get; set; }

        //[Required(ErrorMessage = "Select Session Name")]
        public int SessionId { get; set; }
        public string SerialNo { get; set; }
        public string StudentCode { get; set; }
        public List<Common> ListOfClasses { get; set; }
        public List<Common> ListOfSessions { get; set; }
        public List<Common> ListOfSections { get; set; }
        public List<Common> ListOfStudents { get; set; }

    }

    public class SectionBo
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter name")]
        public string Name { get; set; }
        public string ClassName { get; set; }
        public string SchoolName { get; set; }
        public int? SchoolId { get; set; }
        public int? ClassId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }

        public int Status { get; set; }
        public int? LastUpdatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }

        public List<Common> ListOfClasses { get; set; }
        public List<SelectListItem> SchoolList { get; set; }

    }


    //public class HolidayBo
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //    public string Description { get; set; }
    //    public DateTime? FromDate { get; set; }
    //    public DateTime? ToDate { get; set; }
    //    public int SessionId { get; set; }
    //    public int SchoolId { get; set; }
    //    public int? LastUpdatedBy { get; set; }
    //    public DateTime? LastUpdatedOn { get; set; }
    //    public DateTime? CreatedOn { get; set; }
    //    public int? CreatedBy { get; set; }
    //    public int? StatusId { get; set; }
    //    public List<Common> SessionList { get; set; }
    //}

    public class Permission
    {
        public List<UserRight> UserRights { get; set; }
        public List<MenuMasterBo> MenuList { get; set; }
        public List<Common> SessionList { get; set; }
    }

    public class MenuMasterBo : MenuMaster
    {
        public int Count { get; set; }
    }

    public class StudentExamMappingBo : StudentExamMapping
    {
        public List<Common> ListOfClasses { get; set; }
        public List<StudentExamList> StudentExamLists { get; set; }
        public List<Common> ListOfSessions { get; set; }
        public List<Common> ListOfSections { get; set; }
        public List<Common> ListOfStudents { get; set; }
        public List<Common> ListOfExamType { get; set; }
        public string ClassName { get; set; }
        public string TotalMarks { get; set; }
        public string StudentName { get; set; }
        public string ExamName { get; set; }
        public string SectionName { get; set; }
        public int ExamId { get; set; }
    }
    public class QuestionAnswerMappingBo : OnlineTestQuestionMaster
    {        
        public int ChoiceTypeId { get; set; }
        public string ChoiceType { get; set; }
        public List<Common> ListOfSubject { get; set; }
        public List<Common> ListOfChoice { get; set; }
        public List<QuestionAnswerMapping> Answers { get; set; }
    }

    public class StudentExamList
    {
        public int ExamSubjectMappingId { get; set; }
        public string SubjectName { get; set; }
        public decimal TotalMarks { get; set; }
        public decimal ObtainMarks { get; set; }
    }
    public class ExamSubjectList
    {
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public int Sequence { get; set; }
        public int TimeInMinutes { get; set; }
    }
    public class PermissionTree
    {
        public bool IsSelected { get; set; }
        public MenuMaster Permission { get; set; }
        public List<PermissionTree> Children { get; set; }
    }

    public class AttendanceBo
    {
        public int SchoolId { get; set; }
        public List<Common> DayStatusList { set; get; }
        public int StatusId { get; set; }
        public int SessionId { get; set; }
        public List<Common> SessionList { get; set; }
        public int ClassId { get; set; }
        public List<Common> ClassList { get; set; }
        public int SectionId { get; set; }
        public List<Common> SectionList { get; set; }
        public DateTime Date { get; set; }
        public string DateStr { get; set; }
        public AttendanceTemplateBo AttendanceTemplateBo { get; set; }
        public int MsgCount { get; set; }


    }

    public class AttendanceTemplateBo
    {
        public List<AttendanceList> AttendanceLists { get; set; }
        public List<Common> DayStatusList { get; set; }
        public List<AttendanceDetailList> AttendanceDetailList { get; set; }
    }

    public class AttendanceList
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public int DayStatusId { get; set; }
        public string Remark { get; set; }
        public string StudentCode { get; set; }
        public DateTime AttendanceDate { get; set; }
        public int studentStatusId { get; set; }

    }

    public class FeeTransactionTemplateBo
    {
        public List<ClassByComponentList> ClassByComponentLists { get; set; }
        public List<GeneralByComponentList> GeneralByComponentLists { get; set; }
        public List<FeetransactionDetailList> FeetransactionDetailList { get; set; }
        public List<FeeTransactionMappingIdList> FeetransactionMappingIdList { get; set; }
        public List<FineDetail> FineDetailList { get; set; }
        public List<ComponentList> ComponentList { get; set; }
        public decimal Prevbal { get; set; }
        // public List<FineList> FineList { get; set; }
        //public List<DicountList> DiscountList { get; set; }
        public List<DiscountDetail> DiscountDetailList { get; set; }
    }
    public class FineDetail
    {
        public int Id { get; set; }
        public string FineName { get; set; }
        public int FineFeeHeadId { get; set; }
        public decimal? Amount { get; set; }
    }
    public class DiscountDetail
    {
        public int Id { get; set; }
        public string DiscountTypeName { get; set; }
        public int DiscountFeeHeadId { get; set; }
        public decimal? DiscountAmount { get; set; }
    }
    public class Mapping
    {
        public int ClassId { get; set; }
        public int SubjectId { get; set; }
        public int SectionId { get; set; }
        public string ClassName { get; set; }
        public string SectionName { get; set; }
        public string SubjectName { get; set; }


    }

    public class ClassByComponentList
    {
        public int FeeHeadMappingId { get; set; }
        public string Name { get; set; }
        public int ClassByClassId { get; set; }
        public string ClassName { get; set; }
        public int ClassByMonthId { get; set; }
        public string FeeMonthName { get; set; }
        public decimal? Amount { get; set; }
        public bool Selected { get; set; }

    }
    public class ComponentList
    {
        public int FeeHeadTypeId { get; set; }
        public string Name { get; set; }
        public decimal? Amount { get; set; }

    }
    public class FineList
    {
        public int FeeHeadTypeId { get; set; }
        public string Name { get; set; }
        public decimal? Amount { get; set; }

    }
    public class DicountList
    {
        public int FeeHeadTypeId { get; set; }
        public string Name { get; set; }
        public decimal? Amount { get; set; }

    }
    public class SubjectClassMappingBo
    {
        public int MappingId { get; set; }
        public int SubjectId { get; set; }
        public int ClassId { get; set; }
        public int MappingStatusId { get; set; }
        public int MappingCreatedBy { get; set; }
        public DateTime MappingCreatedOn { get; set; }
        public int? MappingLastUpdatedBy { get; set; }
        public DateTime? MappingLastUpdatedOn { get; set; }
    }

    public class FeeStatusDetailBo
    {
        public int FeeStatus { get; set; }
        public List<PaidList> FeePaidLists { get; set; }
    }

    public class PaidList
    {


       
        
        public int Id { get; set; }
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public decimal TotalAmountDeposit { get; set; }
        public bool IsSettled { get; set; }
        public decimal Total { get; set; }
        public decimal TotalPayableAmount { get; set; }
        //public decimal PreviousBalance { get; set; }
        //public decimal BalanceAmount { get; set; }
        //public decimal TotalAmount { get; set; }
        //public decimal Fine { get; set; }
        //public decimal Discount { get; set; }
        //public DateTime TransactionDate { get; set; }
        //public string FromMonth { get; set; }
        //public string ToMonth { get; set; }
    }


    public class AttendanceDetailList
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public int DayStatusId { get; set; }
        public string Remark { get; set; }
        public string StudentCode { get; set; }
        public DateTime AttendanceDate { get; set; }
        public int studentStatusId { get; set; }

    }

    public class FeetransactionDetailList
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public decimal TotalAmountDeposit { get; set; }
        public bool IsSettled { get; set; }
        public decimal Total { get; set; }
        public decimal TotalPayableAmount { get; set; }
        public decimal PreviousBalance { get; set; }
        public decimal BalanceAmount { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal Fine { get; set; }
        public decimal Discount { get; set; }
        public DateTime TransactionDate { get; set; }
        public string FromMonth { get; set; }
        public string ToMonth { get; set; }
    }

    public class FeeTransactionMappingIdList
    {
        public int FeeHeadMappingId { get; set; }
    }

    public class GeneralByComponentList
    {
        public int FeeHeadMappingId { get; set; }
        public string Name { get; set; }
        public string FeeMonthName { get; set; }
        public decimal? Amount { get; set; }
        public bool Selected { get; set; }
    }

    public enum DayStatus
    {
        Present = 1,
        Absent = 2,
        HalfDay = 3,
        Holiday = 4,
        SchoolHoliday = 5
    }

    public enum FeeStatus
    {
        Paid = 1,
        Unpaid = 2
    }

    public enum FeeHeadType
    {
        General = 1,
        ClassBy = 2,
        Fine = 4,
        Discount = 3

    }
    public class AttendanceVm
    {
        public List<DayStatus> DayStatuses { get; set; }
        public List<DateTime> HeaderDates { get; set; }
        public List<AttendanceInfoVm> AttendanceInfoVMs { get; set; }
    }

    public class AttendanceInfoVm
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public List<AttendanceMaster> AttendanceBos { get; set; }
    }

    public enum VerificationType
    {
        Email = 1,
        Sms = 2
    }

    public enum AutomateMesageFor
    {
        Absent = 2,
        FeeNotificationBeforeExam = 4,
        Dues = 5,
        UserCreated = 7,
        //StudentCreated=7
    }
    public class MessageData
    {
        public string Number { get; set; }
        public string MessageId { get; set; }
    }

    public class SmsResponse
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string JobId { get; set; }
        public List<MessageData> MessageData { get; set; }
    }

    public class ContactUs
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Message { get; set; }
    }
}
