﻿

using System.ComponentModel.DataAnnotations.Schema;
using System;
 
namespace EduErp.BO
{ 
    public class StudentShadow
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public int? SessionId { get; set; }
        public int? ClassId { get; set; }
        public int? SectionId { get; set; }
        public int? TitleId { get; set; }
        public string Email { get; set; }
        public string StudentMobile { get; set; }
        public string StudentOtherInfo { get; set; }
        public string GuardianAddress { get; set; }
        public string GuardianOccupation { get; set; }
        public string GuardianRelationWithStudent { get; set; }
        public string GuardianMobileNo { get; set; }
        public string GuardianName { get; set; }
        public string MessageMobileNumber { get; set; }
        public string MotherName { get; set; }
        public string FatherName { get; set; }
        public string CorrAddress { get; set; }
        public int? CorrCityId { get; set; }
        public string CorrPinCode { get; set; }
        public string PermanentAddress { get; set; }
        public int? PermanentCityId { get; set; }
        public int? CurrentStateId { get; set; }
        public int? PermanentStateId { get; set; }
        public string ParentMobileNo { get; set; }
        public string ParentContactNo { get; set; }
        public string PermanentPinCode { get; set; }
        public int? SchoolId { get; set; }
        public DateTime? DOJ { get; set; }
        public DateTime? DOB { get; set; }
        public DateTime? DOE { get; set; }
        public string Uid { get; set; }
        public int? StatusId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public int? LastUpdatedBy { get; set; }
        public string ImagePath { get; set; }
        public int? RelegionId { get; set; }
        public int? CategoryId { get; set; }
        public string ReasonForExit { get; set; }
        public int? NationlityId { get; set; }
        public int? GenderId { get; set; }
	}
     
    public class SubjectClassMapping
    {
        public int Id { get; set; }
        public int SubjectId { get; set; }
        public int ClassId { get; set; }
        public int StatusId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? LastUpdatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
	}
     
    public class SubjectMaster
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SchoolId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public int? Status { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public int? LastUpdatedBy { get; set; }
	}
     
    public class TeacherMaster
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public int? EmployeeTypeId { get; set; }
        public string EmployeeCode { get; set; }
        public int? TitleId { get; set; }
        public string Email { get; set; }
        public string MobileNumber { get; set; }
        public string MotherName { get; set; }
        public string FatherName { get; set; }
        public string CurrentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public int? CurrentCityId { get; set; }
        public int? PermanentCityId { get; set; }
        public int? CurrentStateId { get; set; }
        public int? PermanentStateId { get; set; }
        public string UniversityName { get; set; }
        public string Graduation { get; set; }
        public DateTime? GraduationPassingYear { get; set; }
        public decimal? GraduationMarks { get; set; }
        public string HighSchoolBoardName { get; set; }
        public DateTime? HighschoolPassingYear { get; set; }
        public int? HighSchoolMediumId { get; set; }
        public decimal? HighSchoolMarks { get; set; }
        public string InterSchoolBoardName { get; set; }
        public DateTime? InterPassingYear { get; set; }
        public int? InterSchoolMediumId { get; set; }
        public decimal? InterSchoolMarks { get; set; }
        public string CurrentPinCode { get; set; }
        public string PermanentPinCode { get; set; }
        public int SchoolId { get; set; }
        public DateTime? DOJ { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public DateTime? DateOfExit { get; set; }
        public string UID { get; set; }
        public string PanNumber { get; set; }
        public int? StatusId { get; set; }
        public string ImagePath { get; set; }
        public int? RelegionId { get; set; }
        public int? CategoryId { get; set; }
        public int? NationalityId { get; set; }
        public int? GenderId { get; set; }
        public string ReasonForExit { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public int? LastUpdatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public Guid? S3key { get; set; }
	}
     
    public class TeacherSubjectMapping
    {
        public int Id { get; set; }
        public int TeacherId { get; set; }
        public int ClassId { get; set; }
        public int SectionId { get; set; }
        public int SubjectId { get; set; }
	}
     
    public class TemplateMaster
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
	}
     
    public class UserLoginHistory
    {
        public long Id { get; set; }
        public int UserId { get; set; }
        public DateTime LoginDate { get; set; }
        public string IpAddress { get; set; }
	}
     
    public class UserMaster
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? UserTypeId { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public Guid PasswordSalt { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public int? SchoolId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public int StatusId { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public int? LatModifiedBy { get; set; }
        public string OneTimePassword { get; set; }
        public Guid? VerificationCode { get; set; }
        public DateTime? VerificationCodeGeneratedOnUtc { get; set; }
        public bool? IsPasswordCreated { get; set; }
	}
     
    public class UserRight
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int RightId { get; set; }
        public int MenuId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
	}
     
    public class VendorMaster
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CityId { get; set; }
        public int StateId { get; set; }
        public int? TypeId { get; set; }
        public int? SchoolId { get; set; }
        public string ContactNumber { get; set; }
        public string Address { get; set; }
        public string PinCode { get; set; }
        public int? StatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public int? LastUpdatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
	}
     
    public class AccountDetail
    {
        public int Id { get; set; }
        public int? PlanId { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? NextPaymentDate { get; set; }
        public DateTime? AppStartDate { get; set; }
        public int SchoolId { get; set; }
        public int? LeftUserCount { get; set; }
        public int? LeftStudentCount { get; set; }
        public int? TotalMessageCount { get; set; }
        public int? LeftMessageCount { get; set; }
        public string WeeklyHolidays { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public int? StatusId { get; set; }
	}
     
    public class AccountFeeDetail
    {
        public int Id { get; set; }
        public int SchoolId { get; set; }
        public DateTime? PaymentDate { get; set; }
        public decimal? PaymentAmount { get; set; }
        public int? PaymentModeId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? StatusId { get; set; }
        public int? PlanId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public int? UpadatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
	}
     
    public class AmazonSetting
    {
        public int Id { get; set; }
        public string BucketName { get; set; }
        public string AccessKey { get; set; }
        public string SecretAccessKey { get; set; }
        public string EncryptionKey { get; set; }
        public int? ModifiedById { get; set; }
        public DateTime? DateModified { get; set; }
	}
     
    public class ApplicationLog
    {
        public Guid ID { get; set; }
        public long? UserID { get; set; }
        public string IpAddress { get; set; }
        public int LogSourceID { get; set; }
        public int? SchoolId { get; set; }
        public string OSInfo { get; set; }
        public string RequestUrl { get; set; }
        public string Module { get; set; }
        public string ClassName { get; set; }
        public string MethodName { get; set; }
        public string ExceptionMessage { get; set; }
        public string StackTrace { get; set; }
        public string InnerExceptionMessage { get; set; }
        public string InnerExceptionStackTrace { get; set; }
        public DateTime LoggedDate { get; set; }
	}
     
    public class ApplicationSetting
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
	}
     
    public class AttendanceMaster
    {
        public long Id { get; set; }
        public int SchoolId { get; set; }
        public int StudentId { get; set; }
        public int SessionId { get; set; }
        public int DayStatusId { get; set; }
        public DateTime Date { get; set; }
        public string Remark { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public int? LastModifiedBy { get; set; }
	}
     
    public class AutomateMessage
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
	}
     
    public class CertificateTemplateMaster
    {
        public int Id { get; set; }
        public string TemplateName { get; set; }
        public int? TemplateTypeId { get; set; }
        public string HtmlTemplate { get; set; }
	}
     
    public class CertificateTemplateSchoolMapping
    {
        public int Id { get; set; }
        public int? CertificateTemplateId { get; set; }
        public int SchoolId { get; set; }
        public int? StatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public int? LastUpdatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
	}
     
    public class Student
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddileName { get; set; }
        public string LastName { get; set; }
        public int? TitleId { get; set; }
        public string RegistrationNumber { get; set; }
        public string Email { get; set; }
        public string StudentMobile { get; set; }
        public string StudentOtherInfo { get; set; }
        public string GuardianAddress { get; set; }
        public string GuardianOccupation { get; set; }
        public string GuardianRelationWithStudent { get; set; }
        public string GuardianMobileNo { get; set; }
        public string GuardianName { get; set; }
        public string MessageMobileNumber { get; set; }
        public string MotherName { get; set; }
        public string FatherName { get; set; }
        public string CorrAddress { get; set; }
        public int? CorrCityId { get; set; }
        public string CorrPinCode { get; set; }
        public string PermanentAddress { get; set; }
        public int? PermanentCityId { get; set; }
        public int? CurrentStateId { get; set; }
        public int? PermanentStateId { get; set; }
        public string ParentMobileNo { get; set; }
        public string ParentContactNo { get; set; }
        public string PermanentPinCode { get; set; }
        public int SchoolId { get; set; }
        public DateTime? DOJ { get; set; }
        public DateTime? DOB { get; set; }
        public DateTime? DOE { get; set; }
        public string Uid { get; set; }
        public int StatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public int? LastUpdatedBy { get; set; }
        public string ImagePath { get; set; }
        public int? RelegionId { get; set; }
        public int? CategoryId { get; set; }
        public string ReasonForExit { get; set; }
        public int? NationlityId { get; set; }
        public int? GenderId { get; set; }
        public Guid? S3key { get; set; }
	}
     
    public class CityMaster
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int StateId { get; set; }
	}
     
    public class ClassMaster
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SchoolId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public int? LastUpdatedBy { get; set; }
        public int? Status { get; set; }
	}
     
    public class EmailSend
    {
        public long ID { get; set; }
        public string MailType { get; set; }
        public string Email { get; set; }
        public long? SentBy { get; set; }
        public DateTime? SentOn { get; set; }
        public bool IsSend { get; set; }
        public string Exception { get; set; }
	}
     
    public class EmployeeTypeMaster
    {
        public int Id { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeGrade { get; set; }
        public int SchoolId { get; set; }
        public int? StatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public int? LastUpdatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
	}
     
    public class ExamMaster
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SchoolId { get; set; }
        public int? SessionId { get; set; }
        public int? StatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public int? LastUpdatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
	}
     
    public class OnlineTestMaster
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SchoolId { get; set; }
        public int? StatusId { get; set; }
        public int? LastUpdatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public TimeSpan? StartTime { get; set; }
        public DateTime? StartDate { get; set; }
        public int? TimeInMinutes { get; set; }
	}
     
    public class ExamSubjectMapping
    {
        public int Id { get; set; }
        public int ExamId { get; set; }
        public int SubjectId { get; set; }
        public DateTime? Date { get; set; }
        public decimal TotalMarks { get; set; }
	}
     
    public class FeeHeadMaster
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsDiscount { get; set; }
        public int FeeHeadTypeId { get; set; }
        public int SchoolId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public int StatusId { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public int? LastModifiedBy { get; set; }
	}
     
    public class OnlineTestSubjectMaster
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? SchoolId { get; set; }
        public int? StatusId { get; set; }
        public int? LastUpdatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
	}
     
    public class FeeHeadMonthMapping
    {
        public int Id { get; set; }
        public int FeeHeadId { get; set; }
        public int? ClassId { get; set; }
        public int? SessionId { get; set; }
        public int? MonthId { get; set; }
        public decimal? Amount { get; set; }
	}
     
    public class FeeTransaction
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public int? SessionId { get; set; }
        public decimal? TotalAmountDeposit { get; set; }
        public decimal? TotalPayableAmount { get; set; }
        public decimal? PreviousBalance { get; set; }
        public decimal? TotalAmount { get; set; }
        public bool? IsSettled { get; set; }
        public decimal? BalanceAmount { get; set; }
        public string Remark { get; set; }
        public DateTime? TransactionDate { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public int? PaymentModeId { get; set; }
        public int? FromMonthId { get; set; }
        public int? ToMonthId { get; set; }
        public int? LastUpdatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public int? StatusId { get; set; }
	}
     
    public class FeeTransactionDetail
    {
        public int Id { get; set; }
        public int FeeTransactionId { get; set; }
        public int FeeHeadMappingId { get; set; }
	}
     
    public class FineDiscountTransactionDetail
    {
        public int Id { get; set; }
        public int FeeHeadId { get; set; }
        public int TransactionId { get; set; }
        public decimal? FeeHeadAmount { get; set; }
	}
     
    public class QuestionAnswerMapping
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public string Answer { get; set; }
        public bool? IsCorrectAnswer { get; set; }
       
        public int SchoolId { get; set; }
        public int StatusId { get; set; }
        public int? LastUpdateBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
	}
     
    public class HolidayMaster
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int SessionId { get; set; }
        public int SchoolId { get; set; }
        public int? LastUpdatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public int? StatusId { get; set; }
	}
     
    public class Inventory
    {
        public int Id { get; set; }
        public int SchoolId { get; set; }
        public int? ItemId { get; set; }
        public string ItemDescription { get; set; }
        public decimal Amount { get; set; }
        public int? VenderId { get; set; }
        public int InvetoryTypeId { get; set; }
        public int? PaymentModeTypeId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? SessionId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public int? LastUpdatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public int? StatusId { get; set; }
	}
     
    public class ItemMaster
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? ExpensesTypeId { get; set; }
        public int? StatusId { get; set; }
        public int SchoolId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? LastUpdatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
	}
     
    public class KeyWordMaster
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int KeyWordId { get; set; }
        public string KeyWordName { get; set; }
	}
     
    public class LogSource
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
	}
     
    public class MenuMaster
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? ParentId { get; set; }
        public int StatusId { get; set; }
        public string ActionName { get; set; }
        public string Controller { get; set; }
        public string IconClass { get; set; }
        public string ClassName { get; set; }
        public bool IsSuperAdmin { get; set; }
        public int MenuOrder { get; set; }
        public bool? IsAjax { get; set; }
	}
     
    public class MessageAutomateMapping
    {
        public int Id { get; set; }
        public int MessageAutomateId { get; set; }
        public int TemplateId { get; set; }
        public int? SchoolId { get; set; }
	}
     
    public class MessageMaster
    {
        public int Id { get; set; }
        public int? SchoolId { get; set; }
        public int? MessageId { get; set; }
        public int? ClassId { get; set; }
        public int? SectionId { get; set; }
        public int? StudentId { get; set; }
        public string Subject { get; set; }
        public DateTime? MessageSentDate { get; set; }
        public TimeSpan? MessageSentTime { get; set; }
        public string Message { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public bool? IsMessageSent { get; set; }
        public int? StatusId { get; set; }
	}
     
    public class NoticeMaster
    {
        public int Id { get; set; }
        public int? NoticeTypeId { get; set; }
        public int? SchoolId { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public int? StatusId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
	}
     
    public class OnlineTestQuestionMaster
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public int? SubjectId { get; set; }
        public string Description { get; set; }
        public int? StatusId { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public int? LastUpdatedBy { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? QuestionTypeId { get; set; }
        public string QuestionType { get; set; }
        public bool? Ismcq { get; set; }
        public int? SchoolId { get; set; }
        public string ImagePath { get; set; }
	}
    
     
    public class PlanMaster
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Detail { get; set; }
        public int? PlanTypeId { get; set; }
        public int? MaxStudentLimit { get; set; }
        public int? MaxUserLimit { get; set; }
        public int? MaxMessageLimit { get; set; }
        public int? StatusId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
	}
     
    public class SchoolMaster
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int? ParentSchoolId { get; set; }
        public string Name { get; set; }
        public int? StateId { get; set; }
        public int CityId { get; set; }
        public int? InstituteTypeId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string PIN { get; set; }
        public string OwnerName { get; set; }
        public string ContactNo { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public int? LastUpdatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public int StatusId { get; set; }
        public string ImagePath { get; set; }
        public Guid? S3key { get; set; }
	}
     
    public class SchoolPlanMapping
    {
        public int Id { get; set; }
        public int SchoolId { get; set; }
        public int PlanId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? StatusId { get; set; }
        public int? LastUpdateBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
	}
     
    public class SectionMaster
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? ClassId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public int Status { get; set; }
        public int? LastUpdatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
	}
     
    public class SessionMaster
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int SchoolId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public int StatusId { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public int? LastModifiedBy { get; set; }
	}
     
    public class StateMaster
    {
        public int Id { get; set; }
        public string Name { get; set; }
	}
     
    public class StudentClassMapping
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public int ClassId { get; set; }
        public int SectionId { get; set; }
        public int SessionId { get; set; }
        public string StudentCode { get; set; }
	}
     
    public class StudentExamMapping
    {
        public int Id { get; set; }
        public int ExamSubjectMappingId { get; set; }
        public int ClassId { get; set; }
        public int SectionId { get; set; }
        public decimal ObtainedMarks { get; set; }
        public int StudentId { get; set; }
        public int? StatusId { get; set; }
        public int? LastUpdateBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
	}
     
}

namespace EduErp.BO.Enumerators
{
	/// <summary>
	/// Enum of "LogSource" table
	/// </summary>		
	public enum LogSources
	{
		Website = 1,
		WebService = 2,
	}

	/// <summary>
	/// Enum of "MenuMaster" table
	/// </summary>		
	public enum MenuMasters
	{
		Home = 1,
		School = 2,
		User = 3,
		Session = 4,
		Class = 5,
		Subject = 6,
		Student = 7,
		Attendance = 8,
		Message = 9,
		Holiday = 10,
		Expenses = 11,
		Section = 12,
		FeeHead = 13,
		FeeTransaction = 14,
		AccountDetail = 15,
		AccountFeeDetail = 16,
		Plan = 17,
		Notification = 19,
		StudentUpload = 20,
		Employee = 21,
		EmployeeType = 22,
		Item = 23,
		Vendor = 24,
		Configuration = 25,
		Exam = 26,
		StudentExamMapping = 27,
		OnlineTest = 28,
		Certificate = 30,
		DownLoadCertificate = 31,
		Report = 32,
		StudentReport = 33,
		FeesReport = 34,
		ExamReport = 35,
		ExpenseReport = 36,
		AttendenceReport = 37,
        OTQuestionAnswerMapping = 38,
        OnlineTestSubject = 39,
        OTExamSubjectMapping = 40,
        OTExamQuestionMapping = 41,
		OnlineTestQuestion = 42,
	 
	}

}
