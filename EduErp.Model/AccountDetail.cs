//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EduErp.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class AccountDetail
    {
        public int Id { get; set; }
        public Nullable<int> PlanId { get; set; }
        public Nullable<System.DateTime> DueDate { get; set; }
        public Nullable<System.DateTime> NextPaymentDate { get; set; }
        public Nullable<System.DateTime> AppStartDate { get; set; }
        public int SchoolId { get; set; }
        public Nullable<int> LeftUserCount { get; set; }
        public Nullable<int> LeftStudentCount { get; set; }
        public Nullable<int> TotalMessageCount { get; set; }
        public Nullable<int> LeftMessageCount { get; set; }
        public string WeeklyHolidays { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<int> StatusId { get; set; }
    }
}
