//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EduErp.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class FeeHeadMonthMapping
    {
        public FeeHeadMonthMapping()
        {
            this.FeeTransactionDetails = new HashSet<FeeTransactionDetail>();
        }
    
        public int Id { get; set; }
        public int FeeHeadId { get; set; }
        public Nullable<int> ClassId { get; set; }
        public Nullable<int> SessionId { get; set; }
        public Nullable<int> MonthId { get; set; }
        public Nullable<decimal> Amount { get; set; }
    
        public virtual FeeHeadMaster FeeHeadMaster { get; set; }
        public virtual ICollection<FeeTransactionDetail> FeeTransactionDetails { get; set; }
    }
}
