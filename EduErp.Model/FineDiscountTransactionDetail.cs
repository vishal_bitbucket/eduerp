//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EduErp.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class FineDiscountTransactionDetail
    {
        public int Id { get; set; }
        public int FeeHeadId { get; set; }
        public int TransactionId { get; set; }
        public Nullable<decimal> FeeHeadAmount { get; set; }
    
        public virtual FeeHeadMaster FeeHeadMaster { get; set; }
        public virtual FeeTransaction FeeTransaction { get; set; }
    }
}
