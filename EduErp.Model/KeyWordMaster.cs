//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EduErp.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class KeyWordMaster
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int KeyWordId { get; set; }
        public string KeyWordName { get; set; }
    }
}
