﻿using System;
using System.Collections.Generic;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
    public class VendorService : BaseLayer
    {

        public static ServiceResult<bool> AddVendor(VendorBo model)
        {
            try
            {
                VendorModule.AddVendor(model);
                return Responses.SuccessBoolResult;
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }

        public static ServiceResult<List<VendorBo>> Getvendors(int schoolId)
        {
            try
            {
                var res = VendorModule.GetVendors(schoolId);
                return Responses.SuccessDataResult(res);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<VendorBo>>(null, ex.Message);
            }
        }

        public static ServiceResult<VendorBo> GetVendorById(int id)
        {
            try
            {
                var res = VendorModule.GetVendor(id);
                return Responses.SuccessDataResult(res);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<VendorBo>(null, ex.Message);
            }
        }

        public static ServiceResult<bool> UpdateVedor(VendorBo model)
        {
            try
            {
                VendorModule.UpdateVendor(model);
                return Responses.SuccessBoolResult;
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }

        public static ServiceResult<bool> UpdateStatus(int id, int status, int userId)
        {
            try
            {
                var result = VendorModule.UpdateStatus(id, status, userId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;


            }
        }
    }
}
