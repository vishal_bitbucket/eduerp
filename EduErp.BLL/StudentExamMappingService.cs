﻿using System;
using System.Collections.Generic;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
   public class StudentExamMappingService:BaseLayer
    {
       public static ServiceResult<bool> AddStudentExam(StudentExamMappingBo model)
       {
           try
           {
               StudentExamModule.AddStudentExamMapping(model);
               return Responses.SuccessBoolResult;
           }
           catch (Exception ex)
           {
               
               AddApplicationLog(ex);
               return Responses.FailureBoolResult;
           }
       }
       public static ServiceResult<bool> UpdateStudentExam(StudentExamMappingBo model)
       {
           try
           {
               StudentExamModule.UpdateStudentExam(model);
               return Responses.SuccessBoolResult;
           }
           catch (Exception ex)
           {

               AddApplicationLog(ex);
               return Responses.FailureBoolResult;
           }
       }
       public static ServiceResult<List<Common>> ExamType(int schoolId)
       {
           try
           {
               var result = StudentExamModule.ExamTypeList(schoolId);
               return Responses.SuccessDataResult(result);
           }
           catch (Exception ex)
           {
               AddApplicationLog(ex);
               return Responses.FailureDataResult<List<Common>>(null, ex.Message);
           }
       }


       public static ServiceResult<List<StudentExamList>> GetExamMappingList(int examTypeId, int studentId, int schoolId)
       {
           try
           {
               var result = StudentExamModule.GetExamMappingDetail(examTypeId,studentId,schoolId);
               return Responses.SuccessDataResult(result);
           }
           catch (Exception ex)
           {
               AddApplicationLog(ex);
               return Responses.FailureDataResult<List<StudentExamList>>(null, ex.Message);
           }

       }

       public static ServiceResult<List<StudentExamMappingBo>> GetStudentExamMappingDetails(int schoolId)
       {
           try
           {
               var result = StudentExamModule.GetStudentExamList(schoolId);
               return Responses.SuccessDataResult(result);
           }
           catch (Exception ex)
           {
               AddApplicationLog(ex);
               return Responses.FailureDataResult<List<StudentExamMappingBo>>(null, ex.Message);
           }

       }

       public static ServiceResult<StudentExamMappingBo> GetStudentExamDetailById(int schoolId, int examId,int studentId)
       {
           try
           {
               var result = StudentExamModule.GetStudentExamDetailById(schoolId, examId, studentId);
              // result.StudentExamLists = StudentExamModule.GetExamMappingDetail(schoolId, examId, studentId);
               return Responses.SuccessDataResult(result);
           }
           catch (Exception ex)
           {
               
               AddApplicationLog(ex);
               return Responses.FailureDataResult<StudentExamMappingBo>(null, ex.Message);
           } 
       }

       public static ServiceResult<List<StudentExamList>>GetExamMappingDetailForEdit(int schoolId,int studentId,int examId)
       {
           try
           {
               var resut = StudentExamModule.GetStudentExamMappingList(schoolId,studentId,examId);
               return Responses.SuccessDataResult(resut);
           }
           catch (Exception ex)
           {
               
               AddApplicationLog(ex);
               return Responses.FailureDataResult<List<StudentExamList>>(null, ex.Message);
           }
       }

       public static ServiceResult<bool> UpdateStatus(int id, int status, int userId)
       {
           try
           {
               var result = StudentExamModule.UpdateStatus(id, status, userId);
               return Responses.SuccessDataResult(result);
           }
           catch (Exception ex)
           {
               AddApplicationLog(ex);
               return Responses.FailureBoolResult;


           }
       }
    }
}
