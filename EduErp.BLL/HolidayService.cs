﻿using System;
using System.Collections.Generic;
using EduErp.Base;
using EduErp.BLL.Base;


namespace EduErp.BLL
{
   public class HolidayService:BaseLayer
    {
       public static ServiceResult<int> AddHoliday(BO.HolidayBo model)
       {
           try
           {
               var result= DAL.HolidayModule.AddHoliday(model);
               return Responses.SuccessDataResult(result);
           }
           catch (Exception ex)
           {
               
               AddApplicationLog(ex);
               return Responses.FailureDataResult(0);
           }
       }

       public static ServiceResult<List<BO.HolidayBo>> GetHolidays(int schoolId)
       {
           try
           {
               var result = DAL.HolidayModule.GetHolidays(schoolId);
               return Responses.SuccessDataResult(result);
           }
           catch (Exception ex)
           {
               AddApplicationLog(ex);
               return Responses.FailureDataResult<List<BO.HolidayBo>>(null, ex.Message);
           }
       }

       public static ServiceResult<BO.HolidayBo> GetHolidayById(int id)
       {
           try
           {
               var result = DAL.HolidayModule.GetHolidayById(id);
               return Responses.SuccessDataResult(result);
           }
           catch (Exception ex)
           {
               AddApplicationLog(ex);
               return Responses.FailureDataResult<BO.HolidayBo>(null, ex.Message);
           }
       }
       public static ServiceResult<bool> UpdateHoliday(BO.HolidayBo model)
       {
           try
           {
               var result = DAL.HolidayModule.UpdateHoliday(model);
               return Responses.SuccessDataResult(result);
           }
           catch (Exception ex)
           {
               AddApplicationLog(ex);
               return Responses.FailureBoolResult;
           }
       }
       public static ServiceResult<bool> UpdateStatus(int id, int statusId, int userId)
       {
           try
           {
               var result = DAL.HolidayModule.UpdateStatus(id, statusId, userId);
               return Responses.SuccessDataResult(result);
           }
           catch (Exception ex)
           {
               AddApplicationLog(ex);
               return Responses.FailureBoolResult;
           }
       }

       public static ServiceResult<List<BO.Common>> GetSessionList(int schoolId)
       {
           try
           {
              var result=DAL.HolidayModule.GetSessionList(schoolId);
               return Responses.SuccessDataResult(result);
           }
           catch (Exception ex)
           {
               AddApplicationLog(ex);
               return Responses.FailureDataResult<List<BO.Common>>(null, ex.Message);

           }
       }

    }
}
