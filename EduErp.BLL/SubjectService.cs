﻿using System;
using System.Collections.Generic;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
    public class SubjectService : BaseLayer
    {
        public static ServiceResult<int> AddSubject(SubjectBo model)
        {
            try
            {
              var result=  SubjectModule.AddSuject(model);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult(0,ex.Message);
            }
        }

        //public static ServiceResult<List<ListOfClass>> GetClass(int schoolId)
        //{
        //    try
        //    {
        //        var classList = SubjectModule.GetClassList(schoolId);
        //        return Responses.SuccessDataResult(classList);
        //    }
        //    catch (Exception ex)
        //    {
        //        AddApplicationLog(ex);
        //        return Responses.FailureDataResult<List<ListOfClass>>(null, ex.Message);
        //    }
        //}
        public static ServiceResult<List<ClassMaster>> GetClasses(int schoolId)
        {
            try
            {
                var classes = SubjectModule.GetClasses(schoolId);
                return Responses.SuccessDataResult(classes);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<ClassMaster>>(null, ex.Message);
            }
        }

        public static ServiceResult<List<ClassMaster>> GetClassesBySubjectId(int subjectId)
        {
            try
            {
                var classes = SubjectModule.GetClassesBySubjectId(subjectId);
                return Responses.SuccessDataResult(classes);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<ClassMaster>>(null, ex.Message);
            }
        }

        public static ServiceResult<SubjectBo> GetSubjects(int schoolId)
        {
            try
            {

                var res = new List<int>();
                var subject = SubjectModule.GetSubjects(schoolId);
                if (subject != null && subject.SubjectBoList.Count > 0)
                    res = SubjectModule.GetClassId(subject.SubjectBoList[0].SubjectId);
                if (subject != null)
                    subject.ClassIdes = res.ToArray();


                return Responses.SuccessDataResult(subject);

            }

            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<SubjectBo>(null, ex.Message);
            }
        }

        public static ServiceResult<bool> UpdateSubject(SubjectBo model)
        {
            try
            {
                SubjectModule.UpdateSubject(model);
                return Responses.SuccessBoolResult;
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }

        public static ServiceResult<SubjectBo> GetSubjectById(int id)
        {
            try
            {
                var result = SubjectModule.GetSubjectById(id);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<SubjectBo>(null, ex.Message);

            }
        }

        public static ServiceResult<bool> UpdateStatus(int id, int status, int userId)
        {
            try
            {
                var result = SubjectModule.UpdateStatus(id, status, userId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;


            }

        }
    }
}
