﻿using System;
using System.Collections.Generic;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
    public class ClassService : BaseLayer
    {

        public static ServiceResult<int> AddClass(ClassBo model)
        {
            try
            {
                var result=ClassModule.AddClass(model);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult(0,ex.Message);

            }
        }

        public static ServiceResult<List<ClassBo>> GetClasses(int schoolId)
        {
            try
            {
                var result = ClassModule.GetClasses(schoolId);
                return Responses.SuccessDataResult(result);

            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<ClassBo>>(null, ex.Message);
            }

        }

        public static ServiceResult<bool> UpdateClass(ClassBo model)
        {
            try
            {
                ClassModule.UpdateClass(model);
                return Responses.SuccessBoolResult;
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;

            }
        }

        public static ServiceResult<ClassBo> GetClassById(int id)
        {
            try
            {
                var result = ClassModule.GetClassById(id);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<ClassBo>(null, ex.Message);

            }
        }

        public static ServiceResult<bool> UpdateStatus(int id, int status, int userId)
        {
            try
            {
                var result = ClassModule.UpdateStatus(id, status, userId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;


            }

        }


    }
}
