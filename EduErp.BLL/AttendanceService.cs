﻿using System;
using System.Collections.Generic;
using EduErp.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
    public class AttendanceService : Base.BaseLayer
    {
        public static ServiceResult<List<Common>> GetDayStatus()
        {
            try
            {
                var users = DAL.AttendanceModule.GetDayStatus();
                return Responses.SuccessDataResult(users);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }
        public static ServiceResult<List<Common>> GetMonthList()
        {
            try
            {
                var monthes = AttendanceModule.GetMonthList();
                return Responses.SuccessDataResult(monthes);

            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }
        public static ServiceResult<AttendanceTemplateBo> GetStudentAttendance(int schoolId, int sessionId, int classId, int sectionId)
        {
            try
            {
                var users = DAL.AttendanceModule.GetStudentAttendance(schoolId, sessionId, classId, sectionId);
                return Responses.SuccessDataResult(users);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<AttendanceTemplateBo>(null, ex.Message);
            }
        }
        //public static ServiceResult<AttendanceTemplateBo> GetStudentAttendances(int schoolId, int sessionId, int classId, int sectionId)
        //{
        //    try
        //    {
        //        var users = DAL.AttendanceModule.GetStudentAttendances(schoolId, sessionId, classId, sectionId);
        //        return Responses.SuccessDataResult(users);
        //    }
        //    catch (Exception ex)
        //    {
        //        AddApplicationLog(ex);
        //        return Responses.FailureDataResult<AttendanceTemplateBo>(null, ex.Message);
        //    }
        //}

        public static ServiceResult<bool> AddAttendance(AttendanceBo model,int schoolId)
        {
            try
            {
                var users = DAL.AttendanceModule.AddAttendance(model,schoolId);
                return Responses.SuccessDataResult(users);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult(false, ex.Message);
            }
        }

        public static ServiceResult<List<AttendanceList>> GetAllAttendace(DateTime fromDate, DateTime toDate, int schoolId, int sessionId, int classId, int sectionId)
        {
            try
            {
                var users = DAL.AttendanceModule.GetAllAttendace(fromDate, toDate, schoolId, sessionId, classId, sectionId);
                return Responses.SuccessDataResult(users);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<AttendanceList>>(null, ex.Message);
            }
        }

        public static ServiceResult<bool> UpdateAttendance(AttendanceMaster model)
        {
            try
            {
                var users = DAL.AttendanceModule.UpdateAttendance(model);
                return Responses.SuccessDataResult(users);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult(false, ex.Message);
            }
        }
    }
}
