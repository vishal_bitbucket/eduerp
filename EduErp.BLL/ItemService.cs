﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
     public class ItemService:BaseLayer
    {

         public static ServiceResult<int> AddItem(ItemBo model)
         {
             try
             {
                var res=ItemModule.AddItem(model);
                 return Responses.SuccessDataResult(res);
             }
             catch (Exception ex)
             {
                 
                 AddApplicationLog(ex);
                 return Responses.FailureDataResult(0,ex.Message);
             }
         }
         public static ServiceResult<List<ItemBo>> GetItemes(int schoolId)
         {
             try
             {
                 var res = ItemModule.GetItemes(schoolId);
                 return Responses.SuccessDataResult(res);
             }
             catch (Exception ex)
             {
                 AddApplicationLog(ex);
                 return Responses.FailureDataResult<List<ItemBo>>(null, ex.Message);
             }
         }

         public static ServiceResult<ItemBo> GetitemById(int id)
         {
             try
             {
                 var res = ItemModule.GetItem(id);
                 return Responses.SuccessDataResult(res);
             }
             catch (Exception ex)
             {
                 AddApplicationLog(ex);
                 return Responses.FailureDataResult<ItemBo>(null, ex.Message);
             }
         }
         public static ServiceResult<bool> UpdateItem(ItemBo model)
         {
             try
             {
                 ItemModule.UpdateItem(model);
                 return Responses.SuccessBoolResult;
             }
             catch (Exception ex)
             {
                 AddApplicationLog(ex);
                 return Responses.FailureBoolResult;
             }
         }
         public static ServiceResult<bool> UpdateStatus(int id, int status, int userId)
         {
             try
             {
                 var result = ItemModule.UpdateStatus(id, status, userId);
                 return Responses.SuccessDataResult(result);
             }
             catch (Exception ex)
             {
                 AddApplicationLog(ex);
                 return Responses.FailureBoolResult;


             }

         }
    }
}
