﻿using System;
using System.Collections.Generic;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
    public class FeeTransactionService : BaseLayer
    {
        public static ServiceResult<List<Common>> GetClasses(int schoolId)
        {
            try
            {
                var classes = FeeTransactionModule.GetClasses(schoolId);
                return Responses.SuccessDataResult(classes);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }

        public static ServiceResult<List<Common>> GetMonthList()
        {
            try
            {
                var monthes = FeeTransactionModule.GetMonthList();
                return Responses.SuccessDataResult(monthes);

            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }

        public static ServiceResult<List<Common>> GetFineFeeHeadsList(int schoolId)
        {
            try
            {
                var list = FeeTransactionModule.GetFineTypeFeeHeads(schoolId);
                return Responses.SuccessDataResult(list);

            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }
        public static ServiceResult<List<Common>> GetDiscountFeeHeadsList(int schoolId)
        {
            try
            {
                var list = FeeTransactionModule.GetDiscountTypeFeeHeads(schoolId);
                return Responses.SuccessDataResult(list);

            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }
        public static ServiceResult<List<Common>> GetPaymentModeList()
        {
            try
            {
                var list = FeeTransactionModule.GetPaymentModeList();
                return Responses.SuccessDataResult(list);

            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }


        public static ServiceResult<List<Common>> GetSections(int classId)
        {
            try
            {
                var sections = StudentModule.GetSections(classId);
                return Responses.SuccessDataResult(sections);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }

        public static ServiceResult<bool> UpdateStatus(int id, int statusId, int userId)
        {
            try
            {
                var result = FeeTransactionModule.UpdateStatus(id, statusId, userId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;


            }

        }

        public static ServiceResult<List<Common>> GetStudentsBySectionId(int sectionId, int sessionId)
        {
            try
            {
                var students = FeeTransactionModule.GetStudentBySectionId(sectionId, sessionId);
                return Responses.SuccessDataResult(students);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }
        public static ServiceResult<decimal> GetPreviousAmount(int studentId)
        {
            try
            {
                var result = FeeTransactionModule.GetPreviousAmount(studentId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<decimal>(0);
            }
        }

        public static ServiceResult<FeeTransactionTemplateBo> GetComponent(int sessionId, int schoolId, int fromMonthId, int toMonthId, int classId, int studentId)
        {
            try
            {

                var components = FeeTransactionModule.GetComponent(sessionId, schoolId, fromMonthId, toMonthId, classId, studentId);
                return Responses.SuccessDataResult(components);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<FeeTransactionTemplateBo>(null, ex.Message);
            }
        }

        public static ServiceResult<FeeTransactionBo> GetReceiptData(int transactionId, int schoolId)
        {
            try
            {
                FeeTransactionBo result = FeeTransactionModule.GetFeeTransactionByTransactionId(transactionId);

                FeeTransactionBo model = new FeeTransactionBo
                {
                    SessionId = result.SessionId,
                    ClassId = result.ClassId,
                    FromMonthId = result.FromMonthId,
                    ToMonthId = result.ToMonthId,
                    TotalAmountDeposit = result.TotalAmountDeposit,
                    StudentId = result.StudentId,
                    IsBalancedSettled = result.IsBalancedSettled,
                    BalanceAmount = result.BalanceAmount,
                    TransactionDate = result.TransactionDate,
                    Remark = result.Remark,
                    TotalClassGeneralAmount = result.TotalClassGeneralAmount,
                    CreatedOn = result.CreatedOn,
                    CreatedBy = result.CreatedBy,
                    StatusId = result.StatusId,
                    SessionName = result.SessionName,
                    SectionName = result.SectionName,
                    FromMonthName = result.FromMonthName,
                    ToMonthName = result.ToMonthName,
                    ClassName = result.ClassName,
                    StudentName = result.StudentName,
                    //FirstName = result.FirstName,
                    //LastName = result.LastName,
                    PaymentModeName = result.PaymentModeName,
                    TotalPayableAmount = result.TotalPayableAmount,
                    PreviousBalance = result.PreviousBalance,
                    S3key = result.S3key,
                    Id = result.Id

                };
                //model=result;
                var components = FeeTransactionModule.GetReceiptData(transactionId, model.StudentId);
                model.FeeTransactionTemplateBo = components;
                return Responses.SuccessDataResult(model);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<FeeTransactionBo>(null, ex.Message);

            }

        }

        public static ServiceResult<FeeTransactionBo> GetTransactionByTransactionIdForEdit(int transactionId, int schoolId)
        {

            try
            {
                FeeTransactionBo result = FeeTransactionModule.GetFeeTransactionByTransactionId(transactionId);

                FeeTransactionBo model = new FeeTransactionBo
                {
                    SessionId = result.SessionId,
                    ClassId = result.ClassId,
                    FromMonthId = result.FromMonthId,
                    ToMonthId = result.ToMonthId,
                    TotalAmountDeposit = result.TotalAmountDeposit,
                    StudentId = result.StudentId,
                    IsBalancedSettled = result.IsBalancedSettled,
                    BalanceAmount = result.BalanceAmount,
                    TransactionDate = result.TransactionDate,
                    Remark = result.Remark,
                    TotalAmount = result.TotalAmount,
                    CreatedOn = result.CreatedOn,
                    CreatedBy = result.CreatedBy,
                    StatusId = result.StatusId,
                    SessionName = result.SessionName,
                    SectionName = result.SectionName,
                    FromMonthName = result.FromMonthName,
                    ToMonthName = result.ToMonthName,
                    ClassName = result.ClassName,
                    
                    StudentName = result.StudentName,
                    //FirstName = result.FirstName,
                    PaymentModeId = result.PaymentModeId,
                    TotalPayableAmount = result.TotalPayableAmount,
                    PreviousBalance = result.PreviousBalance,
                    TotalClassGeneralAmount = result.TotalClassGeneralAmount

                };
                //model=result;
                FeeTransactionTemplateBo components = FeeTransactionModule.GetComponentForEdit(model.SessionId, schoolId, model.FromMonthId ?? 0, model.ToMonthId ?? 0, model.ClassId, model.StudentId);
                model.FeeTransactionTemplateBo = components;
                model.FeeTransactionTemplateBo.FineDetailList = FeeTransactionModule.GetFineList(transactionId);
                model.FeeTransactionTemplateBo.DiscountDetailList = FeeTransactionModule.GetDiscountList(transactionId);

                List<int> list = FeeTransactionModule.GetFeeTransactionMappingId(transactionId);


                // model.FeeTransactionTemplateBo.FeetransactionMappingIdList=list;
                foreach (ClassByComponentList a in model.FeeTransactionTemplateBo.ClassByComponentLists)
                {
                    foreach (int b in list)
                    {
                        if (a.FeeHeadMappingId == b)
                        {
                            a.Selected = true;
                        }


                    }
                }
                foreach (GeneralByComponentList x in model.FeeTransactionTemplateBo.GeneralByComponentLists)
                {
                    foreach (int y in list)
                    {
                        if (x.FeeHeadMappingId == y)
                        {
                            x.Selected = true;
                        }


                    }
                }

                return Responses.SuccessDataResult(model);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<FeeTransactionBo>(null, ex.Message);
            }
        }

        public static ServiceResult<List<BO.MessageAutomateMappingBo>> GetAutomateMessageIds(int schoolId)
        {
            try
            {
                var result = FeeTransactionModule.GetAutomateMessageId(schoolId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<BO.MessageAutomateMappingBo>>(null, ex.Message);
            }
        }

        public static ServiceResult<FeeTransactionTemplateBo> GetFeeTransactionDetailByClassSection(int sectionId, int classId, int schoolId, int sessionId)
        {
            try
            {

                var transactions = FeeTransactionModule.GetFeeTransactionDetailByClassSection(sectionId, classId, schoolId, sessionId);
                return Responses.SuccessDataResult(transactions);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<FeeTransactionTemplateBo>(null, ex.Message);
            }
        }

        public static ServiceResult<FeeTransactionTemplateBo> GetFeeTransactionDetailByStudentId(int studentId, int sessionId)
        {
            try
            {
                var transactionDetail = FeeTransactionModule.GetFeeTransactionDetailByStudentId(studentId, sessionId);
                return Responses.SuccessDataResult(transactionDetail);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<FeeTransactionTemplateBo>(null, ex.Message);
            }

        }


        public static ServiceResult<int> AddFeeTransaction(FeeTransactionBo model, int schoolId)
        {

            try
            {
                var transactionId = FeeTransactionModule.AddFeeTransaction(model, schoolId);

                return Responses.SuccessDataResult(transactionId);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult(0, ex.Message);
            }
        }


        public static ServiceResult<bool> UpdateFeeTransaction(FeeTransactionBo model)
        {
            try
            {

                FeeTransactionModule.UpdateComponent(model);
                return Responses.SuccessBoolResult;
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }
        //public static ServiceResult<FeeTransactionTemplateBo> GetGeneralByComponent(int sessionId, int schoolId, int fromMonthId, int toMonthId, int classId)
        //{
        //    try
        //    {

        //        var components = DAL.FeeTransactionModule.GetGeneralrByComponent(sessionId, schoolId, fromMonthId, toMonthId, classId);
        //        return Responses.SuccessDataResult(components);
        //    }
        //    catch (Exception ex)
        //    {

        //        AddApplicationLog(ex);
        //        return Responses.FailureDataResult<FeeTransactionTemplateBo>(null, ex.Message);
        //    }
        //}
    }
}
