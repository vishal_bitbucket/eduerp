﻿using System;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;
using System.Collections.Generic;

namespace EduErp.BLL
{
    public class PlanService : BaseLayer
    {
        public static ServiceResult<bool> AddPlan(PlanBo model)
        {
            try
            {
                PlanModule.AddPlan(model);
                return Responses.SuccessBoolResult;

            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }

        public static ServiceResult<bool> UpdatePlan(PlanBo model)
        {
            try
            {
                PlanModule.UpdatePlan(model);
                return Responses.SuccessBoolResult;
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }
        public static ServiceResult<PlanBo> GetPlan(int id)
        {

            try
            {
                var result = PlanModule.GetPlan(id);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<PlanBo>(null, ex.Message);
            }
        }

        public static ServiceResult<bool> UpdateStatus(int id, int status, int userId)
        {
            try
            {
                var result = PlanModule.UpdateStatus(id, status, userId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;


            }

        }

        public static ServiceResult<List<Common>> GetPlanType()
        {
            try
            {
                var planList = PlanModule.GetPlanType();
                return Responses.SuccessDataResult(planList);
            }
            catch (Exception ex)
            {
                
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }
        public static ServiceResult<List<PlanBo>> GetPlans()
        {
            try
            {
                var result = PlanModule.GetPlans();
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<PlanBo>>(null, ex.Message);

            }
        }
    }
}
