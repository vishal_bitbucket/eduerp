﻿using System;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
    public class AccountService : BaseLayer
    {
        public static ServiceResult<LoginResult> AuthenticateUser(string email, string password)
        {
            try
            {
                AuthenticateUserBo user;
                string token;

                var messageType = AccountModule.AuthenticateUser(email, password, ClientIpAddress, out user, out token).Result;
                switch (messageType)
                {
                    case AuthMessageType.Success:
                        break;
                    case AuthMessageType.UserNotFound:
                    case AuthMessageType.PasswordDoNotMatch:
                    case AuthMessageType.PasswordNotCreated:
                        return Responses.SuccessDataResult<LoginResult>(null, MessageType.InvalidUser);
                    case AuthMessageType.AccountNotVerified:
                        return Responses.SuccessDataResult<LoginResult>(null, MessageType.AccountNotVerified);
                    case AuthMessageType.SubscriptionExpired:
                        return Responses.SuccessDataResult<LoginResult>(null, MessageType.SubscriptionExpired);
                    case AuthMessageType.AccountNotApproved:
                        return Responses.SuccessDataResult<LoginResult>(null, MessageType.AccountNotApproved);
                    case AuthMessageType.AccountInactive:
                        return Responses.SuccessDataResult<LoginResult>(null, MessageType.AccountInactive);
                    case AuthMessageType.AccountDeleted:
                        return Responses.SuccessDataResult<LoginResult>(null, MessageType.InvalidUser);
                    case AuthMessageType.AccessDeniedToProduct:
                        return Responses.SuccessDataResult<LoginResult>(null, MessageType.AccessDeniedToProduct);
                    case AuthMessageType.CasServerError:
                        return Responses.SuccessDataResult<LoginResult>(null, MessageType.CASServerError);
                    default:
                        return Responses.SuccessDataResult<LoginResult>(null, MessageType.Failed);
                }
                //if (user.ProfilePictureSmall == null) user.ProfilePictureSmall = GetDefaultProfileImage();
                var result = new LoginResult() { User = user };
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<LoginResult>(null, ex.Message);
            }
        }
    }
}
