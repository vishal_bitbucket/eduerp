﻿using System;
using System.Collections.Generic;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
    public class MessageService : BaseLayer
    {
        public static ServiceResult<int> AddMessage(MessageBo model)
        {
            try
            {
                var result=MessageModule.AddMessage(model);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult(0);
            }

        }

        public static ServiceResult<List<MessageBo>> GetMessages(int schoolId)
        {
            try
            {
                var result = MessageModule.GetMessages(schoolId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<MessageBo>>(null, ex.Message);
            }
        }

        public static ServiceResult<List<Common>> GetTemplateList()
        {
            try
            {
                var result = MessageModule.GetTemplateList();
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
                
            }
        }
        public static ServiceResult<int> LeftMessage(int schoolId)
        {
            try
            {
                var result = MessageModule.LeftMessage(schoolId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                
                AddApplicationLog(ex);
                return Responses.FailureDataResult(0, ex.Message);
            }
        }
        public static ServiceResult<string> GetNumberByStudentId(int studentId)
        {
            try
            {
                string number = MessageModule.GetNumberByStudentId(studentId);
                return Responses.SuccessDataResult(number);
            }
            catch (Exception ex)
            {
                
                AddApplicationLog(ex);
                return Responses.FailureDataResult<string>(null, ex.Message);
            }
        }
        public static ServiceResult<List<StudentList>> GetStudentsForMessage(int classId, int sectionId, int schoolId)
        {
            try
            {
                var result = MessageModule.GetStudentsForMessage(classId, sectionId, schoolId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<StudentList>>(null, ex.Message);

            }
        }


        public static ServiceResult<List<Common>> GetClasses(int schoolId)
        {
            try
            {
                var classes = MessageModule.GetClasses(schoolId);
                return Responses.SuccessDataResult(classes);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }
        public static ServiceResult<List<Common>> GetStudentsBySectionId(int sectionId)
        {
            try
            {
                var students = MessageModule.GetStudentBySectionId(sectionId);
                return Responses.SuccessDataResult(students);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }

        public static ServiceResult<List<Common>> GetSections(int classId)
        {
            try
            {
                var sections = MessageModule.GetSections(classId);
                return Responses.SuccessDataResult(sections);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }

        public static ServiceResult<bool> UpdateStatus(int id, int statusId, int userId)
        {
            try
            {
                var result = MessageModule.UpdateStatus(id, statusId, userId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;


            }
        }

    }
}
