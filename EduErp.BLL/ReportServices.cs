﻿using System;
using System.Collections.Generic;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
    public class ReportServices : BaseLayer
    {
        public static ServiceResult<List<Common>> GetPayStatus()
        {
            try
            {
                var result = ReportModule.GetPayStatus();
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }
        public static ServiceResult<GendderWiseStudentReport> GetStudentStatusChartList(int schoolId)
        {
            try
            {

                var result = DAL.ReportModule.GetStudentGenderDetail(schoolId);
                return Responses.SuccessDataResult(result);
            }
            catch(Exception ex) {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<GendderWiseStudentReport>(null, ex.Message);
            }
        
        }
        public static ServiceResult<CategoryWiseStudentReport> GetStudentCategorywiseChartList(int schoolId)
        {
            try
            {

                var result = DAL.ReportModule.GetStudentCategoryDetail(schoolId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<CategoryWiseStudentReport>(null, ex.Message);
            }

        }
        public static ServiceResult<TypeWiseExpensesReport> GetTypewiseExpensesChartList(int sessionId, int schoolId)
        {
            try
            {

                var result = DAL.ReportModule.GetTypeWiseExpensesDetail(sessionId,schoolId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<TypeWiseExpensesReport>(null, ex.Message);
            }

        }

        public static ServiceResult<TypeWiseExamReport> GetTypeExamWiseChartList(int sessionId, int schoolId,int ExamId)
        {
            try
            {

                var result = DAL.ReportModule.GetTypeExamWiseChartList(sessionId, schoolId, ExamId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<TypeWiseExamReport>(null, ex.Message);
            }

        }
        public static ServiceResult<TypeStudentWiseExamReport> GetTypeStudentWiseChartList(int sessionId, int schoolId, int ExamId,int studentId)
        {
            try
            {

                var result = DAL.ReportModule.GetTypeStudentWiseChartList(sessionId, schoolId, ExamId, studentId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<TypeStudentWiseExamReport>(null, ex.Message);
            }

        }
        public static ServiceResult<SessionWiseStudentReport> GetStudentSessionwiseChartList(int schoolId)
        {
            try
            {

                var result = DAL.ReportModule.GetStudentSessionWise(schoolId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<SessionWiseStudentReport>(null, ex.Message);
            }

        }

        public static ServiceResult<List<ExpensesBo>> GetExpenseListReport(int schoolId, int sessionId, int expenseId, int fromMonthId, int toMonthId)
        {
            try
            {

                var result = ReportModule.GetExpenseDetail(schoolId, sessionId, expenseId, fromMonthId, toMonthId);
                return Responses.SuccessDataResult < List<ExpensesBo>>(result);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<ExpensesBo>>(null, ex.Message);
            }
        }
        public static ServiceResult<List<ExamBo>> GetExamListReport(int schoolId,int sessionId, int examId,int subjectId, int classId, int sectionId,int studentId)
        {
            try
            {

                var result = ReportModule.GetExamDetail(schoolId, sessionId, examId, subjectId, classId, sectionId, studentId);
                return Responses.SuccessDataResult<List<ExamBo>>(result);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<ExamBo>>(null, ex.Message);
            }
        }
        public static ServiceResult<List<Common>> GetMonths()
        {
            try
            {
                var result = ReportModule.GetMonths();
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }

        public static ServiceResult<FeeTransactionTemplateBo> GetFeeStatus(int schoolId, int sessionId, int classId, int sectionId, int fromMonthId, int toMonthId, int PayStatusId)
        {
            try
            {
                var result = ReportModule.GetFeeStatusDetail(schoolId, sessionId, classId, sectionId, fromMonthId, toMonthId, PayStatusId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<FeeTransactionTemplateBo>(null, ex.Message);
            }


        }


        public static ServiceResult<List<Common>> GetAdmissionStatus()
        {
            try
            {
                var result = ReportModule.GetAdmissionStatus();
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }
        public static ServiceResult<List<Common>> Getclasses(int schoolId)
        {
            try
            {
                var result = ReportModule.GetClasses(schoolId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }
        public static ServiceResult<List<Common>> GetSubjectByExamId(int examId,int schoolId)
        {
            try
            {
                var result = ReportModule.GetSubjectsByExamId(examId,schoolId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }
        public static ServiceResult<List<Common>> GetClassBySubjectId(int subjectId, int schoolId)
        {
            try
            {
                var result = ReportModule.GetClassBySubjectId(subjectId, schoolId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }
       
        public static ServiceResult<List<Common>> GetExams(int schoolId)
        {
            try
            {
                var result = ReportModule.GetExams(schoolId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }
        public static ServiceResult<List<Common>> GetResultStatus()
        {
            try
            {
                var result = ReportModule.GetResultStatus();
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }
    }
}
