﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;
using ExpertXls.ExcelLib;
using System.Data.OleDb;
using System.Text;

namespace EduErp.BLL
{
    public class StudentUploadService : BaseLayer
    {
        public static void ExportToExcelFromDataTable(DataTable objDt, string reportName)
        {
            const int headerColumnPosition = 1;
            var fileName = String.Format("({0}).xls", reportName + DateTime.Now.ToShortDateString());
            var workbook = new ExcelWorkbook
            {
                LicenseKey = ConfigurationManager.AppSettings.Get("ExpertExcelLicenseKey")
            };
            ExcelWorksheet wsExportDataWorksheet = workbook.Worksheets[0];

            wsExportDataWorksheet.LoadDataTable(objDt, headerColumnPosition, 1, true);
            wsExportDataWorksheet.AutofitColumns();
            wsExportDataWorksheet.AutofitRows();
            HttpResponse response = HttpContext.Current.Response;

            response.Clear();
            response.ContentType = "application/vnd.ms-excel";
            response.AddHeader("Content-Disposition", "attachment;Filename=" + fileName);
            workbook.Save(response.OutputStream);
            workbook.Close();
            response.End();
        }

        private static string GetConnectionString(string filePath)
        {
            Dictionary<string, string> props = new Dictionary<string, string>();

            // XLSX - Excel 2007, 2010, 2012, 2013
            props["Provider"] = "Microsoft.ACE.OLEDB.12.0;";
            props["Extended Properties"] = "Excel 12.0 XML";
            props["Data Source"] = filePath; //"C:\\MyExcel.xlsx";

            // XLS - Excel 2003 and Older
            //props["Provider"] = "Microsoft.Jet.OLEDB.4.0";
            //props["Extended Properties"] = "Excel 8.0";
            //props["Data Source"] = "C:\\MyExcel.xls";

            StringBuilder sb = new StringBuilder();

            foreach (KeyValuePair<string, string> prop in props)
            {
                sb.Append(prop.Key);
                sb.Append('=');
                sb.Append(prop.Value);
                sb.Append(';');
            }

            return sb.ToString();
        }

        private static DataSet ReadExcelFile(string filePath)
        {
            DataSet ds = new DataSet();

            string connectionString = GetConnectionString(filePath);

            using (OleDbConnection conn = new OleDbConnection(connectionString))
            {
                conn.Open();
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = conn;

                // Get all Sheets in Excel File
                DataTable dtSheet = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                // Loop through all Sheets to get data
                foreach (DataRow dr in dtSheet.Rows)
                {
                    string sheetName = dr["TABLE_NAME"].ToString();

                    if (!sheetName.EndsWith("$"))
                        continue;

                    // Get all rows from the Sheet
                    cmd.CommandText = "SELECT * FROM [" + sheetName + "]";

                    DataTable dt = new DataTable();
                    dt.TableName = sheetName;

                    OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                    da.Fill(dt);

                    ds.Tables.Add(dt);
                }

                cmd = null;
                conn.Close();
            }

            return ds;
        }
        public static ServiceResult<DataTable> GetExcelData(string file)
        {
            try
            {
                new DataTable();
                DataSet ds = new DataSet();
                ds  =   ReadExcelFile(file);
               
                var mdatatable = ds.Tables[0];               
                //ExcelWorkbook tempWorkbook = new ExcelWorkbook(file);
                //tempWorkbook.LicenseKey = ConfigurationManager.AppSettings.Get("ExpertExcelLicenseKey");
                //tempWorkbook.Format = ExcelWorkbookFormat.Xlsx_2007;
                //ExcelWorksheet tempWorksheet = tempWorkbook.Worksheets[0];
                //var mdatatable = tempWorksheet.GetDataTable(tempWorksheet.UsedRange, true, false, true);
                //tempWorkbook.Close();
                
                //remove spaces in column names
                foreach (DataColumn column in mdatatable.Columns)
                {
                    column.ColumnName = column.ColumnName.Replace(" ", "");

                }
                return Responses.SuccessDataResult(mdatatable);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<DataTable>(null, ex.Message);
            }
        }

        //public static ServiceResult<StudentUploadBo> SaveStudentDetail(DataTable dt, int userId,int sessionId,int classId,int sectionId)
        //{
        //    try
        //    {
        //        var errlist = new List<ErrorDetail>();
        //        foreach (DataRow dts in dt.Rows)
        //        {
                   
        //            var email = dts.Field<string>("Email");
        //           // var res = IsValidEmail(email);
        //            if (!res)
        //            {
        //                var errorDetail = new ErrorDetail
        //                {
        //                    StudentName = dts["FirstName"].ToString(),
        //                    Error = "Please enter valid Email",
        //                    Description = dts.GetColumnError("Email")
        //                };
        //                errlist.Add(errorDetail);
        //            }

        //            int mobileno;
        //            var mobile = dts.Field<string>("StudentMobile");
        //            if (int.TryParse(mobile, out mobileno) && mobile.Length==10)
        //            {
        //                var errorDetail = new ErrorDetail
        //                {
        //                    StudentName = dts["FirstName"].ToString(),
        //                    Error = "Please check mobile number",
        //                    Description = dts.GetColumnError("StudentMobile")
        //                };
        //                errlist.Add(errorDetail);
        //            }

        //            DateTime dateTime;
        //            var date = dts.Field<string>("DOB");
        //            if (!DateTime.TryParse(date, out dateTime))
        //            {
        //                var errorDetail = new ErrorDetail
        //                {
        //                    StudentName = dts["FirstName"].ToString(),
        //                    Error = "Please enter valid Date",
        //                    Description = dts.GetColumnError("DOB")
        //                };
        //                errlist.Add(errorDetail);

        //            }

        //            var firstName = dts.Field<string>("FirstName");
        //            if (firstName == null)
        //            {
        //                var errorDetail = new ErrorDetail
        //                {
        //                    StudentName = dts["FirstName"].ToString(),
        //                    Error = "Please enter Required Name",
        //                    Description = dts.GetColumnError("FirstName")
        //                };
        //                errlist.Add(errorDetail);
        //            }
        //        }
        //        if (errlist.Count == 0)
        //        {
        //            var result = StudentUploadModule.SaveStudentDetail(dt, userId,sessionId,classId,sectionId);
        //            return Responses.SuccessDataResult(result);
        //        }
        //        else
        //        {
        //            var model = new StudentUploadBo
        //            {
        //                ErrorDetailsList = errlist
        //             };
        //            return Responses.SuccessDataResult(model);
        //        }

        //    }
        //    catch (Exception ex)
        //    {

        //        AddApplicationLog(ex);
        //        return Responses.FailureDataResult<StudentUploadBo>(null, ex.Message);
        //    }
        //}

        


    }
}
