﻿using System;
using System.Collections.Generic;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
    public class AccountDetailService : BaseLayer
    {

        public static ServiceResult<bool> AddAcountDetail(AccountDetailBo model)
        {
            try
            {
                AccountDetailModule.AddAccountDetail(model);
                return Responses.SuccessBoolResult;

            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }

        public static ServiceResult<bool> UpdateAccountDetail(AccountDetailBo model)
        {
            try
            {
                AccountDetailModule.UpdateAccountDetail(model);
                return Responses.SuccessBoolResult;
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }


        public static ServiceResult<AccountDetailBo> GetAccountDetailById(int id)
        {
            try
            {
                var result = AccountDetailModule.GetAccountDetailById(id);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<AccountDetailBo>(null, ex.Message);
            }
        }
        public static ServiceResult<List<AccountDetailBo>> GetAccountDetail()
        {
            try
            {
                var result = AccountDetailModule.GetAcountDetails();
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<AccountDetailBo>>(null, ex.Message);
            }
        }

        public static ServiceResult<List<DayMenu>> GetDay()
        {
            try
            {
                var dayList = AccountDetailModule.GetDayList();
                return Responses.SuccessDataResult(dayList);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<DayMenu>>(null, ex.Message);
            }
        }
        public static ServiceResult<List<Common>> GetPaymentMode()
        {
            try
            {
                var payment = AccountDetailModule.GetPaymentMode();
                return Responses.SuccessDataResult(payment);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }

        public static ServiceResult<List<Common>> GetPlan()
        {
            try
            {
                var plan = AccountDetailModule.GetPlan();
                return Responses.SuccessDataResult(plan);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }

        public static ServiceResult<bool> UpdateStatus(int id, int status, int userId)
        {
            try
            {
                var result = AccountDetailModule.UpdateStatus(id, status, userId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;


            }

        }

    }
}
