﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
    public class ExamService:BaseLayer
    {
        public static ServiceResult<int> AddExam(ExamBo model)
        {
            try
            {
                var result=ExamModule.AddExam(model);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult(0,ex.Message);

            }
        }

        public static ServiceResult<ExamBo> GetExamById(int id , int schoolId)
        {
            try
            {
                var res = ExamModule.GetExamById(id, schoolId);
                res.MappingList = ExamModule.GetMappingList(id);
                return Responses.SuccessDataResult(res);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<ExamBo>(null, ex.Message);
            }
        }

        public static ServiceResult<List<ExamBo>> GetExams(int schoolId)
        {
            try
            {
                var result = ExamModule.GetExames(schoolId);
                return Responses.SuccessDataResult(result);

            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<ExamBo>>(null, ex.Message);
            }

        }

        public static ServiceResult<bool> UpdateExam(ExamBo model)
        {
            try
            {
                ExamModule.UpdateExam(model);
                return Responses.SuccessBoolResult;
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;

            }
        }
        public static ServiceResult<bool> UpdateStatus(int id, int statusId, int userId)
        {
            try
            {
                var result = ExamModule.UpdateStatus(id, statusId, userId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;


            }

        }
    }
}
