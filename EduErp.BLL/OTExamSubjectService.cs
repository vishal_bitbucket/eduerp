﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EduErp.BO;
using EduErp.DAL;
using EduErp.Base;
using EduErp.BLL.Base;

namespace EduErp.BLL
{
    public class OTExamSubjectService : BaseLayer
    {
        public static ServiceResult<List<Common>> GetExams(int schoolId)
        {
            try
            {
                var res = OTExamSubjectModule.GetExams(schoolId);
                return Responses.SuccessDataResult(res);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }
        public static ServiceResult<List<Common>> GetSubjects(int schoolId)
        {
            try
            {
                var res = OTExamSubjectModule.GetSubjects(schoolId);
                return Responses.SuccessDataResult(res);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }
        public static ServiceResult<int> AddExamSubject(OTExamSubjectMapping model)
        {
            try
            {
                var result = OTExamSubjectModule.AddExamSubject(model);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult(0, ex.Message);

            }
        }
        public static ServiceResult<List<OTExamSubjectMapping>> GetExamSubject(int schoolId)
        {
            try
            {
                var result = OTExamSubjectModule.GetExamSubject(schoolId);
                return Responses.SuccessDataResult(result);

            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<OTExamSubjectMapping>>(null, ex.Message);
            }

        }
        public static ServiceResult<bool> UpdateStatus(int id, int statusId, int userId)
        {
            try
            {
                var result = OTExamSubjectModule.UpdateStatus(id, statusId, userId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;


            }

        }
        public static ServiceResult<OTExamSubjectMapping> GetOTExamSubjectById(int id)
        {
            try
            {
                var result = OTExamSubjectModule.GetOTExamSubjectById(id);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<OTExamSubjectMapping>(null, ex.Message);

            }
        }
        public static ServiceResult<bool> UpdateExamSubject(OTExamSubjectMapping model)
        {
            try
            {
                OTExamSubjectModule.UpdateExamSubject(model);
                return Responses.SuccessBoolResult;
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;

            }
        }
        public static ServiceResult<List<ExamSubjectList>> GetExamSubjectMappingList(int examId,int schoolId)
        {
            try
            {
                var result = OTExamSubjectModule.GetExamSubjectMappingDetail(examId, schoolId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<ExamSubjectList>>(null, ex.Message);
            }

        }
    }
}
