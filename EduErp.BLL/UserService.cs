﻿using System;
using System.Collections.Generic;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
    public class UserService : BaseLayer
    {
        public static ServiceResult<int> AddUser(UserBo user)
        {
            try
            {
                var result = UserModule.AddUser(user);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult(0,ex.Message);
            }
        }

        public static ServiceResult<List<UserBo>> GetUsers(int schoolId)
        {
            try
            {
                var users = UserModule.GetUsers(schoolId);
                return Responses.SuccessDataResult(users);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<UserBo>>(null, ex.Message);
            }

        }

        public static ServiceResult<UserBo> AuthenticateUserByOldPassword(int id, string password)
        {
            try
            {
                var result = UserModule.AuthenticateUserByOldPassword(id, password);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                
               AddApplicationLog(ex);
                return Responses.FailureDataResult<UserBo>(null, ex.Message);
            }
        }

        public static ServiceResult<UserBo> GetUserByEmail(string email)
        {
            try
            {
                var userByEmail = UserModule.GetUserByEmail(email);
                return Responses.SuccessDataResult(userByEmail);
            }
            catch (Exception ex)
            {
                
                AddApplicationLog(ex);
                return Responses.FailureDataResult<UserBo>(null, ex.Message);
            }
        }
        public static ServiceResult<UserBo> GetUserByMobileNumber(string mobileNumber)
        {
            try
            {
                var userByEmail = UserModule.GetUserByNumber(mobileNumber);
                return Responses.SuccessDataResult(userByEmail);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<UserBo>(null, ex.Message);
            }
        }
        public static ServiceResult<UserBo> GetUser(int id)
        {
            try
            {
                var result = UserModule.GetUser(id);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<UserBo>(null, ex.Message);
            }
        }

        public static ServiceResult<bool> Update(UserBo model)
        {
            try
            {
                var result = UserModule.Update(model);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }

        }

        public static ServiceResult<bool> ChangePassword(int userId, string password)
        {
            try
            {
                UserModule.ChangePassword(userId, password);
                return Responses.SuccessBoolResult;
            }
            catch (Exception ex)
            {
                
               AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }

        public static ServiceResult<bool> Delete(UserBo model)
        {
            try
            {
                var result = UserModule.Delete(model);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }

        public static ServiceResult<bool> UpdateStatus(int id, int statusId, int userId)
        {

            try
            {
                var result = UserModule.UpdateStatus(id, statusId, userId);
                return Responses.SuccessDataResult(result);

            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }

        public static ServiceResult<List<Common>> GetRights()
        {
            try
            {
                var users = UserModule.GetRights();
                return Responses.SuccessDataResult(users);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }

        }
        public static ServiceResult<string> UpdateOtp(int userId, string otp)
        {
            try
            {
                var code = UserModule.UpdateOtp(userId, otp);
                return Responses.SuccessDataResult(code);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<string>(null, ex.Message);
            }
        }

        public static ServiceResult<Guid> UpdateVerificationCode(int userId, Guid verificationCode)
        {
            try
            {
                var code = UserModule.UpdateVerificationCode(userId, verificationCode);
                return Responses.SuccessDataResult(code);
            }
            catch (Exception ex)
            {
                
                AddApplicationLog(ex);
                return Responses.FailureDataResult<Guid>(Guid.Empty, ex.Message);
            }
        }

        public static ServiceResult<UserBo> GetUserByOtp(string otp)
        {
            try
            {
                var user = UserModule.GetUserByOtp(otp);
                return Responses.SuccessDataResult(user);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<UserBo>(null, ex.Message);
            }
        }
        public static ServiceResult<UserBo> GetUserByVerificationCode(Guid verificationCode)
        {
            try
            {
                var user = UserModule.GetUserByVerificationCode(verificationCode);
                return Responses.SuccessDataResult(user);
            }
            catch (Exception ex)
            {
                
                AddApplicationLog(ex);
                return Responses.FailureDataResult<UserBo>(null, ex.Message);
            }
        }

        public static ServiceResult<bool> ForgetPasswordCreateNew(string userId, string password)
        {
            try
            {
                UserModule.ForgetPassWordCreateNew(userId, password);
                return Responses.SuccessBoolResult;
            }
            catch (Exception ex)
            {
                
               AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }
        public static ServiceResult<List<Common>> GetUserType()
        {
            try
            {
                var users = UserModule.GetUserType();
                return Responses.SuccessDataResult(users);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }

        }
        public static ServiceResult<List<MenuItem>> GetMenu()
        {
            try
            {
                var users = UserModule.GetMenu();
                return Responses.SuccessDataResult(users);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<MenuItem>>(null, ex.Message);
            }

        }

        public static ServiceResult<int> LeftUser(int schoolId)
        {
            try
            {
                var result = UserModule.LeftUser(schoolId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult(0, ex.Message);
            }
        }
        public static ServiceResult<Permission> GetPermissionByUserId(int userId, int schoolId = 0)
        {
            try
            {
                var users = UserModule.GetPermissionByUserId(userId, schoolId);
                return Responses.SuccessDataResult(users);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<Permission>(null, ex.Message);
            }

        }
    }
}

