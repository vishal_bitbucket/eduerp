﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
    public class OnlineTestSubjectService : BaseLayer

    {
        public static ServiceResult<List<OnlineExamTestSubjectBo>> GetOnlineTestSubject(int SchoolId)
        {
            try
            {
                var result = OnlineTestSubjectModule.GetOnlineTestSubject(SchoolId);
                return Responses.SuccessDataResult(result);

            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<OnlineExamTestSubjectBo>>(null, ex.Message);
            }

        }
        public static ServiceResult<int> AddOnlineTestSubject(OnlineExamTestSubjectBo model)
        {
            try
            {
                var result = OnlineTestSubjectModule.AddOnlineTestSubject(model);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult(0, ex.Message);

            }
        }
        public static ServiceResult<bool> UpdateStatus(int id, int status, int userId)
        {
            try
            {
                var result = OnlineTestSubjectModule.UpdateStatus(id, status, userId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;


            }

        }
        public static ServiceResult<OnlineExamTestSubjectBo> GetOnlineTestSubjectById(int id)
        {
            try
            {
                var result = OnlineTestSubjectModule.GetOnlineTestSubjectById(id);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<OnlineExamTestSubjectBo>(null, ex.Message);

            }
        }
        public static ServiceResult<bool> UpdateOnlineTestSubject(OnlineExamTestSubjectBo model)
        {
            try
            {
                OnlineTestSubjectModule.UpdateOnlineTestSubject(model);
                return Responses.SuccessBoolResult;
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;

            }
        }
    }
}
