﻿using System;
using System.Collections.Generic;
using System.Linq;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
   public class MyAccountService:BaseLayer
    {
       public static ServiceResult<MyAccountBo> GetMyAccuontDetail(int schoolId)
       {
           try
           {
               var data = MyAccountModule.GetMessageList();
               var model = new MyAccountBo
               {
                   SchoolDetails = MyAccountModule.GetSchoolById(schoolId),
                   AccountDetails = MyAccountModule.GetAccountDetailBySchoolId(schoolId),
                   ListOfTransactions = MyAccountModule.GetAccountFeeDetailsBySchoolId(schoolId),
                   ListOfMessage = BuildTree(data, data.Where(b => b.ParentId == 0).ToList(), MyAccountModule.GetMessageSettingsBySchoolId(schoolId)),
                   PlansList = MyAccountModule.GetPlans(schoolId)
               };
               return Responses.SuccessDataResult(model);

           }
           catch (Exception ex)
           {
               
               AddApplicationLog(ex);
               return Responses.FailureDataResult<MyAccountBo>(null, ex.Message);
           }
       }

       private static List<MessagingTree> BuildTree(List<AutomateMessageBo> permissions, List<AutomateMessageBo> parents, List<MessageAutomateMapping> permissionId)
        {
            var result = new List<MessagingTree>();
            foreach (var parent in parents)
            {
                var messageAutomateMapping = permissionId
                    .FirstOrDefault(a => a.MessageAutomateId == parent.Id);
                if (messageAutomateMapping != null)
                    parent.TemplateId = messageAutomateMapping.TemplateId;
                var item = new MessagingTree() { Permission = parent, IsSelected = permissionId.Any(a => a.MessageAutomateId == parent.Id) };
                var children = permissions.Where(a => a.ParentId == parent.Id).ToList();
                item.Children = BuildTree(permissions, children, permissionId);
                result.Add(item);
            }
            return result;
        }

       public static ServiceResult<bool> SaveMessageSetting(List<MessageSettings> model, int schoolId)
       {
           try
           {
               return Responses.SuccessDataResult(MyAccountModule.SaveMessageSetting(model, schoolId));
           }
           catch(Exception ex) 
           {
               AddApplicationLog(ex);
                   return Responses.FailureBoolResult;
           }
       }
    }
}
