﻿using System;
using System.Collections.Generic;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;
using System.Text.RegularExpressions;

namespace EduErp.BLL
{
   public class CertificateService:BaseLayer
    {
       public static ServiceResult<bool> AddMapping(CertificationBo model)
       {
           try
           {
               CertificateModule.AddMapping(model);
               return Responses.SuccessBoolResult;
           }
           catch (Exception ex)
           {
               AddApplicationLog(ex);
               return Responses.FailureBoolResult;
           }
       }
       public static ServiceResult<bool> UpdateCertificate(CertificationBo model)
       {
           try
           {
               CertificateModule.UpdateCertificate(model);
               return Responses.SuccessBoolResult;
           }
           catch (Exception ex)
           {
               AddApplicationLog(ex);
               return Responses.FailureBoolResult;
           }
       }
       public static ServiceResult<List<Common>> GetCertificateTemplateType()
       {
           try
           {
               var result = CertificateModule.GetCertificateTemplateTypeList();
               return Responses.SuccessDataResult(result);
           }
           catch (Exception ex)
           {

               AddApplicationLog(ex);
               return Responses.FailureDataResult<List<Common>>(null, ex.Message);
           }
       }

       public static ServiceResult<string> GetTemplate(int sessionId, int classId, int sectionId, int studentId, int templateId,int schoolId)
       {
           try
           {
               var student = CertificateModule.GetStudentDetailForCertificate(sessionId, classId, sectionId, studentId);
               var template = CertificateModule.GetTextTemplate(templateId, schoolId);
               if (student != null)
               {
                   Dictionary<string, object> parameters = new Dictionary<string, object>();
                   parameters.Add("[#SchoolName]", student.SchoolName);
                   parameters.Add("[#SchoolAddress]", student.SchoolAddress);
                   parameters.Add("[#Year]", student.SessionName);
                   parameters.Add("[#StudentName]", student.FirstName);
                   parameters.Add("[#SchoolCode]", student.SchoolCode);
                   parameters.Add("[#AdmissionNo.]", student.StudentCode);
                   parameters.Add("[#SerialNo.]", student.SerialNo);
                   parameters.Add("[#ReasonForLeavingTheSchool]", student.ReasonForExit);
                   parameters.Add("[#SchoolFeeDuesIfAny]", student.Dues);
                   parameters.Add("[#OtherRemarksIfAny]", student.OtherRemarks);
                   parameters.Add("[#SchoolWebsite]", student.SchoolWebsite);
                   parameters.Add("[#SchoolEmail]", student.SchoolEmail);
                   parameters.Add("[#StudentGender]", student.GenderName);
                   parameters.Add("[#StudentFatherName]", student.FatherName);
                   parameters.Add("[#StudentMotherName]", student.MotherName);
                   parameters.Add("[#StudentDateOfAdmission]", student.Doj.Value.ToString("dd-MMM-yyyy"));
                   parameters.Add("[#StudentDateOfBirth]", student.Dob.Value.ToString("dd-MMM-yyyy"));
                   parameters.Add("[#StudentNationality]", student.NationlityName);
                   parameters.Add("[#StudentReligion]", student.RelegionName);
                   parameters.Add("[#DateOfApplicationForTheCertificate]", DateTime.Now.ToString());
                   parameters.Add("[#DateOfIssueOfTheCertificate]", DateTime.Now.ToString());

                   template = BLL.EmailNotifications.MailSender.ReplaceKeyByValueCer(template, parameters);  //String.Replace(template,parameters);
               }
                   return Responses.SuccessDataResult(template);
               
           }
           catch (Exception ex) {
               AddApplicationLog(ex);
               return Responses.FailureDataResult<string>(null,ex.Message);
           }


       }

       public static ServiceResult<List<Common>> GetTemplateList(int schoolId)
       {
           try
           {
               var result = CertificateModule.GetTemplateList(schoolId);
               return Responses.SuccessDataResult(result);
           }
           catch (Exception ex)
           {

               AddApplicationLog(ex);
               return Responses.FailureDataResult<List<Common>>(null, ex.Message);
           }
       }
    public static ServiceResult<List<CertificationBo>>GetCertificates(int schoolId)
       {
       try
       {
           var result=CertificateModule.GetCerticicateList(schoolId);
           return Responses.SuccessDataResult(result);
       }catch(Exception ex)
       {
       
            AddApplicationLog(ex);
           return Responses.FailureDataResult<List<CertificationBo>>(null,ex.Message);
       }
       }
        public static ServiceResult<List<Common>>GetCertificateTemplateNameByTeplateTypeId(int templateTypeId)
       {
       try
       {
           var result=CertificateModule.CertificateTemplateName(templateTypeId);
           return Responses.SuccessDataResult(result);
       }catch(Exception ex)
       {
       
            AddApplicationLog(ex);
           return Responses.FailureDataResult<List<Common>>(null,ex.Message);
       }
       }
        public static ServiceResult<CertificationBo> GetCertificateById(int id,int schoolId)
        {
            try
            {
                var result = CertificateModule.GetCertificateById(id, schoolId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<CertificationBo>(null, ex.Message);
            }
        }
         public static ServiceResult<bool> UpdateStatus(int id, int status, int userId)
         {
             try
             {
                 var result = CertificateModule.UpdateStatus(id, status, userId);
                 return Responses.SuccessDataResult(result);
             }
             catch (Exception ex)
             {
                 AddApplicationLog(ex);
                 return Responses.FailureBoolResult;


             }

         }
    }
}
