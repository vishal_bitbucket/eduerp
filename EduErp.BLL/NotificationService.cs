﻿using System;
using System.Collections.Generic;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
     public class NotificationService:BaseLayer
    {
         public static ServiceResult<bool> AddNotification(NotificationBo model)
         {
             try
             {
                 NotificationModule.AddNotification(model);
                 return Responses.SuccessBoolResult;
             }
             catch (Exception ex)
             {
                 
                 AddApplicationLog(ex);
                 return Responses.FailureBoolResult;
             }
         }

         public static ServiceResult<List<NotificationBo>> GetNotificationsForIndex()
         {
             try
             {
                 var result = NotificationModule.GetNotifications();
                 return Responses.SuccessDataResult(result);
             }
             catch (Exception ex)
             {
                 
                 AddApplicationLog(ex);
                 return Responses.FailureDataResult<List<NotificationBo>>(null, ex.Message);
             }
         }

         public static ServiceResult<bool> UpdateNotification(NotificationBo model)
         {
             try
             {
                 NotificationModule.UpdateNotification(model);
                 return Responses.SuccessBoolResult;
             }
             catch (Exception ex)
             {
                 
                 AddApplicationLog(ex);
                 return Responses.FailureBoolResult;
             }
         }

         public static ServiceResult<bool> UpdateStatus(int id, int statusId, int userId)
         {
             try
             {
                 var result = NotificationModule.UpdateStatus(id, statusId, userId);
                 return Responses.SuccessDataResult(result);
             }
             catch (Exception ex)
             {
                 AddApplicationLog(ex);
                 return Responses.FailureBoolResult;
             }
         }
         public static ServiceResult<NotificationBo> GetNotificationById( int id)
         {
             try
             {
                 var result = NotificationModule.GetNotificationById(id);
                 return Responses.SuccessDataResult(result);
             }
             catch (Exception ex)
             {

                 AddApplicationLog(ex);
                 return Responses.FailureDataResult<NotificationBo>(null, ex.Message);
             }
         }

         public static ServiceResult<List<Common>> GetNotifications()
         {
             try
             {
                 var result = NotificationModule.GetNotificationTypeList();
                 return Responses.SuccessDataResult(result);
             }
             catch (Exception ex)
             {
                 
                AddApplicationLog(ex);
                 return Responses.FailureDataResult<List<Common>>(null, ex.Message);
             }
         }
    }
}
