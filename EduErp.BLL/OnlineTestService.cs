﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
   public class OnlineTestService:BaseLayer

    {



       public static ServiceResult<int> AddOnlineTest(OnlineExamTestBo model)
        {
            try
            {
                var result=OnlineTestModule.AddOnlineTest(model);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult(0,ex.Message);

            }
        }
       public static ServiceResult<List<OnlineExamTestBo>> GetOnlineTest(int schoolId)
       {
           try
           {
               var result = OnlineTestModule.GetOnlineTest(schoolId);
               return Responses.SuccessDataResult(result);

           }
           catch (Exception ex)
           {
               AddApplicationLog(ex);
               return Responses.FailureDataResult<List<OnlineExamTestBo>>(null, ex.Message);
           }

       }
       public static ServiceResult<bool> UpdateStatus(int id, int status, int userId)
       {
           try
           {
               var result = OnlineTestModule.UpdateStatus(id, status, userId);
               return Responses.SuccessDataResult(result);
           }
           catch (Exception ex)
           {
               AddApplicationLog(ex);
               return Responses.FailureBoolResult;


           }

       }
       public static ServiceResult<OnlineExamTestBo> GetOnlineTestById(int id)
       {
           try
           {
               var result = OnlineTestModule.GetOnlineTestById(id);
               return Responses.SuccessDataResult(result);
           }
           catch (Exception ex)
           {
               AddApplicationLog(ex);
               return Responses.FailureDataResult<OnlineExamTestBo>(null, ex.Message);

           }
       }
       public static ServiceResult<bool> UpdateClass(OnlineExamTestBo model)
       {
           try
           {
               OnlineTestModule.UpdateClass(model);
               return Responses.SuccessBoolResult;
           }
           catch (Exception ex)
           {
               AddApplicationLog(ex);
               return Responses.FailureBoolResult;

           }
       }
    }
}
