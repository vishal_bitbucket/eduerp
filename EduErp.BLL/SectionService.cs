﻿using System;
using System.Collections.Generic;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
    public class SectionService : BaseLayer
    {
        public static ServiceResult<int> AddSection(SectionBo model)
        {
            try
            {
               var result= SectionModule.AddSection(model);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult(0,ex.Message);
            }
        }
        public static ServiceResult<List<SectionBo>> GetSections(int schoolId)
        {
            try
            {
                var sections = SectionModule.GetSections(schoolId);
                return Responses.SuccessDataResult(sections);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<SectionBo>>(null, ex.Message);
            }
        }

        public static ServiceResult<bool> UpdateStatus(int id, int status, int userId)
        {
            try
            {
                var result = SectionModule.UpdateStatus(id, status, userId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;


            }

        }

        public static ServiceResult<SectionBo> GetSectionById(int id)
        {
            try
            {
                var result = SectionModule.GetSectionById(id);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<SectionBo>(null, ex.Message);
            }
        }
        public static ServiceResult<bool> UpdateSection(SectionBo model)
        {
            try
            {
                SectionModule.UpdateSection(model);
                return Responses.SuccessBoolResult;
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }

        public static ServiceResult<List<Common>> GetClasses(int schoolId)
        {
            try
            {
                var result = SectionModule.GetClasses(schoolId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }

    }
}
