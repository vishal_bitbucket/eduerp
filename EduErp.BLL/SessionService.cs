﻿using System;
using System.Collections.Generic;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
    public class SessionService : BaseLayer
    {
        public static ServiceResult<int> AddSession(SessionMaster model)
        {
            try
            {
                var result = SessionModule.AddSesion(model);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult(0,ex.Message);
            }
        }

        public static ServiceResult<bool> UpdateSession(SessionMaster model)
        {
            try
            {
                var result = SessionModule.UpdateSession(model);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }

        public static ServiceResult<int> UpdateStatus(int sessionId, int statusId, int userId,int schoolId)
        {
            try
            {
                var result = SessionModule.UpdateStatus(sessionId, statusId, userId,schoolId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<int>(0,ex.Message);
            }
        }

        public static ServiceResult<List<SessionMaster>> GetSessions(int id)
        {
            try
            {
                var result = SessionModule.GetSessions(id);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<SessionMaster>>(null, ex.Message);
            }
        }

        public static ServiceResult<SessionMaster> GetSessionById(int id)
        {
            try
            {
                var result = SessionModule.GetSessionById(id);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<SessionMaster>(null, ex.Message);
            }
        }
    }
}
