﻿namespace EduErp.BLL.EmailNotifications
{
    public class EmailSettingViewModel
    {
        public string EmailAddress { get; set; }

        public string Password { get; set; }

        public string SenderName { get; set; }

        public string SmtpHost { get; set; }

        public int Port { get; set; }

        public bool EnableSsl { get; set; }

        //public string AdminMailRecipients { get; set; }
    }

    public enum MailTypes
    {
        TestMail,
        ExceptionMail,
        UserAdded,
        ForgotPasswordMail,
        EmailAddressChanged,
        ResetPasswordMail,
        CreatePasswordMail,
        QueryMail,
        UserVerificationMail,
        ContactUs
    }
}
