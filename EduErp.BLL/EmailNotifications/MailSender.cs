﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Web;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL.EmailNotifications
{
    public static class MailSender
    {
        public static void SendMailAsync(MailTypes mailType, string recipientAddress, Dictionary<string, object> values)
        {
            SendMailAsync(mailType, CurrentMailSettings, recipientAddress, values);
        }

        public static void SendExceptionMailAsync(MailTypes mailType, string recipientAddress, Dictionary<string, object> values)
        {
            SendMailAsync(mailType, CurrentMailSettings, recipientAddress, values);
        }

        private static void SendMailAsync(MailTypes mailType, EmailSettingViewModel mailSetting, string recipientAddress, Dictionary<string, object> values)
        {
            var thread = new Thread(() => SendMail(mailType, mailSetting, recipientAddress, values));
            thread.Start();
        }

        public static string SendMail(MailTypes mailType, string recipientAddress, Dictionary<string, object> values)
        {
            return SendMail(mailType, CurrentMailSettings, recipientAddress, values);
        }

        private static string SendMail(MailTypes mailType, EmailSettingViewModel mailSetting, string recipientAddress, Dictionary<string, object> values)
        {
            //return string.Empty;
            try
            {
                var sender = new MailAddress(mailSetting.EmailAddress, mailSetting.SenderName);
                //var recipient = new MailAddress(recipientAddress);
                var subject = GetMailSubject(mailType) ?? values["Subject"].ToString();
                var mailBody = GetMailBody(mailType, values);
                if (mailType == MailTypes.ExceptionMail)
                    mailBody = mailBody.Replace("##DateTime##", DateTime.UtcNow.ToString());

                var mail = new MailMessage
                {
                    Subject = subject,
                    Body = mailBody,
                    IsBodyHtml = true,
                    Priority = MailPriority.Normal,
                    Sender = sender,
                    From = sender,

                };

                var recipients = recipientAddress.Split(',');
                foreach (var recipient in recipients)
                    mail.To.Add(new MailAddress(recipient));

                //mail.ReplyToList.Add(new MailAddress(ConfigurationManager.AppSettings["ServicesEmail"]));
                //mail.ReplyToList.Add(new MailAddress(ConfigurationManager.AppSettings["MattEmail"]));
                //if (mailType == MailTypes.AccountExpiry || mailType == MailTypes.AccountExpired || mailType == MailTypes.AllUserDataToCompanyAdminMail)
                //{
                //    mail.CC.Add(new MailAddress(ConfigurationManager.AppSettings["ServicesEmail"]));
                //    mail.Bcc.Add(new MailAddress(ConfigurationManager.AppSettings["MattEmail"]));
                //}

                var client = new SmtpClient
                {
                    Host = mailSetting.SmtpHost,
                    Port = mailSetting.Port,
                    //Credentials = new NetworkCredential(mailSetting.EmailAddress, ReversibleEncryptionHelper.Decrypt(mailSetting.Password)),
                    Credentials = new NetworkCredential(mailSetting.EmailAddress, mailSetting.Password),
                    EnableSsl = mailSetting.EnableSsl,
                };

                var signatureView = AlternateView.CreateAlternateViewFromString(mailBody, null, MediaTypeNames.Text.Html);
                var signatureImagePath = GetEmailResourceFilePath("SignatureImageFileName");
                //if (mailType == MailTypes.BackgroundCheckMail || mailType == MailTypes.HardBadgeCheckMail || mailType == MailTypes.RequiredMail || mailType == MailTypes.AllUserDataToCompanyAdminMail)
                //{
                //    string backgroundExcelPath;
                //    try
                //    {
                //        backgroundExcelPath = Path.Combine(HttpRuntime.AppDomainAppPath, "uploads", values["fileName"] + ".xlsx");
                //    }
                //    catch (Exception)
                //    {
                //        backgroundExcelPath = Path.Combine(ConfigurationManager.AppSettings["ExcelFilePath"], values["fileName"] + ".xlsx");
                //    }
                //    if (File.Exists(backgroundExcelPath))
                //    {
                //        var attachment = new Attachment(backgroundExcelPath);
                //        mail.Attachments.Add(attachment);
                //    }
                //}
                //if (mailType == MailTypes.InvoiceMail)
                //{
                //    string backgroundExcelPath;
                //    try
                //    {
                //        backgroundExcelPath = (string)values["fileName"];
                //        if (File.Exists(backgroundExcelPath))
                //        {
                //            var attachment = new Attachment(backgroundExcelPath);
                //            mail.Attachments.Add(attachment);
                //        }
                //    }
                //    catch (Exception)
                //    {

                //    }

                //}

                var signatureResource = new LinkedResource(signatureImagePath, "image/png") { ContentId = "imcredentialed-signature-image" };
                signatureView.LinkedResources.Add(signatureResource);
                mail.AlternateViews.Add(signatureView);
                client.Send(mail);
                //client.SendAsync(mail, null);
                var emailSend = new EmailSend()
                {
                    MailType = mailType.ToString(),
                    Email = recipientAddress,
                    IsSend = true,
                    SentBy = 1,//SessionItems.UserId,
                    SentOn = DateTime.Now
                };
                LogService.AddSendEmail(emailSend);
                return string.Empty;
            }
            catch (Exception ex)
            {
                var innerException = ex.Message + " ";
                innerException += ex.InnerException == null ? string.Empty : ex.InnerException.Message;
                var emailSend = new EmailSend()
                {
                    MailType = mailType.ToString(),
                    Email = recipientAddress,
                    IsSend = false,
                    Exception = innerException,
                    SentBy = 1,//SessionItems.UserId,
                    SentOn = DateTime.Now
                };
                LogService.AddSendEmail(emailSend);
                return string.Format("Mail Not Sent {0} Exception: {1}{2} Inner Exception: {3}", Environment.NewLine, ex.Message, Environment.NewLine, innerException);
            }
        }

        private static string GetMailSubject(MailTypes mailType)
        {
            switch (mailType)
            {
                case MailTypes.TestMail:
                    return GetAppSetting("TestMailSubject");

                case MailTypes.ForgotPasswordMail:
                    return GetAppSetting("ForgotPasswordMailSubject");
                case MailTypes.ExceptionMail:
                    return GetAppSetting("ExceptionMailSubject");
                case MailTypes.QueryMail:
                    return GetAppSetting("QueryMail");
                case MailTypes.ResetPasswordMail:
                    return GetAppSetting("ResetPasswordMailSubject");
                case  MailTypes.CreatePasswordMail:
                    return GetAppSetting("CreatePasswordMailSuject");
                case MailTypes.ContactUs:
                    return GetAppSetting("ContactUsSubject");

                default:
                    throw new ArgumentOutOfRangeException("mailType");
            }
        }

        private static string GetMailBody(MailTypes mailType, Dictionary<string, object> values)
        {
            string templateFilePath;
            switch (mailType)
            {
                case MailTypes.TestMail:
                    templateFilePath = GetEmailResourceFilePath("TestMailTemplate");
                    break;

                case MailTypes.ExceptionMail:
                    templateFilePath = GetEmailResourceFilePath("ExceptionMailTemplate");
                    break;
                case MailTypes.QueryMail:
                    templateFilePath = GetEmailResourceFilePath("SendQueryTemplate");
                    break;
                case MailTypes.ForgotPasswordMail:
                    templateFilePath = GetEmailResourceFilePath("ForgotPasswordMailTemplate");
                    break;
                case MailTypes.CreatePasswordMail:
                    templateFilePath = GetEmailResourceFilePath("CreatePasswordMailTemplate");
                    break;
                case MailTypes.ResetPasswordMail:
                    templateFilePath = GetEmailResourceFilePath("ResetPasswordMailTemplate");
                    break;
                case MailTypes.ContactUs:
                    templateFilePath = GetEmailResourceFilePath("ContactUsMailTemplate");
                    break;

                default:
                    throw new ArgumentOutOfRangeException("mailType");
            }

            string mailBody;

            using (var stream = new FileStream(templateFilePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (var streamReader = new StreamReader(stream))
                {
                    mailBody = streamReader.ReadToEnd();
                }
            }

            mailBody = ReplaceKeyByValue(mailBody, values);

            return mailBody;
        }

        internal static string ReplaceKeyByValue(string template, Dictionary<string, object> values)
        {
            foreach (var key in values.Keys)
            {
                var value = values[key] ?? string.Empty;
                template = template.Replace(string.Format("##{0}##", key), value);
            }
            return template;
        }
        internal static string ReplaceKeyByValueCer(string template, Dictionary<string, object> values)
        {
            foreach (var key in values.Keys)
            {
                var value = values[key] ?? string.Empty;
                template = template.Replace(key, value);
            }
            return template;
        }

        internal static string GetEmailResourceFilePath(string templateKey)
        {
            return Path.Combine(HttpRuntime.AppDomainAppPath, GetAppSetting("EmailTemplatesFolder"), GetAppSetting(templateKey));
        }

        private static string GetAppSetting(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        private static EmailSettingViewModel CurrentMailSettings
        {
            get
            {
                int port;
                if (!int.TryParse(LogModule.GetAppSetting(HostSettingNames.SmtpPort), out port))
                    port = 25;
                bool enableSsl;
                bool.TryParse(LogModule.GetAppSetting(HostSettingNames.SmtpEnableSsl), out enableSsl);

                var mailSettings = new EmailSettingViewModel
                {
                    SenderName = LogModule.GetAppSetting(HostSettingNames.SenderName),
                    EmailAddress = LogModule.GetAppSetting(HostSettingNames.SenderEmailAddress),
                    Password = LogModule.GetAppSetting(HostSettingNames.SenderEmailPassword),
                    SmtpHost = LogModule.GetAppSetting(HostSettingNames.SmtpHost),
                    Port = port,
                    EnableSsl = enableSsl,
                };
                return mailSettings;
            }
        }

        private static string Replace(this string item, object oldValue, object newValue)
        {
            return item.Replace(oldValue.ToString(), newValue.ToString());
        }
    }
}
