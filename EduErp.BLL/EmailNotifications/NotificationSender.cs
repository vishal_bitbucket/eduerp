﻿using System;
using System.Collections.Generic;
using System.Linq;
using EduErp.BO;
using EduErp.DAL;



namespace EduErp.BLL.EmailNotifications
{
    public class NotificationSender
    {
        //public static void SendTestMail(EmailSettingViewModel settings, string recipientAddress)
        //{
        //    MailSender.SendTestMailAsync(MailTypes.TestMail, settings, recipientAddress, new Dictionary<string, object>());
        //}

        public static void SendExceptionMails(ApplicationLogBo log, string requestedUrl = null)
        {
            NotificationUserBo user = null;
            var recipients = new List<string>();
            try
            {
                recipients = LogService.GetAppSetting(HostSettingNames.ErrorLoggingEmailAddresses).Split(',').ToList();
                if (log.UserID.HasValue) user = LogModule.GetUserById(log.UserID.GetValueOrDefault());
            }
            catch (Exception) { }

            //foreach (var recipient in recipients)
            //{
                var values = new Dictionary<string, object>
                             {
                                 {"RecipientName", "Dev Team"},
                                 {"SchoolID", log.SchoolId },
                                 {"SchoolName", log.SchoolName },
                                 {"DisplayName", user != null ? user.DisplayName : string.Empty},
                                 {"EmailAddress", user != null ? user.Email : string.Empty},
                                 {"UserID", log.UserID.HasValue ? log.UserID.ToString() : string.Empty},
                                 {"IPAddress", log.IpAddress},
                                 {"RequestedUrl", requestedUrl},
                                 {"Module", log.Module},
                                 {"ClassName", log.ClassName},
                                 {"MethodName", log.MethodName},
                                 {"DateTime", DateTime.UtcNow},

                                 {"ExceptionMessage", log.ExceptionMessage},
                                 {"StackTrace", log.StackTrace},
                                 {"InnerExceptionMessage", log.InnerExceptionMessage},
                                 {"InnerStackTrace", log.InnerExceptionStackTrace},
                             };
                MailSender.SendExceptionMailAsync(MailTypes.ExceptionMail, LogService.GetAppSetting(HostSettingNames.ErrorLoggingEmailAddresses), values);
            //}

        }

        public static void SendQueryMail( MyAccountBo model, long userId, string userName)
        {
            var values = new Dictionary<string, object>
            {
                {"RecipientName", "Admin"},
                {"SchoolID", model.SchoolDetails.Id },
                {"SchoolName", model.SchoolDetails.Name },
                {"UserID", userId },
                {"UserName", userName },
                {"Subject",model.Subject},
                {"Query",model.Query}
            };
            MailSender.SendMailAsync(MailTypes.QueryMail, LogService.GetAppSetting(HostSettingNames.ErrorLoggingEmailAddresses), values);
        }

        public static void SendCreateNewPasswordEmail(string displayName, string email)
        {
            var values = new Dictionary<string, object> { { "DisplayName", displayName } };
            MailSender.SendMailAsync(MailTypes.CreatePasswordMail, email, values);
        }

       
       public static void SendForgetPasswordMail(string name,string email, string resetPasswordUrl )
        {
            var values = new Dictionary<string, object>
                             {
                                 {"DisplayName", name},
                                 {"Email", email},
                                 {"ResetPasswordUrl", resetPasswordUrl},
                             };

            MailSender.SendMailAsync(MailTypes.ForgotPasswordMail, email, values);
        }

       public static void SendContactUsMail(ContactUs model)
       {
           var values = new Dictionary<string, object>
            {
                {"UserName", model.UserName},
                {"Email", model.Email },
                {"Phone", model.Phone },
                {"Message", model.Message }
            };
           MailSender.SendMailAsync(MailTypes.ContactUs, LogService.GetAppSetting(HostSettingNames.ErrorLoggingEmailAddresses), values);
       }
    }
}
