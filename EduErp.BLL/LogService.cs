﻿using System;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
    public class LogService : BaseLayer
    {
        #region # Application Setting #

        public static string GetAppSetting(HostSettingNames key)
        {
            try
            {
                return LogModule.GetAppSetting(key);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return null;
            }
        }

        public static ServiceResult<bool> AddApplicationLog(ApplicationLog log)
        {
            try
            {
                return LogModule.AddApplicationLog(log);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return null;
            }
        }

        #endregion

        #region # Send Mail #

        public static ServiceResult<bool> AddSendEmail(EmailSend email)
        {
            try
            {
                return LogModule.AddSendEmail(email);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.SuccessDataResult(false);
            }
        }


        #endregion

        #region # Amazon Settings #

        public static ServiceResult<AmazonSetting> GetAmazonSetting()
        {
            try
            {
                return Responses.SuccessDataResult(LogModule.GetAmazonSetting());
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.SuccessDataResult(new AmazonSetting());
            }
        }

        #endregion
    }
}
