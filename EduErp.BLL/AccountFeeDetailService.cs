﻿using System;
using System.Collections.Generic;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
    public class AccountFeeDetailService : BaseLayer
    {
        public static ServiceResult<bool> AddAcountFeeDetail(AccountFeeDtailBo model)
        {
            try
            {
                AccountFeeDetailModule.AddAccountFeeDetail(model);
                return Responses.SuccessBoolResult;

            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }

        public static ServiceResult<List<AccountFeeDtailBo>> GetAccountFeeDetails()
        {
            try
            {
                var result = AccountFeeDetailModule.GetAccountFeeDetails();
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<AccountFeeDtailBo>>(null, ex.Message);
            }
        }

        public static ServiceResult<List<Common>> GetPlanList(int schoolId)
        {
            try
            {
                var result = AccountFeeDetailModule.GetPlans(schoolId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }
        public static ServiceResult<bool> UpdateAccountFeeDetail(AccountFeeDtailBo model)
        {
            try
            {
                AccountFeeDetailModule.UpdateAccountFeeDetail(model);
                return Responses.SuccessBoolResult;
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }

        public static ServiceResult<AccountFeeDtailBo> GetAccountFeeDetailById(int id)
        {
            try
            {
                var result = AccountFeeDetailModule.GetAccountFeeDetailById(id);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<AccountFeeDtailBo>(null, ex.Message);
            }
        }
        public static ServiceResult<List<Common>> GetPaymentMode()
        {
            try
            {
                var payment = AccountFeeDetailModule.GetPaymentMode();
                return Responses.SuccessDataResult(payment);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }

        public static ServiceResult<bool> UpdateStatus(int id, int status, int userId)
        {
            try
            {
                var result = AccountFeeDetailModule.UpdateStatus(id, status, userId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;


            }
        }
    }
}
