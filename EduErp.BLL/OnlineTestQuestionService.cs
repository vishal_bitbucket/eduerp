﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
    public class OnlineTestQuestionService : BaseLayer
    {


        public static ServiceResult<List<Common>> GetQuestionTypeId()
        {
            try
            {
                var ListOfQuestionTypeId = OnlineTestQuestionModule.GetQuestionTypeId();
                return Responses.SuccessDataResult(ListOfQuestionTypeId);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }
        public static ServiceResult<int> AddOnlineTestQuestion(OnlineExamTestQuestionBo model)
        {
            try
            {
                var result = OnlineTestQuestionModule.AddOnlineTestQuestion(model);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult(0, ex.Message);

            }
        }
        public static ServiceResult<List<OnlineExamTestQuestionBo>> GetOnlineTestQuestion(int SchoolId)
        {
            try
            {
                var result = OnlineTestQuestionModule.GetOnlineTestQuestion(SchoolId);
                return Responses.SuccessDataResult(result);

            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<OnlineExamTestQuestionBo>>(null, ex.Message);
            }

        }

        public static ServiceResult<bool> UpdateStatus(int id, int status, int userId)
        {
            try
            {
                var result = OnlineTestQuestionModule.UpdateStatus(id, status, userId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;


            }
        }
    public static ServiceResult<OnlineExamTestQuestionBo> GetOnlineTestQuestionById(int id)
       {
           try
           {
               var result = OnlineTestQuestionModule.GetOnlineTestQuestionById(id);
               return Responses.SuccessDataResult(result);
           }
           catch (Exception ex)
           {
               AddApplicationLog(ex);
               return Responses.FailureDataResult<OnlineExamTestQuestionBo>(null, ex.Message);

           }
       }
    public static ServiceResult<bool> UpdateOnlineTestQuestion(OnlineExamTestQuestionBo model)
    {
        try
        {
            OnlineTestQuestionModule.UpdateOnlineTestQuestion(model);
            return Responses.SuccessBoolResult;
        }
        catch (Exception ex)
        {
            AddApplicationLog(ex);
            return Responses.FailureBoolResult;

        }
    }
    public static ServiceResult<List<Common>> GetSubjects(int schoolId)
    {
        try
        {
            var res = OnlineTestQuestionModule.GetSubjects(schoolId);
            return Responses.SuccessDataResult(res);
        }
        catch (Exception ex)
        {
            AddApplicationLog(ex);
            return Responses.FailureDataResult<List<Common>>(null, ex.Message);
        }
    }
    }
}
