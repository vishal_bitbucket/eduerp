﻿using System;
using System.Collections.Generic;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{ 
    public class FeeHeadService:BaseLayer
    {
        public static ServiceResult<int> AddFeeHead(FeeHeadBo model)
        {
            try
            {
               var result= FeeHeadModule.AddFeeHead(model);
               return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<int>(0,ex.Message);

            }
        }
        public static ServiceResult<int> UpdateFeeHead(FeeHeadBo model)
        {
            try
            {
               var res= FeeHeadModule.UpdateFeeHead(model);
                return Responses.SuccessDataResult(res);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult(0,ex.Message);

            }
        }

        public static ServiceResult<int> UpdateStatus(int feeheadId, int statusId, int userId)
        {
            try
            {
               var res= FeeHeadModule.UpdateStatus(feeheadId, statusId, userId);
                return Responses.SuccessDataResult(res);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult(0,ex.Message);
            }
        }

        public static ServiceResult<List<FeeHeadBo>> GetFeeHeads(int schoolId)
        {
            try
            {
                var result = FeeHeadModule.GetFeeHeads(schoolId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<FeeHeadBo>>(null, ex.Message);
            }
        }

        public static ServiceResult<FeeHeadBo> GetFeeHeadById(int feeHeadId)
        {
            try
            {
                var result = FeeHeadModule.GetFeeHeadById(feeHeadId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<FeeHeadBo>(null, ex.Message);
            }
        }
        public static ServiceResult<List<Common>> GetClasses(int schoolId)
        {
            try
            {
                var result = FeeHeadModule.GetClasses(schoolId);
                return Responses.SuccessDataResult(result);

            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }

        }
        public static ServiceResult<List<Common>> GetFeeHeadName(int schoolId)
        {
            try
            {
                var result = FeeHeadModule.GetFeeHeadName(schoolId);
                return Responses.SuccessDataResult(result);

            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }

        }

        public static ServiceResult<List<Common>> GetFeeHeadType()
        {
            try
            {
                var feeType = FeeHeadModule.GetFeeHeadType();
                return Responses.SuccessDataResult(feeType);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }

        public static ServiceResult<List<MonthMenu>> GetFeeHeadMonth()
        {
            try
            {
                var feeMonth = FeeHeadModule.GetFeeHeadMonth();
                return Responses.SuccessDataResult(feeMonth);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<MonthMenu>>(null, ex.Message);
            }
        }
    }
}
