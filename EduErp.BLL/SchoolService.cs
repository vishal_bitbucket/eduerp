﻿using System;
using System.Collections.Generic;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
    public class SchoolService : BaseLayer
    {
        public static ServiceResult<bool> AddSchool(SchoolBo model)
        {
            try
            {
                var result = SchoolModule.AddSchool(model);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }

        public static ServiceResult<List<SchoolBo>> GetSchools()
        {
            try
            {
                var schools = SchoolModule.GetSchools();
                return Responses.SuccessDataResult(schools);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<SchoolBo>>(null, ex.Message);
            }
        }

        public static ServiceResult<SchoolBo> GetSchool(int id)
        {
            try
            {
                var result = SchoolModule.GetSchool(id);
                result.PlanMappingList = SchoolModule.GetPlanMappingList(id);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<SchoolBo>(null, ex.Message);
            }
        }

        public static ServiceResult<bool> UpdateSchool(SchoolBo detail)
        {
            try
            {
                var result = SchoolModule.UpdateSchool(detail);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }

        public static ServiceResult<bool> Delete(SchoolBo model)
        {
            try
            {
                var result = SchoolModule.Delete(model);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }

        public static ServiceResult<IEnumerable<SchoolBo>> GetParentSchoolList()
        {
            try
            {
                var schoolList = SchoolModule.GetParentSchoolList();
                return Responses.SuccessDataResult(schoolList);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<IEnumerable<SchoolBo>>(null, ex.Message);
            }
        }

        public static ServiceResult<bool> UpdateStatus(int id, int statusId)
        {
            try
            {
                var result = SchoolModule.UpdateStatus(id, statusId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }
        public static ServiceResult<bool> UpdateProfilePic(int id, Guid? s3Key)
        {
            try
            {
                var sections = SchoolModule.UpdateProfilePic(id, s3Key);
                return Responses.SuccessDataResult(sections);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }
        public static ServiceResult<InvoiceBo> GetInvoiceData(int id)
        {
            try
            {
                var result = SchoolModule.GetInvoicData(id);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<InvoiceBo>(null,ex.Message);
            }
        }

        public static ServiceResult<List<PlanBo>> GetPlanList(int planTypeId)
        {
            try
            {
                var planList = SchoolModule.GetPlanList(planTypeId);
                return Responses.SuccessDataResult(planList);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<PlanBo>>(null, ex.Message);
            }
        }
        public static ServiceResult<List<CityMaster>> GetCityList(int stateId)
        {
            try
            {
                var cities = SchoolModule.GetCities(stateId);
                return Responses.SuccessDataResult(cities);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<CityMaster>>(null, ex.Message);
            }
        }

        public static ServiceResult<List<StateMaster>> GetStateList()
        {
            try
            {
                var state = SchoolModule.GetStates();
                return Responses.SuccessDataResult(state);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<StateMaster>>(null, ex.Message);
            }
        }

        public static ServiceResult<List<Common>> GetInstituteType()
        {
            try
            {
                var result = SchoolModule.GetInstituteTypeList();
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                
               AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }

        public static ServiceResult<IEnumerable<Common>> GetSchoolsList()
        {
            try
            {
                var schools = SchoolModule.GetSchoolsList();
                return Responses.SuccessDataResult(schools);

            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<IEnumerable<Common>>(null, ex.Message);
            }
        }
    }
}