﻿using System;
using System.Collections.Generic;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
     public class EmployeeTypeService:BaseLayer
    {
         public static ServiceResult<int> AddEmployeeType(EmployeeTypeBo model)
         {
             try
             {
                  var result=EmployeeTypeModule.AddEmployeeType(model);
                 return Responses.SuccessDataResult(result);
             }
             catch (Exception ex)
             {
                 AddApplicationLog(ex);
                 return Responses.FailureDataResult(0);
             }
         }
         public static ServiceResult<bool> UpdateEmployeeType(EmployeeTypeBo model)
         {
             try
             {
                 EmployeeTypeModule.UpdateEmployeeType(model);
                 return Responses.SuccessBoolResult;
             }
             catch (Exception ex)
             {
                 AddApplicationLog(ex);
                 return Responses.FailureBoolResult;
             }
         }
         public static ServiceResult<List<EmployeeTypeBo>> GetEmployeeTypes(int schoolId)
         {
             try
             {
                 var res = EmployeeTypeModule.GetEmployeeType(schoolId);
                 return Responses.SuccessDataResult(res);
             }
             catch (Exception ex)
             {
                AddApplicationLog(ex);
                 return Responses.FailureDataResult<List<EmployeeTypeBo>>(null, ex.Message);
             }
         }

         public static ServiceResult<EmployeeTypeBo> GetEmployeeType(int id)
         {
             try
             {
                 var res = EmployeeTypeModule.GetEmpType(id);
                 return Responses.SuccessDataResult(res);
             }
             catch (Exception ex)
             {
                 AddApplicationLog(ex);
                 return Responses.FailureDataResult<EmployeeTypeBo>(null, ex.Message);
             }
         }

         public static ServiceResult<bool> UpdateStatus(int id, int status, int userId)
         {
             try
             {
                 var result = EmployeeTypeModule.UpdateStatus(id, status, userId);
                 return Responses.SuccessDataResult(result);
             }
             catch (Exception ex)
             {
                 AddApplicationLog(ex);
                 return Responses.FailureBoolResult;


             }

         }
    }
}
