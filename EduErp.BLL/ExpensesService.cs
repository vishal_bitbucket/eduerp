﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EduErp.Base;

namespace EduErp.BLL
{
    public class ExpensesService:Base.BaseLayer
    {
        public static ServiceResult<bool> AddExpenses(BO.ExpensesBo model)
        {
            try
            {
                var result = DAL.ExpensesModule.AddExpenses(model);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }

        public static ServiceResult<bool> UpdateStatus(int id, int status, int userId)
        {
            try
            {
                var result = DAL.ExpensesModule.UpdateStatus(id, status, userId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;


            }

        }

        public static ServiceResult<List<BO.ExpensesBo>> GetInventories(int schoolId)
        {
            try
            {
                var result = DAL.ExpensesModule.GetInventories(schoolId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<BO.ExpensesBo>>(null, ex.Message);
            }
        }

        public static ServiceResult<bool> UpdateExpenses(BO.ExpensesBo model)
        {
            try
            {
                DAL.ExpensesModule.UpdateExpenses(model);
                return Responses.SuccessBoolResult;
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }
        public static ServiceResult<BO.ExpensesBo> GetExpensesByExpensesId(int ExpensesId)
        {
            try
            {
                var result = DAL.ExpensesModule.GetExpensesByExpensesId(ExpensesId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<BO.ExpensesBo>(null, ex.Message);
            }
        }

        public static ServiceResult<List<BO.Common>> GetExpensesType()
        {
            try
            {
                var Expenses = DAL.ExpensesModule.GetExpensesType();
                return Responses.SuccessDataResult(Expenses);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<BO.Common>>(null, ex.Message);
            }
        }

        public static ServiceResult<List<BO.Common>> GetEmpNameList(int schooId)
        {
            try
            {
                var emp = DAL.ExpensesModule.GetEmployeeNameList(schooId);
                return Responses.SuccessDataResult(emp);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<BO.Common>>(null, ex.Message);
            }
        }
        public static ServiceResult<List<BO.Common>> GetItemList(int expenseTypeId,int schooId)
        {
            try
            {
                var emp = DAL.ExpensesModule.GetItemsList(expenseTypeId, schooId);
                return Responses.SuccessDataResult(emp);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<BO.Common>>(null, ex.Message);
            }
        }

        public static ServiceResult<List<BO.Common>> GetVendorList(int expenseTypeId,int schooId)
        {
            try
            {
                var emp = DAL.ExpensesModule.GetVendorList(expenseTypeId,schooId);
                return Responses.SuccessDataResult(emp);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<BO.Common>>(null, ex.Message);
            }
        }
    }
}
