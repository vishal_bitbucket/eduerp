﻿using System;
using System.Collections.Generic;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
    public class DefaultService : BaseLayer
    {
        public static ServiceResult<DashboardBo> GetCountDetails(int schoolId)
        {
            try
            {
                var model = new DashboardBo
                {
                    CountDetails = DefaultModule.GetCounts(schoolId),
                    TodayBirthDetailLists = DefaultModule.TodayBirthDay(schoolId),
                    UpcomingBirthDetailLists = DefaultModule.UpcomingBirthDay(schoolId),
                    GeneralTypeNotificationLists = DefaultModule.GeneralNotice(schoolId),
                    EmployeeTypeNotificationLists = DefaultModule.EmployeeNotice(schoolId),
                    StudentTypeNotificationLists = DefaultModule.StudentNotice(schoolId),
                    SchoolTypeNotificationLists = DefaultModule.SchoolNotice(schoolId)
                    
                    
                };
                return Responses.SuccessDataResult(model);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<DashboardBo>(null, ex.Message);
            }
        }


        public static ServiceResult<BO.ConfigurationBo> GetConfigurationCount(int schollId)
        {
            try
            {
                var result = DefaultModule.GetConfigurationCounts(schollId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                
               AddApplicationLog(ex);
                return Responses.FailureDataResult<BO.ConfigurationBo>(null, ex.Message);
            }
        }

        public static ServiceResult<List<FeeAndExpenseDetail>> GetFeeExpenseDtail(int schoolId,int sessionId)
        {
            try
            {
                var res = DefaultModule.GetExpeneDetail(schoolId, sessionId);
                return Responses.SuccessDataResult(res);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<FeeAndExpenseDetail>>(null, ex.Message);
            }
        }
        public static ServiceResult<List<ClassStudentChartBo>> GetStudentClassList(int schoolId)
        {
            try
            {
                var res = DefaultModule.ClassStudentList(schoolId);
                return Responses.SuccessDataResult(res);
            }
            catch (Exception ex)
            {
                
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<ClassStudentChartBo>>(null, ex.Message);
            }
        }
    }
}
