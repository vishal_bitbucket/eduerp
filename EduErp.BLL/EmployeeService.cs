﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
    public class EmployeeService:BaseLayer
    {
        public static ServiceResult<int> AddEmployee(EmployeeBo Employee)
        {
            try
            {
                var result= EmployeeModule.AddEmployee(Employee);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<int>(0);
            }
        }
        public static ServiceResult<bool> UpdateEmployee(EmployeeBo model)
        {
            try
            {
                 EmployeeModule.UpdateEmployee(model);
                return Responses.SuccessBoolResult;
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }

        public static ServiceResult<List<Common>> GetSubjects(int schoolId)
        {
            try
            {
                var res = EmployeeModule.GetSubjects(schoolId);
                return Responses.SuccessDataResult(res);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }

        public static ServiceResult<List<Common>> GetEmployeeTypeList(int schoolId)
        {
            try
            {
                var emplist = EmployeeModule.GetEmployeeType(schoolId);
                return Responses.SuccessDataResult(emplist);
            }
            catch (Exception ex)
            {
                
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }
        public static ServiceResult<EmployeeBo> GetEmployee(int schoolId, int id)
        {
            try
            {
                var res = EmployeeModule.GetEmployee(schoolId, id);
                if (res.EmployeeTypeId == 1)
                {
                    res.MappingList = EmployeeModule.GetMappingList(id);  
                }
               
                return Responses.SuccessDataResult(res);
            }
            catch (Exception ex)
            {
                
               AddApplicationLog(ex);
                return Responses.FailureDataResult<EmployeeBo>(null, ex.Message);
            }
        }
        public static ServiceResult<List<EmployeeBo>> GetEmployees(int schoolId)
        {
            try
            {
                var res = EmployeeModule.GetEmployees(schoolId);
                return Responses.SuccessDataResult(res);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<EmployeeBo>>(null, ex.Message);
            }
        }

        public static ServiceResult<List<Common>> GetMedium()
        {
            try
            {
                var mediumlist = EmployeeModule.GetMedium();
                return Responses.SuccessDataResult(mediumlist);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }
        public static ServiceResult<bool> UpdateStatus(int id, int statusId, int userId)
        {

            try
            {
                var result = EmployeeModule.UpdateStatus(id, statusId, userId);
                return Responses.SuccessDataResult(result);

            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }

        public static ServiceResult<bool> UpdateProfilePic(int id, Guid? s3Key)
        {
            try
            {
                var sections = EmployeeModule.UpdateProfilePic(id, s3Key);
                return Responses.SuccessDataResult(sections);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }
    }
}
