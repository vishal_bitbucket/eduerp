﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
    public class ExamQuestionService : BaseLayer
    {
        public static ServiceResult<List<Common>> GetExams(int schoolId)
        {
            try
            {
                var res = ExamQuestionModule.GetExams(schoolId);
                return Responses.SuccessDataResult(res);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }
        public static ServiceResult<List<QuestionList>> GetQuestionsForQuestionMapping(int subjectId)
        {
            try
            {
                var result = ExamQuestionModule.GetQuestionsForQuestionMapping(subjectId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<QuestionList>>(null, ex.Message);

            }
        }
       
    }
}
