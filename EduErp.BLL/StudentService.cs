﻿using System;
using System.Collections.Generic;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
    public class StudentService : BaseLayer
    {
        public static ServiceResult<int> AddStudent(StudentBo student)
        {
            try
            {
                var result = StudentModule.AddStudent(student);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.SuccessDataResult<int>(0);
            }
        }
        public static ServiceResult<int> LeftStudent(int schoolId)
        {
            try
            {
                var result = StudentModule.LeftStudent(schoolId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult(0, ex.Message);
            }
        }

        public static ServiceResult<StudentTmplateBo> GetStudentsList(int classId, int sectionId, int statusId, int sessionId, int schoolId)
        {
            try
            {
                StudentTmplateBo model = new StudentTmplateBo
                {
                    StudentFilterLists = StudentModule.GetStudentList(classId, sectionId, statusId, sessionId, schoolId)
                };
                
                return Responses.SuccessDataResult(model);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<StudentTmplateBo>(null, ex.Message);
            }

        }

        public static ServiceResult<StudentTmplateBo> GetStudentsListForReport(int classId, int sectionId, int statusId, int admsnstatusId, int sessionId, int schoolId)
        {
            try
            {
                StudentTmplateBo model = new StudentTmplateBo
                {
                    StudentFilterLists = StudentModule.GetStudentListForReport(classId, sectionId, statusId, admsnstatusId, sessionId, schoolId)
                };

                return Responses.SuccessDataResult(model);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<StudentTmplateBo>(null, ex.Message);
            }

        }
        public static ServiceResult<List<StudentBo>> GetStudents( int schoolId)
        {
            try
            {

                var result = StudentModule.GetStudents(schoolId);


                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<StudentBo>>(null, ex.Message);
            }

        }
        public static ServiceResult<bool> UpdateStatus(int id, int status, int userId)
        {
            try
            {
                var result = StudentModule.UpdateStatus(id, status, userId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;


            }

        }

        public static ServiceResult<bool> UpdateStudentStatus(BO.StudentBo model, int userId)
        {
            try
            {
                var result = StudentModule.UpdateStudentStatus(model, userId);
                return Responses.SuccessDataResult(result);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;


            }

        }


        public static ServiceResult<StudentProfileBo> GetFeeTransactionDetailForProfile(int fromMonth, int toMonth, int studentId, int sessionId)
        {
            try
            {
                StudentProfileBo model = StudentModule.GetStudentFeetransctionForProfile(fromMonth, toMonth, studentId, sessionId);
                //model.FeetransactionDetail = StudentModule.GetStudentFeetransctionForProfile(fromMonth, toMonth,studentId);
                return Responses.SuccessDataResult(model);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<StudentProfileBo>(null, ex.Message);
            }
        }
        public static ServiceResult<StudentProfileBo> GetAttendanceDetailForProfile(int fromMonth, int toMonth, int studentId, int SessionId)
        {
            try
            {
                StudentProfileBo model = StudentModule.GetStudentAttendanceForProfile(fromMonth, toMonth, studentId, SessionId);
                //model.FeetransactionDetail = StudentModule.GetStudentFeetransctionForProfile(fromMonth, toMonth,studentId);
                return Responses.SuccessDataResult(model);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<StudentProfileBo>(new StudentProfileBo() , ex.Message);
            }
        }
        public static ServiceResult<StudentProfileBo> GetStudentsDetail(int studentId)
        {
            try
            {
                StudentProfileBo model = StudentModule.GetStudentsDetail(studentId);
                //model.FeetransactionDetail = StudentModule.GetStudentFeetransctionForProfile(fromMonth, toMonth,studentId);
                return Responses.SuccessDataResult(model);
            }
            catch (Exception ex)
            {

                AddApplicationLog(ex);
                return Responses.FailureDataResult<StudentProfileBo>(new StudentProfileBo(), ex.Message);
            }
        }

        public static ServiceResult<bool> UpdateStudent(StudentBo model)
        {
            try
            {
                StudentModule.UpdateStudent(model);
                return Responses.SuccessBoolResult;
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }

        public static ServiceResult<StudentBo> GetStudentById(int id,int sessionId)
        {
            try
            {
                var result = StudentModule.GetStudentById(id, sessionId);
                return Responses.SuccessDataResult(result);

            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<StudentBo>(null, ex.Message);
            }
        }



        public static ServiceResult<List<CityMaster>> GetCityList(int stateId)
        {
            try
            {
                var cities = SchoolModule.GetCities(stateId);
                return Responses.SuccessDataResult(cities);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<CityMaster>>(null, ex.Message);
            }
        }

        public static ServiceResult<List<Common>> GetGender()
        {
            try
            {
                var genderlist = StudentModule.GetGender();
                return Responses.SuccessDataResult(genderlist);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }

        public static ServiceResult<List<Common>> GetStatus()
        {
            try
            {
                var statuslist = StudentModule.GetStatus();
                return Responses.SuccessDataResult(statuslist);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }

        public static ServiceResult<List<Common>> GetTitle()
        {
            try
            {
                var titlelist = StudentModule.GetTitle();
                return Responses.SuccessDataResult(titlelist);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }

        public static ServiceResult<List<Common>> GetRelegion()
        {
            try
            {
                var relegionlist = StudentModule.GetRelegion();
                return Responses.SuccessDataResult(relegionlist);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }

        public static ServiceResult<List<Common>> GetNationality()
        {
            try
            {
                var nationality = StudentModule.GetNationality();
                return Responses.SuccessDataResult(nationality);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }
        public static ServiceResult<List<Common>> GetCategory()
        {
            try
            {
                var nationality = StudentModule.GetCategory();
                return Responses.SuccessDataResult(nationality);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }

        public static ServiceResult<List<Common>> GetClasses(int schoolId)
        {
            try
            {
                var classes = StudentModule.GetClasses(schoolId);
                return Responses.SuccessDataResult(classes);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }

        public static ServiceResult<List<Common>> GetSessions(int schoolId)
        {
            try
            {
                var sessions = StudentModule.GetSessions(schoolId);
                return Responses.SuccessDataResult(sessions);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }

        public static ServiceResult<List<Common>> GetSections(int classId)
        {
            try
            {
                var sections = StudentModule.GetSections(classId);
                return Responses.SuccessDataResult(sections);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureDataResult<List<Common>>(null, ex.Message);
            }
        }

        public static ServiceResult<bool> UpdateProfilePic(int id, Guid? s3Key)
        {
            try
            {
                var sections = StudentModule.UpdateProfilePic(id, s3Key);
                return Responses.SuccessDataResult(sections);
            }
            catch (Exception ex)
            {
                AddApplicationLog(ex);
                return Responses.FailureBoolResult;
            }
        }
    }
}
