﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Security;
using EduErp.Base;
using EduErp.BO;
using EduErp.BO.Enumerators;
using EduErp.DAL;
using EduErp.Mvc.Utilities;

namespace EduErp.BLL.Base
{
    public class BaseLayer
    {
        public static void AddApplicationLog(Exception exception, LogSources logSources = LogSources.WebService)
        {
            OperatingSystem os = Environment.OSVersion;
            object osName;
            try
            {
                osName = (from x in new ManagementObjectSearcher("SELECT * FROM Win32_OperatingSystem").Get().OfType<ManagementObject>()
                          select x.GetPropertyValue("Caption")).FirstOrDefault();
                osName = osName != null ? osName.ToString() : "Unknown";
            }
            catch (Exception)
            {

                osName = "Unknown";
            }

            var applicationLog = new ApplicationLogBo
            {
                 ID = Guid.NewGuid(),
                UserID = SessionItems.UserId,
                SchoolId = SessionItems.SchoolId,
                SchoolName = SessionItems.SchoolName,
                Module = exception.TargetSite.Module.ToString(),
                ClassName = (exception.TargetSite != null && exception.TargetSite.ReflectedType != null ? exception.TargetSite.ReflectedType.ToString() : string.Empty),
                MethodName = (exception.TargetSite != null ? exception.TargetSite.Name : string.Empty),
                ExceptionMessage = exception.Message,
                StackTrace = exception.StackTrace ?? string.Empty,
                InnerExceptionMessage = exception.InnerException != null ? exception.InnerException.Message : string.Empty,
                InnerExceptionStackTrace = exception.InnerException != null ? exception.InnerException.StackTrace : string.Empty,
                LoggedDate = DateTime.Now,
                LogSourceID = (int)logSources,
                OSInfo = string.Format("OS Name: {0}, Version: {1}, Service Pack: {2}", osName, os.VersionString, os.ServicePack)
               };
            try
            {
                if (HttpContext.Current != null) applicationLog.RequestUrl = HttpContext.Current.Request.Url.AbsoluteUri;
            }
            catch
            {
                applicationLog.RequestUrl = string.Empty;
            }
            try
            {
                LogModule.AddApplicationLog(applicationLog);
            }
            catch { }
            finally
            {
                var host = HttpContext.Current.Request.Url.AbsoluteUri;
                EmailNotifications.NotificationSender.SendExceptionMails(applicationLog, host);
              }
        }

        internal static byte[] GetDefaultProfileImage()
        {
            try
            {
                var profileImagePath = HostingEnvironment.MapPath("~/Content/Images/DefaultProfileImage.png");
                if (!File.Exists(profileImagePath)) return null;
                var profileImage = File.ReadAllBytes(profileImagePath);
                return profileImage;
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal static ServiceResult<bool> Logout()
        {
            try
            {
                HttpContext.Current.Session.Abandon();
                FormsAuthentication.SignOut();
                return Responses.SuccessBoolResult;
            }
            catch (Exception)
            {
                return Responses.FailureBoolResult;
            }
        }

        public static string ClientIpAddress
        {
            get
            {
                return HttpContext.Current == null ? string.Empty : HttpContext.Current.Request.UserHostAddress;
            }
        }
    }
}
