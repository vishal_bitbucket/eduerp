﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EduErp.Base;
using EduErp.BLL.Base;
using EduErp.BO;
using EduErp.DAL;

namespace EduErp.BLL
{
   public class QuestionAnswerService : BaseLayer
    {
       public static ServiceResult<List<BO.QuestionAnswerMappingBo>> GetQuestionAnswer(int SchoolId)
       {
           try
           {
               var result = QuestionAnswerModule.GetQuestionAnswer(SchoolId);
               return Responses.SuccessDataResult(result);

           }
           catch (Exception ex)
           {
               AddApplicationLog(ex);
               return Responses.FailureDataResult<List<BO.QuestionAnswerMappingBo>>(null, ex.Message);
           }

       }
       public static ServiceResult<int> AddQuestionAnswer(QuestionAnswerMappingBo model)
       {
           try
           {
               var result = QuestionAnswerModule.AddQuestionAnswer(model);
               return Responses.SuccessDataResult(result);
           }
           catch (Exception ex)
           {
               AddApplicationLog(ex);
               return Responses.FailureDataResult(0, ex.Message);

           }
       }
       public static ServiceResult<BO.QuestionAnswerMappingBo> GetQuestionAnswerById(int id)
       {
           try
           {
               var result = QuestionAnswerModule.GetQuestionAnswerById(id);
               return Responses.SuccessDataResult(result);
           }
           catch (Exception ex)
           {
               AddApplicationLog(ex);
               return Responses.FailureDataResult<BO.QuestionAnswerMappingBo>(null, ex.Message);

           }
       }
       public static ServiceResult<List<Common>> GetChoiceTypeId()
       {
           try
           {
               var ListOfChoiceTypeId = QuestionAnswerModule.GetChoiceTypeId();
               return Responses.SuccessDataResult(ListOfChoiceTypeId);
           }
           catch (Exception ex)
           {
               AddApplicationLog(ex);
               return Responses.FailureDataResult<List<Common>>(null, ex.Message);
           }
       }
       public static ServiceResult<bool> UpdateStatus(int id, int statusId, int userId)
       {
           try
           {
               var result = QuestionAnswerModule.UpdateStatus(id, statusId, userId);
               return Responses.SuccessDataResult(result);
           }
           catch (Exception ex)
           {
               AddApplicationLog(ex);
               return Responses.FailureBoolResult;


           }
       }
      
    }
}
